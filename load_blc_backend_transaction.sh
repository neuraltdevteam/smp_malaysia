mycnt=`mysql  --login-path=local -N -B -s -D smp -e"select count(*) from report_module_hist where TDR_name ='$1'"`
#echo $mycnt

if [ "$mycnt" -gt 0 ]
then
        echo "Skipped duplicate file(file $1 already been processed)"
        mysql --login-path=local -D smp -e "Insert into report_module_hist (step1_id, step1_desc, process_id, process_desc, processed_date, TDR_name, status) values ('1', 'Process TDR', '1', 'load data', now(), '$1', 'Skipped duplicate file(file already been processed)')"
else
        mysql --login-path=local -D smp -e "LOAD DATA LOCAL INFILE '$1' INTO TABLE blc_backend_trans
                        FIELDS TERMINATED BY '|'
                        ENCLOSED BY ''
                        LINES TERMINATED BY '\n'
                        (
                        product_code,
                        blc_host_id,
                        reserved1,
                        reserved2,
                        reserved3,
                        timestamp,
                        transaction_id,
                        short_code,
                        msisdn,
                        channel,
                        out_host_id,
                        out_channel,
                        start_time,
                        end_time,
                        duration,
                        api_name,
                        reason_code,
                        @tdr_name
                        )
                        SET tdr_name = '$1';
                ;"

                mysql --login-path=local -D smp -e "Insert into report_module_hist (step1_id, step1_desc, process_id, process_desc, processed_date, TDR_name, status) values ('1', 'Process TDR', '1', 'load data', now(), '$1', 'Success')"

                mysql --login-path=local -D smp -e "call sp_insert_monthly_raw_table_daily('$2');"
                mysql --login-path=local -D smp -e "Insert into report_module_hist (step1_id, step1_desc, process_id, process_desc, processed_date, TDR_name, status) values ('1', 'Process TDR', '2', 'insert to monthly table', now(), '$1', 'Success')"

fi
