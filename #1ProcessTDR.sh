#!/bin/bash

export module_folder="/home/umbadm/bin/step1_daily"
export ussd_sess_tdr_prefix="ussd"
export ussd_menu_req_tdr_prefix="menureq"

export blc_incoming_transaction="blc_in"
export addon_incoming_transaction="AddOn_*in"
export base_incoming_transaction="Base_*in"
export bb_incoming_transaction="BB_*in"
export booster_incoming_transaction="Booster_*in"
export iplan_incoming_transaction="IPLAN_*in"
export mb_incoming_transaction="MB_*in"
export myacct_incoming_transaction="MyAcct_*in"
export umi_incoming_transaction="UMI_*in"
export voice_incoming_transaction="Voice_*in"
export sms_incoming_transaction="SMS_*in"
export prp_act_incoming_transaction="PRP_ACT_*in"
export mgm_incoming_transaction="MGM_*in"
export ucard_incoming_transaction="UCard_*in"
export bday_sync_incoming_transaction="FILE_PROCESSING_*in"
export roaming_incoming_transaction="Roaming_*in"
export offpeak_video_incoming_transaction="OFFPEAK_VIDEO_*in"
export autoRenew_incoming_transaction="AutoRenew_*in"
export DCB_incoming_transaction="DCB_*in"

export blc_outgoing_transaction="blc_out"
export addon_outgoing_transaction="AddOn_*out"
export base_outgoing_transaction="Base_*out"
export bb_outgoing_transaction="BB_*out"
export booster_outgoing_transaction="Booster_*out"
export iplan_outgoing_transaction="IPLAN_*out"
export mb_outgoing_transaction="MB_*out"
export myacct_outgoing_transaction="MyAcct_*out"
export umi_outgoing_transaction="UMI_*out"
export voice_outgoing_transaction="Voice_*out"
export sms_outgoing_transaction="SMS_*out"
export prp_act_outgoing_transaction="PRP_ACT_*out"
export mgm_outgoing_transaction="MGM_*out"
export ucard_outgoing_transaction="UCard_*out"
export bday_sync_outgoing_transaction="FILE_PROCESSING_*out"
export roaming_outgoing_transaction="Roaming_*out"
export offpeak_video_outgoing_transaction="OFFPEAK_VIDEO_*out"
export autoRenew_outgoing_transaction="AutoRenew_*out"
export DCB_outgoing_transaction="DCB_*out"

export blc_sms_in_prefix="BLC_*smsin"
#export blc_sms_in_prefix="SMS_in"
export blc_sms_out="BLC_*smsout"
export smpp_gateway_mdr="mdr_smpp3_host"
export ismpp_gateway_mdr="mdr_ismpp"
export usmpp_gateway_mdr="mdr_usmpp"

export UMB_TDR_folder="/data/umb/tdr"
export UMB_CP_REQ_TDR_folder="cp_req"
export UMB_MAP_TDR_folder="map"
export UMB_SMPP_MDR_TDR_folder="smpp_mdr"
export UMB_ISMPP_MDR_TDR_folder="ismpp_mdr"
export UMB_MENU_HIT_TDR_folder="umb_menu_hit"  
export UMB_SESSION_TDR_folder="umb_session"

export BLC_TDR_folder="/log/tdr"

export UMB_IP_1="L28UMBAGWRPT01xxx"
export UMB_ID_1="blc"
export UMB_Pwd_1="blc@umb2015"

export UMB_IP_2="L28UMBAGWRPT02xxx"
export UMB_ID_2="umbadm"
export UMB_Pwd_2="password"

export BLC_IP_1="L28UMBTBBLC01"
export BLC_ID_1="root"
export BLC_Pwd_1="P@ssw0rd123"

export BLC_IP_2="L28UMBTBBLC02"
export BLC_ID_2="root"
export BLC_Pwd_2="password"

export current_mth=$(date +"%Y%m")
#echo $current_mth

export current_day=$(date +"%Y%m%d")
#echo $current_day

export bkp_folder="/data/tdr/processed"
mkdir -p $bkp_folder/$current_mth
mkdir -p $bkp_folder/$current_mth/$current_day
 
export archv_folder="/data1/tdr/processed"
mkdir -p $archv_folder/$current_mth
mkdir -p $archv_folder/$current_mth/$current_day

export log_folder="/log/report_module"
mkdir -p $log_folder
export log_file=$log_folder/step1_$current_day.log 

echo "" 
echo "" >> $log_file
echo "***** Step 1 starts *****" 
echo "***** Step 1 starts *****" >> $log_file
NOW=$(date)
echo "start date: $NOW" >>  $log_file

if [[ -f "$module_folder/processflag.txt" ]]
then
	echo "previous processing not completed!! Please check and delete $module_folder/processflag.txt from DB 01 and DB 02 to proceed." 
	echo "previous processing not completed!! Please check and delete $module_folder/processflag.txt from DB 01 and DB 02 to proceed." >> $log_file

else
 	pkill -f sftp	
	echo "processing step 1" > $module_folder/processflag.txt
	echo "create in_process file"
	echo "create in_process file" >> $log_file
	
	$module_folder/gen_sftp_put_to_data_DB2.sh $module_folder/processflag.txt $module_folder/
	/usr/bin/expect -f $module_folder/sftp_put_to_data_DB2.sh
	echo "copied in_process file to $module_folder in DB 2" 
	echo "copied in_process file to $module_folder in DB 2" >> $log_file
	
	$module_folder/chk_create_raw_tables.sh
	echo "completed checking and creating raw tables" 
	echo "completed checking and creating raw tables" >> $log_file
	
	$module_folder/delete_raw_tables.sh
	echo "deleting data from raw tables" 
	echo "deleting data from raw tables" >> $log_file
	

	$module_folder/sftp_from_blc1_and_move.sh

	$module_folder/gen_sftp_put_to_data_DB2.sh "/data/tdr/*.txt" "/data/tdr"
	/usr/bin/expect -f $module_folder/sftp_put_to_data_DB2.sh
	echo "copied TDR to tdr folder in DB 2" 
	echo "copied TDR to tdr folder in DB 2" >> $log_file	

	$module_folder/process_tdr.sh

	
	$module_folder/sftp_from_blc2_and_move.sh

	$module_folder/gen_sftp_put_to_data_DB2.sh "/data/tdr/*.txt" "/data/tdr"
	/usr/bin/expect -f $module_folder/sftp_put_to_data_DB2.sh
	echo "copied TDR to tdr folder in DB 2" 
	echo "copied TDR to tdr folder in DB 2" >> $log_file	

	$module_folder/process_tdr.sh
	


        #$module_folder/sftp_from_umb_and_move.sh

        #$module_folder/gen_sftp_put_to_data_DB2.sh "/data/tdr/*.txt" "/data/tdr"
        #/usr/bin/expect -f $module_folder/sftp_put_to_data_DB2.sh
        #echo "copied TDR to tdr folder in DB 2"
        #echo "copied TDR to tdr folder in DB 2" >> $log_file

        #$module_folder/process_tdr.sh
	
	$module_folder/gen_sftp_remove_from_data_DB2.sh $module_folder/processflag.txt
	/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
	echo "removed in_process file from $module_folder in DB 2" 
	echo "removed in_process file from $module_folder in DB 2"  >> $log_file
	
	
	rm $module_folder/processflag.txt
	echo "delete in_process file"
	echo "delete in_process file" >> $log_file
	
fi

NOW=$(date)
echo "end date: $NOW" >>  $log_file

echo "***** Step 1 end *****" 
echo "***** Step 1 end *****" >> $log_file
echo ""
echo "" >> $log_file

