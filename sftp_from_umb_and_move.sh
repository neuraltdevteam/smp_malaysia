#!/bin/bash

echo "" 
echo "" >> $log_file
echo "***** sftp from UMB and move starts *****" 
echo "***** sftp from UMB and move starts *****" >> $log_file
NOW=$(date)
echo "start date: $NOW" >>  $log_file

	$module_folder/gen_sftp_from_UMB.sh "$UMB_TDR_folder"/$UMB_CP_REQ_TDR_folder/*.txt /data/tdr $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 CP 
	/usr/bin/expect -f $module_folder/sftp_from_UMB_CP.sh
	echo "downloaded CP Req TDR from UMB Server 1" 
	echo "downloaded CP Req TDR from UMB Server 1" >> $log_file

	$module_folder/gen_sftp_from_UMB.sh "$UMB_TDR_folder"/$UMB_MAP_TDR_folder/*.txt /data/tdr $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 MAP
	/usr/bin/expect -f $module_folder/sftp_from_UMB_MAP.sh
	echo "downloaded MAP TDR from UMB Server 1" 
	echo "downloaded MAP TDR from UMB Server 1" >> $log_file
	
	$module_folder/gen_sftp_from_UMB.sh "$UMB_TDR_folder"/$UMB_SMPP_MDR_TDR_folder/*.txt /data/tdr $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 SMPP
	/usr/bin/expect -f $module_folder/sftp_from_UMB_SMPP.sh
	echo "downloaded SMPP MDR from UMB Server 1" 
	echo "downloaded SMPP MDR from UMB Server 1" >> $log_file

        $module_folder/gen_sftp_from_UMB.sh "$UMB_TDR_folder"/$UMB_ISMPP_MDR_TDR_folder/*.txt /data/tdr $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 ISMPP
        /usr/bin/expect -f $module_folder/sftp_from_UMB_ISMPP.sh
        echo "downloaded ISMPP MDR from UMB Server 1"
        echo "downloaded ISMPP MDR from UMB Server 1" >> $log_file
	
	$module_folder/gen_sftp_from_UMB.sh "$UMB_TDR_folder"/$UMB_MENU_HIT_TDR_folder/*.txt /data/tdr $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 MENU
	/usr/bin/expect -f $module_folder/sftp_from_UMB_MENU.sh
	echo "downloaded UMB Menu Hit TDR from UMB Server 1" 
	echo "downloaded UMB Menu Hit TDR from UMB Server 1" >> $log_file
	
	$module_folder/gen_sftp_from_UMB.sh "$UMB_TDR_folder"/$UMB_SESSION_TDR_folder/*.txt /data/tdr $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 SESS
	/usr/bin/expect -f $module_folder/sftp_from_UMB_SESS.sh
	echo "downloaded UMB Session TDR from UMB Server 1" 
	echo "downloaded UMB Session TDR from UMB Server 1" >> $log_file		
		
	FILES=/data/tdr/*.txt
	for f in $FILES
	do

	$module_folder/gen_sftp_move_in_UMB.sh "$UMB_TDR_folder"/$UMB_CP_REQ_TDR_folder/${f##*/} "$UMB_TDR_folder"/processed/$current_mth/$current_day/${f##*/} "$UMB_TDR_folder"/processed/$current_mth "$UMB_TDR_folder"/processed/$current_mth/$current_day $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 CP 
	/usr/bin/expect -f $module_folder/sftp_move_in_UMB_CP.sh
	echo "moved CP Req TDR to processed folder in UMB Server 1" 
	echo "moved CP Req TDR to processed folder in UMB Server 1" >> $log_file

	$module_folder/gen_sftp_move_sub_in_UMB.sh "$UMB_TDR_folder"/$UMB_MAP_TDR_folder/${f##*/} "$UMB_TDR_folder"/processed/$current_mth/$current_day/map/${f##*/} "$UMB_TDR_folder"/processed/$current_mth "$UMB_TDR_folder"/processed/$current_mth/$current_day $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 "$UMB_TDR_folder"/processed/$current_mth/$current_day/map MAP
	/usr/bin/expect -f $module_folder/sftp_move_sub_in_UMB_MAP.sh
	echo "moved MAP TDR to processed folder in UMB Server 1" 
	echo "moved MAP TDR to processed folder in UMB Server 1" >> $log_file

	$module_folder/gen_sftp_move_sub_in_UMB.sh "$UMB_TDR_folder"/$UMB_SMPP_MDR_TDR_folder/${f##*/} "$UMB_TDR_folder"/processed/$current_mth/$current_day/smpp_mdr/${f##*/} "$UMB_TDR_folder"/processed/$current_mth "$UMB_TDR_folder"/processed/$current_mth/$current_day $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 "$UMB_TDR_folder"/processed/$current_mth/$current_day/smpp_mdr SMPP
	/usr/bin/expect -f $module_folder/sftp_move_sub_in_UMB_SMPP.sh
	echo "moved SMPP MDR to processed folder in UMB Server 1" 
	echo "moved SMPP MDR to processed folder in UMB Server 1" >> $log_file

	$module_folder/gen_sftp_move_sub_in_UMB.sh "$UMB_TDR_folder"/$UMB_ISMPP_MDR_TDR_folder/${f##*/} "$UMB_TDR_folder"/processed/$current_mth/$current_day/ismpp_mdr/${f##*/} "$UMB_TDR_folder"/processed/$current_mth "$UMB_TDR_folder"/processed/$current_mth/$current_day $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 "$UMB_TDR_folder"/processed/$current_mth/$current_day/ismpp_mdr ISMPP
	/usr/bin/expect -f $module_folder/sftp_move_sub_in_UMB_ISMPP.sh
	echo "moved ISMPP MDR to processed folder in UMB Server 1" 
	
	$module_folder/gen_sftp_move_in_UMB.sh "$UMB_TDR_folder"/$UMB_MENU_HIT_TDR_folder/${f##*/} "$UMB_TDR_folder"/processed/$current_mth/$current_day/${f##*/} "$UMB_TDR_folder"/processed/$current_mth "$UMB_TDR_folder"/processed/$current_mth/$current_day $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 MENU
	/usr/bin/expect -f $module_folder/sftp_move_in_UMB_MENU.sh
	echo "moved UMB Menu Hit TDR to processed folder in UMB Server 1" 
	echo "moved UMB Menu Hit TDR to processed folder in UMB Server 1" >> $log_file

	$module_folder/gen_sftp_move_in_UMB.sh "$UMB_TDR_folder"/$UMB_SESSION_TDR_folder/${f##*/} "$UMB_TDR_folder"/processed/$current_mth/$current_day/${f##*/} "$UMB_TDR_folder"/processed/$current_mth "$UMB_TDR_folder"/processed/$current_mth/$current_day $UMB_IP_1 $UMB_ID_1 $UMB_Pwd_1 SESS
	/usr/bin/expect -f $module_folder/sftp_move_in_UMB_SESS.sh	
	echo "moved UMB Session TDR to processed folder in UMB Server 1" 
	echo "moved UMB Session TDR to processed folder in UMB Server 1" >> $log_file

	done
	

NOW=$(date)
echo "end date: $NOW" >>  $log_file

echo "***** sftp from UMB and move end *****" 
echo "***** sftp from UMB and move end *****" >> $log_file
echo ""
echo "" >> $log_file

