#!/bin/bash
module_folder="/home/umbadm/bin/step1_daily"

echo "#!/usr/bin/expect -f" > $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "spawn sftp $6@$5" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
#echo "expect \"password:\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
#echo "send \"$7\n\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "send \"mkdir $3 \n\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "send \"mkdir $4 \n\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "send \"mkdir $8 \n\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "send \"rename $1 $2 \n\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "send \"rm $1 \n\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh
echo "send \"exit\n\"" >> $module_folder/sftp_move_sub_in_UMB_$9.sh

