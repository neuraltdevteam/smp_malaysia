#!/bin/bash

export module_folder="/home/umbadm/bin/step1_daily"
export ussd_sess_tdr_prefix="ussd"
export ussd_menu_req_tdr_prefix="menureq"
export blc_incoming_transaction="blc_in"
export addon_incoming_transaction="AddOn_in"
export base_incoming_transaction="Base_in"
export bb_incoming_transaction="BB_in"
export booster_incoming_transaction="Booster_in"
export iplan_incoming_transaction="IPLAN_in"
export mb_incoming_transaction="MB_in"
export myacct_incoming_transaction="MyAcct_in"
export umi_incoming_transaction="UMI_in"
export voice_incoming_transaction="Voice_in"

export blc_outgoing_transaction="blc_out"
export addon_outgoing_transaction="AddOn_out"
export base_outgoing_transaction="Base_out"
export bb_outgoing_transaction="BB_out"
export booster_outgoing_transaction="Booster_out"
export iplan_outgoing_transaction="IPLAN_out"
export mb_outgoing_transaction="MB_out"
export myacct_outgoing_transaction="MyAcct_out"
export umi_outgoing_transaction="UMI_out"
export voice_outgoing_transaction="Voice_out"

export log_folder="/log/report_module"
mkdir -p $log_folder
export log_file=$log_folder/step1_$current_day.log 

	echo "Process tdr starts"
	#echo "Process tdr starts" >> $log_file

	FILES=/data/tdr/blc_test/*.txt
	for f in $FILES
	do
	
	echo ""
  echo "Processing $f file..." 
  #echo "Processing $f file..." >> $log_file

	if [[ $f == *$ussd_sess_tdr_prefix* ]]
	then	  
		echo "completed loading $f file..."
		#echo "completed loading $f file..." >> $log_file

	elif [[ ($f == *$addon_incoming_transaction*) || ($f == *$base_incoming_transaction*) || ($f == *$bb_incoming_transaction*) || ($f == *$booster_incoming_transaction*) || ($f == *$iplan_incoming_transaction*) || ($f == *$mb_incoming_transaction*) || ($f == *$myacct_incoming_transaction*) || ($f == *$umi_incoming_transaction*) || ($f == *$voice_incoming_transaction*) ]]
	then	

		echo "in blc incoming, processing $f"

		mysql --login-path=local -D smp -e "delete from blc_incoming_trans;"

		$module_folder/loadfile/load_blc_incoming_transaction.sh $f	$blc_incoming_transaction
		#echo "completed loading $f file..." >> $log_file
		echo "completed loading $f file..." 

	elif [[ ($f == *$addon_outgoing_transaction*) || ($f == *$base_outgoing_transaction*) || ($f == *$bb_outgoing_transaction*) || ($f == *$booster_outgoing_transaction*) || ($f == *$iplan_outgoing_transaction*) || ($f == *$mb_outgoing_transaction*) || ($f == *$myacct_outgoing_transaction*) || ($f == *$umi_outgoing_transaction*) || ($f == *$voice_outgoing_transaction*) ]]
	then	

		echo "in blc outgoing, processing $f"

		mysql --login-path=local -D smp -e "delete from blc_backend_trans;"

		$module_folder/loadfile/load_blc_backend_transaction.sh $f	$blc_outgoing_transaction
		#echo "completed loading $f file..." >> $log_file
		echo "completed loading $f file..." 

	fi
	
	done

	echo "Process tdr end"
