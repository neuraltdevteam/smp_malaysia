#!/bin/bash
module_folder="/home/umbadm/bin/step1_daily"
echo $1
echo $2
echo $3
echo $4

echo "#!/usr/bin/expect -f" > $module_folder/sftp_from_BLC.sh
echo "set timeout -1" > $module_folder/sftp_from_BLC.sh
echo "spawn sftp $4@$3" >> $module_folder/sftp_from_BLC.sh
echo "expect \"password:\"" >> $module_folder/sftp_from_BLC.sh
echo "send \"$5\n\"" >> $module_folder/sftp_from_BLC.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_from_BLC.sh
echo "send \"mget $1 $2 \n\"" >> $module_folder/sftp_from_BLC.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_from_BLC.sh
echo "send \"exit\n\"" >> $module_folder/sftp_from_BLC.sh
