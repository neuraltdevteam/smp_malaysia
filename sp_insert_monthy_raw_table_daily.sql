begin


Declare ussd_sess_tdr_prefix varchar(20);
Declare ussd_menu_req_tdr_prefix varchar(20);
Declare blc_incoming_transaction varchar(20);
Declare blc_backend_transaction varchar(20);
Declare blc_sms_in_prefix varchar(20);
Declare blc_sms_out varchar(20);
Declare smpp_gateway_mdr varchar(20);
Declare ismpp_gateway_mdr varchar(20);
Declare current_ymd varchar(8);
Declare ytd_ymd varchar(8);

set current_ymd = date_format(now(), '%Y%m%d');
set ytd_ymd = date_format((date_sub(now(), interval -1 day)), '%Y%m%d');


set ussd_sess_tdr_prefix="ussd";
set ussd_menu_req_tdr_prefix="menureq";
set blc_incoming_transaction="blc_in";
set blc_backend_transaction="blc_out";
set blc_sms_in_prefix="BLC_*smsin";
set blc_sms_out="BLC_*smsout";
set smpp_gateway_mdr="mdr_smpp3_host";
set ismpp_gateway_mdr="mdr_ismpp";

if IN_TDR_PREFIX = ussd_sess_tdr_prefix then

	set @sqlfull = concat("Insert into UMB_SESS_", current_ymd , " (host_id, short_code, msisdn, session_id, start_date, end_date,sub_grp_id)");
	set @sqlfull = concat(@sqlfull, "select host_id, short_code, msisdn,tdr_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(end_time,12), substr(end_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'),sub_grp_id from umb_session;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("update UMB_SESS_", current_ymd , " set duration = TIMESTAMPDIFF(microsecond,start_date, end_date) / 1000 WHERE duration IS NULL;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("INSERT INTO CHANNEL_", current_ymd , " (host_id,msisdn, session_id, transaction_id, date, channel_name) ");
	set @sqlfull = concat(@sqlfull, " SELECT  host_id,msisdn, tdr_id, tdr_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'),'USSD' FROM umb_session ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

end if;

if IN_TDR_PREFIX = ussd_menu_req_tdr_prefix  then

	set @sqlfull = concat("Insert into UMB_MENU_HIT_", current_ymd , " (host_id,short_code,msisdn,session_id,transaction_id,start_date,end_date, menu_id,menu_name,trans_status,reason_code) ");
	set @sqlfull = concat(@sqlfull, "select host_id, short_code, msisdn,substr(tdr_id, 1, length(tdr_id)-2), tdr_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(`timestamp`,12), substr(`timestamp`, 13, 3), '000'), '%Y%m%d%H%i%s%f'), menu_id, menu_name, (case when (`status_code` = 0) then 'S' else 'F' end), status_code from umb_menu_req;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;


	set @sqlfull = concat("update UMB_MENU_HIT_", current_ymd , "  set duration = TIMESTAMPDIFF(microsecond,start_date, end_date) / 1000 WHERE duration IS NULL;;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

end if;

if IN_TDR_PREFIX = blc_incoming_transaction then

	set @sqlfull = concat("Insert into UMB_BLC_INTERFACE_", current_ymd , " (host_id,short_code,msisdn,session_id,transaction_id,start_date,end_date,duration,interface_name,trans_status,reason_code,result_str,tdr_name) ");
	set @sqlfull = concat(@sqlfull, "select blc_host_id, short_code,msisdn, substr(transaction_id, 1, length(transaction_id) -2), transaction_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(`end_time`,12), substr(`end_time`, 13, 3), '000'), '%Y%m%d%H%i%s%f'),duration,'HTTP to BLC',(case when (`reason_code` like '00%') then 'S' else 'F' end), reason_code,result_str,tdr_name from blc_incoming_trans where blc_incoming_trans.transaction_id not like '%:%';");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("Insert into BLC_TXN_TYPE_", current_ymd , " (host_id,short_code,channel,msisdn,session_id,transaction_id,start_date,end_date,duration,trans_type,in_api_name,trans_status,reason_code,tdr_name) ");
	set @sqlfull = concat(@sqlfull, "select blc_host_id, short_code, channel,msisdn,substr(transaction_id, 1, length(transaction_id) -2),transaction_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(end_time,12), substr(end_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'),duration,trans_type, api_name,(case when (`reason_code` like '00%') then 'S' else 'F' end), reason_code,tdr_name FROM blc_incoming_trans where blc_incoming_trans.transaction_id not like '%:%' ;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("Insert into UMB_BLC_INTERFACE_", current_ymd , " (host_id,short_code,msisdn,session_id,transaction_id,start_date,end_date,duration,interface_name,trans_status,reason_code,result_str,tdr_name) ");
	set @sqlfull = concat(@sqlfull, "select blc_host_id, short_code,msisdn, substr(transaction_id, 1, length(transaction_id) -3), transaction_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(`end_time`,12), substr(`end_time`, 13, 3), '000'), '%Y%m%d%H%i%s%f'),duration,'HTTP to BLC',(case when (`reason_code` like '00%') then 'S' else 'F' end), reason_code,result_str,tdr_name from blc_incoming_trans where blc_incoming_trans.transaction_id like '%:%';");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("Insert into BLC_TXN_TYPE_", current_ymd , " (host_id,short_code,channel,msisdn,session_id,transaction_id,start_date,end_date,duration,trans_type,in_api_name,trans_status,reason_code,tdr_name) ");
	set @sqlfull = concat(@sqlfull, "select blc_host_id, short_code, channel,msisdn,substr(transaction_id, 1, length(transaction_id) -3),transaction_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(end_time,12), substr(end_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'),duration,trans_type, api_name,(case when (`reason_code` like '00%') then 'S' else 'F' end), reason_code,tdr_name FROM blc_incoming_trans where blc_incoming_trans.transaction_id like '%:%' ;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;


	set @sqlfull = concat("INSERT INTO CHANNEL_", current_ymd , " (host_id,msisdn, session_id, transaction_id, date, channel_name,tdr_name) ");
	set @sqlfull = concat(@sqlfull, " SELECT  blc_host_id, msisdn, transaction_id, transaction_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), channel,tdr_name FROM blc_incoming_trans where channel <> 'USSD' and channel <> 'SMS' ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	#set @sqlfull = concat("update UMB_BLC_INTERFACE_", current_ymd , "  set session_id = substr(transaction_id, 1, length(transaction_id) -3) where transaction_id like '%:%' and start_date >= date_sub(now(), interval 1 day);");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	#set @sqlfull = concat("update BLC_TXN_TYPE_", current_ymd , "  set session_id = substr(transaction_id, 1, length(transaction_id) -3) where transaction_id like '%:%' and start_date >= date_sub(now(), interval 1 day);");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	#drop temporary table if exists RemoveAsynRec;

	#set @sqlfull = concat("CREATE TEMPORARY TABLE RemoveAsynRec Select transaction_id, trans_type, in_api_name from BLC_TXN_TYPE_", current_ymd , " " );
	#set @sqlfull = concat(@sqlfull, "where start_date >= date_sub(now(), interval 1 day) and ");
	#set @sqlfull = concat(@sqlfull, "( transaction_id not like '%+%' and length(transaction_id) >= 27)  ");
	#set @sqlfull = concat(@sqlfull, "group by transaction_id, trans_type, in_api_name ");
	#set @sqlfull = concat(@sqlfull, "having count(transaction_id) > 1; ");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	#set @sqlfull = concat("DELETE  FROM BLC_TXN_TYPE_", current_ymd , " ");
	#set @sqlfull = concat(@sqlfull, "WHERE EXISTS ");
	#set @sqlfull = concat(@sqlfull, "(SELECT f.transaction_id FROM RemoveAsynRec f ");
	#set @sqlfull = concat(@sqlfull, "WHERE start_date >= date_sub(now(), interval 1 day) and f.transaction_id = BLC_TXN_TYPE_", current_ymd , ".transaction_id and ");
	#set @sqlfull = concat(@sqlfull, "f.trans_type = BLC_TXN_TYPE_", current_ymd , ".trans_type  and ");
	#set @sqlfull = concat(@sqlfull, "f.in_api_name = BLC_TXN_TYPE_", current_ymd , ".in_api_name and ");
	#set @sqlfull = concat(@sqlfull, "trans_status = 'F' and reason_code = '');");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#$deallocate prepare stmt;

	#set @sqlfull = concat("DELETE FROM UMB_BLC_INTERFACE_", current_ymd , " ");
	#set @sqlfull = concat(@sqlfull, "WHERE EXISTS ");
	#set @sqlfull = concat(@sqlfull, "(SELECT f.transaction_id FROM RemoveAsynRec f ");
	#set @sqlfull = concat(@sqlfull, "WHERE start_date >= date_sub(now(), interval 1 day) and f.transaction_id = UMB_BLC_INTERFACE_", current_ymd , ".transaction_id and ");
	#set @sqlfull = concat(@sqlfull, "trans_status = 'F' and reason_code = '0');");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	#drop temporary table if exists RemoveAsynRec;

end if;

if IN_TDR_PREFIX = blc_backend_transaction then

	Update blc_backend_trans set api_name = concat(api_name, ' (', short_code, ')') where api_name = 'ASYNC_RESP' or api_name = 'ASYNC_TIMEOUT ' or api_name = 'ORDER_BROADCAST';

	set @sqlfull = concat("Insert into BLC_EXT_INTERFACE_", current_ymd , " (host_id,short_code,reserved1,reserved2,reserved3,channel,msisdn,out_host_id,out_channel,session_id,transaction_id,start_date,end_date,duration,api_name,trans_status,reason_code,tdr_name) ");
	set @sqlfull = concat(@sqlfull, " select blc_host_id, short_code,reserved1,reserved2,reserved3, channel,msisdn,out_host_id,out_channel,substr(transaction_id, 1, length(transaction_id) -2),transaction_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(end_time,12), substr(end_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'),duration, api_name, (case when (`reason_code` like '00%') then 'S' else 'F' end), reason_code,tdr_name FROM blc_backend_trans where blc_backend_trans.transaction_id not like '%:%' and out_channel != 'CCN'; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("Insert into BLC_EXT_INTERFACE_", current_ymd , " (host_id,short_code,reserved1,reserved2,reserved3,channel,msisdn,out_host_id,out_channel,session_id,transaction_id,start_date,end_date,duration,api_name,trans_status,reason_code,tdr_name) ");
	set @sqlfull = concat(@sqlfull, " select blc_host_id, short_code,reserved1,reserved2,reserved3, channel,msisdn,out_host_id,out_channel,substr(transaction_id, 1, length(transaction_id) -3),transaction_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(end_time,12), substr(end_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'),duration, api_name, (case when (`reason_code` like '00%') then 'S' else 'F' end), reason_code,tdr_name FROM blc_backend_trans where blc_backend_trans.transaction_id like '%:%' and out_channel != 'CCN'; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("Insert into BLC_EXT_INTERFACE_", current_ymd , " (host_id,short_code,reserved1,reserved2,reserved3,channel,msisdn,out_host_id,out_channel,session_id,transaction_id,start_date,end_date,duration,api_name,trans_status,reason_code,tdr_name) ");
	set @sqlfull = concat(@sqlfull, " select blc_host_id, short_code,reserved1,reserved2,reserved3, channel,msisdn,out_host_id,out_channel,substr(transaction_id, 1, length(transaction_id) -2),transaction_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(end_time,12), substr(end_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'),duration, api_name, (case when (`reason_code` like '2001%') then 'S' else 'F' end), reason_code,tdr_name FROM blc_backend_trans where blc_backend_trans.transaction_id not like '%:%' and out_channel = 'CCN'; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("Insert into BLC_EXT_INTERFACE_", current_ymd , " (host_id,short_code,reserved1,reserved2,reserved3,channel,msisdn,out_host_id,out_channel,session_id,transaction_id,start_date,end_date,duration,api_name,trans_status,reason_code,tdr_name) ");
	set @sqlfull = concat(@sqlfull, " select blc_host_id, short_code,reserved1,reserved2,reserved3, channel,msisdn,out_host_id,out_channel,substr(transaction_id, 1, length(transaction_id) -3),transaction_id, str_to_date(concat('20', left(start_time,12), substr(start_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'), str_to_date(concat('20', left(end_time,12), substr(end_time, 13, 3), '000'), '%Y%m%d%H%i%s%f'),duration, api_name, (case when (`reason_code` like '2001%') then 'S' else 'F' end), reason_code,tdr_name FROM blc_backend_trans where blc_backend_trans.transaction_id like '%:%' and out_channel = 'CCN'; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;


	#set @sqlfull = concat("update BLC_TXN_TYPE_", current_ymd , "");
	#set @sqlfull = concat(@sqlfull, " INNER JOIN BLC_EXT_INTERFACE_", current_ymd , " ON BLC_TXN_TYPE_", current_ymd , ".transaction_id = BLC_EXT_INTERFACE_", current_ymd , ".transaction_id");
	#set @sqlfull = concat(@sqlfull, " set ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", current_ymd , ".out_api_name = BLC_EXT_INTERFACE_", current_ymd , ".api_name, ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", current_ymd , ".out_host_id = BLC_EXT_INTERFACE_", current_ymd , ".out_host_id,");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", current_ymd , ".out_channel = BLC_EXT_INTERFACE_", current_ymd , ".out_channel ");
	#set @sqlfull = concat(@sqlfull, " where BLC_TXN_TYPE_", current_ymd , ".out_api_name IS NULL AND ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", current_ymd , ".out_host_id IS NULL ");
	#set @sqlfull = concat(@sqlfull, " AND BLC_TXN_TYPE_", current_ymd , ".out_channel IS NULL;");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	#set @sqlfull = concat("update BLC_TXN_TYPE_", ytd_ymd , "");
	#set @sqlfull = concat(@sqlfull, " INNER JOIN BLC_EXT_INTERFACE_", current_ymd , " ON BLC_TXN_TYPE_", ytd_ymd , ".transaction_id = BLC_EXT_INTERFACE_", current_ymd , ".transaction_id");
	#set @sqlfull = concat(@sqlfull, " set ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_api_name = BLC_EXT_INTERFACE_", current_ymd , ".api_name, ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_host_id = BLC_EXT_INTERFACE_", current_ymd , ".out_host_id,");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_channel = BLC_EXT_INTERFACE_", current_ymd , ".out_channel ");
	#set @sqlfull = concat(@sqlfull, " where BLC_TXN_TYPE_", ytd_ymd , ".out_api_name IS NULL AND ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_host_id IS NULL ");
	#set @sqlfull = concat(@sqlfull, " AND BLC_TXN_TYPE_", ytd_ymd , ".out_channel IS NULL;");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	#set @sqlfull = concat("update BLC_TXN_TYPE_", ytd_ymd , "");
	#set @sqlfull = concat(@sqlfull, " INNER JOIN BLC_EXT_INTERFACE_", ytd_ymd , " ON BLC_TXN_TYPE_", ytd_ymd , ".transaction_id = BLC_EXT_INTERFACE_", ytd_ymd , ".transaction_id");
	#set @sqlfull = concat(@sqlfull, " set ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_api_name = BLC_EXT_INTERFACE_", ytd_ymd , ".api_name, ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_host_id = BLC_EXT_INTERFACE_", ytd_ymd , ".out_host_id,");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_channel = BLC_EXT_INTERFACE_", ytd_ymd , ".out_channel ");
	#set @sqlfull = concat(@sqlfull, " where BLC_TXN_TYPE_", ytd_ymd , ".out_api_name IS NULL AND ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_host_id IS NULL ");
	#set @sqlfull = concat(@sqlfull, " AND BLC_TXN_TYPE_", ytd_ymd , ".out_channel IS NULL;");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;


	#set @sqlfull = concat("update BLC_TXN_TYPE_", ytd_ymd , "");
	#set @sqlfull = concat(@sqlfull, " INNER JOIN BLC_EXT_INTERFACE_", ytd_ymd , " ON BLC_TXN_TYPE_", ytd_ymd , ".transaction_id = BLC_EXT_INTERFACE_", ytd_ymd , ".transaction_id");
	#set @sqlfull = concat(@sqlfull, " set ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_api_name = BLC_EXT_INTERFACE_", ytd_ymd , ".api_name, ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_host_id = BLC_EXT_INTERFACE_", ytd_ymd , ".out_host_id,");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_channel = BLC_EXT_INTERFACE_", ytd_ymd , ".out_channel ");
	#set @sqlfull = concat(@sqlfull, " where BLC_TXN_TYPE_", ytd_ymd , ".out_api_name IS NULL AND ");
	#set @sqlfull = concat(@sqlfull, " BLC_TXN_TYPE_", ytd_ymd , ".out_host_id IS NULL ");
	#set @sqlfull = concat(@sqlfull, " AND BLC_TXN_TYPE_", ytd_ymd , ".out_channel IS NULL;");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;


	#set @sqlfull = concat("update BLC_EXT_INTERFACE_", current_ymd , "  set session_id = substr(transaction_id, 1, length(transaction_id) -3) where transaction_id like '%:%' and start_date >= date_sub(now(), interval 1 day);");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

end if;

if IN_TDR_PREFIX = blc_sms_in_prefix then
	set @sqlfull = concat("Insert into SMS_IN_", current_ymd , " (host_id,msisdn,session_id,transaction_id,start_date,dest_addr,received_content,IN_SMS_CHANNEL,tdr_name) ");
	set @sqlfull = concat(@sqlfull, " select blc_host_id, msisdn,transaction_id,transaction_id,str_to_date(concat('20', left(timestamp,12), substr(timestamp, 13, 3), '000'), '%Y%m%d%H%i%s%f'),destination_add,received_content,IN_SMS_CHANNEL,tdr_name FROM blc_sms_in;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("INSERT INTO CHANNEL_", current_ymd , " (host_id,msisdn, session_id, transaction_id, date, channel_name,in_sms_channel,tdr_name) ");
	set @sqlfull = concat(@sqlfull, " SELECT  blc_host_id,msisdn, transaction_id,transaction_id, str_to_date(concat('20', left(timestamp,12), substr(timestamp, 13, 3), '000'), '%Y%m%d%H%i%s%f'),'SMS',IN_SMS_CHANNEL,tdr_name FROM blc_sms_in ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

end if;

if IN_TDR_PREFIX = blc_sms_out then

	set @sqlfull = concat("Insert into BLC_SMS_OUT_", current_ymd , " (host_id,msisdn,session_id,transaction_id,start_date,sms_content,messsage_id,trans_status,reason_code,tdr_name) ");
	set @sqlfull = concat(@sqlfull, " select host_id, msisdn,transaction_id,transaction_id,str_to_date(concat('20', left(timestamp,12), substr(timestamp, 13, 3), '000'), '%Y%m%d%H%i%s%f'),content,message_id,(case when (`reason_code` = 0) then 'S' else 'F' end), reason_code,tdr_name FROM blc_sms_out;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("Insert into SMS_OUT_", current_ymd , " (host_id,msisdn,session_id,transaction_id,request_date,sms_content,message_id,request_status,req_reason_code,tdr_name) ");
	set @sqlfull = concat(@sqlfull, " select host_id, msisdn,transaction_id ,transaction_id,str_to_date(concat('20', left(timestamp,12), substr(timestamp, 13, 3), '000'), '%Y%m%d%H%i%s%f'),content,message_id,(case when (`reason_code` = 0) then 'S' else 'F' end), reason_code,tdr_name FROM blc_sms_out;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("update BLC_SMS_OUT_", current_ymd , "  set session_id = substr(transaction_id, 1, length(transaction_id) -3) where transaction_id like '%:%' and start_date >= date_sub(now(), interval 1 day);");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("update SMS_OUT_", current_ymd , "  set session_id = substr(transaction_id, 1, length(transaction_id) -3) where transaction_id like '%:%' and request_date >= date_sub(now(), interval 1 day);");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

end if;

if IN_TDR_PREFIX = smpp_gateway_mdr then

	update smpp_gw_mdr set transaction_id = (case when (substr(optional_param,1,2) = '08') then right(optional_param, length(optional_param)-2)  else '' end)  WHERE optional_param IS NOT NULL AND optional_param <> '';

	update smpp_gw_mdr set transaction_id = substr(transaction_id, 5, cast(CONV(substr(transaction_id,1,4),16,10) as decimal) * 2) WHERE transaction_id IS NOT NULL;

	update smpp_gw_mdr set transaction_id = CONVERT(unhex(transaction_id) USING utf8) WHERE transaction_id IS NOT NULL;

	set @sqlfull = concat("Insert into SMPP_MDR_", current_ymd , " (timestamp,message_id,command_id,command_status,src_addr,dest_addr,dest_imsi, usr_data_length,usr_data_header_flag,user_data,data_coding_scheme,message_status,result_code,priority_flag,transaction_id, dr_enable, session_id,optional_param) ");
	set @sqlfull = concat(@sqlfull, " select str_to_date(concat('20', trim(timestamp), '000'), '%Y%m%d %H%i%s.%f'),MsgRefId,command_id,command_status,src_addr,dest_addr,imsi,usr_data_length,usr_data_header_flag,user_data,data_coding_scheme,message_status,result_code,priority_flag,transaction_id,'1', null, optional_param FROM smpp_gw_mdr;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("UPDATE SMPP_MDR_", current_ymd , " ");
	set @sqlfull = concat(@sqlfull, " SET SMPP_MDR_", current_ymd , ".session_id = substr(transaction_id, 1, length(transaction_id) -2)  ");
	set @sqlfull = concat(@sqlfull, " WHERE timestamp >= date_sub(now(), interval 1 day) and SMPP_MDR_", current_ymd , ".session_id IS NULL and length(transaction_id) >= 27 and transaction_id not like '%:%' and transaction_id not like '%+%' ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("UPDATE SMPP_MDR_", current_ymd , " ");
	set @sqlfull = concat(@sqlfull, " SET SMPP_MDR_", current_ymd , ".session_id = transaction_id ");
	set @sqlfull = concat(@sqlfull, " WHERE timestamp >= date_sub(now(), interval 1 day) and SMPP_MDR_", current_ymd , ".session_id IS NULL");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;


	set @sqlfull = concat("UPDATE SMPP_MDR_", current_ymd , "  INNER JOIN ");
	set @sqlfull = concat(@sqlfull, " smpp_gw_mdr ON SMPP_MDR_", current_ymd , " .message_id = smpp_gw_mdr.MsgRefId AND ");
	set @sqlfull = concat(@sqlfull, " SMPP_MDR_", current_ymd , " .command_id = smpp_gw_mdr.command_id ");
	set @sqlfull = concat(@sqlfull, " SET SMPP_MDR_", current_ymd , " .host_id =  ");
	set @sqlfull = concat(@sqlfull, " SUBSTRING_INDEX( ");
	set @sqlfull = concat(@sqlfull, " SUBSTRING_INDEX(SUBSTRING_INDEX(smpp_gw_mdr.filename, '_', 3 ), '_', -1),'host',-1) ");
	set @sqlfull = concat(@sqlfull, " WHERE SMPP_MDR_", current_ymd , " .host_id IS NULL ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	#set @sqlfull = concat("update SMS_IN_", current_ymd , " ");
	#set @sqlfull = concat(@sqlfull, " INNER JOIN SMPP_MDR_", current_ymd , " ON SMS_IN_", current_ymd , ".transaction_id = SMPP_MDR_", current_ymd , ".transaction_id ");
	#set @sqlfull = concat(@sqlfull, " set SMS_IN_", current_ymd , ".end_date = SMPP_MDR_", current_ymd , ".timestamp, ");
	#set @sqlfull = concat(@sqlfull, " SMS_IN_", current_ymd , ".trans_status = (case when (SMPP_MDR_", current_ymd , ".command_status = 0) then 'S' else 'F' end),");
	#set @sqlfull = concat(@sqlfull, " SMS_IN_", current_ymd , ".reason_code = SMPP_MDR_", current_ymd , ".command_status ");
	#set @sqlfull = concat(@sqlfull, " where SMS_IN_", current_ymd , ".end_date IS NULL AND SMS_IN_", current_ymd , ".trans_status IS NULL ");
	#set @sqlfull = concat(@sqlfull, " AND SMS_IN_", current_ymd , ".reason_code IS NULL AND SMPP_MDR_", current_ymd , ".command_id = 4;");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	#set @sqlfull = concat("update SMS_IN_", current_ymd , " set duration = TIMESTAMPDIFF(microsecond,start_date, end_date) / 1000 WHERE duration IS NULL;");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	set @sqlfull = concat("update SMS_OUT_", current_ymd , " ");
	set @sqlfull = concat(@sqlfull, " INNER JOIN SMPP_MDR_", current_ymd , " ON SMS_OUT_", current_ymd , ".message_id = SMPP_MDR_", current_ymd , ".message_id ");
	set @sqlfull = concat(@sqlfull, " set  ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".data_coding_scheme = SMPP_MDR_", current_ymd , ".data_coding_scheme, ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".submission_date = SMPP_MDR_", current_ymd , ".timestamp, ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".submission_status = (case when (SMPP_MDR_", current_ymd , ".command_status = 0) then 'S' else 'F' end), ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".priority_flag = SMPP_MDR_", current_ymd , ".priority_flag , ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".submission_reason_code = SMPP_MDR_", current_ymd , ". command_status ");
	set @sqlfull = concat(@sqlfull, " where SMS_OUT_", current_ymd , ".submission_date IS NULL AND SMS_OUT_", current_ymd , ".submission_status IS NULL  ");
	set @sqlfull = concat(@sqlfull, " AND SMS_OUT_", current_ymd , ".submission_reason_code IS NULL AND SMPP_MDR_", current_ymd , ".command_id = 4; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;



	set @sqlfull = concat("update SMS_OUT_", current_ymd , "  ");
	set @sqlfull = concat(@sqlfull, " INNER JOIN SMPP_MDR_", current_ymd , " ON SMS_OUT_", current_ymd , ".message_id = SMPP_MDR_", current_ymd , ".message_id ");
	set @sqlfull = concat(@sqlfull, " set  ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".delivery_date = SMPP_MDR_", current_ymd , ".timestamp, ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".delivery_status = (case when (SMPP_MDR_", current_ymd , ".command_status = 'DELIVRD') then 'S' else 'F' end), ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".delivery_reason_code = SMPP_MDR_", current_ymd , ". command_status ");
	set @sqlfull = concat(@sqlfull, " where SMS_OUT_", current_ymd , ".delivery_date IS NULL AND SMS_OUT_", current_ymd , ".delivery_status IS NULL AND SMS_OUT_", current_ymd , ".delivery_reason_code IS NULL AND SMPP_MDR_", current_ymd , ".command_id = 10251; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;


	#set @sqlfull = concat("update SMS_IN_", ytd_ymd , " ");
	#set @sqlfull = concat(@sqlfull, " INNER JOIN SMPP_MDR_", current_ymd , " ON SMS_IN_", ytd_ymd , ".transaction_id = SMPP_MDR_", current_ymd , ".transaction_id ");
	#set @sqlfull = concat(@sqlfull, " set SMS_IN_", ytd_ymd , ".end_date = SMPP_MDR_", current_ymd , ".timestamp, ");
	#set @sqlfull = concat(@sqlfull, " SMS_IN_", ytd_ymd , ".trans_status = (case when (SMPP_MDR_", current_ymd , ".command_status = 0) then 'S' else 'F' end),");
	#set @sqlfull = concat(@sqlfull, " SMS_IN_", ytd_ymd , ".reason_code = SMPP_MDR_", current_ymd , ".command_status ");
	#set @sqlfull = concat(@sqlfull, " where SMS_IN_", ytd_ymd , ".end_date IS NULL AND SMS_IN_", ytd_ymd , ".trans_status IS NULL ");
	#set @sqlfull = concat(@sqlfull, " AND SMS_IN_", ytd_ymd , ".reason_code IS NULL AND SMPP_MDR_", current_ymd , ".command_id = 4;");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	#set @sqlfull = concat("update SMS_IN_", ytd_ymd , " set duration = TIMESTAMPDIFF(microsecond,start_date, end_date) / 1000 WHERE duration = 0;");
	#prepare stmt from @sqlfull;
	#execute stmt;
	#deallocate prepare stmt;

	set @sqlfull = concat("update SMS_OUT_", ytd_ymd , " ");
	set @sqlfull = concat(@sqlfull, " INNER JOIN SMPP_MDR_", current_ymd , " ON SMS_OUT_", ytd_ymd , ".message_id = SMPP_MDR_", current_ymd , ".message_id ");
	set @sqlfull = concat(@sqlfull, " set  ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".data_coding_scheme = SMPP_MDR_", current_ymd , ".data_coding_scheme, ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".submission_date = SMPP_MDR_", current_ymd , ".timestamp, ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".submission_status = (case when (SMPP_MDR_", current_ymd , ".command_status = 0) then 'S' else 'F' end), ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".priority_flag = SMPP_MDR_", current_ymd , ".priority_flag , ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".submission_reason_code = SMPP_MDR_", current_ymd , ". command_status ");
	set @sqlfull = concat(@sqlfull, " where SMS_OUT_", ytd_ymd , ".submission_date IS NULL AND SMS_OUT_", ytd_ymd , ".submission_status IS NULL  ");
	set @sqlfull = concat(@sqlfull, " AND SMS_OUT_", ytd_ymd , ".submission_reason_code IS NULL AND SMPP_MDR_", current_ymd , ".command_id = 4; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;



	set @sqlfull = concat("update SMS_OUT_", ytd_ymd , "  ");
	set @sqlfull = concat(@sqlfull, " INNER JOIN SMPP_MDR_", current_ymd , " ON SMS_OUT_", ytd_ymd , ".message_id = SMPP_MDR_", current_ymd , ".message_id ");
	set @sqlfull = concat(@sqlfull, " set  ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".delivery_date = SMPP_MDR_", current_ymd , ".timestamp, ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".delivery_status = (case when (SMPP_MDR_", current_ymd , ".command_status = 'DELIVRD') then 'S' else 'F' end), ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".delivery_reason_code = SMPP_MDR_", current_ymd , ". command_status ");
	set @sqlfull = concat(@sqlfull, " where SMS_OUT_", ytd_ymd , ".delivery_date IS NULL AND SMS_OUT_", ytd_ymd , ".delivery_status IS NULL AND SMS_OUT_", ytd_ymd , ".delivery_reason_code IS NULL AND SMPP_MDR_", current_ymd , ".command_id = 10251; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

end if;

if IN_TDR_PREFIX = ismpp_gateway_mdr then


update ismpp_gw_mdr set transaction_id = (case when (substr(optional_param,1,2) = '08') then right(optional_param, length(optional_param)-2)  else '' end)  WHERE optional_param IS NOT NULL AND optional_param <> '';

	update ismpp_gw_mdr set transaction_id = substr(transaction_id, 5, cast(CONV(substr(transaction_id,1,4),16,10) as decimal) * 2) WHERE transaction_id IS NOT NULL;

	update ismpp_gw_mdr set transaction_id = CONVERT(unhex(transaction_id) USING utf8) WHERE transaction_id IS NOT NULL;

	set @sqlfull = concat("Insert into ISMPP_MDR_", current_ymd , " (timestamp,message_id,command_id,command_status,src_addr,dest_addr,dest_imsi, usr_data_length,usr_data_header_flag,user_data,data_coding_scheme,message_status,result_code,priority_flag,transaction_id, dr_enable, session_id,optional_param, discharge_time) ");
	set @sqlfull = concat(@sqlfull, " select str_to_date(concat('20', trim(timestamp), '000'), '%Y%m%d %H%i%s.%f'),MsgRefId,command_id,command_status,src_addr,dest_addr,imsi,usr_data_length,usr_data_header_flag,user_data,data_coding_scheme,message_status,result_code,priority_flag,transaction_id,'1', null, optional_param, str_to_date(concat('20', left(discharge_time,12), substr(discharge_time, 13, 3), '000'), '%Y%m%d%H%i%s%f') FROM ismpp_gw_mdr;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("UPDATE ISMPP_MDR_", current_ymd , " ");
	set @sqlfull = concat(@sqlfull, " SET ISMPP_MDR_", current_ymd , ".session_id = substr(transaction_id, 1, length(transaction_id) -2)  ");
	set @sqlfull = concat(@sqlfull, " WHERE timestamp >= date_sub(now(), interval 1 day) and ISMPP_MDR_", current_ymd , ".session_id IS NULL and length(transaction_id) >= 27 and transaction_id not like '%:%' and transaction_id not like '%+%' ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("UPDATE ISMPP_MDR_", current_ymd , " ");
	set @sqlfull = concat(@sqlfull, " SET ISMPP_MDR_", current_ymd , ".session_id = transaction_id ");
	set @sqlfull = concat(@sqlfull, " WHERE timestamp >= date_sub(now(), interval 1 day) and ISMPP_MDR_", current_ymd , ".session_id IS NULL");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("UPDATE ISMPP_MDR_", current_ymd , "  INNER JOIN ");
	set @sqlfull = concat(@sqlfull, " ismpp_gw_mdr ON ISMPP_MDR_", current_ymd , " .message_id = ismpp_gw_mdr.MsgRefId AND ");
	set @sqlfull = concat(@sqlfull, " ISMPP_MDR_", current_ymd , " .command_id = ismpp_gw_mdr.command_id ");
	set @sqlfull = concat(@sqlfull, " SET ISMPP_MDR_", current_ymd , " .host_id =  ");
	set @sqlfull = concat(@sqlfull, " SUBSTRING_INDEX( ");
	set @sqlfull = concat(@sqlfull, " SUBSTRING_INDEX(SUBSTRING_INDEX(ismpp_gw_mdr.filename, '_', 3 ), '_', -1),'host',-1) ");
	set @sqlfull = concat(@sqlfull, " WHERE ISMPP_MDR_", current_ymd , " .host_id IS NULL ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("update SMS_IN_", current_ymd , " ");
	set @sqlfull = concat(@sqlfull, " INNER JOIN ISMPP_MDR_", current_ymd , " ON SMS_IN_", current_ymd , ".transaction_id = ISMPP_MDR_", current_ymd , ".transaction_id ");
	set @sqlfull = concat(@sqlfull, " SET SMS_IN_", current_ymd , ".end_date = ISMPP_MDR_", current_ymd , ".discharge_time,");
	set @sqlfull = concat(@sqlfull, " SMS_IN_", current_ymd , ".trans_status = (case when (ISMPP_MDR_", current_ymd , ".message_status = 'DELIVRD') then 'S' else 'F' end),");
	set @sqlfull = concat(@sqlfull, " SMS_IN_", current_ymd , ".reason_code = ISMPP_MDR_", current_ymd , ".message_status ");
	set @sqlfull = concat(@sqlfull, " where SMS_IN_", current_ymd , ".msisdn =  ISMPP_MDR_", current_ymd , ".dest_addr ");
	set @sqlfull = concat(@sqlfull, " AND SMS_IN_", current_ymd , ".transaction_id = ISMPP_MDR_", current_ymd , ".transaction_id and ISMPP_MDR_", current_ymd , ".command_id = 5;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("update SMS_IN_", current_ymd , " set duration = TIMESTAMPDIFF(microsecond,start_date, end_date) / 1000 WHERE duration = 0 and end_date is not null;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("update SMS_OUT_", current_ymd , "  ");
	set @sqlfull = concat(@sqlfull, " INNER JOIN ISMPP_MDR_", current_ymd , " ON SMS_OUT_", current_ymd , ".message_id = ISMPP_MDR_", current_ymd , ".message_id ");
	set @sqlfull = concat(@sqlfull, " set  ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".delivery_date = ISMPP_MDR_", current_ymd , ".discharge_time, ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".delivery_status = (case when (ISMPP_MDR_", current_ymd , ".message_status = 'DELIVRD') then 'S' else 'F' end), ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", current_ymd , ".delivery_reason_code = ISMPP_MDR_", current_ymd , ". message_status ");
	set @sqlfull = concat(@sqlfull, " where SMS_OUT_", current_ymd , ".delivery_date IS NULL AND SMS_OUT_", current_ymd , ".delivery_status IS NULL AND SMS_OUT_", current_ymd , ".delivery_reason_code IS NULL AND ISMPP_MDR_", current_ymd , ".command_id = 5; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;



	set @sqlfull = concat("update SMS_IN_", ytd_ymd , " ");
	set @sqlfull = concat(@sqlfull, " INNER JOIN ISMPP_MDR_", current_ymd , " ON SMS_IN_", ytd_ymd , ".transaction_id = ISMPP_MDR_", current_ymd , ".transaction_id ");
	set @sqlfull = concat(@sqlfull, " SET SMS_IN_", ytd_ymd , ".trans_status = (case when (ISMPP_MDR_", current_ymd , ".message_status = 'DELIVRD') then 'S' else 'F' end),");
	set @sqlfull = concat(@sqlfull, " SMS_IN_", ytd_ymd , ".reason_code = ISMPP_MDR_", current_ymd , ".message_status ");
	set @sqlfull = concat(@sqlfull, " where SMS_IN_", ytd_ymd , ".msisdn =  ISMPP_MDR_", current_ymd , ".dest_addr ");
	set @sqlfull = concat(@sqlfull, " AND SMS_IN_", ytd_ymd , ".transaction_id = ISMPP_MDR_", current_ymd , ".transaction_id and ISMPP_MDR_", current_ymd , ".command_id = 5;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("update SMS_IN_", ytd_ymd , " set duration = TIMESTAMPDIFF(microsecond,start_date, end_date) / 1000 WHERE  duration = 0 and end_date is not null;");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;

	set @sqlfull = concat("update SMS_OUT_", ytd_ymd , "  ");
	set @sqlfull = concat(@sqlfull, " INNER JOIN ISMPP_MDR_", current_ymd , " ON SMS_OUT_", ytd_ymd , ".message_id = ISMPP_MDR_", current_ymd , ".message_id ");
	set @sqlfull = concat(@sqlfull, " set  ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".delivery_date = ISMPP_MDR_", current_ymd , ".discharge_time, ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".delivery_status = (case when (ISMPP_MDR_", current_ymd , ".message_status = 'DELIVRD') then 'S' else 'F' end), ");
	set @sqlfull = concat(@sqlfull, " SMS_OUT_", ytd_ymd , ".delivery_reason_code = ISMPP_MDR_", current_ymd , ". message_status ");
	set @sqlfull = concat(@sqlfull, " where SMS_OUT_", ytd_ymd , ".delivery_date IS NULL AND SMS_OUT_", ytd_ymd , ".delivery_status IS NULL AND SMS_OUT_", ytd_ymd , ".delivery_reason_code IS NULL AND ISMPP_MDR_", current_ymd , ".command_id = 5; ");
	prepare stmt from @sqlfull;
	execute stmt;
	deallocate prepare stmt;



end if;

end
