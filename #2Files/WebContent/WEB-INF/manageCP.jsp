<?xml version="1.0" encoding="UTF-8"?>
<ui:composition template="/templates/master.xhtml" xmlns="http://www.w3.org/1999/xhtml"
	xmlns:f="http://java.sun.com/jsf/core" xmlns:h="http://java.sun.com/jsf/html"
	xmlns:ui="http://java.sun.com/jsf/facelets" xmlns:p="http://primefaces.org/ui"
	xmlns:c="http://java.sun.com/jsp/jstl/core">

	<ui:define name="title">
		#{msg['main.title']} - #{msg['managecp.users']}
	</ui:define>

	<ui:define name="pageTitle">
		#{msg['managecp.users']}
	</ui:define>

	<ui:define name="content">
		<p:growl id="growl" showDetail="false" globalOnly="false" autoUpdate="true"/>

		<h:form id="userForm">
			<p:panel >
				<f:facet name="header">
					<h:outputText value="#{msg['managecp.users']}" styleClass="tableHeader" />
				</f:facet>
				<p:dataTable var="consoleUser" value="#{ManageCPController.consoleUsers}" paginatorPosition="bottom"
					paginator="true" rows="10" rowsPerPageTemplate="10,20,50" paginatorAlwaysVisible="false">

					<p:column styleClass="colAddBtn" >
						<f:facet name="header">
							<p:commandLink title="#{msg['general.add']}"
									styleClass="ui-icon ui-icon-plusthick"
									process="@this"	 action="#{ManageCPController.addUser}"
									update=":userForm :editForm"
									oncomplete="editDialog.show();"
									rendered="#{UserBean.accessLevel == 'WRITE'}">
								<f:setPropertyActionListener target="#{ManageCPController.createNew}" value="#{true}" />
							</p:commandLink>
						</f:facet>
					</p:column>

					<p:column filterBy="#{consoleUser.userId}" filterMatchMode="contains" headerText="#{msg['managecp.channelid']}" styleClass="userSearchColumn">
						<h:outputText value="#{consoleUser.userId}" />
					</p:column>
					<p:column filterBy="#{consoleUser.name}" filterMatchMode="contains" headerText="#{msg['managecp.channel']}" styleClass="userSearchColumn">
						<h:outputText value="#{consoleUser.name}" />
					</p:column>
					<p:column filterBy="#{consoleUser.group.groupId}" filterMatchMode="contains" headerText="#{msg['managecp.userid']}" styleClass="userSearchColumn">
						<h:outputLink value="#{request.contextPath}/pages/user/group.xhtml">
							<f:param name="selectedGroupId" value="#{consoleUser.group.groupId}"/>
							<h:outputText value="#{consoleUser.group.groupId}" />
						</h:outputLink>
					</p:column>
					<p:column headerText="#{msg['managecp.priority']}" styleClass="alarm-white" sortBy="#{entity.status}"
					filterBy="#{entity.status}"
					filterOptions="#{alarmManagementController.statusSelectItems}"
					style="width:42px">
						<h:outputText value="#{consoleUser.department}" styleClass="#{alarmManagementController.getStatusColor(entity.status)}" />
					</p:column>
					<p:column headerText="#{msg['privilege.status']}" styleClass="alarm-white" sortBy="#{entity.status}"
					filterBy="#{entity.status}"
					filterOptions="#{alarmManagementController.statusSelectItems}"
					style="width:42px">
						<h:outputText value="#{consoleUser.location}" styleClass="#{alarmManagementController.getStatusColor(entity.status)}" />
					</p:column>

					<p:column style="width:50px" headerText="#{msg['managecp.actions']}" rendered="#{UserBean.getAccessLevel() == 'WRITE'}" styleClass="userSearchColumn">
						<!-- c:choose isn't working within here... so, ugly workarounds -->
						<h:panelGroup>
<!-- 							<p:button id="view" icon="ui-icon-search" styleClass="smallButton" outcome="groupsearch.xhtml"> -->
<!-- 								<f:param name="selectedGroup(userGroup)" value="#{consoleUser.group.groupId}" /> -->
<!-- 							</p:button> -->
							<p:commandLink style="float:right" styleClass="ui-icon ui-icon-trash" action="#{ManageCPController.selectUser(consoleUser)}"
								onclick="confirmDelete.show()"
								process="@this"
								title="#{msg['general.delete']}"
								disabled="#{! ManageCPController.isUserDeletable(consoleUser)}"/>
							<p:commandLink style="float:right" styleClass="ui-icon ui-icon-pencil" action="#{ManageCPController.selectUser(consoleUser)}"
								update=":editDialog1"
								process="@this"
								oncomplete="editDialog.show();" title="#{msg['general.edit']}"
								disabled="#{! ManageCPController.isUserEditable(consoleUser)}" >
								<f:setPropertyActionListener target="#{ManageCPController.createNew}" value="#{false}" />
							</p:commandLink>
						</h:panelGroup>
					</p:column>
				</p:dataTable>
			</p:panel>
		</h:form>

		<h:form id="deleteForm">
			<p:confirmDialog header="#{msg['general.delete']}" id="confirmDelete1" modal="true" widgetVar="confirmDelete" message="#{msg['general.confirmPrompt']}">
				<p:commandButton value="#{msg['general.yes']}" actionListener="#{ManageCPController.deleteUser}"
					oncomplete="confirmDelete.hide()" update=":userForm"/>
				<p:commandButton value="#{msg['general.no']}" oncomplete="confirmDelete.hide()"/>
			</p:confirmDialog>
		</h:form>

		<p:dialog id="editDialog1" header="#{msg['general.addEdit']}"
			rendered="#{UserBean.accessLevel == 'WRITE'}"
			visable="#{ManageCPController.consoleUserEmpty}"
			modal="true" widgetVar="editDialog" resizable="false">
			<h:form id="editForm">
				<h:panelGrid id="editTable" columns="2">
					<p:outputLabel for="edUserId" value="#{msg['managecp.userid']}:"/>
					<p:inputText id="edUserId"
						value="#{ManageCPController.selectedUser.userId}"
						disabled="#{! empty ManageCPController.selectedUser.userId}" required="true"
						requiredMessage="#{msg['user.prompt.userid']}" />
					<p:outputLabel for="edUserName" value="#{msg['managecp.name']}:" />
					<p:inputText id="edUserName"
						value="#{ManageCPController.selectedUser.name}" required="true"
						requiredMessage="#{msg['user.prompt.username']}"/>

					<ui:remove>
						<p:outputLabel for="edPassword"  value="#{msg['managecp.password.current']}:" />
						<p:password id="edPassword"
							value="#{ManageCPController.passwordCurrent}" redisplay="true"
							title="#{msg['user.prompt.no_password_change']}"
							disabled="#{ManageCPController.createNew}"/>
					</ui:remove>

					<p:outputLabel for="edPassword1" value="#{msg['managecp.password']}:" />
					<p:password id="edPassword1"
						value="#{ManageCPController.password}" redisplay="true" required="#{ManageCPController.selectedUser.password == null}" requiredMessage="#{msg['user.prompt.password']}"
						title="#{msg['user.prompt.no_password_change']}"/>

					<ui:remove>
						<h:outputText value="#{msg['managecp.password.reenter']}:" />
						<p:password
							value="#{ManageCPController.passwordReenter}" redisplay="true"
							id="password2"
							title="#{msg['user.prompt.no_password_change']}"/>
					</ui:remove>

					<p:outputLabel value="#{msg['managecp.department']}:"/>
					<p:inputText value="#{ManageCPController.selectedUser.department}" />

					<p:outputLabel value="#{msg['managecp.location']}:"/>
					<p:inputText value="#{ManageCPController.selectedUser.location}" />

					<p:outputLabel for="edStatus" value="#{msg['managecp.status']}:" />
					<h:selectOneMenu id="edStatus" value="#{ManageCPController.selectedUser.status}" required="true">
							<f:selectItems value="#{ManageCPController.statuses}"/>
					</h:selectOneMenu>

					<p:outputLabel for="edUserGroup" value="#{msg['managecp.usergroup']}:" />
					<h:panelGrid columns="2">
						<h:selectOneMenu id="edUserGroup" value="#{ManageCPController.selectedUserGroupId}" required="true" requiredMessage="#{msg['user.prompt.usergroup']}">
							<f:selectItem itemLabel="#{msg['managecp.group.select']}" itemValue="" />
							<f:selectItems value="#{ManageCPController.userGroupIds}" />
						</h:selectOneMenu>
		    			<p:commandLink title="#{msg['general.add']} Group" immediate="true" action="#{ManageCPController.onAddGroupClick}"
		    			rendered="#{UserBean.accessLevel == 'WRITE'}"
		    			styleClass="ui-icon ui-icon-circle-plus" />
					</h:panelGrid>
				</h:panelGrid>
				<br />
				<p:commandButton value="#{msg['general.save']}"
					action="#{ManageCPController.saveUser}" process="editForm, @this"
					oncomplete="handleAddEditRequest(xhr, status, args)" update=":userForm :editForm" />
			</h:form>
		</p:dialog>

		<script type="text/javascript">
		    function handleAddEditRequest(xhr, status, args) {
		        if(args.validationFailed) {
		        	editDialog.show();
		        } else {
		        	editDialog.hide();
		        }
		    }
		</script>

	</ui:define>

</ui:composition>
