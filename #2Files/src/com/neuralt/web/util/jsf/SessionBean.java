package com.neuralt.web.util.jsf;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.web.AppConfig;

@ManagedBean(name = "sessionBean")
@SessionScoped
public class SessionBean implements Serializable {
	private static final long serialVersionUID = -4376158576986654972L;
	private static final Map<String, String> localeMap;
	private String locale;
	private static final Logger logger = LoggerFactory
			.getLogger(SessionBean.class);

	private String sessionid;

	static {
		localeMap = new HashMap<String, String>();
		localeMap.put("中文", "zh_TW");
		localeMap.put("English", "en");
	}

	public boolean isInSession() {
		return sessionid != null;
	}

	public SessionBean() {
		Locale defaultLocale = FacesContext.getCurrentInstance()
				.getApplication().getDefaultLocale();
		if (defaultLocale.getCountry() != null
				&& defaultLocale.getCountry().length() > 0)
			locale = defaultLocale.getLanguage() + "_"
					+ defaultLocale.getCountry();
		else
			locale = defaultLocale.getLanguage();

	}

	public void countryLocaleCodeChanged(AjaxBehaviorEvent e) {
		logger.debug("Backing bean action listener: countryLocaleCodeChanged(valueChangeEvent)");

		// locale = e.getNewValue().toString();
		UIInput input = (UIInput) e.getComponent();
		locale = (String) input.getValue();
		FacesContext.getCurrentInstance().renderResponse();
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public Map<String, String> getLocaleMap() {
		return localeMap;
	}

	public String getWebDateFormat() {
		return AppConfig.getWebDateFormat();
	}
}
