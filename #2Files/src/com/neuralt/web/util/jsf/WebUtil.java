package com.neuralt.web.util.jsf;

import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.web.security.ConsoleUser;
import com.neuralt.smp.client.web.security.UserBean;
import com.neuralt.smp.util.StringUtil;

public class WebUtil {

	private static final Logger logger = LoggerFactory.getLogger(WebUtil.class);

	/**
	 * This method accepts a key for search a respective value in
	 * messages.properties for current user locale. If nothing is found, it will
	 * return the input, ie. key. It can be used as an entry point for adding
	 * messages or logging.
	 * 
	 * @param key
	 *            the key in messages.properties
	 * @return
	 */
	public static String getMessage(String key) {
		String result = null;
		try {
			result = getMsgBundle().getString(key);
		} catch (Exception ignore) {
		}
		if (result == null) {
			result = key;
		}
		return result;
	}

	public static ResourceBundle getMsgBundle() {
		// logger.debug("currentInstance"+FacesContext.getCurrentInstance());
		// logger.debug("viewRoot"+FacesContext.getCurrentInstance().getViewRoot());
		// logger.debug("locale"+FacesContext.getCurrentInstance().getViewRoot().getLocale());

		// when there is no web request (ie. the starting of tomcat), the faces
		// context will be null
		// so we need to manually assign a locale to prevent NPE

		Locale locale = null;
		if (FacesContext.getCurrentInstance() != null) {
			locale = FacesContext.getCurrentInstance().getViewRoot()
					.getLocale();
			return ResourceBundle.getBundle(
					"com.neuralt.web.util.UTF8ResourceBundle", locale);
		}
		return ResourceBundle
				.getBundle("com.neuralt.web.util.UTF8ResourceBundle");
	}

	public static String getRequestPath() {
		return FacesContext.getCurrentInstance().getExternalContext()
				.getRequestServletPath();
	}

	public static String getRemoteIp() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		return getRemoteIp(request);
	}

	public static String getRemoteIp(final HttpServletRequest request) {
		final Enumeration<String> headers = request
				.getHeaders("X-Forwarded-For");
		if (headers == null) {
			System.out.println("no access to headers");
		} else {
			while (headers.hasMoreElements()) {
				final String[] ips = headers.nextElement().split(",");
				for (int i = 0; i < ips.length; i++) {
					final String proxy = ips[i].trim();
					if (!proxy.isEmpty() && !proxy.equals("unknown")) {
						return proxy;
					}
				}
			}
		}
		return request.getRemoteAddr();
	}

	public static String getUserId(HttpServletRequest request) {
		UserBean userBean = (UserBean) findBean(UserBean.class);
		if (userBean == null || userBean.getCurrentUserId() == null) {
			return null;
		}
		return userBean.getCurrentUserId();
	}

	public static String getCurrentUserId() {
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		return getUserId(request);
	}

	public static ConsoleUser getCurrentUser() {
		UserBean userBean = (UserBean) findBean(UserBean.class);
		if (userBean == null || userBean.getCurrentUserId() == null) {
			return null;
		}
		return userBean.getUser();
	}

	public static <T> T findBean(Class<T> beanClass) {
		FacesContext context = FacesContext.getCurrentInstance();
		return (T) context.getApplication().evaluateExpressionGet(context,
				"#{" + beanClass.getSimpleName() + "}", Object.class);
	}

	public static void addGlobalMsgByKey(FacesMessage.Severity severity,
			String msgKey) {
		String msg = WebUtil.getMsgBundle().getString(msgKey);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, msg, msg));
	}

	public static void addGlobalMsg(FacesMessage.Severity severity,
			String summary, String detail) {
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, summary, detail));
	}

	public static void addGlobalMsgByKey(FacesMessage.Severity severity,
			String summaryKey, String detailKey) {
		String summary = WebUtil.getMsgBundle().getString(summaryKey);
		String detail = WebUtil.getMsgBundle().getString(detailKey);
		FacesContext.getCurrentInstance().addMessage(null,
				new FacesMessage(severity, summary, detail));
	}

	/**
	 * This method is used as a wrapper for producing messages on web page. If
	 * the input message is a key from messages.properties file, it can
	 * automatically translate it into the respective value from current user
	 * locale.
	 * 
	 * @param summary
	 *            the message summary, can be an actual sentence, a key in
	 *            messages.properties file or null
	 * @param detail
	 *            the message detail
	 * @param severity
	 *            level of severity
	 */
	public static void addMessage(String summary, String detail,
			Severity severity) {
		FacesMessage message = new FacesMessage();
		if (StringUtil.isNullOrEmpty(summary)) {
			summary = "";
		} else {
			summary = WebUtil.getMessage(summary);
		}
		message.setSummary(summary);
		if (StringUtil.isNullOrEmpty(detail)) {
			detail = "";
		} else {
			detail = WebUtil.getMessage(detail);
		}
		message.setDetail(detail);
		message.setSeverity(severity);
		if (FacesContext.getCurrentInstance() != null) {
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}

	/**
	 * Just another version with order changed of the main addMessage
	 * 
	 * @param severity
	 * @param summary
	 * @param detail
	 */
	public static void addMessage(Severity severity, String summary,
			String detail) {
		addMessage(summary, detail, severity);
	}
}
