package com.neuralt.web.util.jsf;

import org.hibernate.StaleObjectStateException;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.exception.JDBCConnectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccessException;

public class MessageUtil {

	private static final Logger logger = LoggerFactory
			.getLogger(MessageUtil.class);

	public static String getDescriptionOfDBException(Exception e) {
		String message;
		if (e instanceof DataAccessException) {
			Throwable cause = e.getCause();
			if (cause instanceof StaleObjectStateException) {
				message = LocaleMessages.getString("db.error.concurrentAccess");
				return message;
			} else if (cause instanceof ConstraintViolationException) {
				message = LocaleMessages
						.getString("db.error.integrityViolation");
				return message;
			} else if (cause instanceof JDBCConnectionException) {
				message = LocaleMessages
						.getString("db.error.connectionException");
				return message;
			}
		}
		message = LocaleMessages.getString("db.error.other");
		return message;
	}

	public static void logDBException(String description, Exception e) {
		// TODO FIXME different handling for each type of exception
		logger.error(description, e);
	}
}
