package com.neuralt.web.util.jsf;

import java.io.Serializable;
import java.util.Locale;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class LocaleController implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4310560941716217433L;
	private String languageCode;

	public LocaleController() {
		languageCode = FacesContext.getCurrentInstance().getExternalContext()
				.getRequestLocale().toString();
	}

	public String changeLocale() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getViewRoot().setLocale(new Locale(languageCode));
		return null;
	}

	public void setLanguageCode(String newValue) {
		languageCode = newValue;
	}

	public String getLanguageCode() {
		return languageCode;
	}
}
