package com.neuralt.web.util.jsf;

import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.context.FacesContext;

import com.neuralt.web.util.UTF8Control;

public class UTF8FacesResourceBundle extends ResourceBundle {
	public static final String BUNDLE_NAME = "lang_pack.messages";
	public static final String BUNDLE_EXTENSION = "properties";

	public UTF8FacesResourceBundle() {
		setParent(FacesContext.getCurrentInstance().getViewRoot().getLocale());
	}

	public UTF8FacesResourceBundle(Locale locale) {
		setParent(locale);
	}

	private void setParent(Locale locale) {
		setParent(ResourceBundle.getBundle(BUNDLE_NAME, locale, Thread
				.currentThread().getContextClassLoader(), new UTF8Control(
				BUNDLE_EXTENSION)));
	}

	@Override
	protected Object handleGetObject(String key) {
		try {
			return parent.getObject(key);
		} catch (Exception e) {
			return "???" + key + "???";
		}
	}

	@Override
	public Enumeration<String> getKeys() {
		return parent.getKeys();
	}
}
