package com.neuralt.web.util.jsf;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("textTrimConverter")
public class TextTrimConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext ctx, UIComponent uic, String str) {
		if (str == null)
			return null;
		return str.trim();
	}

	@Override
	public String getAsString(FacesContext ctx, UIComponent uic, Object obj) {
		if (obj == null)
			return "";
		return obj.toString();
	}

}
