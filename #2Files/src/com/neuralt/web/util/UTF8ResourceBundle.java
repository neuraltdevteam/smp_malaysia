package com.neuralt.web.util;

import java.util.Enumeration;
import java.util.Locale;
import java.util.ResourceBundle;

public class UTF8ResourceBundle extends ResourceBundle {
	public static final String BUNDLE_NAME = "lang_pack.messages";
	public static final String BUNDLE_EXTENSION = "properties";

	public UTF8ResourceBundle() {
		setParent(ResourceBundle.getBundle(BUNDLE_NAME, Locale.getDefault(),
				this.getClass().getClassLoader(), new UTF8Control(
						BUNDLE_EXTENSION)));
	}

	@Override
	protected Object handleGetObject(String key) {
		try {
			return parent.getObject(key);
		} catch (Exception e) {
			// return "???" + key + "???";
			// return original
			return key;
		}
	}

	@Override
	public Enumeration<String> getKeys() {
		return parent.getKeys();
	}
}
