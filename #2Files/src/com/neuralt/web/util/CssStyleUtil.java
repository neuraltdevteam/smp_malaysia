package com.neuralt.web.util;

import java.util.HashMap;
import java.util.Map;

import com.neuralt.smp.client.MonitoredHost.HostStatus;
import com.neuralt.smp.config.ProcessStatus;
import com.neuralt.smp.notification.WarningLevel;

public class CssStyleUtil {

	private static Map<String, String> statusCssMap = createStatusCssMap();

	public static String getStatusCssClass(String statusText) {
		String result = statusCssMap.get(statusText);
		if (result == null)
			result = "";

		return result;
	}

	private static Map<String, String> createStatusCssMap() {
		String green = "status-green";
		String red = "status-red";
		String gray = "status-gray";
		String orange = "status-orange";

		Map<String, String> map = new HashMap<String, String>();
		map.put(HostStatus.CONNECTED.name(), green);
		map.put(HostStatus.DISCONNECTED.name(), red);
		map.put(ProcessStatus.STARTED.name(), green);
		map.put(ProcessStatus.STOPPED.name(), red);
		map.put(ProcessStatus.DIED.name(), red);
		map.put(ProcessStatus.RESTARTED.name(), red);
		map.put(ProcessStatus.STARTING.name(), gray);
		map.put(ProcessStatus.STOPPING.name(), gray);
		map.put(WarningLevel.CLEAR.name(), green);
		map.put(WarningLevel.MINOR.name(), red);
		map.put(WarningLevel.MAJOR.name(), red);
		map.put(WarningLevel.WARNING.name(), orange);
		map.put(WarningLevel.ERROR.name(), red);
		map.put(WarningLevel.FATAL.name(), red);
		return map;
	}
}
