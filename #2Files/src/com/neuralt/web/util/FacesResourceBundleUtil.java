package com.neuralt.web.util;

import java.util.Locale;
import java.util.ResourceBundle;

import com.neuralt.web.util.jsf.UTF8FacesResourceBundle;

public class FacesResourceBundleUtil {

	public static ResourceBundle getMessageBundle() {
		return new UTF8FacesResourceBundle();
	}

	public static ResourceBundle getMessageBundle(Locale locale) {
		return new UTF8FacesResourceBundle(locale);
	}

	public static String getMessage(String key) {
		String msg = getMessageBundle().getString(key);
		return msg;
	}

}