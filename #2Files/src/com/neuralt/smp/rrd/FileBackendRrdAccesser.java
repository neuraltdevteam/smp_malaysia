package com.neuralt.smp.rrd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.rrd4j.ConsolFun;
import org.rrd4j.DsType;
import org.rrd4j.core.FetchData;
import org.rrd4j.core.FetchRequest;
import org.rrd4j.core.RrdDb;
import org.rrd4j.core.RrdDef;
import org.rrd4j.core.Sample;
import org.rrd4j.graph.RrdGraph;
import org.rrd4j.graph.RrdGraphDef;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataPoint;
import com.neuralt.smp.client.data.DataWrapper;
import com.neuralt.smp.client.data.RrdArchiveConfig;
import com.neuralt.smp.client.data.RrdArchiveConfig.ArchiveType;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.client.task.FileDeleteTask;
import com.neuralt.smp.client.util.ColorBox;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;

public class FileBackendRrdAccesser implements RrdAccessInterface {

	private static final Logger logger = LoggerFactory
			.getLogger(FileBackendRrdAccesser.class);

	public static FileBackendRrdAccesser getInstance() {
		if (instance == null) {
			synchronized (FileBackendRrdAccesser.class) {
				if (instance == null)
					instance = new FileBackendRrdAccesser();
			}
		}
		return instance;
	}

	private static FileBackendRrdAccesser instance;

	private FileBackendRrdAccesser() {

	}

	public void update(final String path, final List<String> filters,
			final DataWrapper wrapper) throws IOException,
			IllegalArgumentException {
		List<DataWrapper> wrappers = new ArrayList<DataWrapper>();
		wrappers.add(wrapper);
		update(path, filters, wrappers);
	}

	public void update(final String path, final List<String> filters,
			final List<DataWrapper> wrappers) throws IOException,
			IllegalArgumentException {
		RrdDb db = null;
		try {
			db = new RrdDb(path);
			for (DataWrapper wrapper : wrappers) {
				Sample sample = db.createSample(wrapper.getTime()
						/ ConstantUtil.SEC_MS);
				// as rrd4j time stamp is counted in second
				for (DataPoint dp : wrapper.getPoints()) {
					if (filters != null) {
						if (!filters.contains(dp.getName())) {
							sample.setValue(dp.getName(), dp.getValue());
						}
					} else {
						sample.setValue(dp.getName(), dp.getValue());
					}
				}
				sample.update();
			}
		} catch (IOException e) {
			logger.warn("update RRD failed ", e.getMessage());
			throw e;
		} catch (IllegalArgumentException e) {
			String message = e.getMessage();
			logger.error("unexpected DS occured", e);
			// we have no precise information on which DS is source of error -
			// have to process the message string like this
			// the string is in the form "Datasource *DS* not found" in RRD4J
			// 2.1.1
			// XXX if RRD4J version changed, have to confirm the message again
			// String truncated = message.replace("Datasource ",
			// "").replace(" not found", "");
			String truncated = message.substring(11);
			truncated = truncated.substring(0, truncated.length() - 10);
			throw new IllegalArgumentException(truncated);
		} finally {
			if (db != null) {
				try {
					db.close();
				} catch (IOException e) {
					logger.error("Failed to close RrdDb.", e);
					throw e;
				}
			}
		}
	}

	public void update(final RrdDefConfig def, final DataWrapper wrapper)
			throws IOException {
		List<DataWrapper> wrappers = new ArrayList<DataWrapper>();
		wrappers.add(wrapper);
		update(def, wrappers);
	}

	/**
	 * Batch version of update
	 */
	public void update(final RrdDefConfig def, final List<DataWrapper> wrappers)
			throws IOException {
		RrdDb db = null;
		try {
			db = new RrdDb(def.getPath());
			for (DataWrapper wrapper : wrappers) {
				Sample sample = db.createSample(wrapper.getTime()
						/ ConstantUtil.SEC_MS);
				// as rrd4j time stamp is counted in second
				for (DataPoint dp : wrapper.getPoints()) {
					// logger.debug("updating "+def.getName()+"....ds="+dp.getName()+", value="+dp.getValue());
					sample.setValue(dp.getName(), dp.getValue());
				}
				sample.update();
			}
		} catch (IOException e) {
			logger.warn("update RRD failed ", e.getMessage());
			throw e;
		} catch (IllegalArgumentException e) {
			throw e;
		} finally {
			if (db != null) {
				try {
					db.close();
				} catch (IOException e) {
					logger.error("Failed to close RrdDb.", e);
					throw e;
				}
			}
			logger.debug(def.getName() + ": acceptCount="
					+ def.getAcceptCount() + ", rejectCount="
					+ def.getRejectCount() + ", acceptRatio="
					+ def.getAcceptRatio() + ", runningTime="
					+ def.getRunningTime());
		}
	}

	public void create(final RrdDefConfig def) throws IOException {
		RrdDef rrdDef = null;
		RrdDb rrdDb = null;

		try {
			rrdDef = new RrdDef(def.getPath(), def.getStep());
			for (RrdDsConfig ds : def.getRrdDsConfigs()) {
				logger.debug("adding ds:" + ds);
				DsType type = RrdUtil.translateDsType(ds.getType());
				rrdDef.addDatasource(ds.getName(), type, ds.getHeartbeat(),
						ds.getMinimum(), ds.getMaximum());
			}
			for (RrdArchiveConfig archive : def.getRrdArchiveConfigs()) {
				logger.debug("adding archive:" + archive);
				ConsolFun type = RrdUtil
						.translateArchiveType(archive.getType());
				rrdDef.addArchive(type, archive.getXff(), archive.getStep(),
						archive.getRow());
			}
			logger.debug("checking the new rrdDef...");
			logger.debug(rrdDef.dump());
			rrdDb = new RrdDb(rrdDef);
			if (rrdDb.getRrdDef().equals(rrdDef)) {
				logger.info("RRD DB is successfully created in {}",
						def.getPath());
			} else {
				logger.warn("Invalid RRD file created in {}", def.getPath());
				logger.warn("The invaild file should be deleted after 5 minutes...");
				FileDeleteTask task = new FileDeleteTask(def.getPath());
				task.start();
			}
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			// see what other exception will be thrown
			logger.debug("failed in creating RRD DB", e);
		} finally {
			if (rrdDb != null)
				rrdDb.close();
		}
	}

	public String generateGraph(Date from, Date to, ArchiveType type, int step,
			String title, String vLabel, RrdDefConfig def) throws IOException {
		// generating graph path
		// as rrd4j cannot produce a byte array stream to the front end
		StringBuilder sb = new StringBuilder();
		sb.append(AppConfig.getTempDir()).append(AppConfig.getRrdGraphPrefix())
				.append(def.getName()).append("_")
				.append(StringUtil.convertTimeFormatForExport(new Date()))
				.append(".png");
		String graphPath = sb.toString();
		// setting graph definition
		RrdGraphDef gDef = new RrdGraphDef();
		gDef.setFilename(graphPath);
		gDef.setStartTime(from.getTime() / ConstantUtil.SEC_MS);
		gDef.setEndTime(to.getTime() / ConstantUtil.SEC_MS);
		gDef.setStep(step);
		gDef.setTitle(title);
		gDef.setHeight(700);
		gDef.setWidth(900);
		gDef.setVerticalLabel(vLabel);

		ColorBox box = ColorBox.createColorBox();
		String dbPath = def.getPath();
		ConsolFun cf = RrdUtil.translateArchiveType(type);

		for (RrdDsConfig ds : def.getRrdDsConfigs()) {
			gDef.datasource(ds.getName(), dbPath, ds.getName(), cf);
			gDef.line(ds.getName(), box.nextColor(), ds.getName());
		}

		// gDef.setImageInfo("<img src='%s' width='%d' height = '%d'>");
		gDef.setPoolUsed(false);
		gDef.setImageFormat("png");
		// create graph finally

		RrdGraph graph = new RrdGraph(gDef);
		if (logger.isDebugEnabled())
			logger.debug("graph dump:" + graph.getRrdGraphInfo().dump());
		return graphPath;
	}

	public List<DataWrapper> fetchData(Date from, Date to, long step,
			ArchiveType type, RrdDefConfig def) throws IOException {
		// generating request
		// logger.debug("starting to fetch data");
		ConsolFun cf = RrdUtil.translateArchiveType(type);
		RrdDb rrdDb = new RrdDb(def.getPath(), true);
		// logger.debug("fetching db at:"+def.getPath());
		FetchRequest request = rrdDb
				.createFetchRequest(cf, from.getTime() / ConstantUtil.SEC_MS,
						to.getTime() / ConstantUtil.SEC_MS, step);
		FetchData fetchData = request.fetchData();
		// logger.debug("fetchData="+fetchData.dump());
		if (fetchData.getArcStep() != step) {
			// TODO
			// investigating if this is a bug of rrd4j
			// the param calculation is correct, however rrd4j cannot get the
			// archive requested
			// it will return an archive which the step may be smaller or
			// greater than the required one, ie we have no idea about how it
			// could tend to
			logger.debug("analyzing error...from=" + from.getTime() + ", to="
					+ to.getTime());
			logger.debug("def=" + def.getName() + ", archive step="
					+ fetchData.getArcStep() + ", myStep=" + step);

			logger.debug("request=" + request.dump());
		}
		// fitting data into datawrapper
		List<DataWrapper> list = new ArrayList<DataWrapper>();
		long[] timestamps = fetchData.getTimestamps();
		for (RrdDsConfig ds : def.getRrdDsConfigsAsList()) {
			DataWrapper wrapper = new DataWrapper();
			wrapper.setPoints(new ArrayList<DataPoint>());
			double[] data = fetchData.getValues(ds.getName());
			for (int i = 0; i < data.length; i++) {
				DataPoint dp = new DataPoint(ds.getName(), data[i]);
				dp.setTime(timestamps[i] * ConstantUtil.SEC_MS);
				wrapper.getPoints().add(dp);
			}
			list.add(wrapper);
		}

		rrdDb.close();
		return list;
	}

	public String generateXml(RrdDefConfig def) throws IOException {
		String filename = null;
		RrdDb db = null;
		try {
			db = new RrdDb(def.getPath(), true);
			StringBuilder sb = new StringBuilder();
			sb.append(AppConfig.getTempDir())
					.append(AppConfig.getRrdXmlPrefix()).append(def.getName())
					.append("_")
					.append(StringUtil.convertTimeFormatForExport(new Date()))
					.append(".xml");
			filename = sb.toString();
			db.dumpXml(filename);
		} finally {
			if (db != null && !db.isClosed()) {
				db.close();
			}
		}
		return filename;
	}

	@Override
	public String backup(RrdDefConfig def) throws IOException {
		String filename = null;
		RrdDb db = null;
		try {
			db = new RrdDb(def.getPath(), true);
			StringBuilder sb = new StringBuilder();
			sb.append(AppConfig.getBackupDir())
					.append(AppConfig.getRrdXmlPrefix()).append(def.getName())
					.append("_")
					.append(StringUtil.convertTimeFormatForExport(new Date()))
					.append(".xml");
			filename = sb.toString();
			db.dumpXml(filename);
		} finally {
			if (db != null && !db.isClosed()) {
				db.close();
			}
		}
		return filename;
	}
}
