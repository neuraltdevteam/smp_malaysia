package com.neuralt.smp.rrd;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataCollectionProcedure;
import com.neuralt.smp.client.data.DataCollectionProcedure.MethodType;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.collector.DataCollector;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostSnmpConfig;

/**
 * This class should be the entry point for access to RRD services.
 * 
 * @author g705176
 *
 */
public class RrdService {

	private static final Logger logger = LoggerFactory
			.getLogger(RrdService.class);
	private List<DataCollector> collectors;

	private static RrdService instance;

	public static RrdService getInstance() {
		if (instance == null) {
			synchronized (RrdService.class) {
				if (instance == null) {
					instance = new RrdService();
				}
			}
		}
		return instance;
	}

	private RrdService() {
		collectors = new ArrayList<DataCollector>();
	}

	public static RrdAccessInterface getFileRrdAccess() {
		return FileBackendRrdAccesser.getInstance();
	}

	public static RrdAccessInterface getDefaultRrdAccess() {
		return getFileRrdAccess();
	}

	/**
	 * This method should only be called with SmpClient.start()
	 */
	public void startAllDataCollection() {
		logger.info("Starting all RRD...");
		collectors.clear();

		List<RrdDefConfig> list = DataAccess.rrdDefConfigDao
				.getAllLockedRrdDefs();
		if (list != null && list.size() > 0) {
			for (RrdDefConfig def : list) {
				if (def.isEnabled()) {
					DataCollector collector = new DataCollector(def);
					collector.start();
					collectors.add(collector);
				}
			}
		}
	}

	/**
	 * This method should only be called with SmpClient.stop()
	 */
	public void stopAllDataCollection() {
		logger.info("Stopping all RRD...");
		for (DataCollector collector : collectors) {
			collector.quit();
		}
		collectors.clear();

	}

	/**
	 * This method is used for manually starting a RRD definition
	 * 
	 * @param def
	 *            RRD definition to be started
	 */
	public void startDataCollection(RrdDefConfig def) {
		if (def.isLocked() && def.isEnabled()) {
			boolean ok = true;
			for (DataCollector collector : collectors) {
				if (collector.getDef().equals(def)) {
					ok = false;
					break;
				}
			}
			if (ok) {
				logger.info("Starting rrd:" + def.getName());
				DataCollector collector = new DataCollector(def);
				collectors.add(collector);
				collector.start();
			}
		}
	}

	/**
	 * This method is used for manually stopping a RRD definition
	 * 
	 * @param def
	 *            RRD definition to be stopped
	 */
	public void stopDataCollection(RrdDefConfig def) {
		DataCollector dummy = null;
		for (DataCollector collector : collectors) {
			if (collector.getDef().equals(def)) {
				logger.info("Stopping collector:" + collector);
				collector.quit();
				dummy = collector;
				break;
			}
		}
		if (dummy != null) {
			collectors.remove(dummy);
		}
	}

	/**
	 * This method is used to stop all RRD services related to the host. <br/>
	 * The DataCollectionProcedures should be deleted here.
	 * 
	 * @param host
	 *            the host to be deleted
	 */
	public void deleteHost(HostConfig host) {
		List<RrdDefConfig> defs = DataAccess.rrdDefConfigDao.getAllRrdDefs();
		List<RrdDefConfig> modifiedDefs = new ArrayList<RrdDefConfig>();
		for (RrdDefConfig def : defs) {
			List<DataCollectionProcedure> pros = new ArrayList<DataCollectionProcedure>();
			for (DataCollectionProcedure pro : def.getProcedures()) {
				if (pro.getSnmp() != null) {
					if (pro.getSnmp().getSnmpConfig().getHost().equals(host)) {
						pro.setSnmp(null);
						if (pro.getType().equals(MethodType.SNMP)) {
							pros.add(pro);
						}
					}
				}
				if (pro.getScript() != null) {
					if (pro.getScript().getHost().equals(host)) {
						pro.setScript(null);
						if (pro.getType().equals(MethodType.SCRIPT)) {
							pros.add(pro);
						}
					}
				}
			}
			if (pros.size() > 0) {
				for (DataCollectionProcedure pro : pros) {
					def.getProcedures().remove(pro);
					def.getRrdDsConfigs().remove(pro.getDs());
				}
				def.setEnabled(false);
				def.setLocked(false);
				modifiedDefs.add(def);
				stopDataCollection(def);
			}
		}
		DataAccess.rrdDefConfigDao.batchSave(modifiedDefs);
	}

	public void deleteSnmp(HostSnmpConfig snmp) {
		List<RrdDefConfig> defs = DataAccess.rrdDefConfigDao.getAllRrdDefs();
		List<RrdDefConfig> modifiedDefs = new ArrayList<RrdDefConfig>();
		for (RrdDefConfig def : defs) {
			List<DataCollectionProcedure> pros = new ArrayList<DataCollectionProcedure>();
			for (DataCollectionProcedure pro : def.getProcedures()) {
				if (pro.getSnmp() != null) {
					if (pro.getSnmp().getSnmpConfig().equals(snmp)) {
						pro.setSnmp(null);
						if (pro.getType().equals(MethodType.SNMP)) {
							pros.add(pro);
						}
					}
				}
			}
			if (pros.size() > 0) {
				for (DataCollectionProcedure pro : pros) {
					def.getProcedures().remove(pro);
					def.getRrdDsConfigs().remove(pro.getDs());
				}
				def.setEnabled(false);
				def.setLocked(false);
				modifiedDefs.add(def);
				stopDataCollection(def);
			}
		}
		DataAccess.rrdDefConfigDao.batchSave(modifiedDefs);
	}
}
