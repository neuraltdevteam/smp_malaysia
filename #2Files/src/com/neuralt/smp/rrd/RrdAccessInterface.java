package com.neuralt.smp.rrd;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import com.neuralt.smp.client.data.DataWrapper;
import com.neuralt.smp.client.data.RrdArchiveConfig.ArchiveType;
import com.neuralt.smp.client.data.RrdDefConfig;

public interface RrdAccessInterface {
	public void create(RrdDefConfig def) throws IOException;

	// standard data definition approach
	public void update(RrdDefConfig def, DataWrapper wrapper)
			throws IOException;

	public void update(RrdDefConfig def, List<DataWrapper> wrappers)
			throws IOException;

	// ad-hoc approach to comply with host stat
	public void update(String path, List<String> filters, DataWrapper wrapper)
			throws IOException;

	public void update(String path, List<String> filters,
			List<DataWrapper> wrappers) throws IOException;

	public String generateGraph(Date from, Date to, ArchiveType type, int step,
			String title, String vLabel, RrdDefConfig def) throws IOException;

	public List<DataWrapper> fetchData(Date from, Date to, long step,
			ArchiveType type, RrdDefConfig def) throws IOException;

	public String generateXml(RrdDefConfig def) throws IOException;

	public String backup(RrdDefConfig def) throws IOException;
}
