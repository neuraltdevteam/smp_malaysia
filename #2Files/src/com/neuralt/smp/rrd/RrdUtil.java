package com.neuralt.smp.rrd;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.rrd4j.ConsolFun;
import org.rrd4j.DsType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.agent.mbean.model.Disk;
import com.neuralt.smp.agent.mbean.model.NetworkInterface;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.data.RrdArchiveConfig;
import com.neuralt.smp.client.data.RrdArchiveConfig.ArchiveType;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;

public class RrdUtil {

	private static final Logger logger = LoggerFactory.getLogger(RrdUtil.class);

	public static ConsolFun translateArchiveType(ArchiveType type) {
		ConsolFun cf = null;
		switch (type) {
		case AVERAGE:
			cf = ConsolFun.AVERAGE;
			break;
		case LAST:
			cf = ConsolFun.LAST;
			break;
		case MINIMUM:
			cf = ConsolFun.MIN;
			break;
		case MAXIMUM:
			cf = ConsolFun.MAX;
			break;
		case FIRST:
			cf = ConsolFun.FIRST;
			break;
		case TOTAL:
			cf = ConsolFun.TOTAL;
			break;
		}
		return cf;
	}

	public static DsType translateDsType(RrdDsConfig.DsType ads) {
		DsType type = null;
		switch (ads) {
		case GAUGE:
			type = DsType.GAUGE;
			break;
		case COUNTER:
			type = DsType.COUNTER;
			break;
		case ABSOLUTE:
			type = DsType.ABSOLUTE;
			break;
		case DERIVE:
			type = DsType.DERIVE;
			break;
		}
		return type;
	}

	public static String createRrdDB(MonitoredHost host) throws IOException {
		RrdDefConfig def = new RrdDefConfig();
		def.setName("host stat-" + host.getSys().getName() + "_"
				+ host.getName());
		// DS

		RrdDsConfig cpu = new RrdDsConfig();
		cpu.setName("all");
		cpu.setStandardGaugeParam();
		RrdDsConfig mem = new RrdDsConfig();
		mem.setName("mem");
		mem.setStandardGaugeParam();
		List<RrdDsConfig> disks = new ArrayList<RrdDsConfig>();
		List<RrdDsConfig> inets = new ArrayList<RrdDsConfig>();
		if (host.getLatestDiskUsage() != null) {
			for (Disk disk : host.getLatestDiskUsage().getDisks()) {
				RrdDsConfig diskDs = new RrdDsConfig();
				diskDs.setStandardGaugeParam();
				diskDs.setName(disk.getFs());
			}
		}
		if (host.getLatestNetworkStatus() != null) {
			for (NetworkInterface inet : host.getLatestNetworkStatus()
					.getInterfaces()) {
				RrdDsConfig inetDsIn = new RrdDsConfig();
				inetDsIn.setStandardGaugeParam();
				inetDsIn.setName(inet.getName() + " - in");
				RrdDsConfig inetDsOut = new RrdDsConfig();
				inetDsOut.setStandardGaugeParam();
				inetDsOut.setName(inet.getName() + " - out");
			}
		}
		def.getRrdDsConfigs().add(mem);
		def.getRrdDsConfigs().add(cpu);
		for (RrdDsConfig ds : disks) {
			def.getRrdDsConfigs().add(ds);
		}
		for (RrdDsConfig ds : inets) {
			def.getRrdDsConfigs().add(ds);
		}
		// archive
		for (RrdArchiveConfig archive : RrdDefConfig.generateDefaultArchives()) {
			def.getRrdArchiveConfigs().add(archive);
		}

		// create
		StringBuilder sb = new StringBuilder();
		sb.append(AppConfig.getRrdFileStorageDir()).append(def.getName())
				.append("_")
				.append(StringUtil.convertTimeFormatForExport(new Date()))
				.append(".rrd");
		String path = sb.toString();
		def.setPath(path);
		try {
			RrdService.getDefaultRrdAccess().create(def);
		} catch (IOException e) {
			throw e;
		}
		return path;
	}

	/**
	 * Method for adjusting the date to be a multiple of step
	 * 
	 * @param date
	 *            the date object, should be a start time or end time
	 * @param step
	 *            the step between each data point (unit:second)
	 * @return a date which may be slight less than original date and is a
	 *         multiple of step
	 */
	public static Date normalizeBoundary(Date date, long step) {
		long time = date.getTime() / ConstantUtil.SEC_MS / step;
		time = time * step * ConstantUtil.SEC_MS;
		return new Date(time);
	}
}
