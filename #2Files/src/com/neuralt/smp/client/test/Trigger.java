package com.neuralt.smp.client.test;

import java.io.Serializable;


public class Trigger implements Serializable {

	private static final long serialVersionUID = -5035655285570496811L;
	private String name = "";	
	private String watermark = "";
	private String severity = "";
	private String noti_group = "";
	private boolean enabled = true;
	
	private String target = "";
	private String desc = "";
	private String mon_type = "";
	private String port = "";
	private String items = "";
	

	public Trigger(String name, String watermark, String severity, String noti_group, boolean enabled) {
		this.name = name;		
		this.watermark = watermark;
		this.severity = severity;
		this.noti_group = noti_group;
		this.enabled = enabled;
				
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}

	public String getDesc()
	{
		return desc;
	}

	public void setTarget(String target)
	{
		this.target = target;
	}

	public String getTarget()
	{
		return target;
	}

	public void setWatermark(String watermark)
	{
		this.watermark = watermark;
	}

	public String getWatermark()
	{
		return watermark;
	}
	
	public void setMon_type(String mon_type)
	{
		this.mon_type = mon_type;
	}

	public String getMon_type()
	{
		return mon_type;
	}

	public void setNoti_group(String noti_group)
	{
		this.noti_group = noti_group;
	}

	public String getNoti_group()
	{
		return noti_group;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public boolean getEnabled()
	{
		return enabled;
	}
	

	public void setPort(String port)
	{
		this.port = port;
	}

	public String getPort()
	{
		return port;
	}
	
	public void setItems(String items)
	{
		this.items = items;
	}

	public String getItems()
	{
		return items;
	}
	
	public void setSeverity(String severity)
	{
		this.severity = severity;
	}

	public String getSeverity()
	{
		return severity;
	}
}
