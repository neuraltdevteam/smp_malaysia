package com.neuralt.smp.client.test;

import java.io.Serializable;


public class Item implements Serializable {


	private static final long serialVersionUID = -7187013514483870060L;
	private String name = "";
	private String definition = "";
	private String target = "";
	private String status = "";
	private String interval = "";
	private boolean enabled = true;
	private String triggers = "";

	
	private String mon_type = "";
	private String role = "";
	private String port = "";
	private String items = "";
	

	public Item(String name, String definition,  String status, String interval, boolean enabled,  String triggers) {
		this.name = name;
		this.definition = definition;
		this.status = status;
		this.interval = interval;
		this.enabled = enabled;
		this.triggers = triggers;		
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setDefinition(String definition)
	{
		this.definition = definition;
	}

	public String getDefinition()
	{
		return definition;
	}

	public void setTarget(String target)
	{
		this.target = target;
	}

	public String getTarget()
	{
		return target;
	}
	
	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getStatus()
	{
		return status;
	}

	public void setInterval(String interval)
	{
		this.interval = interval;
	}

	public String getInterval()
	{
		return interval;
	}
	
	public void setMon_type(String mon_type)
	{
		this.mon_type = mon_type;
	}

	public String getMon_type()
	{
		return mon_type;
	}

	public void setRole(String role)
	{
		this.role = role;
	}

	public String getRole()
	{
		return role;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public boolean getEnabled()
	{
		return enabled;
	}
	

	public void setPort(String port)
	{
		this.port = port;
	}

	public String getPort()
	{
		return port;
	}
	
	public void setItems(String items)
	{
		this.items = items;
	}

	public String getItems()
	{
		return items;
	}
	
	public void setTriggers(String triggers)
	{
		this.triggers = triggers;
	}

	public String getTriggers()
	{
		return triggers;
	}
}
