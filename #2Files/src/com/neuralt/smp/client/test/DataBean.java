package com.neuralt.smp.client.test;

import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean(name = "dataBean")
@SessionScoped

public class DataBean implements Serializable {

	private static final long serialVersionUID = -4504837113355097037L;


	private static final Site[] sites = new Site[]
	{
		new Site("Site1", "Hong Kong", "Yau", "8521234567", "ml.yau@pct-int.com"),
		new Site("Site2", "USA", "", "", ""),
		new Site("Site2", "Malaysia", "", "", "")
	};

	
	private static final Host[] hosts = new Host[]
	{
		new Host("Host1", "Host1 of HLR", "Connected", "192.168.1.1", "SNMP agent", "Active", true, "999", "6 Monitor items", "8 Triggers"),
		new Host("Host2", "Host2 of HLR", "Connected", "192.168.1.2", "SMP agent", "Active", true, "999", "5 Monitor items", "7 Triggers")
	};

	private static final Item[] items = new Item[]
	{		
		new Item("CPU", "CPU1_Data", "-", "60 minutes",  true, "3 Triggers"),
		new Item("Memory", "MEM1_Data",  "-", "60 minutes",  true, "1 Trigger"),
		new Item("PS_Apache", "ApacheProcess_Data", "Running", "5 minutes",  true, "1 Trigger"),
		new Item("PS_Mysql", "MysqlProcess_Data", "Running", "5 minutes",  true, "1 Trigger"),
		new Item("Port_Apache", "ApachePort_Data",  "Running", "5 minutes",  true, "1 Trigger"),
		new Item("Port_Mysql", "MysqlPort_Data", "Running", "5 minutes",  true, "1 Trigger"),		
	};

	private static final Trigger[] triggers = new Trigger[]
	{
		/*
		new Trigger("CPU_WARN", "CPU-1", "75%", "Warning" , "Admin Group", true),
		new Trigger("CPU_MINOR", "CPU-1", "85%", "Minor", "Admin Group" ,true),
		new Trigger("CPU_MAJOR", "CPU-1", "90%", "Major", "Admin Group" ,true),
		*/
		
		
		new Trigger("CPU_WARN", "75%", "Warning" , "Admin Group", true),		
		new Trigger("CPU_MINOR", "85%", "Minor", "Admin Group" ,true),
		new Trigger("CPU_MAJOR", "90%", "Major", "Admin Group" ,true),
		
		
		
		//new Trigger("Apache_MAJOR", "0", "Major" , "Admin Group", true),
		
	};	
	/*
	
	private static final Host[] hosts = new Host[2];
	
	public DataBean() {
		hosts[0].setName = "Host1";
		hosts[0].setDescription = "Host1";

		hosts[1].name = "Host2";

	}
	*/

	public Site[] getSites() {return sites;}
	public Host[] getHosts() {return hosts;}
	public Item[] getItems() {return items;}
	public Trigger[] getTriggers() {return triggers;}
}



