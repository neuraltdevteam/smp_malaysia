package com.neuralt.smp.client.test;

import java.io.Serializable;


public class Site implements Serializable {

	private static final long serialVersionUID = -1756169394007802494L;
	private String name = "";
	private String location = "";
	private String contact = "";
	private String phone = "";
	private String email = "";
	
	private String role = "";
	private boolean enabled = true;
	private String port = "";
	private String items = "";
	private String triggers = "";
	

	public Site(String name, String location, String contact, String phone, String email) {
		this.name = name;
		this.location = location;
		this.contact = contact;
		this.phone = phone;
		this.email = email;		
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setLocation(String location)
	{
		this.location = location;
	}

	public String getLocation()
	{
		return location;
	}

	public void setContact(String contact)
	{
		this.contact = contact;
	}

	public String getContact()
	{
		return contact;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}

	public String getPhone()
	{
		return phone;
	}
	
	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getEmail()
	{
		return email;
	}

	public void setRole(String role)
	{
		this.role = role;
	}

	public String getRole()
	{
		return role;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public boolean getEnabled()
	{
		return enabled;
	}
	

	public void setPort(String port)
	{
		this.port = port;
	}

	public String getPort()
	{
		return port;
	}
	
	public void setItems(String items)
	{
		this.items = items;
	}

	public String getItems()
	{
		return items;
	}
	
	public void setTriggers(String triggers)
	{
		this.triggers = triggers;
	}

	public String getTriggers()
	{
		return triggers;
	}
}
