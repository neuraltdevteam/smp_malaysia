package com.neuralt.smp.client.test;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

@ManagedBean(name = "chartBean")
@SessionScoped

public class ChartBean {
	
	private CartesianChartModel model1, model2, model3, model4, model5, model6;
	
	public ChartBean() {
		model1 = new CartesianChartModel();
		
		ChartSeries Server1 = new ChartSeries();
		Server1.setLabel("Server1");
		Server1.set("00:00", (60*4)/100);
		Server1.set("01:00", (50*4)/100);
		Server1.set("02:00", (50*4)/100);
		Server1.set("03:00", (40*4)/100);
		Server1.set("04:00", (40*4)/100);
		Server1.set("05:00", (30*4)/100);
		Server1.set("06:00", (30*4)/100);
		Server1.set("07:00", (70*4)/100);
		Server1.set("08:00", (80*4)/100);
		Server1.set("09:00", (82*4)/100);
		Server1.set("10:00", (75*4)/100);
		Server1.set("11:00", (78*4)/100);
		Server1.set("12:00", (70*4)/100);
		Server1.set("13:00", (72*4)/100);
		Server1.set("14:00", (66*4)/100);
		Server1.set("15:00", (63*4)/100);
		Server1.set("16:00", (74*4)/100);
		Server1.set("17:00", (73*4)/100);
		Server1.set("18:00", (75*4)/100);
		Server1.set("19:00", (77*4)/100);
		Server1.set("20:00", (75*4)/100);
		Server1.set("21:00", (63*4)/100);
		Server1.set("22:00", (53*4)/100);
		Server1.set("23:00", (51*4)/100);		
		
		ChartSeries Server2 = new ChartSeries();
		Server2.setLabel("Server2");
		Server2.set("00:00", (55*4)/100);
		Server2.set("01:00", (43*4)/100);
		Server2.set("02:00", (41*4)/100);
		Server2.set("03:00", (35*4)/100);
		Server2.set("04:00", (33*4)/100);
		Server2.set("05:00", (20*4)/100);
		Server2.set("06:00", (21*4)/100);
		Server2.set("07:00", (66*4)/100);
		Server2.set("08:00", (70*4)/100);
		Server2.set("09:00", (75*4)/100);
		Server2.set("10:00", (65*4)/100);
		Server2.set("11:00", (68*4)/100);
		Server2.set("12:00", (60*4)/100);
		Server2.set("13:00", (62*4)/100);
		Server2.set("14:00", (56*4)/100);
		Server2.set("15:00", (53*4)/100);
		Server2.set("16:00", (64*4)/100);
		Server2.set("17:00", (63*4)/100);
		Server2.set("18:00", (65*4)/100);
		Server2.set("19:00", (67*4)/100);
		Server2.set("20:00", (65*4)/100);
		Server2.set("21:00", (53*4)/100);
		Server2.set("22:00", (43*4)/100);
		Server2.set("23:00", (41*4)/100);
		
		model1.addSeries(Server1);
		model1.addSeries(Server2);

		model2 = new CartesianChartModel();
		
		ChartSeries Server3 = new ChartSeries();
		Server3.setLabel("Server1");
		Server3.set("09:00", 82);
		Server3.set("10:00", 75);
		Server3.set("11:00", 78);
		Server3.set("12:00", 70);
		Server3.set("13:00", 72);
		Server3.set("14:00", 66);
		Server3.set("15:00", 63);
		Server3.set("16:00", 74);
		Server3.set("17:00", 73);
		Server3.set("18:00", 75);

		ChartSeries Server4 = new ChartSeries();
		Server4.setLabel("Server2");
		Server4.set("09:00", 75);
		Server4.set("10:00", 65);
		Server4.set("11:00", 68);
		Server4.set("12:00", 60);
		Server4.set("13:00", 62);
		Server4.set("14:00", 56);
		Server4.set("15:00", 53);
		Server4.set("16:00", 64);
		Server4.set("17:00", 63);
		Server4.set("18:00", 65);
		
		model2.addSeries(Server3);
		model2.addSeries(Server4);

		model3 = new CartesianChartModel();
		
		ChartSeries Server5 = new ChartSeries();
		Server5.setLabel("Server1");
		Server5.set("09:00", 82);
		Server5.set("10:00", 75);
		Server5.set("11:00", 78);
		Server5.set("12:00", 70);

		ChartSeries Server6 = new ChartSeries();
		Server6.setLabel("Server2");
		Server6.set("09:00", 75);
		Server6.set("10:00", 65);
		Server6.set("11:00", 68);
		Server6.set("12:00", 60);
		
		model3.addSeries(Server5);
		model3.addSeries(Server6);
		
		
		model4 = new CartesianChartModel();		
		ChartSeries Session= new ChartSeries();
		//Session.setLabel("Number of sessions");
		Session.set("09:00", 82);
		Session.set("10:00", 75);
		Session.set("11:00", 78);
		Session.set("12:00", 70);
		Session.set("13:00", 72);
		Session.set("14:00", 66);
		Session.set("15:00", 63);
		Session.set("16:00", 74);
		Session.set("17:00", 73);
		Session.set("18:00", 75);
		model4.addSeries(Session);
		
		
		model5 = new CartesianChartModel();		
		ChartSeries ServerThread = new ChartSeries();
		//ServerThread.setLabel("Server Thread Pool Usage");
		ServerThread.set("09:00", 75);
		ServerThread.set("10:00", 65);
		ServerThread.set("11:00", 68);
		ServerThread.set("12:00", 60);
		ServerThread.set("13:00", 62);
		ServerThread.set("14:00", 56);
		ServerThread.set("15:00", 53);
		ServerThread.set("16:00", 64);
		ServerThread.set("17:00", 63);
		ServerThread.set("18:00", 65);
		model5.addSeries(ServerThread);
		
		model6 = new CartesianChartModel();		
		ChartSeries QueryPersecond = new ChartSeries();
		//QueryPersecond.setLabel("Query/second");
		QueryPersecond.set("09:00", 82);
		QueryPersecond.set("10:00", 75);
		QueryPersecond.set("11:00", 78);
		QueryPersecond.set("12:00", 70);
		QueryPersecond.set("13:00", 72);
		QueryPersecond.set("14:00", 66);
		QueryPersecond.set("15:00", 63);
		QueryPersecond.set("16:00", 74);
		QueryPersecond.set("17:00", 73);
		QueryPersecond.set("18:00", 75);
		model6.addSeries(QueryPersecond);
		
		
	}
	
	public CartesianChartModel getModel1() { return model1; }
	
	public CartesianChartModel getModel2() { return model2; }
	
	public CartesianChartModel getModel3() { return model3; }

	public CartesianChartModel getModel4() { return model4; }
	
	public CartesianChartModel getModel5() { return model5; }
	
	public CartesianChartModel getModel6() { return model6; }
}