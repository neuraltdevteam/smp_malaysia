package com.neuralt.smp.client.test;

import java.io.Serializable;


public class Host implements Serializable {

	private static final long serialVersionUID = -1385855658590146947L;
	private String name = "";
	private String desc = "";
	private String status = "";
	private String IP = "";
	private String mon_type = "";
	private String role = "";
	private boolean enabled = true;
	private String port = "";
	private String items = "";
	private String triggers = "";
	

	public Host(String name, String desc, String status, String IP, String mon_type, String role, boolean enabled, String port, String items, String triggers) {
		this.name = name;
		this.desc = desc;
		this.status = status;
		this.IP = IP;
		this.mon_type = mon_type;
		this.role = role;
		this.enabled = enabled;
		this.port = port;
		this.items = items;
		this.triggers = triggers;		
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	public String getName()
	{
		return name;
	}

	public void setDesc(String desc)
	{
		this.desc = desc;
	}

	public String getDesc()
	{
		return desc;
	}

	public void setStatus(String status)
	{
		this.status = status;
	}

	public String getStatus()
	{
		return status;
	}

	public void setIP(String IP)
	{
		this.IP = IP;
	}

	public String getIP()
	{
		return IP;
	}
	
	public void setMon_type(String mon_type)
	{
		this.mon_type = mon_type;
	}

	public String getMon_type()
	{
		return mon_type;
	}

	public void setRole(String role)
	{
		this.role = role;
	}

	public String getRole()
	{
		return role;
	}

	public void setEnabled(boolean enabled)
	{
		this.enabled = enabled;
	}

	public boolean getEnabled()
	{
		return enabled;
	}
	

	public void setPort(String port)
	{
		this.port = port;
	}

	public String getPort()
	{
		return port;
	}
	
	public void setItems(String items)
	{
		this.items = items;
	}

	public String getItems()
	{
		return items;
	}
	
	public void setTriggers(String triggers)
	{
		this.triggers = triggers;
	}

	public String getTriggers()
	{
		return triggers;
	}
}
