package com.neuralt.smp.client.listener;

import javax.management.Notification;
import javax.management.NotificationFilter;

import com.neuralt.smp.notification.MonitorNotification;

public class HostMonitorNotificationFilter implements NotificationFilter {

	private static final long serialVersionUID = 1263126837575226773L;

	@Override
	public boolean isNotificationEnabled(Notification notification) {
		return notification instanceof MonitorNotification;
	}

}
