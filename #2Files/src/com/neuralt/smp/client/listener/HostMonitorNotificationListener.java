package com.neuralt.smp.client.listener;

import javax.management.Notification;
import javax.management.NotificationListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.agent.mbean.model.HostCPUUsage;
import com.neuralt.smp.agent.mbean.model.HostDiskUsage;
import com.neuralt.smp.agent.mbean.model.HostInetStatus;
import com.neuralt.smp.agent.mbean.model.HostMemoryUsage;
import com.neuralt.smp.agent.mbean.model.HostUptime;
import com.neuralt.smp.agent.mbean.model.ProcessHealth;
import com.neuralt.smp.client.HostManager;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.config.ProcessStatus;
import com.neuralt.smp.notification.AgentNotification;
import com.neuralt.smp.notification.CPUUsageNotification;
import com.neuralt.smp.notification.DiskUsageNotification;
import com.neuralt.smp.notification.ErrorNotification;
import com.neuralt.smp.notification.InetStatusUsageNotification;
import com.neuralt.smp.notification.MemoryUsageNotification;
import com.neuralt.smp.notification.ProcessEventNotification;
import com.neuralt.smp.notification.ProcessHealthNotification;
import com.neuralt.smp.notification.UptimeNotification;

public class HostMonitorNotificationListener implements NotificationListener {

	private static final Logger logger = LoggerFactory
			.getLogger(HostMonitorNotificationListener.class);
	private HostManager hostMgr;

	public HostMonitorNotificationListener(HostManager hostMgr) {
		this.hostMgr = hostMgr;
	}

	@Override
	public void handleNotification(Notification notification, Object handback) {
		MonitoredHost host = hostMgr.getHost();
		logger.debug(
				"handleNotification - received notification from [{}], type [{}]",
				host.getName(), notification.getClass());

		if (notification instanceof AgentNotification) {
			// Listener thread should only dispatch. Outsource the processing to
			// ExecutorService threads.
			HostNotificationHandlerTask task = new HostNotificationHandlerTask(
					(AgentNotification) notification);
			SmpClient.instance.submitTask(task);
		}
	}

	private class HostNotificationHandlerTask implements Runnable {

		private AgentNotification notification;

		public HostNotificationHandlerTask(AgentNotification n) {
			this.notification = n;
		}

		@Override
		public void run() {
			MonitoredHost host = hostMgr.getHost();

			if (notification instanceof ProcessEventNotification) {
				handleProcessEventNotification((ProcessEventNotification) notification);
			} else if (notification instanceof CPUUsageNotification) {
				CPUUsageNotification note = (CPUUsageNotification) notification;
				host.receivedStatistics((HostCPUUsage) note.getUsage());
			} else if (notification instanceof DiskUsageNotification) {
				DiskUsageNotification note = (DiskUsageNotification) notification;
				host.receivedStatistics((HostDiskUsage) note.getUsage());
			} else if (notification instanceof InetStatusUsageNotification) {
				InetStatusUsageNotification note = (InetStatusUsageNotification) notification;
				host.receivedStatistics((HostInetStatus) note.getUsage());
			} else if (notification instanceof MemoryUsageNotification) {
				MemoryUsageNotification note = (MemoryUsageNotification) notification;
				host.receivedStatistics((HostMemoryUsage) note.getUsage());
			} else if (notification instanceof ProcessHealthNotification) {
				ProcessHealthNotification note = (ProcessHealthNotification) notification;
				host.receivedStatistics((ProcessHealth) note.getUsage());
			} else if (notification instanceof UptimeNotification) {
				UptimeNotification note = (UptimeNotification) notification;
				host.receivedStatistics((HostUptime) note.getUsage());
			} else if (notification instanceof ErrorNotification) {
				ErrorNotification note = (ErrorNotification) notification;
				host.receivedError(note);
			}
		}

		private void handleProcessEventNotification(ProcessEventNotification n) {
			String procName = n.getProcessName();
			MonitoredProcess proc = hostMgr.getHost().getProcess(procName);
			if (proc == null) {
				logger.error("ProcessEventNotificationListener - notification refers to an unknown process: "
						+ procName);
				return;
			}

			switch (n.getEvtType()) {
			case STARTUP: {
				if (n.isSuccess()) {
					proc.setStatus(ProcessStatus.STARTED);
					proc.setStarted(n.getTimeStamp());
					hostMgr.startMonProcess(proc.getConfig().getName());
					CentralizedEventService.getInstance()
							.onRemoteAppStartSuccess(hostMgr.getHost(),
									procName, n.getMessage());
				} else {
					proc.setStatus(ProcessStatus.STOPPED);
					CentralizedEventService.getInstance()
							.onRemoteAppStartFailed(hostMgr.getHost(),
									procName, n.getMessage());
				}
				// EventLog eventLog = new EventLog(Severity.INFO,
				// SourceType.PROCESS, proc, n.getTimeStamp(),
				// proc.getStatusText());
				/*
				 * Alarm alarm = new Alarm();
				 * alarm.setOid(SvrSnmpConstants.remAppStartSuccessNotification
				 * ); alarm.setSource(hostMgr.getHost().getConfig().getName());
				 * alarm.setTrapAlarmLevel(WarningLevel.INFO.name());
				 * alarm.setStatus(AlarmStatus.CLEARED);
				 * alarm.setTrapAppName(AppConfig.getMyName());
				 * alarm.setSourceIP(hostMgr.getHost().getConfig().getIpAddr());
				 * alarm.setAlarmDescription("Process started");
				 * DataAccess.alarmDao.save(alarm);
				 */
				// DataAccess.eventLogDao.save(eventLog);

				// hostMgr.startMonProcess(procName);
			}
				break;
			case SHUTDOWN: {
				if (n.isSuccess()) {
					proc.setStatus(ProcessStatus.STOPPED);
					proc.setStopped(n.getTimeStamp());
					CentralizedEventService.getInstance().onRemoteAppStopped(
							hostMgr.getHost(), procName, n.getMessage());
				} else {
					// TODO
					CentralizedEventService.getInstance()
							.onRemoteAppStopFailed(hostMgr.getHost(), procName,
									n.getMessage());
				}
				// proc.setStatus(ProcessStatus.STOPPED);
				// proc.setStopped(n.getTimeStamp());
				// EventLog eventLog = new EventLog(Severity.INFO,
				// SourceType.PROCESS, proc, n.getTimeStamp(),
				// proc.getStatusText());
			}
				break;
			case DIED: {
				proc.setStatus(ProcessStatus.DIED);
				// TODO carry out configured action (restart etc?)
				// EventLog eventLog = new EventLog(Severity.SEVERE,
				// SourceType.PROCESS, proc, n.getTimeStamp(),
				// proc.getStatusText());
				/*
				 * Alarm alarm = new Alarm();
				 * alarm.setOid(SvrSnmpConstants.remAppNotPresentNotification);
				 * alarm.setSource(hostMgr.getHost().getConfig().getName());
				 * alarm.setTrapAlarmLevel(WarningLevel.MAJOR.name());
				 * alarm.setStatus(AlarmStatus.UNHANDLED);
				 * alarm.setTrapAppName(AppConfig.getMyName());
				 * alarm.setSourceIP(hostMgr.getHost().getConfig().getIpAddr());
				 * alarm.setAlarmDescription("Process died");
				 * DataAccess.alarmDao.save(alarm);
				 */
				// DataAccess.eventLogDao.save(eventLog);
				CentralizedEventService.getInstance().onRemoteAppDown(
						hostMgr.getHost(), procName, n.getMessage());
			}
				break;
			default:
				logger.error("ProcessEventNotificationListener - unhandled eventType: "
						+ n.getEvtType());
				break;
			}
		}
	}
}
