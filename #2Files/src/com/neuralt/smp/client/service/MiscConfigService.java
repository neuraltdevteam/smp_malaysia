package com.neuralt.smp.client.service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.config.MiscConfig;

public class MiscConfigService {

	// keys
	public static final String key_defaultTrapAlarmEnabled = "DEFAULT_TRAP_ALARM_ENABLED";
	public static final String key_defaultTrapAlarmThreshold = "DEFAULT_TRAP_ALARM_THRESHOLD";

	public static final MiscConfigService instance = new MiscConfigService();

	private Map<String, MiscConfig> cache;

	public MiscConfigService() {
		cache = new ConcurrentHashMap<String, MiscConfig>();
	}

	public void init() {
		refresh();
	}

	public void refresh() {
		cache.clear();
		List<MiscConfig> list = DataAccess.miscConfigDao.list();
		if (list != null && !list.isEmpty()) {
			for (MiscConfig entry : list) {
				cache.put(entry.getConfigKey(), entry);
			}
		}
	}

	public MiscConfig getConfig(String key) {
		return cache.get(key);
	}

	public void saveConfig(MiscConfig cfg) {
		cache.put(cfg.getConfigKey(), cfg);
		DataAccess.miscConfigDao.save(cfg);
	}

	public void saveConfigs(Collection<MiscConfig> configs) {
		for (MiscConfig cfg : configs) {
			cache.put(cfg.getConfigKey(), cfg);
		}
		DataAccess.miscConfigDao.batchSave(configs);
	}
}
