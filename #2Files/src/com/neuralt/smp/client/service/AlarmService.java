package com.neuralt.smp.client.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.AlarmNotifier;
import com.neuralt.smp.client.LiveMonitorReporter;
import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.config.AlarmDefinition;
import com.neuralt.smp.client.data.config.AlarmMap;
import com.neuralt.smp.client.data.config.AlarmMap.AlarmMapKey;
import com.neuralt.smp.client.data.config.AlarmStatus;
import com.neuralt.smp.client.data.config.MiscConfig;
import com.neuralt.smp.client.event.AbstractEventHandler;
import com.neuralt.smp.client.event.DefaultLocalSystemEventHandler;
import com.neuralt.smp.client.event.DefaultRemoteAppEventHandler;
import com.neuralt.smp.client.event.DefaultRemoteHostEventHandler;
import com.neuralt.smp.client.event.LocalSystemEvent;
import com.neuralt.smp.client.event.RemoteAppEvent;
import com.neuralt.smp.client.event.RemoteHostEvent;
import com.neuralt.smp.client.event.SmpEvent;
import com.neuralt.smp.client.event.SmpEvent.EventType;
import com.neuralt.smp.client.util.EventDelegate;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.TextColor;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.StringUtil;

public class AlarmService {

	public static final AlarmService instance = new AlarmService();

	// public static final boolean defaultTrapAlarmEnabled = true;
	// public static final WarningLevel defaultTrapAlarmThreshold =
	// WarningLevel.WARNING;

	private static final Logger logger = LoggerFactory
			.getLogger(AlarmService.class);

	private List<AlarmStatus> defaultAlarmStatusList;
	private List<AlarmStatus> alarmStatusList;
	private Map<String, AlarmStatus> statusIndex;

	private ArrayList<AlarmDefinition> alarmDefinitions;

	private ReadWriteLock alarmMapsLock;
	private Map<AlarmMapKey, AlarmMap> eventAlarmMap; // eventName -> alarmDef
	private Map<AlarmDefinition, List<AlarmMap>> eventAlarmMapReverse; // reverse
																		// of
																		// the
																		// above,
																		// alarmName
																		// ->
																		// eventName
	private Map<AlarmMapKey, AlarmMap> trapAlarmMap; // OID (as string) ->
														// alarmDef
	private Map<AlarmDefinition, List<AlarmMap>> trapAlarmMapReverse; // reverse
																		// of
																		// the
																		// above,
																		// alarmName
																		// ->
																		// OID
																		// (as
																		// string)

	private static AlarmStatus stUnhandled = new AlarmStatus("unhandled",
			AlarmStatus.StatusCategory.INITIAL, TextColor.RED, true);
	private static AlarmStatus stAck = new AlarmStatus("acknowledged",
			AlarmStatus.StatusCategory.INTERIM, TextColor.ORANGE, true);
	private static AlarmStatus stIgnored = new AlarmStatus("ignored",
			AlarmStatus.StatusCategory.FINAL, TextColor.GREEN, true);
	private static AlarmStatus stResolved = new AlarmStatus("resolved",
			AlarmStatus.StatusCategory.FINAL, TextColor.GREEN, true);
	private static AlarmStatus stCleared = new AlarmStatus("auto-cleared",
			AlarmStatus.StatusCategory.FINAL, TextColor.GREEN, true);

	private Map<Class<? extends SmpEvent>, LinkedHashMap<String, AbstractEventHandler>> handlerMap;

	private List<Alarm> unresolvedAlarms = null;
	private long lastUpdatedUnresolvedAlarms;

	private AlarmService() {
		handlerMap = new HashMap<Class<? extends SmpEvent>, LinkedHashMap<String, AbstractEventHandler>>();
		registerDefaultHandlers();
	}

	// create and add default event handlers
	private void registerDefaultHandlers() {
		addEventHandler(LocalSystemEvent.class,
				new DefaultLocalSystemEventHandler());
		addEventHandler(RemoteHostEvent.class,
				new DefaultRemoteHostEventHandler());
		addEventHandler(RemoteAppEvent.class,
				new DefaultRemoteAppEventHandler());
	}

	public synchronized boolean addEventHandler(
			Class<? extends SmpEvent> evtClazz, AbstractEventHandler handler) {
		LinkedHashMap<String, AbstractEventHandler> handlers = handlerMap
				.get(evtClazz);
		if (handlers == null) {
			handlers = new LinkedHashMap<String, AbstractEventHandler>();
			handlerMap.put(evtClazz, handlers);
		}
		if (!handlers.containsKey(handler.getName())) {
			handlers.put(handler.getName(), handler);
			return true;
		}
		return false;
	}

	public synchronized boolean removeEventHandler(
			Class<? extends SmpEvent> evtClazz, String handlerName) {
		LinkedHashMap<String, AbstractEventHandler> handlers = handlerMap
				.get(evtClazz);
		if (handlers != null) {
			return handlers.remove(handlerName) != null;
		}
		return false;
	}

	// dispatch event to corresponding handler(s)
	private SnmpTrap makeTrapFromEvent(SmpEvent event, WarningLevel warningLevel) {
		LinkedHashMap<String, AbstractEventHandler> handlers = handlerMap
				.get(event.getClass());
		if (handlers != null && !handlers.isEmpty()) {
			for (AbstractEventHandler handler : handlers.values()) {
				if (handler.accepts(event)) {
					return handler.toTrap(event, warningLevel);
				}
			}
		}

		return null;
	}

	private void recordEvent(SmpEvent event) {
		LinkedHashMap<String, AbstractEventHandler> handlers = handlerMap
				.get(event.getClass());
		if (handlers != null && !handlers.isEmpty()) {
			for (AbstractEventHandler handler : handlers.values()) {
				if (handler.accepts(event)) {
					handler.recordEvent(event);
				}
			}
		}
	}

	private List<AlarmStatus> defaultAlarmStatusList() {
		return Arrays.asList(new AlarmStatus[] { stUnhandled, stAck, stIgnored,
				stResolved, stCleared });
	}

	public void init() {
		alarmStatusList = new ArrayList<AlarmStatus>();
		statusIndex = new HashMap<String, AlarmStatus>();
		reloadAlarmStatusList();

		alarmDefinitions = new ArrayList<AlarmDefinition>();
		reloadAlarmDefinitions();

		alarmMapsLock = new ReentrantReadWriteLock();
		// use case: given event/oid -> find alarmDefinition
		// given alarmDefintion -> find Mappings leading to it -> events/oids
		// linked to it
		eventAlarmMap = new HashMap<AlarmMapKey, AlarmMap>();
		eventAlarmMapReverse = new HashMap<AlarmDefinition, List<AlarmMap>>();
		trapAlarmMap = new HashMap<AlarmMapKey, AlarmMap>();
		trapAlarmMapReverse = new HashMap<AlarmDefinition, List<AlarmMap>>();
		reloadAlarmMappings();
		// load a number of settings
		// Event / Trap -> Alarm mappings
		// Trigger thresholds (trap level etc.)
	}

	// ////////// AlarmStatus ////////////
	public AlarmStatus getAlarmStatus(String statusName) {
		return statusIndex.get(statusName);
	}

	public void reloadAlarmStatusList() {
		defaultAlarmStatusList = defaultAlarmStatusList();
		synchronized (alarmStatusList) {
			alarmStatusList.clear();
			alarmStatusList.addAll(defaultAlarmStatusList);
			alarmStatusList.addAll(DataAccess.alarmConfigDao.listAlarmStatus());
			reindexAlarmStatus();
		}
	}

	private void reindexAlarmStatus() {
		synchronized (statusIndex) {
			statusIndex.clear();
			for (AlarmStatus status : alarmStatusList) {
				statusIndex.put(status.getName(), status);
			}
		}
	}

	public List<AlarmStatus> listAlarmStatus() {
		List<AlarmStatus> shallowCopy = new ArrayList<AlarmStatus>(
				alarmStatusList.size());
		synchronized (alarmStatusList) {
			shallowCopy.addAll(alarmStatusList);
		}
		return shallowCopy;
	}

	public String getDefaultAlarmStatus() {
		synchronized (alarmStatusList) {
			for (AlarmStatus status : alarmStatusList) {
				if (status.getCategory() == AlarmStatus.StatusCategory.INITIAL) {
					return status.getName();
				}
			}
		}
		return null;
	}

	// /////////// AlarmDefinition & AlarmMap ///////////////
	public void saveAlarmDefinition(AlarmDefinition alarmDef) {
		DataAccess.alarmConfigDao.save(alarmDef);
		// if (alarmDef.isEnabled()) {
		synchronized (alarmDefinitions) {
			int index = Collections.binarySearch(alarmDefinitions, alarmDef);
			if (index < 0) {
				// new alarmDef
				index = (index + 1) * (-1); // insertion point
				alarmDefinitions.add(index, alarmDef);
			} else {
				// existing alarmDef
				alarmDefinitions.set(index, alarmDef);
			}
			// re-index mappings.
			alarmMapsLock.writeLock().lock();
			// note: passing in new alarmDef here, assuming existing & updated
			// alarmDef shares ID (and hashcode)
			clearAlarmMapIndex(alarmDef);

			if (alarmDef.getAlarmMaps() != null) {
				for (AlarmMap mapping : alarmDef.getAlarmMaps()) {
					insertAlarmMapping(mapping);
				}
			}
			alarmMapsLock.writeLock().unlock();
		}
		// } else {
		// alarmMapsLock.writeLock().lock();
		// clearAlarmMapIndex(alarmDef);
		// alarmMapsLock.writeLock().unlock();
		// }
	}

	public void deleteAlarmDefinition(AlarmDefinition alarmDef) {
		DataAccess.alarmConfigDao.delete(alarmDef);
		synchronized (alarmDefinitions) {
			int index = Collections.binarySearch(alarmDefinitions, alarmDef);
			if (index >= 0) {
				alarmMapsLock.writeLock().lock();
				clearAlarmMapIndex(alarmDef);
				alarmMapsLock.writeLock().unlock();
				alarmDefinitions.remove(index);
			}
		}
	}

	public void reloadAlarmDefinitions() {
		synchronized (alarmDefinitions) {
			alarmDefinitions.clear();
			alarmDefinitions.addAll(DataAccess.alarmConfigDao
					.listEnabledAlarmDefinitions());
			Collections.sort(alarmDefinitions);
		}
	}

	public List<AlarmDefinition> listAlarmDefinitions() {
		List<AlarmDefinition> shallowCopy = new ArrayList<AlarmDefinition>(
				alarmDefinitions.size());
		synchronized (alarmDefinitions) {
			shallowCopy.addAll(alarmDefinitions);
		}
		return shallowCopy;
	}

	public void reloadAlarmMappings() {
		List<AlarmMap> rawData = DataAccess.alarmConfigDao.listAlarmMaps();
		alarmMapsLock.writeLock().lock();
		trapAlarmMap.clear();
		trapAlarmMapReverse.clear();
		eventAlarmMap.clear();
		eventAlarmMapReverse.clear();
		for (AlarmMap mapping : rawData) {
			insertAlarmMapping(mapping);
		}
		alarmMapsLock.writeLock().unlock();
	}

	// refresh a specific AlarmDefinition from db, including its mappings
	public AlarmDefinition reloadAlarmDefinition(AlarmDefinition existingCopy) {
		AlarmDefinition newCopy = DataAccess.alarmConfigDao
				.getAlarmDefinition(existingCopy.getId());
		alarmMapsLock.writeLock().lock();

		for (int i = 0; i < alarmDefinitions.size(); i++) {
			if (alarmDefinitions.get(i).getId().equals(newCopy.getId())) {
				alarmDefinitions.set(i, newCopy);
			}
		}

		clearAlarmMapIndex(existingCopy);
		for (AlarmMap mapping : newCopy.getAlarmMaps()) {
			insertAlarmMapping(mapping);
		}

		alarmMapsLock.writeLock().unlock();
		return newCopy;
	}

	private void clearAlarmMapIndex(AlarmDefinition alarmDef) {
		List<AlarmMap> existing = trapAlarmMapReverse.get(alarmDef);
		// traps index
		if (existing != null && !existing.isEmpty()) {
			for (AlarmMap map : existing) {
				trapAlarmMap.remove(map.getKey());
			}
		}
		trapAlarmMapReverse.remove(alarmDef);
		// events index
		existing = eventAlarmMapReverse.get(alarmDef);
		if (existing != null && !existing.isEmpty()) {
			for (AlarmMap map : existing) {
				eventAlarmMap.remove(map.getKey());
			}
		}
		eventAlarmMapReverse.remove(alarmDef);
	}

	private void insertAlarmMapping(AlarmMap mapping) {
		switch (mapping.getMapType()) {
		case EVENT: {
			// map
			eventAlarmMap.put(mapping.getKey(), mapping);
			// reverse map
			List<AlarmMap> evtMappings = eventAlarmMapReverse.get(mapping
					.getMappedAlarm());
			if (evtMappings == null) {
				evtMappings = new ArrayList<AlarmMap>();
				eventAlarmMapReverse.put(mapping.getMappedAlarm(), evtMappings);
			}
			evtMappings.add(mapping);
		}
			break;
		case TRAP: {
			// map
			trapAlarmMap.put(mapping.getKey(), mapping);
			// reverse map
			List<AlarmMap> trapMappings = trapAlarmMapReverse.get(mapping
					.getMappedAlarm());
			if (trapMappings == null) {
				trapMappings = new ArrayList<AlarmMap>();
				trapAlarmMapReverse.put(mapping.getMappedAlarm(), trapMappings);
			}
			trapMappings.add(mapping);
		}
			break;
		}
	}

	public List<AlarmMap> getEventAlarmMaps(AlarmDefinition alarmDef) {
		alarmMapsLock.readLock().lock();
		List<AlarmMap> mappings = eventAlarmMapReverse.get(alarmDef);
		if (mappings == null || mappings.isEmpty()) {
			alarmMapsLock.readLock().unlock();
			return null;
		} else {
			List<AlarmMap> shallowCopy = new ArrayList<AlarmMap>(mappings);
			alarmMapsLock.readLock().unlock();
			return shallowCopy;
		}
	}

	public List<AlarmMap> getTrapAlarmMaps(AlarmDefinition alarmDef) {
		alarmMapsLock.readLock().lock();
		List<AlarmMap> mappings = trapAlarmMapReverse.get(alarmDef);
		if (mappings == null || mappings.isEmpty()) {
			alarmMapsLock.readLock().unlock();
			return null;
		} else {
			List<AlarmMap> shallowCopy = new ArrayList<AlarmMap>(mappings);
			alarmMapsLock.readLock().unlock();
			return shallowCopy;
		}
	}

	private AlarmMap findAlarmMapping(AlarmMapKey key,
			Map<AlarmMapKey, AlarmMap> map) {
		if (alarmMapsLock == null)
			return null;

		alarmMapsLock.readLock().lock();
		AlarmMap result = map.get(key);
		alarmMapsLock.readLock().unlock();
		return result;
	}

	public AlarmMap findTrapAlarmMapping(String trapName, WarningLevel level) {
		return findAlarmMapping(new AlarmMapKey(AlarmMap.AlarmMapType.TRAP,
				trapName, level), trapAlarmMap);
	}

	public AlarmMap findEventAlarmMapping(SmpEvent event) {
		return findEventAlarmMapping(event.getEventType());
	}

	public AlarmMap findEventAlarmMapping(EventType evtType) {
		AlarmMapKey mapKey = new AlarmMapKey(AlarmMap.AlarmMapType.EVENT,
				evtType.toString(), null);
		return findAlarmMapping(mapKey, eventAlarmMap);
	}

	/**
	 * This method is used to translate the SMP events to internal SMP alarm
	 * object and send notification based on alarm mapping/definition.
	 * 
	 * @param event
	 *            the generated SMP event
	 * @param trap
	 */
	public void handleEvent(final SmpEvent event) {
		WarningLevel level = null;
		boolean ignored = false;

		// save event in db
		recordEvent(event);

		try {
			level = event.getLevel();
		} catch (Exception e) {
			level = WarningLevel.UNKNOWN;
		}

		AlarmMap alarmMap = null;
		if (level != null) {
			alarmMap = findEventAlarmMapping(event);
		}

		Date now = new Date();
		Alarm newAlarm = null;
		AlarmDefinition alarmDef = null;
		SnmpTrap trap = translateEventToTrap(event);

		// it doesn't have alarm mapping
		if (alarmMap == null
				|| (alarmMap != null && !alarmMap.getMappedAlarm().isEnabled())) {
			// it doesn't have default alarm trigger enabled
			newAlarm = createAlarm(event);
			if (trap != null) {
				AlarmNotifier.instance.relayTrap(trap.getOriginal(), newAlarm);
			}

			if (!isTriggerAlarmEnabled(event)) {
				return; // no alarm required
			}
			// proceeds to turn this event into alarm

		} else {
			// use mapped alarm
			alarmDef = alarmMap.getMappedAlarm();
			newAlarm = createAlarm(alarmDef, event);

			if (trap != null) {
				AlarmNotifier.instance.relayTrap(trap.getOriginal(), newAlarm);
				ignored = true;
			}
		}

		// check for existing entries, increment occurrence or clear
		if (newAlarm != null) {
			Alarm alarmEntry = DataAccess.alarmDao.getUnresolvedAlarm(
					newAlarm.getName(), newAlarm.getSource(),
					newAlarm.getSourceIp());
			if (alarmEntry == null) {
				// the alarm is not unresolved
				alarmEntry = DataAccess.alarmDao.getIgnoredAlarm(
						newAlarm.getName(), newAlarm.getSource(),
						newAlarm.getSourceIp());
				if (alarmEntry == null) {
					// the alarm is not unresolved & ignored, ie. it is new
					alarmEntry = newAlarm;
				} else {
					ignored = true;
				}
			} else {
				// the alarm is unresolved
				if (newAlarm.getLevel() == WarningLevel.CLEAR) {
					alarmEntry.setResolved(now);
					alarmEntry.setStatus(stCleared.getName());
					alarmEntry.setResolverId(AppConfig.getMyName());
				} else {
					// the alarm is still unresolved
					// occurrence increment will be performed in save method of
					// DAO
					if (alarmMap == null) {
						alarmEntry.setLevel(level);
					} else {
						// do nothing - use predefined level
					}
				}
			}
			if (alarmEntry != null) {
				alarmEntry.setLastOccurred(new Date());
				alarmEntry.setDescription(event.getDetails());
				if (alarmMap == null) {
					// an unmapped alarm - treat the alarm level as the same as
					// that of incoming trap
					alarmEntry.setLevel(level);
				}
				// if status=ignored, still update record but do not report
				if (!StringUtil.streql(alarmEntry.getStatus(),
						stIgnored.getName())) {
					LiveMonitorReporter.getInstance().addHotAlarm(alarmEntry);
				}
				saveAlarmEntry(alarmEntry);
			}
			// perform notification here - do not need to consider cooldown
			if (!ignored) {
				trap.setTrapAlarmLevel(alarmDef != null ? alarmDef.getLevel()
						.name() : WarningLevel.WARNING.name());
				AlarmNotifier.instance.handleOutstandingAlarm(event,
						alarmEntry, trap);
			}
		}
	}

	private SnmpTrap translateEventToTrap(SmpEvent event) {
		LinkedHashMap<String, AbstractEventHandler> handlers = handlerMap
				.get(event.getClass());
		if (handlers != null && !handlers.isEmpty()) {
			for (AbstractEventHandler handler : handlers.values()) {
				if (handler.accepts(event)) {
					return handler.toTrap(event, event.getLevel());
				}
			}
		}
		return null;
	}

	/**
	 * This method is used to translate the incoming traps to internal SMP alarm
	 * object.
	 * 
	 * @param trap
	 *            the incoming trap
	 */
	public void recordSnmpTrap(SnmpTrap trap) {
		WarningLevel level = null;
		boolean ignored = false;
		try {
			level = WarningLevel.valueOf(trap.getLevel());
		} catch (Exception e) {
			level = WarningLevel.UNKNOWN;
		}

		AlarmMap alarmMap = null;
		if (level != null) {
			alarmMap = findTrapAlarmMapping(trap.getOid(), level);
		}

		Date now = new Date();
		Alarm newAlarm = null;
		AlarmDefinition alarmDef = null;

		// no alarm def and alarm def is disabled
		if (alarmMap == null
				|| (alarmMap != null && !alarmMap.getMappedAlarm().isEnabled())) {
			// proceeds to turn this trap into alarm
			newAlarm = createAlarm(trap);
			AlarmNotifier.instance.relayTrap(trap.getOriginal(), newAlarm);

			if (!triggersAlarm(level)) {
				return;
			}
		} else {
			// use mapped alarm
			alarmDef = alarmMap.getMappedAlarm();
			newAlarm = createAlarm(alarmDef, trap);
			AlarmNotifier.instance.relayTrap(trap.getOriginal(), newAlarm);
		}

		// check for existing entries, increment occurrence or clear
		if (newAlarm != null) {
			Alarm alarmEntry = DataAccess.alarmDao.getUnresolvedAlarm(
					newAlarm.getName(), newAlarm.getSource(),
					newAlarm.getSourceIp());
			if (alarmEntry == null) {
				// the alarm is not unresolved
				alarmEntry = DataAccess.alarmDao.getIgnoredAlarm(
						newAlarm.getName(), newAlarm.getSource(),
						newAlarm.getSourceIp());
				if (alarmEntry == null) {
					// the alarm is not unresolved & ignored, ie. it is new
					alarmEntry = newAlarm;
				} else {
					ignored = true;
				}
			} else {
				// the alarm is unresolved
				if (newAlarm.getLevel() == WarningLevel.CLEAR) {
					// not update the level
					// will have something like status=auto-clear, level=warning
					alarmEntry.setResolved(now);
					alarmEntry.setStatus(stCleared.getName());
					alarmEntry.setResolverId(AppConfig.getMyName());
				} else {
					// the alarm is still unresolved
					// occurrence increment will be performed in save method of
					// DAO
					if (alarmMap == null) {
						alarmEntry.setLevel(level);
					} else {
						// do nothing - use predefined level
					}
				}
			}
			// TODO
			// fix the logic - when the new alarm is not appeared before but is
			// a clear signal, will be set as unhandled in here
			if (alarmEntry != null) {
				alarmEntry.setLastOccurred(new Date());
				// update the alarm description to the newest trigger
				alarmEntry.setDescription(trap.getAlarmDescription());
				if (alarmMap == null) {
					// an unmapped alarm - treat the alarm level as the same as
					// that of incoming trap
					alarmEntry.setLevel(level);
				}
				// if status=ignored, still update record but do not report
				if (!StringUtil.streql(alarmEntry.getStatus(),
						stIgnored.getName())) {
					LiveMonitorReporter.getInstance().addHotAlarm(alarmEntry);

					// if cleared also notify
					// if (alarmEntry.getLastNotified() == null ||
					// trap.getTime().getTime()-alarmEntry.getLastNotified().getTime()>=ConstantUtil.HOUR_MS
					// || alarmEntry.getStatus().equals(stCleared.getName())) {
					//
					// alarmEntry.setLastNotified(now);
					// }
				}
				// records in db will be modified by multiple threads, may need
				// retry
				// default retry is 10 times, should set configurable
				saveAlarmEntry(alarmEntry);
			}
			// perform notification here - do not need to consider cooldown
			if (!ignored) {
				AlarmNotifier.instance.handleOutstandingAlarm(trap, alarmEntry);
			}
		}
	}

	private void saveAlarmEntry(Alarm alarmEntry) {
		boolean ok = false;
		for (int i = 0; i < AppConfig.getDBMaxRetry(); i++) {
			if (!ok) {
				try {
					DataAccess.alarmDao.save(alarmEntry);
					ok = true;
					break;
				} catch (Exception e) {
					// will be handled below
				}
			}
		}
		if (!ok) {
			logger.warn("failed in saving alarm" + alarmEntry);
		}
	}

	public boolean isDefaultTrapAlarmEnabled() {
		MiscConfig enabledCfg = MiscConfigService.instance
				.getConfig(MiscConfigService.key_defaultTrapAlarmEnabled);
		if (enabledCfg != null)
			return Boolean.parseBoolean(enabledCfg.getConfigValue());
		return true;
	}

	public void setDefaultTrapAlarmEnabled(boolean value) {
		MiscConfig enabledCfg = MiscConfigService.instance
				.getConfig(MiscConfigService.key_defaultTrapAlarmEnabled);
		if (enabledCfg == null) {
			enabledCfg = new MiscConfig(
					MiscConfigService.key_defaultTrapAlarmEnabled, null);
		}
		enabledCfg.setConfigValue(value + "");
		MiscConfigService.instance.saveConfig(enabledCfg);
	}

	public WarningLevel getDefaultTrapAlarmThreshold() {
		MiscConfig thresholdCfg = MiscConfigService.instance
				.getConfig(MiscConfigService.key_defaultTrapAlarmThreshold);
		if (thresholdCfg != null)
			return WarningLevel.valueOf(thresholdCfg.getConfigValue());
		return WarningLevel.WARNING;
	}

	public void setDefaultTrapAlarmThreshold(WarningLevel value) {
		MiscConfig thresholdCfg = MiscConfigService.instance
				.getConfig(MiscConfigService.key_defaultTrapAlarmThreshold);
		if (thresholdCfg == null) {
			thresholdCfg = new MiscConfig(
					MiscConfigService.key_defaultTrapAlarmThreshold, null);
		}
		thresholdCfg.setConfigValue(value.name());
		MiscConfigService.instance.saveConfig(thresholdCfg);
	}

	private boolean triggersAlarm(WarningLevel level) {
		boolean enabled = isDefaultTrapAlarmEnabled();
		WarningLevel threshold = getDefaultTrapAlarmThreshold();

		// String levelStr = trap.getTrapAlarmLevel();
		// WarningLevel level = null;
		// try {
		// level = WarningLevel.valueOf(levelStr);
		// } catch (Exception e) {
		// // not a defined trap level
		// }
		// default: level > WARNING
		// if (level == null || level.ordinal() >=
		// WarningLevel.WARNING.ordinal()) {
		// return true;
		// }

		if (enabled && level != null && level.ordinal() >= threshold.ordinal()) {
			return true;
		}

		return false;
	}

	private boolean isTriggerAlarmEnabled(SmpEvent event) {
		boolean enabled = isDefaultTrapAlarmEnabled();
		WarningLevel threshold = getDefaultTrapAlarmThreshold();

		WarningLevel level = event.getLevel();

		if (enabled && level != null && level.ordinal() >= threshold.ordinal()) {
			return true;
		}

		return false;
	}

	private Alarm createAlarm(SnmpTrap trap) {
		Alarm alarm = new Alarm();
		alarm.setCause(trap.getOid());
		alarm.setDescription(trap.getTrapDescription());
		Date now = trap.getTime();
		alarm.setFirstOccurred(now);
		alarm.setLastOccurred(now);
		alarm.setName(trap.getTrapName());
		WarningLevel level = null;
		try {
			level = WarningLevel.valueOf(trap.getLevel());
		} catch (Exception e) {
			level = WarningLevel.UNKNOWN;
		}
		alarm.setLevel(level);
		alarm.setOccurrence(1);
		alarm.setOccurrenceCount(1);
		alarm.setSource(trap.getSource());
		alarm.setSourceIp(trap.getSourceIp());
		if (!alarm.getLevel().equals(WarningLevel.CLEAR)) {
			alarm.setStatus(getDefaultAlarmStatus());
		} else {
			alarm.setStatus(stCleared.getName());
		}
		return alarm;
	}

	private Alarm createAlarm(SmpEvent event) {
		Alarm alarm = new Alarm();
		alarm.setCause(event.getEventTypeStr());
		alarm.setDescription(event.getDetails());
		Date now = new Date();
		alarm.setFirstOccurred(now);
		alarm.setLastOccurred(now);
		alarm.setName(event.getName());
		WarningLevel level = null;
		if (event.getLevel() != null) {
			level = event.getLevel();
		} else {
			level = WarningLevel.UNKNOWN;
		}
		alarm.setLevel(level);
		alarm.setOccurrence(1);
		alarm.setOccurrenceCount(1);
		alarm.setSource(event.getSource());
		if (event instanceof LocalSystemEvent) {
			alarm.setSourceIp(EventDelegate.LOCALHOST);
		} else if (event instanceof RemoteAppEvent) {
			RemoteAppEvent dummy = (RemoteAppEvent) event;
			alarm.setSourceIp(dummy.getHost().getIpAddr());
		} else if (event instanceof RemoteHostEvent) {
			RemoteHostEvent dummy = (RemoteHostEvent) event;
			alarm.setSourceIp(dummy.getSourceIp());
		}
		if (!alarm.getLevel().equals(WarningLevel.CLEAR)) {
			alarm.setStatus(getDefaultAlarmStatus());
		} else {
			alarm.setStatus(stCleared.getName());
		}
		return alarm;
	}

	private Alarm createAlarm(AlarmDefinition alarmDef, SmpEvent event) {
		Alarm alarm = createAlarm(event);
		alarm.setName(alarmDef.getName());
		alarm.setLevel(alarmDef.getLevel());
		if (alarm.getLevel() == WarningLevel.CLEAR) {
			alarm.setResolved(new Date());
			alarm.setResolverId(AppConfig.getMyName());
			alarm.setStatus(stCleared.getName());
		}
		return alarm;
	}

	private Alarm createAlarm(AlarmDefinition alarmDef, SnmpTrap trap) {
		Alarm alarm = createAlarm(trap);
		alarm.setName(alarmDef.getName());
		alarm.setLevel(alarmDef.getLevel());
		if (alarm.getLevel() == WarningLevel.CLEAR) {
			alarm.setResolved(new Date());
			alarm.setResolverId(AppConfig.getMyName());
			alarm.setStatus(stCleared.getName());
		}
		return alarm;
	}

	// private Alarm createAlarm(EventLog event) {
	// Alarm alarm = new Alarm();
	// alarm.setCause(event.getEventName());
	// alarm.setDescription(event.getDetails());
	// Date now = new Date();
	// alarm.setFirstOccurred(now);
	// alarm.setLastOccurred(now);
	// alarm.setName(event.getEventName());
	// alarm.setOccurrence(1);
	// alarm.setOccurrenceCount(1);
	// alarm.setSource(event.getSource());
	// alarm.setSourceIp("");
	// alarm.setStatus(getDefaultAlarmStatus());
	// return alarm;
	// }

	public List<Alarm> getUnresolvedAlarms() {
		if (System.currentTimeMillis() - lastUpdatedUnresolvedAlarms > AppConfig
				.getWebMonitorPollInterval()) {
			lastUpdatedUnresolvedAlarms = System.currentTimeMillis();
			unresolvedAlarms = DataAccess.alarmDao.getUnhandledAlarms();
		}

		return unresolvedAlarms;
	}
}
