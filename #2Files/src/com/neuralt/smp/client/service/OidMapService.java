package com.neuralt.smp.client.service;

public class OidMapService {

	private OidMapService() {
	}

	private static OidMapService instance;

	public static OidMapService getInstance() {
		if (instance == null) {
			synchronized (OidMapService.class) {
				if (instance == null) {
					instance = new OidMapService();
				}
			}
		}
		return instance;
	}
}
