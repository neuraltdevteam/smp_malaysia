package com.neuralt.smp.client.config.xml.copy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class CopyOfAction implements Serializable {

	private static final long serialVersionUID = -4104732113322063068L;

	@XmlAttribute(name = "priority")
	public Integer priority;
	@XmlAttribute(name = "script")
	public String script;
	@XmlAttribute(name = "timeout")
	public Integer timeout;
	@XmlAttribute(name = "outputLimit")
	public Integer outputLimit;
	@XmlAttribute(name = "auto")
	public Boolean auto;

	public Integer getPriority() {
		return priority;
	}

	public String getScript() {
		return script;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Action [priority=").append(priority)
				.append(", script=").append(script).append(", timeout=")
				.append(timeout).append(", outputLimit=").append(outputLimit)
				.append(", auto=").append(auto).append("]");
		return builder.toString();
	}

}
