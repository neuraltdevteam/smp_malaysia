package com.neuralt.smp.client.config.xml;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class Host implements Serializable {

	private static final long serialVersionUID = -7224028943191345405L;

	@XmlAttribute(name = "name")
	public String name;
	@XmlAttribute(name = "ip")
	public String ip;
	@XmlAttribute(name = "jmxPort")
	public String jmxPort;
	@XmlAttribute(name = "desc")
	public String desc;
	@XmlAttribute(name = "role")
	public String role;
	@XmlAttribute(name = "enabled")
	public Boolean enabled;

	@XmlElementWrapper(name = "commands")
	@XmlElement(name = "command")
	public List<Command> commands;

	@XmlElementWrapper(name = "monitors")
	@XmlElement(name = "monitor")
	public List<HostMonitor> monitors;

	@XmlElementWrapper(name = "processes")
	@XmlElement(name = "process")
	public List<Process> processes;

	public String getName() {
		return name;
	}

	public String getIp() {
		return ip;
	}

	public String getJmxPort() {
		return jmxPort;
	}

	public String getDesc() {
		return desc;
	}

	public String getRole() {
		return role;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public List<Process> getProcesses() {
		return processes;
	}

	public List<HostMonitor> getMonitors() {
		return monitors;
	}

	public List<Command> getCommands() {
		return commands;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Host [name=").append(name).append(", ip=").append(ip)
				.append(", jmxPort=").append(jmxPort).append(", desc=")
				.append(desc).append(", role=").append(role)
				.append(", enabled=").append(enabled).append(", monitors=")
				.append(monitors).append(", processes=").append(processes)
				.append("]");
		return builder.toString();
	}

}
