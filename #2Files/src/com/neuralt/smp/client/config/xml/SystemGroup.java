package com.neuralt.smp.client.config.xml;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "systems")
public class SystemGroup implements Serializable {

	private static final long serialVersionUID = 3833336740268076659L;

	@XmlElement(name = "system")
	public List<System> systems;

	public List<System> getSystems() {
		return systems;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SystemGroup [systems=").append(systems).append("]");
		return builder.toString();
	}

}
