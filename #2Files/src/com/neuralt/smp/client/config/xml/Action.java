package com.neuralt.smp.client.config.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class Action implements Serializable {

	private static final long serialVersionUID = -4104732113322063068L;

	@XmlAttribute(name = "priority")
	public Integer priority;
	@XmlAttribute(name = "script")
	public String script;

	public Integer getPriority() {
		return priority;
	}

	public String getScript() {
		return script;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Action [priority=").append(priority)
				.append(", script=").append(script).append("]");
		return builder.toString();
	}

}
