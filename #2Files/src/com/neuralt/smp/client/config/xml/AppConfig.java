package com.neuralt.smp.client.config.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class AppConfig implements Serializable {

	private static final long serialVersionUID = -1190025499004706467L;

	@XmlAttribute(name = "location")
	public String location;
	@XmlAttribute(name = "sync")
	public Boolean sync;

	public String getLocation() {
		return location;
	}

	public Boolean getSync() {
		return sync;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AppConfig [location=").append(location)
				.append(", sync=").append(sync).append("]");
		return builder.toString();
	}
}
