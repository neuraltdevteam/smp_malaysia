package com.neuralt.smp.client.config.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class Threshold implements Serializable {

	private static final long serialVersionUID = -2309627056687361008L;

	@XmlAttribute(name = "target")
	public String target;
	@XmlAttribute(name = "watermark")
	public Float watermark;
	@XmlAttribute(name = "op")
	public String operator;
	@XmlAttribute(name = "level")
	public String level;

	public String getTarget() {
		return target;
	}

	public Float getWatermark() {
		return watermark;
	}

	public String getOperator() {
		return operator;
	}

	public String getLevel() {
		return level;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Threshold [target=").append(target)
				.append(", watermark=").append(watermark).append(", operator=")
				.append(operator).append(", level=").append(level).append("]");
		return builder.toString();
	}

}
