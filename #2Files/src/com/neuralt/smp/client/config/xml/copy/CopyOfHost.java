package com.neuralt.smp.client.config.xml.copy;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import com.neuralt.smp.client.config.xml.Command;
import com.neuralt.smp.client.config.xml.HostMonitor;
import com.neuralt.smp.client.config.xml.SnmpConfig;

public class CopyOfHost implements Serializable {

	private static final long serialVersionUID = -7224028943191345405L;

	@XmlAttribute(name = "name")
	public String name;
	@XmlAttribute(name = "ip")
	public String ip;
	@XmlAttribute(name = "jmxPort")
	public int jmxPort;
	@XmlAttribute(name = "monitorType")
	public String type;
	@XmlAttribute(name = "desc")
	public String desc;
	@XmlAttribute(name = "role")
	public String role;
	@XmlAttribute(name = "enabled")
	public Boolean enabled;
	@XmlAttribute(name = "agentDeployed")
	public Boolean agentDeployed;

	@XmlElement(name = "snmpConfig")
	public SnmpConfig snmpConfig;

	@XmlElementWrapper(name = "commands")
	@XmlElement(name = "command")
	public List<Command> commands;

	@XmlElementWrapper(name = "monitors")
	@XmlElement(name = "monitor")
	public List<HostMonitor> monitors;

	@XmlElementWrapper(name = "processes")
	@XmlElement(name = "process")
	public List<CopyOfProcess> processes;

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Host [name=").append(name).append(", ip=").append(ip)
				.append(", jmxPort=").append(jmxPort).append(", desc=")
				.append(desc).append(", role=").append(role).append(", type=")
				.append(type).append(", agentDeployed").append(agentDeployed)
				.append(", enabled=").append(enabled).append(", monitors=")
				.append(monitors).append(", processes=").append(processes)
				.append("]");
		return builder.toString();
	}

}
