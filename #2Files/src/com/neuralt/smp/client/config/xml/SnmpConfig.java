package com.neuralt.smp.client.config.xml;

import javax.xml.bind.annotation.XmlAttribute;

public class SnmpConfig {

	@XmlAttribute(name = "communityGet")
	private String communityGet;
	@XmlAttribute(name = "communityTrap")
	private String communityTrap;
	@XmlAttribute(name = "communitySet")
	private String communitySet;
	@XmlAttribute(name = "username")
	private String username;
	@XmlAttribute(name = "engineId")
	private String engineId;
	@XmlAttribute(name = "authPassword")
	private String authPassword;
	@XmlAttribute(name = "privPassword")
	private String privPassword;
	@XmlAttribute(name = "auth")
	private String auth;
	@XmlAttribute(name = "priv")
	private String priv;
	@XmlAttribute(name = "port")
	private Integer port;

	public String getCommunityGet() {
		return communityGet;
	}

	public void setCommunityGet(String communityGet) {
		this.communityGet = communityGet;
	}

	public String getCommunityTrap() {
		return communityTrap;
	}

	public void setCommunityTrap(String communityTrap) {
		this.communityTrap = communityTrap;
	}

	public String getCommunitySet() {
		return communitySet;
	}

	public void setCommunitySet(String communitySet) {
		this.communitySet = communitySet;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEngineId() {
		return engineId;
	}

	public void setEngineId(String engineId) {
		this.engineId = engineId;
	}

	public String getAuthPassword() {
		return authPassword;
	}

	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}

	public String getPrivPassword() {
		return privPassword;
	}

	public void setPrivPassword(String privPassword) {
		this.privPassword = privPassword;
	}

	public String getAuth() {
		return auth;
	}

	public void setAuth(String auth) {
		this.auth = auth;
	}

	public String getPriv() {
		return priv;
	}

	public void setPriv(String priv) {
		this.priv = priv;
	}

	public Integer getPort() {
		return port;
	}

	public void setPort(Integer port) {
		this.port = port;
	}
}
