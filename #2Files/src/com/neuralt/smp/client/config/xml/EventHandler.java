package com.neuralt.smp.client.config.xml;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class EventHandler implements Serializable {

	private static final long serialVersionUID = 6291906035482015962L;

	@XmlAttribute(name = "event")
	public String event;
	@XmlElement(name = "snmp-trap")
	public SNMPTrap snmpTrap;
	@XmlElementWrapper(name = "actions")
	@XmlElement(name = "action")
	public List<Action> actions;

	public String getEvent() {
		return event;
	}

	public SNMPTrap getSnmp() {
		return snmpTrap;
	}

	public List<Action> getActions() {
		return actions;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EventHandler [event=").append(event)
				.append(", snmpTrap=").append(snmpTrap).append(", actions=")
				.append(actions).append("]");
		return builder.toString();
	}
}
