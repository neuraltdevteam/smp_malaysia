package com.neuralt.smp.client.config.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class Command implements Serializable {
	private static final long serialVersionUID = 2581373775186375926L;

	@XmlAttribute(name = "name")
	public String name;
	@XmlAttribute(name = "script")
	public String script;

	public String getName() {
		return name;
	}

	public String getScript() {
		return script;
	}
}
