package com.neuralt.smp.client.config.xml;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class System implements Serializable {

	private static final long serialVersionUID = 5207452204132219819L;

	@XmlAttribute(name = "name")
	public String name;
	@XmlAttribute(name = "desc")
	public String desc;
	@XmlAttribute(name = "enabled")
	public Boolean enabled;
	@XmlElementWrapper(name = "hosts")
	@XmlElement(name = "host")
	public List<Host> hosts;

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public List<Host> getHosts() {
		return hosts;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("System [name=").append(name).append(", desc=")
				.append(desc).append(", enabled=").append(enabled)
				.append(", hosts=").append(hosts).append("]");
		return builder.toString();
	}

}
