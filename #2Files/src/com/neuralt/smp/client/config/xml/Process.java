package com.neuralt.smp.client.config.xml;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class Process implements Serializable {

	private static final long serialVersionUID = 2463554051723498942L;

	@XmlAttribute(name = "name")
	public String name;
	@XmlAttribute(name = "desc")
	public String desc;
	@XmlAttribute(name = "enabled")
	public Boolean enabled;
	@XmlAttribute(name = "instance")
	public Integer instance;
	@XmlAttribute(name = "priority")
	public Integer priority;
	@XmlAttribute(name = "startup")
	public String startup;
	@XmlAttribute(name = "shutdown")
	public String shutdown;

	@XmlElement(name = "monitor")
	public Monitor monitor;
	@XmlElementWrapper(name = "event-handlers")
	@XmlElement(name = "event-handler")
	public List<EventHandler> eventHandlers;
	@XmlElementWrapper(name = "app-configs")
	@XmlElement(name = "app-config")
	public List<AppConfig> appConfigs;

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public Integer getInstance() {
		return instance;
	}

	public Integer getPriority() {
		return priority;
	}

	public String getStartup() {
		return startup;
	}

	public String getShutdown() {
		return shutdown;
	}

	public Monitor getMonitor() {
		return monitor;
	}

	public List<EventHandler> getEventHandlers() {
		return eventHandlers;
	}

	public List<AppConfig> getAppConfigs() {
		return appConfigs;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Process [name=").append(name).append(", desc=")
				.append(desc).append(", enabled=").append(enabled)
				.append(", instance=").append(instance).append(", priority=")
				.append(priority).append(", startup=").append(startup)
				.append(", shutdown=").append(shutdown).append(", monitor=")
				.append(monitor).append(", eventHandlers=")
				.append(eventHandlers).append(", appConfigs=")
				.append(appConfigs).append("]");
		return builder.toString();
	}

}