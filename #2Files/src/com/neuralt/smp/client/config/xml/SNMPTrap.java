package com.neuralt.smp.client.config.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class SNMPTrap implements Serializable {

	private static final long serialVersionUID = 1552126927500083585L;

	@XmlAttribute(name = "oid")
	public String oid;
	@XmlAttribute(name = "trap")
	public String trap;
	@XmlAttribute(name = "message")
	public String message;

	public String getOid() {
		return oid;
	}

	public String getTrap() {
		return trap;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SNMPTrap [oid=").append(oid).append(", trap=")
				.append(trap).append(", message=").append(message).append("]");
		return builder.toString();
	}

}
