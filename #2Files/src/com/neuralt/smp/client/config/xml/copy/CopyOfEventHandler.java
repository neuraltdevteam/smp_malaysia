package com.neuralt.smp.client.config.xml.copy;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class CopyOfEventHandler implements Serializable {

	private static final long serialVersionUID = 6291906035482015962L;

	@XmlAttribute(name = "event")
	public String event;
	@XmlElementWrapper(name = "actions")
	@XmlElement(name = "action")
	public List<CopyOfAction> actions;

	public String getEvent() {
		return event;
	}

	public List<CopyOfAction> getActions() {
		return actions;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EventHandler [event=").append(event)
				.append(", actions=").append(actions).append("]");
		return builder.toString();
	}
}
