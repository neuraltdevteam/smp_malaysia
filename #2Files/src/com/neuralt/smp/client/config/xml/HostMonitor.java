package com.neuralt.smp.client.config.xml;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class HostMonitor implements Serializable {

	private static final long serialVersionUID = -3208722630811691452L;
	@XmlAttribute(name = "name")
	public String name;
	@XmlAttribute(name = "script")
	public String script;
	@XmlAttribute(name = "interval")
	public Integer interval;

	@XmlElementWrapper(name = "thresholds")
	@XmlElement(name = "threshold")
	public List<Threshold> thresholds;

	public String getName() {
		return name;
	}

	public String getScript() {
		return script;
	}

	public Integer getInterval() {
		return interval;
	}

	public List<Threshold> getThresholds() {
		return thresholds;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("HostMonitor [name=").append(name).append(", script=")
				.append(script).append(", interval=").append(interval)
				.append(", thresholds=").append(thresholds).append("]");
		return builder.toString();
	}

}
