package com.neuralt.smp.client.config.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAttribute;

public class Script implements Serializable {

	private static final long serialVersionUID = 2326950426921063327L;
	@XmlAttribute(name = "name")
	public String name;
	@XmlAttribute(name = "desc")
	public String desc;
	@XmlAttribute(name = "path")
	public String path;

	public String getName() {
		return name;
	}

	public String getDesc() {
		return desc;
	}

	public String getPath() {
		return path;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Script [name=").append(name).append(", desc=")
				.append(desc).append(", path=").append(path).append("]");
		return builder.toString();
	}

}
