package com.neuralt.smp.client.config.xml;

import java.io.Serializable;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

public class Monitor implements Serializable {

	private static final long serialVersionUID = -8574233645850070909L;

	@XmlAttribute(name = "pid")
	public String pid;
	@XmlAttribute(name = "interval")
	public Integer interval;

	@XmlElementWrapper(name = "thresholds")
	@XmlElement(name = "threshold")
	public List<Threshold> thresholds;

	public String getPid() {
		return pid;
	}

	public Integer getInterval() {
		return interval;
	}

	public List<Threshold> getThresholds() {
		return thresholds;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Monitor [pid=").append(pid).append(", interval=")
				.append(interval).append(", thresholds=").append(thresholds)
				.append("]");
		return builder.toString();
	}

}
