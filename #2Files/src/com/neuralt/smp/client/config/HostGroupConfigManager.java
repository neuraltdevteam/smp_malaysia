package com.neuralt.smp.client.config;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.config.xml.Command;
import com.neuralt.smp.client.config.xml.Host;
import com.neuralt.smp.client.config.xml.HostMonitor;
import com.neuralt.smp.client.config.xml.Monitor;
import com.neuralt.smp.client.config.xml.SystemGroup;
import com.neuralt.smp.client.config.xml.Threshold;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.config.CommandConfig;
import com.neuralt.smp.config.CommandConfig.CommandName;
import com.neuralt.smp.config.EventType;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.HostMonitorName;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.config.HostConfig.HostRole;
import com.neuralt.smp.config.ProcessConfig;
import com.neuralt.smp.config.ProcessConfig.EventHandlerConfig;
import com.neuralt.smp.config.SystemConfig;
import com.neuralt.smp.util.PropertiesUtil;

public class HostGroupConfigManager {

	private static Logger logger = LoggerFactory
			.getLogger(HostGroupConfigManager.class);

	public static SystemGroup loadHostGroupConfig(String filename)
			throws Exception {
		SystemGroup hostGroup;

		try {
			filename = PropertiesUtil.parsePropertiesValue(filename);

			logger.info("Loading system group configuration: " + filename);

			File file = new File(filename);

			JAXBContext jaxbContext = JAXBContext
					.newInstance(SystemGroup.class);
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			hostGroup = (SystemGroup) jaxbUnmarshaller.unmarshal(file);
			logger.info("Loaded host group configuration: " + filename);

			return hostGroup;

		} catch (Exception e) {
			logger.error("Failed to load file: {}", filename, e);
			throw e;
		}
	}

	public static List<SystemConfig> toInternalFormatConfig(SystemGroup sysGroup) {
		List<SystemConfig> sysConfigs = new ArrayList<SystemConfig>();
		if (sysGroup != null) {
			for (com.neuralt.smp.client.config.xml.System sys : sysGroup
					.getSystems()) {
				SystemConfig sysConfig = new SystemConfig();
				sysConfig.setDescription(sys.getDesc());
				sysConfig.setEnabled(sys.getEnabled());
				sysConfig.setName(sys.getName());

				for (Host host : sys.getHosts()) {
					HostConfig hostConfig = new HostConfig();
					hostConfig.setAgentJmxPort(Integer.parseInt(host
							.getJmxPort()));
					hostConfig.setDescription(host.getDesc());
					hostConfig.setEnabled(host.getEnabled());
					hostConfig.setIpAddr(host.getIp());
					hostConfig.setName(host.getName());
					hostConfig.setRole(toHostRole(host.getRole()));

					// TODO
					// It seems complicate to set commands (which may be
					// abandoned) and monitors. Maybe here should auto-generate.

					for (Command command : host.getCommands()) {
						CommandConfig config = new CommandConfig();
						config.setName(toCommondName(command.getName()));
						config.setScript(command.getScript());
						config.setHostConfig(hostConfig);
						hostConfig.getCommandConfigs().put(config.getName(),
								config);
					}

					Map<HostMonitorName, HostMonitorConfig> monConfig = new HashMap<HostMonitorName, HostMonitorConfig>();
					for (HostMonitor monitor : host.getMonitors()) {
						Set<MonitorThresholdConfig> thresholdConfig = new TreeSet<MonitorThresholdConfig>();
						if (monitor.getThresholds() != null) {
							for (Threshold threshold : monitor.getThresholds()) {
								MonitorThresholdConfig monitorThresholdConfig = new MonitorThresholdConfig(
										threshold.getTarget(),
										threshold.getWatermark(),
										threshold.getOperator(),
										threshold.getLevel());
								thresholdConfig.add(monitorThresholdConfig);
							}
						}

						HostMonitorName hostMonitor = toHostMonitorName(monitor
								.getName());

						monConfig.put(
								hostMonitor,
								new HostMonitorConfig(hostMonitor, monitor
										.getScript(), monitor.getInterval(),
										thresholdConfig));
						for (MonitorThresholdConfig monitorThresholdConfig : thresholdConfig) {
							monitorThresholdConfig
									.setHostMonitorConfig(monConfig
											.get(hostMonitor));
						}
					}
					hostConfig.setMonitorConfig(monConfig);

					for (com.neuralt.smp.client.config.xml.Process proc : host
							.getProcesses()) {
						ProcessConfig procConfig = new ProcessConfig();
						procConfig.setDescription(proc.getDesc());
						procConfig.setEnabled(proc.getEnabled());
						procConfig.setName(proc.getName());
						procConfig.setStartScriptPath(proc.getStartup());
						procConfig.setStopScriptPath(proc.getShutdown());
						Monitor mon = proc.getMonitor();
						if (mon != null) {
							// procConfig.setMonIntervalMs(mon.getInterval() *
							// 1000);
							procConfig.setPidPath(mon.getPid());
						}

						if (proc.getEventHandlers() != null) {
							for (com.neuralt.smp.client.config.xml.EventHandler evthlr : proc
									.getEventHandlers()) {
								EventHandlerConfig evthlrConfig = new EventHandlerConfig();
								evthlrConfig.setEventType(toEventType(evthlr
										.getEvent()));

								// com.neuralt.smp.client.config.xml.SNMPTrap snmp
								// = evthlr.getSnmp();
								// if (snmp != null) {
								// SNMPConfig snmpConfig = new SNMPConfig();
								// snmpConfig.setMessage(snmp.getMessage());
								// snmpConfig.setOid(snmp.getOid());
								// snmpConfig.setTrap(snmp.getTrap());
								// snmpConfig.setEventHandlerConfig(evthlrConfig);
								// evthlrConfig.setSnmp(snmpConfig);
								// }
								// if (evthlr.getActions() != null) {
								// for (com.neuralt.smp.client.config.xml.Action
								// action : evthlr.getActions()) {
								// ActionConfig actionConfig = new
								// ActionConfig();
								// actionConfig.setCommand(action.getScript());
								// actionConfig.setPriority(action.getPriority());
								// actionConfig.setEventHandlerConfig(evthlrConfig);
								// evthlrConfig.addAction(actionConfig);
								// }
								// }
								evthlrConfig.setProcessConfig(procConfig);
								procConfig.addEventHandler(evthlrConfig);
							}
						}
						procConfig.setHostConfig(hostConfig);
						hostConfig.addProcessConfig(procConfig);
					}
					hostConfig.setSystemConfig(sysConfig);
					sysConfig.addHost(hostConfig);

				}
				// uncomment below to enable writing data to db (only for first
				// time use)
				DataAccess.systemConfigDao.save(sysConfig);
				sysConfigs.add(sysConfig);
			}
		}

		return sysConfigs;
	}

	public static List<SystemConfig> loadSystemConfig() throws Exception {
		List<SystemConfig> dbSysConfigs = DataAccess.systemConfigDao
				.getAllSystemConfigs();
		logger.info("Loaded DB system configs: {}", dbSysConfigs);

		// if (dbSysConfigs.isEmpty())
		// return toInternalFormatConfig(loadHostGroupConfig(xmlPath));

		return dbSysConfigs;
	}

	private static EventType toEventType(String str) {
		String upperStr = str.toUpperCase();
		try {
			return EventType.valueOf(upperStr);
		} catch (Exception e) {
			return null;
		}
	}

	private static HostRole toHostRole(String str) {
		String upperStr = str.toUpperCase();
		try {
			return HostRole.valueOf(upperStr);
		} catch (Exception e) {
			return null;
		}
	}

	private static HostMonitorName toHostMonitorName(String str) {
		try {
			for (HostMonitorName monitorName : HostMonitorName.values()) {
				if (monitorName.getName().equals(str))
					return monitorName;
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

	private static CommandName toCommondName(String str) {
		try {
			for (CommandName commandName : CommandName.values()) {
				if (commandName.getName().equals(str))
					return commandName;
			}
		} catch (Exception e) {
			return null;
		}
		return null;
	}

}
