package com.neuralt.smp.client.event;

import java.io.Serializable;
import java.util.Date;

import com.neuralt.smp.notification.WarningLevel;

// TODO add TrapReceivedEvent subclass?
public abstract class SmpEvent implements Serializable {

	private static final long serialVersionUID = -7583061652882792867L;

	protected Date eventTime;
	protected WarningLevel level;
	protected String details;

	public Date getEventTime() {
		return eventTime;
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	public WarningLevel getLevel() {
		return level;
	}

	public void setLevel(WarningLevel level) {
		this.level = level;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getName() {
		return this.getClass().getSimpleName();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((details == null) ? 0 : details.hashCode());
		result = prime * result
				+ ((getSource() == null) ? 0 : getSource().hashCode());
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SmpEvent other = (SmpEvent) obj;
		if (details == null) {
			if (other.details != null)
				return false;
		} else if (!details.equals(other.details))
			return false;
		if (level != other.level)
			return false;
		if (getSource() == null) {
			if (other.getSource() != null)
				return false;
		} else if (!getSource().equals(other.getSource()))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SmpEvent [level=").append(level).append(", details=")
				.append(details).append(", getSource()=").append(getSource())
				.append("]");
		return builder.toString();
	}

	public abstract EventType getEventType();

	public abstract String getEventTypeStr();

	public abstract String getSource();

	// trying to build a way to allow event identification without combining
	// each subclass's EventSubType enum into a single EventType enum in
	// SmpEvent class
	public static class EventType implements Serializable {

		private static final long serialVersionUID = -6107094878057635193L;

		public Class<? extends SmpEvent> eventClass;
		public int subTypeOrdinal;
		public String subTypeName;

		public EventType(Class<? extends SmpEvent> eventClass,
				int subTypeOrdinal, String subTypeName) {
			this.eventClass = eventClass;
			this.subTypeOrdinal = subTypeOrdinal;
			this.subTypeName = subTypeName;
		}

		public String getEventClassName() {
			return eventClass != null ? eventClass.getSimpleName() : null;
		}

		public String getEventSubTypeName() {
			return subTypeName;
		}

		// used as causeId
		public String toString() {
			if (eventClass == null)
				return null;
			return eventClass.getSimpleName() + " - " + subTypeName;
		}

		public String asCauseId() {
			return toString();
		}

		public boolean equals(Object obj) {
			if (obj instanceof EventType) {
				EventType other = (EventType) obj;
				return this.eventClass == other.eventClass
						&& this.subTypeOrdinal == other.subTypeOrdinal;
			}
			return false;
		}
	}
}
