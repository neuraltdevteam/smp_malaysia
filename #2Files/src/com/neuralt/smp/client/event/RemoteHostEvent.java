package com.neuralt.smp.client.event;

import java.util.Date;

import com.neuralt.smp.agent.mbean.model.HealthUsage;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.notification.WarningLevel;

public class RemoteHostEvent extends SmpEvent {

	private static final long serialVersionUID = 6607385983221273437L;

	public static enum EventSubType {
		AGENT_CONNECTED, AGENT_DISCONNECTED, HOST_UP, HOST_DOWN, SNMP_ACCESS_ERROR, CPU_USAGE, MEM_USAGE, DISK_USAGE, INET_USAGE, INET_DOWN, UPTIME, GENERAL_ERROR;

		public EventType eventType;

		private EventSubType() {
			eventType = new EventType(RemoteHostEvent.class, this.ordinal(),
					this.name());
		}
	}

	private EventSubType eventSubType;
	private MonitoredHost host;
	private HealthUsage usage;

	public RemoteHostEvent() {
		super();
	}

	public RemoteHostEvent(EventSubType evtType, MonitoredHost host, Date time,
			WarningLevel level, String details) {
		this();
		this.eventSubType = evtType;
		this.host = host;
		this.eventTime = time;
		this.level = level;
		this.details = details;
	}

	public EventSubType getEventSubType() {
		return eventSubType;
	}

	public void setEventSubType(EventSubType eventType) {
		this.eventSubType = eventType;
	}

	public MonitoredHost getHost() {
		return host;
	}

	public void setHost(MonitoredHost host) {
		this.host = host;
	}

	public HealthUsage getUsage() {
		return usage;
	}

	public void setUsage(HealthUsage usage) {
		this.usage = usage;
	}

	@Override
	public String getEventTypeStr() {
		return eventSubType != null ? eventSubType.name() : null;
	}

	@Override
	public String getSource() {
		return host != null ? host.getConfig().getName() : null;
	}

	public String getSourceIp() {
		return host != null ? host.getIpAddr() : null;
	}

	@Override
	public EventType getEventType() {
		return eventSubType != null ? eventSubType.eventType : null;
	}
}
