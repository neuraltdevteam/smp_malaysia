package com.neuralt.smp.client.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.EventLog;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.SourceType;
import com.neuralt.smp.client.util.EventDelegate;
import com.neuralt.smp.notification.WarningLevel;

public class DefaultRemoteAppEventHandler extends AbstractEventHandler {

	private static final Logger logger = LoggerFactory
			.getLogger(DefaultRemoteAppEventHandler.class);

	@Override
	public boolean accepts(SmpEvent evt) {
		return evt instanceof RemoteAppEvent;
	}

	@Override
	public SnmpTrap toTrap(SmpEvent evt, WarningLevel warningLevel) {
		RemoteAppEvent raEvt = (RemoteAppEvent) evt;
		MonitoredHost host = raEvt.getHost();
		String appName = raEvt.getAppName();
		String details = raEvt.getDetails();
		SnmpTrap trap = null;

		switch (raEvt.getEventSubType()) {
		case APP_DIED:
			trap = EventDelegate.instance.onRemAppNotPresent(host, appName,
					details, warningLevel);
			break;
		case APP_START:
			trap = EventDelegate.instance.onRemAppStart(host, appName,
					warningLevel);
			break;
		case APP_START_FAILED:
			trap = EventDelegate.instance.onRemAppStartFail(host, appName,
					details, warningLevel);
			break;
		case APP_START_SUCCESS:
			trap = EventDelegate.instance.onRemAppStartSuccess(host, appName,
					details, warningLevel);
			break;
		case APP_STOP:
			trap = EventDelegate.instance.onRemAppStop(host, appName,
					warningLevel);
			break;
		case APP_STOP_FAILED:
			trap = EventDelegate.instance.onRemAppStopFail(host, appName,
					details, warningLevel);
			// TODO case not yet detectable
			// logger.warn("unhandled eventType: "+raEvt.getEventTypeStr());
			break;
		case APP_STOP_SUCCESS:
			// TODO case not yet detectable
			trap = EventDelegate.instance.onRemAppStopSuccess(host, appName,
					details, warningLevel);
			// logger.warn("unhandled eventType: "+raEvt.getEventTypeStr());
			break;
		default:
			logger.warn("unhandled eventType: " + raEvt.getEventTypeStr());
			break;
		}

		// recordEvent(raEvt);
		return trap;
	}

	public void recordEvent(SmpEvent evt) {
		RemoteAppEvent raEvt = (RemoteAppEvent) evt;
		EventLog evtLog = new EventLog();
		evtLog.setEventTime(raEvt.getEventTime());
		evtLog.setEventName(raEvt.getEventTypeStr());
		evtLog.setDetails(raEvt.getDetails());
		evtLog.setLevel(raEvt.getLevel());
		evtLog.setSysName(raEvt.getHost().getSys().getName());
		evtLog.setHostName(raEvt.getHost().getConfig().getName());
		evtLog.setProcName(raEvt.getAppName());
		evtLog.setSourceType(SourceType.PROCESS);
		DataAccess.eventLogDao.save(evtLog);
	}
}
