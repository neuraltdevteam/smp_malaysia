package com.neuralt.smp.client.event;

import java.util.Date;

import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.notification.WarningLevel;

public class LocalSystemEvent extends SmpEvent {

	private static final long serialVersionUID = -3868265786915876328L;

	public static enum EventSubType {
		GENERAL_ERROR, // general internal error
		SYS_STARTUP_SUCCESS, SYS_STARTUP_FAILED, SYS_SHUTDOWN, CONFIG_RELOAD, CONFIG_RELOAD_ERROR;

		public EventType eventType;

		private EventSubType() {
			eventType = new EventType(LocalSystemEvent.class, this.ordinal(),
					this.name());
		}
	}

	private EventSubType eventSubType;

	public LocalSystemEvent() {
		super();
	}

	public LocalSystemEvent(EventSubType evtType, Date time,
			WarningLevel level, String details) {
		this();
		this.eventSubType = evtType;
		this.eventTime = time;
		this.level = level;
		this.details = details;
	}

	public EventSubType getEventSubType() {
		return eventSubType;
	}

	public void setEventSubType(EventSubType eventType) {
		this.eventSubType = eventType;
	}

	@Override
	public String getEventTypeStr() {
		return eventSubType != null ? eventSubType.name() : null;
	}

	@Override
	public String getSource() {
		return AppConfig.getMyName();
	}

	@Override
	public EventType getEventType() {
		return eventSubType != null ? eventSubType.eventType : null;
	}
}
