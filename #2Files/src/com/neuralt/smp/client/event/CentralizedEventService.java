package com.neuralt.smp.client.event;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.agent.mbean.model.HostCPUUsage;
import com.neuralt.smp.agent.mbean.model.HostDiskUsage;
import com.neuralt.smp.agent.mbean.model.HostInetStatus;
import com.neuralt.smp.agent.mbean.model.HostMemoryUsage;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.event.SmpEvent.EventType;
import com.neuralt.smp.client.service.AlarmService;
import com.neuralt.smp.client.util.FixedLengthLinkedHashMap;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.notification.WarningLevel;

/**
 * Centralized event handling class
 * 
 * @author
 * 
 */
public class CentralizedEventService {
	private static final int DEFAULT_EVENT_FLOW_CONTROL_IN_MIN = 5;

	private static final Logger logger = LoggerFactory
			.getLogger(CentralizedEventService.class);

	private static CentralizedEventService instance;
	private boolean hasStarted = false;

	HashMap<SmpEvent, SmpEvent> recentEvents;

	public synchronized static CentralizedEventService getInstance() {
		if (instance == null)
			instance = new CentralizedEventService();
		return instance;
	}

	private CentralizedEventService() {
		recentEvents = new FixedLengthLinkedHashMap<SmpEvent, SmpEvent>(1000);
	}

	public List<EventType> listAllEventTypes() {
		List<EventType> types = new ArrayList<EventType>();
		for (LocalSystemEvent.EventSubType subType : LocalSystemEvent.EventSubType
				.values()) {
			types.add(subType.eventType);
		}
		for (RemoteHostEvent.EventSubType subType : RemoteHostEvent.EventSubType
				.values()) {
			types.add(subType.eventType);
		}
		for (RemoteAppEvent.EventSubType subType : RemoteAppEvent.EventSubType
				.values()) {
			types.add(subType.eventType);
		}
		return types;
	}

	// entry point
	public void raiseEvent(SmpEvent event) {
		// TODO add async event handling?
		// just pass it to handleEvent for now

		if (isUnderControl(event)) { // flow control
			try {
				AlarmService.instance.handleEvent(event);
			} catch (Exception e) {
				logger.error(
						"raiseEvent - error passing event to AlarmService: "
								+ event, e);
			}
		}
	}

	/**
	 * prevent same events from happening too often
	 * 
	 * @param event
	 * @return
	 */
	private boolean isUnderControl(final SmpEvent event) {
		boolean isUnderControl = true;

		if (recentEvents.containsKey(event)) {
			SmpEvent lastEvent = recentEvents.get(event);
			if ((event.getEventTime().getTime() - lastEvent.getEventTime()
					.getTime()) > AppConfig
					.getEventFlowControl(DEFAULT_EVENT_FLOW_CONTROL_IN_MIN)) {
				recentEvents.remove(lastEvent);
				recentEvents.put(event, event);
			} else {
				isUnderControl = false; // ignore it, happening too frequent
			}
		} else {
			recentEvents.put(event, event);
		}

		return isUnderControl;
	}

	// /////////////// various raiseEvent wrappers for convenience
	// /////////////////
	public void onSysStartSuccess() {
		raiseEvent(new LocalSystemEvent(
				LocalSystemEvent.EventSubType.SYS_STARTUP_SUCCESS, new Date(),
				WarningLevel.INFO, null));
		hasStarted = true;
	}

	public void onSysStartFailed(String cause) {
		raiseEvent(new LocalSystemEvent(
				LocalSystemEvent.EventSubType.SYS_STARTUP_FAILED, new Date(),
				WarningLevel.FATAL, cause));
	}

	public void onSysShutdown() {
		if (!hasStarted)
			return;

		raiseEvent(new LocalSystemEvent(
				LocalSystemEvent.EventSubType.SYS_SHUTDOWN, new Date(),
				WarningLevel.INFO, null));
	}

	public void onSysConfigReload(String details) {
		raiseEvent(new LocalSystemEvent(
				LocalSystemEvent.EventSubType.CONFIG_RELOAD, new Date(),
				WarningLevel.INFO, details));
	}

	public void onSysConfigReloadError(String cause) {
		raiseEvent(new LocalSystemEvent(
				LocalSystemEvent.EventSubType.CONFIG_RELOAD_ERROR, new Date(),
				WarningLevel.ERROR, cause));
	}

	public void onSysGeneralError(String detail) {
		raiseEvent(new LocalSystemEvent(
				LocalSystemEvent.EventSubType.GENERAL_ERROR, new Date(),
				WarningLevel.WARNING, detail));
	}

	public void onRemoteHostConnected(MonitoredHost host) {
		raiseEvent(new RemoteHostEvent(RemoteHostEvent.EventSubType.HOST_UP,
				host, new Date(), WarningLevel.INFO, null));
	}

	public void onRemoteHostDisconnected(MonitoredHost host, String details) {
		raiseEvent(new RemoteHostEvent(RemoteHostEvent.EventSubType.HOST_DOWN,
				host, new Date(), WarningLevel.MAJOR, details));
	}

	public void onRemoteHostAgentConnected(MonitoredHost host) {
		raiseEvent(new RemoteHostEvent(
				RemoteHostEvent.EventSubType.AGENT_CONNECTED, host, new Date(),
				WarningLevel.INFO, null));
	}

	public void onRemoteHostAgentDisconnected(MonitoredHost host, String details) {
		raiseEvent(new RemoteHostEvent(
				RemoteHostEvent.EventSubType.AGENT_DISCONNECTED, host,
				new Date(), WarningLevel.MAJOR, details));
	}

	public void onRemoteHostCpuOverload(MonitoredHost host, HostCPUUsage usage) {
		RemoteHostEvent event = new RemoteHostEvent(
				RemoteHostEvent.EventSubType.CPU_USAGE, host, new Date(),
				usage.getWarningLevel(), usage.getMessage());
		event.setUsage(usage);
		raiseEvent(event);
	}

	public void onRemoteHostDiskOverload(MonitoredHost host, HostDiskUsage usage) {
		RemoteHostEvent event = new RemoteHostEvent(
				RemoteHostEvent.EventSubType.DISK_USAGE, host, new Date(),
				usage.getWarningLevel(), usage.getMessage());
		event.setUsage(usage);
		raiseEvent(event);
	}

	public void onRemoteHostMemOverload(MonitoredHost host,
			HostMemoryUsage usage) {
		RemoteHostEvent event = new RemoteHostEvent(
				RemoteHostEvent.EventSubType.MEM_USAGE, host, new Date(),
				usage.getWarningLevel(), usage.getMessage());
		event.setUsage(usage);
		raiseEvent(event);
	}

	public void onRemoteHostInetOverload(MonitoredHost host,
			HostInetStatus status) {
		RemoteHostEvent event = new RemoteHostEvent(
				RemoteHostEvent.EventSubType.INET_USAGE, host, new Date(),
				status.getWarningLevel(), status.getMessage());
		event.setUsage(status);
		raiseEvent(event);
	}

	public void onRemoteHostInetDown(MonitoredHost host, WarningLevel level,
			String message, HostInetStatus status) {
		RemoteHostEvent event = new RemoteHostEvent(
				RemoteHostEvent.EventSubType.INET_DOWN, host, new Date(),
				level, message);
		event.setUsage(status);
		raiseEvent(event);
	}

	public void onRemoteHostSnmpError(MonitoredHost host, String details) {
		raiseEvent(new RemoteHostEvent(
				RemoteHostEvent.EventSubType.SNMP_ACCESS_ERROR, host,
				new Date(), WarningLevel.WARNING, details));
	}

	public void onRemoteHostGeneralError(MonitoredHost host, String details) {
		raiseEvent(new RemoteHostEvent(
				RemoteHostEvent.EventSubType.GENERAL_ERROR, host, new Date(),
				WarningLevel.WARNING, details));
	}

	public void onRemoteAppStart(MonitoredHost host, String appName) {
		raiseEvent(new RemoteAppEvent(RemoteAppEvent.EventSubType.APP_START,
				host, appName, new Date(), WarningLevel.INFO, null));
	}

	public void onRemoteAppStartSuccess(MonitoredHost host, String appName,
			String details) {
		raiseEvent(new RemoteAppEvent(
				RemoteAppEvent.EventSubType.APP_START_SUCCESS, host, appName,
				new Date(), WarningLevel.INFO, details));
	}

	public void onRemoteAppStartFailed(MonitoredHost host, String appName,
			String details) {
		raiseEvent(new RemoteAppEvent(
				RemoteAppEvent.EventSubType.APP_START_FAILED, host, appName,
				new Date(), WarningLevel.WARNING, details));
	}

	public void onRemoteAppStop(MonitoredHost host, String appName) {
		raiseEvent(new RemoteAppEvent(RemoteAppEvent.EventSubType.APP_STOP,
				host, appName, new Date(), WarningLevel.INFO, null));
	}

	public void onRemoteAppStopped(MonitoredHost host, String appName,
			String message) {
		raiseEvent(new RemoteAppEvent(
				RemoteAppEvent.EventSubType.APP_STOP_SUCCESS, host, appName,
				new Date(), WarningLevel.INFO, message));
	}

	public void onRemoteAppStopFailed(MonitoredHost host, String appName,
			String details) {
		raiseEvent(new RemoteAppEvent(
				RemoteAppEvent.EventSubType.APP_STOP_FAILED, host, appName,
				new Date(), WarningLevel.WARNING, details));
	}

	public void onRemoteAppDown(MonitoredHost host, String appName,
			String details) {
		raiseEvent(new RemoteAppEvent(RemoteAppEvent.EventSubType.APP_DIED,
				host, appName, new Date(), WarningLevel.WARNING, details));
	}
}
