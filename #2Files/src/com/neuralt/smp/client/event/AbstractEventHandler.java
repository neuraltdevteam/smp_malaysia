package com.neuralt.smp.client.event;

import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.notification.WarningLevel;

public abstract class AbstractEventHandler {

	public abstract boolean accepts(SmpEvent evt);

	public abstract SnmpTrap toTrap(SmpEvent evt, WarningLevel warningLevel);

	public abstract void recordEvent(SmpEvent evt);

	public String getName() {
		return getClass().getSimpleName();
	}

	public int hashCode() {
		return getName().hashCode();
	}

	public boolean equals(Object obj) {
		if (obj instanceof AbstractEventHandler) {
			AbstractEventHandler other = (AbstractEventHandler) obj;
			return this.getName().equals(other.getName());
		}
		return false;
	}
}
