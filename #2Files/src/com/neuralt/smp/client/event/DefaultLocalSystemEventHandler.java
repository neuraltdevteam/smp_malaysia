package com.neuralt.smp.client.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.EventLog;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.SourceType;
import com.neuralt.smp.client.util.EventDelegate;
import com.neuralt.smp.notification.WarningLevel;

public class DefaultLocalSystemEventHandler extends AbstractEventHandler {

	private static final Logger logger = LoggerFactory
			.getLogger(DefaultLocalSystemEventHandler.class);

	@Override
	public boolean accepts(SmpEvent evt) {
		return evt instanceof LocalSystemEvent;
	}

	@Override
	public SnmpTrap toTrap(SmpEvent evt, WarningLevel warningLevel) {
		LocalSystemEvent lsEvt = (LocalSystemEvent) evt;
		logger.debug("received event - " + lsEvt.getEventSubType());

		SnmpTrap trap = null;

		switch (lsEvt.getEventSubType()) {
		case SYS_STARTUP_SUCCESS:
			trap = EventDelegate.instance.onSysStartUpSuccess(warningLevel);
			break;
		case SYS_STARTUP_FAILED:
			trap = EventDelegate.instance.onSysStartUpFail(lsEvt.getDetails(),
					warningLevel);
			break;
		case SYS_SHUTDOWN:
			// nothing to do, no traps for this yet
			break;
		case CONFIG_RELOAD:
			trap = EventDelegate.instance.onSysConfigReload(lsEvt.getDetails(),
					warningLevel);
			break;
		case CONFIG_RELOAD_ERROR:
			trap = EventDelegate.instance.onConfigReloadError(
					lsEvt.getDetails(), warningLevel);
			break;
		case GENERAL_ERROR:
			trap = EventDelegate.instance.onLocalSystemGeneralError(
					lsEvt.getDetails(), warningLevel);
			break;
		default:
			logger.warn("unhandled event type: " + evt.getEventTypeStr() + ", "
					+ evt.getDetails());
		}

		// recordEvent(lsEvt);

		return trap;
	}

	public void recordEvent(SmpEvent evt) {
		LocalSystemEvent lsEvt = (LocalSystemEvent) evt;
		EventLog evtLog = new EventLog();
		evtLog.setEventTime(lsEvt.getEventTime());
		evtLog.setEventName(lsEvt.getEventTypeStr());
		evtLog.setDetails(lsEvt.getDetails());
		evtLog.setLevel(lsEvt.getLevel());
		evtLog.setSourceType(SourceType.SMP);
		DataAccess.eventLogDao.save(evtLog);
	}
}
