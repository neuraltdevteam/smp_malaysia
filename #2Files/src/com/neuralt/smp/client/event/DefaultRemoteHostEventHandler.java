package com.neuralt.smp.client.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.agent.mbean.model.HostCPUUsage;
import com.neuralt.smp.agent.mbean.model.HostDiskUsage;
import com.neuralt.smp.agent.mbean.model.HostInetStatus;
import com.neuralt.smp.agent.mbean.model.HostMemoryUsage;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.EventLog;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.SourceType;
import com.neuralt.smp.client.util.EventDelegate;
import com.neuralt.smp.notification.WarningLevel;

public class DefaultRemoteHostEventHandler extends AbstractEventHandler {

	private static final Logger logger = LoggerFactory
			.getLogger(DefaultRemoteHostEventHandler.class);

	@Override
	public boolean accepts(SmpEvent evt) {
		return evt instanceof RemoteHostEvent;
	}

	@Override
	public SnmpTrap toTrap(SmpEvent evt, WarningLevel warningLevel) {
		RemoteHostEvent rhEvt = (RemoteHostEvent) evt;
		SnmpTrap trap = null;

		switch (rhEvt.getEventSubType()) {
		case AGENT_CONNECTED:
			trap = EventDelegate.instance.onRemAgentConnected(rhEvt.getHost(),
					warningLevel);
			break;
		case AGENT_DISCONNECTED:
			trap = EventDelegate.instance.onRemAgentDisconnected(
					rhEvt.getHost(), warningLevel);
			break;
		case HOST_UP:
			trap = EventDelegate.instance.onRemHostUp(rhEvt.getHost(),
					warningLevel);
			break;
		case HOST_DOWN:
			trap = EventDelegate.instance.onRemHostDown(rhEvt.getHost(),
					warningLevel);
			break;
		case SNMP_ACCESS_ERROR:
			trap = EventDelegate.instance.onRemHostSnmpError(rhEvt.getHost(),
					rhEvt.getDetails(), warningLevel);
			break;
		case CPU_USAGE:
			trap = EventDelegate.instance.onRemHostCpuOverload(rhEvt.getHost(),
					warningLevel != null ? warningLevel : rhEvt.getLevel(),
					(HostCPUUsage) rhEvt.getUsage());
			break;
		case DISK_USAGE:
			trap = EventDelegate.instance.onRemHostDiskOverload(
					rhEvt.getHost(), warningLevel != null ? warningLevel
							: rhEvt.getLevel(), (HostDiskUsage) rhEvt
							.getUsage());
			break;
		case INET_USAGE:
			trap = EventDelegate.instance.onRemHostInetOverload(
					rhEvt.getHost(), warningLevel != null ? warningLevel
							: rhEvt.getLevel(), (HostInetStatus) rhEvt
							.getUsage());
			break;
		case INET_DOWN:
			trap = EventDelegate.instance.onRemHostInetDown(rhEvt.getHost(),
					warningLevel != null ? warningLevel : rhEvt.getLevel(),
					rhEvt.getDetails());
			break;
		case MEM_USAGE:
			trap = EventDelegate.instance.onRemHostMemOverload(rhEvt.getHost(),
					warningLevel != null ? warningLevel : rhEvt.getLevel(),
					(HostMemoryUsage) rhEvt.getUsage());
			break;
		case UPTIME:
			// TODO
			break;
		case GENERAL_ERROR:
			trap = EventDelegate.instance.onRemHostGeneralError(
					rhEvt.getHost(), rhEvt.getDetails(), warningLevel);
			break;
		default:
			logger.warn("unhandled event type: " + evt.getEventTypeStr() + ", "
					+ evt.getDetails());
			break;
		}

		// recordEvent(rhEvt);
		return trap;
	}

	public void recordEvent(SmpEvent evt) {
		RemoteHostEvent rhEvt = (RemoteHostEvent) evt;
		EventLog evtLog = new EventLog();
		evtLog.setEventTime(rhEvt.getEventTime());
		evtLog.setEventName(rhEvt.getEventTypeStr());
		evtLog.setDetails(rhEvt.getDetails());
		evtLog.setLevel(rhEvt.getLevel());
		evtLog.setSourceType(SourceType.HOST);
		evtLog.setSysName(rhEvt.getHost().getSys().getName());
		evtLog.setHostName(rhEvt.getHost().getConfig().getName());
		DataAccess.eventLogDao.save(evtLog);
	}
}
