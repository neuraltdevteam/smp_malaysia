package com.neuralt.smp.client.event;

import java.util.Date;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.notification.WarningLevel;

public class RemoteAppEvent extends SmpEvent {

	private static final long serialVersionUID = 2267629527267395833L;

	public static enum EventSubType {
		APP_START, APP_START_SUCCESS, APP_START_FAILED, APP_STOP, APP_STOP_SUCCESS, APP_STOP_FAILED, APP_DIED;

		public EventType eventType;

		private EventSubType() {
			eventType = new EventType(RemoteAppEvent.class, this.ordinal(),
					this.name());
		}
	}

	private EventSubType eventSubType;
	// private MonitoredProcess app; // in case the event's related to
	// "All apps"
	private MonitoredHost host;
	private String appName;

	public RemoteAppEvent() {
		super();
	}

	public RemoteAppEvent(EventSubType eventType, MonitoredHost host,
			String appName, Date time, WarningLevel level, String details) {
		this();
		this.eventSubType = eventType;
		// this.app = app;
		this.host = host;
		this.appName = appName;
		this.eventTime = time;
		this.level = level;
		this.details = details;
	}

	public EventSubType getEventSubType() {
		return eventSubType;
	}

	public void getEventSubType(EventSubType eventType) {
		this.eventSubType = eventType;
	}

	// public MonitoredProcess getApp() {
	// return app;
	// }
	//
	// public void setApp(MonitoredProcess app) {
	// this.app = app;
	// }

	@Override
	public String getEventTypeStr() {
		return eventSubType != null ? eventSubType.name() : null;
	}

	public MonitoredHost getHost() {
		return host;
	}

	public void setHost(MonitoredHost host) {
		this.host = host;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	@Override
	public String getSource() {
		if (host != null)
			return host.getConfig().getName()
					+ (appName == null ? "" : (" - " + appName));
		else
			return null;
	}

	@Override
	public EventType getEventType() {
		return eventSubType != null ? eventSubType.eventType : null;
	}
}
