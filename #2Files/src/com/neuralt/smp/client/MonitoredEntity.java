package com.neuralt.smp.client;

import java.io.Serializable;

public interface MonitoredEntity extends Serializable {

	public static enum EntityType {
		ALL, SYSTEM, HOST, PROCESS
	}

	// values used by controller for display purpose
	public String getDisplayableName();

	public String getName();

	public String getFullName();

	public String getDescription();

	public String getStatusText();

	public EntityType getEntityType();

	public String getCurrentCpuUsage();

	public String getCurrentMemoryUsage();

	public String getDuration();

	public String getLastChecked();
}
