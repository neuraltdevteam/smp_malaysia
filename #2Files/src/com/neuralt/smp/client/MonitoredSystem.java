package com.neuralt.smp.client;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.SystemConfig;

public class MonitoredSystem implements MonitoredEntity,
		Comparable<MonitoredSystem> {
	private static final long serialVersionUID = 6636572217951101797L;

	private static final Logger logger = LoggerFactory
			.getLogger(MonitoredSystem.class);

	private SystemConfig config;
	private LinkedHashMap<String, MonitoredHost> hostMap;

	public MonitoredSystem(SystemConfig config) {
		this.config = config;
		this.hostMap = new LinkedHashMap<String, MonitoredHost>();

		for (HostConfig hostConfig : config.getHosts()) {
			try {
				MonitoredHost host = new MonitoredHost(hostConfig, this);
				hostMap.put(hostConfig.getName(), host);
			} catch (Exception e) {
				logger.error("error creating host", e);
			}
		}
	}

	public void editHostMap(String oldHostName, MonitoredHost newHost) {
		hostMap.remove(oldHostName);
		hostMap.put(newHost.getConfig().getName(), newHost);
	}

	public void addHost(MonitoredHost host) {
		hostMap.put(host.getConfig().getName(), host);
		config.addHost(host.getConfig());
	}

	public void deleteHost(MonitoredHost host) {
		hostMap.remove(host.getConfig().getName());
		config.deleteHost(host.getConfig());
	}

	public SystemConfig getBackupSystemConfig() {
		SystemConfig backupSystem = new SystemConfig();
		backupSystem.setDescription(this.getConfig().getDescription());
		backupSystem.setName(this.getConfig().getName());
		backupSystem.setEnabled(this.getConfig().isEnabled());
		return backupSystem;
	}

	public void updateConfig(SystemConfig dummy) {
		getConfig().setName(dummy.getName());
		getConfig().setDescription(dummy.getDescription());
		getConfig().setEnabled(dummy.isEnabled());
	}

	public SystemConfig getConfig() {
		return config;
	}

	public MonitoredHost getHost(String hostName) {
		return hostMap.get(hostName);
	}
	
	public MonitoredHost getHostByID(long id){
		for(MonitoredHost host : hostMap.values()){
			if(host.getConfig().getId() == id){
				return host;
			}
		}
		return null;
	}

	public List<MonitoredHost> getHosts() {
		List<MonitoredHost> list = new ArrayList<MonitoredHost>();
		list.addAll(hostMap.values());
		Collections.sort(list);
		return list;
	}

	@Override
	public String getName() {
		return config.getName();
	}

	@Override
	public String getDisplayableName() {
		return getName();
	}

	@Override
	public String getFullName() {
		return getName();
	}

	@Override
	public String getStatusText() {
		// no status for system
		return "";
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.SYSTEM;
	}

	public String getDescription() {
		return config.getDescription();
	}

	public String toString() {
		return String.format("%s,%s", getName(), getDescription());
	}

	@Override
	public String getCurrentCpuUsage() {
		return "-";
	}

	@Override
	public String getCurrentMemoryUsage() {
		return "-";
	}

	@Override
	public String getDuration() {
		return "-";
	}

	@Override
	public String getLastChecked() {
		return "-";
	}

	public Boolean isEnabled() {
		return config.isEnabled();
	}

	@Override
	public int compareTo(MonitoredSystem o) {
		return (config.compareTo(o.getConfig()));
	}
}
