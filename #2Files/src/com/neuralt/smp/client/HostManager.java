package com.neuralt.smp.client;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.management.InstanceNotFoundException;
import javax.management.JMX;
import javax.management.ListenerNotFoundException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectName;
import javax.management.remote.JMXConnectionNotification;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.AgentRuntimeException;
import com.neuralt.smp.agent.mbean.AgentLightMBean;
import com.neuralt.smp.client.MonitoredHost.HostStatus;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.client.listener.HostMonitorNotificationListener;
import com.neuralt.smp.client.snmp.SnmpTaskHolder;
import com.neuralt.smp.client.task.KeepAliveTask;
import com.neuralt.smp.client.task.StatsTask;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.config.ActionCommand;
import com.neuralt.smp.config.EventType;
import com.neuralt.smp.config.HostConfig.MonitorType;
import com.neuralt.smp.config.ProcessStatus;
import com.neuralt.smp.util.ConstantUtil;

/**
 * Oversees a single host and its processes
 */
public class HostManager implements Serializable {
	private static final int DEFAULT_GENERAL_STAT_AVG_STEP_SIZE_IN_MIN = 15;

	private static final long serialVersionUID = -3667598399852245318L;

	private static final Logger logger = LoggerFactory
			.getLogger(HostManager.class);

	private static final String urlFormat = "service:jmx:rmi:///jndi/rmi://%s:%s/jmxrmi";

	private MonitoredHost host;

	private JMXServiceURL url;
	private Map env;
	private JMXConnector connector;
	private MBeanServerConnection mbsc;
	private HostMonitorNotificationListener noteListener;

	// general statistics via SNMP
	private SnmpTaskHolder holder;

	// manage sub tasks, e.g. statsTask, keepAliveTask
	private HostManagerTask baseTask;
	private Thread baseTaskThread;

	// calculate avg periodically
	private StatsTask statsTask;
	private KeepAliveTask keepAliveTask;

	public HostManager(MonitoredHost host) throws MalformedURLException {
		this.host = host;
		// delay the url creation to connectJMXAgent
		// so modified host ip/port will be correctly reflected in connect
		// url = new JMXServiceURL(String.format(urlFormat, host.getIpAddr(),
		// host.getAgentJmxPort()));
		env = new HashMap(1);
		String[] creds = { AppConfig.getJmxUserName(),
				AppConfig.getJmxPassword() };
		env.put(JMXConnector.CREDENTIALS, creds);

		baseTask = new HostManagerTask();
		keepAliveTask = new KeepAliveTask(host);
		statsTask = new StatsTask(
				host,
				AppConfig
						.getGeneralStatsAvgStepSize(DEFAULT_GENERAL_STAT_AVG_STEP_SIZE_IN_MIN),
				false); // hard-coded
	}

	public synchronized boolean connectJMXAgent() {

		if (logger.isDebugEnabled())
			logger.debug(host.getFullName() + " called connect to "
					+ host.getConfig().getIpAddr() + ":"
					+ host.getConfig().getAgentJmxPort());
		if (host.getConfig().isEnabled()
				&& host.getSys().getConfig().isEnabled()) {
			try {
				if (baseTask.addSubTask(keepAliveTask)) {
					logger.debug(host.getName() + " added keepAliveTask");
				}
				url = new JMXServiceURL(String.format(urlFormat,
						host.getIpAddr(), host.getAgentJmxPort()));
				connector = JMXConnectorFactory.connect(url, env);
				connector.addConnectionNotificationListener(
						new ConnectionNotificationListener(), null, null);

				// the following line throws exception if connection fails
				mbsc = connector.getMBeanServerConnection();

				ObjectName mbeanName = new ObjectName(
						AppConfig.getAgentMBeanName());
				noteListener = new HostMonitorNotificationListener(this);
				mbsc.addNotificationListener(mbeanName, noteListener, null,
						null);
				host.setLastConnected(System.currentTimeMillis());
				host.setStatus(HostStatus.CONNECTED);

				if (logger.isDebugEnabled())
					logger.debug("JMX connection with host {} established",
							host.getIpAddr());

				CentralizedEventService.getInstance()
						.onRemoteHostAgentConnected(host);

				if (logger.isDebugEnabled())
					logger.debug("Pushing config...");
				pushHostConfig();

				// only call startMonHostUsage() when the MonitorType is script
				if (host.getConfig().getType().equals(MonitorType.SCRIPT)) {
					logger.debug("try startMonHostUsage host=" + host.getName());
					startAgentMonHostUsage();
				}
				startMonProcess();
				return true;
			} catch (Exception e) {
				// SnmpNotificationDelegate.instance.onRemHostDown(host);
				// connect failed.
				logger.debug(host.getName() + " - connect failed");
				return false;
			}
		}
		return false;
	}

	public synchronized void startMonProcess() {
//		if (host.isEnabled() && host.getConfig().getPsMonitor().isEnabled()
//				&& host.getConfig().getType().equals(MonitorType.SCRIPT))
//			queryProcessStatus();
	}

	public synchronized void stopMonProcess() {
//		if (host.getConfig().getType().equals(MonitorType.SCRIPT)) {
//			for (MonitoredProcess process : host.getProcesses()) {
//				stopMonProcess(process);
//			}
//		}
	}

	private void queryProcessStatus() {
		AgentLightMBean agent = getAgentProxy();
		Map<String, Boolean> statusMap = agent.queryProcessStatus();
		for (MonitoredProcess proc : host.getProcesses()) {
			Boolean status = statusMap.get(proc.getName());
			if (status != null) {
				if (status) {
					// try not set status as status should be set by process
					// notifications
					// proc.setStatus(ProcessStatus.STARTED);
					startMonProcess(proc);
				} else {
					proc.setStatus(ProcessStatus.STOPPED);
				}

				// startMonProcess(proc);
			}
		}
	}

	public synchronized void disconnectJMXAgent() throws IOException,
			MalformedObjectNameException, InstanceNotFoundException,
			ListenerNotFoundException {
		if (logger.isDebugEnabled())
			logger.debug(host.getFullName() + " called disconnect");
		if (isAgentConnected()) {
			if (noteListener != null && mbsc != null) {
				ObjectName mbeanName = new ObjectName(
						AppConfig.getAgentMBeanName());
				mbsc.removeNotificationListener(mbeanName, noteListener);
			}
			connector.close();
			baseTask.removeSubTask(keepAliveTask);
			host.setStatus(HostStatus.DISCONNECTED);
		}
	}

	public synchronized boolean isAgentConnected() {
		return host.isAgentConnected();
	}

	public boolean isEnabled() {
		return host.isEnabled() && host.getSys().isEnabled();
	}

	public void pushHostConfig() {
		AgentLightMBean agent = getAgentProxy();
		agent.setHostConfig(host.getConfig());
		logger.debug("Pushed config to {}, detail={}", host.getConfig()
				.getName(), host.getConfig());
	}

	public void startSnmpGetUsage() {
		logger.info(host.getName() + " start snmp get usage");
		MonitorType monType = host.getConfig().getType();
		if (host.isEnabled()
				&& host.getConfig().getSnmpConfig() != null
				&& holder == null
				&& (monType.equals(MonitorType.SNMP) || monType
						.equals(MonitorType.BOTH))) {
			logger.debug("{} starting monitor with SNMP GET", host.getName());
			holder = new SnmpTaskHolder(host);
			holder.startTask();

			baseTask.addSubTask(statsTask);
		}
	}

	public void stopSnmpGetUsage() {
		logger.info(host.getName() + " stop snmp get usage");

		if (holder != null)
			holder.stopTask();
		logger.debug("{} Snmp Task Holder stop called.", host.getName());
		baseTask.removeSubTask(statsTask);
		holder = null;
	}

	public void startAgentMonHostUsage() {
		logger.info(host.getName() + " start Mon Host Usage");

		MonitorType monType = host.getConfig().getType();
		if (monType.equals(MonitorType.SCRIPT) && host.isEnabled()
				&& host.isAgentConnected()) {
			AgentLightMBean agent = getAgentProxy();
			if (host.getConfig().getCPUMonitor().isEnabled())
				agent.startMonHostCPUUsage();
			if (host.getConfig().getDiskMonitor().isEnabled())
				agent.startMonHostDiskUsage();
			if (host.getConfig().getMemoryMonitor().isEnabled())
				agent.startMonHostMemoryUsage();
//			if (host.getConfig().getInetMonitor().isEnabled())
//				agent.startMonHostNetworkInterface();
			if (host.getConfig().getUptimeMonitor().isEnabled())
				agent.startMonHostUptime();
			baseTask.addSubTask(statsTask);
		}
	}

	public void stopAgentMonHostUsage() {
		logger.info(host.getName() + " stop Mon Host Usage");
		if (host.isAgentConnected()
				&& host.getConfig().getType() == MonitorType.SCRIPT) {
			try {
				AgentLightMBean agent = getAgentProxy();
				agent.stopMonHostCPUUsage();
				agent.stopMonHostDiskUsage();
				agent.stopMonHostMemoryUsage();
				agent.stopMonHostNetworkInterface();
				agent.stopMonHostUptime();
				baseTask.removeSubTask(statsTask);
			} catch (Exception e) {
				logger.error("failed to stopAgentMonHostUsage", e);
			}
		}
	}

	public synchronized void start() {
		if (baseTaskThread == null || !baseTaskThread.isAlive()) {
			baseTaskThread = new Thread(baseTask);
			baseTaskThread.start();
			logger.debug(host.getName() + " monHost task started");
		}
		try {
			startAgentMonHostUsage();
			startMonProcess();
		} catch (AgentRuntimeException ignore) {
			// should ignore this type of exception if there is no need for
			// return
		}
		startSnmpGetUsage();
	}

	public synchronized void stop() {
		if (baseTask != null) {
			baseTask.stop();
			if (baseTaskThread != null)
				baseTaskThread.interrupt();
		}
		stopAgentMonHostUsage();
		stopMonProcess();
		stopSnmpGetUsage();
	}

	public MonitoredHost getHost() {
		return host;
	}

	public void startProcess(String procName) {
		MonitoredProcess proc = host.getProcess(procName);
		if (proc == null)
			throw new IllegalArgumentException("Process [" + procName
					+ "] not found");
		startProcess(proc);
	}

	private void startProcess(final MonitoredProcess proc) {
		if (!proc.isRunning()) {
			try {
				AgentLightMBean agent = getAgentProxy();
				agent.startProcess(proc.getName());
				proc.setStatus(ProcessStatus.STARTING);
				CentralizedEventService.getInstance().onRemoteAppStart(host,
						proc.getName());
				// don't start monitoring until the process has been fully
				// started
				Thread waitJob = new Thread() {
					public void run() {
						for (ActionCommand action : proc.getConfig()
								.getEventHandler(EventType.STARTUP)
								.getListActionCommand()) {
							if (action.isEnabled())
								try {
									Thread.sleep(action.getTimeout()
											* ConstantUtil.SEC_MS);
								} catch (InterruptedException ignore) {
								}
						}
						// if the process is started, it will be monitored
						queryProcessStatus();
					}
				};
				waitJob.start();
			} catch (Exception e) {
				CentralizedEventService.getInstance().onRemoteAppStartFailed(
						host, proc.getName(), e.getMessage());
			}
		}
	}

	public void startMonProcess(String procName) {
		MonitoredProcess proc = host.getProcess(procName);
		if (proc == null)
			throw new IllegalArgumentException("Process [" + procName
					+ "] not found");
		startMonProcess(proc);
	}

	private void startMonProcess(MonitoredProcess proc) {
		AgentLightMBean agent = getAgentProxy();
		agent.startMonProcess(proc.getConfig().getName());
	}

	public void stopMonProcess(String procName) {
		MonitoredProcess proc = host.getProcess(procName);
		if (proc == null)
			throw new IllegalArgumentException("Process [" + procName
					+ "] not found");
		stopMonProcess(proc);
	}

	private void stopMonProcess(MonitoredProcess proc) {
		AgentLightMBean agent = getAgentProxy();
		agent.stopMonProcess(proc.getConfig().getName());
	}

	public void stopProcess(String procName) {
		MonitoredProcess proc = host.getProcess(procName);
		if (proc == null)
			throw new IllegalArgumentException("Process [" + procName
					+ "] not found");
		stopProcess(proc);
	}

	private void stopProcess(final MonitoredProcess proc) {
		if (proc.isRunning()) {
			final AgentLightMBean agent = getAgentProxy();
			proc.setStatus(ProcessStatus.STOPPING);
			final String procName = proc.getConfig().getName();
			CentralizedEventService.getInstance().onRemoteAppStop(host,
					proc.getName());
			agent.stopMonProcess(procName);
			agent.stopProcess(procName);

			// TODO
			// now make a temporary thread for wait enough time for process to
			// shutdown, should be improved
			Thread waitJob = new Thread() {
				public void run() {
					for (ActionCommand action : proc.getConfig()
							.getEventHandler(EventType.SHUTDOWN)
							.getListActionCommand()) {
						if (action.isEnabled())
							try {
								Thread.sleep(action.getTimeout()
										* ConstantUtil.SEC_MS);
							} catch (InterruptedException ignore) {
							}
					}
					// if the process is still running(as it should not), it
					// will be monitored
					queryProcessStatus();
					// agent.startMonProcess(procName);
				}
			};
			waitJob.start();
			// for (ActionCommand action :
			// proc.getConfig().getEventHandler(EventType.SHUTDOWN).getListActionCommand())
			// {
			// if (action.isAuto())
			// try {
			// Thread.sleep(action.getTimeout()*ConstantUtil.SEC_MS);
			// } catch (InterruptedException ignore) {
			// }
			// }
			// agent.startMonProcess(procName);
			// if (agent.queryProcessStatus().get(proc.getConfig().getName())) {
			// // process still alive
			// SmpEventService.getInstance().onRemoteAppStopFailed(host,
			// proc.getName(),
			// "Process "+proc.getConfig().getName()+" is failed to shutdown");
			// }

			// SnmpNotificationDelegate.instance.onRemAppStop(host,
			// proc.getName());
		}
	}

	public String setClock(String time) {
		AgentLightMBean agent = getAgentProxy();
		return agent.setClock(time);
	}

	public void startProcesses() {
		try {
			AgentLightMBean agent = getAgentProxy();
			agent.startAllProcesses();
			CentralizedEventService.getInstance().onRemoteAppStart(host,
					MonitoredProcess.ALL_PROCESSES);
		} catch (Exception e) {
			CentralizedEventService.getInstance().onRemoteAppStartFailed(host,
					MonitoredProcess.ALL_PROCESSES, e.getMessage());
		}
	}

	public void stopProcesses() {

		for (MonitoredProcess proc : host.getProcesses()) {
			stopProcess(proc);
		}

		// AgentLightMBean agent = getAgentProxy();
		// agent.stopMonAllProcesses();
		// agent.stopAllProcesses();
		// for (ProcessConfig proc : host.getConfig().getProcessConfigs()) {
		// for (ActionCommand action :
		// proc.getEventHandler(EventType.SHUTDOWN).getListActionCommand()) {
		// if (action.isAuto())
		// try {
		// Thread.sleep(action.getTimeout()*ConstantUtil.SEC_MS);
		// } catch (InterruptedException ignore) {
		// }
		// }
		// }
		// agent.startMonAllProcesses();
		CentralizedEventService.getInstance().onRemoteAppStop(host,
				MonitoredProcess.ALL_PROCESSES);
	}

	private AgentLightMBean getAgentProxy() {
		try {
			ObjectName mbeanName = new ObjectName(AppConfig.getAgentMBeanName());
			return JMX.newMBeanProxy(mbsc, mbeanName, AgentLightMBean.class,
					true);
		} catch (Exception e) {
			throw new AgentRuntimeException("Cannot create mbean proxy", e);
		}
	}

	private class ConnectionNotificationListener implements
			NotificationListener {

		@Override
		public void handleNotification(Notification notification, Object source) {
			if (notification instanceof JMXConnectionNotification) {
				JMXConnectionNotification n = (JMXConnectionNotification) notification;
				logger.debug("notification type: {}", n.getType());

				if (JMXConnectionNotification.FAILED.equals(n.getType())) {
					logger.debug("JMX connection with host {} is lost",
							host.getIpAddr());
					host.setStatus(HostStatus.DISCONNECTED);

					CentralizedEventService
							.getInstance()
							.onRemoteHostAgentDisconnected(host, n.getMessage());
				}
			}
		}
	}

	private class HostManagerTask implements Runnable, Serializable {
		private static final long serialVersionUID = -8139944245944212074L;
		private static final long intervalMs = 2000;
		private List<Runnable> subTasks;
		private boolean running = false;

		public HostManagerTask() {
			subTasks = new CopyOnWriteArrayList<Runnable>();
		}

		public boolean addSubTask(Runnable task) {
			boolean added = false;

			if (!subTasks.contains(task))
				added = subTasks.add(task);

			return added;
		}

		public void removeSubTask(Runnable task) {
			if (subTasks.contains(task))
				subTasks.remove(task);
		}

		public void stop() {
			running = false;
		}

		@Override
		public void run() {
			running = true;
			while (running && !Thread.interrupted()) {

				for (Runnable subTask : subTasks) {
					subTask.run();
				}

				try {
					Thread.sleep(intervalMs);
				} catch (InterruptedException e) {
					Thread.currentThread().interrupt();
					running = false;
					logger.debug("HostManagerTask Thread[{}] interrupted",
							Thread.currentThread().getId());
				}
			}
		}
	}

}
