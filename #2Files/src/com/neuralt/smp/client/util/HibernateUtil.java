package com.neuralt.smp.client.util;

import java.io.File;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import com.neuralt.smp.client.web.AppConfig;

public class HibernateUtil {

	private static final SessionFactory sessionFactory = initSessionFactory();
	private static final SessionFactory BLCSessionFactory = initBLCSessionFactory();
	
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public static Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	private static SessionFactory initSessionFactory() {
		try {
			String hibernateConfig = AppConfig.getHibernateConfig();
			Configuration hbConfig = null;
			if (hibernateConfig != null) {
				hbConfig = new Configuration().configure(new File(
						hibernateConfig));
			} else {
				hbConfig = new Configuration().configure();
			}

			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
					.applySettings(hbConfig.getProperties())
					.buildServiceRegistry();

			return hbConfig.buildSessionFactory(serviceRegistry);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new ExceptionInInitializerError(e);
		}
	}
	
	/* BLC session factory */
	
	public static SessionFactory getBLCSessionFactory() {
		return BLCSessionFactory;
	}

	public static Session getCurrentBLCSession() {
		return BLCSessionFactory.getCurrentSession();
	}

	private static SessionFactory initBLCSessionFactory() {
		try {
			String hibernateConfig = AppConfig.getBLCHibernateConfig();
			Configuration hbConfig = null;
			if (hibernateConfig != null) {
				hbConfig = new Configuration().configure(new File(
						hibernateConfig));
			} else {
				hbConfig = new Configuration().configure();
			}

			ServiceRegistry serviceRegistry = new ServiceRegistryBuilder()
					.applySettings(hbConfig.getProperties())
					.buildServiceRegistry();

			return hbConfig.buildSessionFactory(serviceRegistry);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new ExceptionInInitializerError(e);
		}
	}
	
}
