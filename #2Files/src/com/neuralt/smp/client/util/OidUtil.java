package com.neuralt.smp.client.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import net.percederberg.mibble.MibLoaderException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.MibFlag;
import com.neuralt.smp.client.data.OidMap;
import com.neuralt.smp.client.data.OidMap.ObjectName;
import com.neuralt.smp.client.data.ProductGroup;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.snmp.util.MibFileLoader;
import com.neuralt.smp.util.StringUtil;

public class OidUtil {

	private static final Logger logger = LoggerFactory.getLogger(OidUtil.class);

	private OidUtil() {
	}

	private static OidUtil instance;

	public static OidUtil getInstance() {
		if (instance == null) {
			synchronized (OidUtil.class) {
				if (instance == null) {
					instance = new OidUtil();
					instance.init();
				}
			}
		}
		return instance;
	}

	private Map<OID, OidMap> fullOidMap = new HashMap<OID, OidMap>();

	public List<OID> svrOidList;

	public synchronized void reload() {
		try {
			logger.debug("reload called");
			fullOidMap.clear();
			List<OidMap> list = DataAccess.oidMapDao.getAllOidMaps();
			for (OidMap oidMap : list) {
				fullOidMap.put(oidMap.getOid(), oidMap);
				// if (logger.isDebugEnabled()) {
				// logger.debug(oidMap.getOid().toString()+" is put in the map");
				// logger.debug("size of default obj name:"+oidMap.getObjectNames().size());
				// logger.debug("size of obj name map:"+oidMap.getObjectNameMaps().size());
				// }
			}
		} catch (Exception e) {
		}
	}

	public Map<OID, OidMap> getFullOidMap() {
		// logger.debug("getting full oid map");
		if (fullOidMap.isEmpty()) {
			synchronized (fullOidMap) {
				if (fullOidMap.isEmpty()) {
					reload();
				}
			}
		}
		return fullOidMap;
	}

	public String mapOidToTrapName(String oid) {
		if (StringUtil.isNullOrEmpty(oid)) {
			return "";
		}
		String dummy = oid;
		OID dummyOid = new OID(dummy);
		String mapped = null;
		if (fullOidMap.containsKey(dummyOid)) {
			return fullOidMap.get(dummyOid).getOidAsTrapName();
		}

		mapped = MibFileLoader.getInstance().getAllTrapNameMap().get(dummyOid);
		if (!StringUtil.isNullOrEmpty(mapped)) {
			return mapped;
		}
		return oid;
	}

	public String mapOidToTrapName(OID oid) {
		if (StringUtil.isNullOrEmpty(oid)) {
			return "";
		}

		// if (fullOidMap.containsKey(oid)) {
		// return fullOidMap.get(oid).getOidAsTrapName();
		// }

		String mapped = MibFileLoader.getInstance().getAllTrapNameMap()
				.get(oid);
		if (!StringUtil.isNullOrEmpty(mapped)) {
			return mapped;
		}
		return oid.toString();
	}

	public String mapOidToOriginalTrapName(String oid) {
		logger.debug("finding original trap name for:" + oid);
		if (StringUtil.isNullOrEmpty(oid)) {
			return null;
		}
		String mapped = null;
		mapped = MibFileLoader.getInstance().getAllTrapNameMap()
				.get(getOidFromString(oid));
		if (!StringUtil.isNullOrEmpty(mapped)) {
			return mapped;
		}
		return oid;
	}

	public OID getOidFromString(String str) {
		OID oid = null;
		try {
			oid = new OID(str);
		} catch (Exception ignore) {

		}
		return oid;
	}

	public List<OID> getOidListStartWith(OID oid) {
		if (!StringUtil.isNullOrEmpty(oid)) {
			List<OID> list = new ArrayList<OID>();
			for (OID key : MibFileLoader.getInstance().getAllTrapNameMap()
					.keySet()) {
				if ((key.startsWith(oid)) && (key.size() > oid.size())) {
					list.add(key);
				}
			}
			return list;
		}
		return null;
	}

	/**
	 * This is for the upload of new MIB file. It is a temporary solution and
	 * may be refactored later.
	 * 
	 */
	public synchronized void conditionalInit() {
		init();
		reload();
	}

	private void init() {
		List<ProductGroup> currentGroupList = DataAccess.productGroupDao
				.getAllProductGroups();
		List<ProductGroup> tempGroupList = new ArrayList<ProductGroup>();
		for (OID oid : MibFileLoader.getInstance().getAllProductGroup()) {
			logger.debug("checking group oid:" + oid);
			boolean found = false;
			for (ProductGroup group : currentGroupList) {
				if (oid.equals(group.getOid())) {
					found = true;
					break;
				}
			}
			if (!found) {
				ProductGroup newGroup = new ProductGroup();
				newGroup.setOid(oid);
				newGroup.setName(MibFileLoader.getInstance()
						.getAllTrapNameMap().get(oid));
				currentGroupList.add(newGroup);
				tempGroupList.add(newGroup);
				logger.debug("oid=" + oid.toString() + ", group="
						+ newGroup.getName());
			}
		}

		DataAccess.productGroupDao.batchSave(tempGroupList);

		tempGroupList.clear();

		List<OidMap> tempOidMapList = new ArrayList<OidMap>();
		try {
			tempOidMapList = DataAccess.oidMapDao.getAllOidMaps();
		} catch (Exception e) {
			logger.error("failed to retrieve OidMap from DB", e);
		}
		for (OID oid : MibFileLoader.getInstance().getAllTrapObjectMap()
				.keySet()) {
			try {
				boolean found = false;
				for (OidMap map : tempOidMapList) {
					if (oid.equals(map.getOid())) {
						found = true;
						break;
					}
				}
				if (!found) {
					logger.debug("oid {} {} is not found", MibFileLoader
							.getInstance().getAllTrapNameMap().get(oid), oid);
					OidMap oidMap = new OidMap();
					oidMap.setOid(oid);
					oidMap.setObjectNames(new HashSet<ObjectName>());
					for (OID object : MibFileLoader.getInstance()
							.getAllTrapObjectMap().get(oid)) {
						ObjectName name = new ObjectName();
						name.setParent(oidMap);
						name.setOid(object);
						name.setMappedOid(object);
						oidMap.getObjectNames().add(name);
					}
					OID copy = oid.trim();
					boolean groupFound = false;
					ProductGroup parent = null;
					for (ProductGroup dummyGroup : currentGroupList) {
						if (copy.equals(dummyGroup.getOid())) {
							groupFound = true;
							parent = dummyGroup;
							break;
						}
					}
					if (!groupFound) {
						parent = new ProductGroup();
						parent.setOid(copy);
						parent.setName(MibFileLoader.getInstance()
								.getAllTrapNameMap().get(copy));
						// DataAccess.productGroupDao.save(parent);
						currentGroupList.add(parent);
					}
					// ProductGroup group =
					// DataAccess.productGroupDao.getProductGroupByOid(copy);
					oidMap.setParent(parent);
					parent.getOidMaps().add(oidMap);
					// DataAccess.productGroupDao.save(parent);
					boolean parentInList = false;
					for (ProductGroup group : tempGroupList) {
						if (group.getOid().equals(parent.getOid())) {
							parentInList = true;
							break;
						}
					}
					if (!parentInList) {
						tempGroupList.add(parent);
					}
					tempOidMapList.add(oidMap);
				}
			} catch (Exception e) {
				logger.error("failed to generate Object Name for OID {}, [}",
						oid, e);
			}
		}

		DataAccess.productGroupDao.batchSave(tempGroupList);

		List<MibFlag> currentMibFlagList = DataAccess.mibFlagDao
				.getAllMibFlags();
		List<MibFlag> tempMibFlagList = new ArrayList<MibFlag>();

		for (String name : MibFileLoader.getInstance().getAllMibNameList()) {
			boolean found = false;
			for (MibFlag flag : currentMibFlagList) {
				if (name.equals(flag.getName())) {
					found = true;
					break;
				}
			}
			if (!found) {
				MibFlag newFlag = new MibFlag(name, true, MibFileLoader
						.getInstance().getMibToPathMap().get(name));
				currentMibFlagList.add(newFlag);
				tempMibFlagList.add(newFlag);
			}
		}

		// for (Entry<OID, List<String>> entry :
		// MibFileLoader.getInstance().getAllTrapToMibNameMap().entrySet()) {
		// for (String name : entry.getValue()) {
		// boolean found = false;
		// for (MibFlag flag : currentMibFlagList) {
		// if (name.equals(flag.getName())) {
		// found = true;
		// break;
		// }
		// }
		// if (!found) {
		// MibFlag newFlag = new MibFlag(name, true,
		// MibFileLoader.getInstance().getMibToPathMap().get(name));
		// currentMibFlagList.add(newFlag);
		// tempMibFlagList.add(newFlag);
		// }
		// }
		// }
		DataAccess.mibFlagDao.batchSave(tempMibFlagList);
		reload();
		// checkInMemoryOid();
	}

	private void checkInMemoryOid() {
		for (Entry<OID, List<String>> entry : MibFileLoader.getInstance()
				.getAllTrapToMibNameMap().entrySet()) {
			logger.debug(MibFileLoader.getInstance().getAllTrapNameMap()
					.get(entry.getKey())
					+ " belongs to " + entry.getValue());
		}
	}

	public void deleteMib(MibFlag mib) throws MibLoaderException, IOException {
		// move file to temp dir
		boolean ok = false;
		String mibName = mib.getName();
		File file = new File(mib.getFilepath());
		String filename = file.getName();
		logger.debug("deleting {}", filename);
		Map<OID, List<String>> tempMap = new HashMap<OID, List<String>>(
				MibFileLoader.getInstance().getAllTrapToMibNameMap());
		try {
			ok = FileUtil.moveFile(filename, AppConfig.getAllMIBFileDir(),
					AppConfig.getTempDir());
		} catch (IOException e) {
			throw e;
		}
		logger.debug("file moved = {}", ok);

		// reload mibs
		try {
			MibFileLoader.getInstance()
					.loadAllMib(AppConfig.getAllMIBFileDir());
		} catch (MibLoaderException e) {
			// probably deleted a mib which others depends on it
			logger.debug("failed - {} should be a parent of other MIB", mibName);
			// rollback
			// XXX note that user cannot directly replace the parent mibs
			// eg. they will have to delete children > delete old parent >
			// insert new parent > insert children
			try {
				FileUtil.moveFile(filename, AppConfig.getTempDir(),
						AppConfig.getAllMIBFileDir());
			} catch (IOException e1) {
				throw e1;
			}
			throw e;
		} catch (IOException e) {
			throw e;
		}
		if (ok) {
			// delete related DB record
			logger.debug("deleting DB record for {}...", mibName);
			List<OidMap> oidMapList = DataAccess.oidMapDao.getAllOidMaps();
			List<ProductGroup> dummyGroupList = new ArrayList<ProductGroup>();

			for (Entry<OID, List<String>> entry : tempMap.entrySet()) {
				// logger.debug("{} belongs to {}",entry.getKey(),entry.getValue());
				if (entry.getValue().contains(mibName)
						&& entry.getValue().size() == 1) {
					logger.debug("{} belong to this MIB", entry.getKey());
					for (OidMap oidMap : oidMapList) {
						// logger.debug("checking on {}",oidMap.getOid());
						if (oidMap.getOid().equals(entry.getKey())) {
							logger.debug("found associated OID map : {}",
									oidMap.getOid());
							ProductGroup parent = oidMap.getParent();
							if (!dummyGroupList.contains(parent)) {
								logger.debug("parent {} added to batch list",
										parent.getName());
								dummyGroupList.add(parent);
							}
							break;
						}
					}
				}
			}

			DataAccess.mibFlagDao.delete(mib);
			DataAccess.productGroupDao.batchDelete(dummyGroupList);
			reload();

			// delete the oldFile in temp dir
			File oldFile = new File(AppConfig.getTempDir(), filename);
			oldFile.delete();
		}
	}
}
