package com.neuralt.smp.client.util;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.ChartTemplate;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataPoint;
import com.neuralt.smp.client.data.DataWrapper;
import com.neuralt.smp.client.data.RrdArchiveConfig;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.rrd.RrdService;
import com.neuralt.smp.rrd.RrdUtil;
import com.neuralt.smp.util.ConstantUtil;

public class ChartTemplateUtil {

	private static final Logger logger = LoggerFactory
			.getLogger(ChartTemplateUtil.class);

	public static CartesianChartModel generateChart(ChartTemplate template)
			throws IOException {
		return generateChart(template, null, null);
	}

	public static CartesianChartModel generateChart(ChartTemplate template,
			Date from, Date to) throws IOException {
		// logger.debug("generateChart is called for:"+template);
		CartesianChartModel chart = new CartesianChartModel();
		for (RrdArchiveConfig archive : template.getArchiveConfigsAsList()) {
			if (archive.isLocked()) {
				RrdDefConfig def = archive.getRrdDef();
				int interval = archive.getStep() * def.getStep();
				long now = System.currentTimeMillis();
				if (to != null) {
					now = to.getTime();
				}
				now = now / ConstantUtil.SEC_MS / interval;
				now = now * interval * ConstantUtil.SEC_MS;
				long difference = RrdArchiveConfig.translateTime(
						template.getTimeRange(), template.getTimeRangeType())
						* ConstantUtil.SEC_MS;
				if (from != null) {
					difference = to.getTime() - from.getTime();
				}
				difference = difference / ConstantUtil.SEC_MS / interval;
				difference = difference * interval * ConstantUtil.SEC_MS;

				long past = now - difference;
				from = new Date(past);
				to = new Date(now);
				from = RrdUtil.normalizeBoundary(from, interval);
				to = RrdUtil.normalizeBoundary(to, interval);
				try {
					// if (interval==80) {
					// //
					// logger.debug("ready to fetch data for template="+template.getName()+", archive="+archive.getName()+", interval="+interval+", now="+now+", past="+past);
					// if (now%ConstantUtil.SEC_MS==0) {
					// long dummy = now/ConstantUtil.SEC_MS;
					// if (dummy%interval==0) {
					// //
					// logger.debug(archive.getName()+": now is a multiple of "+interval);
					// } else {
					// if (logger.isDebugEnabled())
					// logger.debug(template.getName()+"-"+archive.getName()+" something wrong in now : dummy%interval="+(dummy%interval));
					// }
					// }
					// if (past%ConstantUtil.SEC_MS==0) {
					// long dummy = past/ConstantUtil.SEC_MS;
					// if (dummy%interval==0) {
					// //
					// logger.debug(archive.getName()+": past is a multiple of "+interval);
					// } else {
					// if (logger.isDebugEnabled())
					// logger.debug(template.getName()+"-"+archive.getName()+" something wrong in past : dummy%interval="+(dummy%interval));
					// }
					// }
					// }
					List<DataWrapper> wrappers = RrdService
							.getDefaultRrdAccess().fetchData(from, to,
									interval, archive.getType(), def);
					for (DataWrapper dw : wrappers) {

						if (dw.getPoints().size() != 0) {
							String dsName = dw.getPoints().get(0).getName();
							boolean ok = false;
							for (RrdDsConfig ds : template.getDsConfigsAsList()) {
								if (ds.getName().equals(dsName)
										&& ds.getRrdDef().equals(
												archive.getRrdDef())) {
									ok = true;
									break;
								}
							}
							if (ok) {
								LineChartSeries series = new LineChartSeries();
								series.setLabel(dsName + " - "
										+ archive.getName());
								// logger.debug("adding series for "+series.getLabel());
								Map<Object, Number> map = new HashMap<Object, Number>();
								for (DataPoint dp : dw.getPoints()) {
									if (!dp.getValue().equals(Double.NaN))
										map.put(dp.getTime(), dp.getValue());
									// logger.debug("data point for "+series.getLabel()+" time="+new
									// Date(dp.getTime())+", value="+dp.getValue());
								}
								if (map.size() > 0) {
									series.setData(map);
									chart.addSeries(series);
								} else {
									// logger.debug("data map size="+map.size()+" for ds="+dsName+" in template="+template.getName());
								}
							}
						}
					}
				} catch (IOException e) {
					logger.error("failed in generating chart {},{}", template,
							e);
					throw e;
				}
			}
		}
		// logger.debug("chart "+template.getName()+" series size:"+chart.getSeries().size());
		// if (chart.getSeries().size()==0) {
		// LineChartSeries series = new LineChartSeries();
		// series.setLabel("NO_DATA_AVAILABLE");
		// Map<Object, Number> map = new HashMap<Object, Number>();
		// map.put(System.currentTimeMillis(), 0);
		// series.setData(map);
		// chart.addSeries(series);
		// }
		return chart;
	}

	public static void deleteDs(RrdDsConfig ds) {
		List<ChartTemplate> templates = DataAccess.chartTemplateDao
				.getAllChartTemplates();
		for (ChartTemplate template : templates) {
			RrdDsConfig temp = null;
			for (RrdDsConfig dummy : template.getDsConfigs()) {
				if (dummy.equals(ds)) {
					temp = dummy;
				}
			}
			if (temp != null) {
				if (logger.isDebugEnabled())
					logger.debug("deleting ds:" + temp + " from template:"
							+ template);
				template.getDsConfigs().remove(temp);
				DataAccess.chartTemplateDao.save(template);
			}
		}
	}

	public static void deleteArchive(RrdArchiveConfig archive) {
		List<ChartTemplate> templates = DataAccess.chartTemplateDao
				.getAllChartTemplates();
		for (ChartTemplate template : templates) {
			RrdArchiveConfig tempArchive = null;
			for (RrdArchiveConfig dummy : template.getArchiveConfigs()) {
				if (dummy.equals(archive)) {
					tempArchive = dummy;
				}
			}
			if (tempArchive != null) {
				if (logger.isDebugEnabled())
					logger.debug("deleting archive:" + tempArchive
							+ " from template:" + template);
				template.getArchiveConfigs().remove(tempArchive);
				DataAccess.chartTemplateDao.save(template);
			}
		}
	}
}
