package com.neuralt.smp.client.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This is the class acts as the entrance of handling the mapping of object name
 * values. Currently there is only a temporary method for handling some easy
 * cases.
 *
 */
public class ValueMappingUtil {
	private static final Logger logger = LoggerFactory
			.getLogger(ValueMappingUtil.class);

	private static ValueMappingUtil instance;

	private ValueMappingUtil() {

	}

	public static ValueMappingUtil getInstance() {
		if (instance == null) {
			synchronized (ValueMappingUtil.class) {
				if (instance == null) {
					instance = new ValueMappingUtil();
				}
			}
		}
		return instance;
	}

	/**
	 * This is a temporary method for handling some easy cases in value mapping.
	 * The expression is restricted in format
	 * "(prefix1)(value1),(prefix2)(value2)...". The default concatenation is
	 * AND. eg. ">5,<=16,!=13" should be viewed as >5 AND <=16 AND !=13
	 * 
	 * @param value
	 *            The incoming value
	 * @param expression
	 *            The expression set in Oid Mapping page
	 * @return true when the value matches the expression, false otherwise
	 */
	public boolean isValueMatch(String value, String expression) {
		if (value != null && expression != null) {
			if (value.equals(expression)) {
				return true;
			} else {
				boolean ok = true;
				String[] parts = expression.split(",");
				for (int i = 0; i < parts.length; i++) {
					String part = parts[i].trim();
					String temp = null;
					String prefix = null;
					if (part.startsWith("!=") || part.startsWith(">=")
							|| part.startsWith("<=")) {
						temp = part.substring(2).trim();
						prefix = part.substring(0, 2);
					} else if (part.startsWith(">") || part.startsWith("<")) {
						temp = part.substring(1).trim();
						prefix = part.substring(0, 1);
					}
					if (temp == null) {
						return false;
					}

					if (value.matches("-?\\d+(\\.\\d+)?")
							&& temp.matches("-?\\d+(\\.\\d+)?")) {
						// treat them as numerical
						Float valueFloat = Float.valueOf(value);
						Float tempFloat = Float.valueOf(temp);
						if (part.startsWith("!=")) {
							ok = !valueFloat.equals(tempFloat);
						} else if (part.startsWith(">=")) {
							ok = valueFloat.compareTo(tempFloat) >= 0;
						} else if (part.startsWith("<=")) {
							ok = valueFloat.compareTo(tempFloat) <= 0;
						} else if (part.startsWith(">")) {
							ok = valueFloat.compareTo(tempFloat) > 0;
						} else if (part.startsWith("<")) {
							ok = valueFloat.compareTo(tempFloat) < 0;
						}
					} else {
						// treat them as string
						if (part.startsWith("!=")) {
							ok = !value.equals(temp);
						} else if (part.startsWith(">=")) {
							ok = value.compareTo(temp) >= 0;
						} else if (part.startsWith("<=")) {
							ok = value.compareTo(temp) <= 0;
						} else if (part.startsWith(">")) {
							ok = value.compareTo(temp) > 0;
						} else if (part.startsWith("<")) {
							ok = value.compareTo(temp) < 0;
						}
					}

					if (!ok) {
						return false;
					}
				}
				return true;
			}
		}

		return false;
	}
}
