package com.neuralt.smp.client.util;

import java.io.File;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.web.AppConfig;

/**
 * Currently not applicable to Windows
 * 
 */
public class SudoScriptRunner {
	private static final Logger logger = LoggerFactory
			.getLogger(SudoScriptRunner.class);
	private static final int PREFIX_ARGUMENT = 3;

	public static Process exec(String command) throws IOException {
		return exec(new String[] { command });
	}

	public static Process exec(String[] command) throws IOException {
		String dir = getDir(command);
		command[0] = makeRunnableCommand(command[0]);

		String[] sudoComm = new String[command.length + PREFIX_ARGUMENT];
		sudoComm[0] = "sudo";
		sudoComm[1] = "-u";
		sudoComm[2] = AppConfig.getScriptRunnerUser();
		for (int i = PREFIX_ARGUMENT; i < command.length + PREFIX_ARGUMENT; i++) {
			sudoComm[i] = command[i - PREFIX_ARGUMENT];
		}
		ProcessBuilder builder = new ProcessBuilder(sudoComm);
		logger.debug("changing working directory to [{}]", dir);
		builder.directory(new File(dir));
		// builder.redirectErrorStream(true);
		logger.debug("executing command: [{}]", command);
		Process process = builder.start();
		builder = null;

		return process;
	}

	private static String getDir(String[] command) {
		String dir = ".";
		if (command[0].lastIndexOf('/') > 0)
			dir = command[0].substring(0, command[0].lastIndexOf('/'));
		return dir;
	}

	public static String makeRunnableCommand(String command) {
		if (command.lastIndexOf('/') > 0) {
			command = "./" + command.substring(command.lastIndexOf('/') + 1);
		} else if (command != null && !command.startsWith("./"))
			command = "./" + command;

		return command;
	}
}
