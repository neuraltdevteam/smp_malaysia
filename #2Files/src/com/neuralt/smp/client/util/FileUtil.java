package com.neuralt.smp.client.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FileUtil {

	private static final Logger logger = LoggerFactory
			.getLogger(FileUtil.class);

	public static boolean moveFile(String filename, String fromDir, String toDir)
			throws IOException {
		boolean ok = true;
		int seperator = Math.max(filename.lastIndexOf("/"),
				filename.lastIndexOf("\\"));
		if (seperator >= 0) {
			filename = filename.substring(seperator + 1);
		}
		File oldFile = new File(fromDir, filename);
		File newFile = new File(toDir, filename);
		if (!oldFile.exists()) {
			ok = false;
		}
		if (newFile.exists()) {
			newFile.delete();
		}

		if (ok) {
			FileOutputStream fos = null;
			FileInputStream fis = null;
			try {
				fos = new FileOutputStream(newFile);
				fis = new FileInputStream(oldFile);
				int BUFFER_SIZE = 8192;
				byte[] buffer = new byte[BUFFER_SIZE];
				int a;
				while (true) {
					a = fis.read(buffer);
					if (a < 0)
						break;
					fos.write(buffer, 0, a);
					fos.flush();
				}
			} catch (FileNotFoundException e) {
				logger.error("failed in moving file", e);
				ok = false;
			} catch (IOException e) {
				logger.error("failed in moving file", e);
				ok = false;
			} finally {
				if (fos != null) {
					fos.close();
				}
				if (fis != null) {
					fis.close();
				}
			}
			if (ok) {
				oldFile.delete();
			}
		}
		return ok;
	}
}
