package com.neuralt.smp.client.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.neuralt.smp.client.data.DataAccess;

public class ModelDataTable<T> {
	
	public static final String ALL = "All";
	
	public static final String NAME = "name";

	protected final Class<T> modelType;
	protected final String hql;
	protected final Map<String,Object> param;
	
	protected T selected;
	
	public ModelDataTable(Class<T> modelType, String baseCriteria, Object ... baseParam){
		this.modelType = modelType;
		this.hql = "from "+modelType.getSimpleName()+" where "+baseCriteria;
		this.param = new HashMap<String,Object>();
		String key = null;
		for(Object p : baseParam){
			if(key == null){
				key = (String)p;
			}else{
				this.param.put(key, p);
				key = null;
			}
		}
	}
	
	public Class<T> getModelType() {
		return modelType;
	}

	public String getHql() {
		return hql;
	}

	public Map<String, Object> getParam() {
		return param;
	}

	public List<T> list(Map<String,Object> overrideParam, String extraHql){
		Map<String,Object> param = new HashMap<String,Object>();
		param.putAll(this.param);
		if(overrideParam != null){
			param.putAll(overrideParam);
		}
		String hql = this.hql;
		if(extraHql != null){
			hql+=" "+extraHql;
		}
		return DataAccess.genDao.find(modelType, hql, param);
	}
	
	public List<T> list(Map<String,Object> overrideParam){
		return list(overrideParam, null);
	}
	
	public List<T> list(String extraHql){
		return list(null, extraHql);
	}
	
	public List<T> list(){
		return list(null, null);
	}
	
	public String getParamName(){
		String name = (String)param.get(NAME);
		if(name == null || name.equals("%")){
			return ALL;
		}else{
			return name;
		}
	}
	public void setParamName(String name){
		if(name == null || name.equalsIgnoreCase(ALL)){
			name = "%";
		}
		param.put(NAME, name);
		selected = null;
	}
	
	public T getSelected(boolean reload) {
		if(selected == null || reload){
			//auto select unique result
			List<T> list = list();
			if(list.size() == 1){
				selected = list.get(0);
			}else{
				selected = null;
			}
		}
		return selected;
	}
	public T getSelected(){
		return getSelected(false);
	}
	public void setSelected(T selected) {
		this.selected = selected;
	}
}
