package com.neuralt.smp.client.util;

import javax.faces.context.FacesContext;

public class FacesUtil {
	
	protected static String defaultBeanName(Class<?> type){
		String typeName = type.getSimpleName();
		return Character.toLowerCase(typeName.charAt(0)) + typeName.substring(1);
	}

	public static <T> T getBean(String name, Class<T> type){
		FacesContext context = FacesContext.getCurrentInstance();
	    return context.getApplication().evaluateExpressionGet(context,"#{"+name+"}",type);
	}
	
	public static <T> T getBean(Class<T> type){
		return getBean(defaultBeanName(type), type);
	}
}
