package com.neuralt.smp.client.util;

import java.util.LinkedList;

public class FixedLengthLinkedList<T> extends LinkedList<T> {

	private static final long serialVersionUID = -7714309617961711992L;

	private int maxSize = 1000;

	public FixedLengthLinkedList(int maxSize) {
		super();
		this.maxSize = maxSize;
	}

	@Override
	public boolean add(T item) {
		boolean rc = super.add(item);
		trimList();
		return rc;
	}

	// note: caller should handle the synchronization
	private void trimList() {
		if (this.size() > maxSize)
			this.removeFirst();
	}
}
