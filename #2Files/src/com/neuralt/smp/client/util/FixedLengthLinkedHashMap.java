package com.neuralt.smp.client.util;

import java.util.LinkedHashMap;

public class FixedLengthLinkedHashMap<K, V> extends LinkedHashMap<K, V> {
	private static final long serialVersionUID = -1309219356550676684L;
	private int capacity;

	public FixedLengthLinkedHashMap(int initialCapacity) {
		super(initialCapacity);
		capacity = initialCapacity;
	}

	@Override
	protected boolean removeEldestEntry(java.util.Map.Entry<K, V> arg0) {
		return this.size() > capacity;
	}

}
