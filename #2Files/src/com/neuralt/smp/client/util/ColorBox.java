package com.neuralt.smp.client.util;

import java.awt.Color;
import java.util.LinkedList;
import java.util.Queue;

/**
 * As drawing rrd4j graph needs to specify the color, this class is served for
 * this purpose. This class default can serve up to 8 colors. The color
 * selection process will be expanded for having more choices.
 *
 */
public class ColorBox {

	private Queue<Color> colors;

	private ColorBox() {

	}

	public static ColorBox createColorBox() {
		ColorBox box = new ColorBox();
		box.colors = new LinkedList<Color>();
		box.colors.add(Color.BLUE);
		box.colors.add(Color.GREEN);
		box.colors.add(Color.RED);
		box.colors.add(Color.CYAN);
		box.colors.add(Color.ORANGE);
		box.colors.add(Color.MAGENTA);
		box.colors.add(Color.PINK);
		box.colors.add(Color.DARK_GRAY);
		return box;
	}

	public Color nextColor() {
		if (colors.size() != 0)
			return colors.poll();
		else
			return Color.BLACK;
	}
}
