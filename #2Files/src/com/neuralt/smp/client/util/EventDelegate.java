package com.neuralt.smp.client.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.PDU;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.agent.mbean.model.HostCPUUsage;
import com.neuralt.smp.agent.mbean.model.HostDiskUsage;
import com.neuralt.smp.agent.mbean.model.HostInetStatus;
import com.neuralt.smp.agent.mbean.model.HostMemoryUsage;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.SnmpTrap.Direction;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.snmp.proxy.SvrSnmpConstants;

/**
 * Used by SMP to generate and send traps for events
 */
public class EventDelegate {
	public static final String LOCALHOST = "0.0.0.0";

	private static final Logger logger = LoggerFactory
			.getLogger(EventDelegate.class);

	public static EventDelegate instance = new EventDelegate();

	public SnmpTrap onRemAppNotPresent(MonitoredHost host, String appName,
			String details, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remAppNotPresentNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.MAJOR.name());
			trap.setTrapAppName(appName);
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			// trap.setAlarmDescription(n.getMessage()+" "+n.getEvtType().name());
			trap.setAlarmDescription("App is not presented," + details);

			if (AppConfig.isRemAppNotPresentDB())
				DataAccess.snmpTrapDao.save(trap);

			// SmpAlarmService.instance.recordSnmpTrap(trap); // <-- not doing
			// this for outgoing traps anymore. event service should take over
			// it.
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onSysStartUpSuccess(WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.sysStartUpSuccessNotification);
			trap.setSource(AppConfig.getMyName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.INFO.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(LOCALHOST);
			trap.setAlarmDescription("System started successfully");
			trap.setDirection(Direction.OUTGOING);

			if (AppConfig.isSysStartUpSuccessDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}

		return trap;
	}

	public SnmpTrap onSysStartUpFail(String message, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.sysStartUpFailNotification);
			trap.setSource(AppConfig.getMyName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.WARNING.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(LOCALHOST);
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(message);

			if (AppConfig.isSysStartUpFailDB())
				DataAccess.snmpTrapDao.save(trap);

		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onSysConfigReload(String msg, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.sysConfigReloadNotification);
			trap.setSource(AppConfig.getMyName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.INFO.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(LOCALHOST);
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(msg);

			if (AppConfig.isSysConfigReloadDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onConfigReloadError(String msg, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.sysConfigErrorNotification);
			trap.setSource(AppConfig.getMyName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.INFO.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(LOCALHOST);
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(msg);

			if (AppConfig.isConfigReloadErrorDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemHostDown(MonitoredHost host, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remHostDownNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.WARNING.name());
			trap.setTrapAppName(host.getConfig().getName());
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription("HOST DOWN");

			if (AppConfig.isRemHostDownDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemAgentDisconnected(MonitoredHost host,
			WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remHostDownNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.WARNING.name());
			trap.setTrapAppName(host.getConfig().getName());
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription("SMP agent disconnected");

			if (AppConfig.isRemHostDownDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemHostUp(MonitoredHost host, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remHostUpNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.INFO.name());
			trap.setTrapAppName(host.getConfig().getName());
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription("HOST UP");

			if (AppConfig.isRemHostUpDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemAgentConnected(MonitoredHost host,
			WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remHostUpNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.INFO.name());
			trap.setTrapAppName(host.getConfig().getName());
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription("SMP agent connected");

			if (AppConfig.isRemHostUpDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemHostCpuOverload(MonitoredHost host,
			WarningLevel level, HostCPUUsage usage) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(getOIDByLevel(level)); // OID by level
			trap.setSource(host.getConfig().getName());
			trap.setTrapAlarmLevel(level.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(host.getConfig().getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(usage.getMessage());

			if (AppConfig.isRemHostCpuOverloadDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemHostDiskOverload(MonitoredHost host,
			WarningLevel level, HostDiskUsage usage) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(getOIDByLevel(level)); // OID by level
			trap.setSource(host.getConfig().getName());
			trap.setTrapAlarmLevel(level.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(host.getConfig().getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(usage.getMessage());

			if (AppConfig.isRemHostDiskOverloadDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemHostMemOverload(MonitoredHost host,
			WarningLevel level, HostMemoryUsage usage) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(getOIDByLevel(level)); // OID by level
			trap.setSource(host.getConfig().getName());
			trap.setTrapAlarmLevel(level.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(host.getConfig().getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(usage.getMessage());

			if (AppConfig.isRemHostMemOverloadDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemHostInetOverload(MonitoredHost host,
			WarningLevel level, HostInetStatus status) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(getOIDByLevel(level)); // OID by level
			trap.setSource(host.getConfig().getName());
			trap.setTrapAlarmLevel(level.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(host.getConfig().getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(status.getMessage());

			if (AppConfig.isRemHostInetDownDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemHostInetDown(MonitoredHost host, WarningLevel level,
			String message) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(getOIDByLevel(level)); // OID by level
			trap.setSource(host.getConfig().getName());
			trap.setTrapAlarmLevel(level.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(host.getConfig().getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(message);

			if (AppConfig.isRemHostInetDownDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemHostSnmpError(MonitoredHost host, String details,
			WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.snmpQueryFailedNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.WARNING.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(host.getConfig().getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(details);

			if (AppConfig.isRemHostSnmpErrorDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemAppStart(MonitoredHost host, String procName,
			WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remAppStartNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.INFO.name());
			trap.setTrapAppName(procName);
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription("Starting remote process");

			if (AppConfig.isRemAppStartDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}

		return trap;
	}

	public SnmpTrap onRemAppStop(MonitoredHost host, String procName,
			WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remAppStopNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.INFO.name());
			trap.setTrapAppName(procName);
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription("Stopping remote process");

			if (AppConfig.isRemAppStopDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemAppStartSuccess(MonitoredHost host, String procName,
			String details, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remAppStartSuccessNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.INFO.name());
			trap.setTrapAppName(procName);
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription("Started remote process successfully:"
					+ details);

			if (AppConfig.isRemAppStartSuccessDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemAppStartFail(MonitoredHost host, String procName,
			String details, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remAppStartFailNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.WARNING.name());
			trap.setTrapAppName(procName);
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription("Failed to start remote process, "
					+ details);

			if (AppConfig.isRemAppStartFailDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemAppStopSuccess(MonitoredHost host, String procName,
			String message, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remAppStopSuccessNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.INFO.name());
			trap.setTrapAppName(procName);
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(message);

			if (AppConfig.isRemAppStopSuccessDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemAppStopFail(MonitoredHost host, String procName,
			String details, WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.remAppStopFailNotification);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.WARNING.name());
			trap.setTrapAppName(procName);
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription("Failed to stop remote process, "
					+ details);

			if (AppConfig.isRemAppStopFailDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onRemHostGeneralError(MonitoredHost host, String details,
			WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.svrGensystemProblem);
			trap.setSource(host.getConfig().getName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.WARNING.name());
			trap.setTrapAppName("SMP Agent");
			trap.setSourceIp(host.getIpAddr());
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(details);

			if (AppConfig.isRemHostGeneralErrorDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public SnmpTrap onLocalSystemGeneralError(String details,
			WarningLevel warningLevel) {
		SnmpTrap trap = new SnmpTrap();
		try {
			trap.setOid(SvrSnmpConstants.svrGensystemProblem);
			trap.setSource(AppConfig.getMyName());
			if (warningLevel != null)
				trap.setTrapAlarmLevel(warningLevel.name());
			else
				trap.setTrapAlarmLevel(WarningLevel.WARNING.name());
			trap.setTrapAppName(AppConfig.getMyName());
			trap.setSourceIp(LOCALHOST);
			trap.setDirection(Direction.OUTGOING);
			trap.setAlarmDescription(details);

			if (AppConfig.isRemHostGeneralErrorDB())
				DataAccess.snmpTrapDao.save(trap);
		} catch (Exception e) {
			logger.error("Exception occured while preparing snmp alarm", e);
		}
		return trap;
	}

	public PDU alarmToPdu(SnmpTrap alarm) {
		PDU pdu = new PDU();
		pdu.setType(PDU.NOTIFICATION);
		pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID,
				SvrSnmpConstants.systemMajorAlarm));
		pdu.add(new VariableBinding(SvrSnmpConstants.trapAppName,
				new OctetString(alarm.getTrapAppName())));
		pdu.add(new VariableBinding(SvrSnmpConstants.trapDescription,
				new OctetString(alarm.getAlarmDescription())));
		return pdu;
	}

	private OID getOIDByLevel(WarningLevel level) {
		OID oid = null;
		if (WarningLevel.MAJOR.equals(level)) {
			oid = SvrSnmpConstants.sysMajorNotification;
		} else if (WarningLevel.WARNING.equals(level)) {
			oid = SvrSnmpConstants.sysWarningNotification;
		} else if (WarningLevel.MINOR.equals(level)) {
			oid = SvrSnmpConstants.sysMinorNotification;
		} else if (WarningLevel.INFO.equals(level)
				|| WarningLevel.NORMAL.equals(level)) {
			oid = SvrSnmpConstants.sysInfoNotifcation;
		} else if (WarningLevel.ERROR.equals(level)) {
			oid = SvrSnmpConstants.sysErrorNotification;
		} else if (WarningLevel.FATAL.equals(level)) {
			oid = SvrSnmpConstants.sysCriticalNotification;
		} else {
			logger.warn(
					"Unknown warning level {} when trying to map warning level to OID",
					level);
			oid = SvrSnmpConstants.sysInfoNotifcation;
		}
		return oid;
	}

}
