package com.neuralt.smp.client.data;

import org.primefaces.model.chart.CartesianChartModel;

public class GraphWrapper {
	private CartesianChartModel chart;
	private String title;
	private String vLabel;
	private String extender;
	private long start;

	public CartesianChartModel getChart() {
		return chart;
	}

	public void setChart(CartesianChartModel chart) {
		this.chart = chart;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getvLabel() {
		return vLabel;
	}

	public void setvLabel(String vLabel) {
		this.vLabel = vLabel;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public String getExtender() {
		return extender;
	}

	public void setExtender(String extender) {
		this.extender = extender;
	}
}
