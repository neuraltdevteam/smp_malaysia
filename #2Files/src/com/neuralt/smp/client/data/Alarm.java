package com.neuralt.smp.client.data;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.StringUtil;

/**
 * Models an system "alarm" that needs to be handled.
 * 
 * @author
 *
 */
@Entity
@Table(name = "ALARM")
public class Alarm extends PersistenceEntity implements Comparable<Alarm> {

	private static final long serialVersionUID = 2499488851845910439L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String name; // default is associated trap/event name
	private String status; // unhandled, acknowledged, ignored, processing ...
							// etc. configurable.
	@Enumerated(EnumType.STRING)
	@Column(name = "warningLevel")
	private WarningLevel level;

	private String ownerId; // id of ConsoleUser who should handle this
	private String resolverId; // id of ConsoleUser who marked this as resolved
	// @Enumerated(EnumType.STRING)
	// private SourceType sourceType; // TODO currently not included in trap. no
	// way to know.
	private String source;
	private String sourceIp;
	private String description;

	private String cause; // trap/event that caused this alarm. maps to
							// oid/event name?

	private Date firstOccurred;
	private Date lastOccurred;

	/**
	 * The total occurrence of the alarm
	 */
	private int occurrence;
	/**
	 * The occurrence count of the alarm since last notification. This will be
	 * reset to zero if an notification of type "unattended" is sent.
	 */
	private int occurrenceCount;

	private Date resolved;
	private Date lastNotified;

	@Transient
	private String age;

	public Alarm() {
		lastNotified = new Date(0);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	// public SourceType getSourceType() {
	// return sourceType;
	// }
	// public void setSourceType(SourceType sourceType) {
	// this.sourceType = sourceType;
	// }
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public WarningLevel getLevel() {
		return level;
	}

	public void setLevel(WarningLevel level) {
		this.level = level;
	}

	public String getLevelString() {
		return level == null ? null : level.name();
	}

	public String getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}

	public String getResolverId() {
		return resolverId;
	}

	public void setResolverId(String resolverId) {
		this.resolverId = resolverId;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceIp() {
		return sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCause() {
		return cause;
	}

	public void setCause(String cause) {
		this.cause = cause;
	}

	public Date getFirstOccurred() {
		return firstOccurred;
	}

	public void setFirstOccurred(Date firstOccurred) {
		this.firstOccurred = firstOccurred;
	}

	public Date getLastOccurred() {
		return lastOccurred;
	}

	public void setLastOccurred(Date lastOccurred) {
		this.lastOccurred = lastOccurred;
	}

	public int getOccurrence() {
		return occurrence;
	}

	public void setOccurrence(int occurrence) {
		this.occurrence = occurrence;
	}

	public Date getResolved() {
		return resolved;
	}

	public void setResolved(Date resolved) {
		this.resolved = resolved;
	}

	public boolean isResolved() {
		return resolved != null;
	}

	public void incrOccurrence() {
		++occurrence;
		++occurrenceCount;
		lastOccurred = new Date();
	}

	public String getDisplayableSource() {
		// return source+" "+sourceIp;
		return source;
	}

	public Date getLastNotified() {
		return lastNotified;
	}

	public void setLastNotified(Date lastNotified) {
		this.lastNotified = lastNotified;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public int getOccurrenceCount() {
		return occurrenceCount;
	}

	public void setOccurrenceCount(int occurrenceCount) {
		this.occurrenceCount = occurrenceCount;
	}

	@Transient
	public String[] getCsvRow() {
		String[] row = new String[13];

		return row;
	}

	@Override
	public int compareTo(Alarm o) {
		return getLastOccurred().compareTo(o.getLastOccurred());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Alarm other = (Alarm) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Alarm: [name=").append(name).append(", status=")
				.append(status).append(", level=").append(level)
				.append(", source=").append(source).append(", sourceIp=")
				.append(sourceIp).append(", cause=").append(cause)
				.append(", firstOccurred=").append(firstOccurred.toString())
				.append(", lastOccurred=").append(lastOccurred.toString())
				.append(", ownerId=").append(ownerId);
		if (!StringUtil.isNullOrEmpty(resolverId)) {
			sb.append(", resolverId=").append(resolverId);
		}
		if (resolved != null) {
			sb.append(", resolved=").append(resolved.toString());
		}
		sb.append("]");
		return sb.toString();
	}

}
