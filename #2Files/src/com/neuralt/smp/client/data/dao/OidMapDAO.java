package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.Restrictions;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.data.OidMap;

public class OidMapDAO extends GenericDAO<OidMap, Long> {

	public OidMapDAO(boolean callerManagedTransAllowed) {
		super(OidMap.class, callerManagedTransAllowed);
	}

	public List<OidMap> getAllOidMaps() {
		List<OidMap> list = findByCriteriaAndRemoveDuplicate(basicCriteria(),
				null, null);
		Collections.sort(list);
		return list;
	}

	public OidMap getOidMapByOid(OID oid) throws Exception {
		List<OidMap> list = findByCriteria(
				basicCriteria().add(Restrictions.eq("oid", oid)), null, null);
		return (list != null && !list.isEmpty()) ? list.get(0) : null;
	}
}
