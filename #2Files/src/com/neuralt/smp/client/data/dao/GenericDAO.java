package com.neuralt.smp.client.data.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccessException;

// wraps around rawDAO
// Class specific DAOs should extend this

public class GenericDAO<T, ID extends Serializable> {

	protected Class<T> entityClass;
	protected RawDAO rawDao;

	protected static final Logger logger = LoggerFactory
			.getLogger(GenericDAO.class);

	protected DetachedCriteria basicCriteria() {
		DetachedCriteria detachedCrit = DetachedCriteria.forClass(entityClass);
		return detachedCrit;
	}

	public GenericDAO(Class<T> entityClass, boolean callerManagedTransAllowed) {
		this.entityClass = entityClass;
		this.rawDao = new RawDAO(callerManagedTransAllowed);
	}

	protected Session getCurrentSession() {
		return rawDao.getCurrentSession();
	}

	protected Transaction getTransaction(Session session) {
		return rawDao.getTransaction(session);
	}

	protected void finishTransaction(Transaction transaction, boolean commit) {
		rawDao.finishTransaction(transaction, commit);
	}

	public synchronized boolean hasCallerManagedTransaction() {
		return rawDao.hasCallerManagedTransaction();
	}

	public synchronized void beginTransaction() throws DataAccessException {
		rawDao.beginTransaction();
	}

	public synchronized void commitTransaction() throws DataAccessException {
		rawDao.commitTransaction();
	}

	public synchronized void rollbackTransaction() throws DataAccessException {
		rawDao.rollbackTransaction();
	}

	// forbidden almighty spell 1
	// why forbidden: Coz we're *supposed* to hide the hibernate mechanics from
	// other classes that use this DAO.
	// i.e. Other classes are supposed to pass parameters to DAO, and DAO builds
	// the criteria object.
	public synchronized List<T> findByCriteria(DetachedCriteria detachedCrit,
			Integer firstResult, Integer maxResults) throws DataAccessException {
		return rawDao.findByCriteria(entityClass, detachedCrit, firstResult,
				maxResults);
	}

	// forbidden almighty spell 2
	public synchronized Object findUniqueByCriteria(
			DetachedCriteria detachedCrit) throws DataAccessException {
		return rawDao.findUniqueByCriteria(entityClass, detachedCrit);
	}

	/**
	 * As query by joining tables will likely produce duplicated results, this
	 * method is here to provide a shortcut for handling that.
	 * 
	 * @param crit
	 *            the detached criteria
	 * @param firstResult
	 *            the first result
	 * @param maxResult
	 *            the max result
	 * @return an ArrayList of required objects, if no result is found, the list
	 *         will be empty but not null
	 */
	public synchronized List<T> findByCriteriaAndRemoveDuplicate(
			DetachedCriteria crit, Integer firstResult, Integer maxResult) {
		List<T> list = new ArrayList<T>();
		List<T> dummy = findByCriteria(crit, firstResult, maxResult);
		for (T temp : dummy) {
			if (!list.contains(temp)) {
				list.add(temp);
			}
		}
		return list;
	}

	public synchronized long count(DetachedCriteria detachedCrit)
			throws DataAccessException {
		return rawDao.count(detachedCrit);
	}

	public synchronized T findByKey(ID key) throws DataAccessException {
		return rawDao.findByKey(entityClass, key);
	}

	public synchronized List<T> findByExample(T exampleInstance,
			String[] excludeProperty) throws DataAccessException {
		return rawDao.findByExample(entityClass, exampleInstance,
				excludeProperty);
	}

	public synchronized List<T> list() throws DataAccessException {
		return rawDao.list(entityClass);
	}

	public synchronized List<T> list(int firstResult, int maxResults)
			throws DataAccessException {
		return rawDao.list(entityClass, firstResult, maxResults);
	}

	public synchronized List<T> getLatestVersions(String table, String nameField)
			throws DataAccessException {
		return rawDao.getLatestVersions(entityClass, table, nameField);
	}

	public synchronized void save(T entity) throws DataAccessException {
		rawDao.save(entity);
	}
	
	public synchronized void merge(T entity) throws DataAccessException {
		rawDao.merge(entity);
	}

	public synchronized void delete(T entity) throws DataAccessException {
		rawDao.delete(entity);
	}

	public synchronized void batchPersist(Collection<T> batch)
			throws DataAccessException {
		rawDao.batchPersist(batch);
	}

	public synchronized void batchPersist(T[] batch) throws DataAccessException {
		rawDao.batchPersist(batch);
	}

	// Actually this might not work well as an inserting save flushes session
	// immediately
	public synchronized void batchSave(Collection<T> batch)
			throws DataAccessException {
		rawDao.batchSave(batch);
	}

	public synchronized void batchDelete(Collection<T> batch)
			throws DataAccessException {
		rawDao.batchDelete(batch);
	}
	
	public synchronized List<T> findByCriteriaBySQL(String strSQL,
			Integer firstResult, Integer maxResults) throws DataAccessException {
		return rawDao.findByCriteriaBySQL(entityClass, strSQL, firstResult,
				maxResults);
	}

	public synchronized List<T> findByCriteriaBySQLSP(String reportmode, String strSQL,
			String sqlwhere, String sqlgroupby, String sqlorderby,
			Integer firstResult, Integer maxResults) throws DataAccessException {
		return rawDao.findByCriteriaBySQLSP(entityClass, reportmode, strSQL, sqlwhere, sqlgroupby, sqlorderby, firstResult,
				maxResults);
	}
	
	public synchronized List<T> findReportByCriteriaBySQLSP(String reportmode, String strSQL,
			String startdate, String enddate, String sqlgroupby, String sqlorderby,
			Integer firstResult, Integer maxResults, String additional_cond) throws DataAccessException {
		return rawDao.findReportByCriteriaBySQLSP(entityClass, reportmode, strSQL, startdate, enddate, sqlgroupby, sqlorderby, firstResult,
				maxResults, additional_cond);
	}
	
}
