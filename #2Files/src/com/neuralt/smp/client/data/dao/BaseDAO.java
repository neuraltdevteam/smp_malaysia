package com.neuralt.smp.client.data.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;

public interface BaseDAO<G extends Serializable> extends AbstractDAO{
	
	public Class<G> getModelType();
	
	public G findById(Serializable id);

	public List<G> findByProperty(String propertyName, Object value, boolean ignoreCase);
	public List<G> findByProperty(String propertyName, Object value);
	
	public G findUniqueByProperty(String propertyName, Object value, boolean ignoreCase);
	public G findUniqueByProperty(String propertyName, Object value);
	
	public List<G> find(final String hql, final Map<String,Object> param, final Integer take, final Integer skip);
	public List<G> find(final String hql, final Map<String,Object> param);
	public List<G> find(final String hql);
	
	public G findUnique(final String hql, final Map<String,Object> param);
	public G findUnique(final String hql);
	
	public List<G> findByCriteria(DetachedCriteria criteria, Integer take, Integer skip);
	public List<G> findByCriteria(DetachedCriteria criteria);
	
	public G findUniqueByCriteria(DetachedCriteria criteria);
	
	public void save(G transientInstance);
	
	public G merge(G detachedInstance);

	public void delete(G persistentInstance);
	
	public List<G> findAll();
	
}
