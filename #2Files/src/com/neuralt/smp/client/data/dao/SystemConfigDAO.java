package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.config.SystemConfig;

public class SystemConfigDAO extends GenericDAO<SystemConfig, Long> {

	private static final int MAX_RESULT = AppConfig.getDBMaxResult();
	private static final Logger logger = LoggerFactory
			.getLogger(SystemConfigDAO.class);

	public SystemConfigDAO(boolean callerManagedTransAllowed) {
		super(SystemConfig.class, callerManagedTransAllowed);
	}

	// public void deleteNullEntry() {
	// DetachedCriteria detachedCrit = basicCriteria();
	// detachedCrit.add(Restrictions.eq("id", new Long(15)));
	// Set<SystemConfig> list = new TreeSet<SystemConfig>();
	// list.addAll(findByCriteria(detachedCrit,null,null));
	// List<SystemConfig> configList = new ArrayList<SystemConfig>();
	// configList.addAll(list);
	// batchDelete(list);
	// }

	public List<SystemConfig> getAllSystemConfigs() {
		List<SystemConfig> list = findByCriteriaAndRemoveDuplicate(
				basicCriteria(), null, null);
		Collections.sort(list);
		return list;
	}
	
	public SystemConfig getSystemByName(String name) {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("name", name));
		List<SystemConfig> list = findByCriteriaAndRemoveDuplicate(crit, null, null);
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

}
