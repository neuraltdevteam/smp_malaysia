package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.config.HostConfig;

public class HostConfigDAO extends GenericDAO<HostConfig, Long> {
	public HostConfigDAO(boolean callerManagedTransAllowed) {
		super(HostConfig.class, callerManagedTransAllowed);
	}

	public void deleteNullEntry() {
		DetachedCriteria detachedCrit = DetachedCriteria.forClass(entityClass);
		DetachedCriteria detachedCrit2 = DetachedCriteria.forClass(entityClass);
		detachedCrit.add(Restrictions.eq("id", new Long(19)));
		detachedCrit2.add(Restrictions.eq("id", new Long(20)));
		Set<HostConfig> list = new TreeSet<HostConfig>();
		list.addAll(findByCriteria(detachedCrit, null, null));
		list.addAll(findByCriteria(detachedCrit2, null, null));
		batchDelete(list);
	}

	public List<HostConfig> getAllEnabledHost() {
		DetachedCriteria crit = DetachedCriteria.forClass(entityClass);
		List<HostConfig> list = findByCriteriaAndRemoveDuplicate(crit, null,
				null);
		Collections.sort(list);
		return list;
	}
}
