package com.neuralt.smp.client.data.dao;

import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.LiveMonitorReporter;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.EventLog;
import com.neuralt.smp.util.StringUtil;

public class EventLogDAO extends GenericDAO<EventLog, Integer> {

	public EventLogDAO(boolean callerManagedTransAllowed) {
		super(EventLog.class, callerManagedTransAllowed);
	}

	protected DetachedCriteria basicCriteria() {
		return DetachedCriteria.forClass(EventLog.class).addOrder(
				Order.desc("eventTime"));
	}

	private DetachedCriteria addFilters(DetachedCriteria crit,
			Map<String, String> filters) {
		for (String property : filters.keySet()) {
			String value = filters.get(property);
			String key = property;
			if (property.equals("level")) {
				key = "trapAlarmLevel";
			}
			if (value != null && !key.equals("sourceType")
					&& !key.equals("appName") && !key.equals("appName")) {
				StringBuilder sb = new StringBuilder();
				sb.append("%");
				sb.append(value);
				sb.append("%");
				crit.add(Restrictions.like(key, sb.toString()).ignoreCase());
			}
		}
		return crit;
	}

	public List<EventLog> getEventLogs(String sysName, String hostName,
			String procName, Timestamp from, Timestamp to, Integer firstResult,
			Integer maxResults) {
		DetachedCriteria crit = basicCriteria();
		if (!StringUtil.isNullOrEmpty(sysName))
			crit.add(Property.forName("sysName").eq(sysName));
		if (!StringUtil.isNullOrEmpty(hostName))
			crit.add(Property.forName("hostName").eq(hostName));
		if (!StringUtil.isNullOrEmpty(procName))
			crit.add(Property.forName("procName").eq(procName));
		if (from != null)
			crit.add(Property.forName("eventTime").ge(from));
		if (to != null)
			crit.add(Property.forName("eventTime").le(to));
		return super.findByCriteria(crit, firstResult, maxResults);
	}

	@Override
	public synchronized void save(EventLog entity) throws DataAccessException {
		super.save(entity);
		LiveMonitorReporter.getInstance().addHotEvent(entity);
	}

	// public List<EventLog> getEventLogs(Integer maxResults) {
	// DetachedCriteria crit = DetachedCriteria.forClass(EventLog.class);
	//
	// crit.addOrder(Order.desc("eventTime"));
	// return super.findByCriteria(crit, 0, maxResults);
	// }

}
