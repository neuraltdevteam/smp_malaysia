package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.ScriptConfig;
import com.neuralt.smp.config.HostConfig;

public class ScriptConfigDAO extends GenericDAO<ScriptConfig, Integer> {
	public ScriptConfigDAO(boolean callerManagedTransAllowed) {
		super(ScriptConfig.class, callerManagedTransAllowed);
	}

	public List<ScriptConfig> getScriptsByHost(HostConfig host) {
		DetachedCriteria crit = basicCriteria().add(
				Restrictions.eq("host", host));
		List<ScriptConfig> list = findByCriteriaAndRemoveDuplicate(crit, null,
				null);
		Collections.sort(list);
		return list;
	}

	public ScriptConfig getScriptsByHostAndName(HostConfig host, String name) {
		DetachedCriteria crit = basicCriteria().add(
				Restrictions.eq("host", host)).add(
				Restrictions.eq("name", name));
		List<ScriptConfig> dummy = findByCriteria(crit, null, null);
		if (dummy.size() != 0) {
			return dummy.get(0);
		}
		return null;
	}

	public void deleteScriptConfigByHost(HostConfig host) {
		List<ScriptConfig> list = getScriptsByHost(host);
		batchDelete(list);
	}
}
