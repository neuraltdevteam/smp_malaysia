package com.neuralt.smp.client.data.dao;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.DataCollectionProcedure;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.client.data.ScriptConfig;

public class DataCollectionProcedureDAO extends
		GenericDAO<DataCollectionProcedure, Integer> {
	public DataCollectionProcedureDAO(boolean callerManagedTransAllowed) {
		super(DataCollectionProcedure.class, callerManagedTransAllowed);
	}

	public DataCollectionProcedure getMethodByRrdDsConfig(RrdDsConfig ds) {
		DetachedCriteria crit = basicCriteria().add(Restrictions.eq("ds", ds));
		List<DataCollectionProcedure> list = findByCriteria(crit, null, null);
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	// public List<DataCollectionProcedure> getMethodBySnmpTarget(SnmpTarget
	// target) {
	// DetachedCriteria crit = basicCriteria().add(Restrictions.eq("snmp",
	// target));
	// List<DataCollectionProcedure> list =
	// findByCriteriaAndRemoveDuplicate(crit,null,null);
	// return list;
	// }

	public List<DataCollectionProcedure> getMethodByScriptConfig(
			ScriptConfig script) {
		DetachedCriteria crit = basicCriteria().add(
				Restrictions.eq("script", script));
		List<DataCollectionProcedure> list = findByCriteriaAndRemoveDuplicate(
				crit, null, null);
		return list;
	}

	public void deleteProcedureByRrdDef(RrdDefConfig def) {
		for (RrdDsConfig ds : def.getRrdDsConfigs()) {
			DetachedCriteria crit = basicCriteria().add(
					Restrictions.eq("ds", ds));
			List<DataCollectionProcedure> list = findByCriteriaAndRemoveDuplicate(
					crit, null, null);
			batchDelete(list);
		}
	}
}
