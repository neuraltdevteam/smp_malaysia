package com.neuralt.smp.client.data.dao;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.type.Type;
import org.slf4j.Logger;

import com.neuralt.smp.client.data.dao.HibernateHelper.TransactionCallback;
import com.neuralt.smp.client.util.HibernateUtil;

abstract public class AbstractHibernateDAO{
	
	protected static final HibernateHelper hibernateHelper = new HibernateHelper();
	
	public Session getSession(){
		return HibernateUtil.getCurrentSession();
	}
	
	abstract protected Logger getLog();
	
	@SuppressWarnings("unchecked")
	public <T> T findModelById(final Class<T> modelClass, final Serializable id){
		getLog().debug("finding Model<"+modelClass.getSimpleName()+"> by id ["+id+"]");
		final Session session = getSession();
		return hibernateHelper.doInTransaction(session, new TransactionCallback<T>(){
			@Override
			public T execute() {
				return (T)session.get(modelClass, id);
			}
		});
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findModelByProperty(final Class<T> modelClass, final String propertyName, final Object value, final boolean ignoreCase) {
		try {
			getLog().debug("finding Model<" + modelClass.getSimpleName()+ "> with Property<" + propertyName + "> : " + value+ (ignoreCase ? " [ignoreCase]" : ""));
			final Session session = getSession();
			return hibernateHelper.doInTransaction(session, new TransactionCallback<List<T>>(){
				@Override
				public List<T> execute() {
					String query = "from " + modelClass.getSimpleName() + " as model";
					if (ignoreCase) {
						query += " where upper(model." + propertyName + ") = upper(:value)";
					} else {
						query += " where model." + propertyName + " = :value";
					}
					return session.createQuery(query)
							.setParameter("value", value)
							.list();
				}
			});
		} catch (RuntimeException e) {
			getLog().error("failed finding Model<" + modelClass.getSimpleName()+ "> with Property<" + propertyName + ">", e);
			throw e;
			
		}finally{
			//closeSession();
		}
	}
	
	public <T> List<T> findModelByProperty(Class<T> modelClass, String propertyName, Object value){
		return findModelByProperty(modelClass, propertyName, value, false);
	}
	
	public <T> T findModelUniqueByProperty(Class<T> modelClass, String propertyName, Object value, boolean ignoreCase){
		List<T> list = findModelByProperty(modelClass, propertyName, value, ignoreCase);
		if(list.size() < 1){
			return null;
		}else{
			return list.get(0);
		}
	}
	
	public <T> T findModelUniqueByProperty(Class<T> modelClass, String propertyName, Object value){
		return findModelUniqueByProperty(modelClass, propertyName, value, false);
	}
	
	public <T> List<T> find(Class<T> resultClass, final String hql, final Map<String,Object> param, final Integer take, final Integer skip){
		try{
			final Session session = getSession();
			List<T> list = hibernateHelper.doInTransaction(session, new TransactionCallback<List<T>>(){
				@SuppressWarnings("unchecked")
				@Override
				public List<T> execute() {
					Query query = session.createQuery(hql);
					if (param != null) {
						for (Map.Entry<String, Object> p:param.entrySet()) {
							if (p.getValue() instanceof Collection) {
								query.setParameterList(p.getKey(), (Collection<?>)p.getValue());
							}/*else if(p.getValue() instanceof Timestamp){
								query.setTimestamp(p.getKey(),(Timestamp)p.getValue());
							}else if(p.getValue() instanceof Date){
								query.setDate(p.getKey(),(Date)p.getValue());
							}*/ else {
								query.setParameter(p.getKey(),p.getValue());
							}
						}
					}
					if(skip != null && skip > 0){
						query.setFirstResult(skip);
					}
					if(take != null && take > 0){
						query.setMaxResults(take);
						query.setFetchSize(take);
					}
					return query.list();
				}
			});
			getLog().debug("fetched ["+list.size()+"] in query ["+hql+"]"+(param==null?"":" with param "+param.toString()));
			return list;
			
		}catch(RuntimeException e){
			getLog().error("failed query ["+hql+"]"+(param==null?"":" with param "+param.toString()), e);
			throw e;
			
		}finally{
			//closeSession();
		}
	}
	
	public <T> List<T> find(Class<T> resultClass, final String hql, final Map<String,Object> param){
		return find(resultClass, hql, param, null, null);
	}
	
	public <T> List<T> find(Class<T> resultClass, final String hql){
		return find(resultClass, hql, null, null, null);
	}
	
	public <T> T findUnique(Class<T> resultClass, final String hql, final Map<String,Object> param){
		List<T> list = find(resultClass, hql, param, 1, null);
		if(list.size() < 1){
			return null;
		}else{
			return list.get(0);
		}
	}
	
	public <T> T findUnique(Class<T> resultClass, final String hql){
		return findUnique(resultClass, hql, null);
	}
	
	@SuppressWarnings("unchecked")
	public List<Map<String,Object>> findBySQL(final String sql, final Map<String, Type> map, final Map<String,Object> param, final Integer take, final Integer skip){
		try{
			final String[] keys = new String[map.size()];
			final Session session = getSession();
			List<?> raw = hibernateHelper.doInTransaction(session, new TransactionCallback<List<?>>(){
				@Override
				public List<?> execute() {
					SQLQuery query = session.createSQLQuery(sql);
					
					//bind return types
					int index=0;
					for(Map.Entry<String, Type> m : map.entrySet()){
						query.addScalar(m.getKey(), m.getValue());
						keys[index++]=m.getKey();
					}
					
					//bind input params
					if (param != null) {
						for (Map.Entry<String, Object> p:param.entrySet()) {
							if (p.getValue() instanceof Collection) {
								query.setParameterList(p.getKey(), (Collection<?>)p.getValue());
							}/* else if(p.getValue() instanceof Timestamp){
								query.setTimestamp(p.getKey(),(Timestamp)p.getValue());
							}else if(p.getValue() instanceof Date){
								query.setDate(p.getKey(),(Date)p.getValue());
							}*/ else {
								query.setParameter(p.getKey(),p.getValue());
							}
						}
					}
					if(skip != null && skip > 0){
						query.setFirstResult(skip);
					}
					if(take != null && take > 0){
						query.setMaxResults(take);
						query.setFetchSize(take);
					}
					return query.list();
				}
			});
			List<Map<String, Object>> ret = new ArrayList<Map<String, Object>>();
			
			if(map.size() > 1){
				//raw is list of object array
				for(Object[] data: (List<Object[]>)raw){
					Map<String, Object> responseStruct = new HashMap<String, Object>();
					for(int i=0; i<keys.length; i++){
						responseStruct.put(keys[i], data[i]);
					}
					ret.add(responseStruct);
				}
			}else{
				//raw is list of object
				for(Object data : raw){
					Map<String, Object> responseStruct = new HashMap<String, Object>();
					responseStruct.put(keys[0], data);
					ret.add(responseStruct);
				}
			}
			getLog().debug("fetched ["+ret.size()+"] in query ["+sql+"]"+(param==null?"":" with param "+param.toString()));
			return ret;
			
		}catch(RuntimeException e){
			getLog().error("failed query ["+sql+"]"+(param==null?"":" with param "+param.toString()), e);
			throw e;
			
		}finally{
			//closeSession();
		}
	}
	
	public List<Map<String,Object>> findBySQL(final String sql, final Map<String, Type> map, final Map<String,Object> param){
		return findBySQL(sql, map, param, null, null);
	}
	
	public List<Map<String,Object>> findBySQL(final String sql, final Map<String, Type> map){
		return findBySQL(sql, map, null, null, null);
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> findBySQL(Class<T> resultClass, Type hibernateType, final String sql, final Map<String,Object> param, final Integer take, final Integer skip){
		final Map<String,Type> map = new HashMap<String,Type>();
		map.put("result", hibernateType);
		List<Map<String,Object>> listMap = this.findBySQL(sql, map, param, take, skip);
		List<T> list = new ArrayList<T>();
		for(Map<String,Object> r : listMap){
			list.add((T)r.get("result"));
		}
		return list;
	}
	
	public <T> List<T> findBySQL(Class<T> resultClass, Type hibernateType, final String sql, final Map<String,Object> param){
		return this.findBySQL(resultClass, hibernateType, sql, param, null, null);
	}
	
	public <T> List<T> findBySQL(Class<T> resultClass, Type hibernateType, final String sql){
		return this.findBySQL(resultClass, hibernateType, sql, null, null, null);
	}
	
	public Map<String,Object> findUniqueBySQL(final String sql, final Map<String, Type> map, final Map<String,Object> param){
		List<Map<String,Object>> list = this.findBySQL(sql, map, param, 1, null);
		if(list.size() < 1){
			return null;
		}else{
			return list.get(0);
		}
	}
	
	public Map<String,Object> findUniqueBySQL(final String sql, final Map<String, Type> map){
		return findUniqueBySQL(sql, map, null);
	}
	
	public <T> T findUniqueBySQL(Class<T> resultClass, Type hibernateType, final String sql, final Map<String,Object> param){
		List<T> list = this.findBySQL(resultClass, hibernateType, sql, param, 1, null);
		if(list.size() < 1){
			return null;
		}else{
			return list.get(0);
		}
	}
	
	public <T> T findUniqueBySQL(Class<T> resultClass, Type hibernateType, final String sql){
		return this.findUniqueBySQL(resultClass, hibernateType, sql, null);
	}
	
	public int executeUpdate(final String hql, final Map<String,Object> param){
		try{
			final Session session = getSession();
			return hibernateHelper.doInTransaction(session, new TransactionCallback<Integer>(){
				@Override
				public Integer execute() {
					Query query = session.createQuery(hql);
					if (param != null) {
						for (Map.Entry<String, Object> p:param.entrySet()) {
							if (p.getValue() instanceof Collection) {
								query.setParameterList(p.getKey(), (Collection<?>)p.getValue());
							}else if(p.getValue() instanceof Timestamp){
								query.setTimestamp(p.getKey(),(Timestamp)p.getValue());
							}else if(p.getValue() instanceof Date){
								query.setDate(p.getKey(),(Date)p.getValue());
							} else {
								query.setParameter(p.getKey(),p.getValue());
							}
						}
					}
					int result = query.executeUpdate();
					
					getLog().debug("["+result+"] executed ["+hql+"]"+(param==null?"":" with param "+param.toString()));
					return result;
				}
			});
		}catch(RuntimeException e){
			getLog().error("failed execute ["+hql+"]"+(param==null?"":" with param "+param.toString()), e);
			throw e;
			
		}finally{
			//closeSession();
		}
	}
	
	@SuppressWarnings("unchecked")
	public <T> List<T> findByCriteria(Class<T> modelClass, final DetachedCriteria criteria, final Integer take, final Integer skip) {
		try {
			getLog().debug("finding Model<" + modelClass.getSimpleName()+ "> with criteria : "+criteria.toString());
			final Session session = getSession();
			return hibernateHelper.doInTransaction(session, new TransactionCallback<List<T>>(){
				@Override
				public List<T> execute() {
					Criteria query = criteria.getExecutableCriteria(session);
					
					if(skip != null && skip > 0){
						query.setFirstResult(skip);
					}
					if(take != null && take > 0){
						query.setMaxResults(take);
						query.setFetchSize(take);
					}
					return query.list();
				}
			});
		} catch (RuntimeException e) {
			getLog().error("failed finding Model<" + modelClass.getSimpleName()+ "> with criteria", e);
			throw e;
			
		}finally{
			//closeSession();
		}
	}
	
	public <T> List<T> findByCriteria(Class<T> modelClass, DetachedCriteria criteria){
		return this.findByCriteria(modelClass, criteria, null, null);
	}
	
	public <T> T findUniqueByCriteria(Class<T> modelClass, DetachedCriteria criteria){
		List<T> list = this.findByCriteria(modelClass, criteria, 1, null);
		if(list.size() < 1){
			return null;
		}else{
			return list.get(0);
		}
	}
}
