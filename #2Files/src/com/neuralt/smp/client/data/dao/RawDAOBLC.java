package com.neuralt.smp.client.data.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Example;
import org.hibernate.criterion.Projections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.util.HibernateUtil;

// As we can't instantiate parameterized GenericDAOs using reflection during runtime,
// We'll need to rely on hibernate to avoid creating N DAOs for N pluggable entities

// base for all DAOs

// On error, rollbacks transaction (unless caller managed), and throw DataAccessException
// The cause exception/error is embedded in DataAcessException.
// Caller is expected to catch these runtime exceptions and act accordingly
public class RawDAOBLC implements Serializable {
	private static final long serialVersionUID = -2829876503030580699L;
	Session gSession; // g for global
	Transaction gTransaction;
	boolean callerManagedTransAllowed;

	protected static final Logger logger = LoggerFactory
			.getLogger(RawDAO.class);

	public RawDAOBLC(boolean callerManagedTransAllowed) {
		this.callerManagedTransAllowed = callerManagedTransAllowed;
	}

	Session getCurrentSession() {
		if (gSession != null)
			return gSession;

		return HibernateUtil.getCurrentBLCSession();
	}

	Transaction getTransaction(Session session) {
		if (session == gSession)
			return gTransaction;
		else
			return session.beginTransaction();
	}

	void finishTransaction(Transaction transaction, boolean commit) {
		if (transaction != gTransaction) {
			if (!commit) {
				logger.info("finishTransaction - rolling back transaction");
				transaction.rollback();
			} else {
				transaction.commit();
			}
		}
	}

	public boolean hasCallerManagedTransaction() {
		return gTransaction != null;
	}

	public void beginTransaction() throws DataAccessException {
		if (callerManagedTransAllowed) {
			if (gSession == null) {
				gSession = HibernateUtil.getCurrentBLCSession();
				gTransaction = gSession.beginTransaction();
			} else {
				throw new DataAccessException(
						"instance already has an active transaction");
			}
		} else {
			throw new DataAccessException(
					"Caller-managed-transactions not allowed on this instance");
		}
	}

	public void commitTransaction() throws DataAccessException {
		if (callerManagedTransAllowed) {
			if (gTransaction != null) {
				gTransaction.commit();
				gSession = null;
				gTransaction = null;
			} else {
				throw new DataAccessException("no active transaction");
			}
		} else {
			throw new DataAccessException(
					"Caller-managed-transactions not allowed on this instance");
		}
	}

	public void rollbackTransaction() throws DataAccessException {
		if (callerManagedTransAllowed) {
			if (gTransaction != null) {
				gTransaction.rollback();
				gSession = null;
				gTransaction = null;
			} else {
				throw new DataAccessException("no active transaction");
			}
		} else {
			throw new DataAccessException(
					"Caller-managed-transactions not allowed on this instance");
		}
	}

	// forbidden almighty spell 1
	// why forbidden: Coz we're *supposed* to hide the hibernate mechanics from
	// other classes that use this DAO.
	// i.e. Other classes are supposed to pass parameters to DAO, and DAO builds
	// the criteria object.
	public <T> List<T> findByCriteria(Class<T> entityClass,
			DetachedCriteria detachedCrit, Integer firstResult,
			Integer maxResults) throws DataAccessException {
		logger.debug(entityClass.getSimpleName() + ".findByCriteria start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			Criteria criteria = detachedCrit
					.getExecutableCriteria(localSession);
			if (firstResult != null)
				criteria.setFirstResult(firstResult);
			if (maxResults != null)
				criteria.setMaxResults(maxResults);
			List<T> results = criteria.list();
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName() + ".findByCriteria end");
			return results;
		} catch (Exception e) {
			logger.error("findByCritieria", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in findByCritieria", e);
		}
	}

	// forbidden almighty spell 2
	public <T> T findUniqueByCriteria(Class<T> entityClass,
			DetachedCriteria detachedCrit) throws DataAccessException {
		logger.debug(entityClass.getSimpleName()
				+ ".findUniqueByCriteria start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			Criteria criteria = detachedCrit
					.getExecutableCriteria(localSession);
			T result = entityClass.cast(criteria.uniqueResult());
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName()
					+ ".findUniqueByCriteria end");
			return result;
		} catch (Exception e) {
			logger.error("findUniqueByCriteria", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in findUniqueByCriteria",
					e);
		}

	}

	public long count(DetachedCriteria detachedCrit) throws DataAccessException {
		detachedCrit.setProjection(Projections.rowCount());
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			Criteria criteria = detachedCrit
					.getExecutableCriteria(localSession);
			Long count = (Long) criteria.uniqueResult();
			finishTransaction(tx, true);
			return count;
		} catch (Exception e) {
			logger.error("count", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in count", e);
		}
	}

	public <T> T findByKey(Class<T> entityClass, Serializable key)
			throws DataAccessException {
		logger.debug(entityClass.getSimpleName() + ".findByKey start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			T result = entityClass.cast(localSession.get(entityClass, key));
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName() + ".findByKey end");
			return result;
		} catch (Exception e) {
			logger.error("findByKey", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in findByKey", e);
		}
	}

	public <T> List<T> findByExample(Class<T> entityClass, T exampleInstance,
			String[] excludeProperty) throws DataAccessException {
		logger.debug(entityClass.getSimpleName() + ".findByExample start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			Criteria crit = localSession.createCriteria(entityClass);
			Example example = Example.create(exampleInstance);
			for (String propName : excludeProperty) {
				example.excludeProperty(propName);
			}
			crit.add(example);
			List<T> results = crit.list();
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName() + ".findByExample end");
			return results;
		} catch (Exception e) {
			logger.error("findByExample", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in findByExample", e);
		}
	}

	public <T> List<T> list(Class<T> entityClass) throws DataAccessException {
		logger.debug(entityClass.getSimpleName() + ".list start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			Criteria crit = localSession.createCriteria(entityClass);
			crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			List<T> results = crit.list();
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName() + ".list end");
			return results;
		} catch (Exception e) {
			logger.error("list", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in list", e);
		}
	}

	public <T> List<T> list(Class<T> entityClass, int firstResult,
			int maxResults) throws DataAccessException {
		logger.debug(entityClass.getSimpleName() + ".list start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			Criteria crit = localSession.createCriteria(entityClass);
			crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			crit.setFirstResult(firstResult);
			crit.setMaxResults(maxResults);
			List<T> results = crit.list();
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName() + ".list end");
			return results;
		} catch (Exception e) {
			logger.error("list", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in list", e);
		}
	}

	public <T> List<T> getLatestVersions(Class<T> entityClass, String table,
			String nameField) throws DataAccessException {
		logger.debug(entityClass.getSimpleName() + ".getLatestVersions start");
		Session session = getCurrentSession();
		Transaction tx = getTransaction(session);
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("SELECT a.* FROM %1$s a");
			sb.append(" INNER JOIN (SELECT %2$s, max(version) as version FROM %1$s GROUP BY %2$s) b");
			sb.append(" ON (a.%2$s = b.%2$s AND a.version = b.version)");
			String sqlString = String.format(sb.toString(), table, nameField);
			SQLQuery query = session.createSQLQuery(sqlString);
			query.addEntity(entityClass);
			List<T> results = query.list();
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName() + ".getLatestVersions end");
			return results;
		} catch (Exception e) {
			logger.error("getLatestVersions", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in getLatestVersions", e);
		}
	}

	public <T> void save(T entity) throws DataAccessException {
		logger.debug(entity.getClass().getSimpleName() + ".save start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			localSession.saveOrUpdate(entity);
			finishTransaction(tx, true);
			logger.debug(entity.getClass().getSimpleName() + ".save end");
		} catch (Exception e) {
			logger.error("save", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in save", e);
		}
	}
	
	public <T> void merge(T entity) throws DataAccessException {
		logger.debug(entity.getClass().getSimpleName() + ".merge start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			localSession.merge(entity);
			finishTransaction(tx, true);
			logger.debug(entity.getClass().getSimpleName() + ".merge end");
		} catch (Exception e) {
			logger.error("merge", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in merge", e);
		}
	}

	public <T> void delete(T entity) throws DataAccessException {
		logger.debug(entity.getClass().getSimpleName() + ".delete start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			localSession.delete(entity);
			finishTransaction(tx, true);
			logger.debug(entity.getClass().getSimpleName() + ".delete end");
		} catch (Exception e) {
			logger.error("delete", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in delete", e);
		}
	}

	public <T> void batchPersist(Collection<T> batch)
			throws DataAccessException {
		logger.debug(this.getClass().getSimpleName() + ".batchPersist start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			for (T entity : batch) {
				localSession.persist(entity);
			}
			finishTransaction(tx, true);
			logger.debug(this.getClass().getSimpleName() + ".batchPersist end");
		} catch (Exception e) {
			logger.error("batchPersist", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in batchPersist", e);
		}
	}

	public <T> void batchPersist(T[] batch) throws DataAccessException {
		logger.debug(this.getClass().getSimpleName() + ".batchPersist start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			for (T entity : batch) {
				localSession.persist(entity);
			}
			finishTransaction(tx, true);
			logger.debug(this.getClass().getSimpleName() + ".batchPersist end");
		} catch (Exception e) {
			logger.error("batchPersist", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in batchPersist", e);
		}
	}

	// Actually this might not work well as an inserting save flushes session
	// immediately
	public <T> void batchSave(Collection<T> batch) throws DataAccessException {
		logger.debug(this.getClass().getSimpleName() + ".batchSave start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			for (T entity : batch) {
				localSession.saveOrUpdate(entity);
			}
			finishTransaction(tx, true);
			logger.debug(this.getClass().getSimpleName() + ".batchSave end");
		} catch (Exception e) {
			logger.error("batchSave", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in batchSave", e);
		}
	}

	public <T> void batchDelete(Collection<T> batch) throws DataAccessException {
		logger.debug(this.getClass().getSimpleName() + ".batchDelete start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			for (T entity : batch) {
				localSession.delete(entity);
			}
			finishTransaction(tx, true);
			logger.debug(this.getClass().getSimpleName() + ".batchDelete end");
		} catch (Exception e) {
			logger.error("batchDelete", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in batchDelete", e);
		}
	}
	
	@SuppressWarnings("unchecked")  public <T> List<T> findByCriteriaBySQL(Class<T> entityClass,
			String strSQL, Integer firstResult,
			Integer maxResults) throws DataAccessException {
		
		logger.info("in findByCriteriaBySQL");
		logger.info("strSQL=" + strSQL);
		logger.debug(entityClass.getSimpleName() + ".findByCriteria start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			if (maxResults != null)
				strSQL = strSQL + " limit " + firstResult + "," + maxResults ;			
			SQLQuery query = localSession.createSQLQuery(strSQL);
			query.addEntity(entityClass);
			List<T> results = query.list();
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName() + ".findByCriteriaBySQL end");
			return results;
		} catch (Exception e) {
			logger.error("findByCritieria", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in findByCritieriaBySQL", e);
		}
	}
	
	@SuppressWarnings("unchecked")  public <T> List<T> findReportByCriteriaBySQLSP(Class<T> entityClass,
			String reportmode, String strSQL, String startdate, String enddate, String sqlgroupby, String sqlorderby, Integer firstResult,
			Integer maxResults, String additional_cond) throws DataAccessException {
		
		logger.info("in findReportByCriteriaBySQLSP");
		logger.info("strSQL=" + strSQL);
		logger.debug(entityClass.getSimpleName() + ".findReportByCriteriaBySQLSP start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			String sqllimit = "";
			if (maxResults != null)
				sqllimit = firstResult + "," + maxResults ;			
			
			SQLQuery query = localSession.createSQLQuery(strSQL);
			query.setParameter(0, reportmode);
			query.setParameter(1, startdate);
			query.setParameter(2, enddate);
			query.setParameter(3, sqlgroupby);
			query.setParameter(4, sqlorderby);
			query.setParameter(5, sqllimit);
			query.setParameter(6, additional_cond);
			query.addEntity(entityClass);
			List<T> results = query.list();
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName() + ".findReportByCriteriaBySQLSP end");
			return results;
		} catch (Exception e) {
			logger.error("findByCritieria", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in findReportByCriteriaBySQLSP", e);
		}
	}
	
	@SuppressWarnings("unchecked")  public <T> List<T> findByCriteriaBySQLSP(Class<T> entityClass,
			String reportmode, String strSQL, String sqlwhere, String sqlgroupby, String sqlorderby, Integer firstResult,
			Integer maxResults) throws DataAccessException {
		
		logger.info("in findByCriteriaBySQLSP");
		logger.info("strSQL=" + strSQL);
		logger.debug(entityClass.getSimpleName() + ".findByCriteriaBySQLSP start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			String sqllimit = "";
			if (maxResults != null)
			{
				//sqllimit = " limit " + firstResult + "," + maxResults ;			
				sqllimit = firstResult + "," + maxResults ;
			}
			
			SQLQuery query = localSession.createSQLQuery(strSQL);
			query.setParameter(0, reportmode);
			query.setParameter(1, sqlwhere);
			query.setParameter(2, sqlgroupby);
			query.setParameter(3, sqlorderby);
			query.setParameter(4, sqllimit);
			query.addEntity(entityClass);
			List<T> results = query.list();
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName() + ".findByCriteriaBySQLSP end");
			return results;
		} catch (Exception e) {
			logger.error("findByCritieria", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in findByCriteriaBySQLSP", e);
		}
	}
	
	@SuppressWarnings("unchecked")  public long countBySQL(String strSQL) throws DataAccessException {
		
		//logger.debug(entityClass.getSimpleName() + ".countBySQL start");
		logger.info("strSQL=" + strSQL);
		
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
		
			SQLQuery query = localSession.createSQLQuery(strSQL); 
			//query.addEntity(entityClass);
			Long count = (Long) query.uniqueResult();			
			finishTransaction(tx, true);
			//logger.debug(entityClass.getSimpleName() + ".countBySQL end");
			return count;
		} catch (Exception e) {
			logger.error("countBySQL", e);
			finishTransaction(tx, false);
			throw new DataAccessException("Exception in countBySQL", e);
		}
	}
	
}
