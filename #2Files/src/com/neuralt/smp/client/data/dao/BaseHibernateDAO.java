package com.neuralt.smp.client.data.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;

import com.neuralt.smp.client.data.dao.HibernateHelper.TransactionCallback;

abstract public class BaseHibernateDAO<G extends Serializable> extends AbstractHibernateDAO implements BaseDAO<G>{

	protected final Class<G> modelType;
	
	public BaseHibernateDAO(Class<G> modelType){
		this.modelType = modelType;
	}
	
	public Class<G> getModelType() {
		return modelType;
	}

	public G findById(Serializable id){
		return findModelById(modelType, id);
	}

	public List<G> findByProperty(String propertyName, Object value, boolean ignoreCase) {
		return findModelByProperty(modelType, propertyName, value, ignoreCase);
	}
	
	public List<G> findByProperty(String propertyName, Object value){
		return findByProperty(propertyName, value, false);
	}
	
	public G findUniqueByProperty(String propertyName, Object value, boolean ignoreCase){
		return findModelUniqueByProperty(modelType, propertyName, value, ignoreCase);
	}
	
	public G findUniqueByProperty(String propertyName, Object value){
		return findUniqueByProperty(propertyName, value, false);
	}
	
	public List<G> find(final String hql, final Map<String,Object> param, final Integer take, final Integer skip){
		return find(modelType, hql, param, take, skip);
	}
	
	public List<G> find(final String hql, final Map<String,Object> param){
		return find(hql, param, null, null);
	}
	
	public List<G> find(final String hql){
		return find(hql, null, null, null);
	}
	
	public G findUnique(final String hql, final Map<String,Object> param){
		return findUnique(modelType, hql, param);
	}
	
	public G findUnique(final String hql){
		return findUnique(hql, null);
	}
	
	public List<G> findByCriteria(DetachedCriteria criteria, Integer take, Integer skip) {
		return findByCriteria(modelType, criteria, take, skip);
	}
	
	public List<G> findByCriteria(DetachedCriteria criteria){
		return findByCriteria(modelType, criteria, null, null);
	}
	
	public G findUniqueByCriteria(DetachedCriteria criteria){
		return findUniqueByCriteria(modelType, criteria);
	}
	
	public void save(final G transientInstance) {
		getLog().debug("saving "+modelType.getSimpleName()+" instance");
		try {
			final Session session = getSession();
			hibernateHelper.doInTransaction(session, new TransactionCallback<Object>(){
				@Override
				public Object execute() {
					session.save(transientInstance);
					return null;
				}
			});
			getLog().debug("save successful");
		} catch (RuntimeException re) {
			getLog().error("save failed", re);
			throw re;
			
		}finally{
			//closeSession();
		}
	}
	
	@SuppressWarnings("unchecked")
	public G merge(final G detachedInstance) {
		getLog().debug("merging "+modelType.getSimpleName()+" instance");
		try {
			final Session session = getSession();
			G result = hibernateHelper.doInTransaction(session, new TransactionCallback<G>(){
				@Override
				public G execute() {
					return (G)session.merge(detachedInstance);
				}
			});
			getLog().debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			getLog().error("merge failed", re);
			throw re;
			
		}finally{
			//closeSession();
		}
	}

	public void delete(final G persistentInstance) {
		getLog().debug("deleting "+modelType.getSimpleName()+" instance");
		try {
			final Session session = getSession();
			hibernateHelper.doInTransaction(session, new TransactionCallback<Object>(){
				@Override
				public Object execute() {
					session.delete(persistentInstance);
					return null;
				}
			});
			getLog().debug("delete successful");
		} catch (RuntimeException re) {
			getLog().error("delete failed", re);
			throw re;
			
		}finally{
			//closeSession();
		}
	}
	
	public List<G> findAll() {
		return find("from "+modelType.getSimpleName());
	}
	
}
