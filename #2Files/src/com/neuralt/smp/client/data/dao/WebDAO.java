package com.neuralt.smp.client.data.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;

import com.neuralt.smp.client.web.security.ConsoleUser;
import com.neuralt.smp.client.web.security.PasswordHistory;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.web.security.UserGroup;
import com.neuralt.smp.client.web.security.UserRight;
import com.neuralt.smp.client.web.security.UserRole;

public class WebDAO {

	private RawDAO rawDao;

	public WebDAO() {
		rawDao = new RawDAO(false);
	}

	public ConsoleUser getUser(String userId) {
		return rawDao.findByKey(ConsoleUser.class, userId);
	}

	public ConsoleUser getUserInDepth(String userId) {
		DetachedCriteria crit = DetachedCriteria.forClass(ConsoleUser.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setFetchMode("userGroup.userRoles", FetchMode.JOIN)
				.add(Property.forName("userId").eq(userId));
		return rawDao.findUniqueByCriteria(ConsoleUser.class, crit);
	}

	public List<ConsoleUser> getUsers() {
		return rawDao.list(ConsoleUser.class);
	}

	public void saveUser(ConsoleUser user) {
		saveUser(user, false);
	}

	public void saveUser(ConsoleUser user, boolean savePasswordHistory) {
		rawDao.save(user);
		if (savePasswordHistory) {
			PasswordHistory history = new PasswordHistory();
			history.setConsoleUser(user);
			history.setPassword(user.getPassword());
			history.setLastUpdatedBy(user.getLastUpdatedBy());
			rawDao.save(history);
		}
	}

	public void removeUser(String userId) {
		ConsoleUser user = getUser(userId);
		if (user != null)
			rawDao.delete(user);
	}

	public void removeUser(ConsoleUser user) {
		rawDao.delete(user);
	}

	public UserGroup getUserGroup(String groupId) {
		DetachedCriteria crit = DetachedCriteria.forClass(UserGroup.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setFetchMode("userRoles", FetchMode.JOIN)
				.add(Property.forName("groupId").eq(groupId));
		return rawDao.findUniqueByCriteria(UserGroup.class, crit);
	}

	public List<UserGroup> getUserGroups() {
		return rawDao.list(UserGroup.class);
	}

	public void saveUserGroup(UserGroup group) {
		rawDao.save(group);
	}

	public void removeUserGroup(String groupId) {
		UserGroup group = getUserGroup(groupId);
		if (group != null)
			rawDao.delete(group);
	}

	public void removeUserGroup(UserGroup group) {
		rawDao.delete(group);
	}

	public UserRole getUserRole(String roleId) {
		return rawDao.findByKey(UserRole.class, roleId);
	}

	public List<UserRole> getUserRoles() {
		return rawDao.list(UserRole.class);
	}

	public void saveUserRole(UserRole role) {
		rawDao.save(role);
	}

	public void removeUserRole(String roleId) {
		UserRole role = getUserRole(roleId);
		if (role != null)
			rawDao.delete(role);
	}

	public void removeUserRole(UserRole role) {
		rawDao.delete(role);
	}

	public List<UserRight> getUserRights(ScreenType type) {
		DetachedCriteria crit = DetachedCriteria.forClass(UserRight.class).add(
				Property.forName("typeId").eq(type));
		return rawDao.findByCriteria(UserRight.class, crit, null, null);
	}

	// password encrypted
	public boolean isPasswordUsed(String userId, String password) {
		DetachedCriteria crit = DetachedCriteria
				.forClass(PasswordHistory.class)
				.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY)
				.setProjection(Projections.property("password"))
				.addOrder(Order.desc("lastUpdatedOn"))
				.add(Property.forName("consoleUser.userId").eq(userId));
		final List<String> passwords = rawDao.findByCriteria(String.class,
				crit, 0, 4);
		return passwords.contains(password);
	}
}
