package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.GroupSnmpConfig;

public class GroupSnmpConfigDAO extends GenericDAO<GroupSnmpConfig, Integer> {

	public GroupSnmpConfigDAO(boolean callerManagedTransAllowed) {
		super(GroupSnmpConfig.class, callerManagedTransAllowed);
	}

	protected DetachedCriteria basicCriteria() {
		return DetachedCriteria.forClass(GroupSnmpConfig.class).addOrder(
				Order.asc("name"));
	}

	public GroupSnmpConfig getGroupSnmpConfigByName(String name) {
		DetachedCriteria crit = basicCriteria().add(
				Restrictions.eq("name", name));
		List<GroupSnmpConfig> dummy = super.findByCriteria(crit, null, null);
		if (dummy.size() != 0) {
			return dummy.get(0);
		}
		return null;
	}

	public List<GroupSnmpConfig> getAllGroupSnmpConfigs() {
		List<GroupSnmpConfig> list = findByCriteriaAndRemoveDuplicate(
				basicCriteria(), null, null);
		Collections.sort(list);
		return list;
	}
}
