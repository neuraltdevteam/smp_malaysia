
package com.neuralt.smp.client.data.dao;

/**
 *
 * @author ag111
 */
import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.ManageSMS;

//public class ManageCPDAO extends GenericDAO<ManageCP, Boolean> {

public class ManageSMSDAO extends GenericDAO<ManageSMS, Long> {
    
        public ManageSMSDAO(boolean callerManagedTransAllowed) {
		super(ManageSMS.class, callerManagedTransAllowed);
	}
        
        protected DetachedCriteria basicCriteria() {
		return DetachedCriteria.forClass(ManageSMS.class).addOrder(
				Order.asc("CHANNEL_ID"));
	}
    
        //public SiteDAO(boolean callerManagedTransAllowed) {
	//	super(ManageCP.class, callerManagedTransAllowed);
	//}

	public List<ManageSMS> getAllSMSs() {
		List<ManageSMS> list = findByCriteriaAndRemoveDuplicate(basicCriteria(),
				null, null);
		//Collections.sort(list);
		return list;
	}

	public ManageSMS getSMSByName(String name) {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("CHANNEL", name));
		List<ManageSMS> list = findByCriteriaAndRemoveDuplicate(crit, null, null);
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	public ManageSMS getSMSById(Long id) {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("CHANNEL_ID", id));
		List<ManageSMS> list = findByCriteria(crit, null, null);
		if (list != null && list.size() != 0) {
			return list.get(0);
		}
		return null;
	}
}
