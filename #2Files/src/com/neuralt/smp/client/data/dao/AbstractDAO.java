package com.neuralt.smp.client.data.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.type.Type;

public interface AbstractDAO {

	public <T> T findModelById(Class<T> modelClass, Serializable id);

	public <T> List<T> findModelByProperty(Class<T> modelClass, String propertyName, Object value, boolean ignoreCase);
	public <T> List<T> findModelByProperty(Class<T> modelClass, String propertyName, Object value);
	
	public <T> T findModelUniqueByProperty(Class<T> modelClass, String propertyName, Object value, boolean ignoreCase);
	public <T> T findModelUniqueByProperty(Class<T> modelClass, String propertyName, Object value);
	
	public <T> List<T> find(Class<T> resultClass, final String hql, final Map<String,Object> param, final Integer take, final Integer skip);
	public <T> List<T> find(Class<T> resultClass, final String hql, final Map<String,Object> param);
	public <T> List<T> find(Class<T> resultClass, final String hql);
	
	public <T> T findUnique(Class<T> resultClass, final String hql, final Map<String,Object> param);
	public <T> T findUnique(Class<T> resultClass, final String hql);
	
	public List<Map<String,Object>> findBySQL(final String sql, final Map<String, Type> map, final Map<String,Object> param, final Integer take, final Integer skip);
	public List<Map<String,Object>> findBySQL(final String sql, final Map<String, Type> map, final Map<String,Object> param);
	public List<Map<String,Object>> findBySQL(final String sql, final Map<String, Type> map);
	
	public <T> List<T> findBySQL(Class<T> resultClass, Type hibernateType, final String sql, final Map<String,Object> param, final Integer take, final Integer skip);
	public <T> List<T> findBySQL(Class<T> resultClass, Type hibernateType, final String sql, final Map<String,Object> param);
	public <T> List<T> findBySQL(Class<T> resultClass, Type hibernateType, final String sql);
	
	public Map<String,Object> findUniqueBySQL(final String sql, final Map<String, Type> map, final Map<String,Object> param);
	public Map<String,Object> findUniqueBySQL(final String sql, final Map<String, Type> map);
	
	public <T> T findUniqueBySQL(Class<T> resultClass, Type hibernateType, final String sql, final Map<String,Object> param);
	public <T> T findUniqueBySQL(Class<T> resultClass, Type hibernateType, final String sql);
	
	public int executeUpdate(final String hql, final Map<String,Object> param);
	
	public <T> List<T> findByCriteria(Class<T> modelClass, DetachedCriteria criteria, Integer take, Integer skip);
	public <T> List<T> findByCriteria(Class<T> modelClass, DetachedCriteria criteria);
	
	public <T> T findUniqueByCriteria(Class<T> modelClass, DetachedCriteria criteria);
}
