package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.LiveMonitorReporter;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.web.AppConfig;

public class SnmpTrapDAO extends GenericDAO<SnmpTrap, Long> {

	private static final int MAX_RESULT = AppConfig.getDBMaxResult();

	public SnmpTrapDAO(boolean callerManagedTransAllowed) {
		super(SnmpTrap.class, callerManagedTransAllowed);
	}

	protected DetachedCriteria basicCriteria() {
		DetachedCriteria detachedCrit = DetachedCriteria.forClass(entityClass)
				.addOrder(Order.desc("time"));
		return detachedCrit;
	}

	public List<SnmpTrap> getTrapsByHost(String source,
			Map<String, String> filters) throws Exception {
		DetachedCriteria detachedCrit = basicCriteria();
		if (source != null)
			detachedCrit = detachedCrit
					.add(Restrictions.like("source", source));
		if (filters != null)
			detachedCrit = addFilters(detachedCrit, filters);
		List<SnmpTrap> list = findByCriteriaAndRemoveDuplicate(detachedCrit,
				null, MAX_RESULT);
		Collections.sort(list);
		return list;
	}

	public List<SnmpTrap> getTrapsBySystem(MonitoredSystem system,
			Map<String, String> filters) throws Exception {
		Disjunction dis = Restrictions.disjunction();
		for (MonitoredHost host : system.getHosts()) {
			dis.add(Restrictions.like("source", host.getConfig().getName()));
		}

		DetachedCriteria detachedCrit = basicCriteria().add(dis);
		detachedCrit = addFilters(detachedCrit, filters);
		List<SnmpTrap> list = findByCriteriaAndRemoveDuplicate(detachedCrit,
				null, MAX_RESULT);
		Collections.sort(list);
		return list;
	}

	public List<SnmpTrap> getTrapsByTimeAndHost(Date timeFrom, Date timeTo,
			String source, Map<String, String> filters) throws Exception {
		DetachedCriteria detachedCrit = basicCriteria();
		detachedCrit = addFilters(detachedCrit, filters);
		if (source != null)
			detachedCrit = addSource(detachedCrit, source);
		if (timeFrom != null)
			detachedCrit = addTimeFrom(detachedCrit, timeFrom);
		if (timeTo != null)
			detachedCrit = addTimeTo(detachedCrit, timeTo);
		List<SnmpTrap> list = findByCriteriaAndRemoveDuplicate(detachedCrit,
				null, MAX_RESULT);
		Collections.sort(list);
		return list;
	}

	private DetachedCriteria addSource(DetachedCriteria crit, String source) {
		crit = crit.add(Restrictions.like("source", source));
		return crit;
	}

	private DetachedCriteria addTimeFrom(DetachedCriteria crit, Date timeFrom) {
		crit = crit.add(Restrictions.ge("time", timeFrom));
		return crit;
	}

	private DetachedCriteria addTimeTo(DetachedCriteria crit, Date timeTo) {
		crit = crit.add(Restrictions.le("time", timeTo));
		return crit;
	}

	private DetachedCriteria addFilters(DetachedCriteria crit,
			Map<String, String> filters) {
		for (String property : filters.keySet()) {
			String value = filters.get(property);
			String key = property;
			if (property.equals("level")) {
				key = "trapAlarmLevel";
			} else if (property.equals("appName")) {
				key = "trapAppName";
			}
			if (value != null && !key.equals("sourceType")) {
				StringBuilder sb = new StringBuilder();
				sb.append("%");
				sb.append(value);
				sb.append("%");
				crit.add(Restrictions.like(key, sb.toString()).ignoreCase());
			}
		}
		return crit;
	}

	public List<SnmpTrap> getAllTraps() throws Exception {
		List<SnmpTrap> list = findByCriteriaAndRemoveDuplicate(basicCriteria(),
				null, MAX_RESULT);
		Collections.sort(list);
		return list;
	}

	public List<SnmpTrap> getOldTraps(Date time) throws Exception {
		DetachedCriteria detachedCrit = basicCriteria().add(
				Restrictions.le("time", time));
		List<SnmpTrap> list = findByCriteriaAndRemoveDuplicate(detachedCrit,
				null, MAX_RESULT);
		return list;
	}

	// public List<SnmpTrap> getUnhandledTraps() {
	// DetachedCriteria detachedCrit = basicCriteria();
	// List<SnmpTrap> list =
	// findByCriteriaAndRemoveDuplicate(detachedCrit,null,MAX_RESULT);
	// Collections.sort(list);
	// return list;
	// }

	@Override
	public synchronized void save(SnmpTrap entity) throws DataAccessException {
		super.save(entity);

		LiveMonitorReporter.getInstance().addHotTrap(entity);
		LiveMonitorReporter.getInstance().addGrowlMessage(entity);
		logger.debug(entity + " added to hot alarm");
	}

	public List<SnmpTrap> getTrapsByTimeAndSystem(Date timeFrom, Date timeTo,
			MonitoredSystem selectedSystem, Map<String, String> filters) {

		Disjunction dis = Restrictions.disjunction();
		for (MonitoredHost host : selectedSystem.getHosts()) {
			dis.add(Restrictions.like("source", host.getConfig().getName()));
		}

		DetachedCriteria detachedCrit = basicCriteria().add(dis);

		detachedCrit = addFilters(detachedCrit, filters);

		if (timeFrom != null)
			detachedCrit = addTimeFrom(detachedCrit, timeFrom);
		if (timeTo != null)
			detachedCrit = addTimeTo(detachedCrit, timeTo);
		List<SnmpTrap> list = findByCriteriaAndRemoveDuplicate(detachedCrit,
				null, MAX_RESULT);
		return list;
	}

}
