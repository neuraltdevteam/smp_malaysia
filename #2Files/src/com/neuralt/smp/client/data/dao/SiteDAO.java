package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.config.Site;

public class SiteDAO extends GenericDAO<Site, Integer> {

	protected DetachedCriteria basicCriteria() {
		return DetachedCriteria.forClass(Site.class).addOrder(
				Order.asc("siteName"));
	}

	public SiteDAO(boolean callerManagedTransAllowed) {
		super(Site.class, callerManagedTransAllowed);
	}

	public List<Site> getAllSites() {
		List<Site> list = findByCriteriaAndRemoveDuplicate(basicCriteria(),
				null, null);
		Collections.sort(list);
		return list;
	}

	public Site getSiteByName(String name) {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("siteName", name));
		List<Site> list = findByCriteriaAndRemoveDuplicate(crit, null, null);
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	public Site getSiteById(Long id) {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("id", id));
		List<Site> list = findByCriteria(crit, null, null);
		if (list != null && list.size() != 0) {
			return list.get(0);
		}
		return null;
	}
}
