package com.neuralt.smp.client.data.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;

import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.config.AlarmStatus.StatusCategory;
import com.neuralt.smp.client.service.AlarmService;
import com.neuralt.smp.client.web.AppConfig;

public class AlarmDAO extends GenericDAO<Alarm, Long> {

	public AlarmDAO(boolean callerManagedTransAllowed) {
		super(Alarm.class, callerManagedTransAllowed);
	}

	public Alarm getUnresolvedAlarm(String name, String source, String sourceIp) {
		DetachedCriteria crit = DetachedCriteria.forClass(Alarm.class)
				.add(Property.forName("resolved").isNull())
				.add(Property.forName("name").eq(name))
				.add(Property.forName("source").eq(source).ignoreCase())
				.add(Property.forName("sourceIp").eq(sourceIp));
		// .add(Property.forName("cause").eq(cause));
		crit.addOrder(Order.desc("firstOccurred"));
		List<Alarm> list = this.findByCriteria(crit, null, null);
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}
	}

	// TODO
	// use config
	public Alarm getIgnoredAlarm(String name, String source, String sourceIp) {
		DetachedCriteria crit = DetachedCriteria.forClass(Alarm.class)
				.add(Property.forName("status").eq("ignored"))
				.add(Property.forName("name").eq(name))
				.add(Property.forName("sourceIp").eq(sourceIp))
				.add(Property.forName("source").eq(source));
		// .add(Property.forName("cause").eq(cause));
		crit.addOrder(Order.desc("firstOccurred"));
		List<Alarm> list = this.findByCriteria(crit, null, null);
		if (list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}
	}

	public List<Alarm> getUnresolvedAlarms() {
		DetachedCriteria crit = DetachedCriteria.forClass(Alarm.class).add(
				Property.forName("resolved").isNull());
		crit.addOrder(Order.desc("firstOccurred"));
		List<Alarm> list = this.findByCriteriaAndRemoveDuplicate(crit, null,
				null);
		return list;
	}

	public List<Alarm> getUnhandledAlarms() {
		DetachedCriteria crit = DetachedCriteria.forClass(Alarm.class).add(
				Property.forName("status").eq("unhandled"));
		List<Alarm> list = this.findByCriteriaAndRemoveDuplicate(crit, null,
				null);
		return list;
	}

	public void save(Alarm alarm) throws DataAccessException {
		logger.debug(this.getClass().getSimpleName() + ".save start");
		if (alarm.getId() != null) {
			Session localSession = getCurrentSession();
			Transaction tx = getTransaction(localSession);
			try {
				Alarm freshCopy = (Alarm) localSession.load(Alarm.class,
						alarm.getId());
				freshCopy.setLastOccurred(alarm.getLastOccurred());
				freshCopy.setLastNotified(alarm.getLastNotified());
				// as occurrence maybe modified by other threads
				freshCopy.incrOccurrence();
				freshCopy.setLevel(alarm.getLevel());
				freshCopy.setResolved(alarm.getResolved());
				freshCopy.setResolverId(alarm.getResolverId());
				freshCopy.setStatus(alarm.getStatus());
				freshCopy.setDescription(alarm.getDescription());
				finishTransaction(tx, true);
				logger.debug(this.getClass().getSimpleName() + ".save end");
			} catch (Exception e) {
				logger.error("save", e);
				finishTransaction(tx, false);
				logger.debug(this.getClass().getSimpleName() + ".save end");
				throw new DataAccessException("Exception in save", e);
			}
		} else {
			super.save(alarm);
		}
	}

	public void saveAlarmStatus(Collection<Alarm> batch)
			throws DataAccessException {
		logger.debug(this.getClass().getSimpleName() + ".saveAlarmStatus start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			for (Alarm alarm : batch) {
				Alarm freshCopy = (Alarm) localSession.load(Alarm.class,
						alarm.getId());
				freshCopy.setStatus(alarm.getStatus());
				if (alarm.getOwnerId() != null) {
					freshCopy.setOwnerId(alarm.getOwnerId());
				}
				logger.debug("alarm status={}", alarm.getStatus());
				logger.debug("mapped obj={}",
						AlarmService.instance.getAlarmStatus(alarm.getStatus()));
				if (AlarmService.instance.getAlarmStatus(alarm.getStatus())
						.getCategory() == StatusCategory.FINAL) {
					freshCopy.setResolved(new Date());
					freshCopy.setResolverId(alarm.getResolverId());
				}
			}
			finishTransaction(tx, true);
			logger.debug(this.getClass().getSimpleName()
					+ ".saveAlarmStatus end");
		} catch (Exception e) {
			logger.error("saveAlarmStatus", e);
			finishTransaction(tx, false);
			logger.debug(this.getClass().getSimpleName()
					+ ".saveAlarmStatus end");
			throw new DataAccessException("Exception in saveAlarmStatus", e);
		}
	}

	/*
	 * Intended to reuse the same detached criteria && criteria in LazyDataModel
	 * for file export.
	 */
	public List<Alarm> findByCriteriaWithNoPagination(
			DetachedCriteria detachedCrit) {
		logger.debug(entityClass.getSimpleName()
				+ ".findByCriteriaWithNoPagination start");
		Session localSession = getCurrentSession();
		Transaction tx = getTransaction(localSession);
		try {
			Criteria criteria = detachedCrit
					.getExecutableCriteria(localSession);
			criteria.setFirstResult(0);
			criteria.setMaxResults(AppConfig.getDBMaxResult());
			List<Alarm> results = criteria.list();
			finishTransaction(tx, true);
			logger.debug(entityClass.getSimpleName()
					+ ".findByCriteriaWithNoPagination end");
			return results;
		} catch (Exception e) {
			logger.error("findByCritieria", e);
			finishTransaction(tx, false);
			throw new DataAccessException(
					"Exception in findByCriteriaWithNoPagination", e);
		}
	}
}
