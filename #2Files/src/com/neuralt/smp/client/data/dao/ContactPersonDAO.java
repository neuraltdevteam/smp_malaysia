package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.ContactPerson;

public class ContactPersonDAO extends GenericDAO<ContactPerson, Integer> {

	public ContactPersonDAO(boolean callerManagedTransAllowed) {
		super(ContactPerson.class, callerManagedTransAllowed);
	}

	protected DetachedCriteria basicCriteria() {
		return DetachedCriteria.forClass(ContactPerson.class).addOrder(
				Order.asc("name"));
	}

	public ContactPerson getContactPersonByName(String name) {
		DetachedCriteria crit = basicCriteria().add(
				Restrictions.eq("name", name));
		List<ContactPerson> dummy = findByCriteria(crit, null, null);
		if (dummy.size() != 0) {
			return dummy.get(0);
		}
		return null;
	}

	public List<ContactPerson> getAllContactPersons() {
		List<ContactPerson> list = findByCriteriaAndRemoveDuplicate(
				basicCriteria(), null, null);
		Collections.sort(list);
		return list;
	}
}
