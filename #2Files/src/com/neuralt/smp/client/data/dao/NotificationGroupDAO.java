package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.AlarmNotifier;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.data.NotificationGroup.EventType;

public class NotificationGroupDAO extends GenericDAO<NotificationGroup, Long> {
	public NotificationGroupDAO(boolean callerManagedTransAllowed) {
		super(NotificationGroup.class, callerManagedTransAllowed);
	}

	public List<NotificationGroup> getAllGroups() {
		List<NotificationGroup> list = findByCriteriaAndRemoveDuplicate(
				basicCriteria(), null, null);
		Collections.sort(list);
		return list;
	}

	public List<NotificationGroup> getGroups(EventType type) {
		DetachedCriteria detachedCrit = basicCriteria();
		detachedCrit.add(Restrictions.eq("type", type));
		List<NotificationGroup> list = findByCriteriaAndRemoveDuplicate(
				detachedCrit, null, null);
		Collections.sort(list);
		return list;
	}

	public void save(NotificationGroup group) {
		super.save(group);
		AlarmNotifier.instance.reload();
	}

	public void delete(NotificationGroup group) {
		super.delete(group);
		AlarmNotifier.instance.reload();
	}
}
