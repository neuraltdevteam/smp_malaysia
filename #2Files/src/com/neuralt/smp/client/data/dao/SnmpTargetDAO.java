package com.neuralt.smp.client.data.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.SnmpTarget;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostSnmpConfig;

public class SnmpTargetDAO extends GenericDAO<SnmpTarget, Integer> {

	public SnmpTargetDAO(boolean callerManagedTransAllowed) {
		super(SnmpTarget.class, callerManagedTransAllowed);
	}

	public SnmpTarget getSnmpTargetByName(String name, HostSnmpConfig snmpConfig) {
		DetachedCriteria crit = basicCriteria().add(
				Restrictions.eq("name", name)).add(
				Restrictions.eq("snmpConfig", snmpConfig));
		List<SnmpTarget> dummy = findByCriteria(crit, null, null);
		if (!dummy.isEmpty()) {
			return dummy.get(0);
		}
		return null;
	}

	public List<SnmpTarget> getSnmpTargetByHostSnmpConfig(
			HostSnmpConfig snmpConfig) {
		DetachedCriteria crit = basicCriteria().add(
				Restrictions.eq("snmpConfig", snmpConfig));
		List<SnmpTarget> list = findByCriteriaAndRemoveDuplicate(crit, null,
				null);
		Collections.sort(list);
		return list;
	}

	public void deleteSnmpTargetByHost(HostConfig host) {
		List<SnmpTarget> list = new ArrayList<SnmpTarget>();
		for (HostSnmpConfig snmpConfig : host.getSnmpConfigs()) {
			List<SnmpTarget> dummy = getSnmpTargetByHostSnmpConfig(snmpConfig);
			list.addAll(dummy);
		}
		batchDelete(list);
	}

	public void deleteSnmpTargetByHostSnmpConfig(HostSnmpConfig snmpConfig) {
		List<SnmpTarget> list = getSnmpTargetByHostSnmpConfig(snmpConfig);
		batchDelete(list);
	}
}
