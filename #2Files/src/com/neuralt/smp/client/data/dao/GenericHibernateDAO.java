package com.neuralt.smp.client.data.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GenericHibernateDAO extends AbstractHibernateDAO{

	private static final Logger log = LoggerFactory.getLogger(GenericHibernateDAO.class);
	
	@Override
	protected Logger getLog() {
		return log;
	}

}
