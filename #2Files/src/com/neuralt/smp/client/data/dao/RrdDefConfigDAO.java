package com.neuralt.smp.client.data.dao;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.RrdDefConfig;

public class RrdDefConfigDAO extends GenericDAO<RrdDefConfig, Integer> {
	public RrdDefConfigDAO(boolean callerManagedTransAllowed) {
		super(RrdDefConfig.class, callerManagedTransAllowed);
	}

	public boolean isRrdDefConfigNotExist(String name) {
		boolean ok = true;
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("name", name));
		List<RrdDefConfig> list = findByCriteriaAndRemoveDuplicate(crit, null,
				null);
		ok = list == null || list.isEmpty();
		return ok;
	}

	public List<RrdDefConfig> getAllRrdDefs() {
		List<RrdDefConfig> list = findByCriteriaAndRemoveDuplicate(
				basicCriteria(), null, null);
		Collections.sort(list);
		return list;
	}

	public List<RrdDefConfig> getAllLockedRrdDefs() {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("locked", true));
		List<RrdDefConfig> list = findByCriteriaAndRemoveDuplicate(crit, null,
				null);
		Collections.sort(list);
		return list;
	}

	public RrdDefConfig getFreshCopy(RrdDefConfig def) {
		DetachedCriteria crit = basicCriteria().add(
				Restrictions.eq("id", def.getId()));
		List<RrdDefConfig> dummy = findByCriteria(crit, null, null);
		if (dummy.size() != 0) {
			return dummy.get(0);
		}
		return null;
	}

	public void updateTime(RrdDefConfig def) {
		logger.debug("update time for " + def.getName() + " start");
		if (def.getId() != 0) {
			Session localSession = getCurrentSession();
			Transaction tx = getTransaction(localSession);
			try {
				RrdDefConfig freshCopy = (RrdDefConfig) localSession.load(
						RrdDefConfig.class, def.getId());
				freshCopy.setLastUpdate(new Timestamp(System
						.currentTimeMillis()));
				finishTransaction(tx, true);
				logger.debug("update time for " + def.getName() + " end");
			} catch (Exception e) {
				logger.error("save", e);
				finishTransaction(tx, false);
				logger.debug(this.getClass().getSimpleName() + ".save end");
				throw new DataAccessException("Exception in save", e);
			}
		}
	}
}
