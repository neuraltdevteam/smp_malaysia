package com.neuralt.smp.client.data.dao;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.ChartTemplate;
import com.neuralt.web.util.jsf.WebUtil;

public class ChartTemplateDAO extends GenericDAO<ChartTemplate, Long> {

	public ChartTemplateDAO(boolean callerManagedTransAllowed) {
		super(ChartTemplate.class, callerManagedTransAllowed);
	}

	protected DetachedCriteria basicCriteria() {
		DetachedCriteria crit = DetachedCriteria.forClass(ChartTemplate.class);
		crit.add(Restrictions.eq("user", WebUtil.getCurrentUser()));
		return crit;
	}

	public ChartTemplate getTemplateByName(String name) {
		DetachedCriteria crit = basicCriteria().add(
				Restrictions.eq("name", name));
		List<ChartTemplate> dummy = findByCriteria(crit, null, null);
		if (dummy.size() != 0) {
			return dummy.get(0);
		}
		return null;
	}

	public ChartTemplate getTemplateById(long id) {
		DetachedCriteria crit = basicCriteria().add(Restrictions.eq("id", id));
		List<ChartTemplate> dummy = findByCriteria(crit, null, null);
		if (dummy.size() != 0) {
			return dummy.get(0);
		}
		return null;
	}

	public List<ChartTemplate> getAvailableChartTemplates() {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("enabled", true));
		List<ChartTemplate> dummy = findByCriteriaAndRemoveDuplicate(crit,
				null, null);
		List<ChartTemplate> list = new ArrayList<ChartTemplate>();
		for (ChartTemplate template : dummy) {
			if (template.isConfigValid()) {
				logger.debug("adding available template:" + template);
				list.add(template);
			}
		}
		Collections.sort(list);
		return list;
	}

	public List<ChartTemplate> getAllChartTemplatesForCurrentUser() {
		DetachedCriteria crit = basicCriteria();
		List<ChartTemplate> list = findByCriteriaAndRemoveDuplicate(crit, null,
				null);
		Collections.sort(list);
		return list;
	}

	public List<ChartTemplate> getAllChartTemplates() {
		DetachedCriteria crit = DetachedCriteria.forClass(ChartTemplate.class);
		List<ChartTemplate> list = findByCriteriaAndRemoveDuplicate(crit, null,
				null);
		Collections.sort(list);
		return list;
	}
}
