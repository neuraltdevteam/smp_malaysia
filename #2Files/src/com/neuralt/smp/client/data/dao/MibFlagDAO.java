package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.MibFlag;

public class MibFlagDAO extends GenericDAO<MibFlag, Integer> {

	public MibFlagDAO(boolean callerManagedTransAllowed) {
		super(MibFlag.class, callerManagedTransAllowed);
	}

	protected DetachedCriteria basicCriteria() {
		return DetachedCriteria.forClass(MibFlag.class).addOrder(
				Order.asc("name"));
	}

	public List<MibFlag> getAllMibFlags() {
		List<MibFlag> list = findByCriteriaAndRemoveDuplicate(basicCriteria(),
				null, null);
		Collections.sort(list);
		return list;
	}

	public List<MibFlag> getLoadedMibFlags() {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("loaded", true));
		List<MibFlag> list = findByCriteriaAndRemoveDuplicate(crit, null, null);
		Collections.sort(list);
		return list;
	}

	public MibFlag getMibFlagByName(String name) {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("name", name));
		List<MibFlag> list = findByCriteria(crit, null, null);
		if (list != null && !list.isEmpty())
			return list.get(0);
		return null;
	}
}
