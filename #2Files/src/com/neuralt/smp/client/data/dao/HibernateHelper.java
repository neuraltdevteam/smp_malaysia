package com.neuralt.smp.client.data.dao;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateHelper {
	
	private static final Logger log = LoggerFactory.getLogger(HibernateHelper.class);
	
	public Logger getLog(){
		return log;
	}

	@SuppressWarnings("unchecked")
	public <T> T findModelById(Session session, Class<T> modelClass, Serializable id){
		getLog().debug("finding Model<"+modelClass.getSimpleName()+"> by id ["+id+"]");
		return (T)session.get(modelClass, id);
	}

	@SuppressWarnings("unchecked")
	public <T> List<T> findModelByProperty(Session session, Class<T> modelClass, String propertyName, Object value, boolean ignoreCase) {
		try {
			getLog().debug("finding Model<" + modelClass.getSimpleName()+ "> with Property<" + propertyName + "> : " + value+ (ignoreCase ? " [ignoreCase]" : ""));
			
			String query = "from " + modelClass.getSimpleName() + " as model";
			if (ignoreCase) {
				query += " where upper(model." + propertyName + ") = upper(:value)";
			} else {
				query += " where model." + propertyName + " = :value";
			}
			return session.createQuery(query)
					.setParameter("value", value)
					.list();
			
		} catch (RuntimeException e) {
			getLog().error("failed finding Model<" + modelClass.getSimpleName()+ "> with Property<" + propertyName + ">", e);
			throw e;
		}
	}
	
	public <T> List<T> findModelByProperty(Session session, Class<T> modelClass, String propertyName, Object value){
		return findModelByProperty(session, modelClass, propertyName, value, false);
	}
	
	public <T> T findModelUniqueByProperty(Session session, Class<T> modelClass, String propertyName, Object value, boolean ignoreCase){
		List<T> list = findModelByProperty(session, modelClass, propertyName, value, ignoreCase);
		if(list.size() < 1){
			return null;
		}else{
			return list.get(0);
		}
	}
	
	public <T> T findModelUniqueByProperty(Session session, Class<T> modelClass, String propertyName, Object value){
		return findModelUniqueByProperty(session, modelClass, propertyName, value, false);
	}
	
	public <T> List<T> find(Session session, Class<T> resultClass, final String hql, final Map<String,Object> param, final Integer take, final Integer skip){
		try{
			Query query = session.createQuery(hql);
			if (param != null) {
				for (Map.Entry<String, Object> p:param.entrySet()) {
					if (p.getValue() instanceof Collection) {
						query.setParameterList(p.getKey(), (Collection<?>)p.getValue());
					} else {
						query.setParameter(p.getKey(),p.getValue());
					}
				}
			}
			if(skip != null && skip > 0){
				query.setFirstResult(skip);
			}
			if(take != null && take > 0){
				query.setMaxResults(take);
				query.setFetchSize(take);
			}
			@SuppressWarnings("unchecked")
			List<T> list = query.list();
			
			getLog().debug("fetched ["+list.size()+"] in query ["+hql+"]"+(param==null?"":" with param "+param.toString()));
			return list;
			
		}catch(RuntimeException e){
			getLog().error("failed query ["+hql+"]"+(param==null?"":" with param "+param.toString()), e);
			throw e;
		}
	}
	
	public <T> List<T> find(Session session, Class<T> resultClass, final String hql, final Map<String,Object> param){
		return find(session, resultClass, hql, param, null, null);
	}
	
	public <T> List<T> find(Session session, Class<T> resultClass, final String hql){
		return find(session, resultClass, hql, null, null, null);
	}
	
	public <T> T findUnique(Session session, Class<T> resultClass, final String hql, final Map<String,Object> param){
		List<T> list = find(session, resultClass, hql, param, null, null);
		if(list.size() < 1){
			return null;
		}else{
			return list.get(0);
		}
	}
	
	public <T> T findUnique(Session session, Class<T> resultClass, final String hql){
		return findUnique(session, resultClass, hql, null);
	}
	
	public List<Map<String,Object>> findBySQL(Session session, final String sql, final Map<String, Type> map, final Map<String,Object> param, final Integer take, final Integer skip){
		try{
			SQLQuery query = session.createSQLQuery(sql);
			
			//bind return types
			String[] keys = new String[map.size()];
			int index=0;
			for(Map.Entry<String, Type> m : map.entrySet()){
				query.addScalar(m.getKey(), m.getValue());
				keys[index++]=m.getKey();
			}
			
			//bind input params
			if (param != null) {
				for (Map.Entry<String, Object> p:param.entrySet()) {
					if (p.getValue() instanceof Collection) {
						query.setParameterList(p.getKey(), (Collection<?>)p.getValue());
					} else {
						query.setParameter(p.getKey(),p.getValue());
					}
				}
			}
			if(skip != null && skip > 0){
				query.setFirstResult(skip);
			}
			if(take != null && take > 0){
				query.setMaxResults(take);
				query.setFetchSize(take);
			}
			
			@SuppressWarnings("unchecked")
			List<Object[]> raw = (List<Object[]>)query.list();
			List<Map<String, Object>> ret = new ArrayList<Map<String, Object>>();
			
			for(Object[] data:raw){
				Map<String, Object> responseStruct = new HashMap<String, Object>();
				for(int i=0; i<keys.length; i++){
					responseStruct.put(keys[i], data[i]);
				}
				ret.add(responseStruct);
			}
			getLog().debug("fetched ["+ret.size()+"] in query ["+sql+"]"+(param==null?"":" with param "+param.toString()));
			return ret;
			
		}catch(RuntimeException e){
			getLog().error("failed query ["+sql+"]"+(param==null?"":" with param "+param.toString()), e);
			throw e;
		}
	}
	
	public int executeUpdate(Session session, final String hql, final Map<String,Object> param){
		try{
			Query query = session.createQuery(hql);
			if (param != null) {
				for (Map.Entry<String, Object> p:param.entrySet()) {
					if (p.getValue() instanceof Collection) {
						query.setParameterList(p.getKey(), (Collection<?>)p.getValue());
					} else {
						query.setParameter(p.getKey(),p.getValue());
					}
				}
			}
			int result = query.executeUpdate();
			
			getLog().debug("["+result+"] executed ["+hql+"]"+(param==null?"":" with param "+param.toString()));
			return result;
			
		}catch(RuntimeException e){
			getLog().error("failed execute ["+hql+"]"+(param==null?"":" with param "+param.toString()), e);
			throw e;
		}
	}
	
	public int executeUpdateSQL(Session session, final String sql, final Map<String,Object> param){
		try{
			SQLQuery query = session.createSQLQuery(sql);
			if (param != null) {
				for (Map.Entry<String, Object> p:param.entrySet()) {
					if (p.getValue() instanceof Collection) {
						query.setParameterList(p.getKey(), (Collection<?>)p.getValue());
					} else {
						query.setParameter(p.getKey(),p.getValue());
					}
				}
			}
			int result = query.executeUpdate();
			
			getLog().debug("["+result+"] executed ["+sql+"]"+(param==null?"":" with param "+param.toString()));
			return result;
			
		}catch(RuntimeException e){
			getLog().error("failed execute ["+sql+"]"+(param==null?"":" with param "+param.toString()), e);
			throw e;
		}
	}
	
	public <T> T doInTransaction(Session session, TransactionCallback<T> callback){
		Transaction tx = null;
		try{
			tx = session.beginTransaction();
			T result = callback.execute();
			tx.commit();
			return result;
			
		}catch(RuntimeException e){
			if(tx != null){
				try{
					tx.rollback();
				}catch(Exception ex){
					log.error("failed rollback transaction", ex);
				}
			}
			throw e;
		}
	}
	
	public static interface TransactionCallback<T>{
		public T execute();
	}
}
