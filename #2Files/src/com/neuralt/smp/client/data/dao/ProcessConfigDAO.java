package com.neuralt.smp.client.data.dao;

import com.neuralt.smp.config.ProcessConfig;

public class ProcessConfigDAO extends GenericDAO<ProcessConfig, Long> {
	public ProcessConfigDAO(boolean callerManagedTransAllowed) {
		super(ProcessConfig.class, callerManagedTransAllowed);
	}

}
