package com.neuralt.smp.client.data.dao;

import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.data.ProductGroup;

public class ProductGroupDAO extends GenericDAO<ProductGroup, Long> {

	public ProductGroupDAO(boolean callerManagedTransAllowed) {
		super(ProductGroup.class, callerManagedTransAllowed);
	}

	public ProductGroup getProductGroupByOid(OID oid) {
		DetachedCriteria detachedCrit = basicCriteria().add(
				Restrictions.eq("oid", oid));
		List<ProductGroup> list = findByCriteria(detachedCrit, null, null);
		return list.isEmpty() ? null : list.get(0);
	}

	public ProductGroup getProductGroupByName(String name) {
		DetachedCriteria detachedCrit = basicCriteria().add(
				Restrictions.eq("name", name));
		List<ProductGroup> list = findByCriteria(detachedCrit, null, null);
		return list.isEmpty() ? null : list.get(0);
	}

	public List<ProductGroup> getAllProductGroups() {
		List<ProductGroup> list = findByCriteriaAndRemoveDuplicate(
				basicCriteria(), null, null);
		Collections.sort(list);
		return list;
	}
}
