/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuralt.smp.client.data.dao;

/**
 *
 * @author ag111
 */
import java.util.Collections;
import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.data.CRSMS_CHANNEL_PROFILE;

//public class ManageCPDAO extends GenericDAO<ManageCP, Boolean> {

public class ManageCPDAO extends GenericDAO<CRSMS_CHANNEL_PROFILE, Long> {
    
        public ManageCPDAO(boolean callerManagedTransAllowed) {
		super(CRSMS_CHANNEL_PROFILE.class, callerManagedTransAllowed);
	}
        
        protected DetachedCriteria basicCriteria() {
		return DetachedCriteria.forClass(CRSMS_CHANNEL_PROFILE.class).addOrder(
				Order.asc("channel_id"));
	}
    
        //public SiteDAO(boolean callerManagedTransAllowed) {
	//	super(ManageCP.class, callerManagedTransAllowed);
	//}

	public List<CRSMS_CHANNEL_PROFILE> getAllCPs() {
		
		System.out.println("ManageCPDAO.getAllCPs()");
		List<CRSMS_CHANNEL_PROFILE> list = null;
		try
		{
			list = findByCriteriaAndRemoveDuplicate(basicCriteria(),
					null, null);
			Collections.sort(list);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return list;
	}

	public CRSMS_CHANNEL_PROFILE getCPByName(String name) {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("channel", name));
		List<CRSMS_CHANNEL_PROFILE> list = findByCriteriaAndRemoveDuplicate(crit, null, null);
		if (!list.isEmpty()) {
			return list.get(0);
		}
		return null;
	}

	public CRSMS_CHANNEL_PROFILE getCPeById(Long id) {
		DetachedCriteria crit = basicCriteria();
		crit.add(Restrictions.eq("channel_id", id));
		List<CRSMS_CHANNEL_PROFILE> list = findByCriteria(crit, null, null);
		if (list != null && list.size() != 0) {
			return list.get(0);
		}
		return null;
	}
}
