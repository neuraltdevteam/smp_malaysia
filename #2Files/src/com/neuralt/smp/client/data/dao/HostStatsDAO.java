package com.neuralt.smp.client.data.dao;

import java.sql.Timestamp;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;

import com.neuralt.smp.client.data.stats.HostStats;
import com.neuralt.smp.client.data.stats.ProcessStats;
import com.neuralt.smp.util.StringUtil;

public class HostStatsDAO extends GenericDAO<HostStats, Integer> {

	public HostStatsDAO(boolean callerManagedTransAllowed) {
		super(HostStats.class, callerManagedTransAllowed);
	}

	public List<HostStats> getHostStats(String sysName, String hostName,
			Timestamp from, Timestamp to, Integer firstResult,
			Integer maxResults) {
		DetachedCriteria crit = DetachedCriteria.forClass(HostStats.class);
		crit.addOrder(Order.asc("ts"));
		logger.debug("sysName=" + sysName + ", hostName=" + hostName
				+ ", from=" + from + ", to=" + to);
		if (!StringUtil.isNullOrEmpty(sysName))
			crit.add(Property.forName("sysName").eq(sysName));
		if (!StringUtil.isNullOrEmpty(hostName))
			crit.add(Property.forName("hostName").eq(hostName));
		if (from != null)
			crit.add(Property.forName("ts").between(from, to));
		// crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return super.findByCriteria(crit, firstResult, maxResults);
	}

	// might as well let ProcessStats live here ...
	public List<ProcessStats> getProcessStats(String sysName, String hostName,
			String procName, Timestamp from, Timestamp to, Integer firstResult,
			Integer maxResults) {
		DetachedCriteria crit = DetachedCriteria.forClass(ProcessStats.class);
		if (!StringUtil.isNullOrEmpty(sysName))
			crit.add(Property.forName("key.sysName").eq(sysName));
		if (!StringUtil.isNullOrEmpty(hostName))
			crit.add(Property.forName("key.hostName").eq(hostName));
		if (!StringUtil.isNullOrEmpty(procName))
			crit.add(Property.forName("key.processName").eq(procName));
		if (from != null)
			crit.add(Property.forName("ts").ge(from));
		if (to != null)
			crit.add(Property.forName("ts").le(to));
		crit.setFetchMode("memStats", FetchMode.JOIN);
		crit.setFetchMode("diskStats", FetchMode.JOIN);
		crit.setFetchMode("cpuStats", FetchMode.JOIN);
		crit.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		return super.rawDao.findByCriteria(ProcessStats.class, crit,
				firstResult, maxResults);
	}

}
