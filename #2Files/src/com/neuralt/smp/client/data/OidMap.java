package com.neuralt.smp.client.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.util.OidUtil;
import com.neuralt.smp.util.StringUtil;

/**
 * The entity of storing OID mapping info.
 */
@Entity
@Table(name = "OID_MAP")
public class OidMap implements Serializable, Comparable<OidMap> {
	private static final long serialVersionUID = -7966548141382329226L;
	private static final String UNCHANGED = "(UNCHANGED)";
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@ManyToOne
	@ForeignKey(name = "FK_OID_MAP_PRODUCT")
	@JoinColumn(name = "PARENT_ID")
	private ProductGroup parent;
	private OID oid;
	private OID mappedOid;
	private String name;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parent", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<ObjectName> objectNames;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parent", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<ObjectNameMap> objectNameMaps;

	public OidMap() {
		oid = new OID();
		mappedOid = new OID();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Set<ObjectNameMap> getObjectNameMaps() {
		return objectNameMaps;
	}

	public void setObjectNameMaps(Set<ObjectNameMap> objectNameMaps) {
		this.objectNameMaps = new TreeSet<ObjectNameMap>(objectNameMaps);
	}

	public OID getOid() {
		return oid;
	}

	public void setOid(OID oid) {
		this.oid = oid;
	}

	public OID getMappedOid() {
		return mappedOid;
	}

	public void setMappedOid(OID mappedOid) {
		this.mappedOid = mappedOid;
	}

	public ProductGroup getParent() {
		return parent;
	}

	public void setParent(ProductGroup parent) {
		this.parent = parent;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Transient
	public List<ObjectNameMap> getObjectNameMapsAsList() {
		if (objectNameMaps != null) {
			List<ObjectNameMap> dummy = new ArrayList<ObjectNameMap>(
					objectNameMaps);
			Collections.sort(dummy);
			return dummy;
		}
		return null;
	}

	public Set<ObjectName> getObjectNames() {
		return objectNames;
	}

	public void setObjectNames(Set<ObjectName> objectNames) {
		this.objectNames = new TreeSet<ObjectName>(objectNames);
	}

	@Transient
	public List<ObjectName> getObjectNamesAsList() {
		if (objectNames != null) {
			List<ObjectName> dummy = new ArrayList<ObjectName>(objectNames);
			Collections.sort(dummy);
			return dummy;
		}
		return null;
	}

	@Transient
	public String getOidAsString() {
		return StringUtil.getOidAsString(oid);
	}

	@Transient
	public String getMappedOidAsString() {
		return StringUtil.getOidAsString(mappedOid);
	}

	@Transient
	public String getOidAsTrapName() {
		if (!StringUtil.isNullOrEmpty(name))
			return name;
		return OidUtil.getInstance().mapOidToTrapName(oid);
	}

	@Transient
	public String getMappedOidAsTrapName() {
		if (StringUtil.isNullOrEmpty(mappedOid) || mappedOid.equals(oid)) {
			return UNCHANGED;
		}
		OidMap temp = OidUtil.getInstance().getFullOidMap().get(mappedOid);
		if (temp != null) {
			return temp.getOidAsTrapName();
		}
		return OidUtil.getInstance().mapOidToTrapName(mappedOid);
	}

	@Transient
	public String getCurrentRecognizableName() {
		if (StringUtil.isNullOrEmpty(mappedOid) || mappedOid.equals(oid)) {
			return getOidAsTrapName();
		} else {
			OidMap oidMap = OidUtil.getInstance().getFullOidMap()
					.get(mappedOid);
			if (oidMap != null) {
				return oidMap.getOidAsTrapName();
			} else {
				return OidUtil.getInstance().mapOidToTrapName(mappedOid);
			}
		}
	}

	@Transient
	public boolean isNoMap() {
		if (StringUtil.isNullOrEmpty(mappedOid) || mappedOid.equals(oid)) {
			return true;
		}
		return false;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof OidMap) {
			OidMap dummy = (OidMap) o;
			return dummy.getId() == id;
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("OidMap [oid=").append(oid.toString()).append(", name=")
				.append(getOidAsTrapName()).append("]");
		return sb.toString();
	}

	@Entity
	@Table(name = "OBJECT_NAME_MAP")
	public static class ObjectNameMap implements Serializable, Comparable {
		private static final long serialVersionUID = -6093009541076641166L;
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private long id;
		private OID oid;
		private String value;

		private String mappedValue;
		@ManyToOne
		@ForeignKey(name = "FK_OBJECT_NAME_MAP_OID")
		@JoinColumn(name = "PARENT_ID")
		private OidMap parent;

		public ObjectNameMap() {
			oid = new OID();
		}

		public String getValue() {
			if (value != null)
				return value;
			return "";
		}

		public void setValue(String value) {
			this.value = value;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public OID getOid() {
			return oid;
		}

		public void setOid(OID oid) {
			this.oid = oid;
		}

		public String getMappedValue() {
			if (mappedValue != null)
				return mappedValue;
			return "";
		}

		public void setMappedValue(String mappedValue) {
			this.mappedValue = mappedValue;
		}

		public OidMap getParent() {
			return parent;
		}

		public void setParent(OidMap parent) {
			this.parent = parent;
		}

		@Override
		public boolean equals(Object o) {
			if (o instanceof ObjectNameMap) {
				ObjectNameMap dummy = (ObjectNameMap) o;
				return dummy.getId() == id;
			}
			return false;
		}

		@Transient
		public String getOidAsTrapName() {
			return OidUtil.getInstance().mapOidToTrapName(oid);
		}

		@Transient
		public String getOidAsString() {
			return StringUtil.getOidAsString(oid);
		}

		@Transient
		public ObjectNameMap getBackup() {
			ObjectNameMap dummy = new ObjectNameMap();
			dummy.setMappedValue(mappedValue);
			dummy.setOid(oid);
			dummy.setValue(value);
			return dummy;
		}

		public void updateConfig(ObjectNameMap dummy) {
			setOid(dummy.getOid());
			setValue(dummy.getValue());
			setMappedValue(dummy.getMappedValue());
		}

		@Override
		public int compareTo(Object o) {
			if (o instanceof ObjectNameMap) {
				ObjectNameMap dummy = (ObjectNameMap) o;
				return oid.equals(dummy.getOid()) ? value.compareTo(dummy
						.getValue()) : oid.compareTo(dummy.getOid());
			}
			return 0;
		}
	}

	@Entity
	@Table(name = "OBJECT_NAME")
	public static class ObjectName implements Serializable, Comparable {
		private static final long serialVersionUID = -1193893960510913874L;
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private long id;
		private OID oid;
		private OID mappedOid;
		@ManyToOne
		@ForeignKey(name = "FK_OBJECT_NAME_OID")
		@JoinColumn(name = "PARENT_ID")
		private OidMap parent;

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public OID getOid() {
			return oid;
		}

		public void setOid(OID oid) {
			this.oid = oid;
		}

		public OidMap getParent() {
			return parent;
		}

		public void setParent(OidMap parent) {
			this.parent = parent;
		}

		@Override
		public boolean equals(Object o) {
			if (o instanceof ObjectName) {
				ObjectName dummy = (ObjectName) o;
				return dummy.getId() == id;
			}
			return false;
		}

		@Transient
		public String getOidAsTrapName() {
			return OidUtil.getInstance().mapOidToTrapName(oid);
		}

		@Transient
		public String getOidAsString() {
			return StringUtil.getOidAsString(oid);
		}

		public OID getMappedOid() {
			return mappedOid;
		}

		public void setMappedOid(OID mappedOid) {
			this.mappedOid = mappedOid;
		}

		@Transient
		public String getMappedOidAsString() {
			return StringUtil.getOidAsString(mappedOid);
		}

		@Transient
		public String getMappedOidAsTrapName() {
			if (StringUtil.isNullOrEmpty(mappedOid) || mappedOid.equals(oid)) {
				return UNCHANGED;
			}
			return OidUtil.getInstance().mapOidToTrapName(mappedOid);
		}

		public void updateConfig(ObjectName name) {
			setOid(name.getOid());
			setMappedOid(name.getMappedOid());
		}

		public ObjectName getBackup() {
			ObjectName dummy = new ObjectName();
			dummy.setMappedOid(mappedOid);
			dummy.setOid(oid);
			return dummy;
		}

		@Override
		public int compareTo(Object o) {
			if (o instanceof ObjectName) {
				ObjectName dummy = (ObjectName) o;
				return oid.compareTo(dummy.getOid());
			}
			return 0;
		}
	}

	@Override
	public int compareTo(OidMap o) {
		if (o != null && oid != null && o.getOid() != null)
			return oid.compareTo(o.getOid());
		return 0;
	}

	public OidMap getBackup() {
		OidMap dummy = new OidMap();
		dummy.setMappedOid(mappedOid);
		dummy.setOid(oid);
		dummy.setParent(parent);
		dummy.setName(name);
		Set<ObjectName> objNameSet = new TreeSet<ObjectName>();
		for (ObjectName objName : objectNames) {
			ObjectName dummyObjName = objName.getBackup();
			dummyObjName.setParent(dummy);
			objNameSet.add(dummyObjName);
		}
		dummy.setObjectNames(objNameSet);
		Set<ObjectNameMap> objNameMapSet = new TreeSet<ObjectNameMap>();
		for (ObjectNameMap objNameMap : objectNameMaps) {
			ObjectNameMap dummyObjNameMap = objNameMap.getBackup();
			dummyObjNameMap.setParent(dummy);
			objNameMapSet.add(dummyObjNameMap);
		}
		dummy.setObjectNameMaps(objNameMapSet);
		return dummy;
	}

	public void updateConfig(OidMap dummy) {
		setOid(dummy.getOid());
		setMappedOid(dummy.getMappedOid());
		setName(dummy.name);
		setParent(dummy.getParent());
		setObjectNames(dummy.getObjectNames());
		for (ObjectName objName : objectNames) {
			objName.setParent(this);
		}
		setObjectNameMaps(dummy.getObjectNameMaps());
		for (ObjectNameMap objNameMap : objectNameMaps) {
			objNameMap.setParent(this);
		}
	}
}
