package com.neuralt.smp.client.data;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.neuralt.smp.config.model.BaseSnmpConfig;
import com.neuralt.smp.config.model.Editable;

/**
 * The entity storing SNMP config for NotificationGroup to use.
 *
 */
@Entity
@Table(name = "GROUP_SNMP_CONFIG")
public class GroupSnmpConfig extends BaseSnmpConfig implements Serializable,
		Comparable<GroupSnmpConfig>, Editable<GroupSnmpConfig> {
	private static final long serialVersionUID = 8398928504919525757L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String ip;
	private String name;
	private boolean original;

	public GroupSnmpConfig() {
		version = SnmpVersion.SNMPV1;
	}

	public GroupSnmpConfig getBackup() {
		GroupSnmpConfig dummy = new GroupSnmpConfig();
		dummy.setAuth(auth);
		dummy.setAuthPassword(authPassword);
		dummy.setCommunityGet(communityGet);
		dummy.setCommunitySet(communitySet);
		dummy.setCommunityTrap(communityTrap);
		dummy.setEngineId(engineId);
		dummy.setPriv(priv);
		dummy.setPrivPassword(privPassword);
		dummy.setUsername(username);
		dummy.setPort(port);
		dummy.setIp(ip);
		dummy.setName(name);
		dummy.setVersion(version);
		dummy.setOriginal(original);
		return dummy;
	}

	public void update(GroupSnmpConfig dummy) {
		setAuth(dummy.getAuth());
		setAuthPassword(dummy.getAuthPassword());
		setCommunityGet(dummy.getCommunityGet());
		setCommunitySet(dummy.getCommunitySet());
		setCommunityTrap(dummy.getCommunityTrap());
		setEngineId(dummy.getEngineId());
		setPriv(dummy.getPriv());
		setPrivPassword(dummy.getPrivPassword());
		setUsername(dummy.getUsername());
		setPort(dummy.getPort());
		setIp(dummy.getIp());
		setName(dummy.getName());
		setVersion(dummy.getVersion());
		setOriginal(dummy.isOriginal());
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isOriginal() {
		return original;
	}

	public void setOriginal(boolean original) {
		this.original = original;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof GroupSnmpConfig) {
			GroupSnmpConfig dummy = (GroupSnmpConfig) o;
			return id == dummy.getId();
		}
		return false;
	}

	@Override
	public int compareTo(GroupSnmpConfig o) {
		if (o != null) {
			if (name != null && o.getName() != null)
				return name.compareTo(o.getName());
		}
		return 0;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("GroupSnmpConfig: name=").append(name).append(", ip=")
				.append(ip).append(", port=").append(port).append(", version=")
				.append(version.name()).append(", original=").append(original);
		if (version == SnmpVersion.SNMPV3) {
			sb.append(", username=").append(username).append(", engineId=")
					.append(engineId).append(", auth=").append(auth.name())
					.append(", priv=").append(priv.name());
		} else if (version == SnmpVersion.SNMPV1
				|| version == SnmpVersion.SNMPV2) {

		}
		return sb.toString();
	}
}
