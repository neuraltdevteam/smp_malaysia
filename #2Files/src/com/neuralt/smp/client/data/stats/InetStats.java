package com.neuralt.smp.client.data.stats;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.neuralt.smp.agent.mbean.model.NetworkInterface;

@Entity
@Table(name = "HOST_STATS_INET")
public class InetStats {

	@EmbeddedId
	private InetStatsKey key;
	private String statusText;
	private Integer status;
	private Float inOctet;
	private Float outOctet;

	public InetStats() {
	}

	public InetStats(HostStats owner, String desc) {
		key = new InetStatsKey(owner, desc);
		status = 0;
		statusText = "";
		inOctet = (float) 0;
		outOctet = (float) 0;
	}

	public HostStats getOwner() {
		return key.owner;
	}

	public void setOwner(HostStats owner) {
		this.key.owner = owner;
	}

	public String getDesc() {
		return key.descr;
	}

	public void setDesc(String desc) {
		this.key.descr = desc;
	}

	public InetStatsKey getKey() {
		return key;
	}

	public void setKey(InetStatsKey key) {
		this.key = key;
	}

	public String getStatusText() {
		return statusText;
	}

	public void setStatusText(String statusText) {
		this.statusText = statusText;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Float getInOctet() {
		return inOctet;
	}

	public void setInOctet(Float inOctet) {
		this.inOctet = inOctet;
	}

	public Float getOutOctet() {
		return outOctet;
	}

	public void setOutOctet(Float outOctet) {
		this.outOctet = outOctet;
	}

	public void add(NetworkInterface data) {
		inOctet += data.getInOctet();
		outOctet += data.getOutOctet();
	}

	public void divideBy(int noteCount) {
		if (noteCount > 0) {
			inOctet /= noteCount;
			outOctet /= noteCount;
		}
	}

	@Embeddable
	public static class InetStatsKey implements Serializable {

		private static final long serialVersionUID = 2369908184064917098L;
		@ManyToOne
		@ForeignKey(name = "FK_INET_HOST_STATS")
		private HostStats owner;
		private String descr;

		public InetStatsKey() {
		}

		public InetStatsKey(HostStats owner, String descr) {
			this.owner = owner;
			this.descr = descr;
		}

		public HostStats getOwner() {
			return owner;
		}

		public void setOwner(HostStats owner) {
			this.owner = owner;
		}

		public String getDescr() {
			return descr;
		}

		public void setDescr(String descr) {
			this.descr = descr;
		}
	}

}
