package com.neuralt.smp.client.data.stats;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.neuralt.smp.agent.mbean.model.CPU;

@Entity
@Table(name = "HOST_STATS_CPU")
public class CpuStats {

	@EmbeddedId
	private CpuStatsKey key;
	private Double usr;
	private Double sys;
	private Double iowait;
	private Double idle;

	public CpuStats() {
	}

	public CpuStats(HostStats owner, String processor) {
		key = new CpuStatsKey(owner, processor);
		usr = 0.0;
		sys = 0.0;
		iowait = 0.0;
		idle = 0.0;
	}

	public HostStats getOwner() {
		return key.owner;
	}

	public void setOwner(HostStats owner) {
		this.key.owner = owner;
	}

	public String getProcessor() {
		return key.processor;
	}

	public void setProcessor(String processor) {
		this.key.processor = processor;
	}

	public CpuStatsKey getKey() {
		return key;
	}

	public void setKey(CpuStatsKey key) {
		this.key = key;
	}

	public Double getUsr() {
		return usr;
	}

	public void setUsr(Double usr) {
		this.usr = usr;
	}

	public Double getSys() {
		return sys;
	}

	public void setSys(Double sys) {
		this.sys = sys;
	}

	public Double getIowait() {
		return iowait;
	}

	public void setIowait(Double iowait) {
		this.iowait = iowait;
	}

	public Double getIdle() {
		return idle;
	}

	public void setIdle(Double idle) {
		this.idle = idle;
	}

	// convenience methods for calculation
	public void add(CPU data) {
		usr += data.getUsr();
		sys += data.getSys();
		idle += data.getIdle();
		iowait += data.getIowait();
	}

	public void divideBy(int divider) {
		if (divider > 0) {
			usr /= divider;
			sys /= divider;
			idle /= divider;
			iowait /= divider;
		}
	}

	@Embeddable
	public static class CpuStatsKey implements Serializable {
		private static final long serialVersionUID = 3774453197645909914L;

		@ManyToOne
		@ForeignKey(name = "FK_STATS_CPU_HOST")
		private HostStats owner;
		private String processor;

		public CpuStatsKey() {
		}

		public CpuStatsKey(HostStats owner, String processor) {
			this.owner = owner;
			this.processor = processor;
		}

		public HostStats getOwner() {
			return owner;
		}

		public void setOwner(HostStats owner) {
			this.owner = owner;
		}

		public String getProcessor() {
			return processor;
		}

		public void setProcessor(String processor) {
			this.processor = processor;
		}
	}
}
