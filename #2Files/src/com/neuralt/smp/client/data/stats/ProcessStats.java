package com.neuralt.smp.client.data.stats;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.neuralt.smp.agent.mbean.model.ProcessHealth;

@Entity
@Table(name = "PROCESS_STATS")
public class ProcessStats {

	@EmbeddedId
	private ProcessStatsKey key;
	private Float cpu;
	private Float mem;
	@Transient
	private boolean empty = true;

	public ProcessStats() {
	}

	public ProcessStats(String sysName, String hostName, String processName,
			Timestamp ts) {
		key = new ProcessStatsKey(sysName, hostName, processName, ts);
		cpu = 0.0f;
		mem = 0.0f;
	}

	public String getSysName() {
		return key.sysName;
	}

	public void setSysName(String sysName) {
		this.key.sysName = sysName;
	}

	public String getHostName() {
		return key.hostName;
	}

	public void setHostName(String hostName) {
		this.key.hostName = hostName;
	}

	public String getProcessName() {
		return key.processName;
	}

	public void setProcessName(String processName) {
		this.key.processName = processName;
	}

	public Timestamp getTs() {
		return key.ts;
	}

	public void setTs(Timestamp ts) {
		this.key.ts = ts;
	}

	public ProcessStatsKey getKey() {
		return key;
	}

	public void setKey(ProcessStatsKey key) {
		this.key = key;
	}

	public Float getCpu() {
		return cpu;
	}

	public void setCpu(Float cpu) {
		this.cpu = cpu;
	}

	public Float getMem() {
		return mem;
	}

	public void setMem(Float mem) {
		this.mem = mem;
	}

	public boolean isEmpty() {
		return empty;
	}

	public void setEmpty(boolean empty) {
		this.empty = empty;
	}

	// convenience methods
	public void add(ProcessHealth data) {
		empty = false;
		cpu += data.getCpu();
		mem += data.getMem();
	}

	public void divideBy(int divider) {
		cpu /= divider;
		mem /= divider;
	}

	@Embeddable
	public static class ProcessStatsKey implements Serializable {

		private static final long serialVersionUID = 254559028810797851L;

		@Column(length = 100)
		private String sysName;
		@Column(length = 100)
		private String hostName;
		@Column(length = 100)
		private String processName;
		private Timestamp ts;

		public ProcessStatsKey() {
		}

		public ProcessStatsKey(String sysName, String hostName,
				String processName, Timestamp ts) {
			this.sysName = sysName;
			this.hostName = hostName;
			this.processName = processName;
			this.ts = ts;
		}

		public String getSysName() {
			return sysName;
		}

		public void setSysName(String sysName) {
			this.sysName = sysName;
		}

		public String getHostName() {
			return hostName;
		}

		public void setHostName(String hostName) {
			this.hostName = hostName;
		}

		public String getProcessName() {
			return processName;
		}

		public void setProcessName(String processName) {
			this.processName = processName;
		}

		public Timestamp getTs() {
			return ts;
		}

		public void setTs(Timestamp ts) {
			this.ts = ts;
		}
	}
}
