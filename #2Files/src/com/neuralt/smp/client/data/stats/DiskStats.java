package com.neuralt.smp.client.data.stats;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.neuralt.smp.agent.mbean.model.Disk;

@Entity
@Table(name = "HOST_STATS_DISK")
public class DiskStats {

	@EmbeddedId
	private DiskStatsKey key;
	@Column(name = "disk_size")
	private Long size;
	private Long used;
	private Long avail;
	private Long usedPercent;
	private String mount;

	public DiskStats() {
	}

	public DiskStats(HostStats owner, String fs) {
		key = new DiskStatsKey(owner, fs);
		size = 0L;
		used = 0L;
		avail = 0L;
		usedPercent = 0L;
	}

	public HostStats getOwner() {
		return key.owner;
	}

	public void setOwner(HostStats owner) {
		this.key.owner = owner;
	}

	public String getFs() {
		return key.fs;
	}

	public void setFs(String fs) {
		this.key.fs = fs;
	}

	public DiskStatsKey getKey() {
		return key;
	}

	public void setKey(DiskStatsKey key) {
		this.key = key;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Long getUsed() {
		return used;
	}

	public void setUsed(Long used) {
		this.used = used;
	}

	public Long getAvail() {
		return avail;
	}

	public void setAvail(Long avail) {
		this.avail = avail;
	}

	public Long getUsedPercent() {
		return usedPercent;
	}

	public void setUsedPercent(Long usedPercent) {
		this.usedPercent = usedPercent;
	}

	public String getMount() {
		return mount;
	}

	public void setMount(String mount) {
		this.mount = mount;
	}

	// convenience method for calculating
	public void add(Disk data) {
		size = data.getSize();
		used += data.getUsed();
		avail += data.getAvail();
		usedPercent += data.getUse();
	}

	public void divideBy(int divider) {
		if (divider > 0) {
			used /= divider;
			avail /= divider;
			usedPercent /= divider;
		}
	}

	@Embeddable
	public static class DiskStatsKey implements Serializable {
		private static final long serialVersionUID = -703611586249843795L;

		@ManyToOne
		@ForeignKey(name = "FK_CFG_DISK_HOST")
		private HostStats owner;
		private String fs;

		public DiskStatsKey() {
		}

		public DiskStatsKey(HostStats owner, String fs) {
			this.owner = owner;
			this.fs = fs;
		}

		public HostStats getOwner() {
			return owner;
		}

		public void setOwner(HostStats owner) {
			this.owner = owner;
		}

		public String getFs() {
			return fs;
		}

		public void setFs(String fs) {
			this.fs = fs;
		}
	}
}
