package com.neuralt.smp.client.data.stats;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;

import com.neuralt.smp.agent.mbean.model.Memory;

@Entity
@Table(name = "HOST_STATS_MEM")
public class MemStats {
	@EmbeddedId
	private MemStatsKey key;
	private Long total;
	private Long free;
	private Long used;
	private Float usedPct;

	public MemStats() {
	}

	public MemStats(HostStats owner, String statName) {
		key = new MemStatsKey(owner, statName);
		total = 0L;
		free = 0L;
		used = 0L;
		usedPct = 0.0f;
	}

	public MemStatsKey getKey() {
		return key;
	}

	public void setKey(MemStatsKey key) {
		this.key = key;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Long getFree() {
		return free;
	}

	public void setFree(Long free) {
		this.free = free;
	}

	public Long getUsed() {
		return used;
	}

	public void setUsed(Long used) {
		this.used = used;
	}

	public Float getUsedPct() {
		return usedPct;
	}

	public void setUsedPct(Float usedPct) {
		this.usedPct = usedPct;
	}

	// convenience methods for calculation
	public void add(Memory data) {
		if (data.getTotal() != null)
			total += data.getTotal();
		if (data.getFree() != null)
			free += data.getFree();
		if (data.getUsed() != null)
			used += data.getUsed();
		if (data.getUsedPct() != null)
			usedPct += data.getUsedPct();
	}

	public void divideBy(int divider) {
		if (divider > 0) {
			total /= divider;
			free /= divider;
			used /= divider;
			usedPct /= divider;
		}
	}

	@Embeddable
	public static class MemStatsKey implements Serializable {
		private static final long serialVersionUID = 6007977948743775794L;

		@ManyToOne
		@ForeignKey(name = "FK_STATS_MEM_HOST")
		private HostStats owner;
		private String statName; // mem/low/high/swap

		public MemStatsKey() {
		}

		public MemStatsKey(HostStats owner, String statName) {
			this.owner = owner;
			this.statName = statName;
		}

		public HostStats getOwner() {
			return owner;
		}

		public void setOwner(HostStats owner) {
			this.owner = owner;
		}

		public String getStatName() {
			return statName;
		}

		public void setStatName(String statName) {
			this.statName = statName;
		}
	}

	/*
	 * @OneToOne(mappedBy="memStats") private HostStats owner;
	 * 
	 * private Integer curTotal; private Integer curUsed; private Integer
	 * curFree; private Integer lowTotal; private Integer lowUsed; private
	 * Integer lowFree; private Integer highTotal; private Integer highUsed;
	 * private Integer highFree; private Integer swapTotal; private Integer
	 * swapUsed; private Integer swapFree;
	 * 
	 * public MemStats(HostStats owner) { this.owner = owner; curTotal = 0;
	 * curUsed = 0; curFree = 0; lowTotal = 0; lowUsed = 0; lowFree = 0;
	 * highTotal = 0; highUsed = 0; highFree = 0; swapTotal = 0; swapUsed = 0;
	 * swapFree = 0; }
	 * 
	 * public HostStats getOwner() { return owner; } public void
	 * setOwner(HostStats owner) { this.owner = owner; } public Integer
	 * getCurTotal() { return curTotal; } public void setCurTotal(Integer
	 * curTotal) { this.curTotal = curTotal; } public Integer getCurUsed() {
	 * return curUsed; } public void setCurUsed(Integer curUsed) { this.curUsed
	 * = curUsed; } public Integer getCurFree() { return curFree; } public void
	 * setCurFree(Integer curFree) { this.curFree = curFree; } public Integer
	 * getLowTotal() { return lowTotal; } public void setLowTotal(Integer
	 * lowTotal) { this.lowTotal = lowTotal; } public Integer getLowUsed() {
	 * return lowUsed; } public void setLowUsed(Integer lowUsed) { this.lowUsed
	 * = lowUsed; } public Integer getLowFree() { return lowFree; } public void
	 * setLowFree(Integer lowFree) { this.lowFree = lowFree; } public Integer
	 * getHighTotal() { return highTotal; } public void setHighTotal(Integer
	 * highTotal) { this.highTotal = highTotal; } public Integer getHighUsed() {
	 * return highUsed; } public void setHighUsed(Integer highUsed) {
	 * this.highUsed = highUsed; } public Integer getHighFree() { return
	 * highFree; } public void setHighFree(Integer highFree) { this.highFree =
	 * highFree; } public Integer getSwapTotal() { return swapTotal; } public
	 * void setSwapTotal(Integer swapTotal) { this.swapTotal = swapTotal; }
	 * public Integer getSwapUsed() { return swapUsed; } public void
	 * setSwapUsed(Integer swapUsed) { this.swapUsed = swapUsed; } public
	 * Integer getSwapFree() { return swapFree; } public void
	 * setSwapFree(Integer swapFree) { this.swapFree = swapFree; }
	 */
}
