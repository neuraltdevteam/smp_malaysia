package com.neuralt.smp.client.data.stats;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Index;

@Entity
@Table(name = "HOST_STATS")
public class HostStats implements Comparable<HostStats> {
	@Id
	@GeneratedValue
	private Integer id;
	private String sysName;
	private String hostName;

	@Index(name = "IDX_STATS_TS")
	private Timestamp ts;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "key.owner", orphanRemoval = true, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 500)
	@OrderBy("key.statName ASC")
	private Set<MemStats> memStats;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "key.owner", orphanRemoval = true, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 500)
	@OrderBy("key.processor ASC")
	private Set<CpuStats> cpuStats;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "key.owner", orphanRemoval = true, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 500)
	@OrderBy("key.fs ASC")
	private Set<DiskStats> diskStats;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "key.owner", orphanRemoval = true, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 500)
	@OrderBy("key.descr ASC")
	private Set<InetStats> inetStats;

	@Transient
	private MemStats[] memStatsArray;
	@Transient
	private CpuStats[] cpuStatsArray;
	@Transient
	private DiskStats[] diskStatsArray;
	@Transient
	private InetStats[] inetStatsArray;

	public HostStats() {
	}

	public HostStats(String sysName, String hostName, Timestamp ts) {
		// key = new HostStatsKey(sysName, hostName, ts);
		this.sysName = sysName;
		this.hostName = hostName;
		this.ts = ts;
		memStats = new HashSet<MemStats>();
		cpuStats = new HashSet<CpuStats>();
		diskStats = new HashSet<DiskStats>();
		inetStats = new HashSet<InetStats>();
	}

	public void addCpuStats(CpuStats stats) {
		if (cpuStats == null)
			cpuStats = new HashSet<CpuStats>();
		cpuStats.add(stats);
	}

	public void addDiskStats(DiskStats stats) {
		if (diskStats == null)
			diskStats = new HashSet<DiskStats>();
		diskStats.add(stats);
	}

	public void addInetStats(InetStats stats) {
		if (inetStats == null)
			inetStats = new HashSet<InetStats>();
		inetStats.add(stats);
	}

	public void addMemStats(MemStats stats) {
		if (memStats == null)
			memStats = new HashSet<MemStats>();
		memStats.add(stats);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public Timestamp getTs() {
		return ts;
	}

	public void setTs(Timestamp ts) {
		this.ts = ts;
	}

	// public HostStatsKey getKey() {
	// return key;
	// }
	// public void setKey(HostStatsKey key) {
	// this.key = key;
	// }
	public Set<MemStats> getMemStats() {
		return memStats;
	}

	public void setMemStats(Set<MemStats> memStats) {
		this.memStats = memStats;
	}

	public Set<CpuStats> getCpuStats() {
		return cpuStats;
	}

	public void setCpuStats(Set<CpuStats> cpuStats) {
		this.cpuStats = cpuStats;
	}

	public Set<DiskStats> getDiskStats() {
		return diskStats;
	}

	public void setDiskStats(Set<DiskStats> diskStats) {
		this.diskStats = diskStats;
	}

	public Set<InetStats> getInetStats() {
		return inetStats;
	}

	public void setInetStats(Set<InetStats> inetStats) {
		this.inetStats = inetStats;
	}

	public boolean isEmpty() {
		return (memStats == null || memStats.isEmpty())
				&& (cpuStats == null || cpuStats.isEmpty())
				&& (diskStats == null || diskStats.isEmpty())
				&& (inetStats == null || inetStats.isEmpty());
	}

	public void convertStatsToArrays() {
		memStatsArray = memStats.toArray(new MemStats[0]);
		diskStatsArray = diskStats.toArray(new DiskStats[0]);
		cpuStatsArray = cpuStats.toArray(new CpuStats[0]);
		inetStatsArray = inetStats.toArray(new InetStats[0]);
	}

	public MemStats[] getMemStatsArray() {
		return memStatsArray;
	}

	public CpuStats[] getCpuStatsArray() {
		return cpuStatsArray;
	}

	public DiskStats[] getDiskStatsArray() {
		return diskStatsArray;
	}

	public InetStats[] getInetStatsArray() {
		return inetStatsArray;
	}

	@Override
	public int compareTo(HostStats o) {
		if (ts != null && o.getTs() != null)
			return ts.compareTo(o.getTs());
		return 0;
	}

}
