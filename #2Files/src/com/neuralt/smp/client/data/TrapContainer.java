package com.neuralt.smp.client.data;

import java.io.Serializable;

import org.snmp4j.smi.OID;

public class TrapContainer implements Comparable, Serializable {
	private static final long serialVersionUID = 776056414000463643L;
	private OID oid;
	private String name;

	public TrapContainer() {
	}

	public TrapContainer(OID oid, String name) {
		this.oid = oid;
		this.name = name;
	}

	public OID getOid() {
		return oid;
	}

	public void setOid(OID oid) {
		this.oid = oid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Object o) {
		if (o instanceof TrapContainer) {
			return (oid.compareTo(((TrapContainer) o).getOid()));
		}
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof TrapContainer) {
			TrapContainer dummy = (TrapContainer) o;
			return dummy.getOid().equals(oid) && dummy.getName().equals(name);
		}
		return false;
	}

	@Override
	public int hashCode() {
		int i = 59;
		if (oid != null) {
			i += oid.hashCode() * 23;
		}
		if (name != null) {
			i += name.hashCode() * 11;
		}
		return i;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("TrapContainer : [oid=").append(oid.toString())
				.append(", name=").append(name).append("]");
		return sb.toString();
	}
}