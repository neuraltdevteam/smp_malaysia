package com.neuralt.smp.client.data.config;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;

import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.StringUtil;

@Entity
@Table(name = "ALARM_MAP")
public class AlarmMap implements Serializable, Comparable<AlarmMap> {

	private static final long serialVersionUID = -6530818958676828297L;

	public static enum AlarmMapType {
		EVENT, TRAP
	}

	// public static enum AlarmMapAction {
	// SET, CLEAR
	// }

	@Id
	@GeneratedValue
	private Integer id;
	@Embedded
	private AlarmMapKey key;
	@ManyToOne(fetch = FetchType.EAGER)
	@ForeignKey(name = "FK_ALARM_MAP")
	@Index(name = "IDX_ALARM_MAP_NAME")
	private AlarmDefinition mappedAlarm;

	// @Enumerated(EnumType.STRING)
	// private AlarmMapAction mapAction;

	public AlarmMap() {
		this.key = new AlarmMapKey();
	}

	public AlarmMap(AlarmMapType mapType, String causeId,
			WarningLevel causeLevel, AlarmDefinition mappedAlarm) {
		this();
		this.key.mapType = mapType;
		this.key.causeId = causeId;
		this.key.causeLevel = causeLevel;
		this.mappedAlarm = mappedAlarm;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public AlarmMapType getMapType() {
		return key.mapType;
	}

	public void setMapType(AlarmMapType mapType) {
		this.key.mapType = mapType;
	}

	public String getCauseId() {
		return key.causeId;
	}

	public void setCauseId(String causeId) {
		this.key.causeId = causeId;
	}

	public WarningLevel getCauseLevel() {
		return key.causeLevel;
	}

	public void setCauseLevel(WarningLevel causeLevel) {
		this.key.causeLevel = causeLevel;
	}

	public AlarmDefinition getMappedAlarm() {
		return mappedAlarm;
	}

	public void setMappedAlarm(AlarmDefinition mappedAlarm) {
		this.mappedAlarm = mappedAlarm;
	}

	public AlarmMapKey getKey() {
		return key;
	}

	public int hashCode() {
		return key.hashCode();
	}

	public boolean equals(Object obj) {
		if (obj instanceof AlarmMap) {
			AlarmMap other = (AlarmMap) obj;
			return key.equals(other.key);
		}
		return false;
	}

	public String toString() {
		return key + " - " + mappedAlarm;
	}

	@Embeddable
	public static class AlarmMapKey implements Serializable {

		private static final long serialVersionUID = 3632634881083017348L;

		@Enumerated(EnumType.STRING)
		private AlarmMapType mapType;
		// if event, causeId = event name. If trap, causeId = oid ... or
		// trapName?
		private String causeId;
		@Enumerated(EnumType.STRING)
		private WarningLevel causeLevel;

		public AlarmMapKey() {
		}

		public AlarmMapKey(AlarmMapType mapType, String causeId,
				WarningLevel causeLevel) {
			this.mapType = mapType;
			this.causeId = causeId;
			this.causeLevel = causeLevel;
		}

		public AlarmMapType getMapType() {
			return mapType;
		}

		public void setMapType(AlarmMapType mapType) {
			this.mapType = mapType;
		}

		public String getCauseId() {
			return causeId;
		}

		public void setCauseId(String causeId) {
			this.causeId = causeId;
		}

		public WarningLevel getCauseLevel() {
			return causeLevel;
		}

		public void setCauseLevel(WarningLevel causeLevel) {
			this.causeLevel = causeLevel;
		}

		public int hashCode() {
			if (StringUtil.isNullOrEmpty(causeId))
				return 0;
			return (causeId + causeLevel).hashCode();
		}

		public boolean equals(Object obj) {
			if (obj instanceof AlarmMapKey) {
				AlarmMapKey other = (AlarmMapKey) obj;
				return (mapType == other.mapType
						&& StringUtil.streql(causeId, other.causeId) && causeLevel == other.causeLevel);
			}
			return false;
		}

		public String toString() {
			return mapType + " " + causeId + " " + causeLevel;
		}
	}

	@Override
	public int compareTo(AlarmMap map) {
		if (map != null) {
			if (getMapType().equals(map.getMapType())) {
				if (getMapType().equals(AlarmMapType.EVENT)) {
					return getCauseId().compareTo(map.getCauseId());
				} else {
					if (getCauseId().equals(map.getCauseId())) {
						return getCauseLevel().compareTo(map.getCauseLevel());
					}
					return getCauseId().compareTo(map.getCauseId());
				}
			}
		}
		return 0;
	}
}
