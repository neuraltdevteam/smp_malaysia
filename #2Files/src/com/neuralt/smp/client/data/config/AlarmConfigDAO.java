package com.neuralt.smp.client.data.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;

import com.neuralt.smp.client.AlarmNotifier;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.dao.RawDAO;
import com.neuralt.smp.notification.WarningLevel;

public class AlarmConfigDAO {

	private RawDAO rawDao;

	public AlarmConfigDAO() {
		rawDao = new RawDAO(true);
	}

	public List<AlarmStatus> listAlarmStatus() {
		return rawDao.list(AlarmStatus.class);
	}

	public List<AlarmDefinition> listEnabledAlarmDefinitions() {
		DetachedCriteria crit = DetachedCriteria
				.forClass(AlarmDefinition.class).add(
						Restrictions.eq("enabled", true));
		Set<AlarmDefinition> set = new TreeSet<AlarmDefinition>(
				rawDao.findByCriteria(AlarmDefinition.class, crit, null, null));
		List<AlarmDefinition> list = new ArrayList<AlarmDefinition>(set);
		return list;
	}

	public List<AlarmDefinition> listAlarmDefinitions() {
		return rawDao.list(AlarmDefinition.class);
	}

	public AlarmDefinition getAlarmDefinition(Integer id) {
		return rawDao.findByKey(AlarmDefinition.class, id);
	}

	public List<AlarmDefinition> getAlarmDefinition(String name) {
		DetachedCriteria crit = DetachedCriteria
				.forClass(AlarmDefinition.class).add(
						Restrictions.eq("key.name", name));
		Set<AlarmDefinition> set = new TreeSet<AlarmDefinition>(
				rawDao.findByCriteria(AlarmDefinition.class, crit, null, null));
		List<AlarmDefinition> list = new ArrayList<AlarmDefinition>(set);
		return list;
	}

	public List<AlarmMap> listAlarmMaps() {
		return rawDao.list(AlarmMap.class);
	}

	public List<AlarmMap> listAlarmMaps(String alarmName, WarningLevel level) {
		DetachedCriteria crit = DetachedCriteria.forClass(AlarmMap.class)
				.createAlias("mappedAlarm", "ma")
				.add(Restrictions.eq("ma.key.name", alarmName))
				.add(Restrictions.eq("ma.key.level", level));
		Set<AlarmMap> set = new TreeSet<AlarmMap>(rawDao.findByCriteria(
				AlarmMap.class, crit, null, null));
		List<AlarmMap> list = new ArrayList<AlarmMap>(set);
		return list;
	}

	public <T> void save(T entity) {
		rawDao.save(entity);
		AlarmNotifier.instance.reload();
	}

	public <T> void batchPersist(Collection<T> batch) {
		rawDao.batchPersist(batch);
		AlarmNotifier.instance.reload();
	}

	public <T> void delete(T entity) {
		rawDao.delete(entity);
		AlarmNotifier.instance.reload();
	}

	public <T> void batchDelete(Collection<T> batch) {
		rawDao.batchDelete(batch);
		AlarmNotifier.instance.reload();
	}

	public <T> void batchPersist(Collection<T> batchToPersist,
			Collection<T> batchToDelete) {
		rawDao.beginTransaction();
		try {
			rawDao.batchDelete(batchToDelete);
			rawDao.batchPersist(batchToPersist);
			rawDao.commitTransaction();
			AlarmNotifier.instance.reload();
		} catch (Exception e) {
			rawDao.rollbackTransaction();
			throw new DataAccessException("batchPersist failed", e);
		}
	}
}
