package com.neuralt.smp.client.data.config;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.StringUtil;

/**
 * Declaration of an "Alarm"
 */
@Entity
@Table(name = "ALARM_DEFINITION")
public class AlarmDefinition implements Serializable,
		Comparable<AlarmDefinition> {

	private static final long serialVersionUID = 7746039044840586955L;

	public static final String TRAP_CONFIG_ENTRY_KEY = "trapDefault";
	public static final String EVENT_CONFIG_ENTRY_KEY = "eventDefault";

	// @Id
	// private String name;
	// private WarningLevel level;

	@Id
	@GeneratedValue
	private Integer id;
	@Embedded
	private AlarmDefinitionKey key;
	private boolean enabled = false;

	// mappings that points to this alarm definition ... mainly used for
	// hibernate's cascade remove and "orphanRemoval" feature here
	@OneToMany(mappedBy = "mappedAlarm", cascade = CascadeType.REMOVE, orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<AlarmMap> alarmMaps;

	public AlarmDefinition() {
		key = new AlarmDefinitionKey();
	}

	public AlarmDefinition(String name, WarningLevel level) {
		key = new AlarmDefinitionKey();
		key.setName(name);
		key.setLevel(level);
	}

	public String getName() {
		return key.getName();
	}

	public void setName(String name) {
		key.setName(name);
	}

	public WarningLevel getLevel() {
		return key.getLevel();
	}

	public void setLevel(WarningLevel level) {
		key.setLevel(level);
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Set<AlarmMap> getAlarmMaps() {
		return alarmMaps;
	}

	public void setAlarmMaps(Set<AlarmMap> alarmMaps) {
		this.alarmMaps = alarmMaps;
	}

	public int hashCode() {
		return key.hashCode();
	}

	public boolean equals(Object obj) {
		if (obj instanceof AlarmDefinition) {
			return key.equals(((AlarmDefinition) obj).key);
		}
		return false;
	}

	public String getKeyString() {
		return key.toString();
	}

	public String toString() {
		return key + ", enabled=" + enabled;
	}

	@Embeddable
	public static class AlarmDefinitionKey implements Serializable,
			Comparable<AlarmDefinitionKey> {

		private static final long serialVersionUID = 5794032770137851812L;

		private String name;
		@Enumerated(EnumType.ORDINAL)
		@Column(name = "warningLevel")
		private WarningLevel level;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public WarningLevel getLevel() {
			return level;
		}

		public void setLevel(WarningLevel level) {
			this.level = level;
		}

		public String toString() {
			return name + " - " + level.name();
		}

		public int hashCode() {
			if (StringUtil.isNullOrEmpty(name) || level == null)
				return 0;
			return (name + level.name()).hashCode();
		}

		public boolean equals(Object obj) {
			if (obj instanceof AlarmDefinitionKey) {
				AlarmDefinitionKey other = (AlarmDefinitionKey) obj;
				return (StringUtil.streql(this.name, other.name))
						&& (this.level == other.level);
			}
			return false;
		}

		@Override
		public int compareTo(AlarmDefinitionKey key) {
			if (key != null) {
				if (name != null && key.name != null) {
					if (name.equals(key.name)) {
						if (level != null && key.level != null) {
							return level.compareTo(key.level);
						}
					}
					return name.compareTo(key.name);
				}
			}
			return 0;
		}
	}

	@Override
	public int compareTo(AlarmDefinition def) {
		if (def != null) {
			if (key != null && def.key != null) {
				return key.compareTo(def.key);
			}
		}
		return 0;
	}
}
