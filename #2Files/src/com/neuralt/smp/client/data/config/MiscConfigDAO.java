package com.neuralt.smp.client.data.config;

import com.neuralt.smp.client.data.dao.GenericDAO;

// ... needed?
public class MiscConfigDAO extends GenericDAO<MiscConfig, String> {

	public MiscConfigDAO(boolean callerManagedTransAllowed) {
		super(MiscConfig.class, callerManagedTransAllowed);
	}
}
