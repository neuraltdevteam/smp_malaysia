package com.neuralt.smp.client.data.config;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Miscellaneous String-String key-value pairs that must be configurable in
 * console (persisted to DB)
 */
@Entity
@Table(name = "MISC_CONFIG")
public class MiscConfig implements Serializable {

	private static final long serialVersionUID = -4893196862607209839L;

	@Id
	private String configKey;
	private String configValue;

	public MiscConfig() {

	}

	public MiscConfig(String configKey, String configValue) {
		this.configKey = configKey;
		this.configValue = configValue;
	}

	public String getConfigKey() {
		return configKey;
	}

	public void setConfigKey(String configKey) {
		this.configKey = configKey;
	}

	public String getConfigValue() {
		return configValue;
	}

	public void setConfigValue(String configValue) {
		this.configValue = configValue;
	}
}
