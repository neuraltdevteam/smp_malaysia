package com.neuralt.smp.client.data.config;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.neuralt.smp.client.web.TextColor;

@Entity
@Table(name = "ALARM_STATUS")
public class AlarmStatus implements Serializable {

	private static final long serialVersionUID = -1589102077787671421L;
	
	public static final AlarmStatus UNHANDLED = new AlarmStatus("unhandled", StatusCategory.INITIAL, TextColor.RED, true);
	public static final AlarmStatus ACKNOWLEDGED = new AlarmStatus("acknowledged", StatusCategory.INTERIM, TextColor.ORANGE, true);
	public static final AlarmStatus IGNORED = new AlarmStatus("ignored", StatusCategory.FINAL, TextColor.GREEN, true);
	public static final AlarmStatus RESOLVED = new AlarmStatus("resolved", StatusCategory.FINAL, TextColor.GREEN, true);
	public static final AlarmStatus CLEARED = new AlarmStatus("auto-cleared", StatusCategory.FINAL, TextColor.GREEN, true);

	public static enum StatusCategory {
		INITIAL, INTERIM, FINAL
	}

	@Id
	private String name;
	@Enumerated(EnumType.STRING)
	private StatusCategory category;
	@Enumerated(EnumType.STRING)
	private TextColor textColor;
	@Transient
	private boolean isDefault = false;

	public AlarmStatus() {

	}

	public AlarmStatus(String name, StatusCategory category, TextColor color,
			boolean isDefault) {
		this.name = name;
		this.category = category;
		this.textColor = color;
		this.isDefault = isDefault;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public StatusCategory getCategory() {
		return category;
	}

	public void setCategory(StatusCategory category) {
		this.category = category;
	}

	public TextColor getTextColor() {
		return textColor;
	}

	public void setTextColor(TextColor textColor) {
		this.textColor = textColor;
	}

	public boolean isDefault() {
		return isDefault;
	}

	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
}
