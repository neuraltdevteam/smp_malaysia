package com.neuralt.smp.client.data;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.neuralt.smp.config.HostSnmpConfig;
import com.neuralt.smp.config.model.Editable;

@Entity
@Table(name = "SNMP_TARGET")
public class SnmpTarget implements Serializable, Comparable<SnmpTarget>,
		Editable<SnmpTarget> {
	private static final long serialVersionUID = 2539592141758659049L;
	private static String NOT_APPLICABLE = "N/A";
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	/**
	 * The name has to be unique in a HostConfig
	 */
	private String name;

	public SnmpTarget() {
		methodType = MethodType.STATIC;
	}

	private String walkDir;
	private String valueDir;
	private String keyDir;
	private String targetString;
	@Enumerated(EnumType.STRING)
	private MethodType methodType;
	@Enumerated(EnumType.STRING)
	private TargetType keyType;
	@ManyToOne
	@JoinColumn(name = "HOST_SNMP_CONFIG_ID", nullable = false)
	private HostSnmpConfig snmpConfig;
	private int suffix;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "snmp", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<DataCollectionProcedure> dataCollectProc;

	public enum MethodType {
		STATIC, DYNAMIC
	}

	public enum TargetType {
		INTEGER, STRING, BOOLEAN
	}

	@Transient
	public String getKeyString() {
		if (methodType.equals(MethodType.STATIC)) {
			return NOT_APPLICABLE;
		}
		return targetString;
	}

	@Transient
	public String getSuffixString() {
		if (methodType.equals(MethodType.DYNAMIC)) {
			return NOT_APPLICABLE;
		}
		return String.valueOf(suffix);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(" [name=")
				.append(name).append(", methodType=").append(methodType)
				.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof SnmpTarget) {
			SnmpTarget dummy = (SnmpTarget) o;
			return id == dummy.getId();
		}
		return false;
	}

	@Override
	public int compareTo(SnmpTarget other) {
		if (other != null) {
			if (name != null && other.getName() != null) {
				return name.compareTo(other.getName());
			}
		}
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((keyDir == null) ? 0 : keyDir.hashCode());
		result = prime * result + ((keyType == null) ? 0 : keyType.hashCode());
		result = prime * result
				+ ((methodType == null) ? 0 : methodType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + suffix;
		result = prime * result
				+ ((targetString == null) ? 0 : targetString.hashCode());
		result = prime * result
				+ ((valueDir == null) ? 0 : valueDir.hashCode());
		result = prime * result + ((walkDir == null) ? 0 : walkDir.hashCode());
		return result;
	}

	@Override
	public SnmpTarget getBackup() {
		SnmpTarget dummy = new SnmpTarget();
		dummy.setKeyDir(keyDir);
		dummy.setKeyType(keyType);
		dummy.setMethodType(methodType);
		dummy.setTargetString(targetString);
		dummy.setValueDir(valueDir);
		dummy.setWalkDir(walkDir);
		dummy.setSuffix(suffix);
		dummy.setName(name);
		return dummy;
	}

	@Override
	public void update(SnmpTarget dummy) {
		walkDir = dummy.getWalkDir();
		keyDir = dummy.getKeyDir();
		valueDir = dummy.getValueDir();
		targetString = dummy.getTargetString();
		methodType = dummy.getMethodType();
		keyType = dummy.getKeyType();
		suffix = dummy.getSuffix();
		name = dummy.getName();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getWalkDir() {
		return walkDir;
	}

	public void setWalkDir(String walkDir) {
		this.walkDir = walkDir;
	}

	public String getValueDir() {
		return valueDir;
	}

	public void setValueDir(String valueDir) {
		this.valueDir = valueDir;
	}

	public String getKeyDir() {
		return keyDir;
	}

	public void setKeyDir(String keyDir) {
		this.keyDir = keyDir;
	}

	public String getTargetString() {
		return targetString;
	}

	public void setTargetString(String targetString) {
		this.targetString = targetString;
	}

	public MethodType getMethodType() {
		return methodType;
	}

	public void setMethodType(MethodType methodType) {
		this.methodType = methodType;
	}

	public TargetType getKeyType() {
		return keyType;
	}

	public void setKeyType(TargetType keyType) {
		this.keyType = keyType;
	}

	public HostSnmpConfig getSnmpConfig() {
		return snmpConfig;
	}

	public void setSnmpConfig(HostSnmpConfig snmpConfig) {
		this.snmpConfig = snmpConfig;
	}

	public int getSuffix() {
		return suffix;
	}

	public void setSuffix(int suffix) {
		this.suffix = suffix;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<DataCollectionProcedure> getDataCollectProc() {
		return dataCollectProc;
	}

	public void setDataCollectProc(Set<DataCollectionProcedure> dataCollectProc) {
		this.dataCollectProc = dataCollectProc;
	}

}
