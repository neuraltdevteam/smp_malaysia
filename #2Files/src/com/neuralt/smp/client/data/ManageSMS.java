package com.neuralt.smp.client.data;

/**
 *
 * @author ag111
 */


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class ManageSMS implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Integer id;
        private Integer errorcode;
        private Integer langid;
        private String message;
        private Integer channel_id;
        private Timestamp createDate;
      
	public ManageSMS() {
		createDate = new Timestamp(System.currentTimeMillis());
	}

	public ManageSMS(Integer id, Integer errorcode, Integer langid, String message, Integer channel_id) {
		this.id = id;
		this.errorcode = errorcode;
                this.langid = langid;
                this.message = message;
                this.channel_id = channel_id;
	}
        
          public ManageSMS getBackup() {
		ManageSMS dummy = new ManageSMS();
		dummy.setid(id);
		dummy.seterrorcode(errorcode);
		dummy.setlangid(langid);
		dummy.setmessage(message);
		dummy.setchannel_id(channel_id);
		
		return dummy;
	}

	public void update(ManageSMS dummy) {
		dummy.setid(id);
		dummy.seterrorcode(errorcode);
		dummy.setlangid(langid);
		dummy.setmessage(message);
		dummy.setchannel_id(channel_id);
	}

	public Integer getid() {
		return id;
	}

	public void setid(Integer id) {
		this.id = id;
	}

	public Integer geterrorcode() {
		return errorcode;
	}

	public void seterrorcode(Integer errorcode) {
		this.errorcode = errorcode;
	}
        
        public Integer getlangid() {
		return langid;
	}

	public void setlangid(Integer langid) {
		this.langid = langid;
	}
        
        public String getmessage() {
		return message;
	}

	public void setmessage(String message) {
		this.message = message;
	}
        
        public Integer getchannel_id() {
		return channel_id;
	}

	public void setchannel_id(Integer channel_id) {
		this.channel_id = channel_id;
	}
        

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("ID: " + id + ";");
		buffer.append("ERRORCODE: " + errorcode + ";");
                buffer.append("LANGID: " + langid + ";");
                buffer.append("MESSAGE: " + message + ";");
                buffer.append("CHANNEL_ID: " + channel_id + ";");
               
		return buffer.toString();
	}
}
