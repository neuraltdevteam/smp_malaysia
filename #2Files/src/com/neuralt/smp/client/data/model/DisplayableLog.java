package com.neuralt.smp.client.data.model;

import java.util.Date;

public interface DisplayableLog {

	public String getLevel();

	public String getDisplayableSource();

	public Date getTime();

	public String getDescription();

	public String getAppName();

	public String getEventName();

	public String getSourceType();

	public boolean isTrap();

	public long getId();
}
