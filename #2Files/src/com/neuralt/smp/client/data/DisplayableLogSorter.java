package com.neuralt.smp.client.data;

import java.util.Comparator;

import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.model.DisplayableLog;
import com.neuralt.smp.notification.WarningLevel;

public class DisplayableLogSorter implements Comparator<DisplayableLog> {

	private String sortField;
	private SortOrder sortOrder;
	private static final Logger logger = LoggerFactory
			.getLogger(DisplayableLogSorter.class);

	public DisplayableLogSorter(String sortField, SortOrder sortOrder) {
		this.sortField = sortField;
		this.sortOrder = sortOrder;
	}

	@Override
	public int compare(DisplayableLog o1, DisplayableLog o2) {
		int result = 0;
		try {
			// suppose sort order is ascending
			if (sortOrder != SortOrder.UNSORTED) {
				if (sortField.equals("time"))
					result = o1.getTime().compareTo(o2.getTime());
				else if (sortField.equals("level"))
					result = WarningLevel.valueOf(o1.getLevel()).compareTo(
							WarningLevel.valueOf(o2.getLevel()));
			}
			// else reverse the sign of result
			if (sortOrder == SortOrder.DESCENDING) {
				result = -result;
			}
		} catch (NullPointerException e) {
			return 0;
		} catch (SecurityException e) {
			logger.debug("security exception", e);
		} catch (IllegalArgumentException e) {
			logger.debug("illegal arg exception", e);
			return 0;
		}
		return result;
	}

}
