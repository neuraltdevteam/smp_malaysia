package com.neuralt.smp.client.data;

public class DataAccessException extends RuntimeException {

	private static final long serialVersionUID = 2557858542774480098L;

	public DataAccessException(String msg) {
		super(msg);
	}

	public DataAccessException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
