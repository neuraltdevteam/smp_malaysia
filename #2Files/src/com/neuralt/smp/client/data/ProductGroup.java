package com.neuralt.smp.client.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.snmp4j.smi.OID;

import com.neuralt.smp.snmp.util.MibFileLoader;
import com.neuralt.smp.util.StringUtil;

@Entity
@Table(name = "PRODUCT_GROUP")
public class ProductGroup implements Serializable, Comparable<ProductGroup> {
	private static final long serialVersionUID = -7682731548283965381L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private OID oid;
	private String name;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "parent", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<OidMap> oidMaps;

	public ProductGroup() {
		oidMaps = new TreeSet<OidMap>();
	}

	@Transient
	public String getOidAsTrapName() {
		if (!StringUtil.isNullOrEmpty(name)) {
			return name;
		}
		if (oid != null) {
			if (MibFileLoader.getInstance().getSvrTrapNameMap()
					.containsKey(oid)) {
				return MibFileLoader.getInstance().getSvrTrapNameMap().get(oid);
			}
			return oid.toString();
		}
		return "";
	}

	@Transient
	public List<OidMap> getOidMapAsList() {
		List<OidMap> list = new ArrayList<OidMap>(oidMaps);
		Collections.sort(list);
		return list;
	}

	public Set<OidMap> getOidMaps() {
		return oidMaps;
	}

	public void setOidMaps(Set<OidMap> oidMaps) {
		this.oidMaps = oidMaps;
	}

	public OID getOid() {
		return oid;
	}

	public void setOid(OID oid) {
		this.oid = oid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Override
	public int compareTo(ProductGroup o) {
		if (o != null) {
			if (oid != null && o.oid != null)
				return oid.compareTo(o.oid);
		}
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ProductGroup) {
			ProductGroup dummy = (ProductGroup) o;
			return id == dummy.getId();
		}
		return false;
	}

	public ProductGroup getBackup() {
		ProductGroup dummy = new ProductGroup();
		dummy.setName(name);
		dummy.setOid(oid);
		return dummy;
	}

	public void updateConfig(ProductGroup dummy) {
		setOid(dummy.getOid());
		setName(dummy.getName());
	}

}
