package com.neuralt.smp.client.data.collector;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataPoint;
import com.neuralt.smp.client.data.DataWrapper;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.rrd.RrdAccessInterface;
import com.neuralt.smp.rrd.RrdService;
import com.neuralt.smp.util.ConstantUtil;

/**
 * This class is backed by a RrdDefConfig object. It assigns subtasks for
 * collecting data with respective to the DSs. It collects the data from the
 * subtasks and wraps them into a DataWrapper, then hands over to
 * RrdAccessInterface for the actual update action.
 * 
 * @author g705176
 *
 */
public class DataCollector extends Thread {
	public static final String MY_NAME = "DATA_COLLECTOR";

	private static final Logger logger = LoggerFactory
			.getLogger(DataCollector.class);
	private RrdDefConfig def;
	private RrdAccessInterface access;
	private boolean quit;
	private DataWrapper wrapper;
	private long lastReportTime;
	private List<CollectWorker> workers;

	// private ExecutorService threadPool;

	public DataCollector(RrdDefConfig def) {
		// super(MY_NAME);
		this.def = def;
		workers = new ArrayList<CollectWorker>();
		wrapper = DataWrapper.createDataWrapper();
		access = RrdService.getDefaultRrdAccess();

		// int corePoolSize = 10;
		// int maximumPoolSize = 100;
		// long keepAliveTime = 1;
		// TimeUnit unit = TimeUnit.MINUTES;
		// BlockingQueue<Runnable> workQueue = new
		// LinkedBlockingQueue<Runnable>();
		// ThreadFactory threadFactory = new
		// CollectorThreadFactory(CollectWorker.MY_NAME);
		// RejectedExecutionHandler handler = new
		// ThreadPoolExecutor.DiscardOldestPolicy();
		//
		// threadPool = new ThreadPoolExecutor(corePoolSize, maximumPoolSize,
		// keepAliveTime, unit, workQueue, threadFactory, handler);
	}

	public void setAccess(RrdAccessInterface access) {
		this.access = access;
	}

	public void run() {
		// startup underlying tasks for each DS
		// logger.debug("ds is null:"+(def.getRrdDsConfigs()==null));
		// logger.debug("no. of DS:"+def.getRrdDsConfigs().size());
		for (RrdDsConfig ds : def.getRrdDsConfigs()) {
			CollectWorker worker = new CollectWorker(this, def.getStep(), ds);
			worker.start();
			// threadPool.execute(worker);
			workers.add(worker);
		}
		lastReportTime = System.currentTimeMillis() - def.getStep()
				* ConstantUtil.SEC_MS;
		// start waiting for data collection and hand over to RRD
		while (!quit) {
			boolean wait = false;
			if (wrapper.getPoints().size() < workers.size()) {
				// not all data points are received, lets wait a bit longer, say
				// 20% of the step...
				wait = true;
			}

			if (wait) {
				try {
					Thread.sleep(def.getStep() * ConstantUtil.SEC_MS / 5);
				} catch (InterruptedException ignore) {
				}
			}
			synchronized (wrapper) {
				lastReportTime = System.currentTimeMillis();
				wrapper.setTime(lastReportTime);
				try {
					access.update(def, wrapper);
				} catch (Exception e) {
					logger.error("failed to update RRD, quiting...", e);
					def.setEnabled(false);
					def.setLocked(false);
					DataAccess.rrdDefConfigDao.save(def);
					quit();
					// TODO
					// add some log here?
					break;
				}
				DataAccess.rrdDefConfigDao.updateTime(def);
				wrapper.getPoints().clear();
			}
			try {
				Thread.sleep(def.getStep() * ConstantUtil.SEC_MS);
			} catch (InterruptedException ignore) {
			}
		}
		logger.info("data collector of " + def.getName() + " is quiting....");
	}

	public boolean quit() {
		quit = true;
		this.interrupt();

		for (CollectWorker worker : workers) {
			logger.debug("worker " + worker + " is quit:" + worker.quit());
		}

		// if (threadPool != null) {
		// threadPool.shutdownNow();
		// }

		return quit;
	}

	/**
	 * This is the method for the DS subtasks to hand their data to the
	 * collector
	 * 
	 * @param name
	 *            the DS name
	 * @param time
	 *            time of data collection
	 * @param value
	 *            value of data collection
	 */
	public void updateWrapper(DataPoint dp) {
		// logger.debug("report received, start analyzing...");
		if (!wrapper.getPoints().contains(dp)) {
			synchronized (wrapper) {
				if (!wrapper.getPoints().contains(dp)) {
					// check for the time of dp
					// logger.debug("report received at:"+dp.getTime()+" with value:"+dp.getValue());
					// TODO revise the logic
					// if the dp time is too close to lastReportTime, it is
					// probably a late report that should be sent with the last
					// batch, so discard
					if (dp.getTime() - lastReportTime > def.getStep()
							* ConstantUtil.SEC_MS / 10) {
						wrapper.getPoints().add(dp);
						def.incAccept();
						// logger.debug("report accepted - time difference:"+(dp.getTime()-lastReportTime));
					} else {
						// logger.debug("report rejected - "+def.getName()+" - time difference:"+(dp.getTime()-lastReportTime));
						def.incReject();
					}
				}
			}
		}
	}

	public RrdDefConfig getDef() {
		return def;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(" :[rrdDef=")
				.append(def).append("]");
		return sb.toString();
	}
}
