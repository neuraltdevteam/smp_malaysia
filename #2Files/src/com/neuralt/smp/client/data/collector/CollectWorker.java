package com.neuralt.smp.client.data.collector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataCollectionProcedure;
import com.neuralt.smp.client.data.DataCollectionProcedure.MethodType;
import com.neuralt.smp.client.data.DataPoint;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.client.data.ScriptConfig;
import com.neuralt.smp.client.data.SnmpTarget;
import com.neuralt.smp.client.snmp.task.GeneralSnmpGetTask;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.client.snmp.util.V1GetStrategy;
import com.neuralt.smp.client.snmp.util.V2GetStrategy;
import com.neuralt.smp.client.snmp.util.V3GetStrategy;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.config.HostSnmpConfig;
import com.neuralt.smp.config.model.BaseSnmpConfig.SnmpVersion;
import com.neuralt.smp.script.DefaultLinuxScriptRunner;
import com.neuralt.smp.script.DefaultWindowsScriptRunner;
import com.neuralt.smp.script.ScriptRunnerInterface;
import com.neuralt.smp.util.ConstantUtil;

public class CollectWorker extends Thread {
	public static final String MY_NAME = "COLLECT_WORKER";

	private static final Logger logger = LoggerFactory
			.getLogger(CollectWorker.class);
	private boolean quit;
	private long interval;
	private RrdDsConfig ds;
	private DataCollector parent;
	private GeneralSnmpGetTask snmpTask;
	private ScriptRunnerInterface scriptTask;

	public CollectWorker(DataCollector parent, long interval, RrdDsConfig ds) {
		// super(MY_NAME);
		this.parent = parent;
		this.interval = interval;
		this.ds = ds;
	}

	public void run() {
		// find the associated method of the DS
		DataCollectionProcedure procedure = ds.getDataCollectProc();
		if (procedure != null) {
			// use the method to start collecting data
			if (procedure.getType().equals(MethodType.SNMP)) {
				startSnmpGetTask(procedure);
			} else if (procedure.getType().equals(MethodType.SCRIPT)) {
				startScriptGetTask(procedure);
			}
		}
	}

	private void startSnmpGetTask(DataCollectionProcedure procedure) {
		SnmpTarget target = procedure.getSnmp();
		HostSnmpConfig config = target.getSnmpConfig();
		SnmpGetStrategy util = null;
		if (config.getVersion().equals(SnmpVersion.SNMPV1)) {
			util = new V1GetStrategy(config.getHost().getIpAddr(),
					config.getCommunityGet(), config.getPort());
		} else if (config.getVersion().equals(SnmpVersion.SNMPV2)) {
			util = new V2GetStrategy(config.getHost().getIpAddr(),
					config.getCommunityGet(), config.getPort());
		} else if (config.getVersion().equals(SnmpVersion.SNMPV3)) {
			util = new V3GetStrategy(config.getHost().getIpAddr(),
					config.getPort(), config.getUsername(), config.getAuth()
							.getProtocol(), config.getAuthPassword(), config
							.getPriv().getProtocol(), config.getPrivPassword());
		}
		MonitoredHost monitoredHost = SmpClient.instance.getHost(target
				.getSnmpConfig().getHost().getSystemConfig().getName(), target
				.getSnmpConfig().getHost().getName());
		if (monitoredHost != null) {
			snmpTask = new GeneralSnmpGetTask(util, target, monitoredHost);
		} else {
			snmpTask = new GeneralSnmpGetTask(util, target);
		}
		while (!quit) {
			DataPoint dp = null;
			try {
				dp = new DataPoint(ds.getName(), (double) snmpTask.query());
				dp.setTime(System.currentTimeMillis());
				parent.updateWrapper(dp);
			} catch (Exception ignore) {
				logger.error("snmp query failed for DS {} : {}", ds.getName(),
						ignore.getMessage());
				// ignore.printStackTrace();
				// snmp query failed, ignore
			}

			try {
				Thread.sleep(interval * ConstantUtil.SEC_MS);
			} catch (InterruptedException ignore) {
			}
		}
	}

	private void startScriptGetTask(DataCollectionProcedure procedure) {
		ScriptConfig script = procedure.getScript();
		// TODO
		// should be configurable somewhere as it may require different runner
		if (AppConfig.getScriptRunnerVersion().equals(ConstantUtil.WINDOWS))
			scriptTask = new DefaultWindowsScriptRunner();
		else if (AppConfig.getScriptRunnerVersion().equals(ConstantUtil.LINUX)) {
			scriptTask = new DefaultLinuxScriptRunner();
		}
		while (!quit) {
			DataPoint dp = null;
			try {
				dp = new DataPoint(ds.getName(), scriptTask.query(script
						.getPath()));
				dp.setTime(System.currentTimeMillis());
				parent.updateWrapper(dp);
				Thread.sleep(interval * ConstantUtil.SEC_MS);
			} catch (InterruptedException ignore) {
			}
		}
	}

	public boolean quit() {
		quit = true;
		this.interrupt();
		// bug fix for can't disabled and lock status in datadefinition page, 30/10/2015 by jimmy.so 
		if (snmpTask != null){
			snmpTask.quit();
			logger.debug("{} is quiting...", this);
		}
		else 
		{
			logger.error("snmpTask can not be interrupted! , becuase snmpTask is null.");
		}
		// End , 30/10/2015 by jimmy.so 
		return quit;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(" :[ds=").append(ds)
				.append("]");
		return sb.toString();
	}
}
