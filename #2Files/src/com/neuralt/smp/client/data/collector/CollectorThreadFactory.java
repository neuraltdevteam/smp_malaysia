package com.neuralt.smp.client.data.collector;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 
 * Customized ThreadFactory to create worker threads with proper names
 * 
 * @author Kin Cheung
 *
 */
public class CollectorThreadFactory implements ThreadFactory {
	final String namePrefix;
	final AtomicInteger count;

	public CollectorThreadFactory(String name) {
		this.namePrefix = name;
		count = new AtomicInteger(1);
	}

	@Override
	public Thread newThread(Runnable runnable) {
		Thread t = new Thread(runnable);
		t.setName(namePrefix);
		t.setDaemon(false);
		t.setPriority(Thread.NORM_PRIORITY);
		return t;
	}

}
