package com.neuralt.smp.client.data;

import java.util.Comparator;

import com.neuralt.smp.client.web.security.UserRight;

public class UserRightComparator implements Comparator<UserRight> {

	@Override
	public int compare(UserRight o1, UserRight o2) {
		UserRight right1 = (UserRight) o1;
		UserRight right2 = (UserRight) o2;
		int flag = right1.getTypeId().compareTo(right2.getTypeId());
		return flag;
	}

}