package com.neuralt.smp.client.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.RrdArchiveConfig.TimeType;
import com.neuralt.smp.client.web.security.ConsoleUser;
import com.neuralt.smp.config.model.Editable;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;

@Entity
@Table(name = "CHART_TPL")
public class ChartTemplate implements Serializable, Editable<ChartTemplate>,
		Comparable<ChartTemplate> {
	private static final long serialVersionUID = -6971248678069650401L;
	private static final Logger logger = LoggerFactory
			.getLogger(ChartTemplate.class);

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String verticalLabel;
	private int timeRange;
	private int priority;
	@Enumerated(EnumType.STRING)
	private TimeType timeRangeType;
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<RrdDsConfig> dsConfigs;
	@ManyToMany(fetch = FetchType.EAGER)
	private Set<RrdArchiveConfig> archiveConfigs;
	@ManyToOne(fetch = FetchType.EAGER)
	private ConsoleUser user;
	private boolean enabled;

	public ChartTemplate() {
		timeRangeType = TimeType.SECOND;
		dsConfigs = new HashSet<RrdDsConfig>();
		archiveConfigs = new HashSet<RrdArchiveConfig>();
	}

	@Transient
	public String getExtender() {
		// return "ext_"+name;
		return StringUtil.generateExtender(name);
		// return "ext";
	}

	@Transient
	public long getStart() {
		long now = System.currentTimeMillis();
		long difference = (RrdArchiveConfig.translateTime(timeRange,
				timeRangeType) + 1) * ConstantUtil.SEC_MS;
		return now - difference;
	}

	@Transient
	public String getLabel() {
		if (id == 0)
			logger.debug("getting label...name=" + name);
		if (StringUtil.isNullOrEmpty(name)) {
			return "NEW Template";
		}
		return name;
	}

	@Transient
	public boolean isConfigValid() {
		if (dsConfigs.size() > 0 && archiveConfigs.size() > 0) {
			boolean ok = false;
			for (RrdArchiveConfig archive : archiveConfigs) {
				if (archive.isLocked()) {
					for (RrdDsConfig ds : dsConfigs) {
						if (ds.getRrdDef().equals(archive.getRrdDef())) {
							ok = true;
							break;
						}
					}
				}
			}
			return ok;
		}
		return false;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(int timeRange) {
		this.timeRange = timeRange;
	}

	public TimeType getTimeRangeType() {
		return timeRangeType;
	}

	public void setTimeRangeType(TimeType timeRangeType) {
		this.timeRangeType = timeRangeType;
	}

	public Set<RrdDsConfig> getDsConfigs() {
		return dsConfigs;
	}

	public void setDsConfigs(Set<RrdDsConfig> dsConfigs) {
		this.dsConfigs = dsConfigs;
	}

	public Set<RrdArchiveConfig> getArchiveConfigs() {
		return archiveConfigs;
	}

	public void setArchiveConfigs(Set<RrdArchiveConfig> archiveConfigs) {
		this.archiveConfigs = archiveConfigs;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public String getVerticalLabel() {
		return verticalLabel;
	}

	public void setVerticalLabel(String verticalLabel) {
		this.verticalLabel = verticalLabel;
	}

	public ConsoleUser getUser() {
		return user;
	}

	public void setUser(ConsoleUser user) {
		this.user = user;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Transient
	public List<RrdArchiveConfig> getArchiveConfigsAsList() {
		List<RrdArchiveConfig> list = new ArrayList<RrdArchiveConfig>();
		if (archiveConfigs != null) {
			list = new ArrayList<RrdArchiveConfig>(archiveConfigs);
			Collections.sort(list);
		}
		return list;
	}

	@Transient
	public List<RrdDsConfig> getDsConfigsAsList() {
		List<RrdDsConfig> list = new ArrayList<RrdDsConfig>();
		if (dsConfigs != null) {
			list = new ArrayList<RrdDsConfig>(dsConfigs);
			Collections.sort(list);
		}
		return list;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(" :[name=")
				.append(name).append(", verticalLabel=").append(verticalLabel)
				.append(", priority=").append(priority).append(", timeRange=")
				.append(timeRange).append(" ").append(timeRangeType)
				.append(", ds count=").append(dsConfigs.size())
				.append(", archive count=").append(archiveConfigs.size())
				.append("]");
		return sb.toString();
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ChartTemplate) {
			ChartTemplate dummy = (ChartTemplate) o;
			return id == dummy.getId();
		}
		return false;
	}

	@Override
	public int compareTo(ChartTemplate temp) {
		if (temp != null) {
			return priority - temp.priority;
		}
		return 0;
	}

	@Override
	public ChartTemplate getBackup() {
		ChartTemplate dummy = new ChartTemplate();
		dummy.name = name;
		dummy.timeRange = timeRange;
		dummy.timeRangeType = timeRangeType;
		dummy.dsConfigs = dsConfigs;
		dummy.archiveConfigs = archiveConfigs;
		return dummy;
	}

	@Override
	public void update(ChartTemplate dummy) {
		name = dummy.name;
		timeRange = dummy.timeRange;
		timeRangeType = dummy.timeRangeType;
		dsConfigs = dummy.dsConfigs;
		archiveConfigs = dummy.archiveConfigs;
	}
}
