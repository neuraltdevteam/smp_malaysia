package com.neuralt.smp.client.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;

import com.neuralt.smp.config.model.Editable;
import com.neuralt.smp.util.ConstantUtil;

@Entity
@Table(name = "RRD_ARCH_CFG")
public class RrdArchiveConfig implements Serializable,
		Comparable<RrdArchiveConfig>, Editable<RrdArchiveConfig> {
	private static final long serialVersionUID = 473901162628181604L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@ManyToOne
	@ForeignKey(name = "FK_CFG_RRD_ARCHIVE_RRD_DEF")
	@JoinColumn(name = "RRD_DEF_CONFIG_ID")
	private RrdDefConfig rrdDef;
	@Enumerated(EnumType.STRING)
	private ArchiveType type;
	private float xff;
	private int step;
	@Column(name = "rrd_row")
	private int row;
	private String name;
	private String unit;

	private int length;
	@Enumerated(EnumType.STRING)
	private TimeType lengthType;
	private int frequency;
	@Enumerated(EnumType.STRING)
	private TimeType frequencyType;

	public RrdArchiveConfig() {
		type = ArchiveType.AVERAGE;
		xff = 0.5f;
	}

	public enum ArchiveType {
		MAXIMUM, MINIMUM, AVERAGE, LAST, FIRST, TOTAL
	}

	public enum TimeType {
		SECOND, MINUTE, HOUR, DAY, WEEK, MONTH, YEAR
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public RrdDefConfig getRrdDef() {
		return rrdDef;
	}

	public void setRrdDef(RrdDefConfig rrdDef) {
		this.rrdDef = rrdDef;
	}

	public ArchiveType getType() {
		return type;
	}

	public void setType(ArchiveType type) {
		this.type = type;
	}

	public float getXff() {
		return xff;
	}

	public void setXff(float xff) {
		this.xff = xff;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public TimeType getLengthType() {
		return lengthType;
	}

	public void setLengthType(TimeType lengthType) {
		this.lengthType = lengthType;
	}

	public int getFrequency() {
		return frequency;
	}

	public void setFrequency(int frequency) {
		this.frequency = frequency;
	}

	public TimeType getFrequencyType() {
		return frequencyType;
	}

	public void setFrequencyType(TimeType frequencyType) {
		this.frequencyType = frequencyType;
	}

	@Transient
	public static int translateTime(int number, TimeType type) {
		long result = 0;
		switch (type) {
		case SECOND:
			result = number;
			break;
		case MINUTE:
			result = number * ConstantUtil.MIN_SEC;
			break;
		case HOUR:
			result = number * ConstantUtil.HOUR_SEC;
			break;
		case DAY:
			result = number * ConstantUtil.DAY_SEC;
			break;
		case WEEK:
			result = number * ConstantUtil.WEEK_SEC;
			break;
		case MONTH:
			result = number * ConstantUtil.MONTH_SEC;
			break;
		case YEAR:
			result = number * ConstantUtil.YEAR_SEC;
			break;
		}
		return (int) result;
	}

	@Transient
	public boolean isLocked() {
		return rrdDef.isLocked();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(" [name=")
				.append(name).append(", type=").append(type).append(", xff=")
				.append(xff).append(", step=").append(step).append(", row=")
				.append(row).append("]");
		return sb.toString();
	}

	@Override
	public int compareTo(RrdArchiveConfig o) {
		if (o != null) {
			if (name != null && o.name != null) {
				return name.compareTo(o.name);
			}
		}
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + frequency;
		result = prime * result
				+ ((frequencyType == null) ? 0 : frequencyType.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + length;
		result = prime * result
				+ ((lengthType == null) ? 0 : lengthType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + row;
		result = prime * result + step;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + Float.floatToIntBits(xff);
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof RrdArchiveConfig) {
			RrdArchiveConfig dummy = (RrdArchiveConfig) o;
			return id == dummy.getId();
		}
		return false;
	}

	@Override
	public RrdArchiveConfig getBackup() {
		RrdArchiveConfig dummy = new RrdArchiveConfig();
		dummy.name = name;
		dummy.row = row;
		dummy.step = step;
		dummy.xff = xff;
		dummy.type = type;
		dummy.unit = unit;
		dummy.frequency = frequency;
		dummy.frequencyType = frequencyType;
		dummy.length = length;
		dummy.lengthType = lengthType;
		return dummy;
	}

	@Override
	public void update(RrdArchiveConfig dummy) {
		row = dummy.row;
		type = dummy.type;
		xff = dummy.xff;
		step = dummy.step;
		name = dummy.name;
		unit = dummy.unit;
		frequency = dummy.frequency;
		frequencyType = dummy.frequencyType;
		length = dummy.length;
		lengthType = dummy.lengthType;
	}
}
