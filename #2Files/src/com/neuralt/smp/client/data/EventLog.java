package com.neuralt.smp.client.data;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.notification.WarningLevel;

/**
 * A record of an internal event
 */

@Entity
@Table(name = "EVENTLOG")
public class EventLog implements Comparable<EventLog> {

	private static final Logger logger = LoggerFactory
			.getLogger(EventLog.class);

	@Id
	@GeneratedValue
	// TODO sequence
	private long id;
	@Enumerated(EnumType.STRING)
	@Column(name = "warningLevel")
	private WarningLevel level;
	private String eventName;
	@Enumerated(EnumType.STRING)
	private SourceType sourceType;
	// names of associated system/host/application, if any
	private String sysName;
	private String hostName;
	private String procName;
	@Index(name = "IDX_EVENTLOG_TIME")
	private Date eventTime;
	private String details;

	private transient String age;

	public EventLog() {
	}

	public EventLog(String eventName, WarningLevel level,
			SourceType sourceType, MonitoredEntity source, Date eventTime,
			String details) {
		this.eventName = eventName;
		this.level = level;
		this.eventTime = eventTime;
		this.details = details;
		this.sourceType = sourceType;

		switch (source.getEntityType()) {
		case SYSTEM:
			sysName = source.getName();
			break;
		case HOST:
			hostName = ((MonitoredHost) source).getDisplayableName();
			sysName = ((MonitoredHost) source).getSys().getName();
			break;
		case PROCESS:
			procName = source.getName();
			MonitoredHost host = ((MonitoredProcess) source).getHost();
			hostName = host.getDisplayableName();
			sysName = host.getSys().getName();
			break;
		default:
			logger.warn("unhandled source entity type: "
					+ source.getEntityType());
		}
	}

	public EventLog(String eventName, WarningLevel level,
			SourceType sourceType, MonitoredEntity source, long eventTime,
			String details) {
		this(eventName, level, sourceType, source, new Timestamp(eventTime),
				details);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setLevel(WarningLevel level) {
		this.level = level;
	}

	public String getSourceType() {
		return sourceType.name();
	}

	public void setSourceType(SourceType sourceType) {
		this.sourceType = sourceType;
	}

	public String getSource() {
		if (sourceType == null)
			return null;
		switch (sourceType) {
		case SMP:
			return AppConfig.getMyName();
		case SYSTEM:
			return sysName;
		case HOST:
			return hostName;
		case PROCESS:
			return hostName + " - " + procName;
		default:
			return "unknown";
		}
	}

	public String getSysName() {
		return sysName;
	}

	public void setSysName(String sysName) {
		this.sysName = sysName;
	}

	public String getHostName() {
		return hostName;
	}

	public void setHostName(String hostName) {
		this.hostName = hostName;
	}

	public String getProcName() {
		return procName;
	}

	public void setProcName(String procName) {
		this.procName = procName;
	}

	public String getEventName() {
		return eventName;
	}

	public void setEventName(String eventName) {
		this.eventName = eventName;
	}

	// public String getSource() {
	// return source;
	// }
	// public void setSource(String source) {
	// this.source = source;
	// }
	public WarningLevel getLevel() {
		return level;
	}

	public Date getEventTime() {
		return eventTime;
	}

	public void setEventTime(Date eventTime) {
		this.eventTime = eventTime;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	@Override
	public int compareTo(EventLog o) {
		return getEventTime().compareTo(o.getEventTime());
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("EventLog [id=").append(id).append(", severity=")
				.append(level).append(", sourceType=")
				.append(sourceType)
				// .append(", sysName=").append(sysName).append(", hostName=").append(hostName).append(", procName=").append(procName)
				.append(", source=").append(getSource()).append(", eventTime=")
				.append(eventTime).append(", details=").append(details)
				.append("]");
		return builder.toString();
	}

	// for consistency
	public String getDisplayableSource() {
		return getSource();
	}
}
