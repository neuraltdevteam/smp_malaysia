package com.neuralt.smp.client.data;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;

@Entity
@Table(name = "DATA_COLLECTION_PROCEDURE")
public class DataCollectionProcedure implements Serializable {
	private static final long serialVersionUID = -5071308276891172560L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne
	@ForeignKey(name = "FK_CFG_PROCEDURE_RRD_DEF")
	@JoinColumn(name = "RRD_DEF_CONFIG_ID")
	private RrdDefConfig rrdDef;
	@OneToOne
	@JoinColumn(name = "RRD_DS_CONFIG_ID")
	private RrdDsConfig ds;
	@ManyToOne
	@JoinColumn(name = "SNMP_TARGET_ID", nullable = true)
	private SnmpTarget snmp;
	@ManyToOne
	@JoinColumn(name = "SCRIPT_CONFIG_ID", nullable = true)
	private ScriptConfig script;
	@Enumerated(EnumType.STRING)
	private MethodType type;

	public enum MethodType {
		SNMP, SCRIPT
	}

	@Transient
	public boolean isValid() {
		if (type != null) {
			if (type.equals(MethodType.SCRIPT)) {
				return script != null;
			} else if (type.equals(MethodType.SNMP)) {
				return snmp != null;
			}
		}
		return false;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public RrdDsConfig getDs() {
		return ds;
	}

	public void setDs(RrdDsConfig ds) {
		this.ds = ds;
	}

	public SnmpTarget getSnmp() {
		return snmp;
	}

	public void setSnmp(SnmpTarget snmp) {
		this.snmp = snmp;
	}

	public ScriptConfig getScript() {
		return script;
	}

	public void setScript(ScriptConfig script) {
		this.script = script;
	}

	public MethodType getType() {
		return type;
	}

	public void setType(MethodType type) {
		this.type = type;
	}

	public RrdDefConfig getRrdDef() {
		return rrdDef;
	}

	public void setRrdDef(RrdDefConfig rrdDef) {
		this.rrdDef = rrdDef;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof DataCollectionProcedure) {
			DataCollectionProcedure dummy = (DataCollectionProcedure) o;
			return id == dummy.getId();
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(" [ds=").append(ds)
				.append(", type=").append(type).append("]");
		return sb.toString();
	}
}
