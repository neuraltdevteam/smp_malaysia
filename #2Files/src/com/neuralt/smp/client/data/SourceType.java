package com.neuralt.smp.client.data;

public enum SourceType {
	GENERAL, SMP, SYSTEM, HOST, PROCESS
}