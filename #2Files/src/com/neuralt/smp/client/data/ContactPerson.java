package com.neuralt.smp.client.data;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.neuralt.smp.config.model.AbstractContact;
import com.neuralt.smp.config.model.Editable;

@Entity
@Table(name = "CONTACT_PERSON")
public class ContactPerson implements Serializable, Comparable<ContactPerson>,
		AbstractContact, Editable<ContactPerson> {
	private static final long serialVersionUID = -5969589286400530776L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String email;
	private String phone;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ContactPerson) {
			ContactPerson dummy = (ContactPerson) o;
			return id == dummy.getId();
		}
		return false;
	}

	public ContactPerson getBackup() {
		ContactPerson dummy = new ContactPerson();
		dummy.setEmail(email);
		dummy.setName(name);
		dummy.setPhone(phone);
		return dummy;
	}

	@Override
	public int compareTo(ContactPerson contact) {
		if (contact != null) {
			if (name != null && contact.getName() != null) {
				return name.compareTo(contact.getName());
			}
		}
		return 0;
	}

	@Override
	public void update(ContactPerson dummy) {
		setEmail(dummy.getEmail());
		setPhone(dummy.getPhone());
		setName(dummy.getName());
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ContactPerson :[name").append(name).append(", phone=")
				.append(phone).append(", email=").append(email).append("]");
		return sb.toString();
	}
}