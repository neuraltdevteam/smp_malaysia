package com.neuralt.smp.client.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

/**
 * 
 * all entity classes should derive from this class
 * 
 * @author Kin Cheung
 *
 */
@MappedSuperclass
public abstract class PersistenceEntity implements Serializable {
	private static final long serialVersionUID = 1428932033006928584L;
	// @Column(name = "LAST_MODIFIED")
	// protected Date lastModified;
	// @Column(name = "CREATED_DATE")
	// protected Date dateCreated;
	//
	// @Column(name = "MODIFIED_BY")
	// protected String modifiedBy;
	// @Column(name = "CREATED_BY")
	// protected String createdBy;

	@Version
	@Column(name = "VERSION")
	protected int version;

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	// TODO
	// PreUpdate & PrePersist seem do not work with Session
	// @PreUpdate
	// @PrePersist
	// public void updateTimeStamps() {
	// lastModified = new Date();
	// // modifiedBy = TODO;
	// if (dateCreated==null) {
	// dateCreated = new Date();
	// }
	// if (createdBy==null) {
	// // createdBy = TODO;
	// }
	// }

}
