package com.neuralt.smp.client.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.hibernate.annotations.Index;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.OidMap.ObjectName;
import com.neuralt.smp.client.data.OidMap.ObjectNameMap;
import com.neuralt.smp.client.data.model.DisplayableLog;
import com.neuralt.smp.client.util.OidUtil;
import com.neuralt.smp.client.util.ValueMappingUtil;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.snmp.model.TrapRawObject;
import com.neuralt.smp.snmp.proxy.SvrSnmpConstants;
import com.neuralt.smp.snmp.util.MibFileLoader;
import com.neuralt.smp.util.StringUtil;

/**
 * This entity is used for holding incoming traps. All setters in this class
 * involve mapping of values, so avoid calling them other than initiating the
 * object.
 */
@Entity
@Table(name = "SNMP_TRAP")
public class SnmpTrap extends PersistenceEntity implements
		Comparable<DisplayableLog>, DisplayableLog {
	private static final long serialVersionUID = 8660889704262662727L;
	private static final Logger logger = LoggerFactory
			.getLogger(SnmpTrap.class);

	public static enum Direction {
		INCOMING, OUTGOING,
	}

	/*
	 * setOid will be called at the end of this method, since all mappings
	 * depend on the original OID.
	 */
	public SnmpTrap(TrapRawObject obj) {
		this.original = obj;
		this.variables = new TreeSet<Variable>();
		this.oid = obj.getOid().toString();
		this.trapAlarmLevel = StringUtil.isNullOrEmpty(obj.getTrapAlarmLevel()) ? WarningLevel.UNKNOWN
				.name() : obj.getTrapAlarmLevel();

		setTrapVersion(obj.getTrapVersion());
		String ip = StringUtil.parseIP(obj.getSourceIP());
		setSourceIp(ip);
		obj.setSourceIP(ip);
		if (SmpClient.instance.getIpHostMap().get(sourceIp) != null)
			setSource(SmpClient.instance.getIpHostMap().get(sourceIp)
					.getConfig().getName());
		else
			setSource(sourceIp);
		setAlarmDescription(obj.getAlarmDescription());
		setTime(obj.getTime());
		setLastCheckTime(obj.getTime());
		setTrapAppName(obj.getTrapAppName());

		setTrapAlarmId(obj.getTrapAlarmId());
		for (String key : obj.getVariables().keySet()) {
			Variable newVariable = new Variable();
			OID keyOid = new OID(key);
			boolean ok = true;
			if (OidUtil.getInstance().getFullOidMap().containsKey(oid())) {
				OidMap oidMap = OidUtil.getInstance().getFullOidMap()
						.get(oid());
				for (ObjectName objName : oidMap.getObjectNames()) {
					if (keyOid.startsWith(objName.getOid())
							&& !StringUtil
									.isNullOrEmpty(objName.getMappedOid())) {
						if (objName.getMappedOid().equals(
								SvrSnmpConstants.trapAlarmId)) {
							setTrapAlarmId(obj.getVariables().get(key));
							ok = false;
						} else if (objName.getMappedOid().equals(
								SvrSnmpConstants.trapAlarmLevel)) {
							setTrapAlarmLevel(obj.getVariables().get(key));
							ok = false;
						} else if (objName.getMappedOid().equals(
								SvrSnmpConstants.trapAppName)) {
							setTrapAppName(obj.getVariables().get(key));
							ok = false;
						} else if (objName.getMappedOid().equals(
								SvrSnmpConstants.trapDescription)) {
							setTrapDescription(obj.getVariables().get(key));
							ok = false;
						}
					}
				}
			} else if (keyOid.equals(SnmpConstants.snmpTrapAddress)) {
				// this is a forwarded trap
				// the source in here is already a proxy address - need to
				// change the IP
				this.sourceIp = obj.getVariables().get(key);
				this.original.setSourceIP(obj.getVariables().get(key));
				ok = false;
			}
			if (ok) {
				newVariable.setOid(key);

				boolean varOk = false;
				OID mappedOid = new OID(key);
				if (OidUtil.getInstance().getFullOidMap()
						.containsKey(mappedOid)) {
					if (!StringUtil.isNullOrEmpty(OidUtil.getInstance()
							.getFullOidMap().get(mappedOid).getMappedOid()))
						mappedOid = OidUtil.getInstance().getFullOidMap()
								.get(mappedOid).getMappedOid();
				}
				String trapName = MibFileLoader.getInstance()
						.getSvrTrapNameMap().get(mappedOid);
				if (!StringUtil.isNullOrEmpty(trapName)) {
					newVariable.setMappedOid(trapName);
					varOk = true;
				}
				// Most OID ends with .0 or .[index], so if we cannot find with
				// original one, we trim it once and find again.
				if (!varOk) {
					OID trimmed = mappedOid.trim();
					if (OidUtil.getInstance().getFullOidMap()
							.containsKey(trimmed)) {
						if (!StringUtil.isNullOrEmpty(OidUtil.getInstance()
								.getFullOidMap().get(trimmed).getMappedOid()))
							trimmed = OidUtil.getInstance().getFullOidMap()
									.get(trimmed).getMappedOid();
					}
					String trimmedTrapName = MibFileLoader.getInstance()
							.getAllTrapNameMap().get(trimmed);
					if (!StringUtil.isNullOrEmpty(trimmedTrapName)) {
						StringBuilder sb = new StringBuilder();
						sb.append(trimmedTrapName).append(".")
								.append(mappedOid.last());
						newVariable.setMappedOid(sb.toString());
						varOk = true;
					}
					// if (!varOk)
					// newVariable.setMappedOid(trimmed.toString());
				}
				if (!varOk)
					newVariable.setMappedOid(mappedOid.toString());

				newVariable.setValue(obj.getVariables().get(key));
				newVariable.setAlarm(this);
				this.variables.add(newVariable);
			}
		}

		setOid(obj.getOid().toString());
	}

	public SnmpTrap() {
		Date now = new Date();
		time = now;
		lastCheckTime = now;
		this.trapAlarmLevel = WarningLevel.UNKNOWN.name();
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String oid;
	private int trapVersion;
	private String trapName;
	private String source;
	private String sourceIp;
	@Column(length = 1024)
	private String trapDescription;
	@Index(name = "IDX_TRAP_TIME")
	private Date time;
	private Date lastCheckTime;
	@Transient
	private TrapRawObject original;

	/*
	 * The fields below are used to be matched with the trap names in MIB files.
	 * Therefore they have to be the same as those trap names, in order to use
	 * reflection when setting values.
	 */

	private String trapAppName;
	private String trapAlarmLevel;
	private String trapAlarmId;
	private String alarmDescription;

	@Enumerated(EnumType.ORDINAL)
	private Direction direction;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "alarm", orphanRemoval = true, fetch = FetchType.EAGER)
	private Set<Variable> variables;

	private transient String age;

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
		OID objOID = new OID(oid);
		if (OidUtil.getInstance().getFullOidMap().containsKey(oid())) {
			OidMap oidMap = OidUtil.getInstance().getFullOidMap().get(oid());
			if (!StringUtil.isNullOrEmpty(oidMap.getMappedOid())) {
				this.oid = oidMap.getMappedOid().toString();
			}
		}
		objOID = new OID(this.oid);
		if (MibFileLoader.getInstance().getSvrTrapDescMap().get(objOID) != null) {
			this.setTrapDescription(MibFileLoader.getInstance()
					.getSvrTrapDescMap().get(objOID));
			this.setTrapName(MibFileLoader.getInstance().getSvrTrapNameMap()
					.get(objOID));
		} else if (MibFileLoader.getInstance().getAllTrapDescMap().get(objOID) != null) {
			this.setTrapDescription(MibFileLoader.getInstance()
					.getAllTrapDescMap().get(objOID));
			this.setTrapName(MibFileLoader.getInstance().getAllTrapNameMap()
					.get(objOID));
		} else {
			this.setTrapName(objOID.toString());
		}
	}

	public void setOid(OID oid) {
		this.oid = oid.toString();
		if (OidUtil.getInstance().getFullOidMap().containsKey(oid())) {
			OidMap oidMap = OidUtil.getInstance().getFullOidMap().get(oid());
			if (!StringUtil.isNullOrEmpty(oidMap.getMappedOid())) {
				this.oid = oidMap.getMappedOid().toString();
			}
		}
		if (MibFileLoader.getInstance().getSvrTrapDescMap().get(oid) != null) {
			this.setTrapDescription(MibFileLoader.getInstance()
					.getSvrTrapDescMap().get(oid));
			this.setTrapName(MibFileLoader.getInstance().getSvrTrapNameMap()
					.get(oid));
		} else if (MibFileLoader.getInstance().getAllTrapDescMap().get(oid) != null) {
			this.setTrapDescription(MibFileLoader.getInstance()
					.getAllTrapDescMap().get(oid));
			this.setTrapName(MibFileLoader.getInstance().getAllTrapNameMap()
					.get(oid));
		} else {
			this.setTrapName(oid.toString());
		}
	}

	public Date getTime() {
		return time;
	}

	private void setTime(Date time) {
		this.time = time;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getTrapVersion() {
		return trapVersion;
	}

	public void setTrapVersion(int trapVersion) {
		this.trapVersion = trapVersion;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getSourceIp() {
		return sourceIp;
	}

	public void setSourceIp(String sourceIp) {
		this.sourceIp = sourceIp;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public String getTrapAppName() {
		return trapAppName;
	}

	public void setTrapAppName(String trapAppName) {
		this.trapAppName = trapAppName;
		if (OidUtil.getInstance().getFullOidMap().containsKey(oid())) {
			OidMap oidMap = OidUtil.getInstance().getFullOidMap().get(oid());
			for (ObjectName objName : oidMap.getObjectNames()) {
				if (objName.getMappedOid().equals(SvrSnmpConstants.trapAppName)) {
					for (ObjectNameMap objNameMap : oidMap.getObjectNameMaps()) {
						if (objNameMap.getOid().equals(objName.getOid())
								&& ValueMappingUtil.getInstance().isValueMatch(
										trapAppName, objNameMap.getValue())) {
							// if (objNameMap.getOid().equals(objName.getOid())
							// && objNameMap.getValue().equals(trapAppName)) {
							this.trapAppName = objNameMap.getMappedValue();
							logger.debug("trap app name changed");
							break;
						}
					}
				}
			}
		}
	}

	public String getTrapAlarmLevel() {
		return trapAlarmLevel;
	}

	public void setTrapAlarmLevel(String trapAlarmLevel) {
		this.trapAlarmLevel = trapAlarmLevel;
		if (OidUtil.getInstance().getFullOidMap().containsKey(oid())) {
			OidMap oidMap = OidUtil.getInstance().getFullOidMap().get(oid());
			// logger.debug("after entering if");
			// logger.debug("size of objnamemap:"+oidMap.getObjectNameMaps().size());
			for (ObjectName objName : oidMap.getObjectNames()) {
				if (objName.getMappedOid().equals(
						SvrSnmpConstants.trapAlarmLevel)) {
					for (ObjectNameMap objNameMap : oidMap.getObjectNameMaps()) {
						if (objNameMap.getOid().equals(objName.getOid())
								&& ValueMappingUtil.getInstance().isValueMatch(
										trapAlarmLevel, objNameMap.getValue())) {
							// if (objNameMap.getOid().equals(objName.getOid())
							// && objNameMap.getValue().equals(trapAlarmLevel))
							// {
							this.trapAlarmLevel = objNameMap.getMappedValue();
							logger.debug("alarm level changed");
						}
					}
				}
			}
		}
	}

	public String getTrapAlarmId() {
		return trapAlarmId;
	}

	public void setTrapAlarmId(String trapAlarmId) {
		this.trapAlarmId = trapAlarmId;
		if (OidUtil.getInstance().getFullOidMap().containsKey(oid())) {
			OidMap oidMap = OidUtil.getInstance().getFullOidMap().get(oid());
			for (ObjectName objName : oidMap.getObjectNames()) {
				if (objName.getMappedOid().equals(SvrSnmpConstants.trapAlarmId)) {
					for (ObjectNameMap objNameMap : oidMap.getObjectNameMaps()) {
						if (objNameMap.getOid().equals(objName.getOid())
								&& ValueMappingUtil.getInstance().isValueMatch(
										trapAlarmId, objNameMap.getValue())) {
							// if (objNameMap.getOid().equals(objName.getOid())
							// && objNameMap.getValue().equals(trapAlarmId)) {
							this.trapAlarmId = objNameMap.getMappedValue();
							logger.debug("alarm ID changed");
						}
					}
				}
			}
		}
	}

	public String getAlarmDescription() {
		return alarmDescription;
	}

	public void setAlarmDescription(String alarmDescription) {
		this.alarmDescription = alarmDescription;
	}

	public String getTrapName() {
		return trapName;
	}

	private void setTrapName(String trapName) {
		if (!StringUtil.isNullOrEmpty(trapName)) {
			this.trapName = trapName;
		} else {
			this.trapName = oid;
		}
	}

	public String getTrapDescription() {
		return trapDescription;
	}

	private void setTrapDescription(String trapDescription) {
		this.trapDescription = trapDescription;
		if (OidUtil.getInstance().getFullOidMap().containsKey(oid())) {
			OidMap oidMap = OidUtil.getInstance().getFullOidMap().get(oid());
			for (ObjectName objName : oidMap.getObjectNames()) {
				if (objName.getMappedOid().equals(
						SvrSnmpConstants.trapDescription)) {
					for (ObjectNameMap objNameMap : oidMap.getObjectNameMaps()) {
						if (objNameMap.getOid().equals(objName.getOid())
								&& ValueMappingUtil.getInstance().isValueMatch(
										trapDescription, objNameMap.getValue())) {
							// if (objNameMap.getOid().equals(objName.getOid())
							// && objNameMap.getValue().equals(trapDescription))
							// {
							this.trapDescription = objNameMap.getMappedValue();
							logger.debug("trap descr changed");
						}
					}
				}
			}
		}
	}

	public Set<Variable> getVariables() {
		return variables;
	}

	public void setVariables(Set<Variable> variables) {
		this.variables = new TreeSet<Variable>(variables);
	}

	public String getDisplayableSource() {
		if (this.source != null) {
			StringBuilder sb = new StringBuilder(source).append(" (")
					.append(sourceIp).append(")");
			return sb.toString();
		} else if (this.sourceIp != null) {
			return sourceIp;
		} else {
			return "-";
		}
	}

	@Override
	public int compareTo(DisplayableLog o) {
		if (time != null)
			return getTime().compareTo(o.getTime());
		return 0;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public List<Variable> getListVariables() {
		List<Variable> list = new ArrayList<Variable>(variables);
		Collections.sort(list);
		return list;
	}

	private OID oid() {
		if (!StringUtil.isNullOrEmpty(oid)) {
			return new OID(oid);
		}
		return null;
	}

	public Date getLastCheckTime() {
		return lastCheckTime;
	}

	public void setLastCheckTime(Date lastCheckTime) {
		this.lastCheckTime = lastCheckTime;
	}

	public TrapRawObject getOriginal() {
		if (original == null) {
			original = new TrapRawObject();
			original.setOid(oid());
			original.setTrapAlarmId(trapAlarmId);
			original.setTrapAlarmLevel(trapAlarmLevel);
			original.setAlarmDescription(alarmDescription);
			original.setTrapAppName(trapAppName);
			original.setSourceIP(sourceIp);
		}
		return original;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Alarm [id=").append(id).append(", oid=").append(oid)
				.append(", trapVersion=").append(trapVersion)
				.append(", trapName=").append(trapName).append(", source=")
				.append(source).append(", sourceIP=").append(sourceIp)
				.append(", trapDescription=").append(trapDescription)
				.append(", time=").append(time).append(", trapAppName=")
				.append(trapAppName).append(", trapAlarmLevel=")
				.append(trapAlarmLevel).append(", trapAlarmId=")
				.append(trapAlarmId).append(", alarmDescription=")
				.append(alarmDescription).append(", lastCheckTime=")
				.append(lastCheckTime).append("]");
		return builder.toString();
	}

	@Entity
	@Table(name = "VARIABLE")
	public static class Variable implements Serializable, Comparable<Variable> {
		private static final long serialVersionUID = -4250126891447236914L;
		@Id
		@GeneratedValue(strategy = GenerationType.AUTO)
		private long id;
		@ManyToOne
		@ForeignKey(name = "FK_VARIABLE_TRAP")
		@JoinColumn(name = "ALARM_ID")
		private SnmpTrap alarm;
		private String oid;
		private String mappedOid;
		private String value;

		public String getOid() {
			return oid;
		}

		public void setOid(String oid) {
			this.oid = oid;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}

		public SnmpTrap getAlarm() {
			return alarm;
		}

		public void setAlarm(SnmpTrap alarm) {
			this.alarm = alarm;
		}

		public long getId() {
			return id;
		}

		public void setId(long id) {
			this.id = id;
		}

		public String getMappedOid() {
			return mappedOid;
		}

		public void setMappedOid(String mappedOid) {
			this.mappedOid = mappedOid;
		}

		/*
		 * @Override public boolean equals(Object o) { if (o instanceof
		 * Variable) { Variable var = (Variable) o; return var.getId()==this.id;
		 * } return false; }
		 */
		@Override
		public int compareTo(Variable var) {
			return (new OID(oid).compareTo(new OID(var.getOid())));
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("Variable [id=").append(id).append(", alarm=")
					.append(alarm).append(", oid=").append(oid)
					.append(", value=").append(value).append("]");
			return sb.toString();
		}

	}

	@Override
	public String getLevel() {
		return getTrapAlarmLevel();
	}

	@Override
	public String getDescription() {
		return getAlarmDescription();
	}

	@Override
	public String getAppName() {
		return getTrapAppName();
	}

	@Override
	public String getEventName() {
		return getTrapName();
	}

	@Override
	public String getSourceType() {
		return "HOST";
	}

	@Override
	public boolean isTrap() {
		return true;
	}

}
