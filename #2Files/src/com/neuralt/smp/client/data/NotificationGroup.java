package com.neuralt.smp.client.data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.primefaces.model.DualListModel;

import com.neuralt.smp.client.data.config.AlarmDefinition;
import com.neuralt.smp.client.service.AlarmService;
import com.neuralt.smp.util.StringUtil;

@Entity
@Table(name = "NOTI_GROUP")
public class NotificationGroup implements Serializable,
		Comparable<NotificationGroup> {
	protected static final long serialVersionUID = -5997798100562804429L;

	public static enum EventType {
		NEW, UNATTENDED,
	}

	public static enum UnattendedTrigger {
		TIME, OCCURENCE, EITHER, BOTH
	}

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected long id;
	protected String name;
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	protected Set<ContactPerson> contacts;
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	protected Set<AlarmDefinition> alarms;
	@ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	protected Set<GroupSnmpConfig> snmpConfigs;
	@Enumerated(EnumType.STRING)
	protected EventType type;
	protected boolean sms;
	protected boolean email;
	protected boolean trap;
	protected boolean wildcard;
	private Long time;
	private Integer occurence;
	@Enumerated(EnumType.STRING)
	private UnattendedTrigger triggerer;
	@Transient
	protected List<AlarmDefinition> source;
	@Transient
	protected List<AlarmDefinition> target;
	@Transient
	protected DualListModel<AlarmDefinition> dualList;

	public void generateDualList() {
		source = new ArrayList<AlarmDefinition>();
		if (alarms != null) {
			target = getAlarmsAsList();
		} else {
			target = new ArrayList<AlarmDefinition>();
		}
		List<AlarmDefinition> list = AlarmService.instance
				.listAlarmDefinitions();
		for (AlarmDefinition def : list) {
			if (!target.contains(def)) {
				source.add(def);
			}
		}
		Collections.sort(source);
		Collections.sort(target);
		dualList = new DualListModel<AlarmDefinition>(source, target);
	}

	@Transient
	public List<AlarmDefinition> getAlarmsAsList() {
		if (alarms != null) {
			List<AlarmDefinition> list = new ArrayList<AlarmDefinition>(alarms);
			Collections.sort(list);
			return list;
		}
		return null;
	}

	@Transient
	public List<ContactPerson> getContactsAsList() {
		if (contacts != null) {
			List<ContactPerson> list = new ArrayList<ContactPerson>(contacts);
			Collections.sort(list);
			return list;
		}
		return null;
	}

	@Transient
	public List<GroupSnmpConfig> getSnmpConfigsAsList() {
		if (snmpConfigs != null) {
			List<GroupSnmpConfig> list = new ArrayList<GroupSnmpConfig>(
					snmpConfigs);
			Collections.sort(list);
			return list;
		}
		return null;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	@Transient
	public String getLabel() {
		if (!StringUtil.isNullOrEmpty(name)) {
			return wildcard ? name + " (*)" : name;
		}
		return "Alarm - " + type.name();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<ContactPerson> getContacts() {
		if (contacts == null) {
			contacts = new TreeSet<ContactPerson>();
		}
		return contacts;
	}

	public void setContacts(Set<ContactPerson> contacts) {
		this.contacts = contacts;
	}

	public EventType getType() {
		return type;
	}

	public void setType(EventType type) {
		this.type = type;
	}

	public List<AlarmDefinition> getSource() {
		return source;
	}

	public void setSource(List<AlarmDefinition> source) {
		this.source = source;
	}

	public List<AlarmDefinition> getTarget() {
		return target;
	}

	public void setTarget(List<AlarmDefinition> target) {
		this.target = target;
	}

	public DualListModel<AlarmDefinition> getDualList() {
		return dualList;
	}

	public void setDualList(DualListModel<AlarmDefinition> dualList) {
		this.dualList = dualList;
	}

	public Set<GroupSnmpConfig> getSnmpConfigs() {
		if (snmpConfigs == null) {
			snmpConfigs = new TreeSet<GroupSnmpConfig>();
		}
		return snmpConfigs;
	}

	public void setSnmpConfigs(Set<GroupSnmpConfig> snmpConfigs) {
		this.snmpConfigs = snmpConfigs;
	}

	public boolean isSms() {
		return sms;
	}

	public void setSms(boolean sms) {
		this.sms = sms;
	}

	public boolean isEmail() {
		return email;
	}

	public void setEmail(boolean email) {
		this.email = email;
	}

	public boolean isTrap() {
		return trap;
	}

	public void setTrap(boolean trap) {
		this.trap = trap;
	}

	public Long getTime() {
		return time;
	}

	public void setTime(Long time) {
		this.time = time;
	}

	public Integer getOccurence() {
		return occurence;
	}

	public void setOccurence(Integer occurence) {
		this.occurence = occurence;
	}

	public UnattendedTrigger getTriggerer() {
		return triggerer;
	}

	public void setTriggerer(UnattendedTrigger triggerer) {
		this.triggerer = triggerer;
	}

	// @Entity
	// @Table (name="TRAP_LIST")
	// public static class TrapWrapper implements Serializable,
	// Comparable<TrapWrapper> {
	// protected static final long serialVersionUID = 680674407558333292L;
	// @Id
	// @GeneratedValue(strategy = GenerationType.AUTO)
	// protected long id;
	// @ManyToOne
	// @ForeignKey(name="FK_CFG_TRAPLIST_NOTIFICATION")
	// @JoinColumn(name = "NOTIFICATION_GROUP_ID")
	// protected NotificationGroup notificationGroup;
	// protected OID oid;
	// protected int cooldown;
	//
	// public TrapWrapper(OID oid) {
	// this.oid = oid;
	// this.cooldown = ConstantUtil.MIN_MS*30;
	// }
	//
	// public long getId() {
	// return id;
	// }
	// public void setId(long id) {
	// this.id = id;
	// }
	// public NotificationGroup getNotificationGroup() {
	// return notificationGroup;
	// }
	// public void setNotificationGroup(NotificationGroup notificationGroup) {
	// this.notificationGroup = notificationGroup;
	// }
	// public OID getOid() {
	// return oid;
	// }
	// public void setOid(OID oid) {
	// this.oid = oid;
	// }
	// public int getCooldown() {
	// return cooldown;
	// }
	// public void setCooldown(int cooldown) {
	// this.cooldown = cooldown;
	// }
	//
	// @Override
	// public boolean equals(Object o) {
	// if (o instanceof TrapWrapper) {
	// TrapWrapper dummy = (TrapWrapper) o;
	// return id==dummy.getId();
	// }
	// return false;
	// }
	//
	// @Override
	// public int compareTo(TrapWrapper o) {
	// if (oid!=null && o.getOid()!=null) {
	// return oid.compareTo(o.getOid());
	// }
	// return 0;
	// }
	// }

	public Set<AlarmDefinition> getAlarms() {
		return alarms;
	}

	public void setAlarms(Set<AlarmDefinition> alarms) {
		this.alarms = alarms;
	}

	public boolean isWildcard() {
		return wildcard;
	}

	public void setWildcard(boolean wildcard) {
		this.wildcard = wildcard;
	}

	@Override
	public int compareTo(NotificationGroup group) {
		if (group != null)
			if (name != null && group.getName() != null)
				return name.compareTo(group.getName());
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof NotificationGroup) {
			NotificationGroup dummy = (NotificationGroup) o;
			return id == dummy.getId();
		}
		return false;
	}

}
