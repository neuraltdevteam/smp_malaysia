package com.neuralt.smp.client.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MIB_FLAG")
public class MibFlag implements Serializable, Comparable<MibFlag> {
	private static final long serialVersionUID = -1067264162271571790L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(unique = true)
	private String name;
	private boolean loaded;
	private String filepath;

	public MibFlag() {
	}

	public MibFlag(String name, boolean loaded, String filepath) {
		this.name = name;
		this.loaded = loaded;
		this.filepath = filepath;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isLoaded() {
		return loaded;
	}

	public void setLoaded(boolean loaded) {
		this.loaded = loaded;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public boolean equals(Object o) {
		if (o instanceof MibFlag) {
			return id == ((MibFlag) o).getId();
		}
		return false;
	}

	@Override
	public int compareTo(MibFlag o) {
		if (o != null) {
			if (o.getName() != null && name != null) {
				return name.compareTo(o.getName());
			}
		}
		return 0;
	}
}
