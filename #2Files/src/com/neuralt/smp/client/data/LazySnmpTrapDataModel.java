package com.neuralt.smp.client.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;

public class LazySnmpTrapDataModel extends LazyDataModel<SnmpTrap> {
	private static final long serialVersionUID = -745637973463870338L;

	private static final Logger logger = LoggerFactory
			.getLogger(LazySnmpTrapDataModel.class);
	private List<SnmpTrap> data;
	private MonitoredSystem system;
	private MonitoredHost host;
	private Date timeFrom;
	private Date timeTo;

	public LazySnmpTrapDataModel() {
	}

	@Override
	public SnmpTrap getRowData(String rowKey) {
		for (SnmpTrap alarm : data) {
			if (alarm.getId() == Integer.valueOf(rowKey)) {
				return alarm;
			}
		}
		return null;
	}

	public Object getRowKey(SnmpTrap alarm) {
		return alarm.getId();
	}

	@Override
	public List<SnmpTrap> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, String> filters) {
		data = new ArrayList<SnmpTrap>();
		/*
		 * try { if (host!=null) { data =
		 * DataAccess.alarmDao.getAlarmsByHost(filters, host.getName()); } else
		 * if (system!=null) { for (MonitoredHost host : system.getHosts()) {
		 * data.addAll(DataAccess.alarmDao.getAlarmsByHost(filters,
		 * host.getName())); } } else { data =
		 * DataAccess.alarmDao.getAllAlarms(); } } catch (Exception e) { // TODO
		 * Auto-generated catch block }
		 */
		// List<Alarm> data = DataAccess.alarmDao.getAlarmsByHost(filters);
		logger.info("before checking alarms");
		logger.info(filters.toString());

		if ((timeFrom == null) & (timeTo == null)) {
			try {
				if (host != null) {
					data = DataAccess.snmpTrapDao.getTrapsByHost(host
							.getConfig().getName(), filters);
				} else if (system != null) {
					data = DataAccess.snmpTrapDao.getTrapsBySystem(system,
							filters);
				} else {
					data = DataAccess.snmpTrapDao.getTrapsByHost(null, filters);
				}
			} catch (Exception e) {
				logger.warn("Getting alarm from DB failed:" + e);
			}
		} else {
			if ((timeFrom == null) || (timeTo == null)
					|| (timeFrom.compareTo(timeTo) <= 0)) {
				try {
					if (host != null) {
						data = DataAccess.snmpTrapDao.getTrapsByTimeAndHost(
								timeFrom, timeTo, host.getConfig().getName(),
								filters);
					} else if (system != null) {
						data = DataAccess.snmpTrapDao.getTrapsByTimeAndSystem(
								timeFrom, timeTo, system, filters);
					} else {
						data = DataAccess.snmpTrapDao.getTrapsByTimeAndHost(
								timeFrom, timeTo, null, filters);
					}
				} catch (Exception e) {
					logger.warn("Getting alarm from DB failed:" + e);
				}
			}
		}
		/*
		 * for (Alarm alarm : dataSource) { boolean match = true; //
		 * logger.info("checking for alarm:"+alarm); for (String filterProperty
		 * : filters.keySet()) {
		 * logger.info("filterProperty:"+filterProperty+":end"); try { String
		 * filterValue = filters.get(filterProperty);
		 * logger.info("filterValue:"+filterValue+":end"); Field field =
		 * alarm.getClass().getDeclaredField(filterProperty);
		 * field.setAccessible(true); String fieldValue =
		 * String.valueOf(field.get(alarm));
		 * logger.info("filterValue:"+filterValue+" fieldValue:"+fieldValue); if
		 * (!StringUtil.stringContainIgnoreCase(fieldValue, filterValue)) {
		 * match = false; break; } } catch (Exception e) { match = false; } } if
		 * (match) { data.add(alarm); } }
		 */
		// sort
		if (sortField != null) {
			Collections.sort(data, new SnmpTrapSorter(sortField, sortOrder));
		}

		// rowCount
		int dataSize = data.size();
		this.setRowCount(dataSize);

		// paginate
		if (dataSize > pageSize) {
			try {
				return data.subList(first, first + pageSize);
			} catch (IndexOutOfBoundsException e) {
				return data.subList(first, first + (dataSize % pageSize));
			}
		} else {
			return data;
		}
	}

	public void setSystem(MonitoredSystem system) {
		this.system = system;
	}

	public void setHost(MonitoredHost host) {
		this.host = host;
	}

	public void setTimeFrom(Date timeFrom) {
		this.timeFrom = timeFrom;
	}

	public void setTimeTo(Date timeTo) {
		this.timeTo = timeTo;
	}

	public List<SnmpTrap> getData() {
		return data;
	}

	public void setData(List<SnmpTrap> data) {
		this.data = data;
	}
}
