package com.neuralt.smp.client.data;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataCollectionProcedure.MethodType;
import com.neuralt.smp.config.model.Editable;

@Entity
@Table(name = "RRD_DS_CONFIG")
public class RrdDsConfig implements Serializable, Comparable<RrdDsConfig>,
		Editable<RrdDsConfig> {
	private static final long serialVersionUID = 8524805424587905789L;
	private static final Logger logger = LoggerFactory
			.getLogger(RrdDsConfig.class);
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	@Enumerated(EnumType.STRING)
	private DsType type;
	private long heartbeat;
	private Float minimum;
	private Float maximum;
	@ManyToOne
	@ForeignKey(name = "FK_CFG_RRD_DS_RRD_DEF")
	@JoinColumn(name = "RRD_DEF_CONFIG_ID")
	private RrdDefConfig rrdDef;

	@OneToOne(mappedBy = "ds")
	private DataCollectionProcedure dataCollectProc;

	public RrdDsConfig() {
		type = DsType.COUNTER;
		heartbeat = 20;
		minimum = Float.NaN;
		maximum = Float.NaN;
	}

	public enum DsType {
		COUNTER, GAUGE, ABSOLUTE, DERIVE
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public DsType getType() {
		return type;
	}

	public void setType(DsType type) {
		this.type = type;
	}

	public long getHeartbeat() {
		return heartbeat;
	}

	public void setHeartbeat(long heartbeat) {
		this.heartbeat = heartbeat;
	}

	public DataCollectionProcedure getDataCollectProc() {
		return dataCollectProc;
	}

	public String getDisplayableDataCollectProc() {
		String output = "";

		if (dataCollectProc == null) {
			dataCollectProc = DataAccess.dataCollectionProcedureDao
					.getMethodByRrdDsConfig(this);
		}

		if (dataCollectProc != null) {
			if (dataCollectProc.getType().equals(MethodType.SCRIPT)) {
				output = "SCRIPT - " + dataCollectProc.getScript().getName();
			} else if (dataCollectProc.getType().equals(MethodType.SNMP)) {
				output = "SNMP - " + dataCollectProc.getSnmp().getName();
			}
		}

		return output;
	}

	public void setDataCollectProc(DataCollectionProcedure dataSource) {
		this.dataCollectProc = dataSource;
	}

	// FIXME
	// As MYSQL cannot store NaN (and NaN is a needed value for RRD), we have to
	// add a translation like this
	public Float getMinimum() {
		// logger.debug(name+" getMinimum called:"+Math.abs((minimum /
		// Float.MAX_VALUE) - 0.9));
		if (Math.abs(minimum / Float.MAX_VALUE - 0.9) < 0.1)
			return Float.NaN;
		return minimum;
	}

	public void setMinimum(Float minimum) {
		if (minimum.equals(Float.NaN))
			minimum = Float.MAX_VALUE * 0.9f;
		this.minimum = minimum;
	}

	public Float getMaximum() {
		// logger.debug(name+" getMaximum called:"+Math.abs((maximum /
		// Float.MAX_VALUE) - 0.9));
		if (Math.abs(maximum / Float.MAX_VALUE - 0.9) < 0.1)
			return Float.NaN;
		return maximum;
	}

	public void setMaximum(Float maximum) {
		if (maximum.equals(Float.NaN))
			maximum = Float.MAX_VALUE * 0.9f;
		this.maximum = maximum;
	}

	public RrdDefConfig getRrdDef() {
		return rrdDef;
	}

	public void setRrdDef(RrdDefConfig rrdDef) {
		this.rrdDef = rrdDef;
	}

	@Transient
	public void setStandardGaugeParam() {
		minimum = Float.MAX_VALUE * 0.9f;
		maximum = Float.MAX_VALUE * 0.9f;
		type = DsType.GAUGE;
		heartbeat = 20;
	}

	@Transient
	public boolean isLocked() {
		return rrdDef.isLocked();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(" [id=").append(id)
				.append(", name=").append(name).append(" ,type=").append(type)
				.append("]");
		return sb.toString();
	}

	@Override
	public int compareTo(RrdDsConfig o) {
		if (o != null) {
			if (name != null && o.name != null) {
				return name.compareTo(o.name);
			}
		}
		return 0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (heartbeat ^ (heartbeat >>> 32));
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((maximum == null) ? 0 : maximum.hashCode());
		result = prime * result + ((minimum == null) ? 0 : minimum.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof RrdDsConfig) {
			RrdDsConfig dummy = (RrdDsConfig) o;
			return id == dummy.getId();
		}
		return false;
	}

	@Override
	public RrdDsConfig getBackup() {
		RrdDsConfig dummy = new RrdDsConfig();
		dummy.heartbeat = heartbeat;
		dummy.maximum = maximum;
		dummy.minimum = minimum;
		dummy.type = type;
		dummy.name = name;
		return dummy;
	}

	@Override
	public void update(RrdDsConfig dummy) {
		this.type = dummy.type;
		this.heartbeat = dummy.heartbeat;
		this.maximum = dummy.maximum;
		this.minimum = dummy.minimum;
		this.name = dummy.name;
	}
}
