package com.neuralt.smp.client.data;

public class DataPoint {

	private String name;
	private Double value;
	private long time;

	public DataPoint(String name, Double value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof DataPoint) {
			DataPoint dummy = (DataPoint) o;
			return name.equals(dummy.getName());
		}
		return false;
	}
}
