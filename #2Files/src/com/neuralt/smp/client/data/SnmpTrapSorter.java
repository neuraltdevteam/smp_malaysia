package com.neuralt.smp.client.data;

import java.lang.reflect.Field;
import java.util.Comparator;
import java.util.Date;

import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.notification.WarningLevel;

public class SnmpTrapSorter implements Comparator<SnmpTrap> {

	private static final Logger logger = LoggerFactory
			.getLogger(SnmpTrapSorter.class);
	private String sortField;
	private SortOrder sortOrder;

	public SnmpTrapSorter(String sortField, SortOrder sortOrder) {
		this.sortField = sortField;
		this.sortOrder = sortOrder;
	}

	@Override
	public int compare(SnmpTrap one, SnmpTrap two) {
		int result = 0;
		try {
			Field field = SnmpTrap.class.getDeclaredField(sortField);
			field.setAccessible(true);
			// suppose sort order is ascending
			if (sortOrder != SortOrder.UNSORTED) {
				if (field.getName().equals("time"))
					result = ((Date) field.get(one)).compareTo(((Date) field
							.get(two)));
				else if (field.getName().equals("trapAlarmLevel"))
					result = (WarningLevel.valueOf((String) field.get(one)))
							.compareTo(WarningLevel.valueOf((String) field
									.get(two)));
				else
					result = ((String) field.get(one))
							.compareTo(((String) field.get(two)));
			}
			// else reverse the sign of result
			if (sortOrder == SortOrder.DESCENDING) {
				result = -result;
			}
		} catch (SecurityException e) {
			logger.debug("failed in comparing SnmpTrap", e);
		} catch (NoSuchFieldException e) {
			logger.debug("failed in comparing SnmpTrap", e);
		} catch (IllegalArgumentException e) {
			logger.debug("failed in comparing SnmpTrap", e);
		} catch (IllegalAccessException e) {
			logger.debug("failed in comparing SnmpTrap", e);
		}
		return result;
	}

}
