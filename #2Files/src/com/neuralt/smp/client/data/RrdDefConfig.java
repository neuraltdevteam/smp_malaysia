package com.neuralt.smp.client.data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.neuralt.smp.client.data.RrdArchiveConfig.ArchiveType;
import com.neuralt.smp.client.data.RrdArchiveConfig.TimeType;
import com.neuralt.smp.config.model.Editable;
import com.neuralt.smp.util.StringUtil;

@Entity
@Table(name = "RRD_DEF_CONFIG")
public class RrdDefConfig implements Serializable, Comparable<RrdDefConfig>,
		Editable<RrdDefConfig> {
	private static final long serialVersionUID = -7035220365777879673L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String path;
	private int step;
	private boolean locked;
	private boolean enabled;

	private Timestamp lastUpdate;
	private Timestamp createDate;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "rrdDef", orphanRemoval = true, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 100)
	private Set<RrdDsConfig> rrdDsConfigs;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "rrdDef", orphanRemoval = true, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 100)
	private Set<RrdArchiveConfig> rrdArchiveConfigs;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "rrdDef", orphanRemoval = true, fetch = FetchType.EAGER)
	@Fetch(FetchMode.SELECT)
	@BatchSize(size = 100)
	private Set<DataCollectionProcedure> procedures;

	@Transient
	private long rejectCount;
	@Transient
	private long acceptCount;
	@Transient
	private long startTime;

	public RrdDefConfig() {
		step = 10;
		rrdDsConfigs = new TreeSet<RrdDsConfig>();
		rrdArchiveConfigs = new TreeSet<RrdArchiveConfig>();
		procedures = new TreeSet<DataCollectionProcedure>();
	}

	/**
	 * This temporary method is used for removing data source objects. The
	 * getRrdDsConfigs().remove() seems not working and is under reviewed.
	 * 
	 * @param ds
	 *            the data source object to be removed
	 * @return true if the object is successfully removed
	 */
	@Transient
	public boolean removeDs(RrdDsConfig ds) {
		RrdDsConfig dummy = null;
		for (RrdDsConfig temp : rrdDsConfigs) {
			if (temp.equals(ds)) {
				dummy = temp;
				break;
			}
		}
		if (dummy != null)
			return rrdDsConfigs.remove(dummy);
		else
			return false;
	}
	
	/**
	 * This temporary method is used for removing data source objects. The
	 * getRrdArchiveConfigs().remove() seems not working and is under reviewed.
	 * 
	 * @param ds
	 *            the data source object to be removed
	 * @return true if the object is successfully removed
	 */
	@Transient
	public boolean removeArchive(RrdArchiveConfig archive) {
		RrdArchiveConfig dummy = null;
		for (RrdArchiveConfig temp : rrdArchiveConfigs) {
			if (temp.equals(archive)) {
				dummy = temp;
				break;
			}
		}
		if (dummy != null)
			return rrdArchiveConfigs.remove(dummy);
		else
			return false;
	}

	@Transient
	public boolean containDs(RrdDsConfig ds) {
		return rrdDsConfigs.contains(ds);
	}

	@Transient
	public boolean containArchive(RrdArchiveConfig archive) {
		return rrdArchiveConfigs.contains(archive);
	}
	
	@Transient
	public static List<RrdArchiveConfig> generateDefaultArchives() {
		List<RrdArchiveConfig> list = new ArrayList<RrdArchiveConfig>();
		list.add(createHourlyArchiveTemplate());
		list.add(createDailyArchiveTemplate());
		list.add(createWeeklyArchiveTemplate());
		list.add(createMonthlyArchiveTemplate());
		list.add(createYearlyArchiveTemplate());
		return list;
	}

	@Transient
	public void generateDefaultHostStatArchives() {
		List<RrdArchiveConfig> list = new ArrayList<RrdArchiveConfig>();
		list.add(createHourlyArchiveTemplate());
		list.add(createDailyArchiveTemplate());
		rrdArchiveConfigs.addAll(list);
	}

	@Transient
	public void createArchive(int step, int row, ArchiveType type) {
		RrdArchiveConfig archive = new RrdArchiveConfig();
		archive.setType(type);
		archive.setRow(row);
		archive.setStep(step);
		rrdArchiveConfigs.add(archive);
	}

	@Transient
	public static RrdArchiveConfig createHourlyArchiveTemplate() {
		RrdArchiveConfig hour = new RrdArchiveConfig();
		hour.setName("1hr AVG");
		hour.setFrequency(1);
		hour.setFrequencyType(TimeType.MINUTE);
		hour.setLength(1);
		hour.setLengthType(TimeType.HOUR);

		hour.setStep(6);
		hour.setRow(60);
		return hour;
	}

	@Transient
	public static RrdArchiveConfig createDailyArchiveTemplate() {
		RrdArchiveConfig day = new RrdArchiveConfig();
		day.setName("1day AVG");
		day.setFrequency(20);
		day.setFrequencyType(TimeType.MINUTE);
		day.setLength(1);
		day.setLengthType(TimeType.DAY);

		day.setStep(120);
		day.setRow(72);
		return day;
	}

	@Transient
	public static RrdArchiveConfig createWeeklyArchiveTemplate() {
		RrdArchiveConfig week = new RrdArchiveConfig();
		week.setName("1wk AVG");
		week.setFrequency(2);
		week.setFrequencyType(TimeType.HOUR);
		week.setLength(1);
		week.setLengthType(TimeType.WEEK);

		week.setStep(720);
		week.setRow(24 * 7);
		return week;
	}

	@Transient
	public static RrdArchiveConfig createMonthlyArchiveTemplate() {
		RrdArchiveConfig month = new RrdArchiveConfig();
		month.setName("1mth AVG");
		month.setFrequency(4);
		month.setFrequencyType(TimeType.HOUR);
		month.setLength(1);
		month.setLengthType(TimeType.MONTH);

		month.setStep(1440);
		month.setRow(24 * 30);
		return month;
	}

	@Transient
	public static RrdArchiveConfig createYearlyArchiveTemplate() {
		RrdArchiveConfig year = new RrdArchiveConfig();
		year.setName("1yr AVG");
		year.setFrequency(1);
		year.setFrequencyType(TimeType.DAY);
		year.setLength(1);
		year.setLengthType(TimeType.YEAR);

		year.setStep(8640);
		year.setRow(365);
		return year;
	}

	@Transient
	public RrdArchiveConfig getArchive(String name) {
		for (RrdArchiveConfig archive : rrdArchiveConfigs) {
			if (archive.getName().equals(name)) {
				return archive;
			}
		}
		return null;
	}

	@Transient
	public RrdArchiveConfig getArchive(ArchiveType type, int step) {
		for (RrdArchiveConfig archive : rrdArchiveConfigs) {
			if (archive.getType().equals(type) && archive.getStep() == step) {
				return archive;
			}
		}
		return null;
	}

	@Transient
	public RrdDsConfig getDs(String name) {
		for (RrdDsConfig ds : rrdDsConfigs) {
			if (ds.getName().equals(name)) {
				return ds;
			}
		}
		return null;
	}

	@Transient
	public String getEntityType() {
		return "Data Definition";
	}

	@Transient
	public List<RrdDsConfig> getRrdDsConfigsAsList() {
		List<RrdDsConfig> list = new ArrayList<RrdDsConfig>();
		if (rrdDsConfigs != null) {
			list = new ArrayList<RrdDsConfig>(rrdDsConfigs);
			Collections.sort(list);
		}
		return list;
	}

	@Transient
	public List<RrdArchiveConfig> getRrdArchiveConfigsAsList() {
		List<RrdArchiveConfig> list = new ArrayList<RrdArchiveConfig>();
		if (rrdArchiveConfigs != null) {
			list = new ArrayList<RrdArchiveConfig>(rrdArchiveConfigs);
			Collections.sort(list);
		}
		return list;
	}

	@Override
	public int compareTo(RrdDefConfig o) {
		if (o != null) {
			if (name != null && o.getName() != null) {
				return name.compareTo(o.getName());
			}
		}
		return 0;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof RrdDefConfig) {
			RrdDefConfig dummy = (RrdDefConfig) o;
			return id == dummy.getId();
		}
		return false;
	}

	public void incReject() {
		++rejectCount;
	}

	public long getRejectCount() {
		return rejectCount;
	}

	public void incAccept() {
		++acceptCount;
	}

	public long getAcceptCount() {
		return acceptCount;
	}

	public String getRunningTime() {
		if (startTime == 0) {
			startTime = System.currentTimeMillis();
		}
		return StringUtil.millisecToTimeticksStr(System.currentTimeMillis()
				- startTime);
	}

	public float getAcceptRatio() {
		if (acceptCount + rejectCount != 0) {
			return (float) acceptCount / (acceptCount + rejectCount);
		}
		return 0;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public Set<RrdDsConfig> getRrdDsConfigs() {
		return rrdDsConfigs;
	}

	public void setRrdDsConfigs(Set<RrdDsConfig> rrdDsConfigs) {
		this.rrdDsConfigs = rrdDsConfigs;
	}

	public Set<RrdArchiveConfig> getRrdArchiveConfigs() {
		return rrdArchiveConfigs;
	}

	public void setRrdArchiveConfigs(Set<RrdArchiveConfig> rrdArchiveConfigs) {
		this.rrdArchiveConfigs = rrdArchiveConfigs;
	}

	public Set<DataCollectionProcedure> getProcedures() {
		return procedures;
	}

	public void setProcedures(Set<DataCollectionProcedure> procedures) {
		this.procedures = procedures;
	}

	public Timestamp getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Timestamp lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	@Override
	public RrdDefConfig getBackup() {
		RrdDefConfig dummy = new RrdDefConfig();
		dummy.setLocked(locked);
		dummy.setName(name);
		dummy.setStep(step);
		dummy.setPath(path);
		dummy.setEnabled(enabled);
		return dummy;
	}

	@Override
	public void update(RrdDefConfig dummy) {
		// fixed && running will be set separately
		setName(dummy.name);
		setStep(dummy.step);
		setPath(dummy.path);
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(this.getClass().getSimpleName()).append(" :[name=")
				.append(name).append(", path=").append(path).append(", step=")
				.append(step).append(", locked=").append(locked)
				.append(", enabled=").append(enabled).append(", ds count=")
				.append(rrdDsConfigs.size()).append(", archive count=")
				.append(rrdArchiveConfigs.size()).append(", procedure count=")
				.append(procedures.size()).append("]");
		return sb.toString();
	}
}
