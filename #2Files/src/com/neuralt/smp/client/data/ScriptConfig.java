package com.neuralt.smp.client.data;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.ForeignKey;

import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.model.Editable;

@Entity
@Table(name = "SCRIPT_CONFIG")
public class ScriptConfig implements Serializable, Editable<ScriptConfig>,
		Comparable<ScriptConfig> {
	private static final long serialVersionUID = -1160527394696228086L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String path;

	@ManyToOne
	@ForeignKey(name = "FK_CFG_SCRIPT_HOST")
	@JoinColumn(name = "HOST_CONFIG_ID")
	private HostConfig host;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public HostConfig getHost() {
		return host;
	}

	public void setHost(HostConfig host) {
		this.host = host;
	}

	@Transient
	public boolean isLocked() {
		List<DataCollectionProcedure> procedures = DataAccess.dataCollectionProcedureDao
				.getMethodByScriptConfig(this);
		for (DataCollectionProcedure proc : procedures) {
			if (proc.getDs().isLocked()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof ScriptConfig) {
			ScriptConfig dummy = (ScriptConfig) o;
			return id == dummy.getId();
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("ScriptConfig :[name=").append(name).append(", path=")
				.append(path).append(", host=").append(host.getName())
				.append("]");
		return sb.toString();
	}

	@Override
	public ScriptConfig getBackup() {
		ScriptConfig dummy = new ScriptConfig();
		dummy.setName(name);
		dummy.setPath(path);
		return dummy;
	}

	@Override
	public void update(ScriptConfig dummy) {
		setName(dummy.getName());
		setPath(dummy.getPath());
	}

	@Override
	public int compareTo(ScriptConfig script) {
		if (script != null) {
			if (name != null && script.getName() != null) {
				return name.compareTo(script.getName());
			}
		}
		return 0;
	}
}
