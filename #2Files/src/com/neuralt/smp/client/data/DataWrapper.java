package com.neuralt.smp.client.data;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class DataWrapper implements Serializable {
	private static final long serialVersionUID = 4632721512076623650L;

	public static DataWrapper createDataWrapper() {
		DataWrapper wrapper = new DataWrapper();
		wrapper.setPoints(new CopyOnWriteArrayList<DataPoint>());
		return wrapper;
	}

	private long time;
	private List<DataPoint> points;

	public long getTime() {
		return time;
	}

	public void setTime(long time) {
		this.time = time;
	}

	public List<DataPoint> getPoints() {
		return points;
	}

	public void setPoints(List<DataPoint> points) {
		this.points = points;
	}

	public boolean addDataPoint(DataPoint dp) {
		boolean ok = false;
		if (points != null) {
			ok = true;
			for (DataPoint temp : points) {
				if (temp.getName().equals(dp.getName())) {
					ok = false;
					break;
				}
			}
			if (ok) {
				points.add(dp);
			}
		}
		return ok;
	}
}
