package com.neuralt.smp.client.data;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.data.model.DisplayableLog;

@Deprecated
public class LazyDisaplayableLogDataModel extends LazyDataModel<DisplayableLog> {
	private static final long serialVersionUID = -3782159286659693956L;
	private static final Logger logger = LoggerFactory
			.getLogger(LazyDisaplayableLogDataModel.class);
	private List<DisplayableLog> data;
	private MonitoredSystem system;
	private MonitoredHost host;
	private Date timeFrom;
	private Date timeTo;

	public LazyDisaplayableLogDataModel() {
	}

	/*
	 * @Override public void setRowIndex(int rowIndex) {
	 * 
	 * // The following is in ancestor (LazyDataModel): // this.rowIndex =
	 * rowIndex == -1 ? rowIndex : (rowIndex % pageSize);
	 * 
	 * if (rowIndex == -1 || getPageSize() == 0) { super.setRowIndex(-1); } else
	 * super.setRowIndex(rowIndex % getPageSize()); }
	 */
	@Override
	public DisplayableLog getRowData(String rowKey) {
		for (DisplayableLog log : data) {
			if (log.getId() == Integer.valueOf(rowKey)) {
				return log;
			}
		}
		return null;
	}

	public Object getRowKey(DisplayableLog log) {
		return log.getId();
	}

	@Override
	public List<DisplayableLog> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, String> filters) {
		if (logger.isDebugEnabled()) {
			logger.debug("displayable log lazy model load called");
		}
		data = new ArrayList<DisplayableLog>();
		List<SnmpTrap> alarmList = new ArrayList<SnmpTrap>();
		List<EventLog> evtLogList = new ArrayList<EventLog>();
		/*
		 * try { if (host!=null) { data =
		 * DataAccess.alarmDao.getAlarmsByHost(filters, host.getName()); } else
		 * if (system!=null) { for (MonitoredHost host : system.getHosts()) {
		 * data.addAll(DataAccess.alarmDao.getAlarmsByHost(filters,
		 * host.getName())); } } else { data =
		 * DataAccess.alarmDao.getAllAlarms(); } } catch (Exception e) { // TODO
		 * Auto-generated catch block }
		 */
		// List<Alarm> data = DataAccess.alarmDao.getAlarmsByHost(filters);
		if (logger.isInfoEnabled()) {
			logger.info("before checking alarms");
			logger.info(filters.toString());
		}
		if ((timeFrom == null) & (timeTo == null)) {
			try {
				if (host != null) {
					alarmList = DataAccess.snmpTrapDao.getTrapsByHost(host
							.getConfig().getName(), filters);
					evtLogList = DataAccess.eventLogDao.getEventLogs(null, host
							.getConfig().getName(), null, null, null, null,
							null);
				} else if (system != null) {
					alarmList = DataAccess.snmpTrapDao.getTrapsBySystem(system,
							filters);
					evtLogList = DataAccess.eventLogDao.getEventLogs(host
							.getSys().getConfig().getName(), null, null, null,
							null, null, null);
				} else {
					alarmList = DataAccess.snmpTrapDao.getTrapsByHost(null,
							filters);
					evtLogList = DataAccess.eventLogDao.getEventLogs(null,
							null, null, null, null, null, null);
				}
			} catch (Exception e) {
				if (logger.isWarnEnabled())
					logger.warn("Getting data from DB failed:" + e);
			}
		} else {
			if ((timeFrom == null) || (timeTo == null)
					|| (timeFrom.compareTo(timeTo) <= 0)) {
				try {
					if (host != null) {
						alarmList = DataAccess.snmpTrapDao
								.getTrapsByTimeAndHost(timeFrom, timeTo, host
										.getConfig().getName(), filters);
						evtLogList = DataAccess.eventLogDao.getEventLogs(host
								.getSys().getConfig().getName(), host
								.getConfig().getName(), null, new Timestamp(
								timeFrom.getTime()),
								new Timestamp(timeTo.getTime()), null, null);
					} else if (system != null) {
						alarmList = DataAccess.snmpTrapDao
								.getTrapsByTimeAndSystem(timeFrom, timeTo,
										system, filters);
						evtLogList = DataAccess.eventLogDao.getEventLogs(host
								.getSys().getConfig().getName(), null, null,
								new Timestamp(timeFrom.getTime()),
								new Timestamp(timeTo.getTime()), null, null);
					} else {
						alarmList = DataAccess.snmpTrapDao
								.getTrapsByTimeAndHost(timeFrom, timeTo, null,
										filters);
						evtLogList = DataAccess.eventLogDao.getEventLogs(null,
								null, null, new Timestamp(timeFrom.getTime()),
								new Timestamp(timeTo.getTime()), null, null);
					}
				} catch (Exception e) {
					if (logger.isWarnEnabled())
						logger.warn("Getting data from DB failed:" + e);
				}
			}
		}
		/*
		 * for (Alarm alarm : dataSource) { boolean match = true; //
		 * logger.info("checking for alarm:"+alarm); for (String filterProperty
		 * : filters.keySet()) {
		 * logger.info("filterProperty:"+filterProperty+":end"); try { String
		 * filterValue = filters.get(filterProperty);
		 * logger.info("filterValue:"+filterValue+":end"); Field field =
		 * alarm.getClass().getDeclaredField(filterProperty);
		 * field.setAccessible(true); String fieldValue =
		 * String.valueOf(field.get(alarm));
		 * logger.info("filterValue:"+filterValue+" fieldValue:"+fieldValue); if
		 * (!StringUtil.stringContainIgnoreCase(fieldValue, filterValue)) {
		 * match = false; break; } } catch (Exception e) { match = false; } } if
		 * (match) { data.add(alarm); } }
		 */
		// sort
		for (SnmpTrap alarm : alarmList) {
			data.add(alarm);
		}
		// for (EventLog log : evtLogList) {
		// data.add(log);
		// }

		if (sortField != null) {
			Collections.sort(data, new DisplayableLogSorter(sortField,
					sortOrder));
		}

		// rowCount
		int dataSize = data.size();
		this.setRowCount(dataSize);

		// paginate
		if (dataSize > pageSize) {
			try {
				return data.subList(first, first + pageSize);
			} catch (IndexOutOfBoundsException e) {
				return data.subList(first, first + (dataSize % pageSize));
			}
		} else {
			return data;
		}
	}

	public void setSystem(MonitoredSystem system) {
		this.system = system;
	}

	public void setHost(MonitoredHost host) {
		this.host = host;
	}

	public void setTimeFrom(Date timeFrom) {
		this.timeFrom = timeFrom;
	}

	public void setTimeTo(Date timeTo) {
		this.timeTo = timeTo;
	}

	public List<DisplayableLog> getData() {
		return data;
	}

	public void setData(List<DisplayableLog> data) {
		this.data = data;
	}
}
