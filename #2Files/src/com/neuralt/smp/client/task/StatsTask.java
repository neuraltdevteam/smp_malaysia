package com.neuralt.smp.client.task;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.data.dao.RawDAO;
import com.neuralt.smp.client.data.stats.HostStats;
import com.neuralt.smp.client.data.stats.ProcessStats;

public class StatsTask implements Runnable, Serializable {
	private static final long serialVersionUID = -669348776558702917L;

	private static final Logger logger = LoggerFactory
			.getLogger(StatsTask.class);

	public static long HOURLY_MS = 3600 * 1000;
	public static long FIVEMIN_MS = 300 * 1000;

	private MonitoredHost host;
	private long intervalMs;
	private long nextOpTime;
	private RawDAO dao;

	public StatsTask(MonitoredHost host, long intervalMs, boolean runImmediately) {
		this.host = host;
		this.intervalMs = intervalMs;
		nextOpTime = runImmediately ? 0 : System.currentTimeMillis()
				+ intervalMs;
		dao = new RawDAO(true);
	}

	@Override
	public void run() {
		long now = System.currentTimeMillis();
		if (now > nextOpTime) {
			logger.info("begin computing host stats for " + host.getName());
			HostStats stats = host.computeAverageStats(intervalMs);
			if (!stats.isEmpty()) {
				dao.save(stats);
			}

			List<ProcessStats> pStatsList = new LinkedList<ProcessStats>();

			for (MonitoredProcess proc : host.getProcesses()) {
				// TODO skip processes that has been down for longer than
				// intervalMs
				ProcessStats pStats = proc.computeAverageStats(intervalMs);
				if (!pStats.isEmpty()) {
					pStatsList.add(pStats);
				}
			}
			dao.batchPersist(pStatsList);

			nextOpTime = now + intervalMs;
			if (logger.isInfoEnabled()) {
				Timestamp nextTime = new Timestamp(nextOpTime);
				logger.info("done computing host stats, next computation at: "
						+ nextTime);
			}
		}
	}

}
