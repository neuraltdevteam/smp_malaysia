package com.neuralt.smp.client.task;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.EventLog;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.util.ConstantUtil;

public class DBHousekeepTask extends Thread {
	private static final String MY_NAME = "DBHousekeepTask";
	private static final Logger logger = LoggerFactory
			.getLogger(DBHousekeepTask.class);
	private static final long interval = AppConfig.getDBRecordMaintain()
			* ConstantUtil.DAY_MS;
	private boolean quit;

	public DBHousekeepTask() {
		super(MY_NAME);
	}

	@Override
	public void run() {
		while (!quit) {
			clearAlarmRecord();
			clearEventLog();
			try {
				Thread.sleep(ConstantUtil.DAY_MS);
			} catch (InterruptedException e) {
				logger.info("{} was waken", MY_NAME);
			}
		}

		logger.info("{} is quitting...", MY_NAME);
	}

	private void clearAlarmRecord() {
		Date time = new Date();
		long timeBeforeInterval = time.getTime() - interval;
		time.setTime(timeBeforeInterval);
		List<SnmpTrap> list = new CopyOnWriteArrayList<SnmpTrap>();
		try {
			list = DataAccess.snmpTrapDao.getOldTraps(time);
			while ((list != null) && (list.size() != 0)) {
				DataAccess.snmpTrapDao.batchDelete(list);
				list = DataAccess.snmpTrapDao.getOldTraps(time);
			}
		} catch (Exception e) {
			logger.error("Access DB failed:" + e);
		}
	}

	private void clearEventLog() {
		Date time = new Date();
		long timeBeforeInterval = time.getTime() - interval;
		Timestamp stamp = new Timestamp(timeBeforeInterval);
		List<EventLog> list = new CopyOnWriteArrayList<EventLog>();
		try {
			list = DataAccess.eventLogDao.getEventLogs(null, null, null, null,
					stamp, null, AppConfig.getDBMaxResult());
			while ((list != null) && (list.size() != 0)) {
				DataAccess.eventLogDao.batchDelete(list);
				list = DataAccess.eventLogDao.getEventLogs(null, null, null,
						null, stamp, null, AppConfig.getDBMaxResult());
			}
		} catch (Exception e) {
			logger.error("Access DB failed:" + e);
		}
	}

	public void quit() {
		quit = true;
		this.interrupt();
	}
}
