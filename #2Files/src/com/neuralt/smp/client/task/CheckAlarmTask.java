package com.neuralt.smp.client.task;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.AlarmNotifier;
import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.data.NotificationGroup.EventType;
import com.neuralt.smp.client.data.config.AlarmDefinition;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.util.ConstantUtil;

public class CheckAlarmTask extends Thread {

	private static final String MY_NAME = "CHECK_ALARM_TASK";
	private static final Logger logger = LoggerFactory
			.getLogger(CheckAlarmTask.class);
	private static final long interval = AppConfig.getDBAlarmCheckInterval()
			* ConstantUtil.MIN_MS;
	private boolean quit;

	public CheckAlarmTask() {
		super(MY_NAME);
	}

	public void quit() {
		quit = true;
		this.interrupt();
	}

	@Override
	public void run() {
		while (!quit) {
			try {
				Thread.sleep(interval);
			} catch (InterruptedException ignore) {
				break;
			}
			if (logger.isDebugEnabled())
				logger.debug("Wake up...start checking...");
			// List<SnmpTrapLog> list =
			// DataAccess.alarmDao.getUnhandledAlarms();
			List<Alarm> alarmList = DataAccess.alarmDao.getUnresolvedAlarms();
			Set<Alarm> batch = new HashSet<Alarm>();
			List<NotificationGroup> groupList = DataAccess.notificationGroupDao
					.getGroups(EventType.UNATTENDED);
			long now = System.currentTimeMillis();
			for (Alarm alarm : alarmList) {
				String name = alarm.getName();
				for (NotificationGroup group : groupList) {
					if (group.isWildcard()) {
						examAlarm(batch, now, alarm, group);
					} else {
						for (AlarmDefinition def : group.getAlarms()) {
							if (name.equals(def.getName())) {
								if (examAlarm(batch, now, alarm, group)) {
									break;
								}
							}
						}
					}
				}
			}
			boolean ok = false;
			for (int i = 0; i < 10; i++) {
				if (!ok) {
					try {
						DataAccess.alarmDao.batchSave(batch);
						ok = true;
						break;
					} catch (Exception ignore) {
						// will be handled below
					}
				}
			}
			if (!ok) {
				logger.warn("failed in saving alarms:" + batch);
				// nothing more can be done
			}
		}
	}

	private boolean examAlarm(Set<Alarm> batch, long now, Alarm alarm,
			NotificationGroup group) {
		boolean time = false;
		boolean occurence = false;
		boolean modified = false;
		if (group.getTime() != null) {
			if (now - alarm.getLastNotified().getTime() >= group.getTime()) {
				time = true;
			}
		}
		if (group.getOccurence() != null) {
			if (alarm.getOccurrenceCount() >= group.getOccurence()) {
				occurence = true;
			}
		}
		switch (group.getTriggerer()) {
		case TIME:
			if (time) {
				AlarmNotifier.instance.handleUnattendedAlarm(alarm, group);
				alarm.setLastNotified(new Date());
				batch.add(alarm);
				modified = true;
			}
			break;
		case OCCURENCE:
			if (occurence) {
				AlarmNotifier.instance.handleUnattendedAlarm(alarm, group);
				alarm.setOccurrenceCount(0);
				alarm.setLastNotified(new Date());
				batch.add(alarm);
				modified = true;
			}
			break;
		case EITHER:
			if (time || occurence) {
				AlarmNotifier.instance.handleUnattendedAlarm(alarm, group);
				alarm.setOccurrenceCount(0);
				alarm.setLastNotified(new Date());
				batch.add(alarm);
				modified = true;
			}
			break;
		case BOTH:
			if (time && occurence) {
				AlarmNotifier.instance.handleUnattendedAlarm(alarm, group);
				alarm.setOccurrenceCount(0);
				alarm.setLastNotified(new Date());
				batch.add(alarm);
				modified = true;
			}
			break;
		}

		return modified;
	}

}
