package com.neuralt.smp.client.task;

import java.io.File;
import java.io.FilenameFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.PropertiesUtil;

/*
 * A housekeep thread to clean up temp files regularly
 */
public class FileHousekeepTask extends Thread {

	private static final String MY_NAME = "FileHousekeepTask";
	private static final Logger logger = LoggerFactory
			.getLogger(FileHousekeepTask.class);
	private static final long interval = ConstantUtil.HOUR_MS;
	private boolean quit;
	private String dirname;

	public FileHousekeepTask(String dirname) {
		super(MY_NAME);
		this.dirname = dirname;
	}

	public FileHousekeepTask() {
		super(MY_NAME);
		this.dirname = PropertiesUtil.parsePropertiesValue(AppConfig
				.getHomeFilepath());
	}

	@Override
	public void run() {
		while (!quit) {
			File[] files = findInDir(dirname);
			long now = System.currentTimeMillis();
			for (int i = 0; i < files.length; i++) {
				if (now - files[i].lastModified() >= ConstantUtil.HOUR_MS) {
					if (files[i].delete()) {
						logger.debug(files[i].getName() + " is deleted");
					} else {
						logger.debug(files[i].getName()
								+ " is failed to be deleted");
					}
				}
			}
			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				logger.info("{} was waken", MY_NAME);
			}
		}
		logger.info("{} is quitting...", MY_NAME);
	}

	public void quit() {
		quit = true;
		this.interrupt();
	}

	/*
	 * util for searching file in given folder
	 */
	public File[] findInDir(String dirName) {
		File dir = new File(dirName);

		return dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String filename) {
				return filename.endsWith(".xls");
			}
		});

	}

}
