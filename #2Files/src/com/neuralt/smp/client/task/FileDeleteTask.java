package com.neuralt.smp.client.task;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.util.ConstantUtil;

/*
 * A short-life task used to delete temp files.
 * 
 */
public class FileDeleteTask extends Thread {
	private static final String MY_NAME = "FileDeleteTask";
	private static final Logger logger = LoggerFactory
			.getLogger(FileDeleteTask.class);
	private static final long interval = 5 * ConstantUtil.MIN_MS;
	private boolean quit;
	private String filename;
	private String dir;

	/**
	 * The constructor for specifying the file to be deleted
	 * 
	 * @param filename
	 *            the full path of file to be deleted
	 */
	public FileDeleteTask(String filename) {
		super(MY_NAME);
		this.filename = filename;
		dir = null;
		logger.info("New task received - preparing to delete:" + filename);
	}

	/**
	 * The constructor for specifying the directory and the filename
	 * 
	 * @param dir
	 *            the directory containing the file to be deleted
	 * @param filename
	 *            the name of the file
	 */
	public FileDeleteTask(String dir, String filename) {
		super(MY_NAME);
		this.filename = filename;
		this.dir = dir;
		logger.info("New task received - preparing to delete:[dir=" + dir
				+ ", filename=" + filename + "]");
	}

	@Override
	public void run() {
		try {
			Thread.sleep(interval);
		} catch (InterruptedException e) {
			logger.info("{} was waken", MY_NAME);
		}
		File file = null;
		if (dir == null) {
			file = new File(filename);
		} else {
			file = new File(dir, filename);
		}
		if (file.delete()) {
			logger.info(filename + " is deleted");
		} else {
			logger.warn(filename + " is failed to be deleted");
		}
		logger.info("{} is quitting...", MY_NAME);
	}
}
