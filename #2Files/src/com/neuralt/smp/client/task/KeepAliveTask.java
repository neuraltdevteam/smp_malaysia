package com.neuralt.smp.client.task;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.HostManager;
import com.neuralt.smp.client.MonitoredHost;

public class KeepAliveTask implements Runnable, Serializable {
	private static final long serialVersionUID = -7710188464468192542L;

	private static final Logger logger = LoggerFactory
			.getLogger(KeepAliveTask.class);

	private MonitoredHost host;

	public KeepAliveTask(MonitoredHost host) {
		this.host = host;
	}

	@Override
	public void run() {
		// logger.debug("Entered KeepAliveTask:"+host.getName());
		HostManager hostMgr = host.getHostMgr();
		if (hostMgr.isEnabled() && !hostMgr.isAgentConnected()) {
			logger.debug("host {} disconnected, attempting reconnect ...",
					host.getName());
			try {
				if (hostMgr.connectJMXAgent())
					logger.debug("host reconnected [{}]", host.toString());
			} catch (Exception e) {
				logger.error("connect failed for host [{}]", host.toString(), e);
			}
		}
	}

	public MonitoredHost getHost() {
		return host;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((host == null) ? 0 : host.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj instanceof KeepAliveTask) {
			KeepAliveTask dummy = (KeepAliveTask) obj;
			return dummy.host.equals(host);
		}
		return false;
		// if (this == obj)
		// return true;
		// if (obj == null)
		// return false;
		// if (getClass() != obj.getClass())
		// return false;
		// KeepAliveTask other = (KeepAliveTask) obj;
		// if (host == null) {
		// if (other.host != null)
		// return false;
		// } else if (!host.equals(other.host))
		// return false;
		// return true;
	}

}
