package com.neuralt.smp.client;

import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.DualListModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.TimeTicks;

import com.neuralt.smp.agent.mbean.model.CPU;
import com.neuralt.smp.agent.mbean.model.Disk;
import com.neuralt.smp.agent.mbean.model.HealthUsage;
import com.neuralt.smp.agent.mbean.model.HostCPUUsage;
import com.neuralt.smp.agent.mbean.model.HostDiskUsage;
import com.neuralt.smp.agent.mbean.model.HostInetStatus;
import com.neuralt.smp.agent.mbean.model.HostMemoryUsage;
import com.neuralt.smp.agent.mbean.model.HostUptime;
import com.neuralt.smp.agent.mbean.model.Memory;
import com.neuralt.smp.agent.mbean.model.NetworkInterface;
import com.neuralt.smp.agent.mbean.model.ProcessHealth;
import com.neuralt.smp.client.data.DataWrapper;
import com.neuralt.smp.client.data.stats.CpuStats;
import com.neuralt.smp.client.data.stats.DiskStats;
import com.neuralt.smp.client.data.stats.HostStats;
import com.neuralt.smp.client.data.stats.InetStats;
import com.neuralt.smp.client.data.stats.MemStats;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.client.util.FixedLengthLinkedHashMap;
import com.neuralt.smp.client.util.FixedLengthLinkedList;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.config.HostConfig.HostRole;
import com.neuralt.smp.config.HostConfig.MonitorType;
import com.neuralt.smp.config.ProcessConfig;
import com.neuralt.smp.config.ProcessStatus;
import com.neuralt.smp.notification.ErrorNotification;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.MonitorUtil;
import com.neuralt.smp.util.StringUtil;

public class MonitoredHost implements MonitoredEntity,
		Comparable<MonitoredHost> {
	private static final long serialVersionUID = -8116627217817486378L;
	private static final Logger logger = LoggerFactory
			.getLogger(MonitoredHost.class);

	public static enum HostStatus {
		CONNECTED, DISCONNECTED
		// UNREACHABLE // as java ping won't work
	}

	private MonitoredSystem sys;
	private HostConfig config;
	private HostManager hostMgr;
	private LinkedHashMap<String, MonitoredProcess> procMap;

	private HostStatus status;
	private long lastConnected;
	private int keepNotesLimit = 1000;
	private String duration;
	private String name;

	// keeping notifications for stats purpose
	private FixedLengthLinkedList<HostCPUUsage> cpuUsageNotes;
	private FixedLengthLinkedList<HostDiskUsage> diskUsageNotes;
	private FixedLengthLinkedList<HostInetStatus> inetStatusNotes;
	private FixedLengthLinkedList<HostMemoryUsage> memUsageNotes;

	// for live charts
	private Map<Object, Number> cpuHealthMap;
	private Map<Object, Number> memHealthMap;

	// for pick list
	private DualListModel<String> dualList;

	private long lastChecked;
	private String displayableName;

	private Map<HealthNotificationFootprint, Long> notificationFootprints;

	private List<String> filters = new ArrayList<String>();
	private DataWrapper wrapper = DataWrapper.createDataWrapper();

	// private boolean enableRrd;

	public void addProcess(MonitoredProcess process) {
		procMap.put(process.getName(), process);
		config.addProcessConfig(process.getConfig());
	}

	public void deleteProcess(MonitoredProcess process) {
		procMap.remove(process.getName());
		config.deleteProcessConfig(process.getConfig());
	}

	public void deleteProcessByName(String processName) {
		config.deleteProcessConfig(procMap.get(processName).getConfig());
		procMap.remove(processName);
	}

	@Override
	public String getCurrentCpuUsage() {
		HostCPUUsage usage = this.getLatestCPUUsage();
		// added timestamp check to make sure it will not display old records
		// TODO
		// the timestamp is given by remote host and can be a wrong time
		if (usage != null)
			logger.debug("host="
					+ getName()
					+ ", usage time="
					+ StringUtil.convertTimeFormat(new Date(usage
							.getTimeStamp())));
		return (usage != null && System.currentTimeMillis()
				- usage.getTimeStamp() < AppConfig
				.getTimeConsideredDisconnected() * ConstantUtil.MIN_MS) ? StringUtil
				.formatNumberWithMaximumFractionDigits(usage.getCPUS()[0]
						.getUsr()) : "-";
	}

	@Override
	public String getCurrentMemoryUsage() {
		HostMemoryUsage usage = this.getLatestMemoryUsage();
		return (usage != null && System.currentTimeMillis()
				- usage.getTimeStamp() < AppConfig
				.getTimeConsideredDisconnected() * ConstantUtil.MIN_MS) ? StringUtil
				.formatNumberWithMaximumFractionDigits(usage.getMems()[0]
						.getUsedPct()) : "-";
	}

	@Override
	public String getDuration() {
		if (StringUtil.isNullOrEmpty(duration)
				|| System.currentTimeMillis() - lastChecked > AppConfig
						.getTimeConsideredDisconnected() * ConstantUtil.MIN_MS)
			return "-";
		else {
			return duration;
		}
	}

	@Override
	public String getLastChecked() {
		SimpleDateFormat sf = new SimpleDateFormat(AppConfig.getWebDateFormat());
		return lastChecked != 0 ? sf.format(new Date(lastChecked)) : "-";
	}

	public MonitoredHost(HostConfig config, MonitoredSystem sys)
			throws MalformedURLException {
		this.config = config;
		this.sys = sys;

		procMap = new LinkedHashMap<String, MonitoredProcess>();
		for (ProcessConfig procConfig : config.getProcessConfigs()) {
			procMap.put(procConfig.getName(), new MonitoredProcess(procConfig,
					this));
		}

		status = HostStatus.DISCONNECTED;
		lastConnected = 0;

		cpuUsageNotes = new FixedLengthLinkedList<HostCPUUsage>(keepNotesLimit);
		diskUsageNotes = new FixedLengthLinkedList<HostDiskUsage>(
				keepNotesLimit);
		inetStatusNotes = new FixedLengthLinkedList<HostInetStatus>(
				keepNotesLimit);
		memUsageNotes = new FixedLengthLinkedList<HostMemoryUsage>(
				keepNotesLimit);

		cpuHealthMap = new FixedLengthLinkedHashMap<Object, Number>(
				AppConfig.getCpuLiveChartPlotSize());
		memHealthMap = new FixedLengthLinkedHashMap<Object, Number>(
				AppConfig.getMemoryLiveChartPlotSize());

		hostMgr = new HostManager(this);

		displayableName = String.format("%s (%s)", config.getName(),
				config.getIpAddr());
		notificationFootprints = new HashMap<HealthNotificationFootprint, Long>();

	}

	public Map<String, Map<Object, Number>> getProcessCpuHealthMaps(
			String sysName, String hostName) {
		Map<String, Map<Object, Number>> map = new LinkedHashMap<String, Map<Object, Number>>();
		for (MonitoredProcess monProcess : this.getProcesses()) {
			map.put(monProcess.getName(), monProcess.getCpuHealthMap());
		}

		return map;
	}

	public Map<String, Map<Object, Number>> getProcessMemHealthMaps(
			String sysName, String hostName) {
		Map<String, Map<Object, Number>> map = new LinkedHashMap<String, Map<Object, Number>>();
		for (MonitoredProcess monProcess : this.getProcesses()) {
			map.put(monProcess.getName(), monProcess.getMemHealthMap());
		}

		return map;
	}

	/**
	 * if the same type of notification has been coming in too often if too
	 * often return false, otherwise true
	 * 
	 * @param warningLevel
	 * @param notiName
	 * @return
	 */
	private boolean isTimeToHandle(WarningLevel warningLevel, String notiName) {
		HealthNotificationFootprint footprint = new HealthNotificationFootprint(
				warningLevel, notiName);
		Long lastSeen = notificationFootprints.get(footprint);
		Long now = System.currentTimeMillis();

		boolean out = lastSeen == null
				|| (now - lastSeen) > AppConfig
						.getHostHealthNotificationControl();

		if (out)
			notificationFootprints.put(footprint, now);

		return out;
	}

	private boolean isSevereEnough(HealthUsage usage) {
		return usage.getWarningLevel().compareTo(WarningLevel.WARNING) >= 0;
	}

	public void receivedStatistics(HostCPUUsage usage) {
		if (!isStatisticsValid(usage)) {
			return;
		}
		synchronized (cpuUsageNotes) {
			cpuUsageNotes.add(usage);
		}
		lastChecked = System.currentTimeMillis();
		for (CPU cpu : usage.getCPUS()) {
			if (cpu.getProcessor().equals("all")) {
				cpuHealthMap.put(usage.getTimeStamp(), cpu.getUsr());
				break;
			}
		}

		if (isSevereEnough(usage)
				&& isTimeToHandle(usage.getWarningLevel(), usage.getClass()
						.getSimpleName())) {
			CentralizedEventService.getInstance().onRemoteHostCpuOverload(this,
					usage);

			LiveMonitorReporter.getInstance().addGrowlMessage(
					usage.getMessage());
		}
	}

	public void receivedStatistics(HostDiskUsage usage) {
		if (!isStatisticsValid(usage)) {
			return;
		}
		synchronized (diskUsageNotes) {
			diskUsageNotes.add(usage);
		}

		if (isSevereEnough(usage)) {
			if (isTimeToHandle(usage.getWarningLevel(), usage.getClass()
					.getSimpleName())) {
				Map<String, Map<Float, MonitorThresholdConfig>> warningLevelMap = MonitorUtil
						.getWarningLevelMap(config.getDiskMonitor());
				for (Disk disk : usage.getDisks()) {
					WarningLevel level = MonitorUtil.checkSingleThreshold(
							WarningLevel.WARNING, this.getName(), disk.getFs(),
							(float) disk.getUse(), warningLevelMap);
					if (level.compareTo(WarningLevel.WARNING) >= 0) {
						String message = level.getDetails();
						CentralizedEventService.getInstance()
								.onRemoteHostDiskOverload(this, usage);
						LiveMonitorReporter.getInstance().addGrowlMessage(
								message);
					}
				}
				CentralizedEventService.getInstance().onRemoteHostDiskOverload(
						this, usage);

				LiveMonitorReporter.getInstance().addGrowlMessage(
						usage.getMessage());
			}
		}
	}

	public void receivedStatistics(HostInetStatus status) {
		if (!isStatisticsValid(status)) {
			return;
		}
		logger.debug("inet status received: {}", getName());

		synchronized (inetStatusNotes) {
			inetStatusNotes.add(status);
		}

		if (isSevereEnough(status)
				&& isTimeToHandle(status.getWarningLevel(), status.getClass()
						.getSimpleName())) {
			Map<String, Map<Float, MonitorThresholdConfig>> warningLevelMap = MonitorUtil
					.getWarningLevelMap(config.getInetMonitor());
			for (NetworkInterface inet : status.getInterfaces()) {
				WarningLevel level = MonitorUtil.checkSingleThreshold(
						WarningLevel.INFO, this.getName(), inet.getName(),
						(float) inet.getStatus(), warningLevelMap);
				if (level.compareTo(WarningLevel.WARNING) >= 0) {
					String message = String.format(
							"[%s] %s reports [%s] is now [%s]", level,
							this.getName(), inet.getName(),
							inet.getStatusText());
					// XXX
					// just consider down at this moment
					CentralizedEventService.getInstance().onRemoteHostInetDown(
							this, level, message, status);
					LiveMonitorReporter.getInstance().addGrowlMessage(message);
					// TODO
					// add back case for high network flow
				}
			}
		}
	}

	public void receivedStatistics(HostMemoryUsage usage) {
		if (!isStatisticsValid(usage)) {
			return;
		}
		synchronized (memUsageNotes) {
			memUsageNotes.add(usage);
		}
		for (Memory mem : usage.getMems()) {
			if (mem.getName().equals("mem")) {
				memHealthMap.put(usage.getTimeStamp(), mem.getUsedPct());
				break;
			}
		}
		lastChecked = System.currentTimeMillis();

		if (isSevereEnough(usage)
				&& isTimeToHandle(usage.getWarningLevel(), usage.getClass()
						.getSimpleName())) {
			CentralizedEventService.getInstance().onRemoteHostMemOverload(this,
					usage);

			LiveMonitorReporter.getInstance().addGrowlMessage(
					usage.getMessage());
		}
	}

	public void receivedStatistics(HostUptime usage) {
		if (!isStatisticsValid(usage)) {
			return;
		}
		// logger.debug("host "+getName()+" uptime usage received: duration="+usage.getDuration());
		lastChecked = System.currentTimeMillis();
		if (usage.getDays() != null) {
			// logger.debug("days="+usage.getDays());
			// logger.debug("rounded timeticks="+Math.round((double)
			// (usage.getDays()*ConstantUtil.DAY_MS/10)));
			duration = new TimeTicks(Math.round((double) (usage.getDays()
					* ConstantUtil.DAY_MS / 10))).toString(); // cast to double
																// so the
																// rounded
																// result will
																// be long
																// instead of
																// int
		} else {
			duration = usage.getDuration();
		}

		// logger.debug("uptime received:"+usage);
		// logger.debug("duration="+duration);
		// ? under what circumstances would "uptime" (not downtime) trigger
		// traps???
		// if (isSevereEnough(usage) && isTimeToHandle(usage.getWarningLevel(),
		// usage.getClass().getSimpleName())) {
		// EventLog evtlog = new
		// EventLog(Severity.mapWarningLevel(usage.getWarningLevel()),
		// SourceType.HOST, this,
		// usage.getTimeStamp(), usage.getMessage());
		// DataAccess.eventLogDao.save(evtlog);
		// SnmpTrap trap = new SnmpTrap();
		// trap.setOid(SvrSnmpConstants.systemMajorAlarm);
		// trap.setSource(config.getName());
		// trap.setTrapAlarmLevel(usage.getWarningLevel().name());
		// trap.setStatus(AlarmStatus.UNHANDLED);
		// trap.setTrapAppName(AppConfig.getMyName());
		// trap.setSourceIP(config.getIpAddr());
		// trap.setAlarmDescription(usage.getMessage());
		// DataAccess.alarmDao.save(trap);
		// AlarmService.instance.recordSnmpTrap(trap);
		//
		// LiveMonitorReporter.getInstance().addGrowlMessage(usage.getMessage());
		// }
	}

	public void setHostUp() {
		logger.debug("{} setHostUp invoked", getName());
		if (status == HostStatus.DISCONNECTED) {
			status = HostStatus.CONNECTED;
			// StringBuilder sb = new StringBuilder();
			// sb.append(getName()).append(" is CONNECTED");
			CentralizedEventService.getInstance().onRemoteHostConnected(this);
		}
	}

	public void setHostDown() {
		logger.debug("{} setHostDown invoked", getName());
		status = HostStatus.DISCONNECTED;
		StringBuilder sb = new StringBuilder();
		sb.append(getName()).append(" is DISCONNECTED");
		CentralizedEventService.getInstance().onRemoteHostDisconnected(this,
				sb.toString());
	}

	public void receivedStatistics(ProcessHealth usage) {
		if (!isStatisticsValid(usage)) {
			return;
		}
		String processName = usage.getProcessName();
		procMap.get(processName).receivedNotification(usage);

		if (isSevereEnough(usage)
				&& isTimeToHandle(usage.getWarningLevel(), usage.getClass()
						.getSimpleName())) {
			// ProcessHealth for a dead process -> trigger died event again?
			String procName = usage.getProcessName();
			MonitoredProcess source = getProcess(procName);
			if (source.getStatus().equals(ProcessStatus.DIED)) {
				CentralizedEventService.getInstance().onRemoteAppDown(this,
						procName, usage.getMessage());
			}
		}
	}

	public void receivedError(ErrorNotification note) {
		// TODO Auto-generated method stub
		StringBuilder sb = new StringBuilder();
		sb.append("Message=").append(note.getMessage()).append(", Cause=")
				.append(StringUtil.nvl(note.getCause().getMessage(), ""));
		CentralizedEventService.getInstance().onRemoteHostGeneralError(this,
				sb.toString());
	}

	public int getKeepNotesLimit() {
		return keepNotesLimit;
	}

	public void setKeepNotesLimit(int keepNotesLimit) {
		this.keepNotesLimit = keepNotesLimit;
	}

	public HostStats computeAverageStats(long periodMs) {
		long now = System.currentTimeMillis();
		HostStats stats = new HostStats(sys.getName(), config.getName(),
				new Timestamp(now));
		int bufSize = 128;

		synchronized (cpuUsageNotes) {
			int noteCount = 0;
			int cpuCount = 0;
			CpuStats[] cpuBuf = new CpuStats[bufSize];
			Iterator<HostCPUUsage> backIter = cpuUsageNotes
					.descendingIterator();
			while (backIter.hasNext()) {
				HostCPUUsage usage = backIter.next();
				if (now - usage.getTimeStamp() <= periodMs) {
					noteCount++;
					CPU[] cpus = usage.getCPUS();
					cpuCount = Math.max(cpuCount, cpus.length);
					for (int i = 0; i < cpus.length; i++) {
						if (cpuBuf[i] == null)
							cpuBuf[i] = new CpuStats(stats,
									cpus[i].getProcessor());
						cpuBuf[i].add(cpus[i]);
					}
				} else {
					break;
				}
			} // end while
			if (noteCount > 0) {
				for (int i = 0; i < cpuCount; i++) {
					CpuStats avg = cpuBuf[i];
					avg.divideBy(noteCount);
					stats.addCpuStats(avg);
				}
			}
		}

		synchronized (diskUsageNotes) {
			int noteCount = 0;
			int diskCount = 0;
			DiskStats[] diskBuf = new DiskStats[bufSize];
			Iterator<HostDiskUsage> backIter = diskUsageNotes
					.descendingIterator();
			while (backIter.hasNext()) {
				HostDiskUsage usage = backIter.next();
				if (now - usage.getTimeStamp() <= periodMs) {
					noteCount++;
					Disk[] disks = usage.getDisks();
					diskCount = Math.max(diskCount, disks.length);
					for (int i = 0; i < disks.length; i++) {
						if (diskBuf[i] == null)
							diskBuf[i] = new DiskStats(stats, disks[i].getFs());
						diskBuf[i].add(disks[i]);
					}
				} else {
					break;
				}
			} // end while
			if (noteCount > 0) {
				for (int i = 0; i < diskCount; i++) {
					DiskStats avg = diskBuf[i];
					avg.divideBy(noteCount);
					stats.addDiskStats(avg);
				}
			}
		}

		// TODO
		// revise algorithm
		synchronized (inetStatusNotes) {
			int noteCount = 0;
			int inetCount = 0;
			InetStats[] inetBuf = new InetStats[bufSize];
			Iterator<HostInetStatus> backIter = inetStatusNotes
					.descendingIterator();
			while (backIter.hasNext()) {
				HostInetStatus usage = backIter.next();
				if (now - usage.getTimeStamp() <= periodMs) {
					noteCount++;
					NetworkInterface[] inets = usage.getInterfaces();
					inetCount = Math.max(inetCount, inets.length);
					for (int i = 0; i < inets.length; i++) {
						if (inetBuf[i] == null)
							inetBuf[i] = new InetStats(stats,
									inets[i].getName());
						inetBuf[i].add(inets[i]);
					}
				} else {
					break;
				}
			} // end while
			if (noteCount > 0) {
				for (int i = 0; i < inetCount; i++) {
					InetStats avg = inetBuf[i];
					avg.divideBy(noteCount);
					stats.addInetStats(avg);
				}
			}
		}
		// TODO revise stat
		synchronized (memUsageNotes) {
			int noteCount = 0;
			int memCount = 0;
			MemStats[] memBuf = new MemStats[1]; // mem/swap
			Iterator<HostMemoryUsage> backIter = memUsageNotes
					.descendingIterator();
			while (backIter.hasNext()) {
				HostMemoryUsage usage = backIter.next();
				if (now - usage.getTimeStamp() <= periodMs) {
					noteCount++;
					Memory[] memUsages = usage.getMems();
					memCount = memBuf.length;// Math.max(memCount,
												// memUsages.length); // should
												// end up being 2 anyway
					for (int i = 0; i < memBuf.length; i++) { // memUsages.length=4
																// mem/swap/high/low
						if (memBuf[i] == null) // will throw
												// ArrayIndexOutOfBoundsException
							memBuf[i] = new MemStats(stats,
									memUsages[i].getName());

						// System.err.println(memUsages[i]);
						memBuf[i].add(memUsages[i]);
					}
				} else {
					break;
				}
			} // end while

			if (noteCount > 0) {
				for (int i = 0; i < memCount; i++) {
					MemStats avg = memBuf[i];
					avg.divideBy(noteCount);
					stats.addMemStats(avg);
				}
			}
		}

		return stats;
	}

	private boolean isStatisticsValid(HealthUsage usage) {
		if (usage instanceof HostCPUUsage) {
			HostCPUUsage cpuUsage = (HostCPUUsage) usage;
			for (CPU cpu : cpuUsage.getCPUS()) {
				if (cpu.getProcessor().equals("all")) {
					if (cpu.getUsr() == null) {
						return false;
					} else {
						if (cpu.getUsr() < 0 || cpu.getUsr() > 100) {
							return false;
						}
					}
				}
			}
		} else if (usage instanceof HostDiskUsage) {
			HostDiskUsage diskUsage = (HostDiskUsage) usage;
			for (Disk disk : diskUsage.getDisks()) {
				if (disk.getUse() == null) {
					return false;
				} else {
					if (disk.getUse() < 0 || disk.getUse() > 100) {
						return false;
					}
				}
			}
		} else if (usage instanceof HostInetStatus) {
			HostInetStatus inetUsage = (HostInetStatus) usage;
			for (NetworkInterface inet : inetUsage.getInterfaces()) {
				if (inet.getInOctet() == null || inet.getOutOctet() == null) {
					return false;
				} else {
					if (inet.getInOctet() < 0 || inet.getOutOctet() < 0) {
						return false;
					}
				}
			}
		} else if (usage instanceof HostMemoryUsage) {
			HostMemoryUsage memUsage = (HostMemoryUsage) usage;
			for (Memory mem : memUsage.getMems()) {
				if (mem.getName().equals("mem")) {
					if (mem.getUsedPct() == null) {
						return false;
					} else {
						if (mem.getUsedPct() < 0 || mem.getUsedPct() > 100) {
							return false;
						}
					}
				}
			}
		} else if (usage instanceof HostUptime) {
			HostUptime uptimeUsage = (HostUptime) usage;
			if (uptimeUsage.getDays() != null) {
				if (uptimeUsage.getDays() < 0) {
					return false;
				}
			}
		} else if (usage instanceof ProcessHealth) {
			ProcessHealth processUsage = (ProcessHealth) usage;
			if (processUsage.getCpu() == null || processUsage.getMem() == null) {
				return false;
			} else {
				if (processUsage.getCpu() < 0 || processUsage.getCpu() > 100
						|| processUsage.getMem() < 0
						|| processUsage.getMem() > 100) {
					return false;
				}
			}
		}
		return true;
	}

	public HostCPUUsage getLatestCPUUsage() {
		if (cpuUsageNotes.isEmpty())
			return null;
		return cpuUsageNotes.getLast();
	}

	public HostDiskUsage getLatestDiskUsage() {
		if (diskUsageNotes.isEmpty())
			return null;
		return diskUsageNotes.getLast();
	}

	public HostMemoryUsage getLatestMemoryUsage() {
		if (memUsageNotes.isEmpty())
			return null;
		return memUsageNotes.getLast();
	}

	public HostInetStatus getLatestNetworkStatus() {
		if (inetStatusNotes.isEmpty())
			return null;
		return inetStatusNotes.getLast();
	}

	public HostConfig getBackupHostConfig() {
		HostConfig backupHost = new HostConfig();
		backupHost.setId(config.getId());
		backupHost.setName(config.getName());
		backupHost.setDescription(config.getDescription());
		backupHost.setAgentJmxPort(config.getAgentJmxPort());
		backupHost.setIpAddr(config.getIpAddr());
		backupHost.setEnabled(config.isEnabled());
		backupHost.setRole(config.getRole());
		backupHost.setType(config.getType());
		backupHost.setAgentDeployed(config.isAgentDeployed());
		backupHost.setSite(config.getSite());
		backupHost.setSystemConfig(config.getSystemConfig());
		return backupHost;
	}

	public void updateConfig(HostConfig dummyHost) {
		getConfig().setName(dummyHost.getName());
		getConfig().setDescription(dummyHost.getDescription());
		getConfig().setEnabled(dummyHost.isEnabled());
		getConfig().setIpAddr(dummyHost.getIpAddr());
		getConfig().setAgentJmxPort(dummyHost.getAgentJmxPort());
		getConfig().setRole(dummyHost.getRole());
		getConfig().setType(dummyHost.getType());
		getConfig().setAgentDeployed(dummyHost.isAgentDeployed());
		getConfig().setSite(dummyHost.getSite());
	}

	public boolean isNeedRestart(HostConfig backupHostConfig) {
		if ((!getConfig().getIpAddr().equals(backupHostConfig.getIpAddr()))
				|| (!(getConfig().getAgentJmxPort() == backupHostConfig
						.getAgentJmxPort()))
				|| (getConfig().getType() != backupHostConfig.getType())) {
			if (getConfig().isEnabled() && getSys().getConfig().isEnabled())
				return true;
		}
		return false;
	}

	public boolean isProcessListEmpty() {
		if (dualList == null) {
			return false;
		} else {
			if (dualList.getSource().isEmpty()) {
				return true;
			}
			return false;
		}
	}

	public HostConfig getConfig() {
		return config;
	}
	
	public void setConfig(HostConfig config) {
		this.config = config;
	}

	public MonitoredSystem getSys() {
		return sys;
	}

	public void setSys(MonitoredSystem sys) {
		this.sys = sys;
	}

	public HostManager getHostMgr() {
		return hostMgr;
	}

	public MonitoredProcess getProcess(String procName) {
		return procMap.get(procName);
	}

	// public void setEnableRrd(boolean enableRrd) {
	// this.enableRrd = enableRrd;
	// }
	public List<MonitoredProcess> getProcesses() {
		List<MonitoredProcess> procs = new LinkedList<MonitoredProcess>();
		procs.addAll(procMap.values());
		Collections.sort(procs);
		return procs;
	}

	/**
	 * Use this method carefully - when SNMP is enabled for monitoring, this
	 * method will return the name get by SNMP ie. sysName(1.3.6.1.2.1.1.5.0)
	 * instead of the name defined in host config. This method should only be
	 * used for display purpose, not storing information or as any key for
	 * searching.
	 * 
	 */
	public String getName() {
		if (StringUtil.isNullOrEmpty(name)) {
			return config.getName();
		}
		return name;
	}

	public String getDisplayableName() {
		return String.format("%s (%s)", getName(), config.getIpAddr());
	}

	public String getFullName() {
		return sys.getFullName() + " - " + getName();
	}

	public String getDescription() {
		return config.getDescription();
	}

	public HostRole getHostRole() {
		return config.getRole();
	}

	public boolean isEnabled() {
		return config.isEnabled();
	}

	public String getIpAddr() {
		return config.getIpAddr();
	}

	public int getAgentJmxPort() {
		return config.getAgentJmxPort();
	}

	public boolean isAgentConnected() {
		return status == HostStatus.CONNECTED;
	}

	public HostStatus getStatus() {
		return status;
	}

	public void setStatus(HostStatus status) {
		this.status = status;
		// to avoid ambiguous in displaying process status
		if (status.equals(HostStatus.DISCONNECTED)) {
			for (MonitoredProcess process : procMap.values()) {
				process.setStatus(ProcessStatus.STOPPED);
			}
		}
	}

	public long getLastConnected() {
		return lastConnected;
	}

	public void setLastConnected(long lastConnected) {
		this.lastConnected = lastConnected;
	}

	public Map<Object, Number> getCpuHealthMap() {
		return cpuHealthMap;
	}

	public Map<Object, Number> getMemHealthMap() {
		return memHealthMap;
	}

	@Override
	public String getStatusText() {
		if (config.getType().equals(MonitorType.SNMP)) {
			return "-";
		}
		return status.name();
		// return "CONNECTED";
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.HOST;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MonitoredHost [sys=").append(sys).append(", config=")
				.append(config).append(", status=").append(status)
				.append(", enabled=").append(", lastChecked=")
				.append(lastChecked).append(", displayableName=")
				.append(displayableName).append("]");
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((config == null) ? 0 : config.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof MonitoredHost) {
			MonitoredHost dummy = (MonitoredHost) o;
			return this.config.equals(dummy.getConfig());
		}
		return false;
		/*
		 * if (this == obj) return true; if (obj == null) return false; if
		 * (getClass() != obj.getClass()) return false; MonitoredHost other =
		 * (MonitoredHost) obj; if (config == null) { if (other.config != null)
		 * return false; } else if (!config.equals(other.config)) return false;
		 * return true;
		 */
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public void setName(String name) {
		logger.debug("set name called");
		if (StringUtil.isNullOrEmpty(name)) {
			this.name = config.getName();
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append(config.getName()).append(" - ").append(name);
			this.name = sb.toString();
		}
		logger.debug("set name finished:" + this.name);
	}

	public DualListModel<String> getDualList() {
		return dualList;
	}

	public void setDualList(DualListModel<String> dualList) {
		this.dualList = dualList;
	}

	@Override
	public int compareTo(MonitoredHost o) {
		return config.compareTo(o.getConfig());
	}

	public void resetFilters() {
		filters.clear();
	}
}
