package com.neuralt.smp.client.snmp;

import java.io.IOException;

import net.percederberg.mibble.MibLoaderException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.snmp.util.MibFileLoader;

public class MibFileHandler {

	private static final Logger logger = LoggerFactory
			.getLogger(MibFileHandler.class);

	private MibFileHandler() {

	}

	private static MibFileHandler instance;

	public static MibFileHandler getInstance() {
		if (instance == null) {
			synchronized (MibFileHandler.class) {
				if (instance == null) {
					instance = new MibFileHandler();
				}
			}
		}
		return instance;
	}

	public void processMibFile(String dir, String fileName) throws Exception {
		try {
			MibFileLoader.getInstance().loadMibFromFile(dir, fileName);
		} catch (MibLoaderException e) {
			throw e;
		} catch (IOException e) {
			throw e;
		}

	}
}
