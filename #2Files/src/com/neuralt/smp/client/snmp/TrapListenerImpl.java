package com.neuralt.smp.client.snmp;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.SnmpTrap.Direction;
import com.neuralt.smp.client.data.config.AlarmMap;
import com.neuralt.smp.client.service.AlarmService;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.snmp.model.TrapListener;
import com.neuralt.smp.snmp.model.TrapRawObject;
import com.neuralt.smp.util.StringUtil;

public class TrapListenerImpl implements TrapListener {

	private Logger logger = LoggerFactory.getLogger(TrapListenerImpl.class);

	public void onTrap(TrapRawObject trap, boolean isEDRW) {
		String ip = StringUtil.parseIP(trap.getSourceIP());
		// String communty = trap.getCommunity();
		// TODO
		// do a search based on ip & community to decide whether the trap should
		// be logged
		// V3?
		try {
			// if(AppConfig.getSNMPv1v2Community().equals(communty)){
			SnmpTrap dbTrap = new SnmpTrap(trap);
			dbTrap.setDirection(Direction.INCOMING);

			// Hard code for EDRW
			if (isEDRW) {
				dbTrap.setAlarmDescription(dbTrap.getTrapDescription());
			}
			// END

			DataAccess.snmpTrapDao.save(dbTrap);

			AlarmService.instance.recordSnmpTrap(dbTrap);

			// Hard code for EDRW
			if (isEDRW) {
				WarningLevel level = null;
				try {
					level = WarningLevel.valueOf(dbTrap.getLevel());
				} catch (Exception e) {
					level = WarningLevel.UNKNOWN;
				}

				AlarmMap alarmMap = null;
				if (level != null) {
					alarmMap = AlarmService.instance.findTrapAlarmMapping(trap
							.getOid().toString(), level);
				}

				if (alarmMap != null) {
					SnmpTrap dbOutTrap = new SnmpTrap();
					dbOutTrap.setOid(dbTrap.getOid());
					dbOutTrap.setTrapVersion(dbTrap.getTrapVersion());
					dbOutTrap.setSource(dbTrap.getSource());
					dbOutTrap.setSourceIp(dbTrap.getSourceIp());
					dbOutTrap.setLastCheckTime(dbTrap.getLastCheckTime());
					
					dbOutTrap.setTrapAppName(dbTrap.getTrapAppName());
					dbOutTrap.setTrapAlarmLevel(dbTrap.getTrapAlarmLevel());
					dbOutTrap.setTrapAlarmId(dbTrap.getTrapAlarmId());
					dbOutTrap.setAlarmDescription(dbTrap.getAlarmDescription());
					
					dbOutTrap.setDirection(Direction.OUTGOING);
					DataAccess.snmpTrapDao.save(dbOutTrap);
				}
			}
			// END

			// logger.info("Logged:"+alarm);
			// }
		} catch (Exception e) {
			logger.error("Alarm logging failed [{}] ", trap, e);
		}

	}

    @Override
    public void onTrap(TrapRawObject trap) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
