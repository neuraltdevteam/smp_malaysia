package com.neuralt.smp.client.snmp;

public class OidIntegrityException extends Exception {
	public OidIntegrityException(String msg) {
		super(msg);
	}

	public OidIntegrityException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
