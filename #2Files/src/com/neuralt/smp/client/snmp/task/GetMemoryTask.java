package com.neuralt.smp.client.snmp.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.agent.mbean.model.HostMemoryUsage;
import com.neuralt.smp.agent.mbean.model.Memory;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.MonitorUtil;

public class GetMemoryTask extends SnmpGetTask {

	private static final Logger logger = LoggerFactory
			.getLogger(GetMemoryTask.class);

	// private static final OID memTotalSwap = new
	// OID(".1.3.6.1.4.1.2021.4.3.0");
	// private static final OID memAvailSwap = new
	// OID(".1.3.6.1.4.1.2021.4.4.0");
	private static final OID memTotalReal = new OID(".1.3.6.1.4.1.2021.4.5.0");
	private static final OID memAvailReal = new OID(".1.3.6.1.4.1.2021.4.6.0");
	private static final OID memBuffer = new OID(".1.3.6.1.4.1.2021.4.14.0");
	private static final OID memCached = new OID(".1.3.6.1.4.1.2021.4.15.0");

	private static List<OID> list;
	private static final String MY_NAME = "MEMORY_MONITOR";
	private Map<OID, Long> map;
	static {
		list = new ArrayList<OID>();
		// list.add(memTotalSwap);
		// list.add(memAvailSwap);
		list.add(memTotalReal);
		list.add(memAvailReal);
		list.add(memBuffer);
		list.add(memCached);
	}

	public GetMemoryTask(MonitoredHost host, SnmpGetStrategy util) {
		super(MY_NAME);
		this.host = host;
		this.config = host.getConfig().getMemoryMonitor();
		this.util = util;
	}

	@Override
	void init() {
		map = new HashMap<OID, Long>();
	}

	@Override
	void performTask() {
		map.clear();
		try {
			List<VariableBinding> vbs = util.getList(list);
			if (vbs != null) {
				for (VariableBinding vb : vbs) {
					map.put(vb.getOid(), Long.valueOf(vb.toValueString()));
				}
				Memory mem = new Memory();
				mem.setName("mem");
				mem.setFree(map.get(memAvailReal) + map.get(memBuffer)
						+ map.get(memCached));
				mem.setTotal(map.get(memTotalReal));
				mem.calculateUsed();
				// Memory swap = new Memory();
				// swap.setName("swap");
				// swap.setFree(map.get(memAvailSwap));
				// swap.setTotal(map.get(memTotalSwap));
				// swap.calculateUsed();
				Memory[] mems = new Memory[1];
				mems[0] = mem;
				// mems[1] = swap;
				HostMemoryUsage usage = new HostMemoryUsage();
				usage.setMems(mems);
				usage.setTimeStamp(System.currentTimeMillis());
				WarningLevel level = WarningLevel.NORMAL;
				Map<String, Map<Float, MonitorThresholdConfig>> warningLevelMap = MonitorUtil
						.getWarningLevelMap(config);
				if (warningLevelMap != null && usage != null) {
					for (Memory memory : usage.getMems()) {
						Float currentValue = memory.getUsedPct();
						level = MonitorUtil.checkSingleThreshold(level,
								this.getName(), memory.getName(), currentValue,
								warningLevelMap);
					}
				}
				usage.setWarningLevel(level);
				usage.setMessage(level.getDetails());
				host.receivedStatistics(usage);
			}
		} catch (NullPointerException e) {
			handleSnmpServiceDown();
		} catch (SnmpAccessException e) {
			handleSnmpAccessException(e.getMessage());
		}
	}
}
