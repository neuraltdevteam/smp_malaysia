package com.neuralt.smp.client.snmp.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.data.SnmpTarget;
import com.neuralt.smp.client.data.SnmpTarget.MethodType;
import com.neuralt.smp.client.data.SnmpTarget.TargetType;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.client.util.OidUtil;

/**
 * At this moment the task is not used as a thread, but a utility for holding
 * snmp configs and query
 *
 */
public class GeneralSnmpGetTask extends SnmpGetTask {

	private static final Logger logger = LoggerFactory
			.getLogger(GeneralSnmpGetTask.class);
	private static final String MY_NAME = "GENERAL_SNMP_GET_TASK";
	private SnmpTarget target;

	public GeneralSnmpGetTask(SnmpGetStrategy util, SnmpTarget target) {
		super(MY_NAME);
		this.util = util;
		this.target = target;
	}

	public GeneralSnmpGetTask(SnmpGetStrategy util, SnmpTarget target,
			MonitoredHost host) {
		super(MY_NAME);
		this.util = util;
		this.target = target;
		this.host = host;
	}

	@Override
	public void run() {
		init();
		while (!quit) {
			performTask();
			try {
				Thread.sleep(50000);
			} catch (InterruptedException ignore) {
			}
		}
	}

	@Override
	void init() {
		// TODO Auto-generated method stub

	}

	@Override
	void performTask() {

		// as this class will be returning values to parent, this method is
		// abandoned

		// if (target.getMethodType().equals(MethodType.STATIC)) {
		// try {
		// util.get(OidUtil.getInstance().getOidFromString(target.getValueDir()));
		// } catch (SnmpAccessException e) {
		// handleSnmpAccessException(e.getMessage());
		// } catch (Exception e) {
		// }
		// } else if (target.getMethodType().equals(MethodType.DYNAMIC)) {
		// try {
		// List<VariableBinding> valueVbs =
		// util.walk(OidUtil.getInstance().getOidFromString(target.getValueDir()));
		// List<VariableBinding> keyVbs =
		// util.walk(OidUtil.getInstance().getOidFromString(target.getKeyDir()));
		// for (VariableBinding vb : keyVbs) {
		// if (target.getKeyType().equals(TargetType.STRING)) {
		// String dummy = vb.getVariable().toString();
		// if (dummy.equals(target.getTargetString())) {
		// for (VariableBinding value : valueVbs) {
		// if (value.getOid().last()==vb.getOid().last()) {
		// handleValue(value.getVariable());
		// break;
		// }
		// }
		// break;
		// }
		// } else if (target.getKeyType().equals(TargetType.INTEGER)) {
		// long dummy = vb.getVariable().toLong();
		// if (dummy==(Long.valueOf(target.getTargetString()))) {
		// for (VariableBinding value : valueVbs) {
		// if (value.getOid().last()==vb.getOid().last()) {
		// handleValue(value.getVariable());
		// break;
		// }
		// }
		// break;
		// }
		// }
		// }
		//
		//
		// } catch (SnmpAccessException e) {
		// handleSnmpAccessException(e.getMessage());
		// } catch (Exception e) {
		// }
		// }
	}

	/**
	 * This method will query the required value by the SnmpTarget object. It is
	 * assumed the return value will be type of long. For STATIC, it is straight
	 * forward. For DYNAMIC, it uses the approach of calling getbulk on valueDir
	 * & keyDir and compare the VariableBindings for finding the matched value. <br/>
	 * As calling walk or getbulk on the higher level OID (ie. walkDir) will
	 * result in slow response, that approach is temporarily abandoned.
	 * 
	 * @return the query result
	 * @throws Exception
	 */
	public long query() throws Exception {
		if (target.getMethodType().equals(MethodType.STATIC)) {
			try {
				return ((Variable) util
						.get(OidUtil.getInstance()
								.getOidFromString(
										target.getValueDir() + "."
												+ target.getSuffix())))
						.toLong();
			} catch (SnmpAccessException e) {
				if (host != null)
					handleSnmpAccessException(e.getMessage());
				throw e;
			} catch (Exception e) {
				throw e;
			}
		} else if (target.getMethodType().equals(MethodType.DYNAMIC)) {
			try {
				List<VariableBinding> valueVbs = util.getbulk(OidUtil
						.getInstance().getOidFromString(target.getValueDir()));
				List<VariableBinding> keyVbs = util.getbulk(OidUtil
						.getInstance().getOidFromString(target.getKeyDir()));
				// OID valueDirOid = target.getValueDirOid();
				OID keyDirOid = OidUtil.getInstance().getOidFromString(
						target.getKeyDir());
				int[] valueDir = OidUtil.getInstance()
						.getOidFromString(target.getValueDir()).toIntArray();
				int[] keyDir = OidUtil.getInstance()
						.getOidFromString(target.getKeyDir()).toIntArray();
				for (VariableBinding vb : keyVbs) {
					if (target.getKeyType().equals(TargetType.STRING)) {
						String dummy = vb.getVariable().toString();
						// logger.debug("checking on variable:"+dummy);
						if (dummy.equals(target.getTargetString())) {
							OID keyOid = vb.getOid();
							int difference = keyOid.size() - keyDirOid.size();
							// int[] keyOid = vb.getOid().toIntArray();
							// int[] residue = new
							// int[keyOid.length-keyDir.length];
							// for (int i=0; i<residue.length; i++) {
							// residue[i] = keyOid[i+keyDir.length];
							// }
							for (VariableBinding value : valueVbs) {
								if (value.getOid().rightMostCompare(difference,
										keyOid) == 0) {
									return value.getVariable().toLong();
								}
								// int[] valueOid = value.getOid().toIntArray();
								// boolean found = true;
								// for (int i=0; i<residue.length; i++) {
								// if (residue[i]!=valueOid[i+valueDir.length])
								// found = false;
								// }
								// if (found) {
								// //
								// logger.debug("successful : ready to return value");
								// return value.getVariable().toLong();
								// }
							}
							break;
						}
					} else if (target.getKeyType().equals(TargetType.INTEGER)) {
						long dummy = vb.getVariable().toLong();
						// logger.debug("checking on variable:"+dummy);
						if (dummy == (Long.valueOf(target.getTargetString()))) {
							OID keyOid = vb.getOid();
							int difference = keyOid.size() - keyDirOid.size();
							// int[] keyOid = vb.getOid().toIntArray();
							// int[] residue = new
							// int[keyOid.length-keyDir.length];
							// for (int i=0; i<residue.length; i++) {
							// residue[i] = keyOid[i+keyDir.length];
							// }
							for (VariableBinding value : valueVbs) {
								if (value.getOid().rightMostCompare(difference,
										keyOid) == 0) {
									return value.getVariable().toLong();
								}
								// int[] valueOid = value.getOid().toIntArray();
								// boolean found = true;
								// for (int i=0; i<residue.length; i++) {
								// if (residue[i]!=valueOid[i+valueDir.length])
								// found = false;
								// }
								// if (found) {
								// //
								// logger.debug("successful : ready to return value");
								// return value.getVariable().toLong();
								// }
							}
							break;
						}
					}
				}
			} catch (SnmpAccessException e) {
				if (host != null)
					handleSnmpAccessException(e.getMessage());
				throw e;
			} catch (Exception e) {
				logger.debug("failed in query", e);
				throw e;
			}
		}
		logger.warn("{} runs to the end of query() - should be not happening",
				MY_NAME);
		return 0;
	}
}
