package com.neuralt.smp.client.snmp.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.LiveMonitorReporter;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

public abstract class SnmpGetTask extends Thread {

	private static final Logger logger = LoggerFactory
			.getLogger(SnmpGetTask.class);

	private String name;

	boolean quit = false;
	SnmpGetStrategy util;
	MonitoredHost host;
	HostMonitorConfig config;

	public SnmpGetTask(String myName) {
		super(myName);
		name = myName;
	}

	@Override
	public void run() {
		init();
		while (!quit) {
			performTask();
			try {
				Thread.sleep(config.getMonitorInterval() * 1000);
			} catch (InterruptedException ignore) {
			}
		}
	}

	abstract void init();

	abstract void performTask();

	public void quit() {
		quit = true;
		this.interrupt();
		util.closeSnmp();
		logger.debug("{} is quiting...", this);
	}

	void handleSnmpServiceDown() {
		handleSnmpAccessException(WebUtil
				.getMessage(MessageKey.NO_SNMP_RESPONSE));
	}

	public void handleSnmpAccessException(String message) {
		if (StringUtil.isNullOrEmpty(message)) {
			message = "Failed in SNMP query";
		}
		message = name + " " + message;
		CentralizedEventService.getInstance().onRemoteHostSnmpError(host,
				message);
		LiveMonitorReporter.getInstance().addGrowlMessage(message);
	}

}
