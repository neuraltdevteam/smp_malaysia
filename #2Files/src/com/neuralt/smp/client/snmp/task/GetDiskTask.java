package com.neuralt.smp.client.snmp.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.agent.mbean.model.Disk;
import com.neuralt.smp.agent.mbean.model.HostDiskUsage;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.MonitorUtil;

public class GetDiskTask extends SnmpGetTask {

	private static final Logger logger = LoggerFactory
			.getLogger(GetDiskTask.class);
	private static final String MY_NAME = "DISK_MONITOR";
	private static final OID dskEntry = new OID(".1.3.6.1.4.1.2021.9.1");
	private static final OID dskPath = new OID(".1.3.6.1.4.1.2021.9.1.2");
	private static final OID dskDevice = new OID(".1.3.6.1.4.1.2021.9.1.3");
	private static final OID dskTotal = new OID(".1.3.6.1.4.1.2021.9.1.6");
	private static final OID dskAvail = new OID(".1.3.6.1.4.1.2021.9.1.7");
	private static final OID dskUsed = new OID(".1.3.6.1.4.1.2021.9.1.8");
	private static final OID dskPercent = new OID(".1.3.6.1.4.1.2021.9.1.9");
	private static List<OID> list;

	private Map<Integer, Disk> map;
	private int diskNumber;

	static {
		list = new ArrayList<OID>();
		list.add(dskPath);
		list.add(dskDevice);
		list.add(dskTotal);
		list.add(dskAvail);
		list.add(dskUsed);
		list.add(dskPercent);
	}

	public GetDiskTask(MonitoredHost host, SnmpGetStrategy util) {
		super(MY_NAME);
		this.host = host;
		this.config = host.getConfig().getDiskMonitor();
		this.util = util;
	}

	@Override
	void init() {
		map = new HashMap<Integer, Disk>();
		diskNumber = 0;
	}

	@Override
	void performTask() {
		map.clear();
		try {
			List<VariableBinding> vbs = util.walk(dskEntry);
			for (VariableBinding vb : vbs) {
				Integer number = vb.getOid().last();
				if (number > diskNumber) {
					diskNumber = number;
				}
			}
			logger.debug("disk number:" + diskNumber);
			if (diskNumber != 0) {
				Disk[] disks = new Disk[diskNumber];
				for (VariableBinding vb : vbs) {
					Integer number = vb.getOid().last();
					if (map.get(number) == null) {
						map.put(number, new Disk());
					}
					for (OID oid : list) {
						if (vb.getOid().leftMostCompare(oid.size(), oid) == 0) {
							if (oid.equals(dskAvail)) {
								// logger.debug("vb.oid:"+vb.getOid());
								// logger.debug("dskAvail:"+vb.getVariable().toString());
								map.get(number).setAvail(
										Long.valueOf(vb.toValueString()));
							} else if (oid.equals(dskPath)) {
								// logger.debug("vb.oid:"+vb.getOid());
								// logger.debug("dskPath:"+vb.getVariable().toString());
								map.get(number).setMount(vb.toValueString());
							} else if (oid.equals(dskDevice)) {
								// logger.debug("vb.oid:"+vb.getOid());
								// logger.debug("dskDevice:"+vb.getVariable().toString());
								map.get(number).setFs(vb.toValueString());
							} else if (oid.equals(dskTotal)) {
								// logger.debug("vb.oid:"+vb.getOid());
								// logger.debug("dskTotal:"+vb.getVariable().toString());
								map.get(number).setSize(
										Long.valueOf(vb.getVariable()
												.toString()));
							} else if (oid.equals(dskUsed)) {
								// logger.debug("vb.oid:"+vb.getOid());
								// logger.debug("dskUsed:"+vb.getVariable().toString());
								map.get(number).setUsed(
										Long.valueOf(vb.toValueString()));
							} else if (oid.equals(dskPercent)) {
								// logger.debug("vb.oid:"+vb.getOid());
								// logger.debug("dskPercent:"+vb.getVariable().toString());
								map.get(number)
										.setUse(vb.toValueString() + "%");
							}
						}
					}
				}
				for (int i = 0; i < diskNumber; i++) {
					disks[i] = map.get(i + 1);
					if (logger.isDebugEnabled()) {
						logger.debug("disk " + i + ":" + disks[i]);
					}
				}
				HostDiskUsage usage = new HostDiskUsage();
				usage.setDisks(disks);
				usage.setTimeStamp(System.currentTimeMillis());
				WarningLevel level = WarningLevel.NORMAL;
				Map<String, Map<Float, MonitorThresholdConfig>> warningLevelMap = MonitorUtil
						.getWarningLevelMap(config);
				if (warningLevelMap != null && usage != null) {
					for (Disk disk : usage.getDisks()) {
						level = MonitorUtil.checkSingleThreshold(level,
								this.getName(), disk.getFs(),
								(float) disk.getUse(), warningLevelMap);
						if (level.compareTo(WarningLevel.WARNING) >= 0) {
							break;
						}
					}
				}
				usage.setWarningLevel(level);
				usage.setMessage(level.getDetails());
				host.receivedStatistics(usage);
			} else {
				handleSnmpServiceDown();
			}
		} catch (NullPointerException e) {
			handleSnmpServiceDown();
		} catch (SnmpAccessException e) {
			handleSnmpAccessException(e.getMessage());
		}
	}
}
