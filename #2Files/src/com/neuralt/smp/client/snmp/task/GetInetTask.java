package com.neuralt.smp.client.snmp.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.Null;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.agent.mbean.model.HostInetStatus;
import com.neuralt.smp.agent.mbean.model.NetworkInterface;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.MonitorUtil;

public class GetInetTask extends SnmpGetTask {

	private static final Logger logger = LoggerFactory
			.getLogger(GetInetTask.class);

	private static final OID ifNumber = new OID(".1.3.6.1.2.1.2.1.0");
	private static final OID ifEntry = new OID(".1.3.6.1.2.1.2.2.1");
	private static final OID ifDescr = new OID(".1.3.6.1.2.1.2.2.1.2");
	private static final OID ifOperStatus = new OID(".1.3.6.1.2.1.2.2.1.8");
	private static final OID ifInOctets = new OID(".1.3.6.1.2.1.2.2.1.10");
	private static final OID ifOutOctets = new OID(".1.3.6.1.2.1.2.2.1.16");
	private static final String MY_NAME = "INET_MONITOR";
	private static final long INT32_MAX_VALUE = Math.round(Math.pow(2, 32) - 1);

	private long inetNumber;
	private static List<OID> list;
	private static Map<Integer, String> statusMap;
	static {
		list = new ArrayList<OID>();
		list.add(ifDescr);
		list.add(ifInOctets);
		list.add(ifOutOctets);
		list.add(ifOperStatus);
		statusMap = new HashMap<Integer, String>();
		statusMap.put(1, "UP");
		statusMap.put(2, "DOWN");
		statusMap.put(3, "TESTING");
		statusMap.put(4, "UNKNOWN");
		statusMap.put(5, "DORMANT");
		statusMap.put(6, "NOT PRESENT");
		statusMap.put(7, "LOWER LAYER DOWN");
	}

	private Map<Integer, Queue<InetStat>> map;
	private Map<Integer, InetStat> singleMap;
	private Map<Integer, NetworkInterface> inetMap;

	public GetInetTask(MonitoredHost host, SnmpGetStrategy util) {
		super(MY_NAME);
		this.host = host;
		this.config = host.getConfig().getInetMonitor();
		this.util = util;
	}

	@Override
	void init() {
		map = new HashMap<Integer, Queue<InetStat>>();
		singleMap = new HashMap<Integer, InetStat>();
		inetMap = new HashMap<Integer, NetworkInterface>();
		inetNumber = 0;
	}

	@Override
	void performTask() {
		try {
			singleMap.clear();
			List<VariableBinding> vbs = util.walk(ifEntry);
			final Object ifObject = util.get(ifNumber);
			if(ifObject instanceof Null)
				throw new NullPointerException();
			inetNumber = ((Integer32) ifObject).toLong();
			logger.debug(host.getName() + " inet number=" + inetNumber);
			if (inetNumber != 0) {
				for (VariableBinding vb : vbs) {
					Integer number = vb.getOid().last();
					if (singleMap.get(number) == null) {
						singleMap.put(number, new InetStat());
						singleMap.get(number).setTime(
								System.currentTimeMillis());
					}
					InetStat stat = singleMap.get(number);
					for (OID oid : list) {
						if (vb.getOid().leftMostCompare(oid.size(), oid) == 0) {
							if (oid.equals(ifDescr)) {
								stat.setDesc(vb.toValueString());
							} else if (oid.equals(ifInOctets)) {
								stat.setIn(Long.valueOf(vb.toValueString()));
							} else if (oid.equals(ifOutOctets)) {
								stat.setOut(Long.valueOf(vb.toValueString()));
							} else if (oid.equals(ifOperStatus)) {
								stat.setStatus(Integer.valueOf(vb
										.toValueString()));
								stat.setStatusText(statusMap.get(stat
										.getStatus()));
							}
							break;
						}
					}
				}
				for (Integer number : singleMap.keySet()) {
					if (map.get(number) == null) {
						map.put(number, new LinkedList<InetStat>());
					}
					Queue<InetStat> queue = map.get(number);
					InetStat stat = singleMap.get(number);
					// logger.debug("queue size="+queue.size());
					// logger.debug("stat="+stat.getDesc());
					if (queue.size() == 1) {
						InetStat first = queue.poll();
						NetworkInterface inet = new NetworkInterface();
						inet.setName(first.getDesc());
						inet.setIndex(number);
						inet.setInOctet(difference(first.getIn(), stat.getIn(),
								stat.getTime() - first.getTime()));
						inet.setOutOctet(difference(first.getOut(),
								stat.getOut(), stat.getTime() - first.getTime()));
						if (stat.getStatus() == 1) {
							inet.setStatus(0);
						} else {
							inet.setStatus(1);
						}
						inet.setStatusText(stat.getStatusText());
						inetMap.put(number, inet);
					}
					queue.add(stat);
				}
				if (!inetMap.isEmpty()) {
					if (inetNumber != inetMap.size()) {
						logger.error(
								"{} has data incompatible issue - inetNumber={}, inetMap size={}",
								new Object[] { host.getName(), inetNumber,
										inetMap.size() });
					}
					NetworkInterface[] inets = new NetworkInterface[(int) inetNumber];
					int i = 0;
					for (NetworkInterface dummy : inetMap.values()) {
						inets[i] = dummy;
						++i;
						if (i == inetNumber) {
							break;
						}
					}
					HostInetStatus usage = new HostInetStatus();
					usage.setInterfaces(inets);
					usage.setTimeStamp(System.currentTimeMillis());
					WarningLevel level = WarningLevel.NORMAL;
					Map<String, Map<Float, MonitorThresholdConfig>> warningLevelMap = MonitorUtil
							.getWarningLevelMap(config);
					if (warningLevelMap != null && usage != null) {
						for (NetworkInterface inet : usage.getInterfaces()) {
							level = MonitorUtil.checkSingleThreshold(level,
									this.getName(), inet.getName(),
									(float) inet.getStatus(), warningLevelMap);
							if (level.compareTo(WarningLevel.WARNING) >= 0) {
								break;
							}
						}
					}
					// here we only considered the highest warning level, leave
					// the individual checking in MonitoredHost
					usage.setWarningLevel(level);
					usage.setMessage(level.getDetails());
					logger.debug("Inet usage=" + usage);
					host.receivedStatistics(usage);
					inetMap.clear();
				}
			} else {
				handleSnmpServiceDown();
			}
		} catch (NullPointerException e) {
			handleSnmpServiceDown();
		} catch (SnmpAccessException e) {
			handleSnmpAccessException(e.getMessage());
		}
	}

	private float difference(long first, long second, long timediff) {
		if (second >= first) {
			return (second - first) / ((float) timediff / 1000) * 8 / 1000;
		} else {
			return (INT32_MAX_VALUE - first + second)
					/ ((float) timediff / 1000) * 8 / 1000;
		}
	}

	private class InetStat {
		private long in;
		private long out;
		private String desc;
		private int status;
		private String statusText;
		private long time;

		public long getIn() {
			return in;
		}

		public void setIn(long in) {
			this.in = in;
		}

		public long getOut() {
			return out;
		}

		public void setOut(long out) {
			this.out = out;
		}

		public String getDesc() {
			return desc;
		}

		public void setDesc(String desc) {
			this.desc = desc;
		}

		public int getStatus() {
			return status;
		}

		public void setStatus(int status) {
			this.status = status;
		}

		public String getStatusText() {
			return statusText;
		}

		public void setStatusText(String statusText) {
			this.statusText = statusText;
		}

		public long getTime() {
			return time;
		}

		public void setTime(long time) {
			this.time = time;
		}
	}

}
