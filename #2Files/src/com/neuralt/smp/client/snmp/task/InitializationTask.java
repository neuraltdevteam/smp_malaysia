package com.neuralt.smp.client.snmp.task;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.TimeTicks;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpConstants;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.config.HostConfig.MonitorType;
import com.neuralt.smp.util.StringUtil;

public class InitializationTask extends SnmpGetTask {

	private static final Logger logger = LoggerFactory
			.getLogger(InitializationTask.class);
	private static final String MY_NAME = "INITIALIZATION_TASK";

	public InitializationTask(MonitoredHost host, SnmpGetStrategy util) {
		super(MY_NAME);
		this.host = host;
		this.util = util;
	}

	@Override
	void init() {

	}

	@Override
	public void run() {
		try {
			boolean cpu = false;
			List<VariableBinding> devices = util
					.walk(SnmpConstants.hrDeviceType);
			for (VariableBinding vb : devices) {
				if (new OID(vb.toValueString())
						.equals(SnmpConstants.hrDeviceProcessor)) {
					cpu = true;
					break;
				}
			}
			boolean mem = false;
			Object memory = util.get(SnmpConstants.memTotalReal);
			try {
				long size = Long.valueOf(memory.toString());
				logger.debug("mem size:" + size);
				mem = true;
			} catch (NumberFormatException e) {
				// the host have no memory
				// OK to have silent error
			}
			boolean disk = false;
			List<VariableBinding> vbs = util.walk(SnmpConstants.dskEntry);
			for (VariableBinding vb : vbs) {
				Integer number = vb.getOid().last();
				if (number > 0) {
					disk = true;
					break;
				}
			}
			boolean uptime = false;
			try {
				TimeTicks time = (TimeTicks) util
						.get(SnmpConstants.hrSystemUptime);
				uptime = true;
			} catch (ClassCastException e) {
				// switch do not have this entry
				logger.debug("ClassCastException in hr sys uptime", e);
			}
			try {
				TimeTicks time = (TimeTicks) util.get(SnmpConstants.sysUptime);
				uptime = true;
			} catch (ClassCastException e) {
				logger.debug("ClassCastException in sys uptime", e);
			}

			try {
				String name = ((OctetString) util.get(SnmpConstants.sysName))
						.toString();
				if (!StringUtil.isNullOrEmpty(name)) {
					logger.debug(host.getConfig().getName() + " has name "
							+ name);
					host.setName(name);
				}
			} catch (ClassCastException e) {
				logger.debug("ClassCastException in sys name", e);
			}

			if (host.getConfig().getType().equals(MonitorType.SNMP)) {
				logger.debug(host.getFullName() + " has no SMP agent");
				host.getConfig().getPsMonitor().setEnabled(false);
			}

			if (!cpu) {
				logger.debug(host.getFullName() + " has no CPU");
				host.getConfig().getCPUMonitor().setEnabled(false);
			}
			if (!mem) {
				logger.debug(host.getFullName() + " has no Memory");
				host.getConfig().getMemoryMonitor().setEnabled(false);
			}
			if (!disk) {
				logger.debug(host.getFullName() + " has no Disk");
				host.getConfig().getDiskMonitor().setEnabled(false);
			}
			if (!uptime) {
				logger.debug(host.getFullName() + " has no Uptime info");
				host.getConfig().getUptimeMonitor().setEnabled(false);
			}
		} catch (SnmpAccessException e) {
			handleSnmpAccessException(e.getMessage());
		}
	}

	@Override
	void performTask() {

	}

}
