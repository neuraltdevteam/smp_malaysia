package com.neuralt.smp.client.snmp.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.agent.mbean.model.ProcessHealth;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpConstants;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.config.ProcessConfig;
import com.neuralt.smp.config.ProcessStatus;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.MonitorUtil;

public class MonitorProcessTask extends SnmpGetTask {

	private static final Logger logger = LoggerFactory
			.getLogger(MonitorProcessTask.class);
	private static final String MY_NAME = "MONITOR_PROCESS_TASK";

	private List<ProcessConfig> list;
	private Map<Integer, String> map;
	private Map<String, Integer> reverseMap;
	private Map<Integer, Queue<ProcessStat>> mapOfQueue;
	private Map<Integer, ProcessStat> currentStatMap;
	private long memSize;

	public MonitorProcessTask(MonitoredHost host, SnmpGetStrategy util) {
		super(MY_NAME);
		this.host = host;
		this.util = util;
		this.config = host.getConfig().getPsMonitor();
		list = new ArrayList<ProcessConfig>(host.getConfig()
				.getProcessConfigs());
		map = new HashMap<Integer, String>();
		reverseMap = new HashMap<String, Integer>();
		mapOfQueue = new HashMap<Integer, Queue<ProcessStat>>();
		currentStatMap = new HashMap<Integer, ProcessStat>();
	}

	@Override
	void init() {
		// TODO Auto-generated method stub

	}

	@Override
	void performTask() {
		currentStatMap.clear();
		try {
			memSize = ((Integer32) util.get(SnmpConstants.memTotalReal))
					.toLong();
			List<VariableBinding> names = util.walk(SnmpConstants.hrSWRunName);
			List<VariableBinding> cpus = util
					.walk(SnmpConstants.hrSWRunPerfCPU);
			List<VariableBinding> mems = util
					.walk(SnmpConstants.hrSWRunPerfMem);
			long time = System.currentTimeMillis();
			for (VariableBinding vb : names) {
				String procName = vb.toValueString();
				if (host.getProcess(procName) != null) {
					if (logger.isDebugEnabled())
						logger.debug("pid:" + vb.getOid().last() + " procName:"
								+ procName);
					if (!map.containsKey(vb.getOid().last())) {
						map.put(vb.getOid().last(), procName);
						if (reverseMap.containsKey(procName)) {
							// TODO
							// proc is restarted
							MonitoredProcess process = host
									.getProcess(procName);
							process.setStatus(ProcessStatus.RESTARTED);
							reverseMap.remove(procName);
						}
						reverseMap.put(procName, vb.getOid().last());
						// proc restarted?
					} else if (!map.get(vb.getOid().last()).equals(procName)) {
						// same pid with different name : involve two monitored
						// processes
						// one restarted, one unknown
						MonitoredProcess process = host.getProcess(procName);
						process.setStatus(ProcessStatus.RESTARTED);
					}
					ProcessStat stat = new ProcessStat();
					stat.setTime(time);
					stat.setName(procName);
					if (logger.isDebugEnabled())
						logger.debug("only time:" + stat);
					currentStatMap.put(vb.getOid().last(), stat);
				}
			}
			for (VariableBinding vb : cpus) {
				if (currentStatMap.containsKey(vb.getOid().last())) {
					currentStatMap.get(vb.getOid().last()).setCpu(
							Long.valueOf(vb.toValueString()));
					if (logger.isDebugEnabled())
						logger.debug("with cpu:"
								+ currentStatMap.get(vb.getOid().last())
										.toString());
				}
			}
			for (VariableBinding vb : mems) {
				if (currentStatMap.containsKey(vb.getOid().last())) {
					currentStatMap.get(vb.getOid().last()).setMem(
							Long.valueOf(vb.toValueString()));
					if (logger.isDebugEnabled())
						logger.debug("with mem:"
								+ currentStatMap.get(vb.getOid().last())
										.toString());
				}
			}
			for (Integer key : currentStatMap.keySet()) {
				if (!mapOfQueue.containsKey(key)) {
					mapOfQueue.put(key, new LinkedList<ProcessStat>());
				}
				mapOfQueue.get(key).add(currentStatMap.get(key));
			}
			for (Integer key : mapOfQueue.keySet()) {
				if (mapOfQueue.get(key).size() == 2) {
					ProcessStat prev = mapOfQueue.get(key).poll();
					ProcessStat now = mapOfQueue.get(key).peek();
					ProcessHealth health = new ProcessHealth();
					health.setPid(key.toString());
					health.setTimeStamp(now.getTime());
					health.setProcessName(now.getName());
					health.setMem((float) now.getMem() / memSize * 100);
					health.setCpu(calcCpuUsage(prev.getCpu(), now.getCpu(),
							now.getTime() - prev.getTime()));
					health.setStatus(0);
					WarningLevel level = WarningLevel.NORMAL;
					Map<String, Map<Float, MonitorThresholdConfig>> warningLevelMap = MonitorUtil
							.getWarningLevelMap(config);
					if (warningLevelMap != null
							&& health != null
							&& ProcessHealth.STATUS_PROCESS_RUNNING == health
									.getStatus()) {
						level = MonitorUtil.checkSingleThreshold(level,
								this.getName(), "cpu", health.getCpu(),
								warningLevelMap);
						level = MonitorUtil.checkSingleThreshold(level,
								this.getName(), "mem", health.getMem(),
								warningLevelMap);
					}
					health.setWarningLevel(level);
					health.setMessage(level.getDetails());
					if (logger.isDebugEnabled())
						logger.debug("health: " + health);
					host.receivedStatistics(health);
					// TODO
					// need to find out vsz, status
				}
			}
			for (ProcessConfig procCfg : host.getConfig().getProcessConfigs()) {
				if (!map.values().contains(procCfg.getName())) {
					ProcessHealth health = new ProcessHealth();
					health.setTimeStamp(time);
					health.setProcessName(procCfg.getName());
					health.setStatus(ProcessHealth.STATUS_PROCESS_NOT_RUNNING);
					health.setWarningLevel(WarningLevel.MAJOR);
					health.setMessage("[Process] " + health.getProcessName()
							+ " is not running");
					host.receivedStatistics(health);
				}
			}
		} catch (NullPointerException e) {
			handleSnmpServiceDown();
		} catch (SnmpAccessException e) {
			handleSnmpAccessException(e.getMessage());
		} catch (ClassCastException e) {
			handleSnmpServiceDown();
		}

	}

	private float calcCpuUsage(long first, long second, long time) {
		float ans;
		if (second >= first) {
			ans = (second - first) / ((float) time / 1000);
			return ans;
		} else {
			ans = (ConstantUtil.INT32_MAX_VALUE - first + second)
					/ ((float) time / 1000);
			return ans;
		}
	}

	public static class ProcessStat {
		private long cpu;
		private long mem;
		private String name;
		private long time;

		public long getCpu() {
			return cpu;
		}

		public void setCpu(long cpu) {
			this.cpu = cpu;
		}

		public long getMem() {
			return mem;
		}

		public void setMem(long mem) {
			this.mem = mem;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public long getTime() {
			return time;
		}

		public void setTime(long time) {
			this.time = time;
		}

		@Override
		public String toString() {
			StringBuilder sb = new StringBuilder();
			sb.append("ProcessStat: [name=").append(name).append(", time=")
					.append(time).append(", cpu=").append(cpu).append(", mem=")
					.append(mem).append("]");
			return sb.toString();
		}
	}
}
