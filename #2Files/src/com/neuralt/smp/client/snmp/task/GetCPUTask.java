package com.neuralt.smp.client.snmp.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.agent.mbean.model.CPU;
import com.neuralt.smp.agent.mbean.model.HostCPUUsage;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.MonitorUtil;

public class GetCPUTask extends SnmpGetTask {

	private static final Logger logger = LoggerFactory
			.getLogger(GetCPUTask.class);
	private static final String MY_NAME = "CPU_MONITOR";
	private static final OID systemStats = new OID(".1.3.6.1.4.1.2021.11");
	private static final OID ssCpuRawUser = new OID(".1.3.6.1.4.1.2021.11.50.0");
	private static final OID ssCpuRawSystem = new OID(
			".1.3.6.1.4.1.2021.11.52.0");
	private static final OID ssCpuRawIdle = new OID(".1.3.6.1.4.1.2021.11.53.0");
	private static final OID ssCpuRawWait = new OID(".1.3.6.1.4.1.2021.11.54.0");
	private static final OID hrDeviceDescr = new OID(".1.3.6.1.2.1.25.3.2.1.3");
	private static final OID hrDeviceProcessor = new OID("1.3.6.1.2.1.25.3.1.3");
	private static final OID hrDeviceType = new OID("1.3.6.1.2.1.25.3.2.1.2");

	private static final long INT32_MAX_VALUE = Math.round(Math.pow(2, 32) - 1);

	private Queue<CpuIntStat> queue;
	private static List<OID> list;
	private Map<OID, Long> map;
	private int cpuNumber;

	static {
		list = new ArrayList<OID>();
		list.add(ssCpuRawUser);
		list.add(ssCpuRawSystem);
		list.add(ssCpuRawIdle);
		list.add(ssCpuRawWait);
	}

	public GetCPUTask(MonitoredHost host, SnmpGetStrategy util) {
		super(MY_NAME);
		this.host = host;
		this.config = host.getConfig().getCPUMonitor();
		this.util = util;
	}

	@Override
	void init() {
		queue = new LinkedList<CpuIntStat>();
		map = new HashMap<OID, Long>();
	}

	@Override
	void performTask() {
		map.clear();
		try {
			List<VariableBinding> vbs = util.getList(list);
			if (cpuNumber == 0) {
				// List<VariableBinding> devices = util.walk(hrDeviceDescr);
				List<VariableBinding> type = util.walk(hrDeviceType);
				for (VariableBinding vb : type) {
					if (new OID(vb.toValueString()).equals(hrDeviceProcessor)) {
						++cpuNumber;
					}
				}
			}
			for (VariableBinding vb : vbs) {
				map.put(vb.getOid(), Long.valueOf(vb.toValueString()));
			}
			CpuIntStat stat = new CpuIntStat();
			stat.setIdle(map.get(ssCpuRawIdle));
			stat.setUsr(map.get(ssCpuRawUser));
			stat.setSys(map.get(ssCpuRawSystem));
			stat.setIowait(map.get(ssCpuRawWait));
			stat.setTime(System.currentTimeMillis());
			queue.add(stat);
			if ((queue.size() == 2) && (cpuNumber != 0)) {
				CPU cpu = new CPU();
				cpu.setProcessor("all");
				CpuIntStat first = queue.poll();
				long timeDiff = stat.getTime() - first.getTime();
				// logger.debug("first:"+first);
				// logger.debug("stat:"+stat);
				// logger.debug("time diff:"+timeDiff);
				cpu.setIdle(difference(first.getIdle(), stat.getIdle(),
						timeDiff));
				cpu.setUsr(difference(first.getUsr(), stat.getUsr(), timeDiff));
				cpu.setSys(difference(first.getSys(), stat.getSys(), timeDiff));
				cpu.setIowait(difference(first.getIowait(), stat.getIowait(),
						timeDiff));
				// logger.debug("cpu:"+cpu);
				CPU[] cpus = new CPU[1];
				cpus[0] = cpu;
				HostCPUUsage usage = new HostCPUUsage();
				usage.setCPUS(cpus);
				usage.setTimeStamp(System.currentTimeMillis());
				WarningLevel level = WarningLevel.NORMAL;
				Map<String, Map<Float, MonitorThresholdConfig>> warningLevelMap = MonitorUtil
						.getWarningLevelMap(config);
				if (warningLevelMap != null && usage != null) {
					for (CPU usageCpu : usage.getCPUS()) {
						Float currentValue = usageCpu.getUsr();
						level = MonitorUtil.checkSingleThreshold(level,
								this.getName(), usageCpu.getProcessor(),
								currentValue, warningLevelMap);
					}
				}
				usage.setWarningLevel(level);
				usage.setMessage(level.getDetails());
				logger.debug("{} CPU usage={}", host.getName(), usage);
				host.receivedStatistics(usage);
			}
		} catch (NullPointerException e) {
			handleSnmpServiceDown();
		} catch (SnmpAccessException e) {
			handleSnmpAccessException(e.getMessage());
		}
	}

	private float difference(long first, long second, long time) {
		float ans;
		if (second >= first) {
			ans = (second - first) / ((float) time / 1000);
			ans = ans / cpuNumber;
			ans = ans;
			return ans;
		} else {
			ans = (INT32_MAX_VALUE - first + second) / ((float) time / 1000);
			ans = ans / cpuNumber;
			ans = ans;
			return ans;
		}
	}

	private class CpuIntStat {
		private long usr;
		private long sys;
		private long iowait;
		private long idle;
		private long time;

		public long getUsr() {
			return usr;
		}

		public void setUsr(long usr) {
			this.usr = usr;
		}

		public long getSys() {
			return sys;
		}

		public void setSys(long sys) {
			this.sys = sys;
		}

		public long getIowait() {
			return iowait;
		}

		public void setIowait(long iowait) {
			this.iowait = iowait;
		}

		public long getIdle() {
			return idle;
		}

		public void setIdle(long idle) {
			this.idle = idle;
		}

		public long getTime() {
			return time;
		}

		public void setTime(long time) {
			this.time = time;
		}

		@Override
		public String toString() {
			return "CpuIntStat [usr=" + usr + ", sys=" + sys + ", iowait="
					+ iowait + ", idle=" + idle + ", time=" + time + "]";
		}

	}

}
