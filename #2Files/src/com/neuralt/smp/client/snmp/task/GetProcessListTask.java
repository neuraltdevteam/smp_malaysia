package com.neuralt.smp.client.snmp.task;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpConstants;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;

public class GetProcessListTask extends SnmpGetTask {
	private static final String MY_NAME = "PROCESS_MONITOR";
	private static final Logger logger = LoggerFactory
			.getLogger(GetProcessListTask.class);

	public GetProcessListTask(MonitoredHost host, SnmpGetStrategy util) {
		super(MY_NAME);
		this.host = host;
		this.util = util;
	}

	public List<String> getProcessList() {
		List<VariableBinding> names = null;
		try {
			names = util.walk(SnmpConstants.hrSWRunName);
			List<String> list = new ArrayList<String>();
			for (VariableBinding vb : names) {
				String procName = vb.toValueString();
				list.add(procName);
			}
			return list;
		} catch (SnmpAccessException e) {
			handleSnmpAccessException(e.getMessage());
			logger.debug("failed to get process list for:" + host.getName());
		}
		return null;
	}

	@Override
	void init() {
		// TODO Auto-generated method stub

	}

	@Override
	void performTask() {
		// TODO Auto-generated method stub

	}

}
