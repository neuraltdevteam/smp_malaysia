package com.neuralt.smp.client.snmp.task;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.TimeTicks;

import com.neuralt.smp.agent.mbean.model.HostUptime;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpConstants;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.MonitorUtil;

/*
 * This class is using SNMP GET to obtain system uptime. If not available, it will try to obtain
 * network manage portion of the system was last re-initialized instead.
 * Both figures are using Integer32/Counter32, which will be overflowed after approximately 
 * 497 days.
 */
public class GetUptimeTask extends SnmpGetTask {

	private static final Logger logger = LoggerFactory
			.getLogger(GetUptimeTask.class);
	private static final String MY_NAME = "UPTIME_MONITOR";

	public GetUptimeTask(MonitoredHost host, SnmpGetStrategy util) {
		super(MY_NAME);
		this.host = host;
		this.config = host.getConfig().getUptimeMonitor();
		this.util = util;
	}

	@Override
	void init() {

	}

	@Override
	void performTask() {
		boolean ok = false;
		HostUptime usage = new HostUptime();
		usage.setTimeStamp(System.currentTimeMillis());
		try {
			TimeTicks time = (TimeTicks) util.get(SnmpConstants.hrSystemUptime);
			usage.setDuration(time.toString());
			usage.setDays((float) time.toMilliseconds() / ConstantUtil.DAY_MS);
			WarningLevel level = WarningLevel.INFO;
			Map<String, Map<Float, MonitorThresholdConfig>> warningLevelMap = MonitorUtil
					.getWarningLevelMap(config);
			if (warningLevelMap != null && usage != null) {
				Float currentValue = usage.getDays();
				level = MonitorUtil.checkSingleThreshold(level, this.getName(),
						ConstantUtil.UPTIME_MONITOR_TARGET, currentValue,
						warningLevelMap);
			}
			usage.setWarningLevel(level);
			usage.setMessage(level.getDetails());
			host.receivedStatistics(usage);
			host.setHostUp();
			// logger.debug("time value="+time.getValue());
			logger.debug(host.getName() + " system uptime:" + time.toString());
			ok = true;
		} catch (NullPointerException e) {
			logger.debug("NullPointerException in sys uptime", e);
			handleSnmpServiceDown();
		} catch (ClassCastException e) {
			logger.debug("ClassCastException in sys uptime", e);
		} catch (SnmpAccessException e) {
			handleSnmpAccessException(e.getMessage());
		}
		if (!ok) {
			try {
				TimeTicks time = (TimeTicks) util.get(SnmpConstants.sysUptime);
				if (time != null) {
					usage.setDuration(time.toString());
					usage.setDays((float) time.toMilliseconds()
							/ ConstantUtil.DAY_MS);
					WarningLevel level = WarningLevel.INFO;
					Map<String, Map<Float, MonitorThresholdConfig>> warningLevelMap = MonitorUtil
							.getWarningLevelMap(config);
					if (warningLevelMap != null && usage != null) {
						Float currentValue = usage.getDays();
						level = MonitorUtil.checkSingleThreshold(level,
								this.getName(),
								ConstantUtil.UPTIME_MONITOR_TARGET,
								currentValue, warningLevelMap);
					}
					usage.setWarningLevel(level);
					usage.setMessage(level.getDetails());
					host.receivedStatistics(usage);
					host.setHostUp();
					// logger.debug("time value="+time.getValue());
					logger.debug(host.getName() + " network uptime:"
							+ time.toString());
					ok = true;
				}
			} catch (NullPointerException e) {
				logger.debug("NullPointerException in network uptime", e);
				host.setHostDown();
				handleSnmpServiceDown();
			} catch (ClassCastException e) {
				logger.debug("ClassCastException in network uptime", e);
			} catch (SnmpAccessException e) {
				host.setHostDown();
				handleSnmpAccessException(e.getMessage());
			}
		}
	}

}
