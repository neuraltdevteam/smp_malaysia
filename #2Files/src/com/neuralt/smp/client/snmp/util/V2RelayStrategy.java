package com.neuralt.smp.client.snmp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.neuralt.smp.client.data.GroupSnmpConfig;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.SnmpTrap.Variable;
import com.neuralt.smp.snmp.proxy.SvrSnmpConstants;

public class V2RelayStrategy implements SnmpRelayStrategy {

	private static final Logger logger = LoggerFactory
			.getLogger(V2RelayStrategy.class);

	@Override
	public void sendPDU(SnmpTrap trap, NotificationGroup group) {
		try {
			TransportMapping transport = new DefaultUdpTransportMapping();
			transport.listen();
			Snmp snmp = new Snmp(transport);

			for (GroupSnmpConfig config : group.getSnmpConfigs()) {
				// create target
				CommunityTarget cTarget = new CommunityTarget();
				cTarget.setCommunity(new OctetString(config.getCommunityTrap()));
				cTarget.setVersion(SnmpConstants.version2c);
				StringBuilder sb = new StringBuilder();
				sb.append(config.getIp()).append("/").append(config.getPort());
				cTarget.setAddress(new UdpAddress(sb.toString()));
				cTarget.setRetries(2);
				cTarget.setTimeout(5000);

				// generate PDU
				PDU pdu = new PDU();
				pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID(
						trap.getOid())));
				pdu.add(new VariableBinding(SvrSnmpConstants.trapAlarmId,
						new OctetString(trap.getTrapAlarmId())));
				pdu.add(new VariableBinding(SvrSnmpConstants.trapAlarmLevel,
						new OctetString(trap.getTrapAlarmLevel())));
				pdu.add(new VariableBinding(SvrSnmpConstants.trapAppName,
						new OctetString(trap.getTrapAppName())));
				pdu.add(new VariableBinding(SvrSnmpConstants.trapDescription,
						new OctetString(trap.getTrapDescription())));
				for (Variable variable : trap.getVariables()) {
					pdu.add(new VariableBinding(new OID(variable.getOid()),
							new OctetString(variable.getValue())));
				}
				pdu.setType(PDU.TRAP);
				// send the PDU
				logger.info("Sending V2 Trap to " + cTarget.getAddress());
				snmp.send(pdu, cTarget);
			}
			snmp.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
