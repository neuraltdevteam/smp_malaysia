package com.neuralt.smp.client.snmp.util;

import java.util.List;

import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.client.snmp.SnmpAccessException;

public interface SnmpGetStrategy {

	public static int VERSION_1 = SnmpConstants.version1;
	public static int VERSION_2C = SnmpConstants.version2c;
	public static int VERSION_3 = SnmpConstants.version3;

	public abstract Object get(OID oid) throws SnmpAccessException;

	public abstract List<VariableBinding> walk(OID oid)
			throws SnmpAccessException;

	public abstract List<VariableBinding> getbulk(OID oid)
			throws SnmpAccessException;

	public abstract List<VariableBinding> getList(List<OID> oid_list)
			throws SnmpAccessException;

	public abstract void closeSnmp();
}
