package com.neuralt.smp.client.snmp.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.Null;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.Variable;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.web.util.jsf.WebUtil;

public class V2GetStrategy implements SnmpGetStrategy {
	private static final Logger logger = LoggerFactory
			.getLogger(V2GetStrategy.class);

	private static int snmpVersion = SnmpGetStrategy.VERSION_2C;
	private String transportProtocol = "UDP";
	private CommunityTarget target;
	private Address address;
	private Snmp snmp;
	private int timeout = 3000;
	private int retry = 3;

	public V2GetStrategy(String host, String community, int port) {
		try {
			TransportMapping transport;
			if (transportProtocol.equalsIgnoreCase("UDP")) {
				transport = new DefaultUdpTransportMapping();
			} else {
				transport = new DefaultTcpTransportMapping();
			}
			snmp = new Snmp(transport);
			snmp.listen();
		} catch (IOException e) {
			logger.error("Failed to create SNMP GET strategy", e);
		}
		target = new CommunityTarget();
		if (community != null)
			target.setCommunity(new OctetString(community));
		address = GenericAddress.parse(transportProtocol + ":" + host + "/"
				+ port);
		target.setAddress(address);
		target.setVersion(snmpVersion);
		target.setTimeout(timeout);
		target.setRetries(retry);
	}

	public Object get(OID oid) throws SnmpAccessException {
		Object ret = null;
		try {
			// create PDU
			PDU pdu = new PDU();
			pdu.setType(PDU.GET);
			pdu.addOID(new VariableBinding(oid));
			pdu.setNonRepeaters(0);
			ResponseEvent responseEvent = snmp.send(pdu, target);
			PDU respPDU = responseEvent.getResponse();

			Vector<? extends VariableBinding> vbs = null;

			if (respPDU != null) {
				vbs = respPDU.getVariableBindings();
				if (vbs.size() > 0) {
					VariableBinding vb = (VariableBinding) vbs.get(0);
					ret = vb.getVariable();
				} else {
					ret = null;
				}
			} else {
				throw new SnmpAccessException(
						"Invalid reponse. Remote SNMP agent might be down.");
			}
		} catch (IOException ex) {
			throw new SnmpAccessException(ex);
		}
		return ret;
	}

	public List<VariableBinding> getList(List<OID> list)
			throws SnmpAccessException {
		List<VariableBinding> ret = new ArrayList<VariableBinding>();
		try {
			// create the PDU
			PDU pdu = new PDU();
			pdu.setType(PDU.GET);
			// put the oids you want to get
			List<VariableBinding> ivbs = new ArrayList<VariableBinding>();
			for (OID o : list) {
				ivbs.add(new VariableBinding(o));
			}
			pdu.addAll(ivbs.toArray(new VariableBinding[] {}));
			pdu.setMaxRepetitions(10);
			pdu.setNonRepeaters(0);
			// send the PDU
			ResponseEvent responseEvent = snmp.send(pdu, target);
			// extract the response PDU (could be null if timed out)
			PDU responsePDU = responseEvent.getResponse();
			if (responsePDU == null) {
				throw new SnmpAccessException(
						WebUtil.getMessage(MessageKey.NO_SNMP_RESPONSE));
			} else {
				Vector vbs = responsePDU.getVariableBindings();
				if (vbs.size() > 0) {
					List<OID> rec_oid = new ArrayList<OID>();
					for (int i = 0; i < vbs.size(); i++) {
						VariableBinding v = (VariableBinding) vbs.get(i);
						if (!rec_oid.contains(v.getOid())) {
							rec_oid.add(v.getOid());
							ret.add((VariableBinding) vbs.get(i));
						}
					}
				}
			}
		} catch (Exception e) {
			throw new SnmpAccessException(e.getMessage(), e);
		}
		return ret;
	}

	public boolean set(OID oid, Variable value) throws SnmpAccessException {
		boolean ret = false;
		try {
			// create PDU
			PDU pdu = new PDU();
			pdu.setType(PDU.SET);
			pdu.add(new VariableBinding(oid, value));
			pdu.setNonRepeaters(0);
			ResponseEvent resp = snmp.set(pdu, target);
			PDU responsePDU = resp.getResponse();
			if (responsePDU == null) {
				throw new SnmpAccessException(WebUtil.getMsgBundle().getString(
						MessageKey.NO_SNMP_RESPONSE));
			} else {
				Vector vbs = responsePDU.getVariableBindings();
				if (vbs.size() > 0) {
					VariableBinding vb = (VariableBinding) vbs.get(0);
					ret = vb.isException() ? false : true;
				} else {
					ret = false;
				}
			}
		} catch (Exception e) {
			throw new SnmpAccessException(e);
		}
		return ret;
	}

	public List<VariableBinding> walk(OID oid) throws SnmpAccessException {
		List<VariableBinding> ret = new ArrayList<VariableBinding>();
		PDU requestPDU = new PDU();
		requestPDU.add(new VariableBinding(oid));
		requestPDU.setType(PDU.GETNEXT);
		try {
			boolean finished = false;
			while (!finished) {
				VariableBinding vb = null;
				ResponseEvent respEvt = snmp.send(requestPDU, target);
				PDU responsePDU = respEvt.getResponse();
				if (responsePDU == null) {
					throw new SnmpAccessException(WebUtil.getMsgBundle()
							.getString(MessageKey.NO_SNMP_RESPONSE));
				} else {
					if (responsePDU != null
							&& responsePDU.getErrorStatus() == 0) {
						vb = responsePDU.get(0);
					}
					if (responsePDU.getErrorStatus() != 0) {
						finished = true;
						throw new SnmpAccessException(
								responsePDU.getErrorStatusText());
					} else if (vb.getOid() == null) {
						finished = true;
					} else if (vb.getOid().size() < oid.size()) {
						finished = true;
					} else if (oid.leftMostCompare(oid.size(), vb.getOid()) != 0) {
						finished = true;
					} else if (Null.isExceptionSyntax(vb.getVariable()
							.getSyntax())) {
						finished = true;
					} else if (vb.getOid().compareTo(oid) <= 0) {
						finished = true;
					} else {
						ret.add(vb);
						// Set up the variable binding for the next entry.
						requestPDU.setRequestID(new Integer32(0));
						requestPDU.set(0, vb);
					}
					if (responsePDU != null) {
						vb = responsePDU.get(0);
					}
				}
			}
		} catch (IOException e) {
			throw new SnmpAccessException(e);
		}
		return ret;
	}

	public void closeSnmp() {
		try {
			snmp.close();
		} catch (IOException e) {
			logger.error("failed in closing SNMP", e);
		}
	}

	public VariableBinding getNext(OID oid) throws SnmpAccessException {
		VariableBinding ret = null;
		try {
			// create the PDU
			PDU pdu = new PDU();
			pdu.setType(PDU.GETNEXT);
			// put the oid you want to get
			pdu.add(new VariableBinding(oid));
			pdu.setNonRepeaters(0);
			// send the PDU
			ResponseEvent responseEvent = snmp.send(pdu, target);
			// extract the response PDU (could be null if timed out)
			PDU responsePDU = responseEvent.getResponse();
			if (responsePDU == null) {
				throw new SnmpAccessException(WebUtil.getMsgBundle().getString(
						MessageKey.NO_SNMP_RESPONSE));
			} else {
				Vector vbs = responsePDU.getVariableBindings();
				if (vbs.size() > 0) {
					ret = (VariableBinding) vbs.get(0);
				}
			}
		} catch (IOException e) {
			throw new SnmpAccessException(e);
		}
		return ret;
	}

	@Override
	public List<VariableBinding> getbulk(OID oid) throws SnmpAccessException {
		List<VariableBinding> ret = new ArrayList<VariableBinding>();
		PDU requestPDU = new PDU();
		requestPDU.add(new VariableBinding(oid));
		requestPDU.setType(PDU.GETBULK);
		requestPDU.setNonRepeaters(0);
		requestPDU.setMaxRepetitions(1000);
		try {
			ResponseEvent respEvt = snmp.getBulk(requestPDU, target);
			PDU responsePDU = respEvt.getResponse();
			if (responsePDU == null) {
				throw new SnmpAccessException(WebUtil.getMsgBundle().getString(
						MessageKey.NO_SNMP_RESPONSE));
			} else {
				Vector vbs = responsePDU.getVariableBindings();
				if (vbs.size() > 0) {
					for (Object obj : vbs.toArray()) {
						VariableBinding vb = (VariableBinding) obj;
						if (vb.getOid().startsWith(oid))
							ret.add(vb);
					}
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}
}
