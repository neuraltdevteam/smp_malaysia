package com.neuralt.smp.client.snmp.util;

import org.snmp4j.smi.OID;

public class SnmpConstants {

	public static final String udpTransport = "UDP";

	// get index/desc of processor and network
	public static final String hrDeviceTable = "1.3.6.1.2.1.25.3.2";

	public static final String hrDeviceIndex = "1.3.6.1.2.1.25.3.2.1.1";
	public static final OID hrDeviceType = new OID("1.3.6.1.2.1.25.3.2.1.2");
	public static final OID hrDeviceDescr = new OID(".1.3.6.1.2.1.25.3.2.1.3");
	public static final OID memTotalReal = new OID(".1.3.6.1.4.1.2021.4.5.0");
	public static final OID dskEntry = new OID(".1.3.6.1.4.1.2021.9.1");
	public static final OID hrSystemUptime = new OID(".1.3.6.1.2.1.25.1.1.0");
	public static final OID sysName = new OID(".1.3.6.1.2.1.1.5.0");
	public static final OID sysUptime = new OID(".1.3.6.1.2.1.1.3.0");
	// device types
	public static final OID hrDeviceProcessor = new OID("1.3.6.1.2.1.25.3.1.3");
	public static final String hrDeviceNetwork = "1.3.6.1.2.1.25.3.1.4";

	public static final OID svrAP = new OID(".1.3.6.1.4.1.18697");

	// memory
	public static final OID hrMemorySize = new OID(".1.3.6.1.2.1.25.2.2.0");

	// process
	public static final OID hrSWRunTable = new OID(".1.3.6.1.2.1.25.4.2");
	public static final OID hrSWRunIndex = new OID(".1.3.6.1.2.1.25.4.2.1.1");
	public static final OID hrSWRunName = new OID(".1.3.6.1.2.1.25.4.2.1.2");
	public static final OID hrSWRunPerfEntry = new OID(".1.3.6.1.2.1.25.5.1.1");
	public static final OID hrSWRunPerfMem = new OID(".1.3.6.1.2.1.25.5.1.1.2");
	public static final OID hrSWRunPerfCPU = new OID(".1.3.6.1.2.1.25.5.1.1.1");

}
