package com.neuralt.smp.client.snmp.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.PDU;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.UserTarget;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.Integer32;
import org.snmp4j.smi.Null;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.neuralt.smp.client.LiveMonitorReporter;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

public class V3GetStrategy implements SnmpGetStrategy {

	private static final Logger logger = LoggerFactory
			.getLogger(V3GetStrategy.class);
	private String transportProtocol = "UDP";
	private MonitoredHost host;
	private OctetString username = null;
	private OID auth;
	private OctetString authPassword;
	private OID priv;
	private OctetString privPassword;
	private int securityLevel;
	private Snmp snmp;
	private UserTarget target;
	private int timeout = 3000;
	private int retries = 3;
	private Address address;

	public V3GetStrategy(String ip, int port, String username, OID auth,
			String authPassword, OID priv, String privPassword) {
		securityLevel = SecurityLevel.NOAUTH_NOPRIV;
		if (!StringUtil.isNullOrEmpty(username)) {
			this.username = new OctetString(username);
		}
		if (!StringUtil.isNullOrEmpty(authPassword)) {
			this.authPassword = new OctetString(authPassword);
			this.auth = auth;
			securityLevel = SecurityLevel.AUTH_NOPRIV;
		}
		if (!StringUtil.isNullOrEmpty(privPassword)) {
			this.privPassword = new OctetString(privPassword);
			this.priv = priv;
			securityLevel = SecurityLevel.AUTH_PRIV;
		}
		try {
			snmp = createSnmpSession();
			snmp.listen();
		} catch (IOException e) {
		}
		address = GenericAddress.parse(transportProtocol + ":" + ip + "/"
				+ port);
		target = new UserTarget();
		target.setAddress(address);
		target.setVersion(SnmpGetStrategy.VERSION_3);// org.snmp4j.mp.*;
		target.setSecurityLevel(securityLevel);
		target.setSecurityName(this.username);
		target.setTimeout(timeout);
		target.setRetries(retries);
	}

	@Override
	public Object get(OID oid) throws SnmpAccessException {
		Object ret = null;
		try {
			// create PDU
			PDU pdu = new ScopedPDU();
			pdu.setType(PDU.GET);
			pdu.addOID(new VariableBinding(oid));
			pdu.setNonRepeaters(0);

			ResponseEvent responseEvent = snmp.send(pdu, target);
			// logger.debug("Check exception:" + responseEvent.getError());
			PDU responsePDU = responseEvent.getResponse();
			if (responsePDU == null) {
				throw new SnmpAccessException(
						WebUtil.getMessage(MessageKey.NO_SNMP_RESPONSE));
			} else if (responsePDU.getType() == PDU.REPORT) {
				printReport(responsePDU);
			} else {
				if (responsePDU.getErrorStatus() != PDU.noError) {
					handleSnmpError(responsePDU.getErrorStatus());
				}
				Vector vbs = responsePDU.getVariableBindings();
				if (vbs.size() > 0) {
					VariableBinding vb = (VariableBinding) vbs.get(0);
					ret = vb.getVariable();
				} else {
					ret = null;
				}
			}
		} catch (IOException e) {
			throw new SnmpAccessException(e);
		}
		return ret;
	}

	@Override
	public List<VariableBinding> walk(OID oid) throws SnmpAccessException {
		List<VariableBinding> ret = new ArrayList<VariableBinding>();
		PDU requestPDU = new ScopedPDU();
		requestPDU.add(new VariableBinding(oid));
		requestPDU.setType(PDU.GETNEXT);
		try {
			boolean finished = false;
			while (!finished) {
				VariableBinding vb = null;
				ResponseEvent responseEvent = null;
				responseEvent = snmp.send(requestPDU, target);
				// logger.debug("Check exception:" + responseEvent.getError());
				PDU responsePDU = responseEvent.getResponse();
				if (responsePDU == null) {
					throw new SnmpAccessException(
							WebUtil.getMessage(MessageKey.NO_SNMP_RESPONSE));
				} else if (responsePDU.getType() == PDU.REPORT) {
					printReport(responsePDU);
					break;
				}
				// logger.debug("response PDU error:"+responsePDU.getErrorStatus());
				if (responsePDU != null) {
					vb = responsePDU.get(0);
				}
				if (responsePDU.getErrorStatus() != PDU.noError) {
					finished = true;
					handleSnmpError(responsePDU.getErrorStatus());
				} else if (vb.getOid() == null) {
					finished = true;
				} else if (vb.getOid().size() < oid.size()) {
					finished = true;
				} else if (oid.leftMostCompare(oid.size(), vb.getOid()) != 0) {
					finished = true;
				} else if (Null.isExceptionSyntax(vb.getVariable().getSyntax())) {
					finished = true;
				} else if (vb.getOid().compareTo(oid) <= 0) {
					finished = true;
				} else {
					ret.add(vb);
					// Set up the variable binding for the next entry.
					requestPDU.setRequestID(new Integer32(0));
					requestPDU.set(0, vb);
				}
			}
		} catch (IOException e) {
			throw new SnmpAccessException(e);
		}
		return ret;
	}

	@Override
	public List<VariableBinding> getList(List<OID> list)
			throws SnmpAccessException {
		List<VariableBinding> ret = new ArrayList<VariableBinding>();
		try {
			// create the PDU
			PDU pdu = new ScopedPDU();
			pdu.setType(PDU.GET);
			// put the oids you want to get
			List<VariableBinding> ivbs = new ArrayList<VariableBinding>();
			for (OID o : list) {
				ivbs.add(new VariableBinding(o));
			}
			pdu.addAll(ivbs.toArray(new VariableBinding[] {}));
			pdu.setMaxRepetitions(10);
			pdu.setNonRepeaters(0);
			// send the PDU
			ResponseEvent responseEvent = snmp.send(pdu, target);
			// extract the response PDU (could be null if timed out)
			// logger.debug("Check exception:" + responseEvent.getError());
			PDU responsePDU = responseEvent.getResponse();
			if (responsePDU == null) {
				throw new SnmpAccessException(
						WebUtil.getMessage(MessageKey.NO_SNMP_RESPONSE));
			} else {
				if (responsePDU.getType() == PDU.REPORT)
					printReport(responsePDU);
				// logger.debug("response PDU error:"+responsePDU.getErrorStatus());
				if (responsePDU.getErrorStatus() != PDU.noError) {
					handleSnmpError(responsePDU.getErrorStatus());
				}
				Vector vbs = responsePDU.getVariableBindings();
				if (vbs.size() > 0) {
					List<OID> rec_oid = new ArrayList<OID>();
					for (int i = 0; i < vbs.size(); i++) {
						VariableBinding v = (VariableBinding) vbs.get(i);
						if (!rec_oid.contains(v.getOid())) {
							rec_oid.add(v.getOid());
							ret.add((VariableBinding) vbs.get(i));
						}
					}
				}
			}
		} catch (Exception e) {
			throw new SnmpAccessException(e.getMessage(), e);
		}

		return ret;
	}

	@Override
	public void closeSnmp() {
		try {
			snmp.close();
		} catch (IOException e) {
			logger.error("failed in closing SNMP", e);
		}
	}

	private Snmp createSnmpSession() throws IOException {
		TransportMapping transport;
		transport = new DefaultUdpTransportMapping();
		Snmp snmp = new Snmp(transport);
		USM usm = new USM(SecurityProtocols.getInstance(), new OctetString(
				MPv3.createLocalEngineID()), 0);
		SecurityModels.getInstance().addSecurityModel(usm);
		UsmUser user = new UsmUser(username, auth, authPassword, priv,
				privPassword);
		snmp.getUSM().addUser(username, user);
		return snmp;
	}

	private void handleSnmpError(int errorStatus) {
		String message = PDU.toErrorStatusText(errorStatus);
		CentralizedEventService.getInstance().onRemoteHostSnmpError(host,
				message);
		LiveMonitorReporter.getInstance().addGrowlMessage(message);
	}

	/*
	 * copied from org.snmp4j.tools.console.SnmpRequest TODO revise for what
	 * level of error info should be disclosed may need to modify message to be
	 * user-friendly
	 */
	private void printReport(PDU response) throws SnmpAccessException {
		if (response.size() < 1) {
			System.out
					.println("REPORT PDU does not contain a variable binding.");
			return;
		}

		VariableBinding vb = response.get(0);
		OID oid = vb.getOid();
		if (SnmpConstants.usmStatsUnsupportedSecLevels.equals(oid)) {
			throw new SnmpAccessException("REPORT: Unsupported Security Level.");
		} else if (SnmpConstants.usmStatsNotInTimeWindows.equals(oid)) {
			throw new SnmpAccessException(
					"REPORT: Message not within time window.");
		} else if (SnmpConstants.usmStatsUnknownUserNames.equals(oid)) {
			throw new SnmpAccessException("REPORT: Unknown user name.");
		} else if (SnmpConstants.usmStatsUnknownEngineIDs.equals(oid)) {
			throw new SnmpAccessException("REPORT: Unknown engine id.");
		} else if (SnmpConstants.usmStatsWrongDigests.equals(oid)) {
			throw new SnmpAccessException("REPORT: Wrong digest.");
		} else if (SnmpConstants.usmStatsDecryptionErrors.equals(oid)) {
			throw new SnmpAccessException("REPORT: Decryption error.");
		} else if (SnmpConstants.snmpUnknownSecurityModels.equals(oid)) {
			throw new SnmpAccessException("REPORT: Unknown security model.");
		} else if (SnmpConstants.snmpInvalidMsgs.equals(oid)) {
			throw new SnmpAccessException("REPORT: Invalid message.");
		} else if (SnmpConstants.snmpUnknownPDUHandlers.equals(oid)) {
			throw new SnmpAccessException("REPORT: Unknown PDU handler.");
		} else if (SnmpConstants.snmpUnavailableContexts.equals(oid)) {
			throw new SnmpAccessException("REPORT: Unavailable context.");
		} else if (SnmpConstants.snmpUnknownContexts.equals(oid)) {
			throw new SnmpAccessException("REPORT: Unknown context.");
		} else {
			throw new SnmpAccessException("REPORT contains unknown OID ("
					+ oid.toString() + ").");
		}
	}

	@Override
	public List<VariableBinding> getbulk(OID oid) throws SnmpAccessException {
		List<VariableBinding> ret = new ArrayList<VariableBinding>();
		PDU requestPDU = new PDU();
		requestPDU.add(new VariableBinding(oid));
		requestPDU.setType(PDU.GETBULK);
		requestPDU.setNonRepeaters(0);
		requestPDU.setMaxRepetitions(1000);
		try {
			ResponseEvent respEvt = snmp.getBulk(requestPDU, target);
			PDU responsePDU = respEvt.getResponse();
			if (responsePDU == null) {
				throw new SnmpAccessException(WebUtil.getMsgBundle().getString(
						MessageKey.NO_SNMP_RESPONSE));
			} else {
				Vector vbs = responsePDU.getVariableBindings();
				if (vbs.size() > 0) {
					for (Object obj : vbs.toArray()) {
						VariableBinding vb = (VariableBinding) obj;
						if (vb.getOid().startsWith(oid))
							ret.add(vb);
					}
				}
			}

		} catch (IOException e) {
			logger.debug("failed in SNMP getbulk", e);
		}
		return ret;
	}

}
