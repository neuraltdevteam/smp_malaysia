package com.neuralt.smp.client.snmp.util;

import java.io.IOException;
import java.util.Map.Entry;

import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.PDUv1;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.UserTarget;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultTcpTransportMapping;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.neuralt.smp.client.data.GroupSnmpConfig;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.snmp.model.TrapRawObject;
import com.neuralt.smp.snmp.proxy.SvrSnmpConstants;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;

/*
 * This class acts as the service for relaying traps.
 * Default transport protocol is UDP.
 */
public class TrapRelayer {

	private static final String transportProtocol = ConstantUtil.UDP;

	private static TrapRelayer instance;

	private TrapRelayer() {
	}

	public static TrapRelayer getInstance() {
		if (instance == null) {
			synchronized (TrapRelayer.class) {
				if (instance == null) {
					instance = new TrapRelayer();
				}
			}
		}
		return instance;
	}

	public void sendTrap(NotificationGroup group, TrapRawObject raw) {
		for (GroupSnmpConfig config : group.getSnmpConfigs()) {
			switch (config.getVersion()) {
			case SNMPV1:
				PDUv1 v1Pdu = getV1Pdu(raw);
				sendV1Trap(config, v1Pdu);
				break;
			case SNMPV2:

				PDU v2Pdu = getV2Pdu(raw);
				sendV2Trap(config, v2Pdu);
				break;
			case SNMPV3:
				ScopedPDU v3Pdu = getV3Pdu(raw);
				sendV3Trap(config, v3Pdu);
				break;
			}
		}
	}

	public void sendV1Trap(GroupSnmpConfig config, PDUv1 pdu) {
		try {
			TransportMapping transport = null;
			if (transportProtocol.equals(ConstantUtil.UDP)) {
				transport = new DefaultUdpTransportMapping();
			} else {
				transport = new DefaultTcpTransportMapping();
				transport.listen();
			}
			Snmp snmp = new Snmp(transport);

			// create target
			CommunityTarget cTarget = new CommunityTarget();
			cTarget.setCommunity(new OctetString(config.getCommunityTrap()));
			cTarget.setVersion(SnmpConstants.version1);
			StringBuilder sb = new StringBuilder();
			sb.append(config.getIp()).append("/").append(config.getPort());
			cTarget.setAddress(new UdpAddress(sb.toString()));
			cTarget.setRetries(5);
			cTarget.setTimeout(1000);

			snmp.send(pdu, cTarget);
			snmp.close();
		} catch (Exception e) {
			handleSnmpError(config.getIp(), e.getMessage());
		}
	}

	public void sendV2Trap(GroupSnmpConfig config, PDU pdu) {
		try {
			TransportMapping transport = null;
			if (transportProtocol.equals(ConstantUtil.UDP)) {
				transport = new DefaultUdpTransportMapping();
			} else {
				transport = new DefaultTcpTransportMapping();
				transport.listen();
			}
			Snmp snmp = new Snmp(transport);

			// create target
			CommunityTarget cTarget = new CommunityTarget();
			cTarget.setCommunity(new OctetString(config.getCommunityTrap()));
			cTarget.setVersion(SnmpConstants.version2c);
			StringBuilder sb = new StringBuilder();
			sb.append(config.getIp()).append("/").append(config.getPort());
			cTarget.setAddress(new UdpAddress(sb.toString()));
			cTarget.setRetries(5);
			cTarget.setTimeout(1000);

			snmp.send(pdu, cTarget);

			snmp.close();
		} catch (Exception e) {
			handleSnmpError(config.getIp(), e.getMessage());
		}
	}

	public void sendV3Trap(GroupSnmpConfig config, ScopedPDU pdu) {
		Snmp snmp = createV3Snmp(config);
		UserTarget target = createV3UserTarget(config);

		pdu.setContextEngineID(new OctetString(config.getEngineId()));
		try {
			snmp.send(pdu, target);
		} catch (IOException e) {
			handleSnmpError(config.getIp(), e.getMessage());
		}
	}

	private Snmp createV3Snmp(GroupSnmpConfig config) {
		OctetString username = null;
		OID auth = null;
		OctetString authPassword = null;
		OID priv = null;
		OctetString privPassword = null;
		Snmp snmp;
		TransportMapping transport = null;
		try {
			if (transportProtocol.equals(ConstantUtil.UDP)) {
				transport = new DefaultUdpTransportMapping();
			} else {
				transport = new DefaultTcpTransportMapping();
				transport.listen();
			}
		} catch (Exception e) {
			handleSnmpError(config.getIp(), e.getMessage());
		}
		if (!StringUtil.isNullOrEmpty(config.getUsername()))
			username = new OctetString(config.getUsername());
		if (!StringUtil.isNullOrEmpty(config.getAuthPassword())) {
			authPassword = new OctetString(config.getAuthPassword());
			auth = config.getAuth().getProtocol();
		}
		if (!StringUtil.isNullOrEmpty(config.getPrivPassword())) {
			privPassword = new OctetString(config.getPrivPassword());
			priv = config.getPriv().getProtocol();
		}

		snmp = new Snmp(transport);
		USM usm = new USM(SecurityProtocols.getInstance(), new OctetString(
				config.getEngineId()), 0);
		SecurityModels.getInstance().addSecurityModel(usm);
		UsmUser user = new UsmUser(username, auth, authPassword, priv,
				privPassword);
		snmp.getUSM().addUser(username, user);

		return snmp;
	}

	private UserTarget createV3UserTarget(GroupSnmpConfig config) {
		OctetString username = null;
		int securityLevel;
		UserTarget target;

		securityLevel = SecurityLevel.NOAUTH_NOPRIV;
		if (!StringUtil.isNullOrEmpty(config.getUsername()))
			username = new OctetString(config.getUsername());
		if (!StringUtil.isNullOrEmpty(config.getAuthPassword())) {
			securityLevel = SecurityLevel.AUTH_NOPRIV;
		}
		if (!StringUtil.isNullOrEmpty(config.getPrivPassword())) {
			securityLevel = SecurityLevel.AUTH_PRIV;
		}

		Address address = GenericAddress.parse(transportProtocol + ":"
				+ config.getIp() + "/" + config.getPort());
		target = new UserTarget();
		target.setAddress(address);
		target.setVersion(SnmpConstants.version3);
		target.setSecurityLevel(securityLevel);
		target.setSecurityName(username);
		target.setTimeout(1000);
		target.setRetries(5);
		return target;
	}

	private PDUv1 getV1Pdu(TrapRawObject raw) {
		PDU pdu = new PDUv1();
		generatePDUfromRaw(raw, pdu);
		pdu.setType(PDU.V1TRAP);
		return (PDUv1) pdu;
	}

	private void generatePDUfromRaw(TrapRawObject raw, PDU pdu) {
		pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, raw.getOid()));
		if (raw.getTrapAlarmId() != null)
			pdu.add(new VariableBinding(SvrSnmpConstants.trapAlarmId,
					new OctetString(raw.getTrapAlarmId())));
		if (raw.getTrapAlarmLevel() != null)
			pdu.add(new VariableBinding(SvrSnmpConstants.trapAlarmLevel,
					new OctetString(raw.getTrapAlarmLevel())));
		if (raw.getTrapAppName() != null)
			pdu.add(new VariableBinding(SvrSnmpConstants.trapAppName,
					new OctetString(raw.getTrapAppName())));
		if (raw.getAlarmDescription() != null)
			pdu.add(new VariableBinding(SvrSnmpConstants.trapDescription,
					new OctetString(raw.getAlarmDescription())));
		boolean forward = false;
		for (Entry<String, String> entry : raw.getVariables().entrySet()) {
			OID key = new OID(entry.getKey());
			pdu.add(new VariableBinding(key, new OctetString(entry.getValue())));
			if (key.equals(SnmpConstants.snmpTrapAddress)) {
				forward = true;
			}
		}
		if (!forward && raw.getSourceIP() != null) {
			pdu.add(new VariableBinding(SnmpConstants.snmpTrapAddress,
					new OctetString(raw.getSourceIP())));
		}
	}

	private PDU getV2Pdu(TrapRawObject raw) {
		PDU pdu = new PDU();
		generatePDUfromRaw(raw, pdu);
		pdu.setType(PDU.TRAP);
		return pdu;
	}

	private ScopedPDU getV3Pdu(TrapRawObject raw) {
		PDU pdu = new ScopedPDU();
		generatePDUfromRaw(raw, pdu);
		pdu.setType(PDU.TRAP);
		return (ScopedPDU) pdu;
	}

	private void handleSnmpError(String ip, String detail) {
		StringBuilder sb = new StringBuilder();
		sb.append("Failed in relaying Trap to host:").append(ip)
				.append(", detail:").append(detail);
		CentralizedEventService.getInstance().onSysGeneralError(sb.toString());
	}
}
