package com.neuralt.smp.client.snmp.util;

import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.data.SnmpTrap;

public interface SnmpRelayStrategy {

	public abstract void sendPDU(SnmpTrap trap, NotificationGroup group);
}
