package com.neuralt.smp.client.snmp.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.PDU;
import org.snmp4j.ScopedPDU;
import org.snmp4j.Snmp;
import org.snmp4j.TransportMapping;
import org.snmp4j.UserTarget;
import org.snmp4j.mp.MPv3;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.security.SecurityLevel;
import org.snmp4j.security.SecurityModels;
import org.snmp4j.security.SecurityProtocols;
import org.snmp4j.security.USM;
import org.snmp4j.security.UsmUser;
import org.snmp4j.smi.Address;
import org.snmp4j.smi.GenericAddress;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;

import com.neuralt.smp.client.data.GroupSnmpConfig;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.SnmpTrap.Variable;
import com.neuralt.smp.config.model.BaseSnmpConfig.AuthProtocol;
import com.neuralt.smp.config.model.BaseSnmpConfig.PrivProtocol;
import com.neuralt.smp.snmp.proxy.SvrSnmpConstants;
import com.neuralt.smp.util.ConstantUtil;

public class V3RelayStrategy implements SnmpRelayStrategy {

	private static final Logger logger = LoggerFactory
			.getLogger(V3RelayStrategy.class);
	private int timeout = 3000;
	private int retries = 2;

	@Override
	public void sendPDU(SnmpTrap trap, NotificationGroup group) {
		for (GroupSnmpConfig snmpConfig : group.getSnmpConfigs()) {
			try {
				AuthProtocol auth = snmpConfig.getAuth();
				OctetString authPassword = null;
				OID authOid = null;
				PrivProtocol priv = snmpConfig.getPriv();
				OctetString privPassword = null;
				OID privOid = null;
				OctetString username = new OctetString(snmpConfig.getUsername());
				int securityLevel = SecurityLevel.NOAUTH_NOPRIV;
				if (auth != AuthProtocol.NONE) {
					authOid = auth.getProtocol();
					authPassword = new OctetString(snmpConfig.getAuthPassword());
					securityLevel = SecurityLevel.AUTH_NOPRIV;
				}
				if (priv != PrivProtocol.NONE) {
					privOid = priv.getProtocol();
					privPassword = new OctetString(snmpConfig.getPrivPassword());
					securityLevel = SecurityLevel.AUTH_PRIV;
				}
				TransportMapping transport;
				transport = new DefaultUdpTransportMapping();
				Snmp snmp = new Snmp(transport);
				USM usm = new USM(SecurityProtocols.getInstance(),
						new OctetString(MPv3.createLocalEngineID()), 0);
				SecurityModels.getInstance().addSecurityModel(usm);
				UsmUser user = new UsmUser(username, authOid, authPassword,
						privOid, privPassword);
				snmp.getUSM().addUser(username, user);
				snmp.listen();
				Address address = GenericAddress.parse(ConstantUtil.UDP + ":"
						+ snmpConfig.getIp() + "/" + snmpConfig.getPort());
				UserTarget target = new UserTarget();
				target.setAddress(address);
				target.setVersion(SnmpGetStrategy.VERSION_3);// org.snmp4j.mp.*;
				target.setSecurityLevel(securityLevel);
				target.setSecurityName(username);
				target.setTimeout(timeout);
				target.setRetries(retries);
				PDU pdu = new ScopedPDU();
				pdu.add(new VariableBinding(SnmpConstants.snmpTrapOID, new OID(
						trap.getOid())));
				pdu.add(new VariableBinding(SvrSnmpConstants.trapAlarmId,
						new OctetString(trap.getTrapAlarmId())));
				pdu.add(new VariableBinding(SvrSnmpConstants.trapAlarmLevel,
						new OctetString(trap.getTrapAlarmLevel())));
				pdu.add(new VariableBinding(SvrSnmpConstants.trapAppName,
						new OctetString(trap.getTrapAppName())));
				pdu.add(new VariableBinding(SvrSnmpConstants.trapDescription,
						new OctetString(trap.getTrapDescription())));
				for (Variable variable : trap.getVariables()) {
					pdu.add(new VariableBinding(new OID(variable.getOid()),
							new OctetString(variable.getValue())));
				}
				pdu.setType(PDU.TRAP);
				snmp.send(pdu, target);
			} catch (Exception e) {

			}
		}
	}

}
