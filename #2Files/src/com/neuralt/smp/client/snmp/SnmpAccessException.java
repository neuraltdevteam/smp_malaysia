package com.neuralt.smp.client.snmp;

public class SnmpAccessException extends Exception {

	public SnmpAccessException(String msg) {
		super(msg);
	}

	public SnmpAccessException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public SnmpAccessException(Throwable arg0) {
		super(arg0.getMessage(), arg0);
	}

	public SnmpAccessException() {
		super();
	}

}
