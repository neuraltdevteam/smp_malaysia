package com.neuralt.smp.client.snmp;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.snmp.task.GetCPUTask;
import com.neuralt.smp.client.snmp.task.GetDiskTask;
import com.neuralt.smp.client.snmp.task.GetInetTask;
import com.neuralt.smp.client.snmp.task.GetMemoryTask;
import com.neuralt.smp.client.snmp.task.GetUptimeTask;
import com.neuralt.smp.client.snmp.task.InitializationTask;
import com.neuralt.smp.client.snmp.task.SnmpGetTask;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.client.snmp.util.V1GetStrategy;
import com.neuralt.smp.client.snmp.util.V2GetStrategy;
import com.neuralt.smp.client.snmp.util.V3GetStrategy;
import com.neuralt.smp.config.HostSnmpConfig;
import com.neuralt.smp.config.model.BaseSnmpConfig.SnmpVersion;

public class SnmpTaskHolder {

	private static final Logger logger = LoggerFactory
			.getLogger(SnmpTaskHolder.class);
	private List<SnmpGetTask> tasks;
	private MonitoredHost host;
	private SnmpGetStrategy util;
	private boolean started;

	public SnmpTaskHolder(MonitoredHost host) {
		logger.debug("starting new obj...host=" + host);
		this.host = host;
	}

	public void addTask(SnmpGetTask task) {
		tasks.add(task);
		task.start();
	}

	public void startTask() {
		tasks = new CopyOnWriteArrayList<SnmpGetTask>();
		util = null;
		if (host.getConfig().getSnmpConfig().getVersion()
				.equals(SnmpVersion.SNMPV1)) {
			util = new V1GetStrategy(host.getConfig().getIpAddr(), host
					.getConfig().getSnmpConfig().getCommunityGet(), host
					.getConfig().getSnmpConfig().getPort());
		} else if (host.getConfig().getSnmpConfig().getVersion()
				.equals(SnmpVersion.SNMPV2)) {
			util = new V2GetStrategy(host.getConfig().getIpAddr(), host
					.getConfig().getSnmpConfig().getCommunityGet(), host
					.getConfig().getSnmpConfig().getPort());
		} else if (host.getConfig().getSnmpConfig().getVersion()
				.equals(SnmpVersion.SNMPV3)) {
			HostSnmpConfig config = host.getConfig().getSnmpConfig();
			util = new V3GetStrategy(host.getConfig().getIpAddr(),
					config.getPort(), config.getUsername(), config.getAuth()
							.getProtocol(), config.getAuthPassword(), config
							.getPriv().getProtocol(), config.getPrivPassword());
		}
		SnmpGetTask initialTask = new InitializationTask(host, util);
		initialTask.start();
		while (initialTask.isAlive()) {
			try {
				Thread.sleep(1000);
			} catch (InterruptedException ignore) {
			}
		}
		if (host.getConfig().getInetMonitor().isEnabled())
			tasks.add(new GetInetTask(host, util));
		if (host.getConfig().getMemoryMonitor().isEnabled())
			tasks.add(new GetMemoryTask(host, util));
		if (host.getConfig().getCPUMonitor().isEnabled())
			tasks.add(new GetCPUTask(host, util));
		if (host.getConfig().getDiskMonitor().isEnabled())
			tasks.add(new GetDiskTask(host, util));
		if (host.getConfig().getUptimeMonitor().isEnabled())
			tasks.add(new GetUptimeTask(host, util));
		// if (host.getConfig().getPsMonitor().isEnabled()) {
		// tasks.add(new MonitorProcessTask(host, util));
		// }
		logger.debug("tasks added...host=" + host.getName());
		logger.debug("Snmp Task Holder start called.");
		for (SnmpGetTask task : tasks) {
			task.start();
		}

		started = true;
	}

	public void stopTask() {

		for (SnmpGetTask task : tasks) {
			task.quit();
			task.interrupt();
		}

		started = false;
	}

	public SnmpGetStrategy getUtil() {
		return util;
	}

	public boolean isStarted() {
		return started;
	}

}
