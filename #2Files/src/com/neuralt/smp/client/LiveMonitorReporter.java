package com.neuralt.smp.client;

import java.util.List;
import java.util.NavigableSet;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.faces.application.FacesMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.EventLog;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.web.AppConfig;

/**
 * 
 * Data container for the most recent 10 events and alarms
 * 
 * @author Kin Cheung
 *
 */
public class LiveMonitorReporter {
	private static final Logger logger = LoggerFactory
			.getLogger(LiveMonitorReporter.class);

	private static final int MOST_RECENT_RECORD = 10;
	private NavigableSet<EventLog> hotEvents;
	private NavigableSet<SnmpTrap> hotTraps;
	private NavigableSet<Alarm> hotAlarms;

	private List<FacesMessage> growlMessages;

	private static LiveMonitorReporter instance;

	private LiveMonitorReporter() {
		this.hotEvents = new ConcurrentSkipListSet<EventLog>();
		this.hotTraps = new ConcurrentSkipListSet<SnmpTrap>();
		this.hotAlarms = new ConcurrentSkipListSet<Alarm>();
		growlMessages = new CopyOnWriteArrayList<FacesMessage>();
	}

	public static LiveMonitorReporter getInstance() {
		if (instance == null) {
			synchronized (LiveMonitorReporter.class) {
				if (instance == null) {
					try {
						instance = new LiveMonitorReporter();
					} catch (Exception e) {
						logger.error(
								"failed in initializing LiveMonitorReporter", e);
					}
				}
			}
		}

		return instance;
	}

	public NavigableSet<EventLog> getHotEvents() {
		return hotEvents;
	}

	public void addHotEvent(EventLog hotEvent) {
		if (hotEvents.size() >= MOST_RECENT_RECORD)
			hotEvents.pollFirst();

		hotEvents.add(hotEvent);
	}

	public NavigableSet<SnmpTrap> getHotTraps() {
		return hotTraps;
	}

	public void addHotTrap(SnmpTrap hotAlarm) {
		if (hotTraps.size() >= 10)
			hotTraps.pollFirst();

		hotTraps.add(hotAlarm);
	}

	public NavigableSet<Alarm> getHotAlarms() {
		return hotAlarms;
	}

	public synchronized void addHotAlarm(Alarm hotAlarm) {
		// FIXME
		// temporary solution: ConcurrentSkipListSet seems do not call equals(),
		// but calls compareTo() instead
		// ie. the set may contain duplicated elements
		boolean ok = true;
		Alarm dummy = null;
		for (Alarm alarm : hotAlarms) {
			if (alarm.equals(hotAlarm)) {
				dummy = alarm;
				ok = false;
				break;
			}
		}
		if (ok) {
			if (hotAlarms.size() >= MOST_RECENT_RECORD)
				hotAlarms.pollFirst();
			hotAlarms.add(hotAlarm);
		} else {
			if (hotAlarm.compareTo(dummy) > 0) {
				hotAlarms.remove(dummy);
				hotAlarms.add(hotAlarm);
			}
		}

		// if (hotAlarms.add(hotAlarm)) {
		// logger.debug("hotAlarm added in reporter: id="+hotAlarm.getId()+" ,name:"+hotAlarm.getDisplayableSource());
		// }
	}

	public void addGrowlMessage(String message) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
				message, "");
		addGrowlMessage(msg);
	}

	public void addGrowlMessage(SnmpTrap entity) {
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN,
				String.format("Trap %s %s", entity.getTrapName(),
						entity.getDisplayableSource()), "");
		addGrowlMessage(msg);
	}

	private void addGrowlMessage(FacesMessage message) {
		boolean added = false;
		if (growlMessages.size() >= AppConfig.getGrowlMessageBuffer()) {
			synchronized (growlMessages) {
				if (growlMessages.size() >= AppConfig.getGrowlMessageBuffer()) {
					growlMessages.remove(0);
					growlMessages.add(message);
					added = true;
				}
			}
		}
		if (!added)
			growlMessages.add(message);
	}

	public synchronized List<FacesMessage> getGrowlMessages() {
		return growlMessages;
	}

}
