package com.neuralt.smp.client.web;

import java.net.MalformedURLException;

import com.neuralt.smp.client.MonitoredHost;

public class SelectedMonitoredHost extends MonitoredHost {

	private static final long serialVersionUID = 6636923743290704079L;
	private boolean selected;

	public SelectedMonitoredHost(MonitoredHost host)
			throws MalformedURLException {
		super(host.getConfig(), host.getSys());
		selected = false;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
