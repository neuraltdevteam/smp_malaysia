package com.neuralt.smp.client.web.security;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = { "ROLEID", "TYPEID" }))
public class UserRight implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum AccessLevel {
		NONE, READ, WRITE
	}

	@Id
	@GeneratedValue
	private Integer id;

	@Transient
	private String roleId;

	@ManyToOne
	@JoinColumn(name = "ROLEID", nullable = false)
	@org.hibernate.annotations.ForeignKey(name = "FK_USERRIGHT_USERROLE")
	private UserRole userRole;

	@Column(length = 50, nullable = false)
	@Enumerated(EnumType.STRING)
	private ScreenType typeId;

	@Column(length = 10, nullable = false)
	@Enumerated(EnumType.STRING)
	private AccessLevel accessLevel;

	private Timestamp createDate;
	@Column(length = 30)
	private String lastUpdatedBy;
	@Version
	private Timestamp lastUpdatedOn;

	public UserRight() {
		createDate = new Timestamp(System.currentTimeMillis());
	}

	public UserRight(ScreenType typeId, AccessLevel accessLevel,
			String lastUpdatedBy) {
		this.typeId = typeId;
		this.accessLevel = accessLevel;
		this.lastUpdatedBy = lastUpdatedBy;
		this.createDate = new Timestamp(System.currentTimeMillis());
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	/**
	 * If maintaining bidirectional relationship of Java object is needed, use
	 * {@link UserRole#addUserRight(UserRight)}. If for performance, use this
	 * method.
	 */
	public UserRole getUserRole() {
		return userRole;
	}

	public void setUserRole(UserRole userRole) {
		this.userRole = userRole;
	}

	public ScreenType getTypeId() {
		return typeId;
	}

	public void setTypeId(ScreenType typeId) {
		this.typeId = typeId;
	}

	public AccessLevel getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(AccessLevel accessLevel) {
		this.accessLevel = accessLevel;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Timestamp getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Timestamp lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("roleId: " + roleId + ";");
		buffer.append("typeId: " + typeId + ";");
		buffer.append("accessLevel: " + accessLevel + ";");
		if (createDate != null) {
			buffer.append("createDate: " + createDate.toString());
		}
		return buffer.toString();
	}
}