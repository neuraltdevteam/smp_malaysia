package com.neuralt.smp.client.web.security;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "PASSWORD_HISTORY")
public class PasswordHistory implements Serializable {

	@Id
	@GeneratedValue
	private Long id;
	@Column(length = 50, nullable = false)
	private String password;
	@Version
	@Column(nullable = false)
	private Timestamp lastUpdatedOn;
	@Column(length = 30)
	private String lastUpdatedBy;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "USERID", nullable = false)
	@org.hibernate.annotations.ForeignKey(name = "FK_PASSWORD_CONSOLEUSER")
	private ConsoleUser consoleUser;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Timestamp updatedTime) {
		this.lastUpdatedOn = updatedTime;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public ConsoleUser getConsoleUser() {
		return consoleUser;
	}

	public void setConsoleUser(ConsoleUser consoleUser) {
		this.consoleUser = consoleUser;
	}

}
