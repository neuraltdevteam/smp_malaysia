package com.neuralt.smp.client.web.security;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.security.auth.login.LoginContext;

import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ConsoleUser.Status;
import com.neuralt.smp.client.web.security.UserRight.AccessLevel;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean(name = "UserBean")
@SessionScoped
public class UserBean implements Serializable {

	private static class LoginResult {
		public LoginRC rc;
		public ConsoleUser user;

		LoginResult(LoginRC rc, ConsoleUser loggedInUser) {
			this.rc = rc;
			this.user = loggedInUser;
		}
	}

	private static enum LoginRC {
		INVALID_USER, INVALID_PASSWORD, INACTIVE_ACCOUNT, ERROR,

		SUCCESS
	}

	private static final Logger logger = LoggerFactory
			.getLogger(UserBean.class);

	private static final long serialVersionUID = 1L;

	private String userId;
	private String password;
	private ConsoleUser user;
	private String reason;
	private Locale locale = Locale.ENGLISH;
	private LoginContext lc;

	private Map<ScreenType, AccessLevel> userAccessMap = null;

	private static final Map<String, ScreenType> pageToTypeId;

	static {
		pageToTypeId = new ConcurrentHashMap<String, ScreenType>();
		pageToTypeId.put("/pages/home.xhtml", ScreenType.MONITOR);
		pageToTypeId.put("/pages/test.xhtml", ScreenType.COMMAND);
		pageToTypeId.put("/pages/user", ScreenType.MANAGE_USER);
		pageToTypeId.put("/pages/monitor", ScreenType.MONITOR);
		pageToTypeId.put("/pages/commands", ScreenType.COMMAND);
		pageToTypeId.put("/pages/config", ScreenType.CONFIG);
		pageToTypeId.put("/pages/changePassword", ScreenType.FREE); // XXX FIXME
																	// this not
																	// yet
																	// enforced
																	// privilege
		pageToTypeId.put("/pages/index", ScreenType.FREE);
		pageToTypeId.put("/pages/error", ScreenType.FREE);
		pageToTypeId.put("/pages/blc/reports", ScreenType.FREE);
		pageToTypeId.put("/pages/blc/trace", ScreenType.FREE);
		
		pageToTypeId.put("/pages/packs/config", ScreenType.FREE);

		// XXX FIXME temporarily i make the following page FREE, to be changed
		// after SYS_DEFAULT role has been assigned
		// a suitable user right
		// pageToTypeId.put("/pages/ruleEngine", ScreenType.FREE);
	}

	public void createAdminUser() {
		if (isZeroUser()) {
			UserGroup group = new UserGroup();
			group.setGroupId(ConstantUtil.SUPER_GROUP);
			group.setGroupName(ConstantUtil.SUPER_GROUP);
			UserRole role = new UserRole();
			role.setRoleId(ConstantUtil.SUPER_ROLE);
			role.setRoleName(ConstantUtil.SUPER_ROLE);
			UserRight commandRight = new UserRight();
			commandRight.setAccessLevel(AccessLevel.WRITE);
			commandRight.setRoleId(ConstantUtil.SUPER_ROLE);
			commandRight.setTypeId(ScreenType.COMMAND);
			UserRight configRight = new UserRight();
			configRight.setAccessLevel(AccessLevel.WRITE);
			configRight.setRoleId(ConstantUtil.SUPER_ROLE);
			configRight.setTypeId(ScreenType.CONFIG);
			UserRight manageUserRight = new UserRight();
			manageUserRight.setAccessLevel(AccessLevel.WRITE);
			manageUserRight.setRoleId(ConstantUtil.SUPER_ROLE);
			manageUserRight.setTypeId(ScreenType.MANAGE_USER);
			UserRight monitorRight = new UserRight();
			monitorRight.setAccessLevel(AccessLevel.WRITE);
			monitorRight.setRoleId(ConstantUtil.SUPER_ROLE);
			monitorRight.setTypeId(ScreenType.MONITOR);
			UserRight freeRight = new UserRight();
			freeRight.setAccessLevel(AccessLevel.WRITE);
			freeRight.setRoleId(ConstantUtil.SUPER_ROLE);
			freeRight.setTypeId(ScreenType.FREE);
			group.addUserRole(role);
			role.addUserGroup(group);
			role.addUserRight(commandRight);
			role.addUserRight(configRight);
			role.addUserRight(manageUserRight);
			role.addUserRight(monitorRight);
			role.addUserRight(freeRight);
			ConsoleUser user = new ConsoleUser();
			group.addConsoleUser(user);
			user.setName("default");
			user.setStatus(Status.ACTIVE);
			user.setUserId(ConstantUtil.SYS_DEFAULT);
			try {
				user.setPassword(CryptUtil.encrypt("a12345"));
			} catch (Exception e) {

			}
			try {
				DataAccess.webDao.saveUserRole(role);
			} catch (Exception e) {

			}
			try {
				DataAccess.webDao.saveUserGroup(group);
			} catch (Exception e) {

			}
			try {
				DataAccess.webDao.saveUser(user);
			} catch (Exception e) {

			}
		}
	}

	public void onChangePasswordClick() {
		try {
			ExternalContext extContext = FacesContext.getCurrentInstance()
					.getExternalContext();
			String url = extContext.getRequestContextPath()
					+ "/pages/changePassword.xhtml";
			extContext.redirect(url);
		} catch (IOException e) {
			logger.error("failed to redirect", e);
		}
	}

	public boolean isZeroUser() {
		boolean ok = false;
		try {
			List<ConsoleUser> users = DataAccess.webDao.getUsers();
			ok = users == null || users.size() == 0;
			return ok;
		} catch (Exception e) {
			return false;
		}
	}

	public boolean isLoggedIn() {
		return user != null;
	}

	public String logout() {
		logger.debug("start to logout");
		FacesContext fctx = FacesContext.getCurrentInstance();
		if (user != null) {
			AuditLogger.logAuditTrail(ScreenType.FREE,
					WebUtil.getRequestPath(), ActionType.LOGOUT, "logout",
					user.getUserId());
			// AuditLogger.logAuditTrail(ScreenId.INDEX, ActionCategory.MISC,
			// ActionType.LOGOUT, true, "user", user.getUserId(), null,
			// null, "");
			user = null;
			fctx.getExternalContext().invalidateSession();
			logger.debug("logged out");
		}
		return "logged_out";
	}

	public ConsoleUser getUser() {
		return user;
	}

	public void setUser(ConsoleUser user) {
		this.user = user;
	}

	public void checkLogin(ComponentSystemEvent event) {
		if (isLoggedIn()) {
			FacesContext context = FacesContext.getCurrentInstance();
			ConfigurableNavigationHandler handler = (ConfigurableNavigationHandler) context
					.getApplication().getNavigationHandler();
			handler.performNavigation("/pages/index.xhtml?faces-redirect=true");
		}
	}

	public String debugLogin() {
		// throw new DatabaseException("test exception handling");
		userId = "SYS_DEFAULT";
		ConsoleUser consoleUser = DataAccess.webDao.getUser(userId);
		if (consoleUser == null) {
			userId = "admin";
		}
		password = "abc123";
		return attemptLogin();
	}

	public void attemptLogin(String remoteUser) {

		if (!isLoggedIn()) {
			LoginResult loginResult = attemptLogin(remoteUser, true);
			switch (loginResult.rc) {
			case SUCCESS:
				this.user = loginResult.user;
				logger.debug("attemptLogin SUCCESS, user = {}", user);
				AuditLogger.logAuditTrail(ScreenType.FREE,
						WebUtil.getRequestPath(), ActionType.LOGIN,
						"login success", user.getUserId());
				// AuditLogger.logAuditTrail(ScreenId.INDEX,
				// ActionCategory.MISC, ActionType.LOGIN, true, "user", userId,
				// null, null, "");
				break;
			case INACTIVE_ACCOUNT:
				logger.debug(
						"attemptLogin FAILURE, INACTIVE_ACCOUNT,  user = {}",
						user);
				addErrorMsg("general_error_inactive");
				AuditLogger
						.logAuditTrail(ScreenType.FREE,
								WebUtil.getRequestPath(), ActionType.LOGIN,
								"login failed", user.getUserId()
										+ ": inactive account");
				// AuditLogger.logAuditTrail(ScreenId.INDEX,
				// ActionCategory.MISC, ActionType.LOGIN, false, "user", userId,
				// "INACTIVE_ACCOUNT", null, "");
				break;
			case ERROR:
				logger.debug("attemptLogin FAILURE, ERROR, user = {}", user);
				addErrorMsg("db_error_reason_other");
				AuditLogger.logAuditTrail(ScreenType.FREE,
						WebUtil.getRequestPath(), ActionType.LOGIN,
						"login failed", user.getUserId() + ": internal error");
				// AuditLogger.logAuditTrail(ScreenId.INDEX,
				// ActionCategory.MISC, ActionType.LOGIN, false, "user", userId,
				// "ERROR", null, "");
				break;
			default:
				logger.debug("attemptLogin FAILURE, default, user = {}", user);
				addErrorMsg("general_error_invalid");
				AuditLogger.logAuditTrail(ScreenType.FREE,
						WebUtil.getRequestPath(), ActionType.LOGIN,
						"login failed", this.userId);
				// AuditLogger.logAuditTrail(ScreenId.INDEX,
				// ActionCategory.MISC, ActionType.LOGIN, false, "user", userId,
				// "INVALID USER OR PASSWORD", null, "");
				break;
			}
			userId = null;
			password = null;
		}

	}

	public String attemptLogin() {
		LoginResult loginResult = attemptLogin(userId, password);
		logger.debug("String attemptLogin: userId: {}, password: {}", userId,
				password);
		String outcome = null;
		switch (loginResult.rc) {
		case SUCCESS:
			this.user = loginResult.user;
			logger.debug("attemptLogin SUCCESS, user = {}", user);
			AuditLogger.logAuditTrail(ScreenType.FREE,
					WebUtil.getRequestPath(), ActionType.LOGIN,
					"login success", user.getUserId());
			// AuditLogger.logAuditTrail(ScreenId.INDEX, ActionCategory.MISC,
			// ActionType.LOGIN, true, "user", userId, null, null, "");
			outcome = "logged_in";
			break;
		case INACTIVE_ACCOUNT:
			logger.debug("attemptLogin FAILURE, INACTIVE_ACCOUNT,  user = {}",
					user);
			addErrorMsg("general_error_inactive");
			AuditLogger.logAuditTrail(ScreenType.FREE,
					WebUtil.getRequestPath(), ActionType.LOGIN, "login failed",
					user.getUserId() + ": inactive account");
			// AuditLogger.logAuditTrail(ScreenId.INDEX, ActionCategory.MISC,
			// ActionType.LOGIN, false, "user", userId, "INACTIVE_ACCOUNT",
			// null, "");
			break;
		case ERROR:
			logger.debug("attemptLogin FAILURE, ERROR, user = {}", user);
			addErrorMsg("db_error_reason_other");
			AuditLogger.logAuditTrail(ScreenType.FREE,
					WebUtil.getRequestPath(), ActionType.LOGIN, "login failed",
					user.getUserId() + ": internal error");
			// AuditLogger.logAuditTrail(ScreenId.INDEX, ActionCategory.MISC,
			// ActionType.LOGIN, false, "user", userId, "ERROR", null, "");
			break;
		default:
			logger.debug("attemptLogin FAILURE, default, user = {}", user);
			addErrorMsg("general_error_invalid");
			AuditLogger.logAuditTrail(ScreenType.FREE,
					WebUtil.getRequestPath(), ActionType.LOGIN, "login failed",
					this.userId);
			// AuditLogger.logAuditTrail(ScreenId.INDEX, ActionCategory.MISC,
			// ActionType.LOGIN, false, "user", userId,
			// "INVALID USER OR PASSWORD", null, "");
			break;
		}
		userId = null;
		password = null;
		return outcome;
	}

	private LoginResult attemptLogin(String userName, String password) {
		logger.debug("attemptLogin");
		LoginRC rc = LoginRC.SUCCESS;

		ConsoleUser consoleUser = null;
		// ConsoleUserDao consoleUserDao = new ConsoleUserDao();
		// Session session = HibernateUtil.getCurrentSession();
		// session.beginTransaction();
		try {
			consoleUser = DataAccess.webDao.getUser(userName);
			if (consoleUser == null) {
				logger.debug("INVALID_USER");
				rc = LoginRC.INVALID_USER;
			} else if (consoleUser.getStatus() != ConsoleUser.Status.ACTIVE) {
				logger.debug("INACTIVE_ACCOUNT");
				rc = LoginRC.INACTIVE_ACCOUNT;
			} else {
				try {
					logger.debug("ConsoleUser: userId={}, password={}",
							consoleUser.getUserId(), consoleUser.getPassword());
					logger.debug("ConsoleUser: userId={}, enteredPassword={}",
							consoleUser.getUserId(),
							CryptUtil.encrypt(password));
					if (!CryptUtil.verify(password, consoleUser.getPassword())) {
						rc = LoginRC.INVALID_PASSWORD;
						consoleUser.setLoginFailure((consoleUser
								.getLoginFailure() == null ? 0 : consoleUser
								.getLoginFailure()) + 1);
					}
				} catch (Exception e) {
					logger.error("attemptLogin", e);
					rc = LoginRC.ERROR;
				}
			}

			if (consoleUser != null) {
				// Initialize variable for later use
				if (consoleUser.getGroup() != null) {
					for (UserRole role : consoleUser.getGroup().getUserRoles()) {
						Hibernate.initialize(role.getUserRights());
					}
				}
				consoleUser.setLastLogin(new Timestamp(System
						.currentTimeMillis()));
				// consoleUserDao.makePersistent(consoleUser);
				DataAccess.webDao.saveUser(consoleUser);
			}

			// session.getTransaction().commit();
		} catch (Exception e) {
			// session.getTransaction().rollback();
			// throw e;
			logger.error("attemptLogin", e);
		}
		return new LoginResult(rc, consoleUser);
	}

	private LoginResult attemptLogin(String userName, boolean passwordVerified) {
		logger.debug("attemptLogin");
		if (passwordVerified) {
			LoginRC rc = LoginRC.SUCCESS;

			ConsoleUser consoleUser = null;
			// ConsoleUserDao consoleUserDao = new ConsoleUserDao();
			// Session session = HibernateUtil.getCurrentSession();
			// session.beginTransaction();
			try {
				consoleUser = DataAccess.webDao.getUser(userName);
				if (consoleUser == null) {
					logger.debug("INVALID_USER");
					rc = LoginRC.INVALID_USER;
				} else if (consoleUser.getStatus() != ConsoleUser.Status.ACTIVE) {
					logger.debug("INACTIVE_ACCOUNT");
					rc = LoginRC.INACTIVE_ACCOUNT;
				}

				// Do not check password here since already checked by
				// DBLoginModule

				if (consoleUser != null) {
					// Initialize variable for later use
					if (consoleUser.getGroup() != null) {
						for (UserRole role : consoleUser.getGroup()
								.getUserRoles()) {
							Hibernate.initialize(role.getUserRights());
						}
					}
					consoleUser.setLastLogin(new Timestamp(System
							.currentTimeMillis()));
					// consoleUserDao.makePersistent(consoleUser);
					DataAccess.webDao.saveUser(consoleUser);
				}

				// session.getTransaction().commit();
			} catch (Exception e) {
				// session.getTransaction().rollback();
				// throw e;
				logger.error("attemptLogin", e);
			}
			return new LoginResult(rc, consoleUser);
		}
		return null;
	}

	private void addErrorMsg(String msgKey) {
		WebUtil.addGlobalMsgByKey(FacesMessage.SEVERITY_ERROR, msgKey);
	}

	/**
	 * This should not be used by other classes. For getting real user ID, use
	 * getCurrentUserId() instead.
	 */
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	// get map of highest allowed access levels, through all rights a user has
	private Map<ScreenType, AccessLevel> getUserAccessMap() {
		if (userAccessMap == null) {
			if (user != null) {
				List<UserRight> userRightList = new ArrayList<UserRight>();
				for (UserRole role : user.getGroup().getUserRoles()) {
					userRightList.addAll(role.getUserRights());
				}
				// after getting a list of user rights that belongs to user,
				// find max right per page
				userAccessMap = new ConcurrentHashMap<ScreenType, AccessLevel>();
				for (UserRight right : userRightList) {
					if (userAccessMap.containsKey(right.getTypeId())) {
						// if access level in roleList is higher, put that into
						// the access map, else do nothing
						if (right.getAccessLevel().ordinal() > userAccessMap
								.get(right.getTypeId()).ordinal()) {
							userAccessMap.put(right.getTypeId(),
									right.getAccessLevel());
						}
					} else { // put access right into map if it doesnt exist yet
						userAccessMap.put(right.getTypeId(),
								right.getAccessLevel());
					}
				}
				/*
				 * for(Map.Entry<String, String> typeToLevelPair :
				 * userAccessMap.entrySet()) {
				 * System.out.println(typeToLevelPair.getKey()
				 * +":"+typeToLevelPair.getValue()); }
				 */
			}
		}
		return userAccessMap;
	}

	private ScreenType getTypeId() {
		// HttpServletRequest request =
		// (HttpServletRequest)FacesContext.getCurrentInstance().getExternalContext().getRequest();
		// String uri = request.getRequestURI();
		String uri = FacesContext.getCurrentInstance().getViewRoot()
				.getViewId();
		for (Map.Entry<String, ScreenType> page : pageToTypeId.entrySet()) {
			if (uri.contains(page.getKey())) {
				return pageToTypeId.get(page.getKey());
			}
		}
		logger.info("getTypeId() = null, userId = {}, requestURI = {}", userId,
				uri);
		return null;
	}

	/**
	 * Return null if user has not logged in.
	 */
	public String getAccessLevel() {
		// TODO remove
		// return AccessLevel.WRITE.name();
		if (isLoggedIn()) {
			ScreenType screenType = getTypeId();
			if (screenType == null) {
				return AccessLevel.NONE.name();
			} else if (screenType == ScreenType.FREE) {
				return AccessLevel.WRITE.name();
			} else if (screenType == ScreenType.READONLY) {
				return AccessLevel.READ.name();
			} else {
				AccessLevel accessLevel = getUserAccessMap().get(screenType);
				if (accessLevel != null) {
					return accessLevel.name();
				} else {
					return AccessLevel.NONE.name();
				}
			}
		} else {
			return null;
		}
	}

	public boolean hasAccess(String screenTypeName) {
		// TODO restore
		// return true;
		try {
			ScreenType screenType = ScreenType.valueOf(screenTypeName);
			switch (screenType) {
			case FREE:
			case READONLY:
				return true;
			default:
				AccessLevel accessLevel = getUserAccessMap().get(screenType);
				if (accessLevel != null && accessLevel != AccessLevel.NONE) {
					return true;
				} else {
					return false;
				}
			}
		} catch (Exception e) {
			return false;
		}
	}

	public Locale getLocale() {
		return locale;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
		// FIXME need confirm
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
	}

	public void setLocale(String language, String country) {
		this.locale = new Locale(language, country);
		// FIXME need confirm
		FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getCurrentUserId() {
		return user == null ? null : user.getUserId();
	}

	/**
	 * This method is used for a workaround for setting privileges in viewing
	 * additional pages
	 * 
	 * @return the groupId of current user, or null if user is null
	 */
	public String getUserGroupId() {
		if (user != null) {
			return user.getGroup().getGroupId();
		}
		return null;
	}
}