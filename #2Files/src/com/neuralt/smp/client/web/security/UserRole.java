package com.neuralt.smp.client.web.security;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class UserRole implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 30)
	private String roleId;
	@Column(length = 60)
	private String roleName;
	private Timestamp createDate;
	@Version
	@Column(nullable = false)
	private Timestamp lastUpdatedOn;
	@Column(length = 30)
	private String lastUpdatedBy;
	// inverse side
	@ManyToMany(mappedBy = "userRoles")
	@org.hibernate.annotations.ForeignKey(name = "FK_GROUPROLE_USERROLE")
	@Cascade(value = { CascadeType.SAVE_UPDATE })
	private Set<UserGroup> userGroups;
	// inverse side
	@OneToMany(mappedBy = "userRole", orphanRemoval = true, fetch = FetchType.EAGER)
	@Cascade(value = { CascadeType.SAVE_UPDATE, CascadeType.DELETE })
	private Set<UserRight> userRights;

	public UserRole() {
		createDate = new Timestamp(System.currentTimeMillis());
	}

	public UserRole(String roleId, String roleName, String lastUpdatedBy) {
		this.roleId = roleId;
		this.roleName = roleName;
		this.lastUpdatedBy = lastUpdatedBy;
		this.createDate = new Timestamp(System.currentTimeMillis());
	}

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Timestamp getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Timestamp lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public Set<UserRight> getUserRights() {
		return userRights;
	}

	public void setUserRights(Set<UserRight> userRights) {
		this.userRights = userRights;
	}

	/**
	 * Ensured referential integrity.
	 */
	public void addUserRight(UserRight userRight) {
		if (userRights == null) {
			userRights = new HashSet<UserRight>();
		}
		userRights.add(userRight);
		userRight.setUserRole(this);
	}

	public void removeUserRight(UserRight userRight) {
		if (userRights != null) {
			userRights.remove(userRight);
		}
	}

	/**
	 * Warning: HAVE NOT ensured referential integrity. Do it manually as
	 * follow:
	 * <p>
	 * <code>
	 * userGroup.addUserRole(userRole);<br/>
	 * userRole.addUserGroup(userGroup);
	 * </code>
	 * </p>
	 */
	public void addUserGroup(UserGroup userGroup) {
		if (userGroups == null) {
			userGroups = new HashSet<UserGroup>();
		}
		userGroups.add(userGroup);
	}

	/**
	 * Warning: HAVE NOT ensured referential integrity. Do it manually as
	 * follow:
	 * <p>
	 * <code>
	 * userGroup.removeUserRole(userRole);<br/>
	 * userRole.removeUserGroup(userGroup);
	 * </code>
	 * </p>
	 */
	public void removeUserGroup(UserGroup userGroup) {
		if (userGroups == null) {
			userGroups = new HashSet<UserGroup>();
		}
		userGroups.remove(userGroup);
	}

	@Override
	public boolean equals(Object o) {
		if (o instanceof UserRole) {
			return this.roleId.equals(((UserRole) o).getRoleId());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.getRoleId().hashCode();
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("roleId: " + roleId + ";");
		buffer.append("roleName: " + roleName + ";");
		if (createDate != null) {
			buffer.append("createDate: " + createDate.toString() + ";");
		}
		if (lastUpdatedOn != null) {
			buffer.append("lastUpdatedOn: " + lastUpdatedOn.toString() + ";");
		}
		if (lastUpdatedBy != null) {
			buffer.append("lastUpdatedBy: " + lastUpdatedBy);
		}
		return buffer.toString();
	}
}