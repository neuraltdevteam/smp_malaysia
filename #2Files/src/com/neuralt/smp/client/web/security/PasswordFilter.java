package com.neuralt.smp.client.web.security;

import java.util.regex.Pattern;

public class PasswordFilter {

	// http://stackoverflow.com/questions/3802192/regexp-java-for-password-validation
	//
	// Explanation:
	//
	// ^ # start-of-string
	// (?=.*[0-9]) # a digit must occur at least once
	// (?=.*[a-z]) # a lower case letter must occur at least once
	// (?=.*[A-Z]) # an upper case letter must occur at least once
	// (?=.*[~!@#$%^&*_-+=`|\(){}[]:;"'<>,.?/]) # a special character must occur
	// at least once
	// (?=\S+$) # no whitespace allowed in the entire string
	// .{8,} # anything, at least eight places though
	// $ # end-of-string
	//
	public static final String PATTERN = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#$%^&*_+=`|\\(){}:;\"'<>,.?/-])(?=\\S+$).{8,}$";
	
	public static void main(String args[]) throws Exception {
		Pattern pattern = Pattern.compile(PasswordFilter.PATTERN);
		
	}

}
