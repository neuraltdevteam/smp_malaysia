package com.neuralt.smp.client.web.security;

import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.web.security.UserRight.AccessLevel;

public class AccessControlPhaseListener implements PhaseListener {

	private static final long serialVersionUID = 1L;

	static final Logger logger = LoggerFactory
			.getLogger(AccessControlPhaseListener.class);

	private UrlFilter restrictedArea;

	public AccessControlPhaseListener() {
		restrictedArea = new UrlFilter();
		restrictedArea.include("/pages/*").exclude("/notAuthorizedError.xhtml")
				.exclude("/login_failed.jsp").exclude("/WEB-INF/login.xhtml");

	}

	@Override
	public void afterPhase(PhaseEvent event) {
		FacesContext facesContext = event.getFacesContext();
		UserBean session = facesContext.getApplication().evaluateExpressionGet(
				facesContext, "#{UserBean}", UserBean.class);
		if (session == null) {
			logger.info("unable to retrieve Session!");
			return;
		}
		String username = facesContext.getExternalContext().getRemoteUser();
		String viewId = facesContext.getViewRoot().getViewId();
		logger.debug("username=" + username);
		logger.debug("viewId = {}", viewId);
		// if (!viewId.startsWith("/login")) {
		// logger.debug("not login page");
		// }
		if (restrictedArea.matches(viewId)) {
			session.attemptLogin(username);

			if (session.isLoggedIn()) {
				// not authorized to access this page
				logger.debug(username + " logged in");
				if (session.getAccessLevel().equals(AccessLevel.NONE.name())) {
					logger.info("no access right");
					facesContext
							.getApplication()
							.getNavigationHandler()
							.handleNavigation(facesContext, null,
									"/notAuthorizedError.xhtml");
				}
			} else {
				logger.info("login required");
				facesContext
						.getApplication()
						.getNavigationHandler()
						.handleNavigation(facesContext, null,
								"/login_failed.jsp");
			}

		}

		// TODO
		// Privilege schema changed, need re-design.
		// AccessControlPhaseListener may no longer be useful. May need to
		// distribute access control logic to each page.
	}

	@Override
	public void beforePhase(PhaseEvent event) {
		// back button forces refresh instead of reusing cache
		FacesContext facesContext = event.getFacesContext();
		HttpServletResponse response = (HttpServletResponse) facesContext
				.getExternalContext().getResponse();
		response.addHeader("Pragma", "no-cache");
		response.addHeader("Cache-Control", "no-cache");
		response.addHeader("Cache-Control", "no-store");
		response.addHeader("Cache-Control", "must-revalidate");
		response.addHeader("Expires", "0");
	}

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.RESTORE_VIEW;
	}
}
