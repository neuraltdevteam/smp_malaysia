package com.neuralt.smp.client.web.security;

import java.sql.Timestamp;
import java.util.List;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.dao.RawDAO;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.web.util.jsf.WebUtil;

public class AuditLogger {

	private static final Logger logger = LoggerFactory
			.getLogger(AuditLogger.class);
	private static RawDAO dao = new RawDAO(false);

	public static void logAuditTrail(ScreenType screenType, String screenId,
			ActionType actionType, String summary, String details) {
		AuditTrailLog log = createAuditTrail(screenType, screenId, actionType,
				summary, details);

		logger.debug("Saving audit trail log: {} ", log.toString());
		try {
			dao.save(log);
		} catch (DataAccessException e) {
			logger.error("logAuditTrail", e);
		}
	}

	public static void logAuditTrail(ScreenType screenType,
			ActionType actionType, String summary, String details) {
		logAuditTrail(screenType, WebUtil.getRequestPath(), actionType,
				summary, details);
	}

	public static AuditTrailLog createAuditTrail(ScreenType screenType,
			String screenId, ActionType actionType, String summary,
			String details) {
		Timestamp now = new Timestamp(System.currentTimeMillis());
		HttpServletRequest request = (HttpServletRequest) FacesContext
				.getCurrentInstance().getExternalContext().getRequest();
		String actorIp = WebUtil.getRemoteIp(request);
		String actorName = WebUtil.getUserId(request);

		AuditTrailLog log = new AuditTrailLog(now, actorName, actorIp,
				actionType, screenType, screenId, summary, details);
		return log;
	}

	public static void logBatch(List<AuditTrailLog> logs) {
		dao.batchPersist(logs);
	}
}
