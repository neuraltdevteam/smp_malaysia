package com.neuralt.smp.client.web.security;

import java.util.ArrayList;
import java.util.List;

public enum ScreenType {
	// "A" = ACTIVE screen
	// "I" = INACTIVE screen
	// "O" = other
	MANAGE_USER("A"), MONITOR("A"), COMMAND("A"), CONFIG("A"),

	FREE("O"), READONLY("O"), ;

	public String active;

	private ScreenType(String active) {
		this.active = active;
	}

	public static List<ScreenType> getEnum(String active) {
		List<ScreenType> list = new ArrayList<ScreenType>();
		for (ScreenType option : ScreenType.values()) {
			if (option.active.equals(active)) {
				list.add(option);
			}
		}
		return list;
	}
}
