package com.neuralt.smp.client.web.security;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AUDIT_TRAIL")
public class AuditTrailLog implements Serializable {

	private static final long serialVersionUID = -3256072599320969706L;

	@Id
	@GeneratedValue
	private Long id;
	private Timestamp actionTime;
	private String actorName;
	private String actorIp;
	@Enumerated(EnumType.STRING)
	private ActionType actionType;
	private String screenId;
	@Enumerated(EnumType.STRING)
	private ScreenType screenType;

	private String summary;
	@Column(length = 1000)
	private String details;

	public AuditTrailLog() {
	}

	public AuditTrailLog(Timestamp actionTime, String actorName,
			String actorIp, ActionType actionType, ScreenType screenType,
			String screenId, String summary, String details) {
		this.actionTime = actionTime;
		this.actorName = actorName;
		this.actorIp = actorIp;
		this.actionType = actionType;
		this.screenType = screenType;
		this.screenId = screenId;
		this.summary = summary;
		this.details = details;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getActionTime() {
		return actionTime;
	}

	public void setActionTime(Timestamp actionTime) {
		this.actionTime = actionTime;
	}

	public String getActorName() {
		return actorName;
	}

	public void setActorName(String actorName) {
		this.actorName = actorName;
	}

	public String getActorIp() {
		return actorIp;
	}

	public void setActorIp(String actorIp) {
		this.actorIp = actorIp;
	}

	public ActionType getActionType() {
		return actionType;
	}

	public void setActionType(ActionType actionType) {
		this.actionType = actionType;
	}

	public String getScreenId() {
		return screenId;
	}

	public void setScreenId(String screenId) {
		this.screenId = screenId;
	}

	public ScreenType getScreenType() {
		return screenType;
	}

	public void setScreenType(ScreenType screenType) {
		this.screenType = screenType;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public static enum ActionType {
		LOGIN, LOGOUT, INSERT, UPDATE, DELETE, START_PROCESS, STOP_PROCESS, COMMAND, IMPORT, EXPORT
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AuditTrailLog [id=").append(id).append(", actionTime=")
				.append(actionTime).append(", actorName=").append(actorName)
				.append(", actorIp=").append(actorIp).append(", actionType=")
				.append(actionType).append(", screenId=").append(screenId)
				.append(", screenType=").append(screenType)
				.append(", summary=").append(summary).append(", details=")
				.append(details).append("]");
		return builder.toString();
	}
}
