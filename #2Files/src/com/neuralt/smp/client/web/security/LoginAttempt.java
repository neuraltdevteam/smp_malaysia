package com.neuralt.smp.client.web.security;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class LoginAttempt {

	@Id
	@Column(name = "USERID", length = 30)
	private String userId;
	private int attemptCount;
	private Timestamp lastLogin;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getAttemptCount() {
		return attemptCount;
	}

	public void setAttemptCount(int count) {
		this.attemptCount = count;
	}

	public Timestamp getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}

}
