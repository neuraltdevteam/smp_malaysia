package com.neuralt.smp.client.web.security;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;
import javax.persistence.Version;

@Entity
// @Table(name = "ConsoleUser")
// @org.hibernate.annotations.BatchSize(size = 10)
public class ConsoleUser implements Serializable {

	private static final long serialVersionUID = 1L;

	public static enum Status {
		ACTIVE("Active"), INACTIVE("Inactive"), ;
		public final String id;

		private Status(String id) {
			this.id = id;
		}

		public static Status getEnum(String id) {
			for (Status obj : Status.values()) {
				if (obj.id.equals(id)) {
					return obj;
				}
			}
			return null;
		}

		@Override
		public String toString() {
			return id; // to be used by hibernate, just #name() to get the enum
						// name
		}
	}

	@Id
	@Column(name = "USERID", length = 30)
	private String userId;
	@Column(length = 60)
	private String name;
	@Column(length = 50, nullable = false)
	private String password;
	@Column(length = 10, nullable = false)
	@Enumerated(EnumType.STRING)
	private Status status;
	private Integer loginFailure;
	private String department;
	private String location;
	private Timestamp lastLogin;
	private Timestamp createDate;
	// @Temporal(TemporalType.TIMESTAMP)
	@Version
	@Column(nullable = false)
	private Timestamp lastUpdatedOn;
	@Column(length = 30)
	private String lastUpdatedBy;
	@Transient
	private String groupId; // for use by console only

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "GROUPID", nullable = false)
	@org.hibernate.annotations.ForeignKey(name = "FK_CONSOLEUSER_USERGROUP")
	private UserGroup group;

	public ConsoleUser() {
		loginFailure = 0;
		createDate = new Timestamp(System.currentTimeMillis());
	}

	public ConsoleUser(String userId, String name, String password,
			Status status, Integer loginFailure, Timestamp lastLogin,
			String lastUpdatedBy) {
		super();
		this.userId = userId;
		this.name = name;
		this.password = password;
		this.status = status;
		this.loginFailure = loginFailure;
		this.lastLogin = lastLogin;
		this.lastUpdatedBy = lastUpdatedBy;
		createDate = new Timestamp(System.currentTimeMillis());
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Integer getLoginFailure() {
		if (loginFailure == null)
			loginFailure = 0;
		return loginFailure;
	}

	public void setLoginFailure(Integer loginFailure) {
		this.loginFailure = loginFailure;
	}

	public Timestamp getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Timestamp getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Timestamp lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * Warning: to be used by console only
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * Warning: to be used by console only
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public UserGroup getGroup() {
		return group;
	}

	/**
	 * If maintaining bidirectional relationship of Java object is needed, use
	 * {@link UserGroup#addConsoleUser(ConsoleUser)}. If for performance, use
	 * this method.
	 */
	public void setGroup(UserGroup group) {
		this.group = group;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ConsoleUser [userId=").append(userId).append(", name=")
				.append(name).append(", password=").append(password)
				.append(", status=").append(status).append(", loginFailure=")
				.append(loginFailure).append(", department=")
				.append(department).append(", location=").append(location)
				.append(", lastLogin=").append(lastLogin)
				.append(", createDate=").append(createDate)
				.append(", lastUpdatedOn=").append(lastUpdatedOn)
				.append(", lastUpdatedBy=").append(lastUpdatedBy)
				.append(", groupId=").append(groupId).append(", group=")
				.append(group).append("]");
		return builder.toString();
	}

	// public String toString() {
	// StringBuffer buffer = new StringBuffer();
	// buffer.append("userId:" + userId + "; ");
	// buffer.append("name:" + name + "; ");
	// buffer.append("password:" + password + "; ");
	// buffer.append("status:" + status + "; ");
	// buffer.append("loginFailure:" + loginFailure + "; ");
	// if( lastLogin != null ) {
	// buffer.append("lastLogin:" + lastLogin + "; ");
	// }
	// if( createDate != null ) {
	// buffer.append("createDate:" + createDate + "; ");
	// }
	// if( lastUpdatedOn != null ) {
	// buffer.append("lastUpdatedOn:" + lastUpdatedOn + "; ");
	// }
	// if( lastUpdatedBy != null ) {
	// buffer.append("lastUpdatedBy:" + lastUpdatedBy + "; ");
	// }
	// if(group != null){
	// buffer.append("groupId:" + group.getGroupId() + "; ");
	// }
	// return buffer.toString();
	// }

}