package com.neuralt.smp.client.web;

import java.lang.management.ManagementFactory;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mchange.v2.c3p0.C3P0Registry;
import com.mchange.v2.c3p0.PooledDataSource;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.client.mbean.ClientApp;
import com.neuralt.smp.client.service.AlarmService;
import com.neuralt.smp.client.service.MiscConfigService;
import com.neuralt.smp.client.snmp.TrapListenerImpl;
import com.neuralt.smp.client.task.CheckAlarmTask;
import com.neuralt.smp.client.task.DBHousekeepTask;
import com.neuralt.smp.client.task.FileHousekeepTask;
import com.neuralt.smp.rrd.RrdService;
import com.neuralt.smp.snmp.proxy.SnmpTrapProxyEngine;
import com.neuralt.smp.snmp.util.MibFileLoader;

public class ContextListener implements ServletContextListener {
	private static final Logger logger = LoggerFactory
			.getLogger(ContextListener.class);
	private SnmpTrapProxyEngine snmpTrapProxyEngine;
	private DBHousekeepTask dbHousekeepTask;
	private CheckAlarmTask checkAlarmTask;
	private FileHousekeepTask fileHousekeepTask;

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		logger.info("===================================");
		logger.info(String.format("SMP Web Console, Version %s",
				AppConfig.VERSION));
		logger.info("===================================");
		logger.info("Initializing application context...");

		// try {
		// (new LicenseOfficer()).checkServerLicense();
		// } catch (Exception e) {
		// logger.error("Failed to check server license file. Shutting down...",
		// e.getCause());
		// throw new RuntimeException("Invalid server license file");
		// }

		try {
			loadConfig();
			MiscConfigService.instance.init();
			AlarmService.instance.init();
			registerMBean();
			startSnmpProxyEngine();
			startSmpAgentClient();
			startDBHouseKeeper();
			startFileHouseKeeper();
			startAlarmChecker();
			CentralizedEventService.getInstance().onSysStartSuccess();
		} catch (Exception e) {
			logger.error("Failed to initialise system context", e);
			CentralizedEventService.getInstance().onSysStartFailed(
					"Failed to initialise system context");
			// that'll block the app from running. container will call
			// contextDestroyed to cleanup afterwards
			throw new RuntimeException(
					"contextInitialized failed. SMP fails to start.", e);
		}
	}

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		logger.info("Application context being destroyed...");

		try {
			CentralizedEventService.getInstance().onSysShutdown();

			SmpClient.instance.stop();

			if (snmpTrapProxyEngine != null)
				snmpTrapProxyEngine.shutdown();

			if (dbHousekeepTask != null)
				dbHousekeepTask.quit();

			if (fileHousekeepTask != null)
				fileHousekeepTask.quit();

			if (checkAlarmTask != null)
				checkAlarmTask.quit();

			RrdService.getInstance().stopAllDataCollection();

			deregisterJDBC();

			closeC3P0DataSources();

			try {
				unregisterMBean();
			} catch (Exception ignored) {
			}
		} catch (Exception e) {
			logger.error("Failed to destory system context", e);
			throw new RuntimeException(
					"contextDestroyed failed. SMP fails to stop.", e);
		}
	}

	private void closeC3P0DataSources() {
		C3P0Registry.getNumPooledDataSources();

		@SuppressWarnings({ "unchecked", "rawtypes" })
		Iterator<Set> it = C3P0Registry.getPooledDataSources().iterator();
		while (it.hasNext()) {
			try {
				PooledDataSource dataSource = (PooledDataSource) it.next();
				dataSource.close();
			} catch (Exception e) {
				logger.error("Failed to close C3P0 data sources", e);
			}
		}
	}

	private void loadConfig() {
		String configFilePath = System.getProperty("app.config",
				AppConfig.getDefaultAppConfigLocation());
		AppConfig.loadAppConfig(configFilePath);
		AppConfig.loadLogConfig();
		AppConfig.loadMailConfig();
	}

	private void registerMBean() throws Exception {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

		ClientApp appMBean = new ClientApp();

		try {
			mbs.registerMBean(appMBean,
					new ObjectName(AppConfig.getAgentAppMBeanName()));
		} catch (Exception e) {
			logger.error("registerMBean failed", e);
			throw e;
		}
	}

	private void unregisterMBean() throws Exception {
		MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();

		try {
			mbs.unregisterMBean(new ObjectName(AppConfig.getAgentAppMBeanName()));
		} catch (Exception e) {
			logger.error("unregisterMBean failed");
		}
	}

	private void startDBHouseKeeper() throws Exception {
		try {
			dbHousekeepTask = new DBHousekeepTask();
			dbHousekeepTask.start();
		} catch (Exception e) {
			logger.error("startDBHouseKeeper failed", e);
			throw e;
		}
	}

	private void startFileHouseKeeper() throws Exception {
		try {
			fileHousekeepTask = new FileHousekeepTask();
			fileHousekeepTask.start();
		} catch (Exception e) {
			logger.error("startFileHouseKeeper failed", e);
			throw e;
		}
	}

	private void startAlarmChecker() throws Exception {
		try {
			checkAlarmTask = new CheckAlarmTask();
			checkAlarmTask.start();
		} catch (Exception e) {
			logger.error("startAlarmChecker failed", e);
			throw e;
		}
	}

	private void startSmpAgentClient() throws Exception {
		try {
			SmpClient.instance.start();
		} catch (Exception e) {
			logger.error("startSmpAgentClient failed", e);
			throw e;
		}
	}

	private void startSnmpProxyEngine() throws Exception {
		snmpTrapProxyEngine = new SnmpTrapProxyEngine(AppConfig.getAppConfig(),
				new TrapListenerImpl());
		// TODO
		// Since MibFileLoader plays an important role other than starting proxy
		// engine, it will be moved to upper project
		try {
			snmpTrapProxyEngine.startup();
			MibFileLoader.getInstance().loadMib(AppConfig.getSvrMIBFileDir());
			MibFileLoader.getInstance()
					.loadAllMib(AppConfig.getAllMIBFileDir());
		} catch (Exception e) {
			logger.error("startSnmpProxyEngine failed", e);
			throw e;
		}
	}

	private void deregisterJDBC() {
		Enumeration<Driver> drivers = DriverManager.getDrivers();
		Driver d = null;
		while (drivers.hasMoreElements()) {
			try {
				d = drivers.nextElement();
				DriverManager.deregisterDriver(d);
				logger.warn(String.format("Driver %s deregistered", d));
			} catch (SQLException ex) {
				logger.warn(String.format("Error deregistering driver %s", d),
						ex);
			}
		}
		Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
		Thread[] threadArray = threadSet.toArray(new Thread[threadSet.size()]);
		for (Thread t : threadArray) {
			if (t.getName().contains("Abandoned connection cleanup thread")) {
				synchronized (t) {
					t.stop(); // don't complain, it works
				}
			}
		}
	}

}
