package com.neuralt.smp.client.web.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class PortValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		Integer port = null;

		try {
			port = (Integer) value;
		} catch (Exception e) {
			throwException();
		}

		if ((port < 1) || (port > 65535)) {
			throwException();
		}
	}

	private void throwException() {
		FacesMessage message = new FacesMessage();
		message.setSummary("Port range error");
		message.setDetail("");
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		throw new ValidatorException(message);
	}
}
