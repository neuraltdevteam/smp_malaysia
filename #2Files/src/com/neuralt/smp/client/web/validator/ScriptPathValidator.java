package com.neuralt.smp.client.web.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class ScriptPathValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String path = (String) value;

		if (!path.endsWith(".sh")) {
			FacesMessage message = new FacesMessage();
			message.setSummary("Script Path Error");
			message.setDetail("");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}

}
