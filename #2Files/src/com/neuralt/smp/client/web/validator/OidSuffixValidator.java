package com.neuralt.smp.client.web.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.web.util.jsf.WebUtil;

public class OidSuffixValidator implements Validator {

	private static final String PATTERN = "(.[0([1-9]{1}[0-9]+)])+";

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String str = (String) value;
		if (!str.startsWith("."))
			str = "." + str;
		Pattern p = Pattern.compile(PATTERN);

		// Match the given string with the pattern
		Matcher m = p.matcher(str);

		// Check whether match is found
		boolean matchFound = m.matches();

		if (!matchFound) {
			FacesMessage message = new FacesMessage();
			message.setSummary(WebUtil.getMessage(MessageKey.GENERAL_ERROR));
			message.setDetail(WebUtil.getMessage(MessageKey.OID_SUFFIX_ERROR));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}

}
