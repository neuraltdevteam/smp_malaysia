package com.neuralt.smp.client.web.validator;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.web.util.jsf.WebUtil;

public class SubscriptionPageDateValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {

        UIInput sd = (UIInput)component.getAttributes().get("startDate");
        Date firstDate = (Date)sd.getValue();
        Date secondDate = (Date)value;
        Date currentDate = new Date();

        long diff = secondDate.getTime() - firstDate.getTime();
        long diffday = diff / (24* 60 * 60 * 1000);
        
        System.out.println("diff="+diff);
        System.out.println("diffday="+diffday);
        
        if (firstDate.after(currentDate)){

			FacesMessage message = new FacesMessage();
			message.setSummary(WebUtil.getMessage(MessageKey.START_DATE_ERROR));
			message.setDetail("");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
        else if (firstDate.after(secondDate)){
			FacesMessage message = new FacesMessage();
			message.setSummary(WebUtil.getMessage(MessageKey.DATE_RANGE_ERROR));
			message.setDetail("");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);        
		}
        else if (diffday > 92){
			FacesMessage message = new FacesMessage();
			message.setSummary(WebUtil.getMessage(MessageKey.DATE_RANGE_OVER_3_MTH_LIMIT_ERROR));
			message.setDetail("");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);        
		}
        
	}

}
