package com.neuralt.smp.client.web.validator;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.web.util.jsf.WebUtil;

public class RrdDefStepValidator implements Validator {

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object object) throws ValidatorException {
		Integer value = (Integer) object;
		if (value <= 0) {
			FacesMessage message = new FacesMessage();
			message.setSummary(WebUtil.getMessage(MessageKey.GENERAL_ERROR));
			message.setDetail(WebUtil
					.getMessage(MessageKey.RRD_DEF_STEP_MIN_PROMPT));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}

}
