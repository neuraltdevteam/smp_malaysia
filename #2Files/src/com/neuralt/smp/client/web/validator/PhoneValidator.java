package com.neuralt.smp.client.web.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public class PhoneValidator implements Validator {
	
	private static final String NUMBER_PATTERN = "^[0-9]*$";
	
	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String phone = (String) value;
		
		Pattern p = Pattern.compile(NUMBER_PATTERN);

		// Match the given string with the pattern
		Matcher m = p.matcher(phone);

		// Check whether match is found
		boolean matchFound = m.matches();

		if (!matchFound) {
			FacesMessage message = new FacesMessage();
			message.setSummary("Phone format error");
			message.setDetail("");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
		
		/*
		boolean ok = true;
		// TODO
		if (!ok) {
			FacesMessage message = new FacesMessage();
			message.setSummary("Phone format error");
			message.setDetail("");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
		*/
		
	}

}
