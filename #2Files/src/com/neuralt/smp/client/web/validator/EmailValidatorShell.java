package com.neuralt.smp.client.web.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.web.util.jsf.WebUtil;

// copy from funambol
public class EmailValidatorShell implements Validator {

	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\."
			+ "[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*"
			+ "(\\.[A-Za-z]{2,})$";

	public void validate(FacesContext context, UIComponent component,
			Object value) {
		String enteredEmail = (String) value;
		// Set the email pattern string
		Pattern p = Pattern.compile(EMAIL_PATTERN);

		// Match the given string with the pattern
		Matcher m = p.matcher(enteredEmail);

		// Check whether match is found
		boolean matchFound = m.matches();

		if (!matchFound) {
			FacesMessage message = new FacesMessage();
			message.setSummary(WebUtil.getMessage(MessageKey.GENERAL_ERROR));
			message.setDetail(WebUtil.getMessage(MessageKey.EMAIL_FORMAT_ERROR));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}

	// @Override
	// public void validate(FacesContext context, UIComponent component, Object
	// value)
	// throws ValidatorException {
	// String email = (String) value;
	// // EmailValidator is get from apache open source lib
	// if (!EmailValidator.getInstance().isValid(email)) {
	// FacesMessage message = new FacesMessage();
	// message.setSummary("Email format error");
	// message.setDetail("");
	// message.setSeverity(FacesMessage.SEVERITY_ERROR);
	// throw new ValidatorException(message);
	// }
	// }

}
