package com.neuralt.smp.client.web.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.web.util.jsf.WebUtil;

public class NameValidator implements Validator {

	private static String NAME_PATTERN = "[(\\-)(\\w)]+";

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		String name = (String) value;

		Pattern p = Pattern.compile(NAME_PATTERN);
		Matcher m = p.matcher(name);

		if (!m.matches()) {
			FacesMessage message = new FacesMessage();
			message.setSummary(WebUtil.getMessage(MessageKey.GENERAL_ERROR));
			message.setDetail(WebUtil.getMessage(MessageKey.NAME_FORMAT_ERROR));
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}

}
