package com.neuralt.smp.client.web.validator;

import java.util.Arrays;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IPValidator implements Validator {

	private static final Logger logger = LoggerFactory
			.getLogger(IPValidator.class);

	@Override
	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		// only support ipv4 for now
		boolean ok = true;
		String ip = (String) value;
		logger.debug("ip=" + ip);
		String[] sec = ip.split("\\.");
		logger.debug(Arrays.toString(sec));
		if (sec.length != 4) {
			ok = false;
		} else {
			for (int i = 0; i < 4; i++) {
				try {
					int j = Integer.valueOf(sec[i]);
					if ((j < 0) || (j > 255)) {
						ok = false;
					}
				} catch (NumberFormatException e) {
					ok = false;
				}
			}
		}

		if (!ok) {
			FacesMessage message = new FacesMessage();
			message.setSummary("IP format error");
			message.setDetail("");
			message.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(message);
		}
	}

}
