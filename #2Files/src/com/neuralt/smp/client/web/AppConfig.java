package com.neuralt.smp.client.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.PropertiesUtil;

public class AppConfig {
	private static final Logger logger = LoggerFactory
			.getLogger(AppConfig.class);

	public static final String VERSION = "1.0.0";

	public static final String DEFAULT_APP_CONFIG_NAME = "${user.home}/smpserver/config/smp_server.properties";
	private static Properties appConfig;
	private static Properties mailConfig;
	private static String appConfigFilePath;

	public static String getAppConfigFilePath() {
		return appConfigFilePath;
	}

	public static synchronized void loadAppConfig() {
		logger.info("Loading application configuration from [{}]",
				appConfigFilePath);
		appConfig = new Properties();

		Properties newConfig = new Properties();
		File configFile = new File(appConfigFilePath);
		try {
			newConfig.load(new BufferedReader(new FileReader(configFile)));
			appConfig = newConfig;
			logger.info("Loaded from {} [{}]", appConfigFilePath,
					AppConfig.getAppConfig());
		} catch (FileNotFoundException e) {
			logger.error(
					"Unable to find application configuration file at location: [{}]",
					appConfigFilePath, e);
			logger.info("Program exiting....");
			System.exit(-1);
		} catch (IOException e) {
			logger.error(
					"Unable to read application configuration file at location: [{}]",
					appConfigFilePath, e);
			logger.info("Program exiting....");
			System.exit(-1);
		}
	}

	public static void loadMailConfig() {
		String mailConfigPath = AppConfig.getMailConfigPath();
		logger.info("Loading mail configuration from [{}]", mailConfigPath);

		mailConfig = new Properties();
		File configFile = new File(mailConfigPath);
		try {
			mailConfig.load(new FileInputStream(configFile));
			logger.info("Loaded from {} [{}]", mailConfigPath, mailConfig);
		} catch (FileNotFoundException e) {
			logger.error(
					"Unable to find application configuration file at location: [{}]",
					mailConfigPath, e);
			logger.info("Program exiting....");
			System.exit(-1);
		} catch (IOException e) {
			logger.error(
					"Unable to read application configuration file at location: [{}]",
					mailConfigPath, e);
			logger.info("Program exiting....");
			System.exit(-1);
		}
	}

	public static void loadAppConfig(String filePath) {
		appConfigFilePath = filePath;
		loadAppConfig();
	}

	public static void loadLogConfig() {
		String logFilepath = AppConfig.getLog4jDir();
		logger.info("Loading logger configuration from [{}]", logFilepath);

		Properties logProperties = new Properties();
		try {
			logProperties.load(new FileInputStream(logFilepath));
			PropertyConfigurator.configure(logProperties);
			logger.info("Loaded: [{}]", logProperties);
		} catch (FileNotFoundException e) {
			logger.error(
					"Unable to find logger configuration file at location: [{}]",
					logFilepath, e);
		} catch (IOException e) {
			logger.error(
					"Unable to read logger configuration file at location: [{}]",
					logFilepath, e);
		}
	}

	public static Properties getAppConfig() {
		return appConfig;
	}

	public static String getHomeFilepath() {
		return appConfig.getProperty("home.filepath");
	}

	public static String getLog4jDir() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("log4j.filepath"));
	}

	public static String getAgentAppMBeanName() {
		return appConfig.getProperty("jmx.name");
	}

	public static String getDefaultAppConfigLocation() {
		return PropertiesUtil.parsePropertiesValue(DEFAULT_APP_CONFIG_NAME);
	}

	public static String getMyName() {
		return appConfig.getProperty("sys.prop.app.name");
	}

	public static String getAgentMBeanName() {
		return appConfig.getProperty("agent.service.mbean.name");
	}

	// public static String getHostProcXMLConfigPath() {
	// return
	// PropertiesUtil.parsePropertiesValue(appConfig.getProperty("hostProcXmlConfig"));
	// }

	public static String getSvrMIBFileDir() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("snmp.mib.svr.dir"));
	}

	public static String getAllMIBFileDir() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("snmp.mib.all.dir"));
	}

	public static String getVendorMIBFileDir() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("snmp.vendor-mib.dir"));
	}

	public static int getCpuLiveChartPlotSize() {
		String str = appConfig.getProperty("web.live.chart.cpu.plot.size");
		return str != null ? Integer.valueOf(str) : 100;
	}

	public static int getMemoryLiveChartPlotSize() {
		String str = appConfig.getProperty("web.live.chart.memory.plot.size");
		return str != null ? Integer.valueOf(str) : 100;
	}

	public static String getWebDateFormat() {
		return appConfig.getProperty("web.date.format");
	}

	public static int getDBMaxResult() {
		return Integer.valueOf(appConfig.getProperty("trap.db.maxresult"));
	}

	public static int getDBRecordMaintain() {
		return Integer.valueOf(appConfig
				.getProperty("trap.db.record.maintain.day"));
	}

	public static int getDBAlarmCheckInterval() {
		String str = (appConfig.getProperty("trap.db.check.interval"));
		return str != null ? Integer.valueOf(str) : 10000;
	}

	public static int getWebMonitorPollInterval() {
		String str = (appConfig.getProperty("web.monitor.poll.interval"));
		return str != null ? Integer.valueOf(str) : 10000;
	}

	public static int getWebMonitorSubPollInterval() {
		return Integer.valueOf(appConfig
				.getProperty("web.monitor.subpoll.interval"));
	}

	public static int getHostHealthNotificationControl() {
		return Integer.valueOf(appConfig
				.getProperty("client.notif.host.health.control")) * 1000;
	}

	public static String getJmxUserName() {
		return appConfig.getProperty("agent.jmx.username");
	}

	public static String getJmxPassword() {
		return appConfig.getProperty("agent.jmx.password");
	}

	public static Integer getGrowlMessageBuffer() {
		return Integer.valueOf(appConfig
				.getProperty("web.monitor.growl.message.buffer"));
	}

	public static int getSnmpPasswordMinLength() {
		return Integer
				.valueOf(appConfig.getProperty("snmp.password.minlength"));
	}

	public static int getTimeConsideredDisconnected() {
		return Integer.valueOf(appConfig
				.getProperty("web.monitor.time.considered_disconnected"));
	}

	public static Properties getMailConfig() {
		return mailConfig;
	}

	public static String getMailConfigPath() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("mail.config.filepath"));
	}

	public static String getStatReportPrefix() {
		return appConfig.getProperty("stat.report.prefix");
	}

	public static String getAlarmReportPrefix() {
		return appConfig.getProperty("alarm.report.prefix");
	}

	public static int getDBMaxRetry() {
		return Integer.valueOf(appConfig.getProperty("trap.db.maxretry"));
	}

	public static String getScriptRunnerUser() {
		return appConfig.getProperty("script.runner");
	}

	public static String getRrdFileStorageDir() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("rrd.path"));
	}

	public static String getRrdGraphPrefix() {
		return appConfig.getProperty("rrd.graph.prefix");
	}

	public static String getTempDir() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("temp.dir"));
	}

	public static String getScriptFileStorageDir() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("script.path"));
	}

	public static String getScriptFileExtension() {
		String ext = appConfig.getProperty("script.extension");
		if (!ext.startsWith(".")) {
			ext = "." + ext;
		}
		return ext;
	}

	public static String getScriptRunnerVersion() {
		return appConfig.getProperty("script.runner.version");
	}

	public static String getRrdXmlPrefix() {
		return appConfig.getProperty("rrd.xml.prefix");
	}

	public static boolean isRrdConfigSimple() {
		return Boolean.valueOf(appConfig.getProperty("rrd.config.simple"));
	}

	public static String getBackupDir() {
		return appConfig.getProperty("backup.dir");
	}

	public static String getHibernateConfig() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("hibernate.config.filepath"));
	}

	public static String getBLCHibernateConfig() {
		return PropertiesUtil.parsePropertiesValue(appConfig
				.getProperty("hibernate.config.blc.filepath"));
	}

	public static String getSNMPv1v2Community() {
		return appConfig.getProperty("snmp.engine.v1_v2.community");
	}

	public static long getEventFlowControl(int defaultValue) {
		return Integer.valueOf(appConfig.getProperty("event.flow.control",
				String.valueOf(defaultValue))) * ConstantUtil.MIN_MS;
	}

	public static long getGeneralStatsAvgStepSize(int defaultValue) {
		return Integer.valueOf(appConfig.getProperty(
				"general.stats.avg.step.size", String.valueOf(defaultValue)))
				* ConstantUtil.MIN_MS;
	}

	/****************************************
	 * Trap related configuration
	 ****************************************/

	public static Boolean isRemAppNotPresentDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_app_not_present.db"));
	}

	public static Boolean isSysStartUpSuccessDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.sys_startup_success.db"));
	}

	public static Boolean isSysStartUpFailDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.sys_startup_fail.db"));
	}

	public static Boolean isSysConfigReloadDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.sys_config_reload.db"));
	}

	public static Boolean isConfigReloadErrorDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.sys_reload_error.db"));
	}

	public static Boolean isRemHostDownDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_host_down.db"));
	}

	public static Boolean isRemHostUpDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_host_up.db"));
	}

	public static Boolean isRemHostCpuOverloadDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_host_cpu_overload.db"));
	}

	public static Boolean isRemHostDiskOverloadDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_host_disk_overload.db"));
	}

	public static Boolean isRemHostMemOverloadDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_host_mem_overload.db"));
	}

	public static Boolean isRemHostInetDownDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_host_inet_down.db"));
	}

	public static Boolean isRemHostSnmpErrorDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_host_snmp_error.db"));
	}

	public static Boolean isRemAppStartDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_app_start.db"));
	}

	public static Boolean isRemAppStopDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_app_stop.db"));
	}

	public static Boolean isRemAppStartSuccessDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_app_start_success.db"));
	}

	public static Boolean isRemAppStartFailDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_app_start_fail.db"));
	}

	public static Boolean isRemAppStopSuccessDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_app_stop_success.db"));
	}

	public static Boolean isRemAppStopFailDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_app_stop_fail.db"));
	}

	public static Boolean isRemHostGeneralErrorDB() {
		return Boolean.valueOf(appConfig
				.getProperty("trap.outgoing.remote_host_general_error.db"));
	}

	/****************************************
	 * Trap related configuration (END)
	 ****************************************/
}
