package com.neuralt.smp.client.web;

import com.neuralt.smp.client.MonitoredSystem;

public class SelectedMonitoredSystem extends MonitoredSystem {

	private static final long serialVersionUID = 7918251428475074908L;
	private boolean selected;

	public SelectedMonitoredSystem(MonitoredSystem system) {
		super(system.getConfig());
		selected = false;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

}
