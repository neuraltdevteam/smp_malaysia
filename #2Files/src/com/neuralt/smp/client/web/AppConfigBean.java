package com.neuralt.smp.client.web;

import java.util.TimeZone;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 * Managed bean wrapper of AppConfig
 */
@ManagedBean(eager = true)
@ApplicationScoped
public class AppConfigBean {

	private TimeZone timezone = TimeZone.getDefault();

	public String getDateFormat() {
		return AppConfig.getWebDateFormat();
	}

	public TimeZone getTimeZone() {
		return timezone;
	}

	public String getVersion() {
		return AppConfig.VERSION;
	}
}
