package com.neuralt.smp.client.web;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.model.SelectItem;

import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.web.util.CssStyleUtil;

/**
 * holds various convenience methods / shared data used by various .jsf pages
 */

@ManagedBean
@ApplicationScoped
public class WebUtilBean implements Serializable {

	private static final long serialVersionUID = -5727517627072628840L;

	public String getWarningLevelStyleClass(String levelStr) {
		return CssStyleUtil.getStatusCssClass(levelStr);
	}

	public List<SelectItem> getWarningLevelFilters() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		SelectItem anyItem = new SelectItem("", "-ANY-");
		anyItem.setNoSelectionOption(true);
		items.add(anyItem);
		for (WarningLevel level : WarningLevel.concreteValues()) {
			items.add(new SelectItem(level, level.name()));
		}
		return items;
	}

	public WarningLevel[] getWarningLevelOptions() {
		return WarningLevel.concreteValues();
	}

	public Boolean[] getBooleanOptions() {
		return new Boolean[] { true, false };
	}
}
