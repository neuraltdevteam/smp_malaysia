package com.neuralt.smp.client.web.view;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.config.AlarmStatus;
import com.neuralt.smp.notification.WarningLevel;

@ManagedBean
@ViewScoped
public class AlarmSummaryView {
	
	protected CountQuery queryCritical = new CountQuery(
			"level in (:levels)",
			"levels", Arrays.asList(WarningLevel.ERROR, WarningLevel.FATAL));
	
	protected CountQuery queryPending = new CountQuery(
			"status in (:status)",
			"status", Arrays.asList(AlarmStatus.UNHANDLED.getName(), AlarmStatus.ACKNOWLEDGED.getName()));
	
	protected CountQuery queryLastHour = new CountQuery(
			"lastOccurred > :lastHour",
			"lastHour", new Date(new Date().getTime() - 3600000l));
	
	protected CountQuery queryLastHourHandled = new CountQuery(
			"status in (:status) and lastOccurred > :lastHour",
			"status", Arrays.asList(AlarmStatus.IGNORED.getName(), AlarmStatus.RESOLVED.getName()),
			"lastHour", new Date(new Date().getTime() - 3600000l));
	
	public int countCritical(Object entity){
		return queryCritical.count(entity);
	}
	
	public int countPending(Object entity){
		return queryPending.count(entity);
	}
	
	public int countLastHour(Object entity){
		return queryLastHour.count(entity);
	}
	
	public int countLastHourHandled(Object entity){
		return queryLastHourHandled.count(entity);
	}
	
	protected static class CountQuery{
		
		protected String hql;
		protected Map<String,Object> param;
		protected Map<String,Integer> cache;
		
		public CountQuery(String baseCriteria, Object ... baseParam){
			hql="select count(*) as result from "+Alarm.class.getSimpleName()+" where "+baseCriteria;
			param = new HashMap<String,Object>();
			String key = null;
			for(Object p : baseParam){
				if(key == null){
					key = (String)p;
				}else{
					param.put(key, p);
					key = null;
				}
			}
			cache = new HashMap<String,Integer>();
		}
		
		public void param(String key, Object val){
			param.put(key, val);
			cache.clear();
		}
		
		public int count(Object ent){
			if(ent instanceof MonitoredEntity){
				MonitoredEntity entity = (MonitoredEntity)ent;
				
				//count will be cached (within the view scope)
				String cacheKey = entity.getClass().getName()+"-"+entity.getName();
				Integer count = cache.get(cacheKey);
				if(count != null){
					return count;
				}
				
				//query DB for the count
				String hql = this.hql;
				Map<String,Object> param = new HashMap<String,Object>();
				param.putAll(this.param);
				
				switch(((MonitoredEntity)entity).getEntityType()){
				case PROCESS:
					hql+=" and source like :source";
					param.put("source", "%"+((MonitoredProcess)entity).getHost().getConfig().getName()+"%"+entity.getName()+"%");
					count = DataAccess.genDao.findUnique(Number.class, hql, param).intValue();
					break;
				case HOST:
					hql+=" and source like :source";
					param.put("source", "%"+((MonitoredHost)entity).getConfig().getName()+"%");
					count = DataAccess.genDao.findUnique(Number.class, hql, param).intValue();
					break;
				case ALL:
					count = DataAccess.genDao.findUnique(Number.class, hql, param).intValue();
					break;
				case SYSTEM:
					//including all hosts alarm count
					count = 0;
					for(MonitoredHost host : ((MonitoredSystem)entity).getHosts()){
						count+=count(host);
					}
				}
				//cache it and return
				cache.put(cacheKey, count);
				return count;
			}
			return 0;
		}
		
	}
}
