package com.neuralt.smp.client.web;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Abuse the nature of ApplicationScoped ManagedBean(eager=true) And perform all
 * the necessary "onload" operations
 */

@ManagedBean(eager = true)
@ApplicationScoped
public class ApplicationInitializer {

	private final static Logger logger = LoggerFactory
			.getLogger(ApplicationInitializer.class);

	public ApplicationInitializer() {
		initSystem();
	}

	private void initSystem() {
	}
}
