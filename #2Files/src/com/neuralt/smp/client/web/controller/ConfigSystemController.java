package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.util.ModelDataTable;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.SystemConfig;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class ConfigSystemController implements ReloadableController, Serializable{
	
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(ConfigSystemController.class);
	
	private final ModelDataTable<SystemConfig> model = new ModelDataTable<SystemConfig>(
			SystemConfig.class,
			"name like :"+ModelDataTable.NAME,
			ModelDataTable.NAME, "%"
		);
	
	private List<SystemConfig> systems = new ArrayList<SystemConfig>();
	private SystemConfig system = new SystemConfig();
	
	private List<SelectItem> selectItems = new ArrayList<SelectItem>();
	
	@PostConstruct
	public void init() {
		reload();
	}
	
	public ModelDataTable<SystemConfig> getModel() {
		return model;
	}

	public List<SystemConfig> getSystems() {
		return systems;
	}
	public void setSystems(List<SystemConfig> systems) {
		this.systems = systems;
	}

	public SystemConfig getSystem() {
		return system;
	}
	public void setSystem(SystemConfig system) {
		this.system = (system == null ? new SystemConfig() : system);
	}

	public List<SelectItem> getSelectItems() {
		return selectItems;
	}
	public void setSelectItems(List<SelectItem> selectItems) {
		this.selectItems = selectItems;
	}

	public boolean isSystemsEmpty() {
		return systems == null || systems.isEmpty();
	}
	
	public MonitoredSystem monitored(SystemConfig system){
		return SmpClient.instance.getSystem(system.getName());
	}
	
	public List<SystemConfig> getAllSystems(){
		return DataAccess.systemConfigDao.getAllSystemConfigs();
	}
	
	public List<SelectItem> getAllSelectItems(boolean withAll){
		List<SystemConfig> systems = getAllSystems(); 		 
		List<SelectItem> selectItems = new ArrayList<SelectItem>();	
		if(withAll){
			selectItems.add(new SelectItem("All", "All"));
		}
	    for(SystemConfig system: systems){
	    	selectItems.add(new SelectItem(system.getName(), system.getName()));
	    }
	    return selectItems;
	}
	public List<SelectItem> getAllSelectItems(){
		return getAllSelectItems(false);
	}
	
	@Override
	public void reload() {
		system = new SystemConfig();
		selectItems = getAllSelectItems(true);
		    
		SystemConfig selected = model.getSelected(true);
		if(selected != null){
			systems = new ArrayList<SystemConfig>();		
			systems.add(selected);
		}else{
			systems = getAllSystems();
		}
	}
	
	public void select(SystemConfig system){
		model.setParamName(system.getName());
	}

	public void add() {
		boolean ok = true;

		SystemConfig dummy = DataAccess.systemConfigDao.getSystemByName(system.getName());
		if (dummy != null) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.DUPLICATED_NAME);
		}

		if (ok) {
			DataAccess.systemConfigDao.save(system);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-system", system.toString());
			
			SmpClient.instance.addSystem(system);
			reload();
		}
	}
	
	public void delete(){
		delete(system);
	}

	public void delete(SystemConfig system) {
		boolean ok = true;
		if (!system.getHosts().isEmpty()) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.SITE_DELETE_FAILED_HAVEHOST);
		}
		if (ok) {
			MonitoredSystem monSystem = SmpClient.instance.getSystem(system.getName());
			if(monSystem == null){
				DataAccess.systemConfigDao.delete(system);
			}else{
				try{
					SmpClient.instance.deleteSystem(monSystem);
				}catch(Exception e){
					log.error("failed delete system ["+system.getName()+"]");
				}
			}
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-system", system.getName());
			reload();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, system.getName() + " "
							+ "is deleted");
		}
	}

	public void edit() {
		log.debug("system=" + system);
		boolean ok = true;

		SystemConfig dummy = DataAccess.systemConfigDao.getSystemByName(system.getName());

		if (dummy != null && !dummy.equals(system)) {
			ok = false;
			system = new SystemConfig();
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.SITE_DUPLICATED);
		}

		if (ok) {
			DataAccess.systemConfigDao.merge(system);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-system", system.getName());
			
			MonitoredSystem monSystem = SmpClient.instance.getSystemByID(system.getId());
			if(monSystem == null){
				SmpClient.instance.addSystem(system);
			}else{
				SystemConfig backupSystem = monSystem.getBackupSystemConfig();
				monSystem.updateConfig(system);
				SmpClient.instance.reloadSystem(backupSystem, monSystem);
			}
			reload();
		}
	}

}
