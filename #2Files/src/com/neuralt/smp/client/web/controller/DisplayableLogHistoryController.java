package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.data.LazyDisaplayableLogDataModel;
import com.neuralt.smp.client.data.model.DisplayableLog;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.StringUtil;

@ManagedBean
@ViewScoped
public class DisplayableLogHistoryController implements Serializable {
	private static final long serialVersionUID = 8679732830955923677L;

	private static final Logger logger = LoggerFactory
			.getLogger(DisplayableLogHistoryController.class);

	@ManagedProperty(value = "#{entitySelectorController}")
	private EntitySelectorController entitySelector;

	private int rowNumber = 20;
	private static final String BACKGROUND_RED = "alarm-red";
	private static final String BACKGROUND_WHITE = "alarm-white";
	private static final String BACKGROUND_ORANGE = "alarm-orange";
	private static final String TEXT_RED = "text-red";
	private static final String TEXT_GREEN = "text-green";
	private static final String TEXT_ORANGE = "text-orange";
	private Date timeFrom;
	private Date timeTo;
	private LazyDisaplayableLogDataModel lazyDataModel;
	private Map<String, String> cssStyleMap;
	private boolean alarmSelected;

	public DisplayableLogHistoryController() {
		lazyDataModel = new LazyDisaplayableLogDataModel();
		cssStyleMap = createStatusCssMap();
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public LazyDisaplayableLogDataModel getLazyDataModel() {
		return lazyDataModel;
	}

	public void setLazyDataModel(LazyDisaplayableLogDataModel lazyDataModel) {
		this.lazyDataModel = lazyDataModel;
	}

	public Date getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(Date timeFrom) {
		this.timeFrom = timeFrom;
	}

	public Date getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(Date timeTo) {
		this.timeTo = timeTo;
	}

	public void updateColumns() {
		// reset table state
		UIComponent table = FacesContext.getCurrentInstance().getViewRoot()
				.findComponent(":mainForm:tblAlarm");
		table.setValueExpression("sortBy", null);

	}

	public String getAlarmColor(DisplayableLog log) {
		if (log != null) {
			if (!StringUtil.isNullOrEmpty(log.getLevel())) {
				String result = cssStyleMap.get(log.getLevel());
				return !StringUtil.isNullOrEmpty(result) ? result : "";
			}
		}
		return "";
	}

	public EntitySelectorController getEntitySelector() {
		return entitySelector;
	}

	public void setEntitySelector(EntitySelectorController entitySelector) {
		this.entitySelector = entitySelector;
		this.entitySelector
				.setEntitySelectionListener(new AlarmEntitySelectionListener());
	}

	public void onDateSelect(SelectEvent event) {
		if (timeFrom != null)
			lazyDataModel.setTimeFrom(timeFrom);
		if (timeTo != null)
			lazyDataModel.setTimeTo(timeTo);
	}

	public void search() {

	}

	private Map<String, String> createStatusCssMap() {
		Map<String, String> map = new HashMap<String, String>();
		// map.put(WarningLevel.INFO.name(), TEXT_GREEN);
		// map.put(WarningLevel.NORMAL.name(), TEXT_GREEN);
		map.put(WarningLevel.WARNING.name(), TEXT_ORANGE);
		map.put(WarningLevel.MAJOR.name(), TEXT_RED);
		map.put(WarningLevel.MINOR.name(), TEXT_RED);
		map.put(WarningLevel.ERROR.name(), TEXT_RED);
		map.put(WarningLevel.FATAL.name(), TEXT_RED);
		return map;
	}

	private class AlarmEntitySelectionListener implements
			EntitySelectorController.EntitySelectionListener {

		@Override
		public void onEntitySelect(MonitoredEntity selectedEntity) {
			/*
			 * if (entitySelector.getSelectedHost()!=null) {
			 * lazyDataModel.setHost(entitySelector.getSelectedHost()); } else
			 * if (entitySelector.getSelectedSystem()!=null) {
			 * lazyDataModel.setSystem(entitySelector.getSelectedSystem()); }
			 */
			try {
				if (entitySelector.getSelectedHost() != null) {
					lazyDataModel.setHost(entitySelector.getSelectedHost());
					// allAlarms =
					// DataAccess.alarmDao.getAlarmsByHost(entitySelector.getSelectedHost().getName());
				} else if (entitySelector.getSelectedSystem() != null) {
					lazyDataModel.setSystem(entitySelector.getSelectedSystem());
					// allAlarms =
					// DataAccess.alarmDao.getAlarmBySystem(entitySelector.getSelectedSystem());
				} else {
					// allAlarms = DataAccess.alarmDao.getAllAlarms();
					lazyDataModel.setHost(null);
					lazyDataModel.setSystem(null);
				}
				// lazyDataModel.setDataSource(allAlarms);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.warn("Failed to query db:" + e);
			}
		}
	}
}
