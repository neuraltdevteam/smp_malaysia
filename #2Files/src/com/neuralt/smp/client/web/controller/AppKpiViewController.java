package com.neuralt.smp.client.web.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.data.ChartTemplate;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataPoint;
import com.neuralt.smp.client.data.DataWrapper;
import com.neuralt.smp.client.data.GraphWrapper;
import com.neuralt.smp.client.data.RrdArchiveConfig;
import com.neuralt.smp.client.data.RrdArchiveConfig.ArchiveType;
import com.neuralt.smp.client.data.RrdArchiveConfig.TimeType;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.client.task.FileDeleteTask;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.rrd.RrdService;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class AppKpiViewController implements Serializable {
	private static final long serialVersionUID = 3472571438579055566L;
	private static final Logger logger = LoggerFactory
			.getLogger(AppKpiViewController.class);

	private TreeNode defRoot;
	private TreeNode selectedNode;
	private MonitoredHost selectedHost;
	private StreamedContent graph;
	private CartesianChartModel customGraph;
	private RrdDefConfig selectedDef;
	private Date from;
	private Date to;
	private String title;
	private String verticalLabel;
	private int step;
	private Long start;
	private ArchiveType type;
	private boolean timeset;
	private boolean showGraph;

	public boolean isShowGraph() {
		return showGraph;
	}

	private PanelGrid pg;
	private List<String> archiveList;
	private List<String> dsList;
	private String selectedArchive;
	private String[] selectedArchives;
	private String[] selectedDss;
	private int timeRange;
	private TimeType timeRangeType;
	private List<GraphWrapper> graphs;
	private ChartTemplate template;

	public AppKpiViewController() {
		archiveList = new ArrayList<String>();
		dsList = new ArrayList<String>();
		reset();
		createDefTreeTable();
		start = System.currentTimeMillis();
		graphs = new ArrayList<GraphWrapper>();
	}

	private void createDefTreeTable() {
		defRoot = new DefaultTreeNode("root", null);

		for (RrdDefConfig def : DataAccess.rrdDefConfigDao.getAllRrdDefs()) {
			TreeNode defNode = new DefaultTreeNode(def, defRoot);
			defNode.setExpanded(true);
		}
	}

	public void onNodeSelected(NodeSelectEvent event) {
		logger.debug("node selected");
		if (event.getTreeNode().getData() instanceof RrdDefConfig) {
			selectedDef = ((RrdDefConfig) event.getTreeNode().getData());
			createLists();
		} else {
			selectedDef = null;
		}
		selectedArchive = null;
		customGraph = null;
		timeset = false;
		timeRangeType = TimeType.SECOND;
		logger.debug("node select finished");
	}

	private void createGraphs() {
		graphs.clear();
		logger.debug("starting to prepare graph");
		for (RrdArchiveConfig archive : selectedDef.getRrdArchiveConfigs()) {
			GraphWrapper wrapper = new GraphWrapper();
			wrapper.setTitle(selectedDef.getName() + " - " + archive.getName());
			wrapper.setvLabel(archive.getUnit());
			wrapper.setExtender("ext_" + archive.getName());
			List<DataWrapper> wrappers = null;
			int interval = archive.getStep() * selectedDef.getStep();
			long now = System.currentTimeMillis();
			now = now / ConstantUtil.SEC_MS / interval;
			now = now * interval * ConstantUtil.SEC_MS;
			long past = now - interval * archive.getRow() * ConstantUtil.SEC_MS;
			from = new Date(past);
			to = new Date(now);
			step = interval;
			type = archive.getType();
			start = past - 5 * ConstantUtil.SEC_MS;
			try {
				wrappers = RrdService.getDefaultRrdAccess().fetchData(from, to,
						step, type, selectedDef);
				wrapper.setChart(buildChart(wrappers));
			} catch (IOException e) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR, MessageKey.FILE_ERROR);
				logger.error("failed in create graph", e);
			}

			graphs.add(wrapper);
		}
	}

	private void createLists() {
		archiveList.clear();
		archiveList.add("");
		for (RrdArchiveConfig archive : selectedDef.getRrdArchiveConfigs()) {
			archiveList.add(archive.getName());
		}
		Collections.sort(archiveList);
		dsList.clear();
		for (RrdDsConfig ds : selectedDef.getRrdDsConfigs()) {
			dsList.add(ds.getName());
		}
		Collections.sort(dsList);
	}

	private void reset() {

	}

	private void prepareArchiveDetails() {
		if (!StringUtil.isNullOrEmpty(selectedArchive)) {
			logger.debug("setting archive and other params...");
			RrdArchiveConfig archive = selectedDef.getArchive(selectedArchive);
			int interval = archive.getStep() * selectedDef.getStep();
			long now = System.currentTimeMillis();
			if (to != null) {
				now = to.getTime() / ConstantUtil.SEC_MS / interval;
			} else {
				now = now / ConstantUtil.SEC_MS / interval;
			}
			now = now * interval * ConstantUtil.SEC_MS;
			long difference = RrdArchiveConfig.translateTime(timeRange,
					timeRangeType) * ConstantUtil.SEC_MS;
			if (from != null) {
				difference = to.getTime() - from.getTime();
			}
			difference = difference / ConstantUtil.SEC_MS / interval;
			difference = difference * interval * ConstantUtil.SEC_MS;
			long past = now - difference;
			from = new Date(past);
			to = new Date(now);
			step = interval;
			type = archive.getType();
			start = past - 5 * ConstantUtil.SEC_MS;
			verticalLabel = archive.getUnit();
			title = selectedDef.getName() + " - " + archive.getName();
		}
	}

	public void prepareGraphByArchive() {
		logger.debug("starting to prepare graph");
		List<DataWrapper> wrappers = null;
		try {
			wrappers = RrdService.getDefaultRrdAccess().fetchData(from, to,
					step, type, selectedDef);
			buildGraph(wrappers);
		} catch (IOException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.FILE_ERROR);
			logger.error("failed in prepare graph", e);
		}
	}

	public void prepareGraph() {
		logger.debug("starting to prepare graph");
		if (from == null || to == null) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.TIME_RANGE_NOT_SPECIFIED);
			return;
		}
		List<DataWrapper> wrappers = null;
		try {
			wrappers = RrdService.getDefaultRrdAccess().fetchData(from, to,
					step, type, selectedDef);
			buildGraph(wrappers);
		} catch (IOException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.FILE_ERROR);
			logger.error("failed in prepare graph", e);
		}
		timeset = false;
	}

	private void buildGraph(List<DataWrapper> wrappers) {
		logger.debug("starting build graph");
		customGraph = new CartesianChartModel();
		for (DataWrapper dw : wrappers) {
			if (dw.getPoints().size() != 0) {
				LineChartSeries series = new LineChartSeries();
				series.setLabel(dw.getPoints().get(0).getName());
				logger.debug("adding series for " + series.getLabel());
				Map<Object, Number> map = new HashMap<Object, Number>();
				for (DataPoint dp : dw.getPoints()) {
					if (!dp.getValue().equals(Double.NaN))
						map.put(dp.getTime(), dp.getValue());
					// logger.debug("data point for "+series.getLabel()+" time="+new
					// Date(dp.getTime())+", value="+dp.getValue());
				}
				if (map.size() != 0) {
					series.setData(map);
					customGraph.addSeries(series);
				}
			}
		}
	}

	private CartesianChartModel buildChart(List<DataWrapper> wrappers) {
		logger.debug("starting build graph");
		CartesianChartModel chart = new CartesianChartModel();
		for (DataWrapper dw : wrappers) {
			if (dw.getPoints().size() != 0) {
				LineChartSeries series = new LineChartSeries();
				series.setLabel(dw.getPoints().get(0).getName());
				// logger.debug("adding series for "+series.getLabel());
				Map<Object, Number> map = new HashMap<Object, Number>();
				for (DataPoint dp : dw.getPoints()) {
					map.put(dp.getTime(), dp.getValue());
					// logger.debug("data point for "+series.getLabel()+" time="+new
					// Date(dp.getTime())+", value="+dp.getValue());
				}
				series.setData(map);
				chart.addSeries(series);
			}
		}
		return chart;
	}

	public void onFromSelected(SelectEvent event) {
		from = (Date) event.getObject();
		start = from.getTime() - 5 * ConstantUtil.SEC_MS;
		logger.debug("start set:" + new Date(start));
	}

	// getter/setter
	public StreamedContent getFile() {
		logger.debug("download xml called for " + selectedDef.getName());
		String filename = null;
		try {
			filename = RrdService.getDefaultRrdAccess()
					.generateXml(selectedDef);
		} catch (IOException e) {
			logger.error("failed in prepare XML for export", e);
		}

		logger.debug("filename:" + filename);
		logger.debug("ready to open input stream");
		InputStream stream = null;
		try {
			stream = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
			logger.warn("file not found:" + e.getMessage());
		}
		StringBuilder sb = new StringBuilder();
		sb.append(AppConfig.getRrdXmlPrefix()).append(selectedDef.getName())
				.append(".xml");

		StreamedContent file = new DefaultStreamedContent(stream,
				"application/xml", sb.toString());

		// Primefaces should close the stream automatically

		Thread deleteTask = new FileDeleteTask(filename);
		deleteTask.start();

		return file;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public TreeNode getDefRoot() {
		return defRoot;
	}

	public MonitoredHost getSelectedHost() {
		return selectedHost;
	}

	public StreamedContent getGraph() {
		return graph;
	}

	public RrdDefConfig getSelectedDef() {
		return selectedDef;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
		start = from.getTime();
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		// logger.debug("setting title...");
		this.title = title;
	}

	public String getVerticalLabel() {
		return verticalLabel;
	}

	public void setVerticalLabel(String verticalLabel) {
		// logger.debug("setting vertical label...");
		this.verticalLabel = verticalLabel;
	}

	public int getStep() {
		return step;
	}

	public void setStep(int step) {
		this.step = step;
	}

	public ArchiveType getType() {
		return type;
	}

	public void setType(ArchiveType type) {
		this.type = type;
	}

	public CartesianChartModel getCustomGraph() {
		return customGraph;
	}

	public Long getStart() {
		return start;
	}

	public boolean isTimeset() {
		return timeset;
	}

	public PanelGrid getPg() {
		return pg;
	}

	public List<String> getArchiveList() {
		return archiveList;
	}

	public List<String> getDsList() {
		return dsList;
	}

	public String[] getSelectedArchives() {
		return selectedArchives;
	}

	public void setSelectedArchives(String[] selectedArchives) {
		this.selectedArchives = selectedArchives;
	}

	public String[] getSelectedDss() {
		return selectedDss;
	}

	public void setSelectedDss(String[] selectedDss) {
		this.selectedDss = selectedDss;
	}

	public String getSelectedArchive() {
		return selectedArchive;
	}

	public List<GraphWrapper> getGraphs() {
		return graphs;
	}

	public int getTimeRange() {
		return timeRange;
	}

	public void setTimeRange(int timeRange) {
		this.timeRange = timeRange;
		prepareArchiveDetails();
	}

	public TimeType getTimeRangeType() {
		return timeRangeType;
	}

	public void setTimeRangeType(TimeType timeRangeType) {
		this.timeRangeType = timeRangeType;
		prepareArchiveDetails();
	}

	public void setSelectedArchive(String selectedArchive) {
		this.selectedArchive = selectedArchive;
		prepareArchiveDetails();
	}

	public ChartTemplate getTemplate() {
		return template;
	}

	public void setTemplate(ChartTemplate template) {
		this.template = template;
	}
}
