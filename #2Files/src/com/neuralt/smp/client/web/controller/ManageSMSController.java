/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuralt.smp.client.web.controller;

/**
 *
 * @author ag111
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.data.ManageSMS;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class ManageSMSController implements Serializable {
	private static final long serialVersionUID = -7016477944366559290L;
	private static final Logger logger = LoggerFactory
			.getLogger(SiteController.class);
	private List<ManageSMS> managesmss = new ArrayList<ManageSMS>();
	private ManageSMS managesms = new ManageSMS();
	private ManageSMS selectedmanagesms;
	private ManageSMS dummymanagesms= new ManageSMS();
	private List<SelectItem> cpSelectItems = new ArrayList<SelectItem>();	
	private String passedParameter;
	
	public ManageSMSController() {
		reload();
	}

	public void reload() {
		managesms = new ManageSMS();
		dummymanagesms = new ManageSMS();		
		
		managesmss = DataAccess.manageSMSDao.getAllSMSs();
		if (managesmss != null) {
			logger.debug("sms size=" + managesmss.size());
		}
	   	   		 
		cpSelectItems = null;
		cpSelectItems = new ArrayList<SelectItem>();	
		
		cpSelectItems.add(new SelectItem("All", "All"));
		
	    for(ManageSMS managesms: managesmss){
	    	//cpSelectItems.add(new SelectItem(site.getSiteName(), site.getSiteName()));
                cpSelectItems.add(new SelectItem(managesms.getchannel_id(), managesms.getid().toString()));
	    }
		    
		selectedmanagesms = null;
		
		String selectedSMSID = getPassedParameter("dataTabView:siteForm:ManageSMS"); 
		logger.info("selectedSMSID=" + selectedSMSID);
		if (selectedSMSID != null)
		{
			
			if (selectedSMSID != "" && selectedSMSID != "All")
			{
				managesmss = null;
				managesmss = new ArrayList<ManageSMS>();		
				//managesmss.add( DataAccess.manageSMSDao.getSiteByName(selectedSiteName));
                                managesmss.add(DataAccess.manageSMSDao.getSMSByName(selectedSMSID));
			}
		}
	}

	public void addSite() {
		boolean ok = true;

//		ManageSMS dummy = DataAccess.siteDao.getSiteByName(site.getSiteName());
                ManageSMS dummy = DataAccess.manageSMSDao.getSMSByName(managesms.getchannel_id().toString());
		if (dummy != null) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.GENERAL_ADD_FAIL);
		}

		if (ok) {
			//DataAccess.siteDao.save(site);
                        DataAccess.manageSMSDao.save(managesms);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-manageSMS", managesms.toString());
			reload();
		}
	}

	public void deleteSite(ManageSMS managesms) {
		boolean ok = true;
		//if (managesms.getHostCount() != 0) {
                if (managesms.getchannel_id() != 0) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.GENERAL_DELETE_FAIL);
		}
		if (ok) {
			DataAccess.manageSMSDao.delete(managesms);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-site", managesms.toString());
			reload();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, managesms.getchannel_id()+ " "
							+ "is deleted");
		}
	}

	public void editSite() {
		logger.debug("dummySite=" + dummymanagesms);
		logger.debug("site=" + managesms);
		boolean ok = true;

		ManageSMS dummy = DataAccess.manageSMSDao.getSMSByName(managesms.getchannel_id().toString());

		if (dummy != null && !dummy.equals(managesms)) {
			ok = false;
			managesms = new ManageSMS();
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.GENERAL_EDIT_FAIL);
		}

		if (ok) {
			managesms.update(dummymanagesms);
			DataAccess.manageSMSDao.save(managesms);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-cp", managesms.toString());
			reload();
		}
	}

	// getter/setter

	public List<ManageSMS> getManageSMSs() {
		return managesmss;
	}

	public void setManageSMSs(List<ManageSMS> managesmss) {
		this.managesmss = managesmss;
	}

	public ManageSMS getManageSMS() {
		return managesms;
	}

	public void setManageSMS(ManageSMS managesms) {
		this.managesms = managesms;
	}

	public ManageSMS getSelectedSMS() {
		return selectedmanagesms;
	}

	public void setSelectedSMS(ManageSMS selectedmanagesms) {
		this.selectedmanagesms = selectedmanagesms;
	}

	public ManageSMS getDummySMS() {
		return dummymanagesms;
	}

	public void setDummySMS(ManageSMS dummymanagesms) {
		this.dummymanagesms = dummymanagesms.getBackup();
		this.managesms = dummymanagesms;
	}

	public boolean isSitesEmpty() {
		return managesmss == null || managesmss.isEmpty();
	}
	
	public List<SelectItem> getSMSSelectItems() {
		return cpSelectItems;
	}

	public void setSMSSelectItems(List<SelectItem> cpSelectItems) {
		this.cpSelectItems = cpSelectItems;
	}
	
	public String getPassedParameter(String paramName) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		this.passedParameter = (String) facesContext.getExternalContext().
		getRequestParameterMap().get(paramName);
		return this.passedParameter;
	}
		 

	public void setPassedParameter(String passedParameter) {
		this.passedParameter = passedParameter;
	}	
}
