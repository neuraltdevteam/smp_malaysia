package com.neuralt.smp.client.web.controller;

public interface ReloadableController {

	public void reload();
}
