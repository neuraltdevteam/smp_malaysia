package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.VariableBinding;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.MibFlag;
import com.neuralt.smp.client.data.TrapContainer;
import com.neuralt.smp.client.snmp.SnmpAccessException;
import com.neuralt.smp.client.snmp.util.SnmpGetStrategy;
import com.neuralt.smp.client.snmp.util.V1GetStrategy;
import com.neuralt.smp.client.snmp.util.V2GetStrategy;
import com.neuralt.smp.client.snmp.util.V3GetStrategy;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.HostSnmpConfig;
import com.neuralt.smp.config.HostSnmpConfig.MibFileWrapper;
import com.neuralt.smp.snmp.util.MibFileLoader;
import com.neuralt.web.util.jsf.WebUtil;

/**
 * This class will serve as a MIB browser with actual SNMP get function.
 * 
 */
@ManagedBean
@ViewScoped
public class SnmpBrowseController implements Serializable {
	private static final long serialVersionUID = -1996938489921271057L;
	private static final Logger logger = LoggerFactory
			.getLogger(SnmpBrowseController.class);

	private TreeNode oidRoot;
	private TreeNode selectedOidNode;
	private TreeNode snmpRoot;
	private TreeNode selectedSnmpNode;
	private List<TrapContainer> results;
	// private List<TrapContainer> trapList;
	private List<OID> oidList;

	public SnmpBrowseController() {
		createOidTreeTable();
		createSnmpTreeTable();
		results = new ArrayList<TrapContainer>();
	}

	public void reset() {

	}

	private void createOidTreeTable() {
		if (selectedSnmpNode == null
				|| !(selectedSnmpNode.getData() instanceof HostSnmpConfig)) {
			oidList = null;
			oidRoot = null;
		} else {
			HostSnmpConfig snmp = (HostSnmpConfig) (selectedSnmpNode.getData());
			oidList = new ArrayList<OID>(MibFileLoader.getInstance()
					.getAllTrapNameMap().keySet());
			// trapList = new ArrayList<TrapContainer>();
			if (!oidList.isEmpty()) {
				Collections.sort(oidList);
				oidList.remove(0); // to remove the entry "0.0"
				oidRoot = new DefaultTreeNode("root", null);

				boolean first = true;
				TreeNode currentNode = oidRoot;
				List<String> mibNamesList = new ArrayList<String>();
				for (MibFileWrapper wrapper : snmp.getMibFileList()) {
					mibNamesList.add(wrapper.getMibName());
				}
				List<MibFlag> mibFlagList = DataAccess.mibFlagDao
						.getAllMibFlags();
				for (OID oid : oidList) {
					boolean ok = false;
					// for each OID, find out the MIBs that this OID belongs to
					List<String> mibNameMapList = MibFileLoader.getInstance()
							.getAllTrapToMibNameMap().get(oid);
					for (String name : mibNameMapList) {

						for (MibFlag flag : mibFlagList) {
							if (name.equals(flag.getName()) && flag.isLoaded()) {
								ok = true;
								break;
							}
						}

						if (ok) {
							break;
						}
					}
					if (ok) {
						for (String name : mibNameMapList) {
							// find out whether the MIBs are inside the SNMP
							// config
							if (mibNamesList.contains(name)) {
								TrapContainer container = new TrapContainer(
										oid, MibFileLoader.getInstance()
												.getAllTrapNameMap().get(oid));
								// logger.debug("container oid="+container.getOid()+", name="+container.getName());
								// logger.debug("currentNode data is null:"+Boolean.valueOf(currentNode.getData()==null));
								if (currentNode != oidRoot) {
									while (!container.getOid().startsWith(
											((TrapContainer) currentNode
													.getData()).getOid())) {
										currentNode = currentNode.getParent();
										if (currentNode == oidRoot) {
											break;
										}
									}
								}
								TreeNode newNode = new DefaultTreeNode(
										container, currentNode);
								currentNode = newNode;
								currentNode.setExpanded(true);
								break;
							}
						}
					}
				}
			}
		}
	}

	private void createSnmpTreeTable() {
		snmpRoot = new DefaultTreeNode("root", null);

		for (MonitoredSystem sys : SmpClient.instance.getSystems()) {
			TreeNode sysNode = new DefaultTreeNode(sys, snmpRoot);
			sysNode.setExpanded(true);
			for (MonitoredHost host : sys.getHosts()) {
				TreeNode hostNode = new DefaultTreeNode(host, sysNode);
				hostNode.setExpanded(true);
				for (HostSnmpConfig snmpConfig : host.getConfig()
						.getSnmpConfigs()) {
					TreeNode procNode = new DefaultTreeNode(snmpConfig,
							hostNode);
					procNode.setExpanded(true);
				}
			}
		}
	}

	public String getHeader() {
		TrapContainer container = (TrapContainer) selectedOidNode.getData();
		StringBuilder sb = new StringBuilder();
		sb.append(container.getOid()).append(" - ").append(container.getName());
		return sb.toString();
	}

	public void query() {
		results.clear();
		TrapContainer container = (TrapContainer) selectedOidNode.getData();
		HostSnmpConfig target = (HostSnmpConfig) (selectedSnmpNode.getData());
		SnmpGetStrategy snmpStrategy = buildSnmpStrategy(target);
		try {
			List<VariableBinding> vbs = snmpStrategy.walk(container.getOid());
			for (VariableBinding vb : vbs) {
				results.add(new TrapContainer(vb.getOid(), vb.toValueString()));
			}
			snmpStrategy.closeSnmp();
			AuditLogger.logAuditTrail(ScreenType.COMMAND, ActionType.COMMAND,
					"query-SNMP", "target=" + target.toString() + ", OID="
							+ container.getName());
		} catch (SnmpAccessException e) {
			WebUtil.addMessage(MessageKey.GENERAL_ERROR, e.getMessage(),
					FacesMessage.SEVERITY_ERROR);
		}
	}

	private SnmpGetStrategy buildSnmpStrategy(HostSnmpConfig hostSnmpConfig) {
		SnmpGetStrategy strat = null;
		switch (hostSnmpConfig.getVersion()) {

		case SNMPV1:
			strat = new V1GetStrategy(hostSnmpConfig.getHost().getIpAddr(),
					hostSnmpConfig.getCommunityGet(), hostSnmpConfig.getPort());
			break;
		case SNMPV2:
			strat = new V2GetStrategy(hostSnmpConfig.getHost().getIpAddr(),
					hostSnmpConfig.getCommunityGet(), hostSnmpConfig.getPort());
			break;
		case SNMPV3:
			strat = new V3GetStrategy(hostSnmpConfig.getHost().getIpAddr(),
					hostSnmpConfig.getPort(), hostSnmpConfig.getUsername(),
					hostSnmpConfig.getAuth().getProtocol(),
					hostSnmpConfig.getAuthPassword(), hostSnmpConfig.getPriv()
							.getProtocol(), hostSnmpConfig.getPrivPassword());
			break;
		}
		return strat;
	}

	public String getTranslatedOid(OID oid) {
		if (oidList.contains(oid)) {
			return MibFileLoader.getInstance().getAllTrapNameMap().get(oid);
		} else {
			OID trim = oid.trim();
			int index = oid.last();
			if (oidList.contains(trim)) {
				StringBuilder sb = new StringBuilder();
				sb.append(
						MibFileLoader.getInstance().getAllTrapNameMap()
								.get(trim)).append(".").append(index);
				return sb.toString();
			}
		}
		return null;
	}

	public TreeNode getSelectedOidNode() {
		return selectedOidNode;
	}

	public void setSelectedOidNode(TreeNode selectedOidNode) {
		this.selectedOidNode = selectedOidNode;
	}

	public TreeNode getSelectedSnmpNode() {
		return selectedSnmpNode;
	}

	public void setSelectedSnmpNode(TreeNode selectedSnmpNode) {
		this.selectedSnmpNode = selectedSnmpNode;
	}

	public TreeNode getOidRoot() {
		return oidRoot;
	}

	public TreeNode getSnmpRoot() {
		return snmpRoot;
	}

	public boolean isOidNodeSelected() {
		return selectedOidNode != null;
	}

	public List<TrapContainer> getResults() {
		return results;
	}

	public void onOidNodeSelected(NodeSelectEvent event) {
		results.clear();
	}

	public void onSnmpNodeSelected(NodeSelectEvent event) {
		createOidTreeTable();
		selectedOidNode = null;
	}

	public boolean isSnmpConfigSelected() {
		if (selectedSnmpNode != null) {
			if (selectedSnmpNode.getData() instanceof HostSnmpConfig) {
				return true;
			}
		}
		return false;
	}

	public String getTreeNodeTypeText(TreeNode node) {
		if (node != null) {
			if (node.getData() instanceof HostSnmpConfig) {
				return "SNMP Agent";
			} else if (node.getData() instanceof MonitoredSystem) {
				return "SYSTEM";
			} else if (node.getData() instanceof MonitoredHost) {
				return "HOST";
			}
		}
		return null;
	}
}
