package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig;

@ManagedBean
@ViewScoped
public class ConfigMonitorController implements ReloadableController,
		Serializable {

	// private static final Logger log =
	// LoggerFactory.getLogger(ConfigMonitorController.class);

	private static final long serialVersionUID = 1L;

	@ManagedProperty(value = "#{configHostController}")
	private ConfigHostController hostController;

	private List<HostMonitorConfig> monitors = new ArrayList<HostMonitorConfig>();
	private HostMonitorConfig monitor = new HostMonitorConfig();

	private List<SelectItem> selectItems = new ArrayList<SelectItem>();

	private String selectedName;
	private HostMonitorConfig selectedMonitor;

	@PostConstruct
	public void init() {
		reload();
	}

	public ConfigHostController getHostController() {
		return hostController;
	}

	public void setHostController(ConfigHostController hostController) {
		this.hostController = hostController;
	}

	public List<HostMonitorConfig> getMonitors() {
		return monitors;
	}

	public void setMonitors(List<HostMonitorConfig> monitors) {
		this.monitors = monitors;
	}

	public HostMonitorConfig getMonitor() {
		return monitor;
	}

	public void setMonitor(HostMonitorConfig monitor) {
		this.monitor = (monitor == null ? new HostMonitorConfig() : monitor);
	}

	public List<SelectItem> getSelectItems() {
		return selectItems;
	}

	public void setSelectItems(List<SelectItem> selectItems) {
		this.selectItems = selectItems;
	}

	public String getSelectedName() {
		return selectedName;
	}

	public void setSelectedName(String selectedName) {
		this.selectedName = selectedName;
		this.selectedMonitor = null;
	}

	public HostMonitorConfig getSelectedMonitor(boolean reload) {
		if (selectedMonitor == null || reload) {
			selectedMonitor = null;
			if (selectedName == null || selectedName.equalsIgnoreCase("All")) {
				return selectedMonitor;
			}
			for (HostMonitorConfig monitor : getAllMonitors()) {
				if (monitor.getName().name().equals(selectedName)) {
					selectedMonitor = monitor;
					break;
				}
			}
		}
		return selectedMonitor;
	}

	public HostMonitorConfig getSelectedMonitor() {
		return getSelectedMonitor(false);
	}

	public List<HostMonitorConfig> getAllMonitors() {
		HostConfig selectedHost = hostController.getSelectedHost(true);
		if (selectedHost == null) {
			return new ArrayList<HostMonitorConfig>();
		} else {
			// PS_MONITOR Removed for UMobile
			// return new
			// ArrayList<HostMonitorConfig>(selectedHost.getMonitorConfig().values());
			return selectedHost.getHostMonitorConfigAsList();
		}
	}

	public List<SelectItem> getAllSelectItems(boolean withAll) {
		List<HostMonitorConfig> monitors = getAllMonitors();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		if (withAll) {
			selectItems.add(new SelectItem("All", "All"));
		}
		for (HostMonitorConfig m : monitors) {
			selectItems.add(new SelectItem(m.getName().name(), m.getName()
					.name()));
		}
		return selectItems;
	}

	public List<SelectItem> getAllSelectItems() {
		return getAllSelectItems(false);
	}

	@Override
	public void reload() {
		hostController.reload();

		monitor = new HostMonitorConfig();
		selectItems = getAllSelectItems(true);

		HostMonitorConfig selected = getSelectedMonitor(true);
		if (selected != null) {
			monitors = new ArrayList<HostMonitorConfig>();
			monitors.add(selected);
		} else {
			monitors = getAllMonitors();
		}
	}

	public void select(HostMonitorConfig monitor) {
		setSelectedName(monitor.getName().name());
	}

	public void add() {
		HostConfig host = hostController.getSelectedHost();
		host.getMonitorConfig().put(monitor.getName(), monitor);
		DataAccess.hostConfigDao.merge(host);
		reload();
	}

	public void delete() {
		delete(monitor);
	}

	public void delete(HostMonitorConfig monitor) {
		HostConfig host = hostController.getSelectedHost();
		host.getMonitorConfig().remove(monitor.getName());
		DataAccess.hostConfigDao.merge(host);
		reload();
	}

	public void edit() {
		HostManageController hostManager = hostController.getHostManager();
		HostConfig host = hostController.getSelectedHost();
		MonitoredHost monHost = hostController.monitored(host);
		hostManager.editHostMonitorConfig(monHost, monHost.getConfig()
				.getMonitorConfig().get(monitor.getName()), monitor);
		reload();
	}

}
