package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.notification.WarningLevel;

@ManagedBean
@ViewScoped
public class ConfigTriggerController implements ReloadableController, Serializable{
	
	//private static final Logger log = LoggerFactory.getLogger(ConfigTriggerController.class);
	
	private static final long serialVersionUID = 1L;
	
	private static final List<WarningLevel> listWarningLevel = Arrays.asList(
			WarningLevel.WARNING, WarningLevel.MINOR, WarningLevel.MAJOR);

	@ManagedProperty(value="#{configMonitorController}")
	private ConfigMonitorController monitorController;
	
	private List<MonitorThresholdConfig> triggers = new ArrayList<MonitorThresholdConfig>();
	private MonitorThresholdConfig trigger = new MonitorThresholdConfig();
	
	private List<SelectItem> selectItems = new ArrayList<SelectItem>();
	
	private String selectedName;
	private MonitorThresholdConfig selectedTrigger;
	
	@PostConstruct
	public void init() {
		reload();
	}
	
	public ConfigMonitorController getMonitorController() {
		return monitorController;
	}
	public void setMonitorController(ConfigMonitorController monitorController) {
		this.monitorController = monitorController;
	}

	public List<MonitorThresholdConfig> getTriggers() {
		return triggers;
	}
	public void setTriggers(List<MonitorThresholdConfig> triggers) {
		this.triggers = triggers;
	}

	public MonitorThresholdConfig getTrigger() {
		return trigger;
	}
	public void setTrigger(MonitorThresholdConfig trigger) {
		if(trigger == null){
			this.trigger = new MonitorThresholdConfig();
		}else{
			//force reselect monitor
			monitorController.select(trigger.getHostMonitorConfig());
			reload();
			this.trigger = trigger;
		}
	}

	public List<SelectItem> getSelectItems() {
		return selectItems;
	}
	public void setSelectItems(List<SelectItem> selectItems) {
		this.selectItems = selectItems;
	}
	
	public String getSelectedName() {
		return selectedName;
	}
	public void setSelectedName(String selectedName) {
		this.selectedName = selectedName;
		this.selectedTrigger = null;
	}

	public MonitorThresholdConfig getSelectedTrigger(boolean reload) {
		if(selectedTrigger == null || reload){
			selectedTrigger = null;
			if(selectedName == null || selectedName.equalsIgnoreCase("All")){
				return selectedTrigger;
			}
			for(MonitorThresholdConfig trigger : getAllTriggers()){
				if(buildName(trigger).equals(selectedName)){
					selectedTrigger = trigger;
					break;
				}
			}
		}
		return selectedTrigger;
	}
	public MonitorThresholdConfig getSelectedTrigger(){
		return getSelectedTrigger(false);
	}

	public List<MonitorThresholdConfig> getAllTriggers(){
		HostMonitorConfig selectedMonitor = monitorController.getSelectedMonitor(true);
		HostConfig selectedHost = monitorController.getHostController().getSelectedHost();
		if(selectedHost == null){
			return new ArrayList<MonitorThresholdConfig>();
		}else if(selectedMonitor == null){
			//list host all triggers
			List<MonitorThresholdConfig> list = new ArrayList<MonitorThresholdConfig>();
			// PS_MONITOR Removed for UMobile
			// for(HostMonitorConfig monitor : selectedHost.getMonitorConfig().values()){
			for(HostMonitorConfig monitor : selectedHost.getHostMonitorConfigAsList()){
				list.addAll(monitor.getThresholdConfig());
			}
			return list;
		}else{
			return selectedMonitor.getThresholdConfigAsList();
		}
	}
	
	public List<SelectItem> getAllSelectItems(boolean withAll){
		List<MonitorThresholdConfig> triggers = getAllTriggers();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();	
		if(withAll){
			selectItems.add(new SelectItem("All", "All"));
		}
	    for(MonitorThresholdConfig t: triggers){
	    	String name = buildName(t);
	    	selectItems.add(new SelectItem(name, name));
	    }
	    return selectItems;
	}
	public List<SelectItem> getAllSelectItems(){
		return getAllSelectItems(false);
	}
	
	public String buildName(MonitorThresholdConfig trigger){
		return trigger.getHostMonitorConfig().getName().name()+"_"+trigger.getId();
	}
	
	public List<SelectItem> getWarningLevels(){
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		for(WarningLevel level : listWarningLevel){
			selectItems.add(new SelectItem(level.name(), level.name()));
		}
		return selectItems;
	}
	
	@Override
	public void reload() {
		monitorController.reload();
		
		trigger = new MonitorThresholdConfig();
		selectItems = getAllSelectItems(true);
		    
		MonitorThresholdConfig selected = getSelectedTrigger(true);
		if(selected != null){
			triggers = new ArrayList<MonitorThresholdConfig>();
			triggers.add(selected);
		}else{
			triggers = getAllTriggers();
		}
	}
   
    // remove add & delete function on trigger, 20/10/2015 by jimmy.so
/*  
  	public void add() {
		ConfigHostController hostController =  monitorController.getHostController();
		HostManageController hostManager = hostController.getHostManager();
		HostConfig host = hostController.getSelectedHost();
		MonitoredHost monHost = hostController.monitored(host);
		HostMonitorConfig monitor = monitorController.getSelectedMonitor();
		hostManager.addMonitorThresholdConfig(monHost, monHost.getConfig().getMonitorConfig().get(monitor.getName()), trigger);
		reload();
	}
	
	public void delete(){
		delete(trigger);
	}

	public void delete(MonitorThresholdConfig trigger) {
		ConfigHostController hostController =  monitorController.getHostController();
		HostManageController hostManager = hostController.getHostManager();
		HostConfig host = hostController.getSelectedHost();
		MonitoredHost monHost = hostController.monitored(host);
		HostMonitorConfig monitor = monitorController.getSelectedMonitor();
		hostManager.deleteMonitorThresholdConfig(monHost, findOriginalTrigger(monHost, monitor, trigger));
		reload();
	}
	*/

	public void edit() {
		ConfigHostController hostController =  monitorController.getHostController();
		HostManageController hostManager = hostController.getHostManager();
		HostConfig host = hostController.getSelectedHost();
		MonitoredHost monHost = hostController.monitored(host);
		HostMonitorConfig monitor = monitorController.getSelectedMonitor();
		hostManager.editMonitorThresholdConfig(monHost, trigger, findOriginalTrigger(monHost, monitor, trigger));
		reload();
	}
	
	protected MonitorThresholdConfig findOriginalTrigger(MonitoredHost monHost, HostMonitorConfig monitor, MonitorThresholdConfig trigger){
		for(MonitorThresholdConfig t : monHost.getConfig().getMonitorConfig().get(monitor.getName()).getThresholdConfig()){
			if(t.getId() == trigger.getId()){
				return t;
			}
		}
		return trigger;
	}
	
}
