package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredHost.HostStatus;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.config.ProcessStatus;

/**
 * Controller behind the entitySelector composite
 */

@ManagedBean
@ViewScoped
public class EntitySelectorController implements Serializable {

	private static final long serialVersionUID = 7430805640016446854L;
	private static final Logger logger = LoggerFactory
			.getLogger(EntitySelectorController.class);

	private static Map<String, String> statusCssMap = createStatusCssMap();

	private TreeNode entityRoot;
	private TreeNode selectedNode; // for TreeTable's own good health
	private MonitoredSystem selectedSystem;
	private MonitoredHost selectedHost;
	private MonitoredProcess selectedProcess;

	private EntitySelectionListener selectionListener;

	public EntitySelectorController() {
		createTreeTableModel();
	}

	private void createTreeTableModel() {
		entityRoot = new DefaultTreeNode("root", null);
		TreeNode dummyNode = new DefaultTreeNode(new DummyEntity(), entityRoot);
		dummyNode.setExpanded(true);

		for (MonitoredSystem sys : SmpClient.instance.getSystems()) {
			TreeNode sysNode = new DefaultTreeNode(sys, dummyNode);
			sysNode.setExpanded(true);
			for (MonitoredHost host : sys.getHosts()) {
				TreeNode hostNode = new DefaultTreeNode(host, sysNode);
				hostNode.setExpanded(true);
				for (MonitoredProcess proc : host.getProcesses()) {
					TreeNode procNode = new DefaultTreeNode(proc, hostNode);
					procNode.setExpanded(true);
				}
			}
		}
	}

	public void onEntitySelect(NodeSelectEvent event) {
		TreeNode entityNode = event.getTreeNode();
		MonitoredEntity entity = (MonitoredEntity) entityNode.getData();
		switch (entity.getEntityType()) {
		case ALL: {
			// functions as "de-select"
			selectedSystem = null;
			selectedHost = null;
			selectedProcess = null;
		}
			break;
		case SYSTEM: {
			selectedSystem = (MonitoredSystem) entity;
			selectedHost = null;
			selectedProcess = null;
		}
			break;
		case HOST: {
			selectedHost = (MonitoredHost) entity;
			selectedProcess = null;
			TreeNode sysNode = entityNode.getParent();
			selectedSystem = (MonitoredSystem) sysNode.getData();
		}
			break;
		case PROCESS: {
			selectedProcess = (MonitoredProcess) entity;
			TreeNode hostNode = entityNode.getParent();
			selectedHost = (MonitoredHost) hostNode.getData();
			TreeNode sysNode = hostNode.getParent();
			selectedSystem = (MonitoredSystem) sysNode.getData();
		}
			break;
		}

		if (selectionListener != null) {
			selectionListener.onEntitySelect(entity);
		}
	}

	public TreeNode getEntityRoot() {
		return entityRoot;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public MonitoredEntity getSelectedEntity() {
		if (selectedNode != null)
			return (MonitoredEntity) selectedNode.getData();
		return null;
	}

	public MonitoredSystem getSelectedSystem() {
		return selectedSystem;
	}

	public MonitoredHost getSelectedHost() {
		return selectedHost;
	}

	public MonitoredProcess getSelectedProcess() {
		return selectedProcess;
	}

	public String getStatusCssClass(String statusText) {
		String result = statusCssMap.get(statusText);
		if (result == null)
			result = "";
		return result;
	}

	public void setEntitySelectionListener(EntitySelectionListener listener) {
		this.selectionListener = listener;
	}

	private static Map<String, String> createStatusCssMap() {
		String green = "status-green";
		String red = "status-red";
		String gray = "status-gray";

		Map<String, String> map = new HashMap<String, String>();
		map.put(HostStatus.CONNECTED.name(), green);
		map.put(HostStatus.DISCONNECTED.name(), red);
		map.put(ProcessStatus.STARTED.name(), green);
		map.put(ProcessStatus.STOPPED.name(), red);
		map.put(ProcessStatus.STARTING.name(), gray);
		map.put(ProcessStatus.STOPPING.name(), gray);
		return map;
	}

	private static class DummyEntity implements MonitoredEntity {

		private static final long serialVersionUID = 7097864444427530292L;

		public String getDisplayableName() {
			return "All";
		}

		public String getName() {
			return "All";
		}

		public String getFullName() {
			return null;
		}

		public String getDescription() {
			return null;
		}

		public String getStatusText() {
			return null;
		}

		public EntityType getEntityType() {
			return EntityType.ALL;
		}

		public String getCurrentCpuUsage() {
			return null;
		}

		public String getCurrentMemoryUsage() {
			return null;
		}

		public String getDuration() {
			return null;
		}

		public String getLastChecked() {
			return null;
		}
	}

	public static interface EntitySelectionListener {
		public void onEntitySelect(MonitoredEntity selectedEntity);
	}
}
