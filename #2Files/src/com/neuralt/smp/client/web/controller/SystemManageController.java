package com.neuralt.smp.client.web.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredEntity.EntityType;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.SystemConfig;

@ManagedBean
@ViewScoped
public class SystemManageController extends EntityManageController implements
		Serializable {
	private static final Logger logger = LoggerFactory
			.getLogger(SystemManageController.class);
	private static final long serialVersionUID = -3535983852631930314L;

	private SystemConfig system;
	private SystemConfig dummy;

	public SystemManageController() {
		system = new SystemConfig();
		dummy = new SystemConfig();
		type = EntityType.SYSTEM;
	}

	public SystemConfig getSystem() {
		return system;
	}

	public void setSystem(SystemConfig system) {
		this.system = system;
	}

	public SystemConfig getDummy() {
		return dummy;
	}

	public void setDummy(SystemConfig dummy) {
		this.dummy = dummy;
	}

	public void editSystem(SystemConfig dummySystem,
			MonitoredSystem monitoredSystem) {
		// verify for duplicate name
		boolean ok = true;

		if (!dummySystem.getName().equals(monitoredSystem.getName())) {
			try {
				if (SmpClient.instance.getSystem(dummySystem.getName()) != null) {
				}
				ok = false;
			} catch (IllegalArgumentException e) {
			}
		}
		// if ok then copy from dummy to original
		if (ok) {
			SystemConfig backupSystem = monitoredSystem.getBackupSystemConfig();
			monitoredSystem.updateConfig(dummySystem);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-system", monitoredSystem.getFullName());
			try {
				DataAccess.systemConfigDao.save(monitoredSystem.getConfig());
				SmpClient.instance.reloadSystem(backupSystem, monitoredSystem);
			} catch (DataAccessException e) {
				monitoredSystem.updateConfig(backupSystem);
				throwDBExceptionMessage();
			}
		} else {
			throwDuplicatedNameMessage(type);
		}
	}

	public void addSystem() {
		logger.info(system.toString());

		boolean ok = true;

		try {
			if (SmpClient.instance.getSystem(system.getName()) != null) {
			}
			ok = false;
		} catch (IllegalArgumentException e) {
		}

		if (ok) {
			try {
				logger.info(system.toString());
				DataAccess.systemConfigDao.save(system);
				SmpClient.instance.addSystem(system);
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
						"add-system", system.toString());
				resetSystem();
			} catch (DataAccessException e) {
				throwDBExceptionMessage();
			}
		} else {
			throwDuplicatedNameMessage(type);
		}

	}

	public void resetSystem() {
		system = new SystemConfig();
	}

}
