package com.neuralt.smp.client.web.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ConsoleUser;
import com.neuralt.smp.client.web.security.ConsoleUser.Status;
import com.neuralt.smp.client.web.security.CryptUtil;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.web.security.UserBean;
import com.neuralt.smp.client.web.security.UserGroup;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.FacesResourceBundleUtil;
import com.neuralt.web.util.jsf.LocaleMessages;
import com.neuralt.web.util.jsf.MessageUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class UserController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(UserController.class);

	@ManagedProperty(value = "#{UserBean}")
	private UserBean userBean;

	private List<ConsoleUser> consoleUsers;
	private List<UserGroup> userGroups;

	private ConsoleUser selectedUser;
	private String selectedUserGroupId;
	private String passwordCurrent;
	private String password;
	private String passwordReenter;
	private ConsoleUser prev;

	@SuppressWarnings("unused")
	private List<ConsoleUser.Status> statuses;
	private List<String> userGroupIds;
	private HashMap<String, UserGroup> userGroupIdMap;

	private boolean createNew;

	public UserController() {
		setConsoleUsers(DataAccess.webDao.getUsers());
		setUserGroups(DataAccess.webDao.getUserGroups());
	}

	// actions //
	public void onAddGroupClick() {
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("group.xhtml?showAddDialog=true");
		} catch (IOException e) {
			logger.error("failed to redirect", e);
		}
	}

	public void selectUser(ConsoleUser user) {
		selectedUser = user;
		selectedUserGroupId = user.getGroup().getGroupId();
	}

	public void addUser() {
		logger.debug("addUser");
		selectedUser = new ConsoleUser();
		resetPassword();
	}

	private void resetPassword() {
		password = null;
		passwordCurrent = null;
		passwordReenter = null;
	}

	public void deleteUser() {
		logger.debug("deleteUser: {}", selectedUser);
		if (consoleUsers.size() == 1) {
			logger.debug("Not allow to delete all users.");
			addErrorMsg("privilege.userDeleteFailure");
		} else if (selectedUser.getUserId().equals(ConstantUtil.SYS_DEFAULT)) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.USER_MODIFY_FAILED_SYS_DEFAULT);
		} else if (selectedUser.getUserId().equals(WebUtil.getCurrentUserId())) {
			// prevent self deletion
			// TODO maybe OK by adding logout method in block?
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.USER_DELETE_FAILED_SELF);
		} else {
			try {
				DataAccess.webDao.removeUser(selectedUser);
				AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
						ActionType.DELETE, "delete-user",
						selectedUser.getUserId());
			} catch (DataAccessException dbe) {
				addErrorMsg("privilege.userDeleteFailure",
						MessageUtil.getDescriptionOfDBException(dbe),
						new Date());
				MessageUtil.logDBException("deleteUser", dbe);
				refreshUsers();
				return;
			}
			refreshUsers();
			addInfoMsg("privilege.userDeleteSuccess");
		}
	}

	public void saveUser() {
		logger.debug("saveUser: {}", selectedUser);
		if (selectedUser.getUserId().equals(ConstantUtil.SYS_DEFAULT)
				&& !WebUtil.getCurrentUserId().equals(ConstantUtil.SYS_DEFAULT)) {
			// only super user can modify super user
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.USER_MODIFY_FAILED_SYS_DEFAULT_LACK_PRIVILEGE);
			selectedUser = null;
			return;
		}
		if (!createNew) {
			prev = DataAccess.webDao.getUser(selectedUser.getUserId());
			prev.setGroupId(selectedUser.getGroup().getGroupId());
		}
		if (failsValidation(selectedUser)) {
			return;
		}
		if (StringUtil.isNullOrEmpty(selectedUserGroupId)) {
			WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
					FacesResourceBundleUtil
							.getMessage("user.prompt.user_error"),
					FacesResourceBundleUtil.getMessage("user.prompt.usergroup"));
			return;
		}

		if (!selectedUser.getUserId().equals(ConstantUtil.SYS_DEFAULT)) {
			selectedUser.setGroup(getUserGroupIdMap().get(selectedUserGroupId));
			selectedUser.setGroupId(selectedUser.getGroup().getGroupId());
		} else {
			if (!getUserGroupIdMap().get(selectedUserGroupId).getGroupId()
					.equals(ConstantUtil.SUPER_GROUP)) {
				// super user cannot be change to other group
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						MessageKey.USER_MODIFY_FAILED_SYS_DEFAULT_CHANGE_GROUP);
				return;
			}
			if (selectedUser.getStatus().equals(Status.INACTIVE)) {
				// super user cannot be inactive
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						MessageKey.USER_MODIFY_FAILED_SYS_DEFAULT_INACTIVE);
				return;
			}
		}
		boolean savePasswordHistory = false;
		if (!StringUtil.isNullOrEmpty(password)) {
			final String encryptedPassword = encrypt(password);
			if (DataAccess.webDao.isPasswordUsed(selectedUser.getUserId(),
					encryptedPassword)) {
				WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
						FacesResourceBundleUtil
								.getMessage("user.prompt.user_error"),
						FacesResourceBundleUtil
								.getMessage("user.prompt.password_used"));
				return;
			}
			selectedUser.setPassword(encryptedPassword);
		}

		selectedUser.setLastUpdatedBy(userBean.getCurrentUserId());

		try {
			logger.debug("webDao.saveUser: {}", selectedUser);
			DataAccess.webDao.saveUser(selectedUser, savePasswordHistory);
			addInfoMsg("privilege.userSaveSuccess");
		} catch (Exception e) {
			addErrorMsg("general.add_edit_fail",
					MessageUtil.getDescriptionOfDBException(e), new Date());
			MessageUtil.logDBException("saveUser", e);
			refreshUsers();
			return;
		}

		if (createNew) {
			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					ActionType.INSERT, "add-user", selectedUser.getUserId());
		} else {
			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					ActionType.UPDATE, "edit-user", selectedUser.getUserId());
		}

		refreshUsers();
		resetPassword();
	}

	public void refreshUsers() {
		setConsoleUsers(DataAccess.webDao.getUsers());
	}

	public String decrypt(String password) {
		try {
			if (password != null)
				return CryptUtil.decrypt(password);
			else
				return null;
		} catch (Exception e) {
			logger.error("decrypt failed", e);
			return null;
		}
	}

	public String encrypt(String password) {
		try {
			if (!StringUtil.isNullOrEmpty(password))
				return CryptUtil.encrypt(password);
			else
				return null;
		} catch (Exception e) {
			logger.error("encrypt failed", e);
			return null;
		}
	}

	private String addInfoMsg(String msgKey) {
		FacesMessage message = LocaleMessages.getMessage(msgKey);
		message.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return message.getSummary();
	}

	private String addErrorMsg(String msgKey) {
		FacesMessage message = LocaleMessages.getMessage(msgKey);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return message.getSummary();
	}

	private String addErrorMsg(String msgKey, String e, Date date) {
		FacesMessage message = LocaleMessages.getMessage(msgKey, e, date);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return message.getSummary();
	}

	// validation //

	public boolean failsValidation(ConsoleUser user) {
		return (userIdExistsAlready(user));
	}

	public boolean userIdExistsAlready(ConsoleUser newUser) {
		for (ConsoleUser user : consoleUsers) {
			if (user.getUserId().equals(newUser.getUserId()) && user != newUser) {
				addErrorMsg("privilege.userExistsAlready");
				return true; // userid exists already, bad
			}
		}
		return false; // userid doesnt exist yet, ok
	}

	// getters setters //

	public List<ConsoleUser> getConsoleUsers() {
		return consoleUsers;
	}

	public void setConsoleUsers(List<ConsoleUser> consoleUsers) {
		this.consoleUsers = consoleUsers;
	}

	public List<UserGroup> getUserGroups() {
		if (userGroups == null) {
			userGroups = DataAccess.webDao.getUserGroups();
			return userGroups;
		}
		return userGroups;
	}

	public void setUserGroups(List<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	public ConsoleUser getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(ConsoleUser selectedUser) {
		this.selectedUser = selectedUser;
	}

	public List<ConsoleUser.Status> getStatuses() {
		return statuses = Arrays.asList(ConsoleUser.Status.values());
	}

	public HashMap<String, UserGroup> getUserGroupIdMap() {
		if (userGroupIdMap == null) {
			userGroupIdMap = new HashMap<String, UserGroup>();
			for (UserGroup group : getUserGroups()) {
				userGroupIdMap.put(group.getGroupId(), group);
			}
		}
		return userGroupIdMap;
	}

	public List<String> getUserGroupIds() {
		if (userGroupIds == null) {
			userGroupIds = new ArrayList<String>();
			for (UserGroup group : getUserGroups()) {
				userGroupIds.add(group.getGroupId());
			}
		}
		return userGroupIds;
	}

	public String getSelectedUserGroupId() {
		return selectedUserGroupId;
	}

	public void setSelectedUserGroupId(String selectedUserGroupId) {
		this.selectedUserGroupId = selectedUserGroupId;
	}

	public String getPassword() {
		// if (selectedUser!=null && selectedUser.getPassword()!=null) {
		// password = decrypt(selectedUser.getPassword());
		// }
		return password;
	}

	public void setPassword(String password) {
		// if (selectedUser!=null) {
		// password = encrypt(password);
		// }
		this.password = password;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	public boolean isCreateNew() {
		return createNew;
	}

	public void setCreateNew(boolean createNew) {
		this.createNew = createNew;
	}

	public String getPasswordCurrent() {
		return passwordCurrent;
	}

	public void setPasswordCurrent(String passwordCurrent) {
		this.passwordCurrent = passwordCurrent;
	}

	public String getPasswordReenter() {
		return passwordReenter;
	}

	public void setPasswordReenter(String passwordReenter) {
		this.passwordReenter = passwordReenter;
	}

	public boolean isUserEditable(ConsoleUser user) {
		return !user.getUserId().equals(ConstantUtil.SYS_DEFAULT)
				|| WebUtil.getCurrentUserId().equals(ConstantUtil.SYS_DEFAULT);
	}

	public boolean isUserDeletable(ConsoleUser user) {
		return !user.getUserId().equals(ConstantUtil.SYS_DEFAULT);
	}

	public boolean isConsoleUserEmpty() {
		return consoleUsers == null || consoleUsers.isEmpty();
	}
}
