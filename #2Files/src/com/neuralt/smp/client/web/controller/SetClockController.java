package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.component.selectbooleancheckbox.SelectBooleanCheckbox;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.AgentRuntimeException;
import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredHost.HostStatus;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.SelectedMonitoredHost;
import com.neuralt.smp.client.web.SelectedMonitoredSystem;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.CssStyleUtil;
import com.neuralt.web.util.jsf.LocaleMessages;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class SetClockController implements Serializable {
	private static final long serialVersionUID = -453928492519225646L;
	private static final Logger logger = LoggerFactory
			.getLogger(SetClockController.class);
	private List<MonitoredHost> hosts;
	private Map<MonitoredHost, String> feedbacks;
	private Map<SelectedMonitoredSystem, MonitoredSystem> sysMap;
	private Map<SelectedMonitoredHost, MonitoredHost> hostMap;
	private TreeNode entityRoot;
	private Date time;

	public SetClockController() {
		feedbacks = new HashMap<MonitoredHost, String>();
		hostMap = new HashMap<SelectedMonitoredHost, MonitoredHost>();
		hosts = new ArrayList<MonitoredHost>();
		createTreeTableModel();
	}

	private void createTreeTableModel() {
		entityRoot = new DefaultTreeNode("root", null);
		entityRoot.setExpanded(true);

		for (MonitoredSystem sys : SmpClient.instance.getSystems()) {
			SelectedMonitoredSystem selectedSys = new SelectedMonitoredSystem(
					sys);
			TreeNode sysNode = new DefaultTreeNode(selectedSys, entityRoot);
			sysNode.setExpanded(true);
			for (MonitoredHost host : sys.getHosts()) {
				SelectedMonitoredHost selectedHost = null;
				try {
					selectedHost = new SelectedMonitoredHost(host);
				} catch (MalformedURLException e) {
				}
				if (host.getConfig().isAgentDeployed()) {
					TreeNode hostNode = new DefaultTreeNode(selectedHost,
							sysNode);
					hostNode.setExpanded(true);
					hostMap.put(selectedHost, host);
					// testing purpose
					// hosts.add(host);

				}
			}
		}
	}

	public String displayStatus(MonitoredEntity entity) {
		if (entity instanceof SelectedMonitoredHost) {
			SelectedMonitoredHost selectedHost = (SelectedMonitoredHost) entity;
			return hostMap.get(selectedHost).getStatusText();
		}
		return "";
	}

	public boolean check(MonitoredEntity entity) {
		if (entity instanceof MonitoredHost) {
			MonitoredHost host = (MonitoredHost) entity;
			if (hosts.contains(host)) {
				hosts.remove(host);
			} else {
				hosts.add(host);
			}
		}

		return false;
	}

	public void setClock() {

		String timeStr = StringUtil.convertTimeFormat(time);
		logger.debug("time set:" + timeStr);
		feedbacks = new HashMap<MonitoredHost, String>();
		for (SelectedMonitoredHost host : hostMap.keySet()) {
			if (host.isSelected()) {
				if (displayStatus(host).equals(HostStatus.DISCONNECTED.name()))
					feedbacks.put(host, host.getStatusText());
				else {
					try {
						feedbacks.put(host, SmpClient.instance.setClock(host
								.getSys().getName(),
								host.getConfig().getName(), timeStr));
						AuditLogger.logAuditTrail(ScreenType.COMMAND,
								WebUtil.getRequestPath(), ActionType.COMMAND,
								"set-clock", host.getConfig().getName() + ": "
										+ timeStr);
					} catch (AgentRuntimeException ignore) {
						// should ignore
						WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
								MessageKey.GENERAL_ERROR,
								LocaleMessages.getString(
										MessageKey.CLOCK_SYNC_CONNECT_FAILED,
										host.getName()));
					}
				}
			}
		}
	}

	public String feedback(MonitoredEntity entity) {
		if (entity instanceof MonitoredSystem) {
			return "";
		} else {
			return feedbacks.get(entity);
		}
	}

	public String getStatusCssClass(String statusText) {
		String result = CssStyleUtil.getStatusCssClass(statusText);
		if (result == null)
			result = "";

		return result;
	}

	public void checkBoxListener(ValueChangeEvent event) {
		SelectBooleanCheckbox sbc = (SelectBooleanCheckbox) event
				.getComponent();
	}

	public boolean isHost(MonitoredEntity entity) {
		return (entity instanceof MonitoredHost);
	}

	public List<MonitoredHost> getHosts() {
		return hosts;
	}

	public void setHosts(List<MonitoredHost> hosts) {
		this.hosts = hosts;
	}

	public Map<MonitoredHost, String> getFeedbacks() {
		return feedbacks;
	}

	public void setFeedbacks(Map<MonitoredHost, String> feedbacks) {
		this.feedbacks = feedbacks;
	}

	public TreeNode getEntityRoot() {
		return entityRoot;
	}

	public void setEntityRoot(TreeNode entityRoot) {
		this.entityRoot = entityRoot;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

}
