package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.EventLog;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.SnmpTrap.Direction;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.web.util.CssStyleUtil;

@ManagedBean
@ViewScoped
public class HistoryController implements Serializable {

	private static final long serialVersionUID = 1149151637275084135L;
	private static final Logger logger = LoggerFactory
			.getLogger(HistoryController.class);

	private static final String BACKGROUND_WHITE = "alarm-white";

	@ManagedProperty(value = "#{entitySelectorController}")
	private EntitySelectorController entitySelector;

	private EventLogDataModel eventDataModel;
	private SnmpTrapDataModel trapDataModel;
	private int activeTab = 0;
	private int numRows = 20;

	// common search criteria
	private Date searchFrom;
	private Date searchTo;
	private MonitoredSystem selectedSystem;
	private MonitoredHost selectedHost;
	private MonitoredProcess selectedProc;
	private List<String> levelFilters;

	public HistoryController() {
		eventDataModel = new EventLogDataModel();
		trapDataModel = new SnmpTrapDataModel();
	}

	public void search() {
		eventDataModel.refreshRowCount();
		trapDataModel.refreshRowCount();
	}

	public EntitySelectorController getEntitySelector() {
		return entitySelector;
	}

	public void setEntitySelector(EntitySelectorController entitySelector) {
		this.entitySelector = entitySelector;
		this.entitySelector
				.setEntitySelectionListener(new HistoryEntitySelectionListener());
	}

	public EventLogDataModel getEventDataModel() {
		return eventDataModel;
	}

	public void setEventDataModel(EventLogDataModel eventDataModel) {
		this.eventDataModel = eventDataModel;
	}

	public SnmpTrapDataModel getTrapDataModel() {
		return trapDataModel;
	}

	public void setTrapDataModel(SnmpTrapDataModel trapDataModel) {
		this.trapDataModel = trapDataModel;
	}

	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date searchFrom) {
		this.searchFrom = searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date searchTo) {
		this.searchTo = searchTo;
	}

	public MonitoredSystem getSelectedSystem() {
		return selectedSystem;
	}

	public void setSelectedSystem(MonitoredSystem selectedSystem) {
		this.selectedSystem = selectedSystem;
	}

	public MonitoredHost getSelectedHost() {
		return selectedHost;
	}

	public void setSelectedHost(MonitoredHost selectedHost) {
		this.selectedHost = selectedHost;
	}

	public MonitoredProcess getSelectedProc() {
		return selectedProc;
	}

	public void setSelectedProc(MonitoredProcess selectedProc) {
		this.selectedProc = selectedProc;
	}

	public List<String> getLevelFilters() {
		return levelFilters;
	}

	public void setLevelFilters(List<String> levelFilters) {
		this.levelFilters = levelFilters;
	}

	public int getActiveTab() {
		return activeTab;
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public int getNumRows() {
		return numRows;
	}

	public void setNumRows(int numRows) {
		this.numRows = numRows;
	}

	public List<String> getWarningLevelList() {
		List<String> list = new ArrayList<String>();
		for (WarningLevel level : WarningLevel.values()) {
			list.add(level.name());
		}
		return list;
	}

	public String getRowStyleClass(Object entity) {
		if (entity instanceof EventLog) {
			WarningLevel level = ((EventLog) entity).getLevel();
			if (level != null)
				return CssStyleUtil.getStatusCssClass(level.name());
		} else if (entity instanceof SnmpTrap) {
			String level = ((SnmpTrap) entity).getTrapAlarmLevel();
			return CssStyleUtil.getStatusCssClass(level);
		}
		return BACKGROUND_WHITE;
	}

	public List<SelectItem> getDirectionOptions() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem("", "-Any-"));
		for (Direction dir : Direction.values()) {
			items.add(new SelectItem(dir, dir.name()));
		}
		return items;
	}

	private class HistoryEntitySelectionListener implements
			EntitySelectorController.EntitySelectionListener {

		@Override
		public void onEntitySelect(MonitoredEntity selectedEntity) {
			try {
				if (entitySelector.getSelectedProcess() != null) {
					selectedProc = entitySelector.getSelectedProcess();
					selectedHost = null;
					selectedSystem = null;
					eventDataModel.refreshRowCount();
					trapDataModel.refreshRowCount();
				} else if (entitySelector.getSelectedHost() != null) {
					selectedHost = (entitySelector.getSelectedHost());
					selectedProc = null;
					selectedSystem = null;
					eventDataModel.refreshRowCount();
					trapDataModel.refreshRowCount();
				} else if (entitySelector.getSelectedSystem() != null) {
					selectedSystem = entitySelector.getSelectedSystem();
					selectedProc = null;
					selectedHost = null;
					eventDataModel.refreshRowCount();
					trapDataModel.refreshRowCount();
				} else {
					selectedProc = null;
					selectedHost = null;
					selectedSystem = null;
				}
			} catch (Exception e) {
				logger.warn("Failed to query db:" + e);
			}
		}
	}

	private class EventLogDataModel extends LazyDataModel<EventLog> {

		private static final long serialVersionUID = 6600751823572844054L;

		private List<EventLog> pageData;

		@Override
		public EventLog getRowData(String rowKey) {
			for (EventLog evt : pageData) {
				if (evt.getId() == Long.valueOf(rowKey)) {
					return evt;
				}
			}
			return null;
		}

		@Override
		public Object getRowKey(EventLog evt) {
			return evt.getId();
		}

		@Override
		public List<EventLog> load(int first, int pageSize, String sortField,
				SortOrder sortOrder, Map<String, String> filters) {
			DetachedCriteria criteria = buildCriteria();
			for (Entry<String, String> filter : filters.entrySet()) {
				if (isEmpty(filter.getValue())) {
					continue;
				}
				if ("level".equals(filter.getKey())) {
					List<Criterion> list = new ArrayList<Criterion>();
					for (WarningLevel level : WarningLevel.values()) {
						if (level.name().toLowerCase()
								.contains(filter.getValue().toLowerCase())) {
							list.add(Property.forName("level").eq(level));
							logger.debug(
									"level {} is added in filter criterion",
									level);
						}
					}
					criteria.add(Restrictions.or(list
							.toArray(new Criterion[list.size()])));
				} else if ("source".equals(filter.getKey())) {
					logger.debug("source filtering : value={}",
							filter.getValue());
					criteria.add(Restrictions.or(
							Restrictions.like("sysName", filter.getValue(),
									MatchMode.ANYWHERE).ignoreCase(),
							Restrictions.like("hostName", filter.getValue(),
									MatchMode.ANYWHERE).ignoreCase(),
							Restrictions.like("procName", filter.getValue(),
									MatchMode.ANYWHERE).ignoreCase()));
				} else {
					criteria.add(Restrictions.like(filter.getKey(),
							filter.getValue(), MatchMode.ANYWHERE).ignoreCase());
				}
			}

			if (isEmpty(sortField))
				sortField = "eventTime";
			switch (sortOrder) {
			case ASCENDING:
				criteria.addOrder(Order.asc(sortField));
				break;
			case DESCENDING:
				criteria.addOrder(Order.desc(sortField));
				break;
			default:
				break;
			}

			pageData = DataAccess.eventLogDao.findByCriteria(criteria, first,
					pageSize);
			if (pageData != null && pageData.size() > this.getRowCount()) {
				refreshRowCount();
			}
			return pageData;
		}

		private DetachedCriteria buildCriteria() {
			DetachedCriteria crit = DetachedCriteria.forClass(EventLog.class);
			if (searchFrom != null)
				crit.add(Property.forName("eventTime").ge(searchFrom));
			if (searchTo != null)
				crit.add(Property.forName("eventTime").le(searchTo));
			if (selectedSystem != null)
				crit.add(Property.forName("sysName").eq(
						selectedSystem.getName()));
			if (selectedHost != null)
				crit.add(Property.forName("hostName").eq(
						selectedHost.getConfig().getName()));
			if (selectedProc != null)
				crit.add(Property.forName("procName")
						.eq(selectedProc.getName()));
			if (levelFilters != null && !levelFilters.isEmpty()) {
				Set<WarningLevel> levels = new HashSet<WarningLevel>();
				for (String levelName : levelFilters) {
					levels.add(WarningLevel.valueOf(levelName));
				}
				crit.add(Restrictions.in("level", levels));
			}
			return crit;
		}

		public long refreshRowCount() {
			DetachedCriteria criteria = buildCriteria();
			long count = DataAccess.eventLogDao.count(criteria);
			this.setRowCount((int) count);
			return count;
		}

		@Override
		public void setRowIndex(int rowIndex) {
			if (getPageSize() == 0) {
				rowIndex = -1;
			}
			super.setRowIndex(rowIndex);
		}
	}

	private class SnmpTrapDataModel extends LazyDataModel<SnmpTrap> {

		private static final long serialVersionUID = 2837677509404660924L;

		private List<SnmpTrap> pageData;

		@Override
		public SnmpTrap getRowData(String rowKey) {
			for (SnmpTrap trap : pageData) {
				if (trap.getId() == Long.valueOf(rowKey)) {
					return trap;
				}
			}
			return null;
		}

		@Override
		public Object getRowKey(SnmpTrap trap) {
			return trap.getId();
		}

		@Override
		public List<SnmpTrap> load(int first, int pageSize, String sortField,
				SortOrder sortOrder, Map<String, String> filters) {
			DetachedCriteria criteria = buildCriteria();
			for (Entry<String, String> filter : filters.entrySet()) {
				if (isEmpty(filter.getValue())) {
					continue;
				}
				if ("source".equals(filter.getKey())) {
					criteria.add(Restrictions
							.disjunction()
							.add(Restrictions.like("source", filter.getValue(),
									MatchMode.ANYWHERE).ignoreCase())
							.add(Restrictions.like("sourceIp",
									filter.getValue(), MatchMode.ANYWHERE)
									.ignoreCase()));
				} else if ("direction".equals(filter.getKey())) {
					try {
						Direction dir = Direction.valueOf(filter.getValue());
						criteria.add(Property.forName("direction").eq(dir));
					} catch (RuntimeException e) {
						// value doesn't match to an enum. just safely ignore.
					}
				} else {
					criteria.add(Restrictions.like(filter.getKey(),
							filter.getValue(), MatchMode.ANYWHERE).ignoreCase());
				}
			}

			if (isEmpty(sortField))
				sortField = "time";
			switch (sortOrder) {
			case ASCENDING:
				criteria.addOrder(Order.asc(sortField));
				break;
			case DESCENDING:
				criteria.addOrder(Order.desc(sortField));
				break;
			default:
				break;
			}

			pageData = DataAccess.snmpTrapDao.findByCriteria(criteria, first,
					pageSize);
			if (pageData != null && pageData.size() > this.getRowCount()) {
				refreshRowCount();
			}
			return pageData;
		}

		private DetachedCriteria buildCriteria() {
			DetachedCriteria crit = DetachedCriteria.forClass(SnmpTrap.class);
			if (searchFrom != null)
				crit.add(Property.forName("time").ge(searchFrom));
			if (searchTo != null)
				crit.add(Property.forName("time").le(searchTo));

			if (selectedHost != null) {
				crit.add(Restrictions
						.disjunction()
						.add(Property.forName("source").like(
								selectedHost.getConfig().getName(),
								MatchMode.ANYWHERE))
						.add(Property.forName("sourceIp").eq(
								selectedHost.getIpAddr())));
			} else if (selectedSystem != null) {
				crit.add(Property.forName("source").like(
						selectedSystem.getName(), MatchMode.ANYWHERE));
			}
			if (levelFilters != null && !levelFilters.isEmpty()) {
				crit.add(Restrictions.in("trapAlarmLevel", levelFilters));
			}
			return crit;
		}

		public long refreshRowCount() {
			DetachedCriteria criteria = buildCriteria();
			long count = DataAccess.snmpTrapDao.count(criteria);
			this.setRowCount((int) count);
			return count;
		}

		@Override
		public void setRowIndex(int rowIndex) {
			if (getPageSize() == 0) {
				rowIndex = -1;
			}
			super.setRowIndex(rowIndex);
		}
	}

	public boolean isEmpty(String sortField) {
		return sortField == null || "".equals(sortField);
	}
}
