package com.neuralt.smp.client.web.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.chart.line.LineChart;
import org.primefaces.component.panelgrid.PanelGrid;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;

@ManagedBean
@ViewScoped
public class TestController implements Serializable {
	private static final long serialVersionUID = -6610680221679487465L;
	private CartesianChartModel linearModel;

	private String script;
	private String value;
	private PanelGrid pg;

	public TestController() {
		createLinearModel();
		generatePg();
	}

	private void generatePg() {
		pg = new PanelGrid();
		pg.setColumns(1);
		LineChart chart = new LineChart();

		chart.setRendered(true);
		chart.setShowMarkers(false);

		CartesianChartModel model = new CartesianChartModel();

		// Map<Object, Number> data = new HashMap<Object, Number>();
		long now = System.currentTimeMillis();

		LineChartSeries s2 = new LineChartSeries();
		s2.setLabel("dummy series2");
		s2.set(now, 123);
		s2.set(now - 10000, 456);
		s2.set(now - 100000, 789);
		s2.set(now - 2000000, 321);
		// model.addSeries(s2);
		chart.setValue(model);
		chart.setExtender("ext_test");

		pg.getChildren().add(chart);
	}

	private void createLinearModel() {
		linearModel = new CartesianChartModel();

		LineChartSeries series1 = new LineChartSeries();
		series1.setLabel("Series 1");

		series1.set(1, 2);
		series1.set(2, 1);
		series1.set(3, 3);
		series1.set(4, 6);
		series1.set(5, 8);

		LineChartSeries series2 = new LineChartSeries();
		series2.setLabel("Series 2");
		series2.setMarkerStyle("diamond");

		series2.set(1, 6);
		series2.set(2, 3);
		series2.set(3, 2);
		series2.set(4, 7);
		series2.set(5, 9);

		// linearModel.addSeries(series1);
		// linearModel.addSeries(series2);
	}

	public void execute() {
		// save script to file

		// execute file with sudo

	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script;
	}

	public CartesianChartModel getLinearModel() {
		return linearModel;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public PanelGrid getPg() {
		return pg;
	}

	public String getSysInfo() {
		StringBuilder sb = new StringBuilder();
		sb.append("Name=").append(System.getProperty("os.name"))
				.append(", Arch=").append(System.getProperty("os.arch"))
				.append(", Version=").append(System.getProperty("os.version"));
		return sb.toString();
	}
}
