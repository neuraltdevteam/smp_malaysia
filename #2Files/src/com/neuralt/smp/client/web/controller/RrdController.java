package com.neuralt.smp.client.web.controller;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ValueChangeEvent;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataCollectionProcedure;
import com.neuralt.smp.client.data.DataCollectionProcedure.MethodType;
import com.neuralt.smp.client.data.RrdArchiveConfig;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.client.data.ScriptConfig;
import com.neuralt.smp.client.data.SnmpTarget;
import com.neuralt.smp.client.task.FileDeleteTask;
import com.neuralt.smp.client.util.ChartTemplateUtil;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostSnmpConfig;
import com.neuralt.smp.rrd.RrdService;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class RrdController implements Serializable {
	private static final long serialVersionUID = -7673455723136706062L;
	private static final Logger logger = LoggerFactory
			.getLogger(RrdController.class);

	private RrdDefConfig rrdDef;
	private RrdDefConfig dummyRrdDef;
	private RrdDsConfig rrdDs;
	private RrdDsConfig dummyRrdDs;
	private RrdArchiveConfig rrdArchive;
	private RrdArchiveConfig dummyRrdArchive;

	private RrdDefConfig selectedRrdDef;
	private MethodType selectedType;
	private List<String> scriptOptions;
	private Map<String, ScriptConfig> scriptMap;
	private List<String> snmpOptions;
	private Map<String, SnmpTarget> snmpMap;
	private List<RrdDefConfig> rrdList;
	private String selectedOption;
	private List<String> hostOptions;
	private Map<String, HostConfig> hostMap;
	private String selectedHost;

	private TreeNode defRoot;
	private TreeNode selectedNode;

	private boolean simpleMode;
	private boolean deleteFile;
	private boolean displayLockPrompt;
	private boolean needBackup;

	public RrdController() {
		simpleMode = AppConfig.isRrdConfigSimple();
		createRrdDefTreeTable();
		rrdList = new ArrayList<RrdDefConfig>();
		scriptOptions = new ArrayList<String>();
		snmpOptions = new ArrayList<String>();
		hostOptions = new ArrayList<String>();
		scriptMap = new TreeMap<String, ScriptConfig>();
		snmpMap = new TreeMap<String, SnmpTarget>();
		hostMap = new TreeMap<String, HostConfig>();
		reset();
	}

	private void createRrdDefTreeTable() {
		defRoot = new DefaultTreeNode("root", null);
		boolean found = false;
		for (RrdDefConfig def : DataAccess.rrdDefConfigDao.getAllRrdDefs()) {
			TreeNode defNode = new DefaultTreeNode(def, defRoot);
			defNode.setExpanded(true);
			if (def.equals(selectedRrdDef)) {
				selectedRrdDef = def;
				found = true;
			}
		}
		if (!found) {
			selectedRrdDef = null;
		}
	}

	private void reset() {
		rrdDef = new RrdDefConfig();
		rrdDs = new RrdDsConfig();
		rrdArchive = new RrdArchiveConfig();
		dummyRrdDef = new RrdDefConfig();
		dummyRrdDs = new RrdDsConfig();
		dummyRrdArchive = new RrdArchiveConfig();
		deleteFile = false;
		selectedType = MethodType.SNMP;
		displayLockPrompt = false;
		needBackup = false;
		selectedOption = null;
	}

	public void onNodeSelected(NodeSelectEvent event) {
		if (event.getTreeNode().getData() instanceof RrdDefConfig) {
			selectedRrdDef = (RrdDefConfig) event.getTreeNode().getData();
			createHostList();
		}
	}

	public void onDefLockChange(ValueChangeEvent event) {
		if (logger.isDebugEnabled())
			logger.debug("event detected:" + "old value="
					+ (Boolean) event.getOldValue() + ", new value="
					+ (Boolean) event.getNewValue());
		if ((rrdDef.isLocked()) && (!(Boolean) event.getNewValue())) {
			displayLockPrompt = true;
		} else {
			displayLockPrompt = false;
		}
	}

	private void createHostList() {
		hostOptions.clear();
		hostMap.clear();
		for (HostConfig host : DataAccess.hostConfigDao.getAllEnabledHost()) {
			String displayName = host.getSystemConfig().getName() + " - "
					+ host.getName();
			hostOptions.add(displayName);
			hostMap.put(displayName, host);
		}
	}

	private void createOptionLists() {
		scriptOptions.clear();
		snmpOptions.clear();
		scriptMap.clear();
		snmpMap.clear();
		selectedOption = null;
		if (!StringUtil.isNullOrEmpty(selectedHost)) {
			HostConfig host = hostMap.get(selectedHost);
			for (HostSnmpConfig snmpConfig : host.getSnmpConfigs()) {
				List<SnmpTarget> targetList = DataAccess.snmpTargetDao
						.getSnmpTargetByHostSnmpConfig(snmpConfig);
				for (SnmpTarget target : targetList) {
					String key = snmpConfig.getName() + " - "
							+ target.getName();
					snmpOptions.add(key);
					snmpMap.put(key, target);
				}
			}
			List<ScriptConfig> scripts = DataAccess.scriptConfigDao
					.getScriptsByHost(host);
			for (ScriptConfig script : scripts) {
				String key = script.getName();
				scriptOptions.add(key);
				scriptMap.put(key, script);
			}
		}
	}

	public void addRrdDef() {
		// delay the path creation to be after confirmed the rrdDef is locked
		DataAccess.rrdDefConfigDao.save(rrdDef);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
				"add-data definition", "def=" + rrdDef.toString());
		reset();
		createRrdDefTreeTable();
		WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
				MessageKey.GENERAL_SUCCESS, MessageKey.RRD_DEF_ADD_SUCC);

	}

	public void deleteRrdDef() {
		if (selectedRrdDef.isLocked()) {
			RrdService.getInstance().stopDataCollection(selectedRrdDef);
			logger.debug("deleteFile=" + deleteFile);
			if (deleteFile) {
				FileDeleteTask task = new FileDeleteTask(
						selectedRrdDef.getPath());
				task.start();
			}
		}
		for (RrdDsConfig ds : selectedRrdDef.getRrdDsConfigs()) {
			ChartTemplateUtil.deleteDs(ds);
		}
		for (RrdArchiveConfig archive : selectedRrdDef.getRrdArchiveConfigs()) {
			ChartTemplateUtil.deleteArchive(archive);
		}
		DataAccess.rrdDefConfigDao.delete(selectedRrdDef);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
				"delete-data definition", "def=" + selectedRrdDef.toString());
		createRrdDefTreeTable();
		reset();
		WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
				MessageKey.GENERAL_SUCCESS, MessageKey.RRD_DEF_DELETE_SUCC);
		// selectedRrdDef = null;
	}

	public void editRrdDef(boolean backup) {
		logger.debug("rrdDef is being edited: ds size="
				+ rrdDef.getRrdDsConfigs().size() + ", archive size="
				+ rrdDef.getRrdArchiveConfigs().size());
		boolean ok = true;
		if (dummyRrdDef.isLocked()) {
			// set to be fixed, start checking if everything is ok
			if (rrdDef.getRrdArchiveConfigs().size() == 0
					|| rrdDef.getRrdDsConfigs().size() == 0) {
				WebUtil.addMessage(MessageKey.GENERAL_ERROR,
						MessageKey.RRD_CREATION_FAIL_NO_PARAM,
						FacesMessage.SEVERITY_ERROR);
				return;
			}
			for (RrdDsConfig ds : rrdDef.getRrdDsConfigsAsList()) {
				DataCollectionProcedure dummy = ds.getDataCollectProc();
				if (dummy == null) {
					// there is DS not assigned with a data collection
					// procedure, rrdDef is hence invalid
					ok = false;
					WebUtil.addMessage(MessageKey.GENERAL_ERROR,
							MessageKey.RRD_DS_NO_PROCEDURE,
							FacesMessage.SEVERITY_ERROR);
					return;
				}
			}
		}
		for (RrdDsConfig ds : rrdDef.getRrdDsConfigsAsList()) {
			if (dummyRrdDef.getStep() >= ds.getHeartbeat()) {
				ok = false;
				WebUtil.addMessage(MessageKey.GENERAL_ERROR,
						MessageKey.RRD_DEF_STEP_MAX_PROMPT,
						FacesMessage.SEVERITY_ERROR);
				return;
			}
		}
		if (ok) {
			rrdDef.update(dummyRrdDef);
			if (dummyRrdDef.isLocked()) {
				if (!rrdDef.isLocked()) {
					// this is a lock edit - need to create rrd db
					try {
						StringBuilder sb = new StringBuilder();
						sb.append(AppConfig.getRrdFileStorageDir())
								.append(rrdDef.getName())
								.append("_")
								.append(StringUtil
										.convertTimeFormatForExport(new Date()))
								.append(".rrd");
						rrdDef.setPath(sb.toString());
						RrdService.getDefaultRrdAccess().create(rrdDef);
						rrdDef.setLocked(true);
						rrdDef.setCreateDate(new Timestamp(System
								.currentTimeMillis()));
					} catch (Exception e) {
						logger.error("creating RRD file failed: {}", e);
						WebUtil.addMessage(
								MessageKey.GENERAL_ERROR,
								WebUtil.getMessage(MessageKey.RRD_CREATION_FAILED)
										+ " " + e.getMessage(),
								FacesMessage.SEVERITY_WARN);
						// if cannot create DB then do not set lock, but still
						// update other params
					}
				}
				// it is reasonable to change the enabled status only when it is
				// locked
				rrdDef.setEnabled(dummyRrdDef.isEnabled());
			} else {
				rrdDef.setEnabled(false);
				if (dummyRrdDef.isEnabled()) {
					WebUtil.addMessage(FacesMessage.SEVERITY_WARN,
							MessageKey.GENERAL_ERROR,
							MessageKey.RRD_DEF_FORCED_DISABLED);
				}
				if (rrdDef.isLocked()) {
					// this is an unlock edit - need to backup and stop the
					// running task
					rrdDef.setLocked(false);
					RrdService.getInstance().stopDataCollection(rrdDef);
					if (backup) {
						backupRrdDef(rrdDef);
					}
					FileDeleteTask task = new FileDeleteTask(rrdDef.getPath());
					task.start();
					rrdDef.setPath(null);
				}
			}

			if (rrdDef.isEnabled()) {
				RrdService.getInstance().startDataCollection(rrdDef);
			} else {
				RrdService.getInstance().stopDataCollection(rrdDef);
			}
			// TODO
			// as the stop call is async, may need to wait for the success sign

			DataAccess.rrdDefConfigDao.save(rrdDef);
			if (logger.isDebugEnabled())
				logger.debug("saving rrdDef:" + rrdDef.toString());
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-data definition", "def=" + rrdDef.toString());
			createRrdDefTreeTable();
			reset();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, MessageKey.RRD_DEF_EDIT_SUCC);

		}
		rrdDef = new RrdDefConfig();
	}

	public void addRrdDs() {

		if (logger.isDebugEnabled()) {
			logger.debug("adding ds for " + selectedRrdDef.getName());
			logger.debug("max=" + rrdDs.getMaximum() + ", min="
					+ rrdDs.getMinimum());
		}
		boolean ok = true;

		if (StringUtil.isNullOrEmpty(selectedOption)) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.RRD_DS_NO_PROCEDURE);
			return;
		}
		if (selectedRrdDef.getDs(rrdDs.getName()) != null) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.DUPLICATED_NAME);
			return;
		}
		if (rrdDs.getHeartbeat() <= selectedRrdDef.getStep()) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.RRD_DS_HEARTBEAT_PROMPT);
			return;
		}

		DataCollectionProcedure procedure = new DataCollectionProcedure();
		ok = isProcedureValid(ok, procedure);

		if (ok) {
			selectedRrdDef.getRrdDsConfigs().add(rrdDs);
			rrdDs.setRrdDef(selectedRrdDef);
			procedure.setDs(rrdDs);
			procedure.setRrdDef(rrdDs.getRrdDef());
			rrdDs.getRrdDef().getProcedures().add(procedure);
			DataAccess.rrdDefConfigDao.save(selectedRrdDef);
		}

		reset();
	}

	public void deleteRrdDs(RrdDsConfig ds) {

		logger.debug("start to delete DS...");
		DataCollectionProcedure procedure = ds.getDataCollectProc();
		RrdDefConfig tempDef = selectedRrdDef;
		if (procedure != null) {
			logger.debug("Procedure is found");
			DataAccess.dataCollectionProcedureDao.delete(procedure);
			tempDef.getProcedures().remove(procedure);
			// DataAccess.rrdDefConfigDao.save(tempDef);
			logger.debug("Procedure is deleted");
		}
		// TODO
		// if do not fetch the fresh copy, below removing ds will fail - under
		// review
		tempDef = DataAccess.rrdDefConfigDao.getFreshCopy(tempDef);
		if (logger.isDebugEnabled()) {
			logger.debug("fresh tempDef=" + tempDef);
			for (RrdDsConfig tempDs : tempDef.getRrdDsConfigs()) {
				logger.debug("comparing..." + ds.compareTo(tempDs));
				logger.debug("inverse comparing..." + tempDs.compareTo(ds));
			}
			logger.debug("ds=" + ds.toString());
			// TODO
			// the following message will be false
			// need to find out why the contains method is not working
			// should double check on RrdDsConfig equals and compareTo methods
			logger.debug("tempDef contains ds:"
					+ tempDef.getRrdDsConfigs().contains(ds));
			logger.debug("tempDef contains ds:" + tempDef.containDs(ds));
			if (tempDef.removeDs(ds)) {
				logger.debug("ds is removed from the set");
			} else {
				logger.debug("ds is not removed from the set!");
			}
			logger.debug("removing DS " + ds.toString() + " from parent def:"
					+ tempDef.toString());
		}
		// TODO
		// temparary workaround - remove ds from template
		ChartTemplateUtil.deleteDs(ds);

		boolean ok = false;
		for (int i = 1; i <= 10; i++) {
			try {
				DataAccess.rrdDefConfigDao.save(tempDef);
				ok = true;
				break;
			} catch (Exception e) {
				logger.debug("Removing DS failed in trail " + i);
			}
		}
		if (!ok) {
			logger.warn("DS is not removed!");
		} else {
			logger.debug("DS is removed");
			selectedRrdDef = tempDef;
		}
	}

	public void editRrdDs() {

		logger.debug("adding ds for " + selectedRrdDef.getName());
		logger.debug("max=" + dummyRrdDs.getMaximum() + ", min="
				+ dummyRrdDs.getMinimum());

		boolean ok = true;
		RrdDsConfig temp = rrdDs.getRrdDef().getDs(dummyRrdDs.getName());
		if (temp != null && !temp.equals(rrdDs)) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.DUPLICATED_NAME);
		} else {
			DataCollectionProcedure procedure = rrdDs.getDataCollectProc();

			logger.debug("after finding procedure");
			if (procedure == null) {
				logger.debug("cannot find an associated procedure");
				procedure = new DataCollectionProcedure();
				procedure.setDs(rrdDs);
				procedure.setRrdDef(rrdDs.getRrdDef());
				rrdDs.getRrdDef().getProcedures().add(procedure);
			}

			// testing purpose
			// procedure.setRrdDef(rrdDs.getRrdDef());
			// rrdDs.getRrdDef().getProcedures().add(procedure);

			ok = isProcedureValid(ok, procedure);
		}
		if (dummyRrdDs.getHeartbeat() <= rrdDs.getRrdDef().getStep()) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.RRD_DS_HEARTBEAT_PROMPT);
		}

		if (ok) {
			rrdDs.update(dummyRrdDs);
			logger.debug("last check");
			logger.debug("max=" + rrdDs.getMaximum() + ", min="
					+ rrdDs.getMinimum());

			DataAccess.rrdDefConfigDao.save(rrdDs.getRrdDef());
			reset();
		}
		rrdDs = new RrdDsConfig();
	}

	private void backupRrdDef(RrdDefConfig def) {
		logger.debug("backup up rrd def:" + def.toString());
		try {
			RrdService.getDefaultRrdAccess().backup(def);
		} catch (IOException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.FILE_ERROR);
			logger.error("failed in backup RRD DB", e);
		}
	}

	private boolean isProcedureValid(boolean ok,
			DataCollectionProcedure procedure) {
		if (!StringUtil.isNullOrEmpty(selectedOption)) {
			procedure.setType(selectedType);
			if (selectedType.equals(MethodType.SCRIPT)) {
				ScriptConfig script = scriptMap.get(selectedOption);
				logger.debug("after finding script");
				if (script != null) {
					logger.debug("script is found");
					procedure.setScript(script);
					DataAccess.dataCollectionProcedureDao.save(procedure);
				} else {
					ok = false;
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.DB_ACCESS_ERROR);
				}
			} else if (selectedType.equals(MethodType.SNMP)) {
				SnmpTarget target = snmpMap.get(selectedOption);
				logger.debug("after finding target");
				if (target != null) {
					logger.debug("target is found");
					procedure.setSnmp(target);
					DataAccess.dataCollectionProcedureDao.save(procedure);
				} else {
					ok = false;
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.DB_ACCESS_ERROR);
				}
			}
		} else {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.RRD_DS_NO_PROCEDURE);
		}
		return ok;
	}

	public void addRrdArchive() {
		boolean ok = true;
		logger.debug("rrdArchive.name=" + rrdArchive.getName());
		logger.debug("selectedDef=" + selectedRrdDef);
		RrdArchiveConfig temp = selectedRrdDef.getArchive(rrdArchive.getName());
		if (temp != null) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.DUPLICATED_NAME);
		} else {
			rrdArchive = setupArchiveTimeRange(rrdArchive, selectedRrdDef);
			ok = isArchiveStepRowValid(rrdArchive, ok);
			if (ok) {
				temp = selectedRrdDef.getArchive(rrdArchive.getType(),
						rrdArchive.getStep());
				if (temp != null) {
					ok = false;
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.RRD_ARCHIVE_SETTING_DUPLICATED);
				}
			}
		}
		// if (ok)
		// ok = isArchiveStepRowValid(rrdArchive, ok);

		if (ok) {
			selectedRrdDef.getRrdArchiveConfigs().add(rrdArchive);
			rrdArchive.setRrdDef(selectedRrdDef);
			DataAccess.rrdDefConfigDao.save(selectedRrdDef);
			reset();
		}
	}

	private boolean isArchiveStepRowValid(RrdArchiveConfig archive, boolean ok) {
		if (archive.getStep() < 1) {
			ok = false;
			if (simpleMode)
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						MessageKey.RRD_ARCHIVE_HIGH_FREQUENCY_PROMPT);
			else
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						MessageKey.RRD_ARCHIVE_MIN_STEP_PROMPT);
		}
		if (archive.getRow() < 2) {
			ok = false;
			if (simpleMode)
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						MessageKey.RRD_ARCHIVE_SHORT_LENGTH_PROMPT);
			else
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						MessageKey.RRD_ARCHIVE_MIN_ROW_PROMPT);
		}
		return ok;
	}

	private RrdArchiveConfig setupArchiveTimeRange(RrdArchiveConfig archive,
			RrdDefConfig def) {
		if (simpleMode) {
			archive.setStep(RrdArchiveConfig.translateTime(
					archive.getFrequency(), archive.getFrequencyType())
					/ def.getStep());
			if (archive.getStep() > 0)
				archive.setRow(RrdArchiveConfig.translateTime(
						archive.getLength(), archive.getLengthType())
						/ archive.getStep() / def.getStep());
		}
		return archive;
	}

	public void editRrdArchive() {
		boolean ok = true;

		RrdArchiveConfig temp = rrdArchive.getRrdDef().getArchive(
				dummyRrdArchive.getName());
		if (temp != null && !temp.equals(rrdArchive)) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.DUPLICATED_NAME);
		} else {
			dummyRrdArchive = setupArchiveTimeRange(dummyRrdArchive,
					rrdArchive.getRrdDef());
			ok = isArchiveStepRowValid(dummyRrdArchive, ok);
			if (ok) {
				temp = rrdArchive.getRrdDef().getArchive(
						dummyRrdArchive.getType(), dummyRrdArchive.getStep());
				if (temp != null && !temp.equals(rrdArchive)) {
					ok = false;
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.RRD_ARCHIVE_SETTING_DUPLICATED);
				}
			}
		}

		if (ok) {
			rrdArchive.update(dummyRrdArchive);
			DataAccess.rrdDefConfigDao.save(rrdArchive.getRrdDef());
			reset();
		}
		rrdArchive = new RrdArchiveConfig();
	}

	public void deleteRrdArchive(RrdArchiveConfig archive) {
		// The method is totally copying the working logic of deleteRrdDs
		// When update the logic of deleteRrdDs, please also update this method
		
		logger.debug("start to delete Archive...");
		RrdDefConfig tempDef = selectedRrdDef;
		// TODO
		// if do not fetch the fresh copy, below removing ds will fail - under
		// review
		tempDef = DataAccess.rrdDefConfigDao.getFreshCopy(tempDef);
		if (logger.isDebugEnabled()) {
			logger.debug("fresh tempDef=" + tempDef);
			for (RrdArchiveConfig tempArv : tempDef.getRrdArchiveConfigs()) {
				logger.debug("comparing..." + archive.compareTo(tempArv));
				logger.debug("inverse comparing..." + tempArv.compareTo(archive));
			}
			logger.debug("ds=" + archive.toString());
			// TODO
			// the following message will be false
			// need to find out why the contains method is not working
			// should double check on RrdDsConfig equals and compareTo methods
			logger.debug("tempDef contains archive:"
					+ tempDef.getRrdArchiveConfigs().contains(archive));
			logger.debug("tempDef contains archive:" + tempDef.containArchive(archive));
		}
		
		if (tempDef.removeArchive(archive)) {
			if (logger.isDebugEnabled()) {
				logger.debug("archive is removed from the set");
			}
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("archive is not removed from the set!");
			}
		}
		if (logger.isDebugEnabled()) {
			logger.debug("removing Archive " + archive.toString() + " from parent def:"
					+ tempDef.toString());	
		}
		
		// TODO
		// Temporary workaround - remove archive from template
		ChartTemplateUtil.deleteArchive(archive);

		boolean ok = false;
		for (int i = 1; i <= 10; i++) {
			try {
				DataAccess.rrdDefConfigDao.save(tempDef);
				ok = true;
				break;
			} catch (Exception e) {
				if (logger.isDebugEnabled()) {
					logger.debug("Removing DS failed in trail " + i);
				}
			}
		}
		if (!ok) {
			logger.warn("Archive is not removed!");
		} else {
			if (logger.isDebugEnabled()) {
				logger.debug("Archive is removed");
			}
			selectedRrdDef = tempDef;
		}
		
		reset();
		createRrdDefTreeTable();
	}

	public void resetRrdDef() {
		rrdDef = new RrdDefConfig();
	}

	public void resetRrdDs(RrdDefConfig def) {
		setSelectedRrdDef(def);
		rrdDs = new RrdDsConfig();
	}

	public void resetRrdArchive(RrdDefConfig def) {
		setSelectedRrdDef(def);
		rrdArchive = new RrdArchiveConfig();
	}

	public void generateDefaultArchives() {
		for (RrdArchiveConfig archive : RrdDefConfig.generateDefaultArchives()) {
			rrdArchive = archive;
			addRrdArchive();
		}
	}

	// getter/setter

	public RrdDefConfig getRrdDef() {
		return rrdDef;
	}

	public void setRrdDef(RrdDefConfig rrdDef) {
		this.rrdDef = rrdDef;
	}

	public RrdDefConfig getDummyRrdDef() {
		return dummyRrdDef;
	}

	public void setDummyRrdDef(RrdDefConfig dummyRrdDef) {
		this.dummyRrdDef = dummyRrdDef.getBackup();
		rrdDef = dummyRrdDef;
		displayLockPrompt = false;
		logger.debug("rrdDef is set for edit: ds size="
				+ rrdDef.getRrdDsConfigs().size() + ", archive size="
				+ rrdDef.getRrdArchiveConfigs().size());
	}

	public RrdDsConfig getRrdDs() {
		return rrdDs;
	}

	public void setRrdDs(RrdDsConfig rrdDs) {
		this.rrdDs = rrdDs;
	}

	public RrdDsConfig getDummyRrdDs() {
		return dummyRrdDs;
	}

	public void setDummyRrdDs(RrdDsConfig dummyRrdDs) {
		this.dummyRrdDs = dummyRrdDs.getBackup();
		rrdDs = dummyRrdDs;
		DataCollectionProcedure procedure = rrdDs.getDataCollectProc();
		if (procedure == null || procedure.getType() == null) {
			selectedType = MethodType.SNMP;
		} else {
			selectedType = procedure.getType();
			if (selectedType.equals(MethodType.SNMP))
				selectedOption = procedure.getSnmp().getName();
			else if (selectedType.equals(MethodType.SCRIPT))
				selectedOption = procedure.getScript().getName();
		}
	}

	public RrdArchiveConfig getRrdArchive() {
		return rrdArchive;
	}

	public void setRrdArchive(RrdArchiveConfig rrdArchive) {
		this.rrdArchive = rrdArchive;
	}

	public RrdArchiveConfig getDummyRrdArchive() {
		return dummyRrdArchive;
	}

	public void setDummyRrdArchive(RrdArchiveConfig dummyRrdArchive) {
		this.dummyRrdArchive = dummyRrdArchive.getBackup();
		rrdArchive = dummyRrdArchive;
	}

	public List<RrdDefConfig> getRrdList() {
		return rrdList;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public TreeNode getDefRoot() {
		return defRoot;
	}

	public RrdDefConfig getSelectedRrdDef() {
		return selectedRrdDef;
	}

	public void setSelectedRrdDef(RrdDefConfig selectedRrdDef) {
		this.selectedRrdDef = selectedRrdDef;
	}

	public MethodType getSelectedType() {
		return selectedType;
	}

	public void setSelectedType(MethodType selectedType) {
		this.selectedType = selectedType;
	}

	public synchronized List<String> getScriptOptions() {
		return scriptOptions;
	}

	public synchronized List<String> getSnmpOptions() {
		return snmpOptions;
	}

	public String getSelectedOption() {
		return selectedOption;
	}

	public void setSelectedOption(String selectedOption) {
		this.selectedOption = selectedOption;
	}

	public boolean isDeleteFile() {
		return deleteFile;
	}

	public void setDeleteFile(boolean deleteFile) {
		this.deleteFile = deleteFile;
	}

	public boolean isSimpleMode() {
		return simpleMode;
	}

	public boolean isNeedBackup() {
		return needBackup;
	}

	public void setNeedBackup(boolean needBackup) {
		this.needBackup = needBackup;
	}

	public boolean isDisplayLockPrompt() {
		return displayLockPrompt;
	}

	public void setSimpleMode(boolean simpleMode) {
		this.simpleMode = simpleMode;
		reset();
	}

	public String getDataCollectionProcedure(RrdDsConfig ds) {
		return ds.getDisplayableDataCollectProc();
	}

	public String getSelectedHost() {
		return selectedHost;
	}

	public void setSelectedHost(String selectedHost) {
		this.selectedHost = selectedHost;
		createOptionLists();

	}

	public synchronized List<String> getHostOptions() {
		return hostOptions;
	}

	public boolean isDdfEmpty() {
		return defRoot == null || defRoot.isLeaf();
	}
}
