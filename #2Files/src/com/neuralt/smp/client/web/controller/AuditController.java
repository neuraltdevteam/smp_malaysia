package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.neuralt.smp.client.data.dao.GenericDAO;
import com.neuralt.smp.client.data.dao.RawDAO;
import com.neuralt.smp.client.web.security.AuditTrailLog;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;

@ManagedBean
@ViewScoped
public class AuditController implements Serializable {

	private static final long serialVersionUID = 8604301842618866191L;

	private static SelectItem[] screenTypes = genScreenTypeItems();
	private static SelectItem[] actionTypes = genActionTypeItems();

	private static SelectItem[] genActionTypeItems() {
		ActionType[] values = ActionType.values();
		SelectItem[] items = new SelectItem[values.length + 1];
		items[0] = new SelectItem("", "--Any--");
		int i = 1;
		for (ActionType value : values) {
			items[i] = new SelectItem(value.name(), value.name());
			i++;
		}
		return items;
	}

	private static SelectItem[] genScreenTypeItems() {
		ScreenType[] values = ScreenType.values();
		SelectItem[] items = new SelectItem[values.length + 1];
		items[0] = new SelectItem("", "--Any--");
		int i = 1;
		for (ScreenType value : values) {
			items[i] = new SelectItem(value.name(), value.name());
			i++;
		}
		return items;
	}

	private Date searchFrom;
	private Date searchTo;

	private LazyDataModel<AuditTrailLog> auditTrails;
	private boolean resultsVisible = false;

	public void searchAuditTrails() {
		auditTrails = new AuditTrailModel();
		auditTrails.setRowCount((int) new RawDAO(false).count(buildCriteria()));
		resultsVisible = true;
	}

	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date searchFrom) {
		this.searchFrom = searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date searchTo) {
		this.searchTo = searchTo;
	}

	public LazyDataModel<AuditTrailLog> getAuditTrails() {
		return auditTrails;
	}

	public boolean isResultsVisible() {
		return resultsVisible;
	}

	public SelectItem[] getScreenTypes() {
		return screenTypes;
	}

	public SelectItem[] getActionTypes() {
		return actionTypes;
	}

	private DetachedCriteria buildCriteria() {
		DetachedCriteria crit = DetachedCriteria.forClass(AuditTrailLog.class);
		if (searchFrom != null)
			crit.add(Property.forName("actionTime").ge(searchFrom));
		if (searchTo != null)
			crit.add(Property.forName("actionTime").le(searchTo));
		return crit;
	}

	private class AuditTrailModel extends LazyDataModel<AuditTrailLog> {

		private static final long serialVersionUID = 1L;
		private GenericDAO<AuditTrailLog, Integer> dao = new GenericDAO<AuditTrailLog, Integer>(
				AuditTrailLog.class, false);

		@Override
		public List<AuditTrailLog> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			DetachedCriteria criteria = buildCriteria();
			if (sortField != null) {
				criteria.addOrder(sortOrder == SortOrder.ASCENDING ? Order
						.asc(sortField) : Order.desc(sortField));
			}
			if (filters != null && !filters.isEmpty()) {
				for (Map.Entry<String, String> entry : filters.entrySet()) {
					criteria.add(Property.forName(entry.getKey())
							.like(entry.getValue(), MatchMode.ANYWHERE)
							.ignoreCase());
					System.out.println(entry.getKey() + " like "
							+ entry.getValue());
				}
			}

			List<AuditTrailLog> results = dao.findByCriteria(criteria, first,
					pageSize);
			return results;
		}

	}
}
