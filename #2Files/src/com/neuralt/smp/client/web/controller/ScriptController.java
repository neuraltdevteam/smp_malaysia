package com.neuralt.smp.client.web.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.ScriptConfig;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class ScriptController implements Serializable {
	private static final long serialVersionUID = 3310031259323230869L;
	private static final Logger logger = LoggerFactory
			.getLogger(ScriptController.class);

	private ScriptConfig script;
	private ScriptConfig dummyScript;
	private List<ScriptConfig> scripts;
	private String content;

	private TreeNode hostRoot;
	private TreeNode selectedNode;
	private MonitoredHost selectedHost;

	public ScriptController() {
		reset();
		createHostTreeTable();
	}

	private void createHostTreeTable() {
		hostRoot = new DefaultTreeNode("root", null);

		for (MonitoredSystem sys : SmpClient.instance.getSystems()) {
			TreeNode sysNode = new DefaultTreeNode(sys, hostRoot);
			sysNode.setExpanded(true);
			for (MonitoredHost host : sys.getHosts()) {
				TreeNode hostNode = new DefaultTreeNode(host, sysNode);
				hostNode.setExpanded(true);
			}
		}
	}

	private void createTable() {
		scripts = DataAccess.scriptConfigDao.getScriptsByHost(selectedHost
				.getConfig());
	}

	public void reset() {
		script = new ScriptConfig();
		dummyScript = new ScriptConfig();
		content = null;
	}

	public void onHostNodeSelected(NodeSelectEvent event) {
		if (event.getTreeNode().getData() instanceof MonitoredHost) {
			selectedHost = ((MonitoredHost) event.getTreeNode().getData());
			createTable();
		} else {
			selectedHost = null;
		}
	}

	private void prepareContent(ScriptConfig script) {
		content = null;
		File file = new File(script.getPath());
		if (file.canRead()) {
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new FileReader(file));
				StringBuilder sb = new StringBuilder();
				while (reader.ready()) {
					sb.append(reader.readLine());
					sb.append("\n");
				}
				content = sb.toString();
			} catch (FileNotFoundException e) {
				logger.warn("File not found:" + e.getMessage());
			} catch (IOException e) {
				logger.warn("IOException:" + e.getMessage());
			} finally {
				if (reader != null)
					try {
						reader.close();
					} catch (IOException e) {
						logger.warn("Failed to close file reader!");
					}
			}
		}
	}

	public void addScript() {
		// generate filename for script
		StringBuilder sb = new StringBuilder();
		sb.append(AppConfig.getScriptFileStorageDir())
				.append(selectedHost.getConfig().getName()).append("_")
				.append(script.getName()).append("_")
				.append(StringUtil.convertTimeFormatForExport(new Date()))
				.append(AppConfig.getScriptFileExtension());
		String fileName = sb.toString();
		script.setPath(fileName);
		// save content to that file
		OutputStream os = null;
		PrintWriter writer = null;
		try {
			logger.debug("fileName: " + fileName);
			os = new FileOutputStream(fileName);
			writer = new PrintWriter(os);
			if (content != null)
				writer.print(content);

		} catch (IOException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.DB_ACCESS_ERROR);
		} finally {
			if (writer != null)
				writer.close();
			makeScriptRunnable();
		}
		// persist script
		script.setHost(selectedHost.getConfig());
		DataAccess.scriptConfigDao.save(script);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
				"add-script", script.toString());
		reset();
		createTable();
	}

	private void makeScriptRunnable() {
		if (AppConfig.getScriptRunnerVersion().equals(ConstantUtil.LINUX)) {
			String user = AppConfig.getScriptRunnerUser();
			String[] sudoComm = new String[6];
			sudoComm[0] = "sudo";
			sudoComm[1] = "-u";
			sudoComm[2] = user;
			sudoComm[3] = "chmod";
			sudoComm[4] = "+x";
			sudoComm[5] = script.getPath();
			ProcessBuilder builder = new ProcessBuilder(sudoComm);
			try {
				Process process = builder.start();
				process.waitFor();
			} catch (Exception e) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR, MessageKey.DB_ACCESS_ERROR);
			}
		}
	}

	public void editScript() {
		// save content to file path
		String fileName = script.getPath();
		script.update(dummyScript);
		OutputStream os = null;
		PrintWriter writer = null;
		try {
			logger.debug("fileName: " + fileName);
			os = new FileOutputStream(fileName);
			writer = new PrintWriter(os);
			if (content != null)
				writer.print(content);

		} catch (IOException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.DB_ACCESS_ERROR);
		} finally {
			if (writer != null)
				writer.close();
			makeScriptRunnable();
		}
		// persist script
		DataAccess.scriptConfigDao.save(script);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
				"edit-script", script.toString());
		reset();
		createTable();
	}

	public void deleteScript() {
		// delete script file on fs

		// delete script entity in db
	}

	// getter/setter
	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public TreeNode getHostRoot() {
		return hostRoot;
	}

	public ScriptConfig getScript() {
		return script;
	}

	public void setScript(ScriptConfig script) {
		this.script = script;
	}

	public ScriptConfig getDummyScript() {
		return dummyScript;
	}

	public void setDummyScript(ScriptConfig dummyScript) {
		this.dummyScript = dummyScript.getBackup();
		this.script = dummyScript;
		prepareContent(script);
	}

	public List<ScriptConfig> getScripts() {
		return scripts;
	}

	public MonitoredHost getSelectedHost() {
		return selectedHost;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
}
