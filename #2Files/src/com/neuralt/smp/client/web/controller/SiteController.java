package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.Site;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class SiteController implements Serializable {
	private static final long serialVersionUID = -7016477944366559289L;
	private static final Logger logger = LoggerFactory
			.getLogger(SiteController.class);
	private List<Site> sites = new ArrayList<Site>();
	private Site site = new Site();
	private Site selectedSite;
	private Site dummySite = new Site();
	private List<SelectItem> siteSelectItems = new ArrayList<SelectItem>();	
	private String passedParameter;
	
	public SiteController() {
		reload();
	}

	public void reload() {
		site = new Site();
		dummySite = new Site();		
		
		sites = DataAccess.siteDao.getAllSites();
		if (sites != null) {
			logger.debug("sites size=" + sites.size());
		}
	   	   		 
		siteSelectItems = null;
		siteSelectItems = new ArrayList<SelectItem>();	
		
		siteSelectItems.add(new SelectItem("All", "All"));
		
	    for(Site site: sites){
	    	siteSelectItems.add(new SelectItem(site.getSiteName(), site.getSiteName()));
	    }
		    
		selectedSite = null;
		
		String selectedSiteName = getPassedParameter("dataTabView:siteForm:Sites"); 
		logger.info("selectedSiteName=" + selectedSiteName);
		if (selectedSiteName != null)
		{
			
			if (selectedSiteName != "" && selectedSiteName != "All")
			{
				sites = null;
				sites = new ArrayList<Site>();		
				sites.add( DataAccess.siteDao.getSiteByName(selectedSiteName));
			}
		}
	}

	public void addSite() {
		boolean ok = true;

		Site dummy = DataAccess.siteDao.getSiteByName(site.getSiteName());
		if (dummy != null) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.SITE_DUPLICATED);
		}

		if (ok) {
			DataAccess.siteDao.save(site);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-site", site.toString());
			reload();
		}
	}

	public void deleteSite(Site site) {
		boolean ok = true;
		if (site.getHostCount() != 0) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.SITE_DELETE_FAILED_HAVEHOST);
		}
		if (ok) {
			DataAccess.siteDao.delete(site);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-site", site.toString());
			reload();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, site.getSiteName() + " "
							+ "is deleted");
		}
	}

	public void editSite() {
		logger.debug("dummySite=" + dummySite);
		logger.debug("site=" + site);
		boolean ok = true;

		Site dummy = DataAccess.siteDao.getSiteByName(dummySite.getSiteName());

		if (dummy != null && !dummy.equals(site)) {
			ok = false;
			site = new Site();
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.SITE_DUPLICATED);
		}

		if (ok) {
			site.update(dummySite);
			DataAccess.siteDao.save(site);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-site", site.toString());
			reload();
		}
	}

	// getter/setter

	public List<Site> getSites() {
		return sites;
	}

	public void setSites(List<Site> sites) {
		this.sites = sites;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}

	public Site getSelectedSite() {
		return selectedSite;
	}

	public void setSelectedSite(Site selectedSite) {
		this.selectedSite = selectedSite;
	}

	public Site getDummySite() {
		return dummySite;
	}

	public void setDummySite(Site dummySite) {
		this.dummySite = dummySite.getBackup();
		this.site = dummySite;
	}

	public boolean isSitesEmpty() {
		return sites == null || sites.isEmpty();
	}
	
	public List<SelectItem> getSiteSelectItems() {
		return siteSelectItems;
	}

	public void setSiteSelectItems(List<SelectItem> siteSelectItems) {
		this.siteSelectItems = siteSelectItems;
	}
	
	public String getPassedParameter(String paramName) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		this.passedParameter = (String) facesContext.getExternalContext().
		getRequestParameterMap().get(paramName);
		return this.passedParameter;
	}
		 

	public void setPassedParameter(String passedParameter) {
		this.passedParameter = passedParameter;
	}	
}
