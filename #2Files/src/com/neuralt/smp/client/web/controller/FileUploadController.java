package com.neuralt.smp.client.web.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.UserBean;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class FileUploadController {
	private static final Logger logger = LoggerFactory
			.getLogger(FileUploadController.class);

	@ManagedProperty(value = "#{UserBean}")
	private UserBean userBean;

	public void handleMibFileUpload(FileUploadEvent event) throws IOException {
		UploadedFile file = event.getFile();
		FileOutputStream fos = null;
		InputStream is = null;
		try {
			logger.debug("fileName: " + file.getFileName());
			String fileName = file.getFileName();
			int i = fileName.lastIndexOf(".");
			StringBuilder newFileName = new StringBuilder();
			newFileName.append(fileName.substring(0, i)).append("_")
					.append(StringUtil.convertTimeFormatForExport(new Date()))
					.append("_uploadedBy_").append(userBean.getCurrentUserId())
					.append(".mib");
			fos = new FileOutputStream(new File(AppConfig.getAllMIBFileDir(),
					newFileName.toString()));
			is = file.getInputstream();
			int BUFFER_SIZE = 8192;
			byte[] buffer = new byte[BUFFER_SIZE];
			int a;
			while (true) {
				a = is.read(buffer);
				if (a < 0)
					break;
				fos.write(buffer, 0, a);
				fos.flush();
			}
			fos.close();
			is.close();
		} catch (IOException e) {
		} finally {
			if (fos != null)
				fos.close();
			if (is != null)
				fos.close();
		}
		logger.debug("detected file upload:" + file.getFileName());
		WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
				MessageKey.GENERAL_SUCCESS, file.getFileName()
						+ " is uploaded.");
	}
}
