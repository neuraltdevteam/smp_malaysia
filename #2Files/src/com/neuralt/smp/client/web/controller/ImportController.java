package com.neuralt.smp.client.web.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import net.percederberg.mibble.MibLoaderException;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.RowEditEvent;
import org.primefaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.MibFlag;
import com.neuralt.smp.client.snmp.MibFileHandler;
import com.neuralt.smp.client.task.FileDeleteTask;
import com.neuralt.smp.client.util.OidUtil;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.web.security.UserBean;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class ImportController implements Serializable {
	private static final long serialVersionUID = -170358930804691437L;
	private static final Logger logger = LoggerFactory
			.getLogger(ImportController.class);
	private List<MibFlag> mibList;
	private MibFlag mib;

	@ManagedProperty(value = "#{UserBean}")
	private UserBean userBean;

	public ImportController() {
		mibList = new ArrayList<MibFlag>();
		reload();
	}

	private void reload() {
		mibList.clear();
		for (MibFlag flag : DataAccess.mibFlagDao.getAllMibFlags()) {
			mibList.add(flag);
		}
	}

	public List<MibFlag> getMibList() {
		return mibList;
	}

	public void handleMibFileUpload(FileUploadEvent event) throws IOException {
		// handle the uploaded file and save it to allMib folder
		boolean ok = true;
		UploadedFile file = event.getFile();
		FileOutputStream fos = null;
		InputStream is = null;
		String fileName = file.getFileName();
		int i = fileName.lastIndexOf(".");
		if (i < 0) {
			i = fileName.length();
		}
		StringBuilder sb = new StringBuilder();
		sb.append(fileName.substring(0, i)).append("_")
				.append(StringUtil.convertTimeFormatForExport(new Date()))
				.append("_uploadedBy_").append(userBean.getCurrentUserId())
				.append(".mib");
		String newFileName = sb.toString();
		String dir = AppConfig.getAllMIBFileDir();
		try {
			logger.debug("fileName: " + file.getFileName());

			fos = new FileOutputStream(new File(dir, newFileName));
			is = file.getInputstream();
			int BUFFER_SIZE = 8192;
			byte[] buffer = new byte[BUFFER_SIZE];
			int a;
			while (true) {
				a = is.read(buffer);
				if (a < 0)
					break;
				fos.write(buffer, 0, a);
				fos.flush();
			}
			fos.close();
			is.close();
		} catch (IOException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.UPLOAD_FAILED + ":"
							+ e.getMessage());
			ok = false;
		} finally {
			if (fos != null)
				fos.close();
			if (is != null)
				fos.close();
		}
		logger.debug("detected file upload:" + file.getFileName());

		// start to process the file
		try {
			MibFileHandler.getInstance().processMibFile(dir, newFileName);
		} catch (Exception e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.MIB_FILE_ERROR + ":"
							+ e.getMessage());
			ok = false;
			// delete the file
			Thread deleteFile = new FileDeleteTask(dir, newFileName);
			deleteFile.start();
		}

		if (ok) {
			OidUtil.getInstance().conditionalInit();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, MessageKey.MIB_FILE_LOADED
							+ ":" + file.getFileName());
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-mib", file.getFileName());
		}
		reload();

	}

	public void deleteMib() {
		boolean ok = true;
		try {
			OidUtil.getInstance().deleteMib(mib);
		} catch (MibLoaderException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.MIB_ERR_LOADER);
			ok = false;
		} catch (IOException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.MIB_ERR_IO);
			ok = false;
		}
		if (ok) {
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, MessageKey.MIB_UNLOADED);
			reload();
		}
	}

	public void setMib(MibFlag mib) {
		this.mib = mib;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	public void onMibTableEdit(RowEditEvent event) {
		MibFlag flag = (MibFlag) (event.getObject());
		DataAccess.mibFlagDao.save(flag);
		WebUtil.addMessage(
				FacesMessage.SEVERITY_INFO,
				MessageKey.GENERAL_SUCCESS,
				flag.getName()
						+ " "
						+ (flag.isLoaded() ? WebUtil
								.getMessage(MessageKey.MIB_LOADED) : WebUtil
								.getMessage(MessageKey.MIB_UNLOADED)));

	}
}