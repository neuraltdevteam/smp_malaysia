package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.OidMap;
import com.neuralt.smp.client.data.OidMap.ObjectName;
import com.neuralt.smp.client.data.OidMap.ObjectNameMap;
import com.neuralt.smp.client.data.ProductGroup;
import com.neuralt.smp.client.data.TrapContainer;
import com.neuralt.smp.client.util.OidUtil;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.snmp.proxy.SvrSnmpConstants;
import com.neuralt.smp.snmp.util.MibFileLoader;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class OidMapController implements Serializable {
	private static final long serialVersionUID = 1206498698352249699L;
	private static final Logger logger = LoggerFactory
			.getLogger(OidMapController.class);

	private TreeNode groupEntityRoot;
	private TreeNode selectedGroupNode;
	private TreeNode trapEntityRoot;
	private TreeNode selectedTrapNode;

	private OidMap oidMap;
	private OidMap dummyOidMap;
	private OidMap newOidMap;
	private ObjectNameMap objectNameMap;
	private ObjectNameMap dummyObjectNameMap;
	private ObjectName dummyObjectName;
	private ObjectName newObjectName;
	private ObjectName objectName;
	private ProductGroup group;
	private ProductGroup dummyProductGroup;
	private ProductGroup newProductGroup;
	private List<ProductGroup> groups;
	private TrapContainer selectedTrap;
	private Map<OID, List<OID>> reverseMap;
	private List<ObjectName> objectNameList;
	private List<OidMap> oidMapList;
	private List<OidMap> dummyDisplayList;
	private List<TrapContainer> svrTrapList;
	public static List<TrapContainer> svrConstantTraps;

	static {
		svrConstantTraps = new ArrayList<TrapContainer>();
		svrConstantTraps.add(new TrapContainer(new OID(""), ""));
		svrConstantTraps.add(new TrapContainer(SvrSnmpConstants.trapAlarmId,
				"trapAlarmId"));
		svrConstantTraps.add(new TrapContainer(SvrSnmpConstants.trapAlarmLevel,
				"trapAlarmLevel"));
		svrConstantTraps.add(new TrapContainer(SvrSnmpConstants.trapAppName,
				"trapAppName"));
		svrConstantTraps.add(new TrapContainer(
				SvrSnmpConstants.trapDescription, "trapDescription"));
	}

	public OidMapController() {
		groups = new ArrayList<ProductGroup>();
		oidMapList = new ArrayList<OidMap>();
		// initializing svr trap list to be mapped
		svrTrapList = new ArrayList<TrapContainer>();
		TrapContainer emptyTrap = new TrapContainer();
		emptyTrap.setName("");
		emptyTrap.setOid(new OID());
		svrTrapList.add(emptyTrap);
		for (OID oid : MibFileLoader.getInstance().getSvrTrapNameMap().keySet()) {
			TrapContainer dummy = new TrapContainer();
			dummy.setOid(oid);
			dummy.setName(MibFileLoader.getInstance().getSvrTrapNameMap()
					.get(oid));
			svrTrapList.add(dummy);
		}
		Collections.sort(svrTrapList);

		createGroupTreeTableModel();
		// TODO
		group = (ProductGroup) groupEntityRoot.getChildren().get(0).getData();
		createTrapTreeTableModel();
		dummyDisplayList = new ArrayList<OidMap>();
		if (trapEntityRoot.getChildren() != null
				&& trapEntityRoot.getChildren().size() != 0) {
			oidMap = (OidMap) trapEntityRoot.getChildren().get(0).getData();
			objectNameList = oidMap.getObjectNamesAsList();
			dummyDisplayList.add(oidMap);
		}
		reset();
	}

	private void createGroupTreeTableModel() {
		groupEntityRoot = new DefaultTreeNode("root", null);
		groupEntityRoot.setExpanded(true);
		try {
			groups = DataAccess.productGroupDao.getAllProductGroups();
			for (ProductGroup gp : groups) {
				TreeNode trapNode = new DefaultTreeNode(gp, groupEntityRoot);
				trapNode.setExpanded(true);
			}
		} catch (Exception e) {
			logger.error("failed in fetching ProductGroup from DB", e);
		}
	}

	private void createTrapTreeTableModel() {
		trapEntityRoot = new DefaultTreeNode("root", null);
		trapEntityRoot.setExpanded(true);
		for (OidMap oidMap : group.getOidMapAsList()) {
			// if (logger.isDebugEnabled())
			// logger.debug("oid:"+oidMap.getOidAsString());
			TreeNode trapNode = new DefaultTreeNode(oidMap, trapEntityRoot);
			trapNode.setExpanded(true);
		}
	}

	public void onGroupSelect(NodeSelectEvent event) {
		TreeNode entityNode = event.getTreeNode();
		group = (ProductGroup) entityNode.getData();
		dummyProductGroup = group.getBackup();
		createTrapTreeTableModel();
	}

	public void onTrapSelect(NodeSelectEvent event) {
		TreeNode entityNode = event.getTreeNode();
		oidMap = (OidMap) entityNode.getData();
		objectNameList = oidMap.getObjectNamesAsList();
		dummyDisplayList.clear();
		dummyDisplayList.add(oidMap);
	}

	public void addNewProductGroup() {
		DataAccess.productGroupDao.save(newProductGroup);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
				"add-product group", newProductGroup.toString());
		reset();
		OidUtil.getInstance().reload();
		createGroupTreeTableModel();
		createTrapTreeTableModel();
	}

	public void editProductGroup() {
		boolean ok = true;

		if (ok) {
			group.updateConfig(dummyProductGroup);
			DataAccess.productGroupDao.save(group);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-product group", group.toString());
			reset();
			OidUtil.getInstance().reload();
			createGroupTreeTableModel();
			createTrapTreeTableModel();
		}
	}

	public void deleteProductGroup() {
		DataAccess.productGroupDao.delete(group);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
				"delete-product group", group.toString());
		reset();
		OidUtil.getInstance().reload();
		createGroupTreeTableModel();
		createTrapTreeTableModel();
	}

	public void addOidMap() {
		boolean ok = true;

		if (StringUtil.isNullOrEmpty(newOidMap.getName())) {
			ok = false;
			WebUtil.addMessage("oid.dlg.prompt.trap_format_error",
					"oid.dlg.prompt.trapName", FacesMessage.SEVERITY_ERROR);
		}
		if (StringUtil.isNullOrEmpty(newOidMap.getOid())) {
			ok = false;
			WebUtil.addMessage("oid.dlg.prompt.trap_format_error",
					"oid.dlg.prompt.oid", FacesMessage.SEVERITY_ERROR);
		}

		if (ok) {
			if (logger.isDebugEnabled())
				logger.debug("adding oidMap:" + newOidMap);
			newOidMap.getParent().getOidMaps().add(newOidMap);
			DataAccess.productGroupDao.save(newOidMap.getParent());
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-OID map", newOidMap.toString());
			reset();
			OidUtil.getInstance().reload();
			createGroupTreeTableModel();
			group = (ProductGroup) groupEntityRoot.getChildren().get(0)
					.getData();
			createTrapTreeTableModel();
		}
	}

	public void addObjectName() {
		newObjectName.setParent(oidMap);
		oidMap.getObjectNames().add(newObjectName);
		DataAccess.oidMapDao.save(oidMap);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
				"add-object name", oidMap.toString());
		reset();
	}

	public void addObjectNameMap() {
		boolean ok = true;

		for (ObjectNameMap iterator : oidMap.getObjectNameMaps()) {
			if (iterator.getOid().equals(objectNameMap.getOid())
					&& iterator.getValue().equals(objectNameMap.getValue())) {
				WebUtil.addMessage(MessageKey.GENERAL_ERROR,
						"Duplicated value", FacesMessage.SEVERITY_ERROR);
				ok = false;
			}
		}

		if (ok) {
			objectNameMap.setParent(oidMap);
			oidMap.getObjectNameMaps().add(objectNameMap);
			try {
				DataAccess.oidMapDao.save(oidMap);
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
						"add-object name map", oidMap.toString());
				reset();
				OidUtil.getInstance().reload();
			} catch (Exception e) {

			}
		}
	}

	public void editObjectNameMap() {
		// checking
		boolean ok = true;

		for (ObjectNameMap iterator : oidMap.getObjectNameMaps()) {
			if (iterator.getOid().equals(dummyObjectNameMap.getOid())
					&& iterator.getValue()
							.equals(dummyObjectNameMap.getValue())
					&& !iterator.equals(objectNameMap)) {
				WebUtil.addMessage(MessageKey.GENERAL_ERROR,
						"Duplicated value", FacesMessage.SEVERITY_ERROR);
				ok = false;
			}
		}

		if (ok) {
			objectNameMap.updateConfig(dummyObjectNameMap);
			try {
				DataAccess.oidMapDao.save(oidMap);
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
						"edit-object name map", oidMap.toString());
				reset();
				OidUtil.getInstance().reload();
			} catch (Exception e) {
			}
		}

	}

	public void deleteObjectName(ObjectName objName) {
		if (logger.isDebugEnabled())
			logger.debug("deleting object Name:" + objName);
		OidMap parent = objName.getParent();
		parent.getObjectNames().remove(objName);
		// also delete the value mappings under this Object Name
		for (ObjectNameMap objNameMap : parent.getObjectNameMapsAsList()) {
			if (objNameMap.getOid().equals(objName.getOid())) {
				parent.getObjectNameMaps().remove(objNameMap);
			}
		}
		DataAccess.oidMapDao.save(parent);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
				"delete-object name", parent.toString());
		reset();
		OidUtil.getInstance().reload();
	}

	public void deleteObjectNameMap(ObjectNameMap objNameMap) {
		if (logger.isDebugEnabled())
			logger.debug("deleting object Name Map:" + objNameMap);
		OidMap parent = objNameMap.getParent();
		parent.getObjectNameMaps().remove(objNameMap);
		DataAccess.oidMapDao.save(parent);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
				"delete-object name map", parent.toString());
		reset();
		OidUtil.getInstance().reload();
	}

	public void deleteOidMap() {
		if (logger.isDebugEnabled())
			logger.debug("deleting oidMap:" + oidMap);
		ProductGroup parent = oidMap.getParent();
		parent.getOidMaps().remove(oidMap);
		DataAccess.productGroupDao.save(parent);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
				"delete-OID map", oidMap.toString());
		reset();
		OidUtil.getInstance().reload();
		createTrapTreeTableModel();
		oidMap = null;
	}

	public void editObjectName() {
		// checking
		boolean ok = true;

		if (ok) {
			objectName.updateConfig(dummyObjectName);
			try {
				DataAccess.oidMapDao.save(oidMap);
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
						"edit-object name", oidMap.toString());
				reset();
				OidUtil.getInstance().reload();
			} catch (Exception e) {
			}
		}

	}

	public void editOidMap() {
		if (logger.isDebugEnabled()) {
			logger.debug("editOidMap is called");
		}
		boolean ok = true;

		OID temp = dummyOidMap.getOid();
		// logger.debug("dummyOidMap="+temp);
		// if (StringUtil.isNullOrEmpty(dummyOidMap.getName())) {
		// ok = false;
		// WebUtil.addMessage(FacesResourceBundleUtil.getMessage("oid.dlg.prompt.trap_format_error"),
		// FacesResourceBundleUtil.getMessage("oid.dlg.prompt.trapName"),
		// FacesMessage.SEVERITY_ERROR);
		// }
		if (StringUtil.isNullOrEmpty(dummyOidMap.getOid())) {
			ok = false;
			WebUtil.addMessage("oid.dlg.prompt.trap_format_error",
					"oid.dlg.prompt.oid", FacesMessage.SEVERITY_ERROR);
		}

		if (ok) {
			if (logger.isDebugEnabled()) {
				logger.debug("original group:" + group.getName() + " id:"
						+ group.getId());
			}
			group.getOidMaps().remove(oidMap);
			// oidMap.setParent(null);
			// oidMap.updateConfig(dummyOidMap);
			if (logger.isDebugEnabled()) {
				logger.debug("new group:" + dummyOidMap.getParent().getName());
			}
			dummyOidMap.getParent().getOidMaps().add(dummyOidMap);
			try {
				// if (logger.isDebugEnabled()) {
				// logger.debug("dummyOidMap objname size after edit:"+dummyOidMap.getObjectNames().size());
				// for (ObjectName o : dummyOidMap.getObjectNames()) {
				// logger.debug(o.getOidAsTrapName()+" has parent dummy:"+o.getParent().equals(dummyOidMap));
				// }
				// }

				// for (OidMap check : group.getOidMaps()) {
				// if (logger.isDebugEnabled()) {
				// logger.debug("oidMap id:"+check.getId());
				// }
				// }
				DataAccess.oidMapDao.save(dummyOidMap);
				DataAccess.productGroupDao.save(dummyOidMap.getParent());
				DataAccess.productGroupDao.save(group);

				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
						"edit-OID map", dummyOidMap.toString());
				OidMap newDummy = DataAccess.oidMapDao
						.getOidMapByOid(dummyOidMap.getOid());
				if (logger.isDebugEnabled()) {
					logger.debug("newDummy objname size:"
							+ newDummy.getObjectNames().size());
				}
				reset();
				OidUtil.getInstance().reload();
				createGroupTreeTableModel();
				createTrapTreeTableModel();
				oidMap = null;
				dummyDisplayList.clear();
				dummyDisplayList.add(oidMap);
			} catch (Exception e) {
				logger.error("failed in editing OidMap", e);
			}
		}
	}

	private void reset() {
		newProductGroup = new ProductGroup();
		objectNameMap = new ObjectNameMap();
		newOidMap = new OidMap();
		List<ProductGroup> list = DataAccess.productGroupDao
				.getAllProductGroups();
		if (list != null && list.size() != 0)
			newOidMap.setParent(DataAccess.productGroupDao
					.getAllProductGroups().get(0));
		dummyOidMap = new OidMap();
		newObjectName = new ObjectName();
		oidMapList.add(new OidMap());
		try {
			oidMapList.addAll(DataAccess.oidMapDao.getAllOidMaps());
		} catch (Exception e) {
			// cannot access db
		}
	}

	public List<ProductGroup> getGroups() {
		return DataAccess.productGroupDao.getAllProductGroups();
	}

	public TrapContainer getSelectedTrap() {
		return selectedTrap;
	}

	public void setSelectedTrap(TrapContainer selectedTrap) {
		this.selectedTrap = selectedTrap;
		try {
			objectNameList = DataAccess.oidMapDao.getOidMapByOid(
					selectedTrap.getOid()).getObjectNamesAsList();
		} catch (Exception e) {
			logger.error("failed in fetching ObjectName from DB", e);
		}
	}

	public boolean isTrapSelected() {
		if (logger.isDebugEnabled())
			logger.debug("rendering..." + selectedTrap);
		return selectedTrap != null;
	}

	public boolean isOidMapSelected() {
		// if (logger.isDebugEnabled())
		// logger.debug("rendering..."+oidMap);
		return oidMap != null;
	}

	public Map<OID, List<OID>> getReverseMap() {
		return reverseMap;
	}

	public List<OID> getListForSelectedTrap() {
		if (logger.isDebugEnabled())
			logger.debug("Getting list...selected trap:" + selectedTrap);
		return reverseMap.get(selectedTrap.getOid());
	}

	public List<ObjectName> getObjectNameList() {
		return objectNameList;
	}

	public List<ObjectNameMap> getObjectNameMapList() {
		try {
			if (oidMap != null)
				return oidMap.getObjectNameMapsAsList();
		} catch (Exception ignore) {
		}
		return null;
	}

	public Map<String, WarningLevel> getWarningLevelMap() {
		Map<String, WarningLevel> map = new HashMap<String, WarningLevel>();
		for (WarningLevel level : WarningLevel.values()) {
			map.put(level.name(), level);
		}
		return map;
	}

	public OidMap getOidMap() {
		return oidMap;
	}

	public void setOidMap(OidMap oidMap) {
		if (logger.isDebugEnabled())
			logger.debug("set oid map called:" + oidMap.getOidAsTrapName());
		this.oidMap = oidMap;
	}

	public ObjectNameMap getObjectNameMap() {
		return objectNameMap;
	}

	public void setObjectNameMap(ObjectNameMap objectNameMap) {
		this.objectNameMap = objectNameMap;
	}

	public ObjectNameMap getDummyObjectNameMap() {
		return dummyObjectNameMap;
	}

	public void setDummyObjectNameMap(ObjectNameMap dummyObjectNameMap) {
		this.dummyObjectNameMap = dummyObjectNameMap.getBackup();
		this.objectNameMap = dummyObjectNameMap;
	}

	public ObjectName getDummyObjectName() {
		return dummyObjectName;
	}

	public void setDummyObjectName(ObjectName dummyObjectName) {
		this.dummyObjectName = dummyObjectName.getBackup();
		this.objectName = dummyObjectName;
		this.oidMap = dummyObjectName.getParent();
	}

	public List<OidMap> getOidMapList() {
		return oidMapList;
	}

	public List<TrapContainer> getSvrConstantTraps() {
		return svrConstantTraps;
	}

	public OidMap getDummyOidMap() {
		return dummyOidMap;
	}

	public void setDummyOidMap() {
		this.dummyOidMap = oidMap.getBackup();
		if (logger.isDebugEnabled()) {
			logger.debug("oidMap objname size before edit:"
					+ oidMap.getObjectNames().size());
			logger.debug("dummyOidMap objname size before edit:"
					+ dummyOidMap.getObjectNames().size());
		}
	}

	public TreeNode getSelectedGroupNode() {
		return selectedGroupNode;
	}

	public void setSelectedGroupNode(TreeNode selectedGroupNode) {
		this.selectedGroupNode = selectedGroupNode;
	}

	public TreeNode getSelectedTrapNode() {
		return selectedTrapNode;
	}

	public void setSelectedTrapNode(TreeNode selectedTrapNode) {
		this.selectedTrapNode = selectedTrapNode;
	}

	public TreeNode getGroupEntityRoot() {
		return groupEntityRoot;
	}

	public TreeNode getTrapEntityRoot() {
		return trapEntityRoot;
	}

	public OidMap getNewOidMap() {
		return newOidMap;
	}

	public void setNewOidMap(OidMap newOidMap) {
		this.newOidMap = newOidMap;
	}

	public ObjectName getObjectName() {
		return objectName;
	}

	public void setObjectName(ObjectName objectName) {
		this.objectName = objectName;
	}

	public ObjectName getNewObjectName() {
		return newObjectName;
	}

	public void setNewObjectName(ObjectName newObjectName) {
		this.newObjectName = newObjectName;
	}

	public List<OidMap> getDummyDisplayList() {
		return dummyDisplayList;
	}

	public ProductGroup getDummyProductGroup() {
		return dummyProductGroup;
	}

	public void setDummyProductGroup(ProductGroup dummyProductGroup) {
		this.dummyProductGroup = dummyProductGroup;
	}

	public ProductGroup getNewProductGroup() {
		return newProductGroup;
	}

	public void setNewProductGroup(ProductGroup newProductGroup) {
		this.newProductGroup = newProductGroup;
	}

	public String getHeader() {
		StringBuilder sb = new StringBuilder();
		sb.append("Mapping");
		if (oidMap != null) {
			sb.append(" (").append(oidMap.getOidAsTrapName()).append(" - ")
					.append(oidMap.getOidAsString()).append(")");
		}
		return sb.toString();
	}

	public List<TrapContainer> getSvrTrapList() {
		return svrTrapList;
	}

}
