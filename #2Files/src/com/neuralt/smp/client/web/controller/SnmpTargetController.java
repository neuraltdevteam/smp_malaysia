package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataCollectionProcedure;
import com.neuralt.smp.client.data.MibFlag;
import com.neuralt.smp.client.data.SnmpTarget;
import com.neuralt.smp.client.data.SnmpTarget.MethodType;
import com.neuralt.smp.client.data.TrapContainer;
import com.neuralt.smp.client.util.OidUtil;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.HostSnmpConfig;
import com.neuralt.smp.config.HostSnmpConfig.MibFileWrapper;
import com.neuralt.smp.snmp.util.MibFileLoader;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class SnmpTargetController implements Serializable {
	private static final long serialVersionUID = -3019129205974543923L;
	private static final Logger logger = LoggerFactory
			.getLogger(SnmpTargetController.class);

	private TreeNode snmpRoot;
	private TreeNode selectedSnmpNode;
	private SnmpTarget target;
	private boolean createNew;
	private SnmpTarget dummyTarget;
	private HostSnmpConfig snmpConfig;
	private List<SnmpTarget> targets;
	private List<TrapContainer> containers;
	private List<String> oidNames = new ArrayList<String>();
	private Map<String, OID> oidNamesMap = new HashMap<String, OID>();
	private String selectedKeyDir;
	private String selectedValueDir;

	private List<OID> oidList;
	private List<MibFlag> mibFlagList;

	public SnmpTargetController() {
		createSnmpTreeTable();
		containers = new ArrayList<TrapContainer>();
		target = new SnmpTarget();

		// keep a list of OIDs of all traps. avoid loading them repeatedly
		oidList = new ArrayList<OID>(MibFileLoader.getInstance()
				.getAllTrapNameMap().keySet());
		Collections.sort(oidList);
		oidList.remove(0); // to remove the entry "0.0"

		// keep a list of all MIB flags. avoid loading them repeatedly
		mibFlagList = DataAccess.mibFlagDao.getAllMibFlags();
	}

	private void createSnmpTreeTable() {
		snmpRoot = new DefaultTreeNode("root", null);

		for (MonitoredSystem sys : SmpClient.instance.getSystems()) {
			TreeNode sysNode = new DefaultTreeNode(sys, snmpRoot);
			sysNode.setExpanded(true);
			for (MonitoredHost host : sys.getHosts()) {
				TreeNode hostNode = new DefaultTreeNode(host, sysNode);
				hostNode.setExpanded(true);
				for (HostSnmpConfig snmpConfig : host.getConfig()
						.getSnmpConfigs()) {
					TreeNode snmpNode = new DefaultTreeNode(snmpConfig,
							hostNode);
					snmpNode.setExpanded(true);
				}
			}
		}
	}

	public void reset() {
		target = new SnmpTarget();
		selectedKeyDir = null;
		selectedValueDir = null;
	}

	public void onSnmpNodeSelected(NodeSelectEvent event) {
		if (event.getTreeNode().getData() instanceof HostSnmpConfig) {
			snmpConfig = (HostSnmpConfig) event.getTreeNode().getData();
			createSnmpTargetTable();
		} else {
			snmpConfig = null;
		}

	}

	private void createSnmpTargetTable() {
		targets = DataAccess.snmpTargetDao
				.getSnmpTargetByHostSnmpConfig(snmpConfig);
		containers.clear();
		oidNames.clear();
		oidNamesMap.clear();

		if (oidList != null && oidList.size() != 0) {
			List<String> mibNameInSnmpConfigList = new ArrayList<String>();
			for (MibFileWrapper wrapper : snmpConfig.getMibFileList()) {
				mibNameInSnmpConfigList.add(wrapper.getMibName());
			}

			for (OID oid : oidList) {
				// for each OID, find out the MIBs that this OID belongs to
				List<String> mibNameMapList = MibFileLoader.getInstance()
						.getAllTrapToMibNameMap().get(oid);
				for (MibFlag importedMib : mibFlagList) {
					if (mibNameMapList.contains(importedMib.getName())
							&& mibNameInSnmpConfigList.contains(importedMib
									.getName()) && importedMib.isLoaded()) {
						String oidName = MibFileLoader.getInstance()
								.getAllTrapNameMap().get(oid);
						if (oidName != null) {
							TrapContainer container = new TrapContainer(oid,
									oidName);
							containers.add(container);
							oidNames.add(oidName);
							oidNamesMap.put(oidName, oid);
							break;
						}
					}
				}

			}
		}

	}

	public void saveSnmpTarget() {
		logger.debug("saving target...");
		for (HostSnmpConfig snmp : snmpConfig.getHost().getSnmpConfigs()) {
			SnmpTarget dummy = DataAccess.snmpTargetDao.getSnmpTargetByName(
					target.getName(), snmp);
			if (dummy != null) {
				if (createNew || (!createNew && !dummy.equals(target))) {
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.DUPLICATED_NAME);
					return;
				}
			}
		}
		logger.debug("key={}, value={}", selectedKeyDir, selectedValueDir);
		target.setSnmpConfig(snmpConfig);
		OID keyDir = oidNamesMap.get(selectedKeyDir);
		OID valueDir = oidNamesMap.get(selectedValueDir);
		logger.debug("(OID) key={}, value={}", keyDir, valueDir);
		try {
			if (MethodType.DYNAMIC.equals(target.getMethodType())) {
				if (keyDir == null) {
					keyDir = new OID(selectedKeyDir);
				}
				target.setKeyDir(keyDir.toString());
			}
			if (valueDir == null) {
				valueDir = new OID(selectedValueDir);
			}
			target.setValueDir(valueDir.toString());
		} catch (Exception e) {
			logger.debug("exception={}", e);
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.OID_FORMAT_ERROR);
			return;
		}
		DataAccess.snmpTargetDao.save(target);
		AuditLogger.logAuditTrail(ScreenType.CONFIG,
				createNew ? ActionType.INSERT : ActionType.UPDATE,
				createNew ? "add-SNMP target" : "edit-SNMP target", target
						.toString());
		createSnmpTargetTable();
		reset();
	}

	public void deleteSnmpTarget(SnmpTarget target) {
		DataAccess.snmpTargetDao.delete(target);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
				"delete-SNMP target", target.toString());
		createSnmpTargetTable();
	}

	// getter/setter

	public TreeNode getSelectedSnmpNode() {
		return selectedSnmpNode;
	}

	public void setSelectedSnmpNode(TreeNode selectedSnmpNode) {
		this.selectedSnmpNode = selectedSnmpNode;
	}

	public TreeNode getSnmpRoot() {
		return snmpRoot;
	}

	public SnmpTarget getTarget() {
		return target;
	}

	public void setTarget(SnmpTarget target) {
		this.target = target;
		if (target.getKeyDir() != null) {
			selectedKeyDir = target.getKeyDir();
			try {
				OID key = new OID(target.getKeyDir());
				String keyName = MibFileLoader.getInstance()
						.getAllTrapNameMap().get(key);
				if (keyName != null) {
					selectedKeyDir = keyName;
				}
			} catch (Exception ignore) {
			}
		}
		if (target.getValueDir() != null) {
			selectedValueDir = target.getValueDir();
			try {
				OID value = new OID(target.getValueDir());
				String valueName = MibFileLoader.getInstance()
						.getAllTrapNameMap().get(value);
				if (valueName != null) {
					selectedValueDir = valueName;
				}
			} catch (Exception ignore) {
			}
		}
	}

	public SnmpTarget getDummyTarget() {
		return dummyTarget;
	}

	public void setDummyTarget(SnmpTarget dummyTarget) {
		this.dummyTarget = dummyTarget.getBackup();
		this.target = dummyTarget;
	}

	public List<SnmpTarget> getTargets() {
		return targets;
	}

	public List<TrapContainer> getContainers() {
		return containers;
	}

	public HostSnmpConfig getSnmpConfig() {
		return snmpConfig;
	}

	public String getWalkDirStr(SnmpTarget target) {
		if (target.getMethodType().equals(MethodType.STATIC)) {
			return WebUtil.getMessage(MessageKey.NOT_APPLICABLE);
		}
		return OidUtil.getInstance().mapOidToOriginalTrapName(
				target.getWalkDir());
	}

	public String getKeyDirStr(SnmpTarget target) {
		if (target.getMethodType().equals(MethodType.STATIC)) {
			return WebUtil.getMessage(MessageKey.NOT_APPLICABLE);
		}
		return OidUtil.getInstance().mapOidToOriginalTrapName(
				target.getKeyDir());
	}

	public String getValueDirStr(SnmpTarget target) {
		return OidUtil.getInstance().mapOidToOriginalTrapName(
				target.getValueDir());
	}

	private Map<SnmpTarget, Boolean> isSnmpTargetLockedCache = new HashMap<SnmpTarget, Boolean>();

	// it is called when rendering the target data table. to speed things up
	public boolean isLocked(SnmpTarget target) {
		Boolean isLocked = isSnmpTargetLockedCache.get(target);
		if (isLocked == null) {
			isLocked = Boolean.FALSE;
			Set<DataCollectionProcedure> procedures = target
					.getDataCollectProc();
			for (DataCollectionProcedure proc : procedures) {
				isLocked = proc.getDs().isLocked();
				isSnmpTargetLockedCache.put(target, isLocked);
			}
		}

		return isLocked;
	}

	public List<String> getOidNames() {
		return oidNames;
	}

	public void setCreateNew(boolean createNew) {
		this.createNew = createNew;
	}

	public String getSelectedKeyDir() {
		return selectedKeyDir;
	}

	public void setSelectedKeyDir(String selectedKeyDir) {
		this.selectedKeyDir = selectedKeyDir;
	}

	public String getSelectedValueDir() {
		return selectedValueDir;
	}

	public void setSelectedValueDir(String selectedValueDir) {
		this.selectedValueDir = selectedValueDir;
	}
}
