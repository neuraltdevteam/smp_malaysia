package com.neuralt.smp.client.web.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.stats.CpuStats;
import com.neuralt.smp.client.data.stats.DiskStats;
import com.neuralt.smp.client.data.stats.HostStats;
import com.neuralt.smp.client.data.stats.InetStats;
import com.neuralt.smp.client.data.stats.MemStats;
import com.neuralt.smp.client.export.DataExporter;
import com.neuralt.smp.client.task.FileDeleteTask;
import com.neuralt.smp.client.util.FixedLengthLinkedHashMap;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.PropertiesUtil;
import com.neuralt.smp.util.StringUtil;

@ManagedBean
@ViewScoped
public class StatisticsViewer implements Serializable {
	private static final long serialVersionUID = -3394266642170614991L;

	private static final Logger logger = LoggerFactory
			.getLogger(StatisticsViewer.class);

	private HostChartModel cpuModel;
	private HostChartModel memModel;
	private HostChartModel diskModel;
	private HostChartModel inetModel;

	private CartesianChartModel dummyModel;
	private DashboardModel model;

	private Date searchFrom;
	private Date searchTo;

	private Long hourStart;
	private Long dayStart;
	private Long weekStart;
	private Long monthStart;
	private Long customStart;
	private Long dummyStart;
	private int sliderNumber;

	private TreeNode entityRoot;
	private TreeNode[] selectedNodes; // for TreeTable's own good health

	private int activeTab = 0;

	public static class HostChartModel {
		private CartesianChartModel hour;
		private CartesianChartModel day;
		private CartesianChartModel week;
		private CartesianChartModel month;
		private CartesianChartModel custom;
		private float hourMax = 10;
		private float dayMax = 10;
		private float weekMax = 10;
		private float monthMax = 10;
		private float customMax = 10;

		private MonitoredHost host;

		public HostChartModel(MonitoredHost host) {
			this.host = host;
		}

		public HostChartModel() {
			hour = new CartesianChartModel();
			day = new CartesianChartModel();
			week = new CartesianChartModel();
			month = new CartesianChartModel();
			custom = new CartesianChartModel();
		}

		public float getHourMax() {
			return hourMax;
		}

		public void setHourMax(float hourMax) {
			this.hourMax = hourMax;
		}

		public float getDayMax() {
			return dayMax;
		}

		public void setDayMax(float dayMax) {
			this.dayMax = dayMax;
		}

		public float getWeekMax() {
			return weekMax;
		}

		public void setWeekMax(float weekMax) {
			this.weekMax = weekMax;
		}

		public float getMonthMax() {
			return monthMax;
		}

		public void setMonthMax(float monthMax) {
			this.monthMax = monthMax;
		}

		public float getCustomMax() {
			return customMax;
		}

		public void setCustomMax(float customMax) {
			this.customMax = customMax;
		}

		public CartesianChartModel getHour() {
			return hour;
		}

		public void setHour(CartesianChartModel hour) {
			this.hour = hour;
		}

		public CartesianChartModel getDay() {
			return day;
		}

		public void setDay(CartesianChartModel day) {
			this.day = day;
		}

		public CartesianChartModel getWeek() {
			return week;
		}

		public void setWeek(CartesianChartModel week) {
			this.week = week;
		}

		public CartesianChartModel getMonth() {
			return month;
		}

		public void setMonth(CartesianChartModel month) {
			this.month = month;
		}

		public CartesianChartModel getCustom() {
			return custom;
		}

		public void setCustom(CartesianChartModel custom) {
			this.custom = custom;
		}

		public MonitoredHost getHost() {
			return host;
		}

		public void setHost(MonitoredHost host) {
			this.host = host;
		}
	}

	public StatisticsViewer() {
		cpuModel = new HostChartModel();
		memModel = new HostChartModel();
		diskModel = new HostChartModel();
		inetModel = new HostChartModel();
		createTreeTableModel();
		/*
		 * model = new DefaultDashboardModel(); DashboardColumn column1 = new
		 * DefaultDashboardColumn(); column1.addWidget("cpu");
		 * model.addColumn(column1);
		 */
		dummyModel = new CartesianChartModel();
		LineChartSeries dummySeries = new LineChartSeries();
		Map<Object, Number> dummyData = new FixedLengthLinkedHashMap<Object, Number>(
				10);
		dummyData.put(System.currentTimeMillis(), 50);
		dummyData.put(System.currentTimeMillis() + 500, 40);
		dummyData.put(System.currentTimeMillis() + 1000, 70);
		dummyStart = (Long) System.currentTimeMillis() - 5000;
		dummySeries.setData(dummyData);
		dummyModel.addSeries(dummySeries);

		// linecharts seem cannot be rendered in entitySelect or tabChange
		// initiating it for workaround
		selectedNodes = new TreeNode[1];
		if (entityRoot.getChildCount() != 0) {
			for (TreeNode node : entityRoot.getChildren()) {
				if (node.getChildCount() != 0) {
					selectedNodes[0] = node.getChildren().get(0);
					break;
				}
			}
		}
		searchFrom = new Date(System.currentTimeMillis() - ConstantUtil.HOUR_MS);
		searchTo = new Date();
		if (isNonEmptyHost()) {
			generateChart();
			searchStat();
		}
	}

	private void createTreeTableModel() {
		entityRoot = new DefaultTreeNode("root", null);

		for (MonitoredSystem sys : SmpClient.instance.getSystems()) {
			TreeNode sysNode = new DefaultTreeNode(sys, entityRoot);
			sysNode.setExpanded(true);
			for (MonitoredHost host : sys.getHosts()) {
				TreeNode hostNode = new DefaultTreeNode(host, sysNode);
				hostNode.setExpanded(true);
				/*
				 * for (MonitoredProcess proc : host.getProcesses()) {
				 * 
				 * @SuppressWarnings("unused") TreeNode procNode = new
				 * DefaultTreeNode(proc, hostNode); procNode.setExpanded(true);
				 * }
				 */
			}
		}
	}

	public void onEntitySelect(NodeSelectEvent event) {
		if (logger.isDebugEnabled())
			logger.debug("length of array:" + selectedNodes.length);
		generateChart();
		searchStat();
	}

	public void generateChart() {
		if (logger.isDebugEnabled()) {
			logger.debug("gen chart called");
		}
		clearModel();
		List<HostStats> stats;
		Set<MonitoredHost> hosts = getSelectedHostsFromNodes();

		try {
			if (logger.isDebugEnabled()) {
				logger.debug("number of hosts in chart:" + hosts.size());
			}
			for (MonitoredHost host : hosts) {
				Timestamp to = new Timestamp(System.currentTimeMillis());
				Timestamp from = new Timestamp(to.getTime()
						- ConstantUtil.MONTH_MS);
				hourStart = (Long) (to.getTime() - ConstantUtil.HOUR_MS - (ConstantUtil.MIN_MS * 3)); // left
																										// pad
																										// 3
																										// mins
				dayStart = (Long) (to.getTime() - ConstantUtil.DAY_MS - (ConstantUtil.HOUR_MS * 1)); // left
																										// pad
																										// 1
																										// hour
				weekStart = (Long) (to.getTime() - ConstantUtil.WEEK_MS - (ConstantUtil.HOUR_MS * 12)); // left
																										// pad
																										// 12
																										// hours
				monthStart = (Long) (to.getTime() - ConstantUtil.MONTH_MS - (ConstantUtil.DAY_MS * 3)); // left
																										// pad
																										// 3
																										// days
				stats = DataAccess.hostStatsDao.getHostStats(host.getSys()
						.getConfig().getName(), host.getConfig().getName(),
						from, to, null, null);
				logger.debug("host=" + host);
				logger.debug("stat size:" + stats.size());
				createModel(to, stats, host, IntervalType.HOUR);
				createModel(to, stats, host, IntervalType.DAY);
				createModel(to, stats, host, IntervalType.WEEK);
				createModel(to, stats, host, IntervalType.MONTH);
			}
		} catch (DataAccessException e) {

		}
		if (logger.isDebugEnabled()) {
			logger.debug("gen chart finished");
		}
	}

	private Set<MonitoredHost> getSelectedHostsFromNodes() {
		Set<MonitoredHost> hosts = new HashSet<MonitoredHost>();
		if (!isStatAvailable()) {
			selectedNodes = new TreeNode[1];
			if (entityRoot.getChildCount() != 0) {
				for (TreeNode node : entityRoot.getChildren()) {
					if (node.getChildCount() != 0) {
						selectedNodes[0] = node.getChildren().get(0);
						break;
					}
				}
			}
		}
		for (TreeNode treeNode : selectedNodes) {
			MonitoredEntity entity = (MonitoredEntity) treeNode.getData();
			switch (entity.getEntityType()) {
			case SYSTEM: {
				MonitoredSystem dummySys = (MonitoredSystem) entity;
				for (MonitoredHost dummyHost : dummySys.getHosts()) {
					hosts.add(dummyHost);
				}
			}
				break;
			case HOST: {
				MonitoredHost dummyHost = (MonitoredHost) entity;
				hosts.add(dummyHost);
			}
				break;
			case PROCESS: {

			}
				break;
			}
		}
		return hosts;
	}

	private void createModel(Timestamp to, List<HostStats> stat,
			MonitoredHost host, IntervalType type) {
		if (logger.isDebugEnabled()) {
			logger.debug("creating model for:" + host.getName() + " type:"
					+ type.name());
		}
		Timestamp from = null;
		switch (type) {
		case HOUR:
			from = new Timestamp(to.getTime() - ConstantUtil.HOUR_MS);
			break;
		case DAY:
			from = new Timestamp(to.getTime() - ConstantUtil.DAY_MS);
			break;
		case WEEK:
			from = new Timestamp(to.getTime() - ConstantUtil.WEEK_MS);
			break;
		case MONTH:
			from = new Timestamp(to.getTime() - ConstantUtil.MONTH_MS);
			break;
		case CUSTOM:
			from = new Timestamp(searchFrom.getTime());
			break;
		}
		Map<Object, Number> cpuStat = new HashMap<Object, Number>();
		Map<Object, Number> memStat = new HashMap<Object, Number>();
		Map<String, Map<Object, Number>> diskStat = new HashMap<String, Map<Object, Number>>();
		Map<String, Map<Object, Number>> inetInStat = new HashMap<String, Map<Object, Number>>();
		Map<String, Map<Object, Number>> inetOutStat = new HashMap<String, Map<Object, Number>>();
		logger.debug("host stat size=" + stat.size());
		for (HostStats hs : stat) {
			if (hs.getTs().compareTo(from) >= 0) {
				Long time = hs.getTs().getTime();
				for (CpuStats cpu : hs.getCpuStats()) {
					if (cpu.getProcessor().equals("all")) {
						cpuStat.put(time, cpu.getUsr());
					}
				}
				for (MemStats mem : hs.getMemStats()) {
					if (mem.getKey().getStatName().equals("mem")) {
						memStat.put(time, mem.getUsedPct());
					}
				}
				for (DiskStats disk : hs.getDiskStats()) {
					if (diskStat.get(disk.getFs()) == null) {
						diskStat.put(disk.getFs(),
								new HashMap<Object, Number>());
					}
					diskStat.get(disk.getFs()).put(time, disk.getUsedPercent());
				}
				for (InetStats inet : hs.getInetStats()) {
					if (inetInStat.get(inet.getKey().getDescr()) == null) {
						inetInStat.put(inet.getKey().getDescr(),
								new HashMap<Object, Number>());
					}
					if (inetOutStat.get(inet.getKey().getDescr()) == null) {
						inetOutStat.put(inet.getKey().getDescr(),
								new HashMap<Object, Number>());
					}
					inetInStat.get(inet.getKey().getDescr()).put(time,
							inet.getInOctet());
					inetOutStat.get(inet.getKey().getDescr()).put(time,
							inet.getOutOctet());
				}
			}
		}
		switch (type) {
		case HOUR:
			createSeries(cpuModel.hour, host.getName(), cpuStat);
			createSeries(memModel.hour, host.getName(), memStat);
			if (logger.isDebugEnabled()) {
				logger.debug("diskStat size:" + diskStat.keySet().size());
			}
			for (String label : diskStat.keySet()) {
				createSeries(diskModel.hour, host.getName() + ": " + label,
						diskStat.get(label));
			}
			for (String label : inetInStat.keySet()) {
				createSeries(inetModel.hour, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - InOctet", inetInStat.get(label));
			}
			for (String label : inetOutStat.keySet()) {
				createSeries(inetModel.hour, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - OutOctet", inetOutStat.get(label));
			}
			break;
		case DAY:
			createSeries(cpuModel.day, host.getName(), cpuStat);
			createSeries(memModel.day, host.getName(), memStat);
			if (logger.isDebugEnabled()) {
				logger.debug("diskStat size:" + diskStat.keySet().size());
			}
			for (String label : diskStat.keySet()) {
				createSeries(diskModel.day, host.getName() + ": " + label,
						diskStat.get(label));
			}
			for (String label : inetInStat.keySet()) {
				createSeries(inetModel.day, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - InOctet", inetInStat.get(label));
			}
			for (String label : inetOutStat.keySet()) {
				createSeries(inetModel.day, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - OutOctet", inetOutStat.get(label));
			}
			break;

		case WEEK:
			createSeries(cpuModel.week, host.getName(), cpuStat);
			createSeries(memModel.week, host.getName(), memStat);
			if (logger.isDebugEnabled()) {
				logger.debug("diskStat size:" + diskStat.keySet().size());
			}
			for (String label : diskStat.keySet()) {
				createSeries(diskModel.week, host.getName() + ": " + label,
						diskStat.get(label));
			}
			for (String label : inetInStat.keySet()) {
				createSeries(inetModel.week, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - InOctet", inetInStat.get(label));
			}
			for (String label : inetOutStat.keySet()) {
				createSeries(inetModel.week, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - OutOctet", inetOutStat.get(label));
			}
			break;

		case MONTH:
			createSeries(cpuModel.month, host.getName(), cpuStat);
			createSeries(memModel.month, host.getName(), memStat);
			if (logger.isDebugEnabled()) {
				logger.debug("diskStat size:" + diskStat.keySet().size());
			}
			for (String label : diskStat.keySet()) {
				createSeries(diskModel.month, host.getName() + ": " + label,
						diskStat.get(label));
			}
			for (String label : inetInStat.keySet()) {
				createSeries(inetModel.month, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - InOctet", inetInStat.get(label));
			}
			for (String label : inetOutStat.keySet()) {
				createSeries(inetModel.month, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - OutOctet", inetOutStat.get(label));
			}
			break;
		case CUSTOM:
			createSeries(cpuModel.custom, host.getName(), cpuStat);
			createSeries(memModel.custom, host.getName(), memStat);
			for (String label : diskStat.keySet()) {
				createSeries(diskModel.custom, host.getName() + ": " + label,
						diskStat.get(label));
			}
			for (String label : inetInStat.keySet()) {
				createSeries(inetModel.custom, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - InOctet", inetInStat.get(label));
			}
			for (String label : inetOutStat.keySet()) {
				createSeries(inetModel.custom, host.getName() + ": "
						+ host.getConfig().getInetDisplayName(label)
						+ " - OutOctet", inetOutStat.get(label));
			}
			break;
		}

	}

	public boolean isHostSelected() {
		return (selectedNodes != null && selectedNodes.length != 0)
				&& searchTo != null && searchFrom != null;
	}

	public void searchStat() {
		cpuModel.custom.clear();
		memModel.custom.clear();
		diskModel.custom.clear();
		inetModel.custom.clear();
		if (logger.isDebugEnabled()) {
			logger.debug("searching stat...");
		}
		List<HostStats> customStat;
		Set<MonitoredHost> hosts = getSelectedHostsFromNodes();

		Timestamp to = new Timestamp(searchTo.getTime());
		Timestamp from = new Timestamp(searchFrom.getTime());
		Long padLeft = (long) ((to.getTime() - from.getTime()) * 0.1);
		customStart = (Long) (from.getTime() - padLeft);
		try {
			for (MonitoredHost host : hosts) {
				customStat = DataAccess.hostStatsDao.getHostStats(host.getSys()
						.getConfig().getName(), host.getConfig().getName(),
						from, to, null, null);
				createModel(to, customStat, host, IntervalType.CUSTOM);
			}

			this.sliderNumber = 4;
		} catch (DataAccessException e) {
			logger.error("Failed to search statistics", e);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("search stat completed");
		}
	}

	public void onDateSelect(SelectEvent event) {

	}

	public Long getCustomStart() {
		return customStart;
	}

	public void setCustomStart(Long customStart) {
		this.customStart = customStart;
	}

	public void setHourStart(Long hourStart) {
		this.hourStart = hourStart;
	}

	public void setDayStart(Long dayStart) {
		this.dayStart = dayStart;
	}

	public void setWeekStart(Long weekStart) {
		this.weekStart = weekStart;
	}

	public void setMonthStart(Long monthStart) {
		this.monthStart = monthStart;
	}

	private void createSeries(CartesianChartModel chartModel, String label,
			Map<Object, Number> map) {
		LineChartSeries hostSeries = new LineChartSeries();
		hostSeries.setLabel(label);

		if (map != null && map.isEmpty()) {
			hostSeries.set(System.currentTimeMillis(), 0.0);
		} else {
			hostSeries.setData(map);
		}

		chartModel.addSeries(hostSeries);
	}

	private void clearModel() {
		cpuModel.hour.clear();
		cpuModel.day.clear();
		cpuModel.week.clear();
		cpuModel.month.clear();
		memModel.hour.clear();
		memModel.day.clear();
		memModel.week.clear();
		memModel.month.clear();
		diskModel.hour.clear();
		diskModel.day.clear();
		diskModel.week.clear();
		diskModel.month.clear();
		inetModel.hour.clear();
		inetModel.day.clear();
		inetModel.week.clear();
		inetModel.month.clear();
	}

	public static enum IntervalType {
		HOUR, DAY, WEEK, MONTH, CUSTOM
	}

	public boolean isStatAvailable() {
		return (selectedNodes != null && selectedNodes.length != 0);
	}

	public void onTabChange(TabChangeEvent event) {
		// generateChart();
	}

	public void onEntitySelect(MonitoredEntity selectedEntity) {
		if (logger.isDebugEnabled())
			logger.debug("length of array:" + selectedNodes.length);
		generateChart();
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public StreamedContent getFile() {
		logger.debug("get file called");
		StringBuilder sb = new StringBuilder();
		String nowString = StringUtil.convertTimeFormatForExport(new Date());
		sb.append(AppConfig.getHomeFilepath())
				.append(AppConfig.getStatReportPrefix()).append(nowString)
				.append(".xls");
		String filename = PropertiesUtil.parsePropertiesValue(sb.toString());
		StringBuilder downloadFilename = new StringBuilder();
		downloadFilename.append(AppConfig.getStatReportPrefix())
				.append(nowString).append(".xls");
		logger.debug("filename:" + filename);
		Timestamp from = null;
		Timestamp to = new Timestamp(System.currentTimeMillis());
		logger.debug("sliderNumber:" + getSliderText());
		switch (sliderNumber) {
		case 0:
			from = new Timestamp(System.currentTimeMillis()
					- ConstantUtil.HOUR_MS);
			break;
		case 1:
			from = new Timestamp(System.currentTimeMillis()
					- ConstantUtil.DAY_MS);
			break;
		case 2:
			from = new Timestamp(System.currentTimeMillis()
					- ConstantUtil.WEEK_MS);
			break;
		case 3:
			from = new Timestamp(System.currentTimeMillis()
					- ConstantUtil.MONTH_MS);
			break;
		case 4:
			from = new Timestamp(searchFrom.getTime());
			to = new Timestamp(searchTo.getTime());
			break;
		default:
			from = new Timestamp(System.currentTimeMillis()
					- ConstantUtil.HOUR_MS);
			break;
		}
		Set<MonitoredHost> set = getSelectedHostsFromNodes();

		logger.debug("ready to export");
		DataExporter.getInstance().exportStats(filename, from, to, set);
		logger.debug("ready to open input stream");
		InputStream stream = null;
		try {
			stream = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
			logger.warn("file not found:" + e.getMessage());
		}
		StreamedContent file = new DefaultStreamedContent(stream,
				"application/vnd.ms-excel	", downloadFilename.toString());

		// Primefaces should close the stream automatically

		// try {
		// stream.close();
		// } catch (IOException e) {
		// logger.warn("IO exception:"+e.getMessage());
		// }

		Thread deleteTask = new FileDeleteTask(
				PropertiesUtil.parsePropertiesValue(filename));
		deleteTask.start();

		return file;
	}

	public String getStatDlgHeader() {
		StringBuilder sb = new StringBuilder();
		sb.append("Statistics").append(" from ")
				.append(StringUtil.convertTimeFormat(searchFrom))
				.append(" to ").append(StringUtil.convertTimeFormat(searchTo));

		return sb.toString();
	}

	public int getActiveTab() {
		return activeTab;
	}

	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date searchFrom) {
		this.searchFrom = searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date searchTo) {
		this.searchTo = searchTo;
	}

	public HostChartModel getCpuModel() {
		return cpuModel;
	}

	public HostChartModel getMemModel() {
		return memModel;
	}

	public HostChartModel getDiskModel() {
		return diskModel;
	}

	public HostChartModel getInetModel() {
		return inetModel;
	}

	public Long getHourStart() {
		return hourStart;
	}

	public CartesianChartModel getDummyModel() {
		return dummyModel;
	}

	public Long getDummyStart() {
		return dummyStart;
	}

	public void setDummyStart(Long dummyStart) {
		this.dummyStart = dummyStart;
	}

	public DashboardModel getModel() {
		return model;
	}

	public TreeNode[] getSelectedNodes() {
		return selectedNodes;
	}

	public void setSelectedNodes(TreeNode[] selectedNodes) {
		this.selectedNodes = selectedNodes;
	}

	public TreeNode getEntityRoot() {
		return entityRoot;
	}

	public Long getDayStart() {
		return dayStart;
	}

	public Long getWeekStart() {
		return weekStart;
	}

	public Long getMonthStart() {
		return monthStart;
	}

	public boolean isDiskModelDisplayable() {
		return diskModel.getDay().getSeries() != null
				&& diskModel.getDay().getSeries().size() != 0;
	}

	public boolean isInetModelDisplayable() {
		return inetModel.getDay().getSeries() != null
				&& inetModel.getDay().getSeries().size() != 0;
	}

	public boolean isCustomDiskModelDisplayable() {
		return diskModel.getCustom().getSeries() != null
				&& diskModel.getCustom().getSeries().size() != 0;
	}

	public boolean isCustomInetModelDisplayable() {
		return inetModel.getCustom().getSeries() != null
				&& inetModel.getCustom().getSeries().size() != 0;
	}

	public int getSliderNumber() {
		return sliderNumber;
	}

	public void setSliderNumber(int sliderNumber) {
		this.sliderNumber = sliderNumber;
	}

	public String getSliderText() {
		switch (sliderNumber) {
		case 0:
			return "Hour";
		case 1:
			return "Day";
		case 2:
			return "Week";
		case 3:
			return "Month";
		case 4:
			return "Custom";
		default:
			return "Hour";
		}
	}

	public boolean isNonEmptyHost() {
		if (entityRoot.getChildCount() != 0) {
			for (TreeNode node : entityRoot.getChildren()) {
				if (node.getChildCount() != 0) {
					return true;
				}
			}
		}
		return false;
	}

}
