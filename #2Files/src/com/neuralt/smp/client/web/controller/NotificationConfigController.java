package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.data.ContactPerson;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.GroupSnmpConfig;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.data.NotificationGroup.EventType;
import com.neuralt.smp.client.data.NotificationGroup.UnattendedTrigger;
import com.neuralt.smp.client.data.config.AlarmDefinition;
import com.neuralt.smp.client.util.OidUtil;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class NotificationConfigController implements Serializable {
	private static final long serialVersionUID = 2121044473063523193L;
	private static final Logger logger = LoggerFactory
			.getLogger(NotificationConfigController.class);
	private static final int PRODUCT_OID_LENGTH = 12;
	private static final String snmpConfigError = "SNMP Config Error";

	private TreeNode entityRoot;
	private TreeNode selectedNode;
	private TreeNode notiGroupEntityRoot;
	private TreeNode notiGroupSelectedNode;

	private int activeTab = 0;
	private long day;
	private long hour;
	private long minute;

	private NotificationGroup newGroup;
	private NotificationGroup group;
	private ContactPerson newContact;
	private ContactPerson contact;
	private GroupSnmpConfig newSnmp;
	private GroupSnmpConfig snmp;
	private List<NotificationGroup> list;

	private List<String> contactList;
	private List<String> snmpConfigList;
	private String newContactName;
	private String newSnmpName;

	public NotificationConfigController() {
		// createTreeTableModel();
		// if (entityRoot.getChildCount()>0)
		// selectedNode = entityRoot.getChildren().get(0);
		createEventTreeTableModel();
		list = DataAccess.notificationGroupDao.getAllGroups();
		contactList = new ArrayList<String>();
		snmpConfigList = new ArrayList<String>();

		reset();
	}

	public void addNewContact() {
		newContact = DataAccess.contactPersonDao
				.getContactPersonByName(newContactName);
		if (newContact != null) {
			group.getContacts().add(newContact);
		} else {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, "Contact Person "
							+ newContactName + " not found");
		}
		resetRecipientLists();
	}

	public void deleteContact(ContactPerson contact) {
		group.getContacts().remove(contact);
		resetRecipientLists();
	}

	public void addNewSnmp() {
		newSnmp = DataAccess.groupSnmpConfigDao
				.getGroupSnmpConfigByName(newSnmpName);
		if (newSnmp != null) {
			group.getSnmpConfigs().add(newSnmp);
		} else {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, "SNMP Config " + newSnmpName
							+ " not found");
		}
		resetRecipientLists();
	}

	public void deleteSnmp(GroupSnmpConfig snmp) {
		group.getSnmpConfigs().remove(snmp);
		resetRecipientLists();
	}

	// private void createTreeTableModel() {
	// entityRoot = new DefaultTreeNode("root", null);
	// entityRoot.setExpanded(true);
	// for (ProductGroup group :
	// DataAccess.productGroupDao.getAllProductGroups()) {
	// if (logger.isDebugEnabled())
	// logger.debug("oid:"+group.getOid()+" name:"+group.getName());
	//
	// TreeNode trapNode = new DefaultTreeNode(group, entityRoot);
	// trapNode.setExpanded(true);
	// }
	// }
	private void createEventTreeTableModel() {
		List<NotificationGroup> temp = DataAccess.notificationGroupDao
				.getGroups(EventType.NEW);
		notiGroupEntityRoot = new DefaultTreeNode("root", null);
		notiGroupEntityRoot.setExpanded(true);
		NotificationGroup dummyOutstanding = new NotificationGroup();
		dummyOutstanding.setType(EventType.NEW);
		dummyOutstanding.setName("");
		dummyOutstanding.generateDualList();
		TreeNode outstandingRootNode = new DefaultTreeNode(dummyOutstanding,
				notiGroupEntityRoot);
		outstandingRootNode.setExpanded(true);
		for (NotificationGroup gp : temp) {
			TreeNode groupNode = new DefaultTreeNode(gp, outstandingRootNode);
			groupNode.setExpanded(true);
		}
		List<NotificationGroup> unattendedTemp = DataAccess.notificationGroupDao
				.getGroups(EventType.UNATTENDED);
		NotificationGroup dummyUnattended = new NotificationGroup();
		dummyUnattended.setType(EventType.UNATTENDED);
		dummyUnattended.setName("");
		dummyUnattended.generateDualList();
		TreeNode unattendedRootNode = new DefaultTreeNode(dummyUnattended,
				notiGroupEntityRoot);
		unattendedRootNode.setExpanded(true);
		for (NotificationGroup gp : unattendedTemp) {
			TreeNode groupNode = new DefaultTreeNode(gp, unattendedRootNode);
			groupNode.setExpanded(true);
		}
	}

	public void onNotiGroupEntitySelect(NodeSelectEvent event) {
		TreeNode entityNode = event.getTreeNode();
		group = (NotificationGroup) entityNode.getData();
		group.generateDualList();
		if (group.getType() == EventType.UNATTENDED) {
			if (group.getTime() != null) {
				long time = group.getTime();
				day = time / ConstantUtil.DAY_MS;
				time = time % ConstantUtil.DAY_MS;
				hour = time / ConstantUtil.HOUR_MS;
				time = time % ConstantUtil.HOUR_MS;
				minute = time / ConstantUtil.MIN_MS;
			} else {
				day = 0;
				hour = 0;
				minute = 0;
			}
		}
		resetRecipientLists();
	}

	private void reset() {
		group = null;
		newContact = new ContactPerson();
		newSnmp = new GroupSnmpConfig();
	}

	public void saveGroup() {
		// checking
		boolean ok = true;

		if (ok) {
			if (group.getAlarmsAsList() == null) {
				group.setAlarms(new TreeSet<AlarmDefinition>());
			}
			group.getAlarms().clear();
			logger.debug("alarmDef target size="
					+ group.getDualList().getTarget().size());
			for (AlarmDefinition def : group.getDualList().getTarget()) {
				logger.debug("alarmDef [{}] is in target", def);
				if (!group.getAlarms().contains(def)) {
					group.getAlarms().add(def);
					logger.debug("alarmDef [{}] is added", def);
				}
			}
			if (group.getType() == EventType.UNATTENDED) {
				group.setTime(day * ConstantUtil.DAY_MS + hour
						* ConstantUtil.HOUR_MS + minute * ConstantUtil.MIN_MS);
			}
			DataAccess.notificationGroupDao.save(group);
			createEventTreeTableModel();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS,
					"[Notification Group] " + group.getName() + " is saved");
			if (group.getId() == 0) {
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
						"add-notification group", group.getName());
			} else {
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
						"edit-notification group", group.getName() + ", id="
								+ group.getId());
			}
		}
		reset();
	}

	public void deleteGroup() {
		if (group != null && group.getId() != 0) {
			String name = group.getName();
			group.setAlarms(null);
			group.setContacts(null);
			group.setSnmpConfigs(null);
			DataAccess.notificationGroupDao.delete(group);
			createEventTreeTableModel();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, "[Notification Group] " + name
							+ " is deleted");
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-notification group", name);
			reset();
		}
	}

	// private boolean isSnmpConfigValid(GroupSnmpConfig config) {
	// boolean ok = true;
	// switch (config.getVersion()) {
	// case SNMPV1 : {
	// if (StringUtil.isNullOrEmpty(config.getCommunityTrap())) {
	// WebUtil.addMessage(MessageKey.GENERAL_ERROR,
	// MessageKey.SNMP_COMMUNITY_TRAP, FacesMessage.SEVERITY_WARN);
	// ok = false;
	// }
	// break;
	// }
	// case SNMPV2 : {
	// if (StringUtil.isNullOrEmpty(config.getCommunityTrap())) {
	// WebUtil.addMessage(MessageKey.GENERAL_ERROR,
	// MessageKey.SNMP_COMMUNITY_TRAP, FacesMessage.SEVERITY_WARN);
	// ok = false;
	// }
	// break;
	// }
	// case SNMPV3 : {
	// if (StringUtil.isNullOrEmpty(config.getUsername())) {
	// WebUtil.addMessage(MessageKey.GENERAL_ERROR, MessageKey.SNMP_USERNAME,
	// FacesMessage.SEVERITY_WARN);
	// ok = false;
	// }
	// if (StringUtil.isNullOrEmpty(config.getEngineId())) {
	// WebUtil.addMessage(MessageKey.GENERAL_ERROR, MessageKey.SNMP_ENGINEID,
	// FacesMessage.SEVERITY_WARN);
	// ok = false;
	// }
	// if (config.getAuth()!=AuthProtocol.NONE) {
	// if (!StringUtil.isLengthOk(config.getAuthPassword(),
	// AppConfig.getSnmpPasswordMinLength())) {
	// WebUtil.addMessage(MessageKey.GENERAL_ERROR,
	// "Password should be at least "+AppConfig.getSnmpPasswordMinLength()+" digits",
	// FacesMessage.SEVERITY_WARN);
	// ok = false;
	// } else if (config.getPriv()!=PrivProtocol.NONE) {
	// if (!StringUtil.isLengthOk(config.getPrivPassword(),
	// AppConfig.getSnmpPasswordMinLength())) {
	// WebUtil.addMessage(MessageKey.GENERAL_ERROR,
	// "Password should be at least "+AppConfig.getSnmpPasswordMinLength()+" digits",
	// FacesMessage.SEVERITY_WARN);
	// ok = false;
	// }
	// }
	// }
	// break;
	// }
	// }
	// return ok;
	// }

	private void resetRecipientLists() {
		if (group != null) {
			resetContactPersonList();
			resetGroupSnmpConfigList();
		}
	}

	private void resetGroupSnmpConfigList() {
		List<GroupSnmpConfig> allSnmps = DataAccess.groupSnmpConfigDao
				.getAllGroupSnmpConfigs();
		if (group.getSnmpConfigs() != null) {
			for (GroupSnmpConfig snmp : group.getSnmpConfigs()) {
				allSnmps.remove(snmp);
			}
		}
		snmpConfigList.clear();
		for (GroupSnmpConfig snmp : allSnmps) {
			snmpConfigList.add(snmp.getName());
		}
	}

	private void resetContactPersonList() {
		List<ContactPerson> allContacts = DataAccess.contactPersonDao
				.getAllContactPersons();
		if (group.getContacts() != null) {
			for (ContactPerson contact : group.getContacts()) {
				allContacts.remove(contact);
			}
		}
		contactList.clear();
		for (ContactPerson contact : allContacts) {
			contactList.add(contact.getName());
		}
	}

	public boolean isRenderDummyTrap() {
		if (group.getLabel().equals(""))
			return false;
		return false;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public TreeNode getEntityRoot() {
		return entityRoot;
	}

	public NotificationGroup getGroup() {
		return group;
	}

	public void setGroup(NotificationGroup group) {
		this.group = group;
	}

	public TreeNode getNotiGroupEntityRoot() {
		return notiGroupEntityRoot;
	}

	public TreeNode getNotiGroupSelectedNode() {
		return notiGroupSelectedNode;
	}

	public int getActiveTab() {
		return activeTab;
	}

	public void setActiveTab(int activeTab) {
		this.activeTab = activeTab;
	}

	public List<NotificationGroup> getList() {
		return list;
	}

	public ContactPerson getNewContact() {
		return newContact;
	}

	public void setNewContact(ContactPerson newContact) {
		this.newContact = newContact;
	}

	public GroupSnmpConfig getNewSnmp() {
		return newSnmp;
	}

	public void setNewSnmp(GroupSnmpConfig newSnmp) {
		this.newSnmp = newSnmp;
	}

	public long getDay() {
		return day;
	}

	public void setDay(long day) {
		this.day = day;
	}

	public long getHour() {
		return hour;
	}

	public void setHour(long hour) {
		this.hour = hour;
	}

	public long getMinute() {
		return minute;
	}

	public void setMinute(long minute) {
		this.minute = minute;
	}

	public String getOidAsTrapName(OID oid) {
		return OidUtil.getInstance().mapOidToTrapName(oid);
	}

	public List<String> getWarningLevelList() {
		List<String> list = new ArrayList<String>();
		list.add("(ANY)");
		for (WarningLevel level : WarningLevel.values()) {
			list.add(level.name());
		}
		return list;
	}

	public List<UnattendedTrigger> getUnattendedTriggerList() {
		List<UnattendedTrigger> list = new ArrayList<UnattendedTrigger>();
		for (UnattendedTrigger trigger : UnattendedTrigger.values()) {
			list.add(trigger);
		}
		return list;
	}

	public String getUnattendedTriggerString(UnattendedTrigger trigger) {
		switch (trigger) {
		case TIME:
			return "Period";
		case OCCURENCE:
			return "Occurence";
		case EITHER:
			return "Either period or occurence";
		case BOTH:
			return "Both period and occurence";
		default:
			return "";
		}
	}

	public boolean isNewGroupSelected() {
		return StringUtil.isNullOrEmpty(newGroup.getName()) ? false : true;
	}

	public boolean isGroupSelected() {
		return group != null;
	}

	public void setNotiGroupSelectedNode(TreeNode notiGroupSelectedNode) {
		this.notiGroupSelectedNode = notiGroupSelectedNode;
	}

	public List<String> getContactList() {
		return contactList;
	}

	public List<String> getSnmpConfigList() {
		return snmpConfigList;
	}

	public String getNewContactName() {
		return newContactName;
	}

	public void setNewContactName(String newContactName) {
		this.newContactName = newContactName;
	}

	public String getNewSnmpName() {
		return newSnmpName;
	}

	public void setNewSnmpName(String newSnmpName) {
		this.newSnmpName = newSnmpName;
	}
}
