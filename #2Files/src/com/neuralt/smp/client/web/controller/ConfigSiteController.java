package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.util.ModelDataTable;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.Site;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class ConfigSiteController implements ReloadableController, Serializable{
	
	private static final long serialVersionUID = 1L;

	private static final Logger log = LoggerFactory.getLogger(ConfigSiteController.class);
	
	private final ModelDataTable<Site> model = new ModelDataTable<Site>(
			Site.class,
			"siteName like :"+ModelDataTable.NAME,
			ModelDataTable.NAME, "%"
		);
	
	private List<Site> sites = new ArrayList<Site>();
	private Site site = new Site();
	
	private List<SelectItem> selectItems = new ArrayList<SelectItem>();
	
	@PostConstruct
	public void init() {
		reload();
	}
	
	public ModelDataTable<Site> getModel() {
		return model;
	}

	public List<Site> getSites() {
		return sites;
	}
	public void setSites(List<Site> sites) {
		this.sites = sites;
	}

	public Site getSite() {
		return site;
	}
	public void setSite(Site site) {
		this.site = (site == null ? new Site() : site);
	}

	public boolean isSitesEmpty() {
		return sites == null || sites.isEmpty();
	}
	
	public List<SelectItem> getSelectItems() {
		return selectItems;
	}
	public void setSelectItems(List<SelectItem> selectItems) {
		this.selectItems = selectItems;
	}
	
	public List<Site> getAllSites(){
		return DataAccess.siteDao.getAllSites();
	}
	
	public List<SelectItem> getAllSelectItems(boolean withAll){
		List<Site> sites = getAllSites(); 		 
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		if(withAll){
			selectItems.add(new SelectItem("All", "All"));
		}
	    for(Site site: sites){
	    	selectItems.add(new SelectItem(site.getSiteName(), site.getSiteName()));
	    }
	    return selectItems;
	}
	public List<SelectItem> getAllSelectItems(){
		return getAllSelectItems(false);
	}

	@Override
	public void reload() {
		site = new Site();
		selectItems = getAllSelectItems(true);
		    
		Site selected = model.getSelected(true);
		if(selected != null){
			sites = new ArrayList<Site>();		
			sites.add(selected);
		}else{
			sites = getAllSites();
		}
	}
	
	public void select(Site site){
		model.setParamName(site.getSiteName());
	}

	public void add() {
		boolean ok = true;

		Site dummy = DataAccess.siteDao.getSiteByName(site.getSiteName());
		if (dummy != null) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.SITE_DUPLICATED);
		}

		if (ok) {
			DataAccess.siteDao.save(site);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-site", site.toString());
			reload();
		}
	}
	
	public void delete(){
		delete(site);
	}

	public void delete(Site site) {
		boolean ok = true;
		if (site.getHostCount() != 0) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.SITE_DELETE_FAILED_HAVEHOST);
		}
		if (ok) {
			DataAccess.siteDao.delete(site);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-site", site.toString());
			reload();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, site.getSiteName() + " "
							+ "is deleted");
		}
	}

	public void edit() {
		log.debug("site=" + site);
		boolean ok = true;

		Site dummy = DataAccess.siteDao.getSiteByName(site.getSiteName());

		if (dummy != null && !dummy.equals(site)) {
			ok = false;
			site = new Site();
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.SITE_DUPLICATED);
		}

		if (ok) {
			DataAccess.siteDao.merge(site);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-site", site.toString());
			reload();
		}
	}

}
