package com.neuralt.smp.client.web.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.web.security.UserBean;
import com.neuralt.smp.client.web.security.UserGroup;
import com.neuralt.smp.client.web.security.UserRole;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.web.util.jsf.LocaleMessages;
import com.neuralt.web.util.jsf.MessageUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class UserGroupController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(UserGroupController.class);

	@ManagedProperty(value = "#{UserBean}")
	private UserBean userBean;

	private List<UserGroup> userGroups;
	private List<UserRole> selectedRoles; // roles current group has

	private List<UserRole> notSelectedRoles; // roles current group does not
												// have
	private Map<String, UserRole> notSelectedRolesStringMap; // string to
																// userrole
																// mapping of
																// roles current
																// group does
																// not have
	private List<String> notSelectedRolesNames; // names of roles current group
												// does not have

	private String selectedRoleName;
	private UserGroup selectedGroup;
	private UserRole selectedRole;
	private String selectedGroupId;
	// private UserGroup prev;

	private boolean createNew;
	private boolean showAddDialog;

	public UserGroupController() {
		init();
	}

	private void init() {
		setUserGroups(DataAccess.webDao.getUserGroups());
	}

	// actions //

	public void onAddGroupClick() {
		try {
			FacesContext.getCurrentInstance().getExternalContext()
					.redirect("role.xhtml?showAddDialog=true");
		} catch (IOException e) {
			logger.error("failed to redirect", e);
		}
	}

	// public void selectGroup(UserGroup group) {
	// selectedGroup = group;
	// logger.debug("selectedGroup: {}",selectedGroup);
	// }

	public void addGroup() {
		logger.debug("addGroup");
		selectedGroup = new UserGroup();
		logger.debug("selectedGroup: {}", selectedGroup);
	}

	public void deleteGroup() {
		logger.debug("selectedGroup: {}", selectedGroup);
		if (selectedGroup.getGroupId().equals(ConstantUtil.SUPER_GROUP)) {
			// NOT ALLOWED to delete super user group
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.GROUP_MODIFY_FAILED_SUPERGROUP);
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE, ActionType.DELETE, false, "UserGroup",
			// selectedGroup.getGroupId(), message, null, null);
			refreshGroups();
			return;
		} else {
			// delete a group will cause deleting all user in the group
			try {
				DataAccess.webDao.removeUserGroup(selectedGroup);
				AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
						WebUtil.getRequestPath(), ActionType.DELETE,
						"delete-user group", selectedGroup.getGroupId());
			} catch (Exception e) {
				String message = addErrorMsg("privilege.groupDeleteFailure",
						MessageUtil.getDescriptionOfDBException(e), new Date());
				// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
				// ActionCategory.PRIVILEGE, ActionType.DELETE, false,
				// "UserGroup",
				// selectedGroup.getGroupId(), message, null, null);
				return;
			} finally {
				refreshGroups();
			}
			addInfoMsg("privilege.groupDeleteSuccess");
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE, ActionType.DELETE, true, "UserGroup",
			// selectedGroup.getGroupId(), null, null, null);
			selectedGroup = null;
			selectedRoles = null;
		}
	}

	public void saveGroup() {
		if (selectedGroup.getGroupId().equals(ConstantUtil.SUPER_GROUP)) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.GROUP_MODIFY_FAILED_SUPERGROUP);
			selectedGroup = null;
			return;
		}
		if (failsValidation(selectedGroup)) {
			return;
		}
		// store the previous group in order to log changes
		// if (!createNew)
		// prev = DataAccess.webDao.getUserGroup(selectedGroup.getGroupId());

		selectedGroup.setLastUpdatedBy(userBean.getUser() != null ? userBean
				.getUser().getUserId() : null);
		try {
			DataAccess.webDao.saveUserGroup(selectedGroup);
		} catch (Exception e) {
			String message = addErrorMsg("privilege.groupSaveFailure",
					MessageUtil.getDescriptionOfDBException(e), new Date());
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE,
			// createNew?ActionType.CREATE:ActionType.UPDATE,
			// false, "UserGroup", createNew?null:selectedGroup.getGroupId(),
			// message, null, null);
			return;
		} finally {
			refreshGroups();
		}
		// find differences between the previous and updated user group
		// String[] differences = {"",""};
		// if (!createNew) {
		// DiffComparator diff = new DiffComparator();
		// differences = diff.compare("UserGroup",prev, selectedGroup);
		// }
		addInfoMsg("privilege.groupSaveSuccess");
		if (createNew) {
			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					WebUtil.getRequestPath(), ActionType.INSERT,
					"add-user group", selectedGroup.getGroupId());
		} else {
			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					WebUtil.getRequestPath(), ActionType.UPDATE,
					"edit-user group", selectedGroup.getGroupId());
		}
		// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
		// ActionCategory.PRIVILEGE,
		// createNew?ActionType.CREATE:ActionType.UPDATE,
		// true, "UserGroup", selectedGroup.getGroupId(), null,
		// differences[0],differences[1]);
		selectedGroup = null;
		selectedRoles = null;
	}

	public void selectRole(UserRole role) {
		selectedRole = role;
		setNotSelectedRoles(DataAccess.webDao.getUserRoles());
	}

	public void addRoleToGroup() {
		if (selectedGroup.getGroupId().equals(ConstantUtil.SUPER_GROUP)) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.GROUP_MODIFY_FAILED_SUPERGROUP);
			return;
		}
		selectedRole = getNotSelectedRolesStringMap().get(selectedRoleName);
		// Session session = HibernateUtil.getCurrentSession();
		// session.beginTransaction();
		try {
			// logger.debug("addrole, before: {}",selectedGroup.getLastUpdatedOn());
			// new UserGroupDao().makePersistent(selectedGroup);
			// logger.debug("Role to added to group: {} => {}",selectedRole,
			// selectedGroup);
			selectedGroup.addUserRole(selectedRole);

			DataAccess.webDao.saveUserGroup(selectedGroup);
			// session.getTransaction().commit();
			logger.debug("addrole, after: {}", selectedGroup.getLastUpdatedOn());

			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					WebUtil.getRequestPath(), ActionType.UPDATE,
					"edit-user group", selectedGroup.getGroupId());
		} catch (Throwable e) {
			// session.getTransaction().rollback();
			String message = addErrorMsg("privilege.groupAddRoleFailure",
					MessageUtil.getDescriptionOfDBException(new Exception(e)),
					new Date());
			// TODO better log format for add role
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE, ActionType.UPDATE, false, "UserGroup",
			// selectedGroup.getGroupId(), message, null,null);
			return;
		}

		refreshGroups();
		refreshRoles();
		addInfoMsg("privilege.groupAddRole");
		// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
		// ActionCategory.PRIVILEGE, ActionType.UPDATE, true, "UserGroup",
		// selectedGroup.getGroupId(), null,
		// "","UserRole(id = "+selectedRole.getRoleId()+") Added");
	}

	public void deleteRoleFromGroup() {
		if (selectedGroup.getGroupId().equals(ConstantUtil.SUPER_GROUP)) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.GROUP_MODIFY_FAILED_SUPERGROUP);
			return;
		}
		selectedGroup.removeUserRole(selectedRole);
		// selectedRole.removeUserGroup(selectedGroup);
		try {
			DataAccess.webDao.saveUserGroup(selectedGroup);
			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					WebUtil.getRequestPath(), ActionType.UPDATE,
					"edit-user group", selectedGroup.getGroupId());
		} catch (Exception e) {
			String message = addErrorMsg("privilege.groupDeleteRoleFailure",
					MessageUtil.getDescriptionOfDBException(new Exception(e)),
					new Date());
			// TODO better format for delete role
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE, ActionType.UPDATE, false, "UserGroup",
			// selectedGroup.getGroupId(), message, null, null);
			return;
		}
		refreshGroups();
		refreshRoles();
		addInfoMsg("privilege.groupDeleteRole");
		// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
		// ActionCategory.PRIVILEGE, ActionType.DELETE, true, "UserGroup",
		// selectedGroup.getGroupId(), null,
		// "","UserRole(id = "+selectedRole.getRoleId()+") removed");
	}

	public void refreshGroups() {
		setUserGroups(DataAccess.webDao.getUserGroups());
		setNotSelectedRoles(DataAccess.webDao.getUserRoles());
	}

	public void refreshRoles() {
		// Session session = HibernateUtil.getCurrentSession();
		// session.beginTransaction();
		try {
			// DataAccess.webDao.saveUserGroup(selectedGroup);
			selectedRoles = new ArrayList<UserRole>(
					selectedGroup.getUserRoles());
			// session.getTransaction().commit();
		} catch (Throwable e) {
			// session.getTransaction().rollback();
			// addErrorMsg("error_selecting_roles");
			logger.error("refreshRoles", e);
		}
	}

	private String addInfoMsg(String msgKey) {
		FacesMessage message = LocaleMessages.getMessage(msgKey);
		message.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return message.getSummary();
	}

	private String addErrorMsg(String msgKey) {
		FacesMessage message = LocaleMessages.getMessage(msgKey);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return message.getSummary();
	}

	private String addErrorMsg(String msgKey, String e, Date date) {
		FacesMessage message = LocaleMessages.getMessage(msgKey, e, date);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return message.getSummary();
	}

	// validation //

	public boolean failsValidation(UserGroup group) {
		return groupIdExistsAlready(group);
	}

	public boolean groupIdExistsAlready(UserGroup newGroup) {
		for (UserGroup group : userGroups) {
			if (group.getGroupId().equals(newGroup.getGroupId())
					&& group != newGroup) {
				String message = addErrorMsg("privilege.groupExistsAlready");
				// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
				// ActionCategory.PRIVILEGE, ActionType.CREATE, false,
				// "UserGroup",
				// selectedGroup.getGroupId(), message, null, null);
				return true; // groupid exists already, bad
			}
		}
		return false; // groupid doesnt exist yet, ok
	}

	// getters setters //

	public List<UserRole> getSelectedRoles() {
		// Session session = HibernateUtil.getCurrentSession();
		// session.beginTransaction();
		try {
			// DataAccess.webDao.saveUserGroup(selectedGroup);
			if (selectedGroup == null || selectedGroup.getUserRoles() == null)
				selectedRoles = new ArrayList<UserRole>();
			else
				selectedRoles = new ArrayList<UserRole>(
						selectedGroup.getUserRoles());
			// session.getTransaction().commit();
		} catch (Throwable e) {
			// session.getTransaction().rollback();
			// addErrorMsg("error_selecting_roles");
			logger.error("getSelectedRoles", e);
			return selectedRoles;
		}
		return selectedRoles;
	}

	public void setSelectedRoles(List<UserRole> selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	public List<UserRole> getNotSelectedRoles() {
		if (selectedGroup != null) {
			List<UserRole> roleList = DataAccess.webDao.getUserRoles();
			if (selectedRoles != null)
				roleList.removeAll(getSelectedRoles());
			return notSelectedRoles = roleList;
		}
		return notSelectedRoles;
	}

	public void setNotSelectedRoles(List<UserRole> notSelectedRoles) {
		this.notSelectedRoles = notSelectedRoles;
	}

	public List<String> getNotSelectedRolesNames() {
		if (selectedGroup != null) {
			notSelectedRolesNames = new ArrayList<String>();
			for (UserRole role : getNotSelectedRoles()) {
				notSelectedRolesNames.add(role.getRoleId());
			}
		}
		return notSelectedRolesNames;
	}

	public Map<String, UserRole> getNotSelectedRolesStringMap() {
		if (selectedGroup != null) {
			notSelectedRolesStringMap = new HashMap<String, UserRole>();
			for (UserRole role : getNotSelectedRoles()) {
				notSelectedRolesStringMap.put(role.getRoleId(), role);
			}
		}
		return notSelectedRolesStringMap;
	}

	public List<UserGroup> getUserGroups() {
		if (userGroups == null) {
			return userGroups = DataAccess.webDao.getUserGroups();
		}
		return userGroups;
	}

	public void setUserGroups(List<UserGroup> userGroups) {
		this.userGroups = userGroups;
	}

	public UserGroup getSelectedGroup() {
		return selectedGroup;
	}

	public void setSelectedGroup(UserGroup selectedGroup) {
		this.selectedGroup = selectedGroup;
	}

	public UserRole getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(UserRole selectedRole) {
		this.selectedRole = selectedRole;
	}

	public String getSelectedRoleName() {
		if (selectedRole != null)
			// return selectedRoleName;
			return selectedRole.getRoleName();
		else
			return selectedRoleName;
	}

	public void setSelectedRoleName(String selectedRoleName) {
		this.selectedRoleName = selectedRoleName;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	public boolean isCreateNew() {
		return createNew;
	}

	public void setCreateNew(boolean createNew) {
		this.createNew = createNew;
	}

	public void setSelectedGroupId(String selectedGroupId) {
		logger.debug("selectedgroupid set");
		this.selectedGroupId = selectedGroupId;
		if (userGroups == null)
			init();
		for (UserGroup group : userGroups) {
			if (group.getGroupId().equals(selectedGroupId)) {
				selectedGroup = group;
				logger.debug("Group {} selected", group);
			}
		}
	}

	public String getSelectedGroupId() {
		return selectedGroupId;
	}

	public boolean isShowAddDialog() {
		return showAddDialog;
	}

	public void setShowAddDialog(boolean showAddDialog) {
		this.showAddDialog = showAddDialog;
		if (showAddDialog)
			selectedGroup = new UserGroup();
	}

	public boolean isGroupDeletable(UserGroup group) {
		return group != null
				&& !group.getGroupId().equals(ConstantUtil.SUPER_GROUP);
	}

	public boolean isUserGroupEmpty() {
		return userGroups == null || userGroups.isEmpty();
	}
}
