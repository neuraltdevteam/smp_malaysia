package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.MibFlag;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.ActionCommand;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.HostMonitorName;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.config.HostSnmpConfig;
import com.neuralt.smp.config.HostSnmpConfig.MibFileWrapper;
import com.neuralt.smp.config.InetCategory;
import com.neuralt.smp.config.InetCategory.Inet;
import com.neuralt.smp.config.ProcessConfig;
import com.neuralt.smp.config.ProcessConfig.EventHandlerConfig;
import com.neuralt.smp.config.Site;
import com.neuralt.smp.config.SystemConfig;
import com.neuralt.web.util.CssStyleUtil;
import com.neuralt.web.util.jsf.WebUtil;

/**
 * This class serves as a collector/holder for all variables use to modify
 * configs. It also takes care of rendering components on systemconfig.xhtml
 * Actual implementation of methods should be handed over to other classes.
 * 
 */
@ManagedBean
@ViewScoped
public class SystemEditer implements Serializable {

	private static final long serialVersionUID = 4963337379787420598L;

	private static final Logger logger = LoggerFactory
			.getLogger(SystemEditer.class);

	private TreeNode entityRoot;
	private TreeNode selectedNode; // for TreeTable's own good health

	public static String selectSiteName = "--select a site--";

	private MonitoredEntity lastSelectedEntity;
	private MonitoredSystem selectedSystem;
	private MonitoredHost selectedHost;
	private MonitoredProcess selectedProcess;
	private EventHandlerConfig eventHandlerConfig = new EventHandlerConfig();
	private ActionCommand actionCommand = new ActionCommand();
	private HostMonitorConfig hostMonitorConfig = new HostMonitorConfig();
	private MonitorThresholdConfig monitorThresholdConfig = new MonitorThresholdConfig();
	private EventHandlerConfig dummyEventHandlerConfig = new EventHandlerConfig();
	private ActionCommand dummyActionCommand = new ActionCommand();
	private MonitorThresholdConfig dummyMonitorThresholdConfig = new MonitorThresholdConfig();
	private HostMonitorConfig dummyHostMonitorConfig = new HostMonitorConfig();

	private Inet inet = new Inet();
	private Inet dummyInet = new Inet();
	private InetCategory inetCategory = new InetCategory();
	private InetCategory dummyInetCategory = new InetCategory();
	private HostSnmpConfig snmpConfig = new HostSnmpConfig();
	private HostSnmpConfig dummySnmpConfig = new HostSnmpConfig();

	private SystemConfig dummySystem = new SystemConfig();
	private HostConfig dummyHost = new HostConfig();
	private ProcessConfig dummyProcess = new ProcessConfig();
	private SystemConfig newSystem = new SystemConfig();
	private HostConfig newHost = new HostConfig();
	private ProcessConfig newProcess = new ProcessConfig();

	// private List<ActionCommand> actionCommands;
	private List<String> selectedMibNames = new ArrayList<String>();

	private List<Site> sites;
	private Site selectedSite;

	private boolean systemSelected;
	private boolean hostSelected;
	private boolean processSelected;
	private boolean newSystemButtonClicked;
	private boolean newNodeButtonClicked;
	private boolean editButtonClicked;

	@ManagedProperty(value = "#{systemManageController}")
	private SystemManageController systemManageController;
	@ManagedProperty(value = "#{hostManageController}")
	private HostManageController hostManageController;
	@ManagedProperty(value = "#{processManageController}")
	private ProcessManageController processManageController;
	@ManagedProperty(value="#{configSystemController}")
	private ConfigSystemController systemController;

	public SystemEditer() {
		systemManageController = new SystemManageController();
		newHost = new HostConfig();
		newHost.generateDefaultSetting();
		monitorThresholdConfig = new MonitorThresholdConfig();
		reload();

		List<MonitoredSystem> systems = SmpClient.instance.getSystems();
		selectedSystem = !systems.isEmpty() ? systems.get(0) : null;
		if (selectedSystem != null) {
			dummySystem = selectedSystem.getBackupSystemConfig();
		}
		systemSelected = true;
		lastSelectedEntity = selectedSystem;
	}

	private void reload() {
		createTreeTableModel();
		sites = DataAccess.siteDao.getAllSites();
		// the dummy is used for display only
		// somehow when it is selected, jsf will interpret it as null object
		Site dummy = new Site();
		dummy.setSiteName(selectSiteName);
		sites.add(dummy);
		Collections.sort(sites);
	}

	private void createTreeTableModel() {
		entityRoot = new DefaultTreeNode("root", null);
		entityRoot.setExpanded(true);
		for (MonitoredSystem sys : SmpClient.instance.getSystems()) {
			TreeNode sysNode = new DefaultTreeNode(sys, entityRoot);
			sysNode.setExpanded(true);
			for (MonitoredHost host : sys.getHosts()) {
				TreeNode hostNode = new DefaultTreeNode(host, sysNode);
				hostNode.setExpanded(true);
				for (MonitoredProcess proc : host.getProcesses()) {
					TreeNode procNode = new DefaultTreeNode(proc, hostNode);
					procNode.setExpanded(true);
				}
			}
		}
	}

	public String getNewNodeHeader() {
		StringBuilder sb = new StringBuilder();
		if (lastSelectedEntity != null) {
			switch (lastSelectedEntity.getEntityType()) {
			case SYSTEM:
				sb.append(selectedSystem.getFullName());
				sb.append(" - New Host");
				break;
			case HOST:
				sb.append(selectedHost.getFullName());
				sb.append(" - New Process");
				break;
			case PROCESS:
				sb.append("Error");
				break;
			}
		}
		return sb.toString();
	}

	// SYSTEM

	public void setEditSystem() {
		dummySystem = selectedSystem.getBackupSystemConfig();
	}

	public void editSystem() {
		systemManageController.editSystem(dummySystem, selectedSystem);
		editButtonClicked = false;
		createTreeTableModel();
	}

	public void addSystem() {
		String newSystemName = systemManageController.getSystem().getName();
		systemManageController.addSystem();
		newSystemButtonClicked = false;
		createTreeTableModel();

		List<MonitoredSystem> systems = SmpClient.instance.getSystems();
		for (MonitoredSystem sys : systems) {
			if (newSystemName.equals(sys.getName())) {
				selectedSystem = sys;
				lastSelectedEntity = selectedSystem;
				systemSelected = true;
				hostSelected = false;
				processSelected = false;
				break;
			}
		}

	}

	public void deleteSystem() {
		try {
			SmpClient.instance.deleteSystem(selectedSystem);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-system", selectedSystem.getFullName());
		} catch (DataAccessException e) {
			logger.warn("failed in accessing DB", e);
		} catch (Exception e) {
			logger.error("failed in deleting system {}",
					selectedSystem.getName(), e);
		}
		createTreeTableModel();
		List<MonitoredSystem> systems = SmpClient.instance.getSystems();
		selectedSystem = !systems.isEmpty() ? systems.get(0) : null;
		lastSelectedEntity = selectedSystem;
	}

	// HOST

	public void setEditHost() {
		dummyHost = selectedHost.getBackupHostConfig();
	}

	public void addHost() {
		logger.debug("addHost invoked");
		hostManageController.addHost(selectedSystem);
		newNodeButtonClicked = false;
		reload();
	}

	public void editHost() {
		logger.debug("selectedSite=" + selectedSite);
		// if (!StringUtil.isNullOrEmpty(selectedSite.getSiteName())) {
		if (selectedSite.getSiteName().equals(selectSiteName)) {
			dummyHost.setSite(null);
		} else {
			dummyHost.setSite(selectedSite);
		}
		// } else {
		// dummyHost.setSite(null);
		// }
		hostManageController.editHost(selectedHost, dummyHost);
		editButtonClicked = false;
		reload();
	}

	public void deleteHost() {
		hostManageController.deleteHost(selectedHost);
		selectedHost = null;
		lastSelectedEntity = null;
		reload();
	}

	public void editMonitorThresholdConfig() {
		hostManageController.editMonitorThresholdConfig(selectedHost,
				dummyMonitorThresholdConfig, monitorThresholdConfig);
	}

	public void addMonitorThresholdConfig() {
		hostManageController.addMonitorThresholdConfig(selectedHost,
				hostMonitorConfig, monitorThresholdConfig);
	}

	public void editHostMonitorConfig() {
		hostManageController.editHostMonitorConfig(selectedHost,
				hostMonitorConfig, dummyHostMonitorConfig);
	}

	public boolean isAllowModifyTarget() {
		if (dummyMonitorThresholdConfig.getHostMonitorConfig() == null) {
			return false;
		}
		return dummyMonitorThresholdConfig.getHostMonitorConfig().getName() == HostMonitorName.DISK_MONITOR;
	}

	public boolean isAllowAddThreshold(HostMonitorConfig config) {
		return ((config.getName() == HostMonitorName.INET_MONITOR) || (config
				.getName() == HostMonitorName.DISK_MONITOR));
	}

	public boolean isDiskMonitorSelected() {
		return (hostMonitorConfig.getName() == HostMonitorName.DISK_MONITOR);
	}

	public boolean isInetMonitorSelected() {
		return (hostMonitorConfig.getName() == HostMonitorName.INET_MONITOR);
	}

	public void deleteMonitorThresholdConfig(MonitorThresholdConfig threshold) {
		hostManageController.deleteMonitorThresholdConfig(selectedHost,
				threshold);
	}

	public boolean isInet(HostMonitorConfig hostMonitorConfig) {
		return (hostMonitorConfig.getName() == HostMonitorName.INET_MONITOR);
	}

	// public void saveProcessList() {
	// // check if all previously monitored processes are in the target list.
	// for (MonitoredProcess target : selectedHost.getProcesses()) {
	// if
	// (!selectedHost.getDualList().getTarget().contains(target.getConfig().getName()))
	// {
	// selectedHost.deleteProcess(target);
	// }
	// }
	// // check if all target processes have a process config. If not, create a
	// new one
	// for (String target : selectedHost.getDualList().getTarget()) {
	// if (selectedHost.getProcess(target)==null) {
	// ProcessConfig newProcess = new ProcessConfig();
	// newProcess.setName(target);
	// newProcess.setHostConfig(selectedHost.getConfig());
	// newProcess.setEnabled(true);
	// MonitoredProcess newMonProcess = new MonitoredProcess(newProcess,
	// selectedHost);
	// selectedHost.addProcess(newMonProcess);
	// }
	// }
	// DataAccess.hostConfigDao.save(selectedHost.getConfig());
	// createTreeTableModel();
	// }

	public void deleteInet(Inet inet) {
		inet.getParent().getInets().remove(inet);
		DataAccess.hostConfigDao.save(selectedHost.getConfig());
	}

	public void addInet() {
		for (InetCategory cate : inetCategory.getParent().getInetCategories()) {
			for (Inet child : cate.getInets()) {
				if (child.getDefaultName().equals(inet.getDefaultName())) {
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.DUPLICATED_NAME);
					return;
				}
			}
		}
		inetCategory.getInets().add(inet);
		inet.setParent(inetCategory);
		DataAccess.hostConfigDao.save(selectedHost.getConfig());
		inet = new Inet();
	}

	public void editInet() {
		if (!dummyInet.getDefaultName().equals(inet.getDefaultName())) {
			for (InetCategory cate : inetCategory.getParent()
					.getInetCategories()) {
				for (Inet child : cate.getInets()) {
					if (dummyInet.getDefaultName().equals(
							child.getDefaultName())) {
						WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
								MessageKey.GENERAL_ERROR,
								MessageKey.DUPLICATED_NAME);
						return;
					}
				}
			}
		}
		inet.update(dummyInet);
		DataAccess.hostConfigDao.save(selectedHost.getConfig());
		inet = new Inet();
		dummyInet = new Inet();
	}

	public void deleteInetCategory(InetCategory inetCategory) {
		inetCategory.getParent().getInetCategories().remove(inetCategory);
		DataAccess.hostConfigDao.save(selectedHost.getConfig());
	}

	public void addInetCategory() {
		for (InetCategory cate : selectedHost.getConfig().getInetCategories()) {
			if (cate.getName().equals(inetCategory.getName())) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR, MessageKey.DUPLICATED_NAME);
				return;
			}
		}
		selectedHost.getConfig().getInetCategories().add(inetCategory);
		inetCategory.setParent(selectedHost.getConfig());
		DataAccess.hostConfigDao.save(selectedHost.getConfig());
	}

	public void editInetCategory() {
		if (!dummyInetCategory.getName().equals(inetCategory.getName())) {
			for (InetCategory cate : inetCategory.getParent()
					.getInetCategories()) {
				if (dummyInetCategory.getName().equals(cate.getName())) {
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.DUPLICATED_NAME);
					return;
				}
			}
		}
		inetCategory.update(dummyInetCategory);
		DataAccess.hostConfigDao.save(selectedHost.getConfig());
	}

	public void resetInetCategory() {
		inetCategory = new InetCategory();
	}

	public void editSnmp() {
		logger.debug("edit snmp called");
		dummySnmpConfig.updateMibFileList(selectedMibNames);
		hostManageController
				.editSnmp(snmpConfig, dummySnmpConfig, selectedHost);
		selectedMibNames.clear();
	}

	public void addSnmp() {
		logger.debug("add snmp called");
		snmpConfig.updateMibFileList(selectedMibNames);
		for (String name : selectedMibNames) {
			logger.debug("name {} is added in HostSnmpConfig", name);
		}
		hostManageController.addSnmp(snmpConfig, selectedHost);
		selectedMibNames.clear();
	}

	public void deleteSnmp(HostSnmpConfig snmp) {
		hostManageController.deleteSnmp(snmp, selectedHost);
	}

	// PROCESS

	public void editProcess() {
		processManageController.editProcess(dummyProcess, selectedProcess);
		editButtonClicked = false;
		createTreeTableModel();
	}

	public void setEditProcess() {
		dummyProcess = selectedProcess.getBackupProcessConfig();
	}

	public void addProcess() {
		processManageController.addProcess(selectedHost);
		newNodeButtonClicked = false;
		createTreeTableModel();
	}

	// public void addEventHandler() {
	// processManageController.addEventHandlerConfig(selectedProcess);
	// }
	public void addActionCommand() {
		logger.debug("send to processManager " + this.eventHandlerConfig);
		processManageController.addActionCommand(eventHandlerConfig);
	}

	public void deleteProcess() {
		processManageController.deleteProcess(selectedProcess);

		selectedProcess = null;
		lastSelectedEntity = null;
		createTreeTableModel();
	}

	// public void deleteEventHandlerConfig() {
	// processManageController.deleteEventHandlerConfig(eventHandlerConfig);
	// eventHandlerConfig = new EventHandlerConfig();
	// }
	// public void editEventHandlerConfig() {
	// processManageController.editEventHandlerConfig(selectedProcess,
	// eventHandlerConfig, dummyEventHandlerConfig);
	// }
	public void deleteActionCommand() {
		processManageController.deleteActionCommand(actionCommand);
		actionCommand = new ActionCommand();
	}

	public void editActionCommand() {
		processManageController.editActionCommand(eventHandlerConfig,
				dummyActionCommand, actionCommand);
		dummyActionCommand = null;
	}

	public boolean isActionCommandSet() {
		return actionCommand != null;
	}

	public boolean isEventHandlerConfigSet() {
		return eventHandlerConfig != null;
	}

	public List<EventHandlerConfig> getEventHandlers() {
		List<EventHandlerConfig> list = null;
		if (selectedProcess != null)
			list = selectedProcess.getConfig().getListEventHandlers();
		return list;
	}

	public List<ActionCommand> getActionCommands() {
		if (eventHandlerConfig != null)
			return eventHandlerConfig.getListActionCommand();
		else
			return new ArrayList<ActionCommand>();
	}

	public String getDisplayName() {
		StringBuilder sb = new StringBuilder();
		switch (lastSelectedEntity.getEntityType()) {
		case SYSTEM:
			sb.append("[SYSTEM] ");
			sb.append(selectedSystem.getFullName());
			break;
		case HOST:
			sb.append("[HOST] ");
			sb.append(selectedHost.getFullName());
			break;
		case PROCESS:
			sb.append("[PROCESS] ");
			sb.append(selectedProcess.getFullName());
			break;
		}
		return sb.toString();
	}

	// public void initRrdDB() {
	// String path = null;
	// logger.debug("initing RRD DB for {}",selectedHost.getName());
	// try {
	// path = RrdUtil.createRrdDB(selectedHost);
	// selectedHost.getConfig().setRrdFilePath(path);
	// SmpClient.instance.reloadHost(selectedHost.getConfig(), selectedHost);
	// selectedHost.resetFilters();
	// selectedHost.setEnableRrd(true);
	// WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
	// MessageKey.GENERAL_SUCCESS, MessageKey.HOST_INIT_RRD_SUCCESS);
	// } catch (Exception e) {
	// logger.warn("failed in initiating host stat",e);
	// WebUtil.addMessage(FacesMessage.SEVERITY_ERROR, MessageKey.GENERAL_ERROR,
	// MessageKey.HOST_INIT_RRD_FAIL);
	// }
	// }

	public void onEntitySelect(NodeSelectEvent event) {
		TreeNode entityNode = event.getTreeNode();
		lastSelectedEntity = (MonitoredEntity) entityNode.getData();
		setNewSystemButtonClicked(false);
		switch (lastSelectedEntity.getEntityType()) {
		case SYSTEM:
			selectedSystem = (MonitoredSystem) lastSelectedEntity;
			dummySystem = selectedSystem.getBackupSystemConfig();
			selectedHost = null;
			selectedProcess = null;
			systemSelected = true;
			hostSelected = false;
			processSelected = false;
			selectedSite = null;
			break;
		case HOST:
			selectedHost = (MonitoredHost) lastSelectedEntity;
			// if (selectedHost.isEnabled() &&
			// selectedHost.getConfig().getType()!=MonitorType.SCRIPT) {
			// selectedHost.generateDualList();
			// }
			dummyHost = selectedHost.getBackupHostConfig();
			selectedProcess = null;
			selectedSystem = (MonitoredSystem) entityNode.getParent().getData();
			if (selectedHost.getConfig().getSite() != null) {
				selectedSite = selectedHost.getConfig().getSite();
			}
			systemSelected = false;
			hostSelected = true;
			processSelected = false;
			snmpConfig = new HostSnmpConfig();
			dummySnmpConfig = new HostSnmpConfig();
			inet = new Inet();
			inetCategory = new InetCategory();
			dummyInet = new Inet();
			dummyInetCategory = new InetCategory();
			break;
		case PROCESS:
			selectedProcess = (MonitoredProcess) lastSelectedEntity;
			dummyProcess = selectedProcess.getBackupProcessConfig();
			selectedHost = (MonitoredHost) entityNode.getParent().getData();
			selectedSystem = (MonitoredSystem) entityNode.getParent()
					.getParent().getData();
			systemSelected = false;
			hostSelected = false;
			processSelected = true;
			selectedSite = null;
			break;
		}
	}

	public TreeNode getEntityRoot() {
		return entityRoot;
	}

	public String getStatusCssClass(String statusText) {
		String result = CssStyleUtil.getStatusCssClass(statusText);
		if (result == null)
			result = "";

		return result;
	}

	public SystemManageController getSystemManageController() {
		return systemManageController;
	}

	public void setSystemManageController(SystemManageController systemManager) {
		this.systemManageController = systemManager;
	}

	/*
	 * public List<ActionConfig> getActionConfigs() { List<ActionConfig> list =
	 * new
	 * ArrayList<ActionConfig>(selectedProcess.getConfig().getEventHandlers().);
	 * return list; }
	 */

	public HostManageController getHostManageController() {
		return hostManageController;
	}

	public EventHandlerConfig getEventHandlerConfig() {
		return eventHandlerConfig;
	}

	public void setEventHandlerConfig(EventHandlerConfig eventHandlerConfig) {
		logger.debug("eventHandlerConfig " + eventHandlerConfig + " is set");
		this.eventHandlerConfig = eventHandlerConfig;
		logger.debug("this.eventHandlerConfig " + this.eventHandlerConfig
				+ " is set");
	}

	public void setHostManageController(
			HostManageController hostManageController) {
		this.hostManageController = hostManageController;
	}

	public ActionCommand getActionCommand() {
		return actionCommand;
	}

	public void setActionCommand(ActionCommand actionCommand) {
		this.actionCommand = actionCommand;
	}

	public ProcessManageController getProcessManageController() {
		return processManageController;
	}

	public void setProcessManageController(
			ProcessManageController processManageController) {
		this.processManageController = processManageController;
	}

	public MonitoredEntity getLastSelectedEntity() {
		return lastSelectedEntity;
	}

	public HostMonitorConfig getHostMonitorConfig() {
		return hostMonitorConfig;
	}

	public void setHostMonitorConfig(HostMonitorConfig hostMonitorConfig) {
		this.hostMonitorConfig = hostMonitorConfig;
		monitorThresholdConfig = new MonitorThresholdConfig();
	}

	public MonitorThresholdConfig getMonitorThresholdConfig() {
		return monitorThresholdConfig;
	}

	public void setMonitorThresholdConfig(
			MonitorThresholdConfig monitorThresholdConfig) {
		this.monitorThresholdConfig = monitorThresholdConfig;
		logger.debug(monitorThresholdConfig.getHostMonitorConfig().getName()
				.name());
	}

	public boolean isDummyMonitorThresholdConfigSet() {
		return dummyMonitorThresholdConfig != null;
	}

	public boolean isHostMonitorConfigSet() {
		return hostMonitorConfig != null;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public MonitoredSystem getSelectedSystem() {
		return selectedSystem;
	}
	
	public void setSelectedHost(HostConfig hostCongfig) {
		MonitoredSystem system = systemController.monitored(systemController.getModel().getSelected());
		MonitoredHost monHost = system.getHostByID(hostCongfig.getId());		
		this.selectedHost = monHost;
		this.selectedHost.setConfig(hostCongfig);
	}

	public MonitoredHost getSelectedHost() {
		return selectedHost;
	}

	public MonitoredProcess getSelectedProcess() {
		return selectedProcess;
	}

	public boolean isSystemSelected() {
		return systemSelected;
	}

	public void setSystemSelected(boolean systemSelected) {
		this.systemSelected = systemSelected;
	}

	public boolean isHostSelected() {
		return hostSelected;
	}

	public boolean isEntitySelected() {
		return lastSelectedEntity != null;
	}

	public void setHostSelected(boolean hostSelected) {
		this.hostSelected = hostSelected;
	}

	public boolean isProcessSelected() {
		return processSelected;
	}

	public void setProcessSelected(boolean processSelected) {
		this.processSelected = processSelected;
	}

	public SystemConfig getDummySystem() {
		return dummySystem;
	}

	public void setDummySystem(SystemConfig dummySystem) {
		this.dummySystem = dummySystem;
	}

	public HostConfig getDummyHost() {
		return dummyHost;
	}

	public void setDummyHost(HostConfig dummyHost) {
		this.dummyHost = dummyHost;
	}

	public ProcessConfig getDummyProcess() {
		return dummyProcess;
	}

	public void setDummyProcess(ProcessConfig dummyProcess) {
		this.dummyProcess = dummyProcess;
	}

	public SystemConfig getNewSystem() {
		return newSystem;
	}

	public void setNewSystem(SystemConfig newSystem) {
		this.newSystem = newSystem;
	}

	public HostConfig getNewHost() {
		return newHost;
	}

	public void setNewHost(HostConfig newHost) {
		this.newHost = newHost;
	}

	public ProcessConfig getNewProcess() {
		return newProcess;
	}

	public void setNewProcess(ProcessConfig newProcess) {
		this.newProcess = newProcess;
	}

	public boolean isNewSystemButtonClicked() {
		return newSystemButtonClicked;
	}

	public void setNewSystemButtonClicked(boolean newSystemButtonClicked) {
		this.newSystemButtonClicked = newSystemButtonClicked;
	}

	public boolean isNewNodeButtonClicked() {
		return newNodeButtonClicked;
	}

	public void setNewNodeButtonClicked(boolean newNodeButtonClicked) {
		this.newNodeButtonClicked = newNodeButtonClicked;
	}

	public boolean isEditButtonClicked() {
		return editButtonClicked;
	}

	public void setEditButtonClicked(boolean editButtonClicked) {
		this.editButtonClicked = editButtonClicked;
	}

	public EventHandlerConfig getDummyEventHandlerConfig() {
		return dummyEventHandlerConfig;
	}

	public MonitorThresholdConfig getDummyMonitorThresholdConfig() {
		return dummyMonitorThresholdConfig;
	}

	public HostMonitorConfig getDummyHostMonitorConfig() {
		return dummyHostMonitorConfig;
	}

	public void setDummyHostMonitorConfig(
			HostMonitorConfig dummyHostMonitorConfig) {
		this.hostMonitorConfig = dummyHostMonitorConfig;
		this.dummyHostMonitorConfig = dummyHostMonitorConfig.getBackupConfig();
	}

	public void setDummyMonitorThresholdConfig(
			MonitorThresholdConfig dummyMonitorThresholdConfig) {
		this.dummyMonitorThresholdConfig = dummyMonitorThresholdConfig
				.getBackupConfig();
		this.monitorThresholdConfig = dummyMonitorThresholdConfig;
		this.hostMonitorConfig = dummyMonitorThresholdConfig
				.getHostMonitorConfig();
	}

	public void setDummyEventHandlerConfig(
			EventHandlerConfig dummyEventHandlerConfig) {
		this.eventHandlerConfig = dummyEventHandlerConfig;
		this.dummyEventHandlerConfig = dummyEventHandlerConfig
				.getBackupEventHandlerConfig();
	}

	public ActionCommand getDummyActionCommand() {
		return dummyActionCommand;
	}

	public void setDummyActionCommand(ActionCommand dummyActionCommand) {
		this.actionCommand = dummyActionCommand;
		this.dummyActionCommand = dummyActionCommand.getBackup();
		this.eventHandlerConfig = dummyActionCommand.getEventHandlerConfig();
	}

	public Inet getInet() {
		return inet;
	}

	public void setInet(Inet inet) {
		this.inet = inet;
	}

	public Inet getDummyInet() {
		return dummyInet;
	}

	public void setDummyInet(Inet dummyInet) {
		this.dummyInet = dummyInet.getBackup();
		this.inet = dummyInet;
	}

	public InetCategory getInetCategory() {
		return inetCategory;
	}

	public void setInetCategory(InetCategory inetCategory) {
		this.inetCategory = inetCategory;
	}

	public InetCategory getDummyInetCategory() {
		return dummyInetCategory;
	}

	public void setDummyInetCategory(InetCategory dummyInetCategory) {
		this.dummyInetCategory = dummyInetCategory.getBackup();
		this.inetCategory = dummyInetCategory;
	}

	public boolean isDummyActionCommandSet() {
		return dummyActionCommand != null;
	}

	public boolean isDummyEventHandlerConfigSet() {
		return dummyEventHandlerConfig != null;
	}

	public boolean isShowTarget(HostMonitorConfig config) {
		return config.getName() != HostMonitorName.CPU_MONITOR;
	}

	public boolean isShowTarget() {
		if (monitorThresholdConfig.getHostMonitorConfig() == null) {
			return false;
		}
		return monitorThresholdConfig.getHostMonitorConfig().getName() != HostMonitorName.CPU_MONITOR
				&& monitorThresholdConfig.getHostMonitorConfig().getName() != HostMonitorName.UPTIME_MONITOR;
	}

	public HostSnmpConfig getSnmpConfig() {
		return snmpConfig;
	}

	public void setSnmpConfig(HostSnmpConfig snmpConfig) {
		this.snmpConfig = snmpConfig;
	}

	public HostSnmpConfig getDummySnmpConfig() {
		return dummySnmpConfig;
	}

	public void setDummySnmpConfig(HostSnmpConfig dummySnmpConfig) {
		this.dummySnmpConfig = dummySnmpConfig.getBackup();
		this.snmpConfig = dummySnmpConfig;
		selectedMibNames.clear();
		for (MibFileWrapper wrapper : dummySnmpConfig.getMibFileList()) {
			selectedMibNames.add(wrapper.getMibName());
		}
	}

	public Map<String, String> getMibNameList() {
		Map<String, String> map = new TreeMap<String, String>();
		for (MibFlag flag : DataAccess.mibFlagDao.getLoadedMibFlags()) {
			map.put(flag.getName(), flag.getName());
		}
		return map;
	}

	public List<String> getSelectedMibNames() {
		return selectedMibNames;
	}

	public void setSelectedMibNames(List<String> selectedMibNames) {
		this.selectedMibNames = selectedMibNames;
	}

	public List<Site> getSites() {
		return sites;
	}

	public void setSites(List<Site> sites) {
		this.sites = sites;
	}

	public Site getSelectedSite() {
		return selectedSite;
	}

	public void setSelectedSite(Site selectedSite) {
		this.selectedSite = selectedSite;
	}

	public boolean isSystemTreeEmpty() {
		return entityRoot == null || entityRoot.isLeaf();
	}

	/* Getter and Setter for systemController */
	public ConfigSystemController getSystemController() {
		return systemController;
	}

	public void setSystemController(ConfigSystemController systemController) {
		this.systemController = systemController;
	}
	
	
}
