package com.neuralt.smp.client.web.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.ChartTemplate;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.RrdArchiveConfig;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.client.util.ChartTemplateUtil;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class HistoricalStatisticsViewer implements Serializable {
	private static final long serialVersionUID = 5285298372722430870L;
	private static final Logger logger = LoggerFactory
			.getLogger(HistoricalStatisticsViewer.class);

	private List<String> defList;
	private List<String> selectedDef;
	private Map<String, RrdDefConfig> defMap;
	private List<String> archiveList;
	private List<String> selectedArchive;
	private Map<String, RrdArchiveConfig> archiveMap;
	private List<String> dsList;
	private List<String> selectedDs;
	private Map<String, RrdDsConfig> dsMap;
	private ChartTemplate template;
	private boolean showGraph;
	private CartesianChartModel graph;
	private Date from;
	private Date to;
	private long start;

	public HistoricalStatisticsViewer() {
		defList = new ArrayList<String>();
		archiveList = new ArrayList<String>();
		dsList = new ArrayList<String>();
		selectedDef = new ArrayList<String>();
		selectedDs = new ArrayList<String>();
		selectedArchive = new ArrayList<String>();
		defMap = new HashMap<String, RrdDefConfig>();
		archiveMap = new HashMap<String, RrdArchiveConfig>();
		dsMap = new HashMap<String, RrdDsConfig>();
		init();
	}

	private void reset() {

	}

	private void init() {
		for (RrdDefConfig def : DataAccess.rrdDefConfigDao
				.getAllLockedRrdDefs()) {
			defList.add(def.getName());
			defMap.put(def.getName(), def);
			for (RrdDsConfig ds : def.getRrdDsConfigs()) {
				dsMap.put(concatName(def.getName(), ds.getName()), ds);
			}
			for (RrdArchiveConfig archive : def.getRrdArchiveConfigs()) {
				archiveMap.put(concatName(def.getName(), archive.getName()),
						archive);
			}
		}
		template = new ChartTemplate();
	}

	private void refreshList() {
		for (String defStr : selectedDef) {
			RrdDefConfig def = defMap.get(defStr);
			for (RrdDsConfig ds : def.getRrdDsConfigs()) {
				String displayName = concatName(def.getName(), ds.getName());
				if (!dsList.contains(displayName)) {
					dsList.add(displayName);
				}
			}
			for (RrdArchiveConfig archive : def.getRrdArchiveConfigs()) {
				String displayName = concatName(def.getName(),
						archive.getName());
				if (!archiveList.contains(displayName)) {
					archiveList.add(displayName);
				}
			}
		}
		Collections.sort(dsList);
		Collections.sort(archiveList);
	}

	private void createDsList() {
		logger.debug("ds count=" + template.getDsConfigs().size());
		for (RrdDsConfig ds : template.getDsConfigs()) {
			String displayName = concatName(ds.getRrdDef().getName(),
					ds.getName());
			logger.debug("template has ds:" + displayName);
			if (!selectedDs.contains(displayName))
				selectedDs.add(displayName);
			if (!selectedDef.contains(ds.getRrdDef().getName())) {
				selectedDef.add(ds.getRrdDef().getName());
				// for (RrdDsConfig dummy :
				// ds.getRrdDef().getRrdDsConfigsAsList()) {
				// dsList.add(concatName(ds.getRrdDef().getName(),
				// dummy.getName()));
				// logger.debug("ds is added to dsList:"+dummy.getName());
				// }
			}
		}
	}

	private void createArchiveList() {
		logger.debug("archive count=" + template.getArchiveConfigs().size());
		for (RrdArchiveConfig archive : template.getArchiveConfigs()) {
			String displayName = concatName(archive.getRrdDef().getName(),
					archive.getName());
			logger.debug("template has archive:" + displayName);
			if (!selectedArchive.contains(displayName))
				selectedArchive.add(displayName);
			if (!selectedDef.contains(archive.getRrdDef().getName())) {
				selectedDef.add(archive.getRrdDef().getName());
				// for (RrdArchiveConfig dummy :
				// archive.getRrdDef().getRrdArchiveConfigsAsList()) {
				// archiveList.add(concatName(archive.getRrdDef().getName(),
				// dummy.getName()));
				// logger.debug("archive is added to archiveList:"+dummy.getName());
				// }
			}
		}
	}

	public void preview() {
		if (from != null && to != null) {
			if (from.compareTo(to) > 0) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR, MessageKey.INVALID_TIME);
			} else {
				template.getDsConfigs().clear();
				for (String dsStr : selectedDs) {
					RrdDsConfig ds = dsMap.get(dsStr);
					template.getDsConfigs().add(ds);
				}
				template.getArchiveConfigs().clear();
				for (String archiveStr : selectedArchive) {
					RrdArchiveConfig archive = archiveMap.get(archiveStr);
					template.getArchiveConfigs().add(archive);
				}
				if (template.isConfigValid()) {
					showGraph = true;
					try {
						graph = ChartTemplateUtil.generateChart(template, from,
								to);
					} catch (IOException e) {
						WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
								MessageKey.GENERAL_ERROR,
								MessageKey.RRD_ACCESS_FAIL);
					}
				} else {
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.TEMPLATE_CONIG_INVAILD);
				}
			}
		} else {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.INVALID_TIME);
		}
	}

	private String concatName(String def, String other) {
		StringBuilder sb = new StringBuilder();
		sb.append(def).append(" - ").append(other);
		return sb.toString();
	}

	// getter/setter
	public List<String> getSelectedDef() {
		return selectedDef;
	}

	public void setSelectedDef(List<String> selectedDef) {
		this.selectedDef = selectedDef;
		// generate dsList & archiveList, also remove some selectedDs &
		// selectedArchive
		dsList.clear();
		archiveList.clear();
		for (String defStr : selectedDef) {
			RrdDefConfig def = defMap.get(defStr);
			for (RrdDsConfig ds : def.getRrdDsConfigs()) {
				dsList.add(concatName(def.getName(), ds.getName()));
			}
			for (RrdArchiveConfig archive : def.getRrdArchiveConfigs()) {
				archiveList.add(concatName(def.getName(), archive.getName()));
			}
		}
		Collections.sort(dsList);
		Collections.sort(archiveList);
		List<String> dummy = new ArrayList<String>();
		for (String dsStr : selectedDs) {
			if (!dsList.contains(dsStr)) {
				dummy.add(dsStr);
			}
		}
		for (String dummyStr : dummy) {
			selectedDs.remove(dummyStr);
		}
		dummy.clear();
		for (String archiveStr : selectedArchive) {
			if (!archiveList.contains(archiveStr)) {
				dummy.add(archiveStr);
			}
		}
		for (String dummyStr : dummy) {
			selectedArchive.remove(dummyStr);
		}
	}

	public List<String> getSelectedArchive() {
		return selectedArchive;
	}

	public void setSelectedArchive(List<String> selectedArchive) {
		this.selectedArchive = selectedArchive;
	}

	public List<String> getSelectedDs() {
		return selectedDs;
	}

	public void setSelectedDs(List<String> selectedDs) {
		this.selectedDs = selectedDs;
	}

	public List<String> getDefList() {
		return defList;
	}

	public List<String> getArchiveList() {
		return archiveList;
	}

	public List<String> getDsList() {
		return dsList;
	}

	public ChartTemplate getTemplate() {
		return template;
	}

	public boolean isShowGraph() {
		return showGraph;
	}

	public CartesianChartModel getGraph() {
		return graph;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
		start = from.getTime() - 5 * ConstantUtil.SEC_MS;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public long getStart() {
		return start;
	}
}
