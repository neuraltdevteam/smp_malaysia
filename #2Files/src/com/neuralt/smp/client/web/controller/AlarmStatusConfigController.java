package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.config.AlarmStatus;
import com.neuralt.smp.client.data.config.AlarmStatus.StatusCategory;
import com.neuralt.smp.client.service.AlarmService;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.TextColor;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class AlarmStatusConfigController implements Serializable {

	private static final long serialVersionUID = 6998385482446512315L;

	private static final Logger logger = LoggerFactory
			.getLogger(AlarmStatusConfigController.class);

	private List<AlarmStatus> alarmStatus;
	private List<AlarmStatus> filteredAlarmStatus; // used by dataTable
	private AlarmStatus selectedAlarmStatus;

	public AlarmStatusConfigController() {
		refresh();
	}

	public void refresh() {
		alarmStatus = AlarmService.instance.listAlarmStatus();
	}

	public void prepareAddAlarmStatus() {
		selectedAlarmStatus = new AlarmStatus();
	}

	public void saveAlarmStatus() {
		if (selectedAlarmStatus != null) {
			logger.debug("saveAlarmStatus: " + selectedAlarmStatus.getName());
			try {
				DataAccess.alarmConfigDao.save(selectedAlarmStatus);
				AlarmService.instance.reloadAlarmStatusList();
				alarmStatus = AlarmService.instance.listAlarmStatus();
			} catch (Exception e) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR, e.getMessage());
				logger.error("saveAlarmStatus failed", e);
			}
		}
	}

	public void deleteAlarmStatus() {
		if (selectedAlarmStatus != null) {
			logger.debug("deleteAlarmStatus: " + selectedAlarmStatus.getName());
			try {
				DataAccess.alarmConfigDao.delete(selectedAlarmStatus);
				AlarmService.instance.reloadAlarmStatusList();
				alarmStatus = AlarmService.instance.listAlarmStatus();
				selectedAlarmStatus = null;
			} catch (Exception e) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR, e.getMessage());
				logger.error("deleteAlarmStatus failed", e);
			}
		}
	}

	public StatusCategory[] getStatusCats() {
		return StatusCategory.values();
	}

	public TextColor[] getTextColors() {
		return TextColor.values();
	}

	public List<SelectItem> getStatusCatFilters() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		items.add(new SelectItem("", "-ANY-"));
		for (StatusCategory cat : StatusCategory.values()) {
			items.add(new SelectItem(cat, cat.name()));
		}
		return items;
	}

	public AlarmStatus getSelectedAlarmStatus() {
		return selectedAlarmStatus;
	}

	public void setSelectedAlarmStatus(AlarmStatus selectedAlarmStatus) {
		this.selectedAlarmStatus = selectedAlarmStatus;
	}

	public List<AlarmStatus> getAlarmStatus() {
		return alarmStatus;
	}

	public List<AlarmStatus> getFilteredAlarmStatus() {
		return filteredAlarmStatus;
	}

	public void setFilteredAlarmStatus(List<AlarmStatus> filteredAlarmStatus) {
		this.filteredAlarmStatus = filteredAlarmStatus;
	}
}
