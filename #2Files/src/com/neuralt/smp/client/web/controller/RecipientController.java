package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.AlarmNotifier;
import com.neuralt.smp.client.data.ContactPerson;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.GroupSnmpConfig;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.model.BaseSnmpConfig.AuthProtocol;
import com.neuralt.smp.config.model.BaseSnmpConfig.PrivProtocol;
import com.neuralt.smp.config.model.BaseSnmpConfig.SnmpVersion;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class RecipientController implements Serializable {
	private static final long serialVersionUID = 8950045166639683902L;
	private static final Logger logger = LoggerFactory
			.getLogger(RecipientController.class);

	private List<ContactPerson> contacts;
	private List<GroupSnmpConfig> snmps;
	private ContactPerson newContact;
	private ContactPerson contact;
	private ContactPerson dummyContact;
	private GroupSnmpConfig newSnmp;
	private GroupSnmpConfig snmp;
	private GroupSnmpConfig dummySnmp;

	public RecipientController() {
		init();
	}

	private void init() {
		contacts = DataAccess.contactPersonDao.getAllContactPersons();
		snmps = DataAccess.groupSnmpConfigDao.getAllGroupSnmpConfigs();
		newContact = new ContactPerson();
		newSnmp = new GroupSnmpConfig();
	}

	private void reload() {
		init();
		AlarmNotifier.instance.reload();
	}

	public void addContact() {
		boolean ok = true;

		if (DataAccess.contactPersonDao.getContactPersonByName(newContact
				.getName()) != null) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.RECIPIENT_DUPLICATED_CONTACTPERSON);
		}

		if (ok) {
			DataAccess.contactPersonDao.save(newContact);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-contact person", newContact.toString());
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS,
					MessageKey.RECIPIENT_CONTACT_SAVED);
			reload();
		}
	}

	public void editContact() {
		boolean ok = true;
		ContactPerson temp = DataAccess.contactPersonDao
				.getContactPersonByName(dummyContact.getName());
		if (temp != null) {
			if (!temp.equals(contact)) {
				ok = false;
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						MessageKey.RECIPIENT_DUPLICATED_CONTACTPERSON);
			}
		}
		if (ok) {
			contact.update(dummyContact);
			DataAccess.contactPersonDao.save(contact);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-contact person", contact.toString());
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS,
					MessageKey.RECIPIENT_CONTACT_SAVED);
			reload();
		}
	}

	public void deleteContact(ContactPerson contact) {
		try {
			List<NotificationGroup> currentGroups = DataAccess.notificationGroupDao
					.getAllGroups();
			List<NotificationGroup> batch = new ArrayList<NotificationGroup>();
			for (NotificationGroup group : currentGroups) {
				for (ContactPerson dummy : group.getContactsAsList()) {
					if (contact.equals(dummy)) {
						group.getContacts().remove(dummy);
						batch.add(group);
						break;
					}
				}
			}
			DataAccess.notificationGroupDao.batchSave(batch);
			DataAccess.contactPersonDao.delete(contact);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-contact person", contact.toString());
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS,
					MessageKey.RECIPIENT_CONTACT_DELETED);
		} catch (Exception e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.GENERAL_DELETE_FAIL
							+ ":" + e.getMessage());
		}
		reload();

	}

	public void addSnmp() {
		boolean ok = true;

		if (DataAccess.groupSnmpConfigDao.getGroupSnmpConfigByName(newSnmp
				.getName()) != null) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.RECIPIENT_DUPLICATED_SNMPCONFIG);
		}
		if (!isGroupSnmpConfigValid(newSnmp)) {
			ok = false;
		}

		if (ok) {
			DataAccess.groupSnmpConfigDao.save(newSnmp);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-SNMP config", newSnmp.toString());
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, MessageKey.RECIPIENT_SNMP_SAVED);
			reload();
		}
	}

	public void editSnmp() {
		boolean ok = true;
		if (!isGroupSnmpConfigValid(dummySnmp)) {
			ok = false;
		}
		GroupSnmpConfig temp = DataAccess.groupSnmpConfigDao
				.getGroupSnmpConfigByName(dummySnmp.getName());
		if (temp != null) {
			if (!temp.equals(snmp)) {
				ok = false;
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						MessageKey.RECIPIENT_DUPLICATED_SNMPCONFIG);
			}
		}

		if (ok) {
			snmp.update(dummySnmp);
			DataAccess.groupSnmpConfigDao.save(snmp);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-SNMP config", snmp.toString());
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, MessageKey.RECIPIENT_SNMP_SAVED);
			reload();
		}
	}

	public void deleteSnmp(GroupSnmpConfig snmp) {
		try {
			List<NotificationGroup> currentGroups = DataAccess.notificationGroupDao
					.getAllGroups();
			List<NotificationGroup> batch = new ArrayList<NotificationGroup>();
			for (NotificationGroup group : currentGroups) {
				for (GroupSnmpConfig dummy : group.getSnmpConfigsAsList()) {
					if (snmp.equals(dummy)) {
						group.getSnmpConfigs().remove(dummy);
						batch.add(group);
						break;
					}
				}
			}
			DataAccess.notificationGroupDao.batchSave(batch);
			DataAccess.groupSnmpConfigDao.delete(snmp);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-SNMP config", snmp.toString());
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS,
					MessageKey.RECIPIENT_SNMP_DELETED);
		} catch (Exception e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.GENERAL_DELETE_FAIL
							+ ":" + e.getMessage());
		}
		reload();
	}

	private boolean isGroupSnmpConfigValid(GroupSnmpConfig config) {
		boolean ok = true;
		if (config.getVersion().equals(SnmpVersion.SNMPV2)
				|| config.getVersion().equals(SnmpVersion.SNMPV1)) {
			// if (StringUtil.isNullOrEmpty(config.getCommunityTrap())) {
			// WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
			// MessageKey.GENERAL_ERROR, MessageKey.SNMP_COMMUNITY_TRAP);
			// ok = false;
			// }
		} else if (config.getVersion().equals(SnmpVersion.SNMPV3)) {
			if (StringUtil.isNullOrEmpty(config.getUsername())) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR, MessageKey.SNMP_USERNAME);
				ok = false;
			} else if (config.getAuth() != AuthProtocol.NONE
					&& !StringUtil.isLengthOk(config.getAuthPassword(),
							AppConfig.getSnmpPasswordMinLength())) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						WebUtil.getMessage(MessageKey.PASSWORD_MINLENGTH)
								+ " : " + AppConfig.getSnmpPasswordMinLength());
				ok = false;
			} else {
				if (config.getPriv() != PrivProtocol.NONE
						&& !StringUtil.isLengthOk(config.getPrivPassword(),
								AppConfig.getSnmpPasswordMinLength())) {
					WebUtil.addMessage(
							FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							WebUtil.getMessage(MessageKey.PASSWORD_MINLENGTH)
									+ " : "
									+ AppConfig.getSnmpPasswordMinLength());
					ok = false;
				}
			}
		}
		return ok;
	}

	// getter&setter

	public List<ContactPerson> getContacts() {
		return contacts;
	}

	public void setContacts(List<ContactPerson> contacts) {
		this.contacts = contacts;
	}

	public List<GroupSnmpConfig> getSnmps() {
		return snmps;
	}

	public void setSnmps(List<GroupSnmpConfig> snmps) {
		this.snmps = snmps;
	}

	public ContactPerson getDummyContact() {
		return dummyContact;
	}

	public void setDummyContact(ContactPerson dummyContact) {
		this.dummyContact = dummyContact.getBackup();
		contact = dummyContact;
	}

	public ContactPerson getNewContact() {
		return newContact;
	}

	public void setNewContact(ContactPerson newContact) {
		this.newContact = newContact;
	}

	public GroupSnmpConfig getNewSnmp() {
		return newSnmp;
	}

	public void setNewSnmp(GroupSnmpConfig newSnmp) {
		this.newSnmp = newSnmp;
	}

	public GroupSnmpConfig getDummySnmp() {
		return dummySnmp;
	}

	public void setDummySnmp(GroupSnmpConfig dummySnmp) {
		this.dummySnmp = dummySnmp.getBackup();
		snmp = dummySnmp;
	}
}
