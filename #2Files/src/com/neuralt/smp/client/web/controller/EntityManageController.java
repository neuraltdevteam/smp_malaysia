package com.neuralt.smp.client.web.controller;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import com.neuralt.smp.client.MonitoredEntity.EntityType;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.web.util.jsf.WebUtil;

public abstract class EntityManageController {
	EntityType type;

	void throwDBExceptionMessage() {
		WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
				MessageKey.DB_ACCESS_ERROR, null);
	}

	void throwAgentExceptionMessage(String name) {
		FacesMessage message = new FacesMessage();
		message.setSummary("Cannot connect to agent at:" + name);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
	}

	void throwDuplicatedNameMessage(EntityType type) {
		WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
				MessageKey.GENERAL_ERROR, MessageKey.DUPLICATED_NAME);
	}

	void throwCustomErrorMessage(String messageStr) {
		WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
				MessageKey.GENERAL_ERROR, messageStr);
	}
}
