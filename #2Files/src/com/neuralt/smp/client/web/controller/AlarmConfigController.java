package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.data.OidMap;
import com.neuralt.smp.client.data.config.AlarmDefinition;
import com.neuralt.smp.client.data.config.AlarmMap;
import com.neuralt.smp.client.data.config.AlarmMap.AlarmMapType;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.client.event.SmpEvent.EventType;
import com.neuralt.smp.client.service.AlarmService;
import com.neuralt.smp.client.util.OidUtil;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.snmp.util.MibFileLoader;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class AlarmConfigController implements Serializable {

	private static final long serialVersionUID = -4509418658125400577L;

	private static final Logger logger = LoggerFactory
			.getLogger(AlarmConfigController.class);

	private List<AlarmDefinition> alarmDefinitions;
	private List<AlarmDefinition> filteredAlarmDefinitions; // used by dataTable
	private AlarmDefinition selectedAlarmDef;
	// mappings associated with selectedAlarmDef
	// mappings from events
	private List<AlarmMapKeyRow> allEventsRows;
	private List<AlarmMapKeyRow> filteredEventsRows;
	private List<AlarmMapValueRow> eventAlarmMapRows;
	private List<AlarmMapValueRow> filteredEventAlarmMapRows;
	private AlarmMapValueRow selectedEventMapRow;
	// mappings from traps
	private List<AlarmMapKeyRow> allOidsRows;
	private List<AlarmMapKeyRow> filteredOidsRows;
	private List<AlarmMapValueRow> trapAlarmMapRows;
	private List<AlarmMapValueRow> filteredTrapAlarmMapRows;
	private AlarmMapValueRow selectedAlarmMapRow;

	private boolean defaultTrapAlarmEnabled;
	private WarningLevel defaultTrapAlarmThreshold;

	public AlarmConfigController() {
		// do onPageLoad here.
		refresh();
		createAllOidsRows();
		createAllEventsRows();
		defaultTrapAlarmEnabled = AlarmService.instance
				.isDefaultTrapAlarmEnabled();
		defaultTrapAlarmThreshold = AlarmService.instance
				.getDefaultTrapAlarmThreshold();
	}

	public void refresh() {
		alarmDefinitions = DataAccess.alarmConfigDao.listAlarmDefinitions();
	}

	// /// alarm definition /////
	public void prepareAddAlarmDef() {
		clearAlarmDefSelection();
		selectedAlarmDef = new AlarmDefinition();
	}

	public void saveAlarmDef() {
		if (selectedAlarmDef != null) {
			logger.debug("saveAlarmDef: " + selectedAlarmDef.getName());
			try {
				if (WarningLevel.CLEAR.equals(selectedAlarmDef.getLevel())) {
					List<AlarmDefinition> defList = DataAccess.alarmConfigDao
							.getAlarmDefinition(selectedAlarmDef.getName());
					for (AlarmDefinition def : defList) {

					}
				}
				AlarmService.instance.saveAlarmDefinition(selectedAlarmDef);
				refresh();
				if (selectedAlarmDef.getId() == null) {
					AuditLogger.logAuditTrail(ScreenType.CONFIG,
							ActionType.INSERT, "add-alarm definition",
							selectedAlarmDef.getName());
				} else {
					AuditLogger.logAuditTrail(ScreenType.CONFIG,
							ActionType.UPDATE, "edit-alarm definition",
							selectedAlarmDef.getName());
				}
				WebUtil.addMessage(MessageKey.ALARM_CONFIG_DEF_SAVED,
						selectedAlarmDef.getName(), FacesMessage.SEVERITY_INFO);
			} catch (Exception e) {
				WebUtil.addMessage(MessageKey.GENERAL_ERROR, e.getMessage(),
						FacesMessage.SEVERITY_ERROR);
				logger.error("saveAlarmDef failed", e);
			}
		}
	}

	public void deleteAlarmDef() {
		if (selectedAlarmDef != null) {
			logger.debug("deleteAlarmDef: " + selectedAlarmDef.getName());
			boolean ok = true;

			for (NotificationGroup group : DataAccess.notificationGroupDao
					.getAllGroups()) {
				if (group.getAlarms().contains(selectedAlarmDef)) {
					WebUtil.addMessage(
							MessageKey.GENERAL_ERROR,
							"Cannot delete alarm definition "
									+ selectedAlarmDef.getName()
									+ " as it is using by some Notification Group",
							FacesMessage.SEVERITY_ERROR);
					ok = false;
					break;
				}
			}

			if (ok) {
				try {
					// for(AlarmMap alarmMap:selectedAlarmDef.getAlarmMaps()){
					// System.err.println("deleting: " + alarmMap);
					// DataAccess.alarmConfigDao.delete(alarmMap);
					// }
					// DataAccess.alarmConfigDao.delete(selectedAlarmDef);
					// SmpAlarmService.instance.reloadAlarmDefinitions();
					AlarmService.instance
							.deleteAlarmDefinition(selectedAlarmDef);
					refresh();
					AuditLogger.logAuditTrail(ScreenType.CONFIG,
							ActionType.DELETE, "delete-alarm definition",
							selectedAlarmDef.getName());
					WebUtil.addMessage(
							MessageKey.GENERAL_SUCCESS,
							"Alarm Definition deleted:"
									+ selectedAlarmDef.getName(),
							FacesMessage.SEVERITY_INFO);
					clearAlarmDefSelection();
				} catch (Exception e) {
					WebUtil.addMessage(
							MessageKey.GENERAL_ERROR,
							"Cannot delete alarm definition "
									+ selectedAlarmDef.getName() + " :"
									+ e.getMessage(),
							FacesMessage.SEVERITY_ERROR);
					logger.error("deleteAlarmDef failed:" + e);
				}
			}
		}
	}

	// /// alarm mapping /////
	public void onAlarmDefSelect(SelectEvent event) {
		createAlarmMapValueRows();
		createAllOidsRows();
		createAllEventsRows();
	}

	private void createAllEventsRows() {
		allEventsRows = new ArrayList<AlarmMapKeyRow>();
		Set<String> usedCauseIds = new HashSet<String>();
		if (selectedAlarmDef != null) {
			List<AlarmMap> maps = AlarmService.instance
					.getEventAlarmMaps(selectedAlarmDef);
			if (maps != null && !maps.isEmpty()) {
				for (AlarmMap map : maps) {
					usedCauseIds.add(map.getCauseId());
				}
			}
		}
		List<EventType> eventTypes = CentralizedEventService.getInstance()
				.listAllEventTypes();
		for (EventType type : eventTypes) {
			AlarmMapKeyRow row = new AlarmMapKeyRow(type);
			String causeId = type.asCauseId();
			if (!usedCauseIds.contains(causeId)) {
				// check if used by other alarms
				AlarmMap existingMap = AlarmService.instance
						.findEventAlarmMapping(type);
				if (existingMap != null) {
					row.enabled = false;
					row.remarks = "mapped to: "
							+ existingMap.getMappedAlarm().getKeyString();
				}
				allEventsRows.add(row);
			}
		}
	}

	private void createAllOidsRows() {
		allOidsRows = new ArrayList<AlarmMapKeyRow>();
		Map<OID, OidMap> fullOidMap = OidUtil.getInstance().getFullOidMap();
		Set<String> usedCauseIds = new HashSet<String>();
		if (selectedAlarmDef != null) {
			List<AlarmMap> maps = AlarmService.instance
					.getTrapAlarmMaps(selectedAlarmDef);
			if (maps != null && !maps.isEmpty()) {
				for (AlarmMap map : maps) {
					usedCauseIds.add(map.getCauseId());
				}
			}
		}
		List<OidMap> oidMapList = new ArrayList<OidMap>(fullOidMap.values());
		Collections.sort(oidMapList);
		for (OidMap oidMap : oidMapList) {
			String trapName = oidMap.getCurrentRecognizableName();
			String trapOid = oidMap.getOidAsString();
			if (oidMap.isNoMap() && !usedCauseIds.contains(trapOid)) {
				allOidsRows.add(new AlarmMapKeyRow(oidMap.getOid(), trapName));
			}
		}
	}

	private void createAlarmMapValueRows() {
		String trapName ;
		trapAlarmMapRows = new ArrayList<AlarmMapValueRow>();

		if (selectedAlarmDef != null) {
			List<AlarmMap> trapMappings = AlarmService.instance
					.getTrapAlarmMaps(selectedAlarmDef);
			if (trapMappings != null && !trapMappings.isEmpty()) {
				// group by causeIds, then convert into rows
				LinkedHashMap<String, List<AlarmMap>> causeIdIndex = new LinkedHashMap<String, List<AlarmMap>>();
				for (AlarmMap map : trapMappings) {
					List<AlarmMap> list = causeIdIndex.get(map.getCauseId());
					if (list == null) {
						list = new ArrayList<AlarmMap>();
						causeIdIndex.put(map.getCauseId(), list);
					}
					list.add(map);
				}
				for (Entry<String, List<AlarmMap>> entry : causeIdIndex
						.entrySet()) {
					OID oid = new OID(entry.getKey());
					logger.debug("oid={}, name={}", oid, MibFileLoader
							.getInstance().getAllTrapNameMap().get(oid));
				    // fix trap name not shown bug, 29/10/2015 by jimmy.so
					// if trap has no name assigned in database, use oid instead
					if (MibFileLoader.getInstance().getAllTrapNameMap().get(oid) == null ){
						trapName =  entry.getKey();
					}
					else 
					{
						trapName = MibFileLoader.getInstance().getAllTrapNameMap().get(oid);
					}
					AlarmMapValueRow newRow = new AlarmMapValueRow(trapName, entry.getValue());
//					AlarmMapValueRow newRow = new AlarmMapValueRow(
//							MibFileLoader.getInstance().getAllTrapNameMap()
//									.get(oid), entry.getValue());
					// End ,  29/10/2015 by jimmy.so
					newRow.oid = oid;
					trapAlarmMapRows.add(newRow);
				}
			}

			eventAlarmMapRows = new ArrayList<AlarmMapValueRow>();
			List<AlarmMap> evtMappings = AlarmService.instance
					.getEventAlarmMaps(selectedAlarmDef);
			if (evtMappings != null && !evtMappings.isEmpty()) {
				for (AlarmMap map : evtMappings) {
					eventAlarmMapRows.add(new AlarmMapValueRow(
							map.getCauseId(), map));
				}
			}
		}
	}

	private void clearAlarmDefSelection() {
		selectedAlarmDef = null;
		trapAlarmMapRows = null;
		eventAlarmMapRows = null;
	}

	public void saveAlarmMap() {
		if (selectedAlarmMapRow != null) {
			try {
				// AlarmMaps to add
				List<AlarmMap> newEntries = new ArrayList<AlarmMap>();
				// AlarmMaps to del
				List<AlarmMap> delEntries = new ArrayList<AlarmMap>();
				List<WarningLevel>[] diff = WarningLevel.difference(
						selectedAlarmMapRow.getSelectedLevels(),
						selectedAlarmMapRow.mappedLevels());
				for (WarningLevel level : diff[0]) {
					logger.debug("new level=" + level);
					newEntries.add(new AlarmMap(AlarmMap.AlarmMapType.TRAP,
							selectedAlarmMapRow.oid.toString(), level,
							selectedAlarmDef));
				}
				for (WarningLevel level : diff[1]) {
					logger.debug("deleted level=" + level);
					delEntries.add(selectedAlarmMapRow.getAlarmMap(level));
				}
				DataAccess.alarmConfigDao.batchPersist(newEntries, delEntries);
				AlarmService.instance.reloadAlarmMappings();
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
						"edit-alarm map", selectedAlarmDef.getName());
				WebUtil.addMessage(MessageKey.ALARM_CONFIG_MAP_SAVED,
						selectedAlarmMapRow.getCauseId(),
						FacesMessage.SEVERITY_INFO);
				refresh();
				for (AlarmDefinition def : alarmDefinitions) {
					if (def.getId().equals(selectedAlarmDef.getId())) {
						selectedAlarmDef = def;
						break;
					}
				}
				createAlarmMapValueRows();
				createAllOidsRows();
			} catch (Exception e) {
				WebUtil.addMessage(MessageKey.GENERAL_ERROR, e.getMessage(),
						FacesMessage.SEVERITY_ERROR);
				logger.error("saveAlarmMap", e);
			}
		}
	}

	public void deleteAlarmMap() {
		if (selectedAlarmMapRow != null) {
			try {
				DataAccess.alarmConfigDao
						.batchDelete(selectedAlarmMapRow.values);
				String name = selectedAlarmMapRow.getCauseId();
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
						"delete-alarm map", selectedAlarmDef.getName());
				WebUtil.addMessage(MessageKey.ALARM_CONFIG_MAP_DELETED, name,
						FacesMessage.SEVERITY_INFO);
				selectedAlarmMapRow = null;
				selectedAlarmDef = AlarmService.instance
						.reloadAlarmDefinition(selectedAlarmDef);
				alarmDefinitions = AlarmService.instance.listAlarmDefinitions();
				createAlarmMapValueRows();
				createAllOidsRows();
				createAllEventsRows();
			} catch (Exception e) {
				WebUtil.addMessage(MessageKey.GENERAL_ERROR, e.getMessage(),
						FacesMessage.SEVERITY_ERROR);
				logger.error("deleteAlarmMap", e);
			}
		}
	}

	public void prepareAddAlarmMap(AlarmMapKeyRow selectedCause) {
		selectedAlarmMapRow = new AlarmMapValueRow();
		selectedAlarmMapRow.causeId = selectedCause.getCauseId();
		selectedAlarmMapRow.oid = selectedCause.getOid();
	}

	public void prepareEditAlarmMap(AlarmMapValueRow selectedCause) {
		selectedAlarmMapRow.oid = selectedCause.oid;
	}

	public void addEventAlarmMap(AlarmMapKeyRow selectedEventRow) {
		try {
			AlarmMap newEntry = new AlarmMap(AlarmMap.AlarmMapType.EVENT,
					selectedEventRow.causeId, null, selectedAlarmDef);
			DataAccess.alarmConfigDao.save(newEntry);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-alarm map", selectedAlarmDef.getName());
			selectedAlarmDef = AlarmService.instance
					.reloadAlarmDefinition(selectedAlarmDef);
			alarmDefinitions = AlarmService.instance.listAlarmDefinitions();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.ALARM_CONFIG_MAP_ADDED,
					selectedEventRow.getCauseId());
			createAlarmMapValueRows();
			createAllEventsRows();
		} catch (Exception e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, e.getMessage());
			logger.error("addEventAlarmMap", e);
		}
	}

	public List<SelectItem> getAlarmMapLevelOptions() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		if (selectedAlarmMapRow != null) {
			for (WarningLevel level : WarningLevel.concreteValues()) {
				SelectItem item = new SelectItem(level, level.name());
				AlarmMap existing = AlarmService.instance.findTrapAlarmMapping(
						selectedAlarmMapRow.causeId, level);
				if (existing != null
						&& !StringUtil.objeql(selectedAlarmDef,
								existing.getMappedAlarm())) {
					// this level of this trap's been mapped to a different
					// Alarm
					item.setDisabled(true);
					item.setLabel(level.name() + " -> "
							+ existing.getMappedAlarm().getKeyString());
				}
				items.add(item);
			}
		}
		return items;// .toArray(new SelectItem[items.size()]);
	}

	public void saveDefaultTrapAlarmConfig() {
		try {
			AlarmService.instance
					.setDefaultTrapAlarmEnabled(defaultTrapAlarmEnabled);
			AlarmService.instance
					.setDefaultTrapAlarmThreshold(defaultTrapAlarmThreshold);
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.ALARM_CONFIG_DEFAULT_SETTING_SAVED, null);
		} catch (Exception e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, e.getMessage());
			logger.error("saveDefaultTrapAlarmConfig", e);
		}
	}

	// get/setters
	public AlarmDefinition getSelectedAlarmDef() {
		return selectedAlarmDef;
	}

	public void setSelectedAlarmDef(AlarmDefinition selectedAlarmDef) {
		this.selectedAlarmDef = selectedAlarmDef;
	}

	public List<AlarmDefinition> getAlarmDefinitions() {
		return alarmDefinitions;
	}

	public List<AlarmMapKeyRow> getAllOidsRows() {
		return allOidsRows;
	}

	public List<AlarmMapValueRow> getTrapAlarmMapRows() {
		return trapAlarmMapRows;
	}

	public AlarmMapValueRow getSelectedAlarmMapRow() {
		return selectedAlarmMapRow;
	}

	public List<AlarmMapKeyRow> getFilteredOidsRows() {
		return filteredOidsRows;
	}

	public void setFilteredOidsRows(List<AlarmMapKeyRow> filteredOidsRows) {
		this.filteredOidsRows = filteredOidsRows;
	}

	public List<AlarmMapValueRow> getFilteredTrapAlarmMapRows() {
		return filteredTrapAlarmMapRows;
	}

	public void setFilteredTrapAlarmMapRows(
			List<AlarmMapValueRow> filteredTrapAlarmMapRows) {
		this.filteredTrapAlarmMapRows = filteredTrapAlarmMapRows;
	}

	public void setSelectedAlarmMapRow(AlarmMapValueRow selectedAlarmMapRow) {
		this.selectedAlarmMapRow = selectedAlarmMapRow;
	}

	public List<AlarmDefinition> getFilteredAlarmDefinitions() {
		return filteredAlarmDefinitions;
	}

	public void setFilteredAlarmDefinitions(
			List<AlarmDefinition> filteredAlarmDefinitions) {
		this.filteredAlarmDefinitions = filteredAlarmDefinitions;
	}

	public List<AlarmMapKeyRow> getFilteredEventsRows() {
		return filteredEventsRows;
	}

	public void setFilteredEventsRows(List<AlarmMapKeyRow> filteredEventsRows) {
		this.filteredEventsRows = filteredEventsRows;
	}

	public List<AlarmMapValueRow> getFilteredEventAlarmMapRows() {
		return filteredEventAlarmMapRows;
	}

	public void setFilteredEventAlarmMapRows(
			List<AlarmMapValueRow> filteredEventAlarmMapRows) {
		this.filteredEventAlarmMapRows = filteredEventAlarmMapRows;
	}

	public AlarmMapValueRow getSelectedEventMapRow() {
		return selectedEventMapRow;
	}

	public void setSelectedEventMapRow(AlarmMapValueRow selectedEventMapRow) {
		this.selectedEventMapRow = selectedEventMapRow;
	}

	public List<AlarmMapKeyRow> getAllEventsRows() {
		return allEventsRows;
	}

	public List<AlarmMapValueRow> getEventAlarmMapRows() {
		return eventAlarmMapRows;
	}

	public boolean isDefaultTrapAlarmEnabled() {
		return defaultTrapAlarmEnabled;
	}

	public void setDefaultTrapAlarmEnabled(boolean defaultTrapAlarmEnabled) {
		this.defaultTrapAlarmEnabled = defaultTrapAlarmEnabled;
	}

	public WarningLevel getDefaultTrapAlarmThreshold() {
		return defaultTrapAlarmThreshold;
	}

	public void setDefaultTrapAlarmThreshold(
			WarningLevel defaultTrapAlarmThreshold) {
		this.defaultTrapAlarmThreshold = defaultTrapAlarmThreshold;
	}

	// TODO doable with a single Row class?
	/**
	 * Wraps around Oid/Event that maps to AlarmMap
	 */
	public static class AlarmMapKeyRow implements Serializable {

		private static final long serialVersionUID = -1229342666305880044L;

		public AlarmMap.AlarmMapType keyType;
		public OID oid;
		public EventType eventType;
		public String causeId;
		public String remarks;
		public boolean enabled = true;

		public AlarmMapKeyRow(OID oid, String trapName) {
			this.keyType = AlarmMapType.TRAP;
			this.oid = oid;
			this.causeId = trapName;
		}

		public AlarmMapKeyRow(EventType eventType) {
			this.keyType = AlarmMapType.EVENT;
			this.eventType = eventType;
			this.causeId = eventType.toString();
		}

		public OID getOid() {
			return oid;
		}

		public String getOidStr() {
			if (oid == null)
				return null;
			else
				return oid.toString();
		}

		public EventType getEventType() {
			return eventType;
		}

		public String getCauseId() {
			return causeId;
		}

		public String getRemarks() {
			return remarks;
		}

		public boolean isEnabled() {
			return enabled;
		}
	}

	/**
	 * Wraps around AlarmMaps
	 */
	public static class AlarmMapValueRow implements Serializable {

		private static final long serialVersionUID = -1674629113058220498L;

		public List<AlarmMap> values;
		public List<WarningLevel> selectedLevels;
		public String causeId;
		public OID oid;

		public AlarmMapValueRow() {
		}

		public AlarmMapValueRow(String causeId, List<AlarmMap> values) {
			this.causeId = causeId;
			this.values = values;
			selectedLevels = mappedLevels();
		}

		public AlarmMapValueRow(String causeId, AlarmMap value) {
			this.causeId = causeId;
			this.values = new ArrayList<AlarmMap>();
			values.add(value);
			selectedLevels = mappedLevels();
		}

		public List<WarningLevel> getSelectedLevels() {
			return selectedLevels;
		}

		// apparently jsf/primefaces turns the values into a collection of
		// strings. have to convert it.
		public void setSelectedLevels(List<String> selectedLevels) {
			this.selectedLevels = new ArrayList<WarningLevel>();
			for (String str : selectedLevels) {
				try {
					this.selectedLevels.add(WarningLevel.valueOf(str));
				} catch (Exception e) {
					// safely ignore.
				}
			}
		}

		public List<WarningLevel> mappedLevels() {
			List<WarningLevel> mappedLevels = new ArrayList<WarningLevel>();
			if (values != null && !values.isEmpty()) {
				for (AlarmMap value : values) {
					if (value.getCauseLevel() != null)
						mappedLevels.add(value.getCauseLevel());
				}
			}
			return mappedLevels;
		}

		public String getLevelStr() {
			List<WarningLevel> levels = getSelectedLevels();
			if (levels == null || levels.isEmpty()) {
				return "";
			}
			List<String> strs = new ArrayList<String>();
			for (WarningLevel level : levels) {
				strs.add(level.name());
			}
			return StringUtil.concat(strs, ", ");
		}

		public AlarmMap getAlarmMap(WarningLevel level) {
			if (values != null && !values.isEmpty()) {
				for (AlarmMap map : values) {
					if (map.getCauseLevel() == level)
						return map;
				}
			}
			return null;
		}

		public String getCauseId() {
			return causeId;
		}
	}
}
