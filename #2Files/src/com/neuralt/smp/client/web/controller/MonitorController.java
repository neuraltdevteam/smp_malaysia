package com.neuralt.smp.client.web.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.primefaces.context.RequestContext;
import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.chart.CartesianChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.LiveMonitorReporter;
import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.ChartTemplate;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.EventLog;
import com.neuralt.smp.client.service.AlarmService;
import com.neuralt.smp.client.util.ChartTemplateUtil;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.CssStyleUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class MonitorController implements Serializable {
	private static final long serialVersionUID = 5602076834478350046L;

	private static final String AUDIO_OPTION_COOKIE = "SMP_HOME_AUDIO";

	private static final Logger logger = LoggerFactory
			.getLogger(MonitorController.class);
	private TreeNode entityRoot;
	private TreeNode selectedNode; // for TreeTable's own good health

	private MonitoredEntity selectedEntity;
	private MonitoredSystem selectedSystem;
	private MonitoredHost selectedHost;
	private MonitoredProcess selectedProcess;

	private String selectedTemplate;
	private boolean stopPoll;
	private boolean stopSubPoll;
	private boolean audioOn = true;
	private int pollInterval;
	private int subPollInterval = AppConfig.getWebMonitorSubPollInterval();
	private DashboardModel dashboard;
	private List<ChartTemplate> templates;
	private CartesianChartModel graphOne;
	private CartesianChartModel graphTwo;
	private CartesianChartModel graphThree;
	private CartesianChartModel graphFour;
	private ChartTemplate template1;
	private ChartTemplate template2;
	private ChartTemplate template3;
	private ChartTemplate template4;

	private boolean hasUnhandledAlarm;

	public MonitorController() {
		dashboard = new DefaultDashboardModel();
		DashboardColumn column1 = new DefaultDashboardColumn();
		DashboardColumn column2 = new DefaultDashboardColumn();
		DashboardColumn column3 = new DefaultDashboardColumn();
		column1.addWidget("graph_b");
		column2.addWidget("graph_c");
		column3.addWidget("graph_d");
		dashboard.addColumn(column1);
		dashboard.addColumn(column2);
		dashboard.addColumn(column3);

		createTreeTableModel();
		createCharts();
		stopSubPoll = true;
		pollInterval = AppConfig.getWebMonitorPollInterval();

		ExternalContext extContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		setAudioOptionFromCookie(extContext);
	}

	private void setAudioOptionFromCookie(ExternalContext extContext) {
		Map<String, Object> cookies = extContext.getRequestCookieMap();
		Cookie cookie = (Cookie) cookies.get(AUDIO_OPTION_COOKIE);
		if (cookie != null) {
			String option = cookie.getValue();
			audioOn = Boolean.valueOf(option);
		}
	}

	private void createCharts() {
		templates = DataAccess.chartTemplateDao.getAvailableChartTemplates();
		// for (ChartTemplate template : templates) {
		// logger.debug("available template is added to monitor:"+template);
		// }
		for (ChartTemplate template : templates)
			genChart(template);
	}

	private void genChart(ChartTemplate template) {
		try {
			switch (template.getPriority()) {
			case 1:
				graphOne = ChartTemplateUtil.generateChart(template);
				template1 = template;
				// logger.debug("graphOne="+template.getName());
				break;
			case 2:
				graphTwo = ChartTemplateUtil.generateChart(template);
				template2 = template;
				// logger.debug("graphTwo="+template.getName());
				break;
			case 3:
				graphThree = ChartTemplateUtil.generateChart(template);
				template3 = template;
				// logger.debug("graphThree="+template.getName());
				break;
			case 4:
				graphFour = ChartTemplateUtil.generateChart(template);
				template4 = template;
				// logger.debug("graphFour="+template.getName());
				break;
			}
		} catch (IOException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.RRD_ACCESS_FAIL);
		}
	}

	private void createTreeTableModel() {
		entityRoot = new DefaultTreeNode("root", null);
		entityRoot.setExpanded(true);

		for (MonitoredSystem sys : SmpClient.instance.getSystems()) {
			if (sys.getConfig().isEnabled()) {
				TreeNode sysNode = new DefaultTreeNode(sys.getEntityType()
						.name(), sys, entityRoot);
				sysNode.setExpanded(true);
				for (MonitoredHost host : sys.getHosts()) {
					if (host.getConfig().isEnabled()) {
						TreeNode hostNode = new DefaultTreeNode(host
								.getEntityType().name(), host, sysNode);
						hostNode.setExpanded(true);
						for (MonitoredProcess proc : host.getProcesses()) {
							if (proc.getConfig().isEnabled()) {
								TreeNode procNode = new DefaultTreeNode(proc
										.getEntityType().name(), proc, hostNode);
								procNode.setExpanded(true);
							}
						}
					}
				}
			}
		}
	}

	public void live() {
		try {
			ExternalContext extContext = FacesContext.getCurrentInstance()
					.getExternalContext();

			String url = extContext.getRequestContextPath()
					+ "/pages/monitor/live.xhtml?selectedSystem="
					+ selectedSystem.getName();
			if (selectedHost != null)
				url = url + "&selectedHost="
						+ selectedHost.getConfig().getName();
			extContext.redirect(url);
		} catch (IOException e) {
			logger.error("Failed to redirect to live.xhtml", e);
		}
	}

	public void redirectAlarmMgmt() {
		try {
			ExternalContext extContext = FacesContext.getCurrentInstance()
					.getExternalContext();

			String url = extContext.getRequestContextPath()
					+ "/pages/monitor/alarmMgmt.xhtml";
			extContext.redirect(url);
		} catch (IOException e) {
			logger.error("Failed to redirect to alarmMgmt.xhtml", e);
		}
	}

	public void redirectHistory() {
		try {
			ExternalContext extContext = FacesContext.getCurrentInstance()
					.getExternalContext();

			String url = extContext.getRequestContextPath()
					+ "/pages/monitor/history.xhtml";
			extContext.redirect(url);
		} catch (IOException e) {
			logger.error("Failed to redirect to alarmMgmt.xhtml", e);
		}
	}

	public void redirectNewTemplateConfig(int priority) {
		try {
			ExternalContext extContext = FacesContext.getCurrentInstance()
					.getExternalContext();

			String url = extContext.getRequestContextPath()
					+ "/pages/config/template.xhtml?priority=" + priority;
			extContext.redirect(url);
		} catch (IOException e) {
			logger.error("Failed to redirect to template.xhtml", e);
		}
	}

	public void redirectTemplateConfig(ChartTemplate template, int priority) {
		try {
			ExternalContext extContext = FacesContext.getCurrentInstance()
					.getExternalContext();

			String url = extContext.getRequestContextPath()
					+ "/pages/config/template.xhtml?priority=" + priority
					+ "&selectedTemplate=" + template.getId();
			extContext.redirect(url);
		} catch (IOException e) {
			logger.error("Failed to redirect to template.xhtml", e);
		}
	}

	public MonitoredEntity getSelectedEntity() {
		return selectedEntity;
	}

	public void setSelectedEntity(MonitoredEntity selectedEntity) {
		this.selectedEntity = selectedEntity;

		switch (selectedEntity.getEntityType()) {
		case SYSTEM:
			selectedSystem = (MonitoredSystem) selectedEntity;
			selectedHost = null;
			selectedProcess = null;
			break;
		case HOST:
			selectedHost = (MonitoredHost) selectedEntity;
			selectedProcess = null;
			selectedSystem = selectedHost.getSys();
			break;
		case PROCESS:
			selectedProcess = (MonitoredProcess) selectedEntity;
			selectedHost = selectedProcess.getHost();
			selectedSystem = selectedHost.getSys();
			break;
		}
	}

	public void onClickLive() {
		live();
	}

	public void onClickAdminLink(MonitoredEntity entity) {
		try {
			ExternalContext extContext = FacesContext.getCurrentInstance()
					.getExternalContext();

			MonitoredProcess proc = (MonitoredProcess) entity;
			String url = proc.getConfig().getAdminLink();
			extContext.redirect(url);
		} catch (IOException e) {
			logger.error("Failed to redirect to admin link", e);
		}
	}

	public void onPoll() {
		FacesContext context = FacesContext.getCurrentInstance();

		for (FacesMessage facesMsg : LiveMonitorReporter.getInstance()
				.getGrowlMessages()) {
			if (facesMsg != null)
				context.addMessage(null, facesMsg);
		}
		// createCharts();
		LiveMonitorReporter.getInstance().getGrowlMessages().clear();

		checkUnhandledAlarms();
	}

	public void onSubPoll() {
		startSubPoll(false);
	}

	private void startSubPoll(boolean isStartSubPoll) {
		if (pollInterval < subPollInterval) {
			this.stopPoll = isStartSubPoll;
			this.stopSubPoll = !isStartSubPoll;
		}
	}

	public void onEntitySelect(NodeSelectEvent event) {
		TreeNode entityNode = event.getTreeNode();
		setSelectedEntity((MonitoredEntity) entityNode.getData());

		startSubPoll(true);
	}

	public void startSystem() {
		startSubPoll(false);

		if (isSysStartable()) {
			SmpClient.instance.startProcesses(selectedSystem.getName());
			List<AuditTrailLog> batch = new LinkedList<AuditTrailLog>();
			for (MonitoredHost host : selectedSystem.getHosts()) {
				for (MonitoredProcess proc : host.getProcesses()) {
					batch.add(AuditLogger.createAuditTrail(ScreenType.MONITOR,
							WebUtil.getRequestPath(), ActionType.START_PROCESS,
							"start-system", proc.getFullName()));
				}
			}
			AuditLogger.logBatch(batch);
		}
	}

	public void stopSystem() {
		startSubPoll(false);

		if (isSysStoppable()) {
			SmpClient.instance.stopProcesses(selectedSystem.getName());
			List<AuditTrailLog> batch = new LinkedList<AuditTrailLog>();
			for (MonitoredHost host : selectedSystem.getHosts()) {
				for (MonitoredProcess proc : host.getProcesses()) {
					batch.add(AuditLogger.createAuditTrail(ScreenType.MONITOR,
							WebUtil.getRequestPath(), ActionType.STOP_PROCESS,
							"stop-system", proc.getFullName()));
				}
			}
			AuditLogger.logBatch(batch);
		}
	}

	public void startHost() {
		startSubPoll(false);

		if (isHostStartable()) {
			SmpClient.instance.startProcesses(selectedSystem.getName(),
					selectedHost.getConfig().getName());
			List<AuditTrailLog> batch = new LinkedList<AuditTrailLog>();
			for (MonitoredProcess proc : selectedHost.getProcesses()) {
				batch.add(AuditLogger.createAuditTrail(ScreenType.MONITOR,
						WebUtil.getRequestPath(), ActionType.START_PROCESS,
						"start-host", proc.getFullName()));
			}
			AuditLogger.logBatch(batch);
		}
	}

	public void stopHost() {
		startSubPoll(false);

		if (isHostStoppable()) {
			SmpClient.instance.stopProcesses(selectedSystem.getName(),
					selectedHost.getConfig().getName());
			List<AuditTrailLog> batch = new LinkedList<AuditTrailLog>();
			for (MonitoredProcess proc : selectedHost.getProcesses()) {
				batch.add(AuditLogger.createAuditTrail(ScreenType.MONITOR,
						WebUtil.getRequestPath(), ActionType.STOP_PROCESS,
						"stop-host", proc.getFullName()));
			}
			AuditLogger.logBatch(batch);
		}
	}

	public void startProcess() {
		startSubPoll(false);

		if (isProcessStartable()) {
			SmpClient.instance.startProcess(selectedSystem.getName(),
					selectedHost.getConfig().getName(),
					selectedProcess.getName());
			AuditLogger.logAuditTrail(ScreenType.MONITOR,
					WebUtil.getRequestPath(), ActionType.START_PROCESS,
					"start-process", selectedProcess.getFullName());
		}
	}

	public void stopProcess() {
		startSubPoll(false);

		if (isProcessStoppable()) {
			SmpClient.instance.stopProcess(selectedSystem.getName(),
					selectedHost.getConfig().getName(),
					selectedProcess.getName());
			AuditLogger.logAuditTrail(ScreenType.MONITOR,
					WebUtil.getRequestPath(), ActionType.STOP_PROCESS,
					"stop-process", selectedProcess.getFullName());
		}
	}

	public boolean isLiveViewable() {
		return selectedSystem != null || selectedHost != null;
	}

	public boolean isSysStartable() {
		boolean ok = false;

		if (selectedSystem != null && selectedSystem == selectedEntity) {
			for (MonitoredHost host : selectedSystem.getHosts()) {
				ok = isHostCompletelyStopped(host);
				if (!ok)
					break;
			}
		}

		return ok;
	}

	public boolean isSysStoppable() {
		boolean ok = false;

		if (selectedSystem != null && selectedSystem == selectedEntity) {
			for (MonitoredHost host : selectedSystem.getHosts()) {
				ok = isHostCompletelyStarted(host);
				if (!ok)
					break;
			}
		}

		return ok;
	}

	public boolean isHostStartable() {
		boolean ok = false;

		if (selectedHost != null && selectedHost == selectedEntity) {
			ok = isHostCompletelyStopped(selectedHost)
					&& selectedHost.getConfig().isAgentDeployed();
		}

		return ok;
	}

	private boolean isHostCompletelyStopped(MonitoredHost selectedHost) {
		boolean stopped = false;
		for (MonitoredProcess process : selectedHost.getProcesses())
			if (process.isStopped()) {
				stopped = true;
			} else {
				stopped = false;
				break;
			}
		return stopped;
	}

	public boolean isHostStoppable() {
		boolean ok = false;

		if (selectedHost != null && selectedHost == selectedEntity) {
			ok = isHostCompletelyStarted(selectedHost)
					&& selectedHost.getConfig().isAgentDeployed();
		}

		return ok;
	}

	private boolean isHostCompletelyStarted(MonitoredHost selectedHost) {
		boolean started = false;
		for (MonitoredProcess process : selectedHost.getProcesses())
			if (process.isRunning()) {
				started = true;
			} else {
				started = false;
				break;
			}
		return started;
	}

	public boolean isProcessStartable() {
		return (selectedProcess != null && selectedProcess == selectedEntity
				&& !selectedProcess.isRunning() && selectedProcess.getHost()
				.getConfig().isAgentDeployed());
	}

	public boolean isProcessStoppable() {
		return (selectedProcess != null && selectedProcess == selectedEntity
				&& selectedProcess.isRunning() && selectedProcess.getHost()
				.getConfig().isAgentDeployed());
	}

	public boolean isAdminLinkAvailable(MonitoredEntity entity) {
		if (entity instanceof MonitoredProcess) {
			return !StringUtil.isNullOrEmpty(((MonitoredProcess) entity)
					.getConfig().getAdminLink());
		}
		return false;
	}

	public TreeNode getEntityRoot() {
		return entityRoot;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public MonitoredSystem getSelectedSystem() {
		return selectedSystem;
	}

	public MonitoredHost getSelectedHost() {
		return selectedHost;
	}

	public MonitoredProcess getSelectedProcess() {
		return selectedProcess;
	}

	public String getStatusCssClass(String statusText) {
		String result = CssStyleUtil.getStatusCssClass(statusText);
		if (result == null)
			result = "";

		return result;
	}

	public List<EventLog> getHotEvents() {
		List<EventLog> list = new LinkedList<EventLog>();

		for (EventLog eventLog : LiveMonitorReporter.getInstance()
				.getHotEvents().descendingSet()) {
			eventLog.setAge(getAge(eventLog.getEventTime().getTime()));
			list.add(eventLog);
		}

		return list;
	}

	public List<Alarm> getHotAlarms() {
		List<Alarm> list = new LinkedList<Alarm>();
		// logger.debug("start adding alarm...set size="+LiveMonitorReporter.getInstance().getHotAlarms().size());

		for (Alarm alarm : LiveMonitorReporter.getInstance().getHotAlarms()
				.descendingSet()) {
			alarm.setAge(getAge(alarm.getLastOccurred().getTime()));
			list.add(alarm);
			// logger.debug("alarm id:"+alarm.getId());
		}
		// logger.debug("add alarm finished...list size="+list.size());
		return list;
	}

	private String getAge(long then) {
		long now = System.currentTimeMillis();
		long diff = (now - then) / 1000;

		if (diff <= 0)
			return "now";

		StringBuilder sb = new StringBuilder();
		if (diff >= (60 * 60 * 24)) {
			sb.append(diff / (60 * 60 * 24)).append("d");
			diff = diff % (60 * 60 * 24);
		}
		if (diff >= (60 * 60)) {
			sb.append(diff / (60 * 60)).append("h");
			diff = diff % (60 * 60);
		}
		if (diff >= 60) {
			sb.append(diff / 60).append("m");
			diff = diff % 60;
		}
		if (diff > 0)
			sb.append(diff % 60).append("s");

		return sb.toString();
	}

	public boolean isStopPoll() {
		return stopPoll;
	}

	public void setStopPoll(boolean stopPoll) {
		this.stopPoll = stopPoll;
	}

	public boolean isAudio() {
		return audioOn;
	}

	public void setAudio(boolean audio) {
		this.audioOn = audio;

		ExternalContext extContext = FacesContext.getCurrentInstance()
				.getExternalContext();
		HttpServletResponse response = (HttpServletResponse) extContext
				.getResponse();
		Cookie cookie = new Cookie(AUDIO_OPTION_COOKIE, Boolean.toString(audio));
		cookie.setMaxAge(-1); // Expire time. -1 = by end of current session, 0
								// = immediately expire it, otherwise just the
								// lifetime in seconds.
		response.addCookie(cookie);

		switchAlarm();
	}

	public int getPollInterval() {
		return pollInterval;
	}

	public void setPollInterval(int pollInterval) {
		this.pollInterval = pollInterval;
	}

	public int getSubPollInterval() {
		return subPollInterval;
	}

	public void setSubPollInterval(int subPollInterval) {
		this.subPollInterval = subPollInterval;
	}

	public boolean isStopSubPoll() {
		return stopSubPoll;
	}

	public void setStopSubPoll(boolean stopSubPoll) {
		this.stopSubPoll = stopSubPoll;
	}

	public DashboardModel getDashboard() {
		return dashboard;
	}

	public List<ChartTemplate> getTemplates() {
		return templates;
	}

	public CartesianChartModel getGraphOne() {
		return graphOne;
	}

	public CartesianChartModel getGraphTwo() {
		return graphTwo;
	}

	public CartesianChartModel getGraphThree() {
		return graphThree;
	}

	public CartesianChartModel getGraphFour() {
		return graphFour;
	}

	public String getSelectedTemplate() {
		return selectedTemplate;
	}

	public void setSelectedTemplate(String selectedTemplate) {
		this.selectedTemplate = selectedTemplate;
	}

	public void checkUnhandledAlarms() {
		List<Alarm> list = AlarmService.instance.getUnresolvedAlarms();
		if (list != null && list.size() > 0) {
			hasUnhandledAlarm = true;

			WebUtil.addMessage(FacesMessage.SEVERITY_WARN, "Unresolved alarms",
					"Please handle unresolved alarms under Fault Management");

			switchAlarm();
		} else {
			hasUnhandledAlarm = false;
			switchAlarm();
		}
	}

	private void switchAlarm() {
		// System.err.println(String.format("hasUnhandledAlarm(%s:%s): ",
		// hasUnhandledAlarm, audioOn));
		if (hasUnhandledAlarm && audioOn) {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("alarmOn();");
		} else {
			RequestContext context = RequestContext.getCurrentInstance();
			context.execute("alarmOff();");
		}
	}

	public boolean hasUnhandledAlarm() {
		return hasUnhandledAlarm && audioOn;
	}

	public ChartTemplate getTemplate1() {
		return template1;
	}

	public void setTemplate1(ChartTemplate template1) {
		this.template1 = template1;
	}

	public ChartTemplate getTemplate2() {
		return template2;
	}

	public void setTemplate2(ChartTemplate template2) {
		this.template2 = template2;
	}

	public ChartTemplate getTemplate3() {
		return template3;
	}

	public void setTemplate3(ChartTemplate template3) {
		this.template3 = template3;
	}

	public ChartTemplate getTemplate4() {
		return template4;
	}

	public void setTemplate4(ChartTemplate template4) {
		this.template4 = template4;
	}

}
