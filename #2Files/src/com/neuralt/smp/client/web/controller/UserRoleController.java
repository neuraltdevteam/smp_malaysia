package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.hibernate.LazyInitializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.UserRightComparator;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.web.security.UserBean;
import com.neuralt.smp.client.web.security.UserRight;
import com.neuralt.smp.client.web.security.UserRight.AccessLevel;
import com.neuralt.smp.client.web.security.UserRole;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.web.util.jsf.LocaleMessages;
import com.neuralt.web.util.jsf.MessageUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class UserRoleController implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(UserRoleController.class);

	@ManagedProperty(value = "#{UserBean}")
	private UserBean userBean;

	private List<UserRole> userRoles;
	private List<UserRight> selectedRights; // rights current role has

	private UserRole selectedRole;
	private UserRight selectedRight;
	private String selectedRoleId;

	private UserRole prev;
	private Map<ScreenType, AccessLevel> prevRights;

	private List<String> levelList;
	private List<String> normalLevelList = new ArrayList<String>();
	private List<String> writeLevelList = new ArrayList<String>();

	private boolean createNew;
	private boolean showAddDialog;

	public UserRoleController() {
		setUserRoles(DataAccess.webDao.getUserRoles());
		for (AccessLevel level : UserRight.AccessLevel.values()) {
			normalLevelList.add(level.name());
		}
		writeLevelList.add(UserRight.AccessLevel.WRITE.name());
	}

	// actions //

	public void selectRole(UserRole role) {
		selectedRole = role;
		refreshRights();
	}

	public void addRole() {
		selectedRole = new UserRole();
	}

	public void deleteRole() {
		logger.debug("deleteRole: {}", selectedRole);
		if (selectedRole.getRoleId().equals(ConstantUtil.SUPER_ROLE)) {
			// Not allow to delete super role
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.ROLE_MODIFY_FAILED_SUPERROLE);
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE, ActionType.DELETE, false, "UserRole",
			// selectedRole.getRoleId(), message, null, null);
			selectedRole = null;
			return;
		}
		try {
			DataAccess.webDao.removeUserRole(selectedRole);
			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					WebUtil.getRequestPath(), ActionType.DELETE, "delete-role",
					selectedRole.getRoleId());
		} catch (DataAccessException dbe) {
			logger.warn("failed in accessing DB");
			String message = addErrorMsg("privilege.roleDeleteError");
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE, ActionType.DELETE, false, "UserRole",
			// selectedRole.getRoleId(), message, null, null);
			MessageUtil.logDBException("deleteRole", dbe);
			refreshRoles();
			return;
		}
		refreshRoles();
		addInfoMsg("privilege_roleDeleteSuccess");
		// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
		// ActionCategory.PRIVILEGE, ActionType.DELETE, true, "UserRole",
		// selectedRole.getRoleId(), null, null, null);
		selectedRole = null;
		selectedRights = null;
	}

	public void saveRole() {
		logger.debug("saveRole: {}", selectedRole);
		if (selectedRole.getRoleId().equals(ConstantUtil.SUPER_ROLE)) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.ROLE_MODIFY_FAILED_SUPERROLE);
			return;
		}
		if (failsValidation(selectedRole)) {
			return;
		}
		if (!createNew) {
			prev = DataAccess.webDao.getUserRole(selectedRole.getRoleId());
		}
		selectedRole.setLastUpdatedBy(userBean.getUser() != null ? userBean
				.getUser().getUserId() : null);

		try {
			DataAccess.webDao.saveUserRole(selectedRole);
		} catch (Exception e) {
			logger.debug("Exception: ", e);
			String message = addErrorMsg("privilege.roleSaveFailure",
					MessageUtil.getDescriptionOfDBException(new Exception(e)),
					new Date());
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE,
			// createNew?ActionType.CREATE:ActionType.UPDATE,
			// false, "UserRole", createNew?null:selectedRole.getRoleId(),
			// message, null, null);
			refreshRoles();
			return;
		}
		refreshRoles();

		// Session session = HibernateUtil.getCurrentSession();
		// session.beginTransaction();
		try {
			// case when role is newly created
			if (selectedRole.getUserRights() == null
					|| selectedRole.getUserRights().isEmpty()) {
				for (ScreenType type : ScreenType.getEnum("A")) {
					// logger.debug("screenType: {}",type.toString());
					selectedRole.addUserRight(new UserRight(type,
							UserRight.AccessLevel.NONE, selectedRole
									.getRoleId()));
				}
			}
			DataAccess.webDao.saveUserRole(selectedRole);
			// session.getTransaction().commit();
		} catch (Throwable e) {
			// session.getTransaction().rollback();
			logger.debug("Exception: ", e);
			String message = addErrorMsg("privilege.roleSaveFailure",
					MessageUtil.getDescriptionOfDBException(new Exception(e)),
					new Date());
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE, ActionType.UPDATE, false, "UserRole",
			// selectedRole.getRoleId(), message, null, null);
			refreshRoles();
			refreshRights();
			return;
		}
		addInfoMsg("privilege.roleSaveSuccess");
		// String[] differences = {"",""};
		// if (!createNew) {
		// DiffComparator diff = new DiffComparator();
		// differences = diff.compare("UserRole",prev,selectedRole);
		// }
		// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
		// ActionCategory.PRIVILEGE, ActionType.UPDATE, true, "UserRole",
		// selectedRole.getRoleId(), null, differences[0], differences[1]);

		if (createNew) {
			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					WebUtil.getRequestPath(), ActionType.INSERT, "add-role",
					selectedRole.getRoleId());
		} else {
			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					WebUtil.getRequestPath(), ActionType.UPDATE, "edit-role",
					selectedRole.getRoleId());
		}

		refreshRoles();
		refreshRights();
	}

	public void selectRight(UserRight right) {
		selectedRight = right;
	}

	public void editRightsInRole() {
		logger.debug("editRights:{} in role{}", selectedRights, selectedRole);
		logger.debug("prevRights: (editing) {}", prevRights);
		// selectedRole.setUserRights(new HashSet<UserRight>(selectedRights));
		try {
			DataAccess.webDao.saveUserRole(selectedRole);
			AuditLogger.logAuditTrail(ScreenType.MANAGE_USER,
					WebUtil.getRequestPath(), ActionType.UPDATE, "edit-role",
					selectedRole.getRoleId());
		} catch (Exception e) {
			String message = addErrorMsg("privilege.roleSaveFailure",
					MessageUtil.getDescriptionOfDBException(new Exception(e)),
					new Date());
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE, ActionType.UPDATE, false, "UserRole",
			// selectedRole.getRoleId(), message, null, null);
			refreshRoles();
			refreshRights();
			return;
		}
		addInfoMsg("privilege.rightEdit");
		// String[] differences = compareRights(prevRights, selectedRights);
		// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
		// ActionCategory.PRIVILEGE, ActionType.UPDATE, true, "UserRole",
		// selectedRole.getRoleId(), null,
		// "UserRight modified:<br/> "+differences[0],
		// "UserRight modified: <br/>"+differences[1]);
		refreshRoles();
		refreshRights();
	}

	private String[] compareRights(Map<ScreenType, AccessLevel> prev,
			List<UserRight> currList) {
		String[] diff = { "", "" };
		for (int i = 0; i < prev.size(); i++) {
			if (!prev.get(currList.get(i).getTypeId()).equals(
					currList.get(i).getAccessLevel())) {
				diff[0] = diff[0] + currList.get(i).getTypeId() + "="
						+ prev.get(currList.get(i).getTypeId());
				diff[1] = diff[1] + currList.get(i).getTypeId() + "="
						+ currList.get(i).getAccessLevel();
				if (i < prev.size() - 1) {
					diff[0] = diff[0] + ";";
					diff[1] = diff[1] + ";";
				}

			}
		}
		return diff;
	}

	public void refreshRoles() {
		setUserRoles(DataAccess.webDao.getUserRoles());
	}

	public void refreshRightsWhenScreenChanges() {
		// Session session = HibernateUtil.getCurrentSession();
		// session.beginTransaction();
		try {

			List<ScreenType> newTypeList = new ArrayList<ScreenType>();
			for (ScreenType type : ScreenType.getEnum("A")) {
				newTypeList.add(type);
			}
			logger.debug("newlist: {}", newTypeList);

			List<ScreenType> oldTypeList = new ArrayList<ScreenType>();
			for (UserRight right : selectedRole.getUserRights()) {
				if (right.getTypeId().active.equals("A"))
					oldTypeList.add(right.getTypeId());
			}
			logger.debug("oldlist: {}", oldTypeList);
			List<ScreenType> removeTypeList = new ArrayList<ScreenType>();
			for (ScreenType type : ScreenType.getEnum("I")) {
				removeTypeList.add(type);
			}
			logger.debug("removelist: {}", removeTypeList);
			newTypeList.removeAll(oldTypeList); // set difference, list of
												// typeids to be saved to db
			logger.debug("after, newlist: {}", newTypeList);
			// case when new screens are made or removed in ScreenType, but not
			// reflected in db yet
			if (!newTypeList.isEmpty()) {
				logger.debug("New screens are made.");

				// add new page
				for (ScreenType newType : newTypeList) {
					UserRight right = new UserRight(newType,
							UserRight.AccessLevel.NONE,
							selectedRole.getRoleId());
					selectedRole.addUserRight(right);
					if (!isRoleDeletable(selectedRole)) {
						right.setAccessLevel(UserRight.AccessLevel.WRITE);
					}
				}
				selectedRole.setLastUpdatedBy(userBean.getUser().getUserId());
				addInfoMsg("privilege.rightAdd");
				// AuditLogger.persistAuditTrail(ScreenId.PRIVILEGE_USER,
				// ActionCategory.PRIVILEGE, ActionType.UPDATE, true,
				// "UserRole", selectedRole.getRoleId(), null, "",
				// "UserRights: "+newTypeList+" added");
			}
			if (!removeTypeList.isEmpty()) {
				logger.debug("Some screens have been removed. ");
				for (ScreenType remove : removeTypeList) {
					List<UserRight> removeRight = DataAccess.webDao
							.getUserRights(remove);
					List<UserRight> actualRemove = new ArrayList<UserRight>();
					for (UserRight right : removeRight)
						if (selectedRole.getUserRights().contains(right)) {
							selectedRole.removeUserRight(right);
							addInfoMsg("privilege.rightRefresh");
							actualRemove.add(right);
						}
					// AuditLogger.persistAuditTrail(ScreenId.PRIVILEGE_USER,
					// ActionCategory.PRIVILEGE, ActionType.UPDATE, true,
					// "UserRole", selectedRole.getRoleId(), null, "",
					// "UserRights: "+actualRemove+" removed");
					removeRight.clear();
					actualRemove.clear();
				}
			}
			selectedRights = new ArrayList<UserRight>(
					selectedRole.getUserRights());
			Collections.sort(selectedRights, new UserRightComparator());
			DataAccess.webDao.saveUserRole(selectedRole);
			// session.getTransaction().commit();
			oldTypeList.clear();
			newTypeList.clear();
			removeTypeList.clear();
		} catch (Throwable e) {
			// session.getTransaction().rollback();
			String message = addErrorMsg("privilege.rightRefresh_error");
			// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
			// ActionCategory.PRIVILEGE, ActionType.UPDATE, false, "UserRole",
			// selectedRole.getRoleId(), message, null, null);
			logger.debug("Exception: ", new Exception(e));
		}
	}

	public void refreshRights() {
		// prevRights = new UserRightController().findByRole(selectedRole);
		// Session session = HibernateUtil.getCurrentSession();
		// session.beginTransaction();
		try {
			// DataAccess.webDao.saveUserRole(selectedRole);
			selectedRights = new ArrayList<UserRight>(
					selectedRole.getUserRights());
			Collections.sort(selectedRights, new UserRightComparator());
			logger.debug("rights: {}", selectedRights);
			if (prevRights == null)
				prevRights = new HashMap<ScreenType, AccessLevel>();
			else
				prevRights.clear();
			for (UserRight right : selectedRights)
				prevRights.put(right.getTypeId(), right.getAccessLevel());
			logger.debug("prev rights: {}", prevRights);
			// session.getTransaction().commit();
		} catch (LazyInitializationException lie) {
			// session.getTransaction().rollback();
			refreshRightsWhenScreenChanges();
		} catch (IllegalArgumentException e) {
			// session.getTransaction().rollback();
			refreshRightsWhenScreenChanges();
		} catch (Throwable e) {
			// session.getTransaction().rollback();
			logger.debug("Exception: ", new Exception(e));
		}
	}

	private String addInfoMsg(String msgKey) {
		FacesMessage message = LocaleMessages.getMessage(msgKey);
		message.setSeverity(FacesMessage.SEVERITY_INFO);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return message.getSummary();
	}

	private String addErrorMsg(String msgKey) {
		FacesMessage message = LocaleMessages.getMessage(msgKey);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return message.getSummary();
	}

	private String addErrorMsg(String msgKey, String e, Date date) {
		FacesMessage message = LocaleMessages.getMessage(msgKey, e, date);
		message.setSeverity(FacesMessage.SEVERITY_ERROR);
		FacesContext.getCurrentInstance().addMessage(null, message);
		return message.getSummary();
	}

	// validation //

	public boolean failsValidation(UserRole role) {
		return roleExistsAlready(role);
	}

	public boolean roleExistsAlready(UserRole newRole) {
		for (UserRole role : userRoles) {
			if (selectedRole != newRole)
				if (role.getRoleId().equals(newRole.getRoleId())
						&& role != newRole) {
					String message = addErrorMsg("privilege.roleExistsAlready");
					// AuditLogger.logAuditTrail(ScreenId.PRIVILEGE_USER,
					// ActionCategory.PRIVILEGE, ActionType.CREATE, false,
					// "UserRole",
					// selectedRole.getRoleId(), message, null, null);
					return true; // roleid exists already, bad
				}
		}
		return false; // roleid doesnt exist yet, ok
	}

	// getters setters //

	public List<UserRight> getSelectedRights() {
		// Session session = HibernateUtil.getCurrentSession();
		// session.beginTransaction();
		try {
			// DataAccess.webDao.saveUserRole(selectedRole);
			if (selectedRights == null && selectedRole != null
					&& selectedRole.getUserRights() != null) {
				selectedRights = new ArrayList<UserRight>(
						selectedRole.getUserRights());
			}
			// session.getTransaction().commit();
		} catch (Throwable e) {
			// session.getTransaction().rollback();
			// addErrorMsg("error_selecting_roles");
			logger.error("getSelectedRights", e);
		}
		return selectedRights;
	}

	public void setSelectedRights(List<UserRight> selectedRights) {
		this.selectedRights = selectedRights;
	}

	public List<UserRole> getUserRoles() {
		if (userRoles == null) {
			userRoles = DataAccess.webDao.getUserRoles();
		}
		return userRoles;
	}

	public void setUserRoles(List<UserRole> userRoles) {
		this.userRoles = userRoles;
	}

	public UserRole getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(UserRole selectedRole) {
		this.selectedRole = selectedRole;
	}

	public UserRight getSelectedRight() {
		return selectedRight;
	}

	public void setSelectedRight(UserRight selectedRight) {
		this.selectedRight = selectedRight;
	}

	public List<String> getLevelList() {
		if (!isRoleDeletable(selectedRole)) {
			return writeLevelList;
		}
		return normalLevelList;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	public boolean isCreateNew() {
		return createNew;
	}

	public void setCreateNew(boolean createNew) {
		this.createNew = createNew;
	}

	public void setSelectedRoleId(String selectedRoleId) {
		logger.debug("selectedroleid set");
		this.selectedRoleId = selectedRoleId;
		if (userRoles == null)
			setUserRoles(DataAccess.webDao.getUserRoles());
		for (UserRole role : userRoles) {
			if (role.getRoleId().equals(selectedRoleId)) {
				selectRole(role);
				logger.debug("Role {} selected", role);
			}
		}
	}

	public String getSelectedRoleId() {
		return selectedRoleId;
	}

	public boolean isShowAddDialog() {
		return showAddDialog;
	}

	public void setShowAddDialog(boolean showAddDialog) {
		this.showAddDialog = showAddDialog;
		if (showAddDialog)
			selectedRole = new UserRole();
	}

	public boolean isRoleDeletable(UserRole role) {
		if (role != null)
			return !role.getRoleId().equals(ConstantUtil.SUPER_ROLE);
		return false;
	}

	public boolean isUserRolesEmpty() {
		return userRoles == null || userRoles.isEmpty();
	}
}
