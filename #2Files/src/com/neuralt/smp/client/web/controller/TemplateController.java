package com.neuralt.smp.client.web.controller;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;
import org.primefaces.model.chart.CartesianChartModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.ChartTemplate;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.RrdArchiveConfig;
import com.neuralt.smp.client.data.RrdArchiveConfig.TimeType;
import com.neuralt.smp.client.data.RrdDefConfig;
import com.neuralt.smp.client.data.RrdDsConfig;
import com.neuralt.smp.client.util.ChartTemplateUtil;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class TemplateController implements Serializable {
	private static final long serialVersionUID = -6094474773257557274L;
	private static final Logger logger = LoggerFactory
			.getLogger(TemplateController.class);
	private static final int DESIRABLE_SAMPLE_NUMBER = 60;

	private List<String> defList;
	private List<String> selectedDef;
	private Map<String, RrdDefConfig> defMap;
	private List<String> archiveList;
	private List<String> selectedArchive;
	private Map<String, RrdArchiveConfig> archiveMap;
	private List<String> dsList;
	private List<String> selectedDs;
	private Map<String, RrdDsConfig> dsMap;
	private TreeNode root;
	private TreeNode selectedNode;
	private ChartTemplate template;
	private boolean showGraph;
	private CartesianChartModel graph;
	private int priority;
	private long selectedTemplate;
	private long oldTemplate;

	public TemplateController() {
		defList = new ArrayList<String>();
		archiveList = new ArrayList<String>();
		dsList = new ArrayList<String>();
		selectedDef = new ArrayList<String>();
		selectedDs = new ArrayList<String>();
		selectedArchive = new ArrayList<String>();
		defMap = new HashMap<String, RrdDefConfig>();
		archiveMap = new HashMap<String, RrdArchiveConfig>();
		dsMap = new HashMap<String, RrdDsConfig>();
		createTreeTable();
		init();
	}

	private void init() {
		for (RrdDefConfig def : DataAccess.rrdDefConfigDao
				.getAllLockedRrdDefs()) {
			defList.add(def.getName());
			defMap.put(def.getName(), def);
			for (RrdDsConfig ds : def.getRrdDsConfigs()) {
				dsMap.put(concatName(def.getName(), ds.getName()), ds);
			}
			for (RrdArchiveConfig archive : def.getRrdArchiveConfigs()) {
				archiveMap.put(concatName(def.getName(), archive.getName()),
						archive);
			}
		}
	}

	private void createTreeTable() {
		root = new DefaultTreeNode("root", null);
		template = new ChartTemplate();
		TreeNode defaultNode = new DefaultTreeNode(template, root);
		defaultNode.setExpanded(true);

		for (ChartTemplate template : DataAccess.chartTemplateDao
				.getAllChartTemplatesForCurrentUser()) {
			TreeNode node = new DefaultTreeNode(template, root);
			node.setExpanded(true);
		}
		selectedDs.clear();
		selectedDef.clear();
		selectedArchive.clear();
		dsList.clear();
		archiveList.clear();
		showGraph = false;
	}

	public void onNodeSelected(NodeSelectEvent event) {
		clearTemplate();
		selectedTemplate = 0;
		showGraph = false;
		dsList.clear();
		archiveList.clear();
		selectedDef.clear();
		selectedDs.clear();
		selectedArchive.clear();
		if (event.getTreeNode().getData() instanceof ChartTemplate) {
			template = (ChartTemplate) event.getTreeNode().getData();
			template.setEnabled(true);
			createDsList();
			createArchiveList();
			refreshList();
		} else {
		}
	}

	private void clearTemplate() {
		if (template.getId() == 0) {
			template.setName(null);
			template.setEnabled(false);
			template.setPriority(0);
			template.setTimeRange(0);
			template.setVerticalLabel(null);
			template.setTimeRangeType(TimeType.SECOND);
			template.setArchiveConfigs(new TreeSet<RrdArchiveConfig>());
			template.setDsConfigs(new TreeSet<RrdDsConfig>());
		}
	}

	public void prepareAddTemplate() {
		logger.debug("preparing new template...");
		template = new ChartTemplate();
	}

	private void refreshList() {
		for (String defStr : selectedDef) {
			RrdDefConfig def = defMap.get(defStr);
			for (RrdDsConfig ds : def.getRrdDsConfigsAsList()) {
				String displayName = concatName(def.getName(), ds.getName());
				if (!dsList.contains(displayName)) {
					dsList.add(displayName);
				}
			}
			for (RrdArchiveConfig archive : def.getRrdArchiveConfigsAsList()) {
				String displayName = concatName(def.getName(),
						archive.getName());
				if (!archiveList.contains(displayName)) {
					archiveList.add(displayName);
				}
			}
		}
		Collections.sort(dsList);
		Collections.sort(archiveList);
	}

	private void createDsList() {
		logger.debug("ds count=" + template.getDsConfigs().size());
		for (RrdDsConfig ds : template.getDsConfigsAsList()) {
			String displayName = concatName(ds.getRrdDef().getName(),
					ds.getName());
			// logger.debug("template has ds:"+displayName);
			if (!selectedDs.contains(displayName))
				selectedDs.add(displayName);
			if (!selectedDef.contains(ds.getRrdDef().getName())) {
				selectedDef.add(ds.getRrdDef().getName());
			}
		}
		Collections.sort(selectedDs);
	}

	private void createArchiveList() {
		logger.debug("archive count=" + template.getArchiveConfigs().size());
		for (RrdArchiveConfig archive : template.getArchiveConfigsAsList()) {
			String displayName = concatName(archive.getRrdDef().getName(),
					archive.getName());
			// logger.debug("template has archive:"+displayName);
			if (!selectedArchive.contains(displayName))
				selectedArchive.add(displayName);
			if (!selectedDef.contains(archive.getRrdDef().getName())) {
				selectedDef.add(archive.getRrdDef().getName());
			}
		}
		Collections.sort(selectedArchive);
	}

	public void preview() {
		template.getDsConfigs().clear();
		for (String dsStr : selectedDs) {
			RrdDsConfig ds = dsMap.get(dsStr);
			template.getDsConfigs().add(ds);
		}
		template.getArchiveConfigs().clear();
		for (String archiveStr : selectedArchive) {
			RrdArchiveConfig archive = archiveMap.get(archiveStr);
			template.getArchiveConfigs().add(archive);
		}
		if (template.isConfigValid()) {
			showGraph = true;
			try {
				graph = ChartTemplateUtil.generateChart(template);
			} catch (IOException e) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR, MessageKey.RRD_ACCESS_FAIL);
			}
		} else {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.TEMPLATE_CONIG_INVAILD);
		}
	}

	public void saveTemplate() {
		boolean ok = true;
		ChartTemplate dummy = DataAccess.chartTemplateDao
				.getTemplateByName(template.getName());
		if (dummy != null && !dummy.equals(template)) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.DUPLICATED_NAME);
		}
		if (ok) {
			if (oldTemplate > 0) {
				dummy = DataAccess.chartTemplateDao.findByKey(oldTemplate);
				dummy.setEnabled(false);
				DataAccess.chartTemplateDao.save(dummy);
			}
			template.setPriority(priority);
			template.getDsConfigs().clear();
			for (String dsStr : selectedDs) {
				RrdDsConfig ds = dsMap.get(dsStr);
				template.getDsConfigs().add(ds);
			}
			template.getArchiveConfigs().clear();
			for (String archiveStr : selectedArchive) {
				RrdArchiveConfig archive = archiveMap.get(archiveStr);
				template.getArchiveConfigs().add(archive);
			}
			if (template.isConfigValid()) {
				template.setUser(WebUtil.getCurrentUser());
				DataAccess.chartTemplateDao.save(template);
				createTreeTable();
				redirectDashboard();
			} else {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						MessageKey.TEMPLATE_CONIG_INVAILD);
			}
		}
	}

	public void redirectDashboard() {
		try {
			ExternalContext extContext = FacesContext.getCurrentInstance()
					.getExternalContext();

			String url = extContext.getRequestContextPath()
					+ "/pages/home.xhtml";
			extContext.redirect(url);
		} catch (IOException e) {
			logger.error("Failed to redirect to home.xhtml", e);
		}
	}

	public void deleteTemplate() {
		if (template.getId() > 0)
			DataAccess.chartTemplateDao.delete(template);
		createTreeTable();
	}

	private String concatName(String def, String other) {
		return def + " - " + other;
	}

	// getter/setter
	public List<String> getSelectedDef() {
		return selectedDef;
	}

	public void setSelectedDef(List<String> selectedDef) {
		this.selectedDef = selectedDef;
		// generate dsList & archiveList, also remove some selectedDs &
		// selectedArchive
		dsList.clear();
		archiveList.clear();
		for (String defStr : selectedDef) {
			RrdDefConfig def = defMap.get(defStr);
			for (RrdDsConfig ds : def.getRrdDsConfigsAsList()) {
				dsList.add(concatName(def.getName(), ds.getName()));
			}
			for (RrdArchiveConfig archive : def.getRrdArchiveConfigsAsList()) {
				archiveList.add(concatName(def.getName(), archive.getName()));
			}
		}
		List<String> dummy = new ArrayList<String>();
		for (String dsStr : selectedDs) {
			if (!dsList.contains(dsStr)) {
				dummy.add(dsStr);
			}
		}
		for (String dummyStr : dummy) {
			selectedDs.remove(dummyStr);
		}
		dummy.clear();
		for (String archiveStr : selectedArchive) {
			if (!archiveList.contains(archiveStr)) {
				dummy.add(archiveStr);
			}
		}
		for (String dummyStr : dummy) {
			selectedArchive.remove(dummyStr);
		}
		autoSelectArchive();
		Collections.sort(dsList);
		Collections.sort(archiveList);
	}

	/**
	 * This method intends to generate an archive based on the time range of
	 * template and the selected definitions. It will consider the ratio of
	 * retention period of an archive to the time range of template, as well as
	 * the number of data points of the archive will be shown. It will find out
	 * the archive which can provide maximum score and assign it to the selected
	 * archive list.<br/>
	 * <code>score = Min(1,ratio) * number of data points</code> <br/>
	 * It is possible that it does not provide such a suggestion or the
	 * suggestion seems unreasonable
	 */
	private void autoSelectArchive() {
		for (String defStr : selectedDef) {
			RrdDefConfig def = defMap.get(defStr);
			RrdArchiveConfig chosen = null;
			int period = RrdArchiveConfig.translateTime(
					template.getTimeRange(), template.getTimeRangeType());
			float score = 0f;
			for (RrdArchiveConfig archive : def.getRrdArchiveConfigsAsList()) {
				int archivePeriod = def.getStep() * archive.getStep()
						* archive.getRow();
				float multiplier = 0f;
				if (period != 0) {
					multiplier = Math.min(1, (float) archivePeriod / period);
				}
				int numberOfRecord = Math.min(archive.getRow(),
						(int) ((float) period / archive.getStep() / def
								.getStep()));
				if ((numberOfRecord * multiplier) > score) {
					score = numberOfRecord * multiplier;
					chosen = archive;
				}
			}
			if (chosen != null) {
				String autoSelectedArchive = concatName(def.getName(),
						chosen.getName());
				if (!selectedArchive.contains(autoSelectedArchive)) {
					selectedArchive.add(autoSelectedArchive);
				}
			}
		}
	}

	public List<String> getSelectedArchive() {
		return selectedArchive;
	}

	public void setSelectedArchive(List<String> selectedArchive) {
		this.selectedArchive = selectedArchive;
	}

	public List<String> getSelectedDs() {
		return selectedDs;
	}

	public void setSelectedDs(List<String> selectedDs) {
		this.selectedDs = selectedDs;
	}

	public List<String> getDefList() {
		return defList;
	}

	public List<String> getArchiveList() {
		return archiveList;
	}

	public List<String> getDsList() {
		return dsList;
	}

	public TreeNode getRoot() {
		return root;
	}

	public ChartTemplate getTemplate() {
		// logger.debug("getting template....selectedTemplate="+selectedTemplate);
		if (selectedTemplate != 0) {
			for (TreeNode node : root.getChildren()) {
				if (node.getData() instanceof ChartTemplate) {
					ChartTemplate dummy = (ChartTemplate) node.getData();
					if (selectedTemplate == (dummy.getId())) {
						// logger.debug("template found:"+dummy);
						selectedNode = node;
						template = dummy;
						selectedTemplate = 0;
						createDsList();
						createArchiveList();
						refreshList();
						break;
					}
				}
			}
		}
		return template;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public boolean isShowGraph() {
		return showGraph;
	}

	public CartesianChartModel getGraph() {
		return graph;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public long getSelectedTemplate() {
		return selectedTemplate;
	}

	public void setSelectedTemplate(long selectedTemplate) {
		this.selectedTemplate = selectedTemplate;
	}

	public long getOldTemplate() {
		return oldTemplate;
	}

	public void setOldTemplate(long oldTemplate) {
		this.oldTemplate = oldTemplate;
	}

	public void redirectDataDefinition() {
		try {
			ExternalContext extContext = FacesContext.getCurrentInstance()
					.getExternalContext();

			String url = extContext.getRequestContextPath()
					+ "/pages/config/datadefinition.xhtml";
			extContext.redirect(url);
		} catch (IOException e) {
			logger.error("Failed to redirect to datadefinition.xhtml", e);
		}
	}
}
