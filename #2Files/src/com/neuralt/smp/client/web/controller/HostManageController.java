package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.net.MalformedURLException;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.AgentRuntimeException;
import com.neuralt.smp.agent.mbean.model.NetworkInterface;
import com.neuralt.smp.client.MonitoredEntity.EntityType;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.HostMonitorName;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig.MonitorThresholdConfig;
import com.neuralt.smp.config.HostConfig.MonitorType;
import com.neuralt.smp.config.HostSnmpConfig;
import com.neuralt.smp.config.HostSnmpConfig.TargetType;
import com.neuralt.smp.config.Site;
import com.neuralt.smp.config.model.BaseSnmpConfig.AuthProtocol;
import com.neuralt.smp.config.model.BaseSnmpConfig.PrivProtocol;
import com.neuralt.smp.config.model.BaseSnmpConfig.SnmpVersion;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.rrd.RrdService;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class HostManageController extends EntityManageController implements
		Serializable {

	private static final Logger logger = LoggerFactory
			.getLogger(HostManageController.class);
	private static final long serialVersionUID = -3406418993539209199L;

	private HostConfig host;
	private Site selectedSite;

	public HostManageController() {
		resetHost();
		type = EntityType.HOST;
	}

	public void editHost(MonitoredHost monitoredHost, HostConfig dummyHost) {
		logger.debug("dummyHost=" + dummyHost);
		logger.debug("site=" + dummyHost.getSite());
		// check for duplicate name
		boolean ok = true;
		if (!monitoredHost.getConfig().getName().equals(dummyHost.getName())) {
			try {
				if (SmpClient.instance.getHost(
						monitoredHost.getSys().getName(), dummyHost.getName()) != null) {
				}
				ok = false;
				throwDuplicatedNameMessage(type);
			} catch (IllegalArgumentException e) {
				if (!isIpOk(dummyHost)) {
					ok = false;
				}
			}
		} else {
			if (!isIpOk(dummyHost)) {
				ok = false;
			}
		}
		if (isHostConfigValid(ok, dummyHost)) {
			HostConfig backupHost = monitoredHost.getBackupHostConfig();
			monitoredHost.updateConfig(dummyHost);
			logger.debug("after update:" + monitoredHost.getConfig());
			try {
				SmpClient.instance.reloadHost(backupHost, monitoredHost);
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
						"edit-host", dummyHost.getLogDetail());
			} catch (DataAccessException e) {
				// throw error msg & rollback
				monitoredHost.updateConfig(backupHost);
				logger.warn("failed in accessing DB");
			} catch (Exception e) {
				logger.debug("failed in reloading host for edit", e);
			}
			logger.debug("after done:" + monitoredHost.getConfig());
		}
	}

	public void addHost(MonitoredSystem selectedSystem) {
		logger.debug("addHost invoked");
		// verify
		boolean ok = true;
		try {
			if (SmpClient.instance.getHost(selectedSystem.getName(), host.getName()) != null) {
				ok = false;
				throwDuplicatedNameMessage(type);
			}
		} catch (IllegalArgumentException e) {
			if (!isIpOk(host)) {
				ok = false;
			}
		}

		if (isHostConfigValid(ok, host)) {
			host.generateDefaultMonitors();
			logger.debug("selectedSite name=" + selectedSite.getSiteName());
			if (selectedSite.getSiteName().equals(SystemEditer.selectSiteName)) {
				host.setSite(null);
			} else {
				host.setSite(selectedSite);
			}
			try {
				host.setSystemConfig(selectedSystem.getConfig());
				//DataAccess.systemConfigDao.save(selectedSystem.getConfig());
				DataAccess.hostConfigDao.save(host);
				MonitoredHost monitoredHost = new MonitoredHost(host,
						selectedSystem);
				SmpClient.instance.addHost(monitoredHost);
				selectedSystem.addHost(monitoredHost);
				if (host.getSite() != null) {
					selectedSite.getHosts().add(monitoredHost.getConfig());
					DataAccess.siteDao.save(selectedSite);
				}
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
						"add-host", host.getLogDetail());
				resetHost();
			} catch (MalformedURLException e) {
				logger.debug("failed in adding host", e);
			} catch (DataAccessException e) {
				logger.warn("failed in accessing DB");
			}
		}

	}

	private boolean isHostConfigValid(boolean ok, HostConfig config) {
		if (!ok) {
			return false;
		} else {
			if (config.getType() == MonitorType.SCRIPT
					|| config.getType() == MonitorType.BOTH) {
				config.setAgentDeployed(true);
			} else {
				config.setAgentDeployed(false);
			}
			// if (config.getType()==MonitorType.SNMPV2) {
			// if
			// (StringUtil.isNullOrEmpty(config.getSnmpConfig().getCommunityGet()))
			// {
			// throwCustomMessage("Please enter SNMP GET Community");
			// ok = false;
			// }
			// }
			// if (config.getType()==MonitorType.SNMPV3) {
			// HostSnmpConfig snmp = config.getSnmpConfig();
			// if
			// (StringUtil.isNullOrEmpty(config.getSnmpConfig().getUsername()))
			// {
			// throwCustomMessage("Please enter SNMP Username");
			// ok = false;
			// } else if ((snmp.getAuth()!=AuthProtocol.NONE) &&
			// StringUtil.isNullOrEmpty(snmp.getAuthPassword())) {
			// throwCustomMessage("Please enter SNMP Authentication Password");
			// ok = false;
			// } else if ((snmp.getAuth()==AuthProtocol.NONE) &&
			// !StringUtil.isNullOrEmpty(snmp.getAuthPassword())) {
			// throwCustomMessage("Please choose SNMP Authentication Protocol");
			// ok = false;
			// } else if (!StringUtil.isLengthOk(snmp.getAuthPassword(),
			// AppConfig.getSnmpPasswordMinLength())) {
			// throwCustomMessage("Password should contain at least 8 digits");
			// ok = false;
			// } else if (snmp.getAuth()!=AuthProtocol.NONE &&
			// !StringUtil.isNullOrEmpty(snmp.getAuthPassword())) {
			// if ((snmp.getPriv()!=null) &&
			// StringUtil.isNullOrEmpty(snmp.getPrivPassword())) {
			// throwCustomMessage("Please enter SNMP Privacy Password");
			// ok = false;
			// } else if ((snmp.getPriv()==null) &&
			// !StringUtil.isNullOrEmpty(snmp.getPrivPassword())) {
			// throwCustomMessage("Please choose SNMP Privacy Protocol");
			// ok = false;
			// } else if (snmp.getPriv()!=PrivProtocol.NONE &&
			// !StringUtil.isLengthOk(snmp.getPrivPassword(),
			// AppConfig.getSnmpPasswordMinLength())) {
			// throwCustomMessage("Password should contain at least 8 digits");
			// ok = false;
			// }
			// }
			// }
			return ok;
		}
	}

	private void resetHost() {
		host = new HostConfig();
		// host.setSnmpConfig(new HostSnmpConfig());
		// host.getSnmpConfig().setHost(host);
		host.generateDefaultSetting();
	}

	public HostConfig getHost() {
		return host;
	}

	public void setHost(HostConfig host) {
		this.host = host;
	}

	public void editMonitorThresholdConfig(MonitoredHost selectedHost,
			MonitorThresholdConfig dummy, MonitorThresholdConfig original) {
		boolean ok = true;
		HostConfig backupHost = selectedHost.getBackupHostConfig();
		Set<MonitorThresholdConfig> list = dummy.getHostMonitorConfig()
				.getThresholdConfig();
		for (MonitorThresholdConfig threshold : list) {
			if (!threshold.isConfigValid(dummy)) {
				ok = false;
				break;
			}
		}

		if (ok) {
			original.updateConfig(dummy);
			saveAndPushConfig(backupHost, selectedHost);
		} else {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.THRESHOLD_CONFLICT);
		}
	}

	public void addMonitorThresholdConfig(MonitoredHost selectedHost,
			HostMonitorConfig hostMonitorConfig,
			MonitorThresholdConfig monitorThresholdConfig) {
		boolean ok = true;
		HostConfig backupHost = selectedHost.getBackupHostConfig();
		for (MonitorThresholdConfig threshold : hostMonitorConfig
				.getThresholdConfig()) {
			if (threshold.getTarget()
					.equals(monitorThresholdConfig.getTarget())) {
				ok = false;
			}
		}

		if (ok) {
			if (hostMonitorConfig.getName() == HostMonitorName.DISK_MONITOR) {
				hostMonitorConfig
						.generateDefaultDiskMonitor(monitorThresholdConfig
								.getTarget());
			} else if (hostMonitorConfig.getName() == HostMonitorName.INET_MONITOR) {
				hostMonitorConfig
						.generateDefaultInetMonitor(monitorThresholdConfig
								.getTarget());
			}
			saveAndPushConfig(backupHost, selectedHost);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-host", backupHost.getLogDetail());
		} else {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.THRESHOLD_CONFLICT);
		}
	}

	public void editHostMonitorConfig(MonitoredHost selectedHost,
			HostMonitorConfig hostMonitorConfig, HostMonitorConfig dummy) {
		HostConfig backupHost = selectedHost.getBackupHostConfig();
		hostMonitorConfig.updateConfig(dummy);
		if (logger.isDebugEnabled())
			logger.debug("updated config: " + hostMonitorConfig);
		if (hostMonitorConfig.getName().equals(HostMonitorName.INET_MONITOR)
				&& hostMonitorConfig.isEnabled()) {
			if (selectedHost.getLatestNetworkStatus() != null) {
				for (NetworkInterface inet : selectedHost
						.getLatestNetworkStatus().getInterfaces()) {
					boolean found = false;
					for (MonitorThresholdConfig threshold : hostMonitorConfig
							.getThresholdConfigAsList()) {
						if (threshold.getTarget().equals(inet.getName())) {
							found = true;
							break;
						}
					}
					if (!found) {
						MonitorThresholdConfig newThreshold = new MonitorThresholdConfig();
						newThreshold.setHostMonitorConfig(hostMonitorConfig);
						hostMonitorConfig.getThresholdConfig()
								.add(newThreshold);
						newThreshold.setTarget(inet.getName());
						newThreshold.setLevel(WarningLevel.WARNING.name());
						newThreshold.setWatermark(1f);
					}
				}
			}
		}
		saveAndPushConfig(backupHost, selectedHost);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
				"edit-host", backupHost.getLogDetail());
	}

	public void deleteMonitorThresholdConfig(MonitoredHost selectedHost,
			MonitorThresholdConfig threshold) {
		HostConfig backupHost = selectedHost.getBackupHostConfig();
		threshold.getHostMonitorConfig().getThresholdConfig().remove(threshold);
		saveAndPushConfig(backupHost, selectedHost);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
				"edit-host", backupHost.getLogDetail());
	}

	public void deleteHost(MonitoredHost selectedHost) {
		logger.info("Attempting delete host:" + selectedHost);
		try {
			SmpClient.instance.deleteHost(selectedHost);
		} catch (Exception e) {
			logger.error("failed in deleting host {}", selectedHost.getName(),
					e);
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, e.getMessage());
		}
	}

	public void deleteSnmp(HostSnmpConfig snmp, MonitoredHost monitoredHost) {
		logger.debug("delete snmp called:" + snmp);
		HostConfig host = monitoredHost.getConfig();
		if (monitoredHost.getConfig().getSnmpConfigs().contains(snmp)) {
			logger.debug("snmp config found in host");
		}
		RrdService.getInstance().deleteSnmp(snmp);
		DataAccess.snmpTargetDao.deleteSnmpTargetByHostSnmpConfig(snmp);
		monitoredHost.getConfig().getSnmpConfigs().remove(snmp);
		logger.debug("snmp removed from host");
		saveAndPushConfig(host, monitoredHost);
		AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
				"delete-SNMP config", monitoredHost.getConfig().getLogDetail());
	}

	public void editSnmp(HostSnmpConfig snmp, HostSnmpConfig dummy,
			MonitoredHost monitoredHost) {
		HostConfig host = monitoredHost.getConfig();
		boolean ok = true;
		
		//NOT allow to update target type
		dummy.setTargetType(snmp.getTargetType());
		
//		if (snmp.getTargetType().equals(TargetType.APP)
//				&& dummy.getTargetType().equals(TargetType.HOST)
//				&& host.getSnmpConfig() != null) {
//			ok = false;
//			WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
//					"SNMP Config Error",
//					"There are duplicated configs for Target Type HOST");
//		}

		if (ok && isSnmpConfigValid(dummy)) {
			snmp.update(dummy);
			saveAndPushConfig(host, monitoredHost);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-SNMP config", host.getLogDetail());
		}
	}

	public void addSnmp(HostSnmpConfig newSnmp, MonitoredHost monitoredHost) {
		HostSnmpConfig snmp = newSnmp.getBackup();
		HostConfig host = monitoredHost.getConfig();
		logger.debug("adding snmp for " + host.getName());
		boolean ok = true;
		if (host.getSnmpConfig() != null
				&& snmp.getTargetType().equals(TargetType.HOST)) {
			ok = false;
			WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
					"SNMP Config Error",
					"There are duplicated configs for Target Type HOST");
		}

		if (ok && isSnmpConfigValid(snmp)) {
			snmp.setHost(host);
			host.getSnmpConfigs().add(snmp);
			saveAndPushConfig(host, monitoredHost);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-SNMP config", host.getLogDetail());
		}
	}

	private boolean isSnmpConfigValid(HostSnmpConfig config) {
		boolean ok = true;
		// TODO
		// below is commented for testing
		// if (config.getVersion().equals(SnmpVersion.SNMPV2) ||
		// config.getVersion().equals(SnmpVersion.SNMPV1)) {
		// if (StringUtil.isNullOrEmpty(config.getCommunityGet())) {
		// throwCustomMessage("Please enter SNMP GET Community");
		// ok = false;
		// }
		// }
		if (config.getVersion().equals(SnmpVersion.SNMPV3)) {
			if (StringUtil.isNullOrEmpty(config.getUsername())) {
				WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
						"SNMP Config Error", "Please enter SNMP Username");
				ok = false;
			} else if ((config.getAuth() != AuthProtocol.NONE)
					&& StringUtil.isNullOrEmpty(config.getAuthPassword())) {
				WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
						"SNMP Config Error",
						"Please enter SNMP Authentication Password");
				ok = false;
			} else if ((config.getAuth() == AuthProtocol.NONE)
					&& !StringUtil.isNullOrEmpty(config.getAuthPassword())) {
				WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
						"SNMP Config Error",
						"Please choose SNMP Authentication Protocol");
				ok = false;
			} else if (!StringUtil.isLengthOk(config.getAuthPassword(),
					AppConfig.getSnmpPasswordMinLength())) {
				WebUtil.addGlobalMsg(
						FacesMessage.SEVERITY_ERROR,
						"SNMP Config Error",
						"Password should contain at least "
								+ AppConfig.getSnmpPasswordMinLength()
								+ " digits");
				ok = false;
			} else if (config.getAuth() != AuthProtocol.NONE
					&& !StringUtil.isNullOrEmpty(config.getAuthPassword())) {
				if ((config.getPriv() != null)
						&& StringUtil.isNullOrEmpty(config.getPrivPassword())) {
					WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
							"SNMP Config Error",
							"Please enter SNMP Privacy Password");
					ok = false;
				} else if ((config.getPriv() == null)
						&& !StringUtil.isNullOrEmpty(config.getPrivPassword())) {
					WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
							"SNMP Config Error",
							"Please choose SNMP Privacy Protocol");
					ok = false;
				} else if (config.getPriv() != PrivProtocol.NONE
						&& !StringUtil.isLengthOk(config.getPrivPassword(),
								AppConfig.getSnmpPasswordMinLength())) {
					WebUtil.addGlobalMsg(
							FacesMessage.SEVERITY_ERROR,
							"SNMP Config Error",
							"Password should contain at least "
									+ AppConfig.getSnmpPasswordMinLength()
									+ " digits");
					ok = false;
				}
			}
		}
		return ok;
	}

	private void saveAndPushConfig(HostConfig backupHost,
			MonitoredHost selectedHost) {
		try {
			SmpClient.instance.reloadHost(backupHost, selectedHost);
			if (selectedHost.getHostMgr().isAgentConnected())
				selectedHost.getHostMgr().pushHostConfig();
		} catch (DataAccessException e) {
			throwDBExceptionMessage();
			logger.warn("failed in accessing DB");
		} catch (AgentRuntimeException e) {
			throwAgentExceptionMessage(selectedHost.getName());
		} catch (Exception e) {
			logger.error("failed in save & push config to host {}",
					selectedHost.getName(), e);
		}
	}

	private boolean isIpOk(HostConfig dummy) {
		MonitoredHost temp = SmpClient.instance.getIpHostMap().get(
				dummy.getIpAddr());
		if (temp != null) {
			if (temp.getConfig().getId() != dummy.getId()) {
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR, "Duplicated " + type.name()
								+ " IP!");
				return false;
			}
		}
		return true;
	}

	public Site getSelectedSite() {
		return selectedSite;
	}

	public void setSelectedSite(Site selectedSite) {
		this.selectedSite = selectedSite;
	}
}
