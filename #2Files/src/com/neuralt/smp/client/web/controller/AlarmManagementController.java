package com.neuralt.smp.client.web.controller;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.primefaces.model.StreamedContent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.config.AlarmStatus;
import com.neuralt.smp.client.export.DataExporter;
import com.neuralt.smp.client.service.AlarmService;
import com.neuralt.smp.client.task.FileDeleteTask;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.web.security.UserBean;
import com.neuralt.smp.notification.WarningLevel;
import com.neuralt.smp.util.PropertiesUtil;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class AlarmManagementController implements Serializable {
	private static final long serialVersionUID = -5510274848181827268L;

	private static final Logger logger = LoggerFactory
			.getLogger(AlarmManagementController.class);

	@ManagedProperty(value = "#{entitySelectorController}")
	private EntitySelectorController entitySelector;
	@ManagedProperty(value = "#{UserBean}")
	private UserBean userBean;

	private int rowNumber = 20;
	private static final String BACKGROUND_RED = "alarm-red";
	private static final String BACKGROUND_WHITE = "alarm-white";
	private static final String BACKGROUND_ORANGE = "alarm-orange";

	// private Date timeFrom;
	// private Date timeTo;
	private AlarmMgmtDataModel lazyDataModel;
	private Map<String, String> cssStyleMap;
	private Alarm selectedAlarm;
	private Alarm[] selectedAlarms;
	private String status;
	private DetachedCriteria crit;

	private boolean stopPoll;
	private int pollInterval;

	public AlarmManagementController() {
		lazyDataModel = new AlarmMgmtDataModel(this);
		cssStyleMap = createStatusCssMap();
		pollInterval = AppConfig.getWebMonitorPollInterval();
	}

	// testing purpose
	private void manualInit() {
		/*
		 * Alarm a1 = new Alarm(); a1.setSource("self");
		 * a1.setSourceIP("127.0.0.1"); a1.setTrapAlarmId("321");
		 * a1.setTrapAlarmLevel("NORMAL"); a1.setTime(new Date());
		 * DataAccess.alarmDao.save(a1); Alarm a2 = new Alarm();
		 * a2.setSource("self"); a2.setSourceIP("127.0.0.1");
		 * a2.setTrapAlarmId("123"); a2.setTrapAlarmLevel("SEVERE");
		 * a2.setTime(new Date()); DataAccess.alarmDao.save(a2); Alarm a3 = new
		 * Alarm(); a3.setSourceIP("192.168.0.1"); a3.setTrapAlarmId("3");
		 * a3.setSource("dummy 3"); a3.setTrapAlarmLevel("WARNING");
		 * a3.setTime(new Date()); DataAccess.alarmDao.save(a3);
		 */
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	public AlarmMgmtDataModel getLazyDataModel() {
		return lazyDataModel;
	}

	public Alarm getSelectedAlarm() {
		return selectedAlarm;
	}

	public void setSelectedAlarm(Alarm selectedAlarm) {
		this.selectedAlarm = selectedAlarm;
	}

	public void updateColumns() {
		// reset table state
		UIComponent table = FacesContext.getCurrentInstance().getViewRoot()
				.findComponent(":alarmForm:tblAlarm");
		table.setValueExpression("sortBy", null);

	}

	public String getAlarmColor(Alarm alarm) {
		if (alarm.getLevel() != null) {
			return cssStyleMap.get(alarm.getLevel().name());
		}
		return BACKGROUND_WHITE;
	}

	public String getStatusColor(String status) {
		AlarmStatus st = AlarmService.instance.getAlarmStatus(status);
		return st == null ? null : st.getTextColor().styleClass();
	}

	public EntitySelectorController getEntitySelector() {
		return entitySelector;
	}

	public void setEntitySelector(EntitySelectorController entitySelector) {
		this.entitySelector = entitySelector;
		this.entitySelector
				.setEntitySelectionListener(new AlarmEntitySelectionListener());
	}

	public void onRowSelect(SelectEvent event) {

	}

	public void onEdit(RowEditEvent event) {
		Alarm alarm = (Alarm) event.getObject();
		try {
			DataAccess.alarmDao.save(alarm);
			FacesMessage msg = new FacesMessage("Alarm Edited");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (DataAccessException e) {
			FacesMessage msg = new FacesMessage("DB Access Exception",
					"Please try again later.");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	public void editStatus() {
		for (Alarm alarm : selectedAlarms) {
			AlarmStatus curStatus = AlarmService.instance.getAlarmStatus(alarm
					.getStatus());
			if (curStatus != null
					&& curStatus.getCategory() == AlarmStatus.StatusCategory.INITIAL) {
				logger.debug("alarm status owner:"
						+ userBean.getCurrentUserId());
				alarm.setOwnerId(userBean.getCurrentUserId());
			}
			AlarmStatus selectedStatus = AlarmService.instance
					.getAlarmStatus(status);
			if (status != null
					&& selectedStatus.getCategory() == AlarmStatus.StatusCategory.FINAL) {
				alarm.setResolverId(userBean.getCurrentUserId());
				alarm.setResolved(new Date());
			}

			alarm.setStatus(status);
		}
		List<Alarm> list = Arrays.asList(selectedAlarms);
		try {
			DataAccess.alarmDao.saveAlarmStatus(list);
			for (Alarm alarm : list) {
				AuditLogger.logAuditTrail(ScreenType.MONITOR,
						WebUtil.getRequestPath(), ActionType.UPDATE,
						"edit-alarm status", alarm.toString());
			}
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS,
					MessageKey.ALARM_RECORD_MODIFIED);
			selectedAlarms = null; // clear selected
		} catch (DataAccessException e) {
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.DB_ACCESS_ERROR, null);
			logger.warn("failed in accessing DB");
		}
	}

	public Alarm[] getSelectedAlarms() {
		return selectedAlarms;
	}

	public void setSelectedAlarms(Alarm[] selectedAlarms) {
		this.selectedAlarms = selectedAlarms;
	}

	public void search() {
		lazyDataModel.refreshRowCount();
	}

	public List<AlarmStatus> getStatusList() {
		return AlarmService.instance.listAlarmStatus();
	}

	// for primefaces column filter
	public List<SelectItem> getStatusSelectItems() {
		List<SelectItem> items = new ArrayList<SelectItem>();
		List<AlarmStatus> statusList = getStatusList();
		items.add(new SelectItem("", "-Any-"));
		if (statusList != null && !statusList.isEmpty()) {
			for (AlarmStatus status : statusList) {
				items.add(new SelectItem(status.getName(), status.getName()));
			}
		}
		return items;
	}

	public List<String> getWarningLevelList() {
		List<String> list = new ArrayList<String>();
		for (WarningLevel level : WarningLevel.values()) {
			list.add(level.name());
		}
		return list;
	}

	private Map<String, String> createStatusCssMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put(WarningLevel.INFO.name(), BACKGROUND_WHITE);
		map.put(WarningLevel.NORMAL.name(), BACKGROUND_WHITE);
		map.put(WarningLevel.WARNING.name(), BACKGROUND_ORANGE);
		map.put(WarningLevel.MAJOR.name(), BACKGROUND_RED);
		map.put(WarningLevel.MINOR.name(), BACKGROUND_RED);
		map.put(WarningLevel.ERROR.name(), BACKGROUND_RED);
		map.put(WarningLevel.FATAL.name(), BACKGROUND_RED);
		return map;
	}

	private class AlarmEntitySelectionListener implements
			EntitySelectorController.EntitySelectionListener {

		@Override
		public void onEntitySelect(MonitoredEntity selectedEntity) {
			try {
				if (entitySelector.getSelectedProcess() != null) {
					lazyDataModel.process = entitySelector.getSelectedProcess();
					lazyDataModel.host = null;
					lazyDataModel.system = null;
					lazyDataModel.refreshRowCount();
				} else if (entitySelector.getSelectedHost() != null) {
					lazyDataModel.process = null;
					lazyDataModel.host = entitySelector.getSelectedHost();
					lazyDataModel.system = null;
					lazyDataModel.refreshRowCount();
				} else if (entitySelector.getSelectedSystem() != null) {
					lazyDataModel.process = null;
					lazyDataModel.host = null;
					lazyDataModel.system = entitySelector.getSelectedSystem();
					lazyDataModel.refreshRowCount();
				} else {
					lazyDataModel.process = null;
					lazyDataModel.host = null;
					lazyDataModel.system = null;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.warn("Failed to query db:" + e);
			}
		}
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getFirstOccurredFrom() {
		return lazyDataModel.foFrom;
	}

	public void setFirstOccurredFrom(Date date) {
		lazyDataModel.foFrom = date;
	}

	public Date getFirstOccurredTo() {
		return lazyDataModel.foTo;
	}

	public void setFirstOccurredTo(Date date) {
		lazyDataModel.foTo = date;
	}

	public Date getLastOccurredFrom() {
		return lazyDataModel.loFrom;
	}

	public void setLastOccurredFrom(Date lastOccurredFrom) {
		lazyDataModel.loFrom = lastOccurredFrom;
	}

	public Date getLastOccurredTo() {
		return lazyDataModel.loTo;
	}

	public void setLastOccurredTo(Date lastOccurredTo) {
		lazyDataModel.loTo = lastOccurredTo;
	}

	public Date getResolvedFrom() {
		return lazyDataModel.resFrom;
	}

	public void setResolvedFrom(Date resolvedFrom) {
		lazyDataModel.resFrom = resolvedFrom;
	}

	public Date getResolvedTo() {
		return lazyDataModel.resTo;
	}

	public void setResolvedTo(Date resolvedTo) {
		lazyDataModel.resTo = resolvedTo;
	}

	public boolean isStopPoll() {
		return stopPoll;
	}

	public void setStopPoll(boolean stopPoll) {
		this.stopPoll = stopPoll;
	}

	public int getPollInterval() {
		return pollInterval;
	}

	public void setPollInterval(int pollInterval) {
		this.pollInterval = pollInterval;
	}

	public List<String> getStatusFilters() {
		return lazyDataModel.statusFilters;
	}

	public void setStatusFilters(List<String> statusFilters) {
		lazyDataModel.statusFilters = statusFilters;
	}

	public List<String> getLevelFilters() {
		return lazyDataModel.levelFilters;
	}

	public void setLevelFilters(List<String> levelFilters) {
		lazyDataModel.levelFilters = levelFilters;
	}

	public DetachedCriteria getCrit() {
		return crit;
	}

	public void setCrit(DetachedCriteria crit) {
		this.crit = crit;
	}

	public String getFilename() {
		StringBuilder sb = new StringBuilder();
		sb.append("SMP_alarm ").append(
				StringUtil.convertTimeFormatForExport(new Date()));
		return sb.toString();
	}

	public StreamedContent getFile() {
		List<Alarm> list = DataAccess.alarmDao
				.findByCriteriaWithNoPagination(crit);
		logger.debug("get file called: list size=" + list.size());
		String downloadFilename = generateDownloadFilename();
		String filename = PropertiesUtil
				.parsePropertiesValue(generateFullFilename(downloadFilename));

		logger.debug("filename:" + filename);

		logger.debug("ready to export");
		DataExporter.getInstance().exportAlarms(filename, list);
		logger.debug("ready to open input stream");
		InputStream stream = null;
		try {
			stream = new FileInputStream(filename);
		} catch (FileNotFoundException e) {
			logger.warn("file not found:" + e.getMessage());
		}
		StreamedContent file = new DefaultStreamedContent(stream,
				"application/vnd.ms-excel	", downloadFilename);

		Thread deleteTask = new FileDeleteTask(filename);
		deleteTask.start();

		return file;
	}

	private String generateDownloadFilename() {
		StringBuilder sb = new StringBuilder();
		String nowString = StringUtil.convertTimeFormatForExport(new Date());
		sb.append(AppConfig.getAlarmReportPrefix()).append(nowString)
				.append(".xls");
		return sb.toString();
	}

	private String generateFullFilename(String downloadFilename) {
		StringBuilder sb = new StringBuilder();
		sb.append(AppConfig.getHomeFilepath()).append(downloadFilename);
		return sb.toString();
	}

	private class AlarmMgmtDataModel extends LazyDataModel<Alarm> {

		private static final long serialVersionUID = 6670542740052158977L;

		private List<Alarm> pageData;

		Date foFrom; // first occurred from
		Date foTo; // first occurred to
		Date loFrom; // last occurred from
		Date loTo;
		Date resFrom; // resolved from
		Date resTo; // resolved to
		MonitoredSystem system;
		MonitoredHost host;
		MonitoredProcess process;
		List<String> statusFilters;
		List<String> levelFilters;

		AlarmManagementController parent;

		public AlarmMgmtDataModel(AlarmManagementController parent) {
			this.parent = parent;
		}

		@Override
		public Alarm getRowData(String rowKey) {
			for (Alarm alarm : pageData) {
				if (alarm.getId().equals(Long.valueOf(rowKey))) {
					return alarm;
				}
			}
			return null;
		}

		@Override
		public Object getRowKey(Alarm alarm) {
			return alarm.getId();
		}

		@Override
		public List<Alarm> load(int first, int pageSize, String sortField,
				SortOrder sortOrder, Map<String, String> filters) {
			if (sortField == null)
				sortField = "lastOccurred";
			DetachedCriteria criteria = buildCriteria();
			logger.debug("Alarm lazy load called");
			for (String key : filters.keySet()) {
				logger.debug("key=[" + key + "] value=[" + filters.get(key)
						+ "]");
			}
			for (Entry<String, String> filter : filters.entrySet()) {
				if (StringUtil.isNullOrEmpty(filter.getValue())) {
					continue;
				}
				if ("source".equals(filter.getKey())) {
					criteria.add(Restrictions
							.disjunction()
							.add(Restrictions.like("source", filter.getValue(),
									MatchMode.ANYWHERE).ignoreCase())
							.add(Restrictions.like("sourceIp",
									filter.getValue(), MatchMode.ANYWHERE)));
				} else {
					criteria.add(Restrictions.like(filter.getKey(),
							filter.getValue(), MatchMode.ANYWHERE).ignoreCase());
				}
			}

			switch (sortOrder) {
			case ASCENDING:
				criteria.addOrder(Order.asc(sortField));
				break;
			case DESCENDING:
				criteria.addOrder(Order.desc(sortField));
				break;
			default:
				break;
			}
			parent.setCrit(criteria);
			pageData = DataAccess.alarmDao.findByCriteria(criteria, first,
					pageSize);
			if (pageData != null && pageData.size() > this.getRowCount()) {
				refreshRowCount();
			}
			return pageData;
		}

		private DetachedCriteria buildCriteria() {
			DetachedCriteria criteria = DetachedCriteria.forClass(Alarm.class);
			if (foFrom != null)
				criteria.add(Property.forName("firstOccurred").ge(foFrom));
			if (foTo != null)
				criteria.add(Property.forName("firstOccurred").le(foTo));
			if (loFrom != null)
				criteria.add(Property.forName("lastOccurred").ge(loFrom));
			if (loTo != null)
				criteria.add(Property.forName("lastOccurred").le(loTo));
			if (resFrom != null)
				criteria.add(Property.forName("resolved").ge(resFrom));
			if (resTo != null)
				criteria.add(Property.forName("resolved").le(resTo));
			if (process != null) {
				final String source = process.getHost().getConfig().getName() + " - "
						+ process.getName();
				criteria.add(Property.forName("source").like(
						source));
			} else if (host != null)
				criteria.add(Property.forName("source").like(
						host.getConfig().getName()));
			else if (system != null) {
				Disjunction dis = Restrictions.disjunction();
				MatchMode matchMode = MatchMode.ANYWHERE;
				dis.add(Restrictions.like("source", system.getName(), matchMode));
				for (MonitoredHost host : system.getHosts()) {
					dis.add(Restrictions.like("source", host.getConfig()
							.getName(), matchMode));
				}
				criteria.add(dis);
			}
			if (statusFilters != null && !statusFilters.isEmpty()) {
				criteria.add(Restrictions.in("status", statusFilters));
			}
			if (levelFilters != null && !levelFilters.isEmpty()) {
				Set<WarningLevel> levels = new HashSet<WarningLevel>();
				for (String levelName : levelFilters) {
					levels.add(WarningLevel.valueOf(levelName));
				}
				criteria.add(Restrictions.in("level", levels));
			}

			return criteria;
		}

		public long refreshRowCount() {
			DetachedCriteria criteria = buildCriteria();
			long count = DataAccess.alarmDao.count(criteria);
			this.setRowCount((int) count);
			return count;
		}
	}
}
