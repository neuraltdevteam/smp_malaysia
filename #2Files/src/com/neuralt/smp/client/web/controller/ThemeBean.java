package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@ManagedBean
@SessionScoped
public class ThemeBean implements Serializable {
	private static final long serialVersionUID = -3165697310794317769L;
	private static final Logger logger = LoggerFactory
			.getLogger(ThemeBean.class);
	private String theme;
	private List<String> list;

	public ThemeBean() {
		// theme = "smoothness";
		theme = "redmond";
		// theme = "sam";
		// theme = "pepper-grinder";
		list = new ArrayList<String>();
		list.add("afternoon");
		list.add("glass-x");
		list.add("sam");
		list.add("smoothness");
		list.add("south-street");
		list.add("pepper-grinder");
		list.add("ui-lightness");
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	public List<String> getList() {
		return list;
	}

}
