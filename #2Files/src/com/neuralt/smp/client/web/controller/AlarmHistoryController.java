package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import org.primefaces.event.RowEditEvent;
import org.primefaces.event.SelectEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredEntity;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.LazySnmpTrapDataModel;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.notification.WarningLevel;

@ManagedBean
@ViewScoped
public class AlarmHistoryController implements Serializable {
	private static final long serialVersionUID = -5510274848181827268L;

	private static final Logger logger = LoggerFactory
			.getLogger(AlarmHistoryController.class);

	@ManagedProperty(value = "#{entitySelectorController}")
	private EntitySelectorController entitySelector;

	private int rowNumber = 20;
	private static final String BACKGROUND_RED = "alarm-red";
	private static final String BACKGROUND_WHITE = "alarm-white";
	private static final String BACKGROUND_ORANGE = "alarm-orange";
	private static final String TEXT_RED = "text-red";
	private static final String TEXT_GREEN = "text-green";
	private static final String TEXT_ORANGE = "text-orange";
	private Date timeFrom;
	private Date timeTo;
	private LazySnmpTrapDataModel lazyDataModel;
	private Map<String, String> cssStyleMap;
	private SnmpTrap selectedAlarm;
	private SnmpTrap[] selectedAlarms;
	private boolean alarmSelected;

	public AlarmHistoryController() {
		lazyDataModel = new LazySnmpTrapDataModel();
		cssStyleMap = createStatusCssMap();
	}

	// testing purpose
	private void manualInit() {
		/*
		 * Alarm a1 = new Alarm(); a1.setSource("self");
		 * a1.setSourceIP("127.0.0.1"); a1.setTrapAlarmId("321");
		 * a1.setTrapAlarmLevel("NORMAL"); a1.setTime(new Date());
		 * DataAccess.alarmDao.save(a1); Alarm a2 = new Alarm();
		 * a2.setSource("self"); a2.setSourceIP("127.0.0.1");
		 * a2.setTrapAlarmId("123"); a2.setTrapAlarmLevel("SEVERE");
		 * a2.setTime(new Date()); DataAccess.alarmDao.save(a2); Alarm a3 = new
		 * Alarm(); a3.setSourceIP("192.168.0.1"); a3.setTrapAlarmId("3");
		 * a3.setSource("dummy 3"); a3.setTrapAlarmLevel("WARNING");
		 * a3.setTime(new Date()); DataAccess.alarmDao.save(a3);
		 */
	}

	public int getRowNumber() {
		return rowNumber;
	}

	public void setRowNumber(int rowNumber) {
		this.rowNumber = rowNumber;
	}

	public LazySnmpTrapDataModel getLazyDataModel() {
		return lazyDataModel;
	}

	public void setLazyDataModel(LazySnmpTrapDataModel lazyDataModel) {
		this.lazyDataModel = lazyDataModel;
	}

	public SnmpTrap getSelectedAlarm() {
		return selectedAlarm;
	}

	public void setSelectedAlarm(SnmpTrap selectedAlarm) {
		this.selectedAlarm = selectedAlarm;
	}

	public Date getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(Date timeFrom) {
		this.timeFrom = timeFrom;
	}

	public Date getTimeTo() {
		return timeTo;
	}

	public void setTimeTo(Date timeTo) {
		this.timeTo = timeTo;
	}

	public void updateColumns() {
		// reset table state
		UIComponent table = FacesContext.getCurrentInstance().getViewRoot()
				.findComponent(":alarmForm:tblAlarm");
		table.setValueExpression("sortBy", null);

	}

	public String getAlarmColor(SnmpTrap alarm) {
		if (alarm.getTrapAlarmLevel() != null) {
			return cssStyleMap.get(alarm.getTrapAlarmLevel());
		}
		return BACKGROUND_WHITE;
	}

	public EntitySelectorController getEntitySelector() {
		return entitySelector;
	}

	public void setEntitySelector(EntitySelectorController entitySelector) {
		this.entitySelector = entitySelector;
		this.entitySelector
				.setEntitySelectionListener(new AlarmEntitySelectionListener());
	}

	public void onRowSelect(SelectEvent event) {

	}

	public void onEdit(RowEditEvent event) {
		SnmpTrap alarm = (SnmpTrap) event.getObject();
		try {
			DataAccess.snmpTrapDao.save(alarm);
			FacesMessage msg = new FacesMessage("Alarm Edited");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		} catch (DataAccessException e) {
			FacesMessage msg = new FacesMessage("DB Access Exception",
					"Please try again later.");
			FacesContext.getCurrentInstance().addMessage(null, msg);
		}
	}

	public void onDateSelect(SelectEvent event) {
		if (timeFrom != null)
			lazyDataModel.setTimeFrom(timeFrom);
		if (timeTo != null)
			lazyDataModel.setTimeTo(timeTo);
	}

	public SnmpTrap[] getSelectedAlarms() {
		return selectedAlarms;
	}

	public void setSelectedAlarms(SnmpTrap[] selectedAlarms) {
		this.selectedAlarms = selectedAlarms;
	}

	public void search() {
		/*
		 * if ((timeFrom==null) && (timeTo==null)) {
		 * WebUtil.addGlobalMsg(FacesMessage.SEVERITY_WARN,"Empty Date",
		 * "Please enter valid dates"); } else { if ((timeFrom==null) ||
		 * (timeTo==null) || (timeFrom.compareTo(timeTo)<=0)) { try { if
		 * (entitySelector.getSelectedHost()!=null) { allAlarms =
		 * DataAccess.alarmDao.getAlarmsByTime(timeFrom, timeTo,
		 * entitySelector.getSelectedHost().getName()); } else if
		 * (entitySelector.getSelectedSystem()!=null) { allAlarms =
		 * DataAccess.alarmDao.getAlarmsByTimeAndSystem(timeFrom, timeTo,
		 * entitySelector.getSelectedSystem()); } else { allAlarms =
		 * DataAccess.alarmDao.getAlarmsByTime(timeFrom, timeTo, null); }
		 * lazyDataModel.setDataSource(allAlarms); } catch (Exception e) {
		 * logger.warn("Getting alarm from DB failed:"+e); } } else {
		 * WebUtil.addGlobalMsg(FacesMessage.SEVERITY_WARN,"Date range error",
		 * "Please enter valid dates");
		 * 
		 * } }
		 */
	}

	private Map<String, String> createStatusCssMap() {
		Map<String, String> map = new HashMap<String, String>();
		map.put(WarningLevel.INFO.name(), BACKGROUND_WHITE);
		map.put(WarningLevel.NORMAL.name(), BACKGROUND_WHITE);
		map.put(WarningLevel.WARNING.name(), BACKGROUND_ORANGE);
		map.put(WarningLevel.MAJOR.name(), BACKGROUND_RED);
		map.put(WarningLevel.MINOR.name(), BACKGROUND_RED);
		map.put(WarningLevel.ERROR.name(), BACKGROUND_RED);
		map.put(WarningLevel.FATAL.name(), BACKGROUND_RED);
		return map;
	}

	private class AlarmEntitySelectionListener implements
			EntitySelectorController.EntitySelectionListener {

		@Override
		public void onEntitySelect(MonitoredEntity selectedEntity) {
			/*
			 * if (entitySelector.getSelectedHost()!=null) {
			 * lazyDataModel.setHost(entitySelector.getSelectedHost()); } else
			 * if (entitySelector.getSelectedSystem()!=null) {
			 * lazyDataModel.setSystem(entitySelector.getSelectedSystem()); }
			 */
			try {
				if (entitySelector.getSelectedHost() != null) {
					lazyDataModel.setHost(entitySelector.getSelectedHost());
					// allAlarms =
					// DataAccess.alarmDao.getAlarmsByHost(entitySelector.getSelectedHost().getName());
				} else if (entitySelector.getSelectedSystem() != null) {
					lazyDataModel.setSystem(entitySelector.getSelectedSystem());
					// allAlarms =
					// DataAccess.alarmDao.getAlarmBySystem(entitySelector.getSelectedSystem());
				} else {
					// allAlarms = DataAccess.alarmDao.getAllAlarms();
					lazyDataModel.setHost(null);
					lazyDataModel.setSystem(null);
				}
				// lazyDataModel.setDataSource(allAlarms);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				logger.warn("Failed to query db:" + e);
			}
		}
	}

	/*
	 * public String getMappedOid(String oid) {
	 * System.out.println("get mapped oid called"); String mappedOid = new
	 * String(oid); while
	 * (OidRuleUtil.getInstance().getOidRuleMap().get(mappedOid)!=null) {
	 * mappedOid =
	 * OidRuleUtil.getInstance().getOidRuleMap().get(mappedOid).toString(); }
	 * String trapName = MibFileLoader.trapNameMap.get(new OID(mappedOid)); if
	 * (trapName!=null) { mappedOid = trapName; } return mappedOid; }
	 */
}
