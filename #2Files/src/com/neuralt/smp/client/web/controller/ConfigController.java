package com.neuralt.smp.client.web.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.TabChangeEvent;

import com.neuralt.smp.client.util.FacesUtil;

@ManagedBean
@ViewScoped
public class ConfigController {
	
	protected String selectedTab;

	public String getSelectedTab() {
		return selectedTab;
	}
	public void setSelectedTab(String selectedTab) {
		this.selectedTab = selectedTab;
	}

	public void onTabChange(TabChangeEvent event) {  
		selectedTab = event.getTab().getId();
	    //reload corresponding controller
		ReloadableController reloadable = FacesUtil.getBean(
				selectedTab+"Controller", ReloadableController.class);
		if(reloadable != null){
			reloadable.reload();
		}
	} 
}
