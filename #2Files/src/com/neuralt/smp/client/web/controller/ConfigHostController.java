package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.util.ModelDataTable;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostConfig.HostMonitorConfig;
import com.neuralt.smp.config.SystemConfig;

@ManagedBean
@ViewScoped
public class ConfigHostController implements ReloadableController, Serializable {

	// private static final Logger log =
	// LoggerFactory.getLogger(ConfigHostController.class);

	private static final long serialVersionUID = 1L;

	private final ModelDataTable<HostConfig> model = new ModelDataTable<HostConfig>(
			HostConfig.class,
			"name like :"
					+ ModelDataTable.NAME
					+ " and site.siteName like :site and systemConfig.name like :system",
			ModelDataTable.NAME, "%", "site", "%", "system", "%");

	@ManagedProperty(value = "#{configSiteController}")
	private ConfigSiteController siteController;

	@ManagedProperty(value = "#{configSystemController}")
	private ConfigSystemController systemController;

	@ManagedProperty(value = "#{hostManageController}")
	private HostManageController hostManager;

	private List<HostConfig> hosts = new ArrayList<HostConfig>();
	private HostConfig host = new HostConfig();

	// remove local variable, 20/10/2015 by jimmy.so
	// private List<SelectItem> selectItems = new ArrayList<SelectItem>();

	@PostConstruct
	public void init() {
		reload();
	}

	public ModelDataTable<HostConfig> getModel() {
		return model;
	}

	public ConfigSiteController getSiteController() {
		return siteController;
	}

	public void setSiteController(ConfigSiteController siteController) {
		this.siteController = siteController;
	}

	public ConfigSystemController getSystemController() {
		return systemController;
	}

	public void setSystemController(ConfigSystemController systemController) {
		this.systemController = systemController;
	}

	public HostManageController getHostManager() {
		return hostManager;
	}

	public void setHostManager(HostManageController hostManager) {
		this.hostManager = hostManager;
	}

	public List<HostConfig> getHosts() {
		return hosts;
	}

	public void setHosts(List<HostConfig> hosts) {
		this.hosts = hosts;
	}

	public HostConfig getHost() {
		return host;
	}

	public void setHost(HostConfig host) {
		if (host == null) {
			this.host = new HostConfig();
			this.host.generateDefaultSetting();
		} else {
			// force reselect site and system
			select(host);
			reload();
			this.host = host;
		}
	}

	// remove get & set method , 20/10/2015 by jimmy.so
	/*
	 * public List<SelectItem> getSelectItems() { return selectItems; } public
	 * void setSelectItems(List<SelectItem> selectItems) { this.selectItems =
	 * selectItems; }
	 */

	public boolean isHostsEmpty() {
		return hosts == null || hosts.isEmpty();
	}

	public MonitoredHost monitored(SystemConfig system, HostConfig host) {
		return SmpClient.instance.getHost(system.getName(), host.getName());
	}

	public MonitoredHost monitored(HostConfig host) {
		if (host.getSystemConfig() == null) {
			return null;
		}
		return monitored(host.getSystemConfig(), host);
	}

	public int countTrigger(HostConfig host) {
		int count = 0;
		// PS_MONITOR Removed for UMobile
		// for(HostMonitorConfig monitor : host.getMonitorConfig().values()){
		for (HostMonitorConfig monitor : host.getHostMonitorConfigAsList()) {
			count += monitor.getThresholdConfig().size();
		}
		return count;
	}

	public List<HostConfig> getAllHosts() {
		model.getParam().put("site",
				siteController.getModel().getParam().get(ModelDataTable.NAME));
		model.getParam()
				.put("system",
						systemController.getModel().getParam()
								.get(ModelDataTable.NAME));

		Map<String, Object> param = new HashMap<String, Object>();
		param.put(ModelDataTable.NAME, "%");
		return model.list(param, "order by name asc");
	}

	// change drop down box 'ALL' to 'Please Select, 20/10/2015 by jimmy.so'
	public List<SelectItem> getAllSelectItems(boolean withAll) {
		return getAllSelectItems(withAll, false);
	}

	public List<SelectItem> getAllSelectItems(boolean withAll, boolean withEmpty) {

		List<HostConfig> hosts = getAllHosts();
		List<SelectItem> selectItems = new ArrayList<SelectItem>();
		if (withAll) {
			selectItems.add(new SelectItem("All", "All"));
		}

		else if (withEmpty) {
			selectItems.add(new SelectItem("", "Please Select"));
		}
		for (HostConfig h : hosts) {
			selectItems.add(new SelectItem(h.getName(), h.getName()));
		}
		return selectItems;
	}

	public List<SelectItem> getAllSelectItems() {
		return getAllSelectItems(true, false);
	}

	public List<SelectItem> getEmptySelectItems() {
		return getAllSelectItems(false, true);
	}

	public HostConfig getSelectedHost(boolean reload) {
		if (model.getParamName().equalsIgnoreCase(ModelDataTable.ALL)) {
			return null;
		}
		return model.getSelected(reload);
	}

	public HostConfig getSelectedHost() {
		return getSelectedHost(false);
	}

	@Override
	public void reload() {
		host = new HostConfig();
		List<SelectItem> selectItems = getAllSelectItems(true);

		HostConfig selected = model.getSelected(true);
		if (selected != null) {
			hosts = new ArrayList<HostConfig>();
			hosts.add(selected);
		} else {
			hosts = getAllHosts();
		}
	}

	public void select(HostConfig host) {
		siteController.select(host.getSite());
		systemController.select(host.getSystemConfig());
		model.setParamName(host.getName());
	}

	protected boolean setSite(HostConfig host) {
		host.setSite(siteController.getModel().getSelected(true));
		if (host.getSite() == null) {
			return false;
		}
		return true;
	}

	protected boolean setSystem(HostConfig host) {
		host.setSystemConfig(systemController.getModel().getSelected(true));
		if (host.getSystemConfig() == null) {
			return false;
		}
		return true;
	}

	public void add() {
		if (!setSite(host)) {
			return;
		} else if (!setSystem(host)) {
			return;
		}
		MonitoredSystem system = systemController.monitored(systemController
				.getModel().getSelected());
		hostManager.setSelectedSite(host.getSite());
		hostManager.setHost(host);
		select(host);
		hostManager.addHost(system);
		reload();
	}

	public void delete() {
		delete(host);
	}

	public void delete(HostConfig host) {
		MonitoredSystem system = systemController.monitored(systemController
				.getModel().getSelected());
		MonitoredHost monHost = system.getHostByID(host.getId());
		hostManager.deleteHost(monHost);
		reload();
	}

	public void edit() {
		if (!setSite(host)) {
			return;
		} else if (!setSystem(host)) {
			return;
		}
		MonitoredSystem system = systemController.monitored(systemController
				.getModel().getSelected());
		MonitoredHost monHost = system.getHostByID(host.getId());
		hostManager.setSelectedSite(host.getSite());
		hostManager.editHost(monHost, host);
		reload();
	}

}
