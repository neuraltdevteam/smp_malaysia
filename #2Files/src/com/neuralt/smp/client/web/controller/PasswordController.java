package com.neuralt.smp.client.web.controller;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ConsoleUser;
import com.neuralt.smp.client.web.security.CryptUtil;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.web.security.UserBean;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;
import com.neuralt.web.util.FacesResourceBundleUtil;
import com.neuralt.web.util.jsf.WebUtil;

@ManagedBean
@ViewScoped
public class PasswordController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LoggerFactory
			.getLogger(PasswordController.class);

	@ManagedProperty(value = "#{UserBean}")
	private UserBean userBean;

	private String passwordCurrent;
	private String password;
	private String passwordReenter;

	private void resetPassword() {
		password = null;
		passwordCurrent = null;
		passwordReenter = null;
	}

	public void changePassword() {
		ConsoleUser user = userBean.getUser();
		if (failsValidation(user)) {
			return;
		} else {
			user.setLastUpdatedBy(userBean.getUserId());
			user.setPassword(encrypt(password));
			DataAccess.webDao.saveUser(user, true);
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, MessageKey.PASSWORD_CHANGED);
			AuditLogger.logAuditTrail(ScreenType.FREE, ActionType.UPDATE,
					"change-password", user.getUserId());
			resetPassword();
		}
	}

	public String decrypt(String password) {
		try {
			if (password != null)
				return CryptUtil.decrypt(password);
			else
				return null;
		} catch (Exception e) {
			logger.error("decrypt failed", e);
			return null;
		}
	}

	public String encrypt(String password) {
		try {
			if (!StringUtil.isNullOrEmpty(password))
				return CryptUtil.encrypt(password);
			else
				return null;
		} catch (Exception e) {
			logger.error("encrypt failed", e);
			return null;
		}
	}

	public boolean failsValidation(ConsoleUser user) {
		return (isPasswordInvalid(user));
	}

	private boolean isPasswordInvalid(ConsoleUser user) {
		if (user != null) {

			if (!StringUtil.isNullOrEmpty(passwordCurrent)) {
				if (!user.getPassword().equals(encrypt(passwordCurrent))) {
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.CURRENT_PASSWORD_NOT_MATCH);
					return true;
				}
				if (!password.equals(passwordReenter)) {
					WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
							MessageKey.GENERAL_ERROR,
							MessageKey.NEW_PASSWORDS_NOT_MATCH);
					return true;
				}
				if (DataAccess.webDao.isPasswordUsed(user.getUserId(),
						encrypt(password))) {
					WebUtil.addGlobalMsg(FacesMessage.SEVERITY_ERROR,
							FacesResourceBundleUtil
									.getMessage("user.prompt.user_error"),
							FacesResourceBundleUtil
									.getMessage("user.prompt.password_used"));
					return true;
				}
				return false;
			}
		}
		return true;
	}

	// getters setters //

	public String getPassword() {
		// if (selectedUser!=null && selectedUser.getPassword()!=null) {
		// password = decrypt(selectedUser.getPassword());
		// }
		return password;
	}

	public void setPassword(String password) {
		// if (selectedUser!=null) {
		// password = encrypt(password);
		// }
		this.password = password;
	}

	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	public String getPasswordCurrent() {
		return passwordCurrent;
	}

	public void setPasswordCurrent(String passwordCurrent) {
		this.passwordCurrent = passwordCurrent;
	}

	public String getPasswordReenter() {
		return passwordReenter;
	}

	public void setPasswordReenter(String passwordReenter) {
		this.passwordReenter = passwordReenter;
	}

	public boolean isUserEditable(ConsoleUser user) {
		return !user.getUserId().equals(ConstantUtil.SYS_DEFAULT)
				|| WebUtil.getCurrentUserId().equals(ConstantUtil.SYS_DEFAULT);
	}

	public boolean isUserDeletable(ConsoleUser user) {
		return !user.getUserId().equals(ConstantUtil.SYS_DEFAULT);
	}
}
