package com.neuralt.smp.client.web.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DashboardColumn;
import org.primefaces.model.DashboardModel;
import org.primefaces.model.DefaultDashboardColumn;
import org.primefaces.model.DefaultDashboardModel;
import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.LineChartSeries;

import com.neuralt.smp.agent.mbean.model.Disk;
import com.neuralt.smp.agent.mbean.model.HostDiskUsage;
import com.neuralt.smp.agent.mbean.model.HostInetStatus;
import com.neuralt.smp.agent.mbean.model.NetworkInterface;
import com.neuralt.smp.client.LiveMonitorReporter;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredSystem;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.config.HostConfig;

@ManagedBean
@ViewScoped
public class LiveChartController implements Serializable {
	private static final int PAD_MIN = 5000; // padding for the min on x-axis

	private static final long serialVersionUID = 4211273078834816886L;

	private DashboardModel model;

	private CartesianChartModel cpuLinearModel;
	private CartesianChartModel memoryLinearModel;

	private int pollInterval;

	private String selectedSystem;
	private String selectedHost;

	private String cpuChartName;
	private Long cpuStart = System.currentTimeMillis();

	private List<MyDataType> currentCpuData;
	private List<MyDataType> currentMemData;

	private String memoryChartName;
	private Long memoryStart = System.currentTimeMillis();

	private String currentMemoryTimestamp;
	private Number currentMemoryPct;

	private Map<String, String> statusCssMap = createStatusCssMap();

	// private String cpuProcessChartName;
	// private Long processStart = System.currentTimeMillis();

	public static final class MyDataType {
		private String subject;
		private String timestamp;
		private Number value;

		public String getSubject() {
			return subject;
		}

		public void setSubject(String subject) {
			this.subject = subject;
		}

		public String getTimestamp() {
			return timestamp;
		}

		public void setTimestamp(String timestamp) {
			this.timestamp = timestamp;
		}

		public Number getValue() {
			return value;
		}

		public void setValue(Number value) {
			this.value = value;
		}

	}

	public LiveChartController() {
		model = new DefaultDashboardModel();
		DashboardColumn column1 = new DefaultDashboardColumn();
		DashboardColumn column2 = new DefaultDashboardColumn();

		column1.addWidget("cpu");
		column1.addWidget("inet");
		column2.addWidget("memory");
		column2.addWidget("disk");

		model.addColumn(column1);
		model.addColumn(column2);

		cpuLinearModel = new CartesianChartModel();
		memoryLinearModel = new CartesianChartModel();

		currentCpuData = new ArrayList<MyDataType>();
		currentMemData = new ArrayList<MyDataType>();

		pollInterval = AppConfig.getWebMonitorPollInterval();
	}

	public void onPoll() {
		FacesContext context = FacesContext.getCurrentInstance();

		for (FacesMessage facesMsg : LiveMonitorReporter.getInstance()
				.getGrowlMessages()) {
			context.addMessage("alerts", facesMsg);
		}

		LiveMonitorReporter.getInstance().getGrowlMessages().clear();
	}

	@PostConstruct
	public void createCharts() {
	}

	public CartesianChartModel getCpuLinearModel() {
		createCpuLinearModel();

		return cpuLinearModel;
	}

	public CartesianChartModel getMemoryLinearModel() {
		createMemoryLinearModel();

		return memoryLinearModel;
	}

	private void createCpuLinearModel() {
		cpuLinearModel.clear();
		currentCpuData.clear();

		if (selectedHostOrProcess()) {
			MonitoredHost monitoredHost = SmpClient.instance.getHost(
					selectedSystem, selectedHost);

			Map<Object, Number> hostDataMap = monitoredHost.getCpuHealthMap();
			createHostSeries(cpuLinearModel, monitoredHost, hostDataMap,
					currentCpuData);

			Map<String, Map<Object, Number>> processDataMaps = monitoredHost
					.getProcessCpuHealthMaps(selectedSystem, selectedHost);
			createProcessCpuSeries(cpuLinearModel, processDataMaps,
					currentCpuData);

			if (hostDataMap != null && !hostDataMap.isEmpty())
				cpuStart = (Long) hostDataMap.keySet().toArray()[0] - PAD_MIN;
		} else if (selectedSystemOnly()) {
			MonitoredSystem monitoredSys = SmpClient.instance
					.getSystem(selectedSystem);

			if (monitoredSys != null) {
				for (MonitoredHost monitoredHost : monitoredSys.getHosts()) {
					if (monitoredHost.isEnabled()) {
						Map<Object, Number> hostDataMap = monitoredHost
								.getCpuHealthMap();
						createHostSeries(cpuLinearModel, monitoredHost,
								hostDataMap, currentCpuData);

						if (hostDataMap != null && !hostDataMap.isEmpty())
							cpuStart = (Long) hostDataMap.keySet().toArray()[0]
									- PAD_MIN;
					}
				}
			}
		}
	}

	private boolean selectedSystemOnly() {
		return selectedSystem != null && selectedHost == null;
	}

	private boolean selectedHostOrProcess() {
		return selectedSystem != null && selectedHost != null;
	}

	private void createMemoryLinearModel() {
		memoryLinearModel.clear();
		currentMemData.clear();

		if (selectedHostOrProcess()) {
			MonitoredHost monitoredHost = SmpClient.instance.getHost(
					selectedSystem, selectedHost);

			Map<Object, Number> hostDataMap = monitoredHost.getMemHealthMap();
			createHostSeries(memoryLinearModel, monitoredHost, hostDataMap,
					currentMemData);

			Map<String, Map<Object, Number>> processDataMaps = monitoredHost
					.getProcessMemHealthMaps(selectedSystem, selectedHost);
			createProcessCpuSeries(memoryLinearModel, processDataMaps,
					currentMemData);

			if (hostDataMap != null && !hostDataMap.isEmpty())
				memoryStart = (Long) hostDataMap.keySet().toArray()[0]
						- PAD_MIN;
		} else if (selectedSystemOnly()) {
			MonitoredSystem monitoredSys = SmpClient.instance
					.getSystem(selectedSystem);

			if (monitoredSys != null) {
				for (MonitoredHost monitoredHost : monitoredSys.getHosts()) {
					if (monitoredHost.isEnabled()) {
						Map<Object, Number> hostDataMap = monitoredHost
								.getMemHealthMap();
						createHostSeries(memoryLinearModel, monitoredHost,
								hostDataMap, currentMemData);

						if (hostDataMap != null && !hostDataMap.isEmpty())
							memoryStart = (Long) hostDataMap.keySet().toArray()[0]
									- PAD_MIN;
					}
				}
			}
		}
	}

	private void createHostSeries(CartesianChartModel chartModel,
			MonitoredHost monitoredHost, Map<Object, Number> map,
			List<MyDataType> currentData) {
		LineChartSeries hostSeries = new LineChartSeries();
		hostSeries.setLabel(monitoredHost.getName());
		MyDataType hostData = new MyDataType();
		hostData.setSubject(monitoredHost.getName());

		if (map != null && map.isEmpty()) {
			hostSeries.set(System.currentTimeMillis(), 0.0);
		} else {
			hostSeries.setData(map);

			Long hostLatestTs = (Long) map.keySet().toArray()[map.keySet()
					.size() - 1];
			hostData.setValue(map.get(hostLatestTs));
			hostData.setTimestamp((new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
					.format(new Date(hostLatestTs)));
		}

		chartModel.addSeries(hostSeries);
		currentData.add(hostData);
	}

	private void createProcessCpuSeries(CartesianChartModel cpuLinearModel,
			Map<String, Map<Object, Number>> processMaps,
			List<MyDataType> currentCpuData) {

		if (processMaps != null && processMaps.isEmpty()) {
			// LineChartSeries processSeries = new LineChartSeries();
			// processSeries.set(System.currentTimeMillis(), 0.0);
			// cpuLinearModel.addSeries(processSeries);

		} else {
			for (String processName : processMaps.keySet()) {
				LineChartSeries series = new LineChartSeries();
				series.setLabel(processName);

				MyDataType processData = new MyDataType();
				processData.setSubject(processName);

				Map<Object, Number> processMap = processMaps.get(processName);

				if (processMap == null
						|| (processMap != null && processMap.isEmpty())) {
					series.set(System.currentTimeMillis(), 0.0);
				} else {
					series.setData(processMap);

					Long processLatestTs = (Long) processMap.keySet().toArray()[processMap
							.size() - 1];
					processData.setValue(processMap.get(processLatestTs));
					processData.setTimestamp((new SimpleDateFormat(
							"yyyy-MM-dd HH:mm:ss")).format(new Date(
							processLatestTs)));
				}

				cpuLinearModel.addSeries(series);
				currentCpuData.add(processData);
			}

		}
	}

	public String getSelectedSystem() {
		return selectedSystem;
	}

	public void setSelectedSystem(String selectedSystem) {
		setChartName(selectedSystem, selectedHost);
		this.selectedSystem = selectedSystem;
	}

	public String getSelectedHost() {
		return selectedHost;
	}

	public void setSelectedHost(String selectedHost) {
		setChartName(selectedSystem, selectedHost);
		this.selectedHost = selectedHost;
	}

	public String getCpuChartName() {
		return cpuChartName;
	}

	private void setChartName(String selectedSystem, String selectedHost) {
		if (selectedSystem != null && selectedHost != null) {
			String hostIp = SmpClient.instance.getHost(selectedSystem,
					selectedHost).getIpAddr();
			cpuChartName = String.format("%s - %s (%s)", selectedSystem,
					selectedHost, hostIp);
			memoryChartName = String.format("%s - %s (%s)", selectedSystem,
					selectedHost, hostIp);
		} else if (selectedSystem != null && selectedHost == null) {
			cpuChartName = String.format("%s", selectedSystem);
			memoryChartName = String.format("%s", selectedSystem);
		}
	}

	public String getMemoryChartName() {
		return memoryChartName;
	}

	public DashboardModel getModel() {
		return model;
	}

	public long getMemoryStart() {
		return memoryStart;
	}

	public String getCurrentMemoryTimestamp() {
		return currentMemoryTimestamp;
	}

	public Number getCurrentMemoryPct() {
		return currentMemoryPct;
	}

	public Long getCpuStart() {
		return cpuStart;
	}

	public List<MyDataType> getCurrentCpuData() {
		return currentCpuData;
	}

	public List<MyDataType> getCurrentMemData() {
		return currentMemData;
	}

	public HostConfig getHost() {
		MonitoredHost monitoredHost = SmpClient.instance.getHost(
				selectedSystem, selectedHost);
		if (monitoredHost != null)
			return monitoredHost.getConfig();
		return null;
	}

	public List<NetworkInterface> getCurrentInetData() {
		if (selectedHostOrProcess()) {
			MonitoredHost monitoredHost = SmpClient.instance.getHost(
					selectedSystem, selectedHost);

			if (monitoredHost.getConfig().getInetMonitor().isEnabled()) {
				HostInetStatus hostInetStatus = monitoredHost
						.getLatestNetworkStatus();
				if (hostInetStatus != null) {
					NetworkInterface[] inetData = hostInetStatus
							.getInterfaces();
					if (inetData != null) {
						List<NetworkInterface> latestInetData = Arrays
								.asList(inetData);
						Collections.sort(latestInetData);
						return latestInetData;
					}
				}
			}
		}
		return null;
	}

	public List<Disk> getCurrentDiskData() {
		if (selectedHostOrProcess()) {
			MonitoredHost monitoredHost = SmpClient.instance.getHost(
					selectedSystem, selectedHost);
			if (monitoredHost.getConfig().getDiskMonitor().isEnabled()) {
				HostDiskUsage hostDiskUsage = monitoredHost
						.getLatestDiskUsage();
				if (hostDiskUsage != null) {
					Disk[] disks = hostDiskUsage.getDisks();
					if (disks != null) {
						List<Disk> latestDiskUsage = Arrays.asList(disks);
						Collections.sort(latestDiskUsage);
						return latestDiskUsage;
					}
				}
			}
		}
		return null;
	}

	public boolean displayCpu() {
		if (selectedHostOrProcess()) {
			MonitoredHost monitoredHost = SmpClient.instance.getHost(
					selectedSystem, selectedHost);
			return monitoredHost.getConfig().getCPUMonitor().isEnabled();
		} else {
			return true;
		}
	}

	public boolean displayMem() {
		if (selectedHostOrProcess()) {
			MonitoredHost monitoredHost = SmpClient.instance.getHost(
					selectedSystem, selectedHost);
			return monitoredHost.getConfig().getMemoryMonitor().isEnabled();
		} else {
			return true;
		}
	}

	public boolean displayDisk() {
		if (selectedHostOrProcess()) {
			MonitoredHost monitoredHost = SmpClient.instance.getHost(
					selectedSystem, selectedHost);
			return monitoredHost.getConfig().getDiskMonitor().isEnabled()
					&& monitoredHost.getLatestDiskUsage() != null;
		} else {
			return false;
		}
	}

	public boolean displayInet() {
		if (selectedHostOrProcess()) {
			MonitoredHost monitoredHost = SmpClient.instance.getHost(
					selectedSystem, selectedHost);
			return monitoredHost.getConfig().getInetMonitor().isEnabled()
					&& monitoredHost.getLatestNetworkStatus() != null;
		} else {
			return false;
		}
	}

	public String getStatusCssClass(String statusText) {
		String result = statusCssMap.get(statusText);
		if (result == null)
			result = "";

		return result;
	}

	private static Map<String, String> createStatusCssMap() {
		String green = "text-green";
		String red = "text-red";

		Map<String, String> map = new HashMap<String, String>();
		map.put("UP", green);
		map.put("DOWN", red);
		return map;
	}

	public int getPollInterval() {
		return pollInterval;
	}
}