package com.neuralt.smp.client.web.controller;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.AgentRuntimeException;
import com.neuralt.smp.client.MonitoredEntity.EntityType;
import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.MonitoredProcess;
import com.neuralt.smp.client.SmpClient;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.ActionCommand;
import com.neuralt.smp.config.ProcessConfig;
import com.neuralt.smp.config.ProcessConfig.EventHandlerConfig;
import com.neuralt.smp.config.ProcessConfig.SNMPConfig;

@ManagedBean
@ViewScoped
public class ProcessManageController extends EntityManageController implements
		Serializable {
	private static final Logger logger = LoggerFactory
			.getLogger(ProcessManageController.class);
	private static final long serialVersionUID = 7588668045319905227L;

	public ProcessManageController() {
		process = new ProcessConfig();
		eventHandlerConfig = new EventHandlerConfig();
		snmp = new SNMPConfig();
		actionCommand = new ActionCommand();
		type = EntityType.PROCESS;
	}

	private ProcessConfig process;
	private EventHandlerConfig eventHandlerConfig;
	private SNMPConfig snmp;
	private ActionCommand actionCommand;

	public void addProcess(MonitoredHost monitoredHost) {
		boolean ok = true;
		if (monitoredHost.getProcess(process.getName()) != null) {
			ok = false;
		}
		if (ok) {
			process.setHostConfig(monitoredHost.getConfig());
			process.generateDefaultEventHandlerConfig();
			monitoredHost.getConfig().addProcessConfig(process);
			try {
				MonitoredProcess monitoredProcess = new MonitoredProcess(
						process, monitoredHost);
				monitoredHost.addProcess(monitoredProcess);
				DataAccess.hostConfigDao.save(monitoredHost.getConfig());
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
						"add-process", process.getAuditString());
				reset();
				if (monitoredHost.isAgentConnected()) {
					monitoredHost.getHostMgr().pushHostConfig();
				}
			} catch (DataAccessException e) {
				monitoredHost.getConfig().deleteProcessConfig(process);
				throwDBExceptionMessage();
			} catch (AgentRuntimeException e) {
				throwAgentExceptionMessage(monitoredHost.getName());
			}
		} else {
			throwDuplicatedNameMessage(type);
		}
		process = new ProcessConfig();

	}

	public void addEventHandlerConfig(MonitoredProcess monitoredProcess) {
		snmp.setEventHandlerConfig(eventHandlerConfig);
		// eventHandlerConfig.setSnmp(snmp);
		eventHandlerConfig.setProcessConfig(monitoredProcess.getConfig());
		monitoredProcess.getConfig().addEventHandler(eventHandlerConfig);
		try {
			DataAccess.processConfigDao.save(monitoredProcess.getConfig());
			if (monitoredProcess.getHost().isAgentConnected()) {
				monitoredProcess.getHost().getHostMgr().pushHostConfig();
			}
		} catch (DataAccessException e) {
			monitoredProcess.getConfig().getEventHandlers()
					.remove(eventHandlerConfig);
			throwDBExceptionMessage();
		} catch (AgentRuntimeException e) {
			throwAgentExceptionMessage(monitoredProcess.getHost().getName());
		} finally {
			eventHandlerConfig = new EventHandlerConfig();
			snmp = new SNMPConfig();
		}
	}

	public void addActionCommand(EventHandlerConfig eventHandlerConfigFromEditer) {
		eventHandlerConfigFromEditer.addAction(actionCommand);
		actionCommand.setEventHandlerConfig(eventHandlerConfigFromEditer);
		logger.debug("after adding relation: " + eventHandlerConfigFromEditer);
		String hostName = null;
		try {
			DataAccess.processConfigDao.save(eventHandlerConfigFromEditer
					.getProcessConfig());
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-action", eventHandlerConfigFromEditer
							.getProcessConfig().getAuditString());
			hostName = eventHandlerConfigFromEditer.getProcessConfig()
					.getHostConfig().getName();
			String sysName = eventHandlerConfigFromEditer.getProcessConfig()
					.getHostConfig().getSystemConfig().getName();
			MonitoredHost host = SmpClient.instance.getHost(sysName, hostName);
			if (host.isAgentConnected()) {
				host.getHostMgr().pushHostConfig();
			}
		} catch (DataAccessException e) {
			eventHandlerConfigFromEditer.getActions().remove(actionCommand);
			throwDBExceptionMessage();
		} catch (AgentRuntimeException e) {
			throwAgentExceptionMessage(hostName);
		} finally {
			actionCommand = new ActionCommand();
		}
	}

	/*
	 * configFile.getProcessConfig().getConfigFiles().add(configFile);
	 * throwDBExceptionMessage(); // TODO handle }
	 */
	public void deleteEventHandlerConfig(EventHandlerConfig eventHandlerConfig) {
		eventHandlerConfig.getProcessConfig().getEventHandlers()
				.remove(eventHandlerConfig);
		String hostName = null;
		try {
			DataAccess.processConfigDao.save(eventHandlerConfig
					.getProcessConfig());
			hostName = eventHandlerConfig.getProcessConfig().getHostConfig()
					.getName();
			String sysName = eventHandlerConfig.getProcessConfig()
					.getHostConfig().getSystemConfig().getName();
			MonitoredHost host = SmpClient.instance.getHost(sysName, hostName);
			if (host.isAgentConnected()) {
				host.getHostMgr().pushHostConfig();
			}
		} catch (DataAccessException e) {
			eventHandlerConfig.getProcessConfig().getEventHandlers()
					.add(eventHandlerConfig);
			throwDBExceptionMessage();
		} catch (AgentRuntimeException e) {
			throwAgentExceptionMessage(hostName);
		}
	}

	public void deleteActionCommand(ActionCommand actionCommand) {
		actionCommand.getEventHandlerConfig().getActions()
				.remove(actionCommand);
		String hostName = null;
		try {
			DataAccess.processConfigDao.save(actionCommand
					.getEventHandlerConfig().getProcessConfig());
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-action", actionCommand.getEventHandlerConfig()
							.getProcessConfig().getAuditString());
			hostName = actionCommand.getEventHandlerConfig().getProcessConfig()
					.getHostConfig().getName();
			String sysName = actionCommand.getEventHandlerConfig()
					.getProcessConfig().getHostConfig().getSystemConfig()
					.getName();
			MonitoredHost host = SmpClient.instance.getHost(sysName, hostName);
			if (host.isAgentConnected()) {
				host.getHostMgr().pushHostConfig();
			}
		} catch (DataAccessException e) {
			actionCommand.getEventHandlerConfig().getActions()
					.add(actionCommand);
			throwDBExceptionMessage();
		} catch (AgentRuntimeException e) {
			throwAgentExceptionMessage(hostName);
		}
	}

	public void deleteProcess(MonitoredProcess selectedProcess) {
		selectedProcess.getHost().deleteProcess(selectedProcess);
		try {
			DataAccess.hostConfigDao
					.save(selectedProcess.getHost().getConfig());
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-process", selectedProcess.getConfig()
							.getAuditString());
		} catch (DataAccessException e) {
			logger.error("failed in deleting process", e);
		}
	}

	public void editProcess(ProcessConfig dummyProcess,
			MonitoredProcess monitoredProcess) {
		// logger.debug("edit process called");
		boolean ok = true;

		if (!monitoredProcess.getName().equals(dummyProcess.getName())) {
			if (monitoredProcess.getHost().getProcess(dummyProcess.getName()) != null) {
				ok = false;
			}
		}
		// logger.debug("edit process ok?"+ok);
		if (ok) {

			ProcessConfig backupProcess = monitoredProcess
					.getBackupProcessConfig();
			monitoredProcess.updateConfig(dummyProcess);

			try {
				DataAccess.processConfigDao.save(monitoredProcess.getConfig());
				// logger.debug("audit logging edit process");
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
						"edit-process", monitoredProcess.getConfig()
								.getAuditString());
				if (monitoredProcess.getHost().isAgentConnected()) {
					monitoredProcess.getHost().getHostMgr().pushHostConfig();
					monitoredProcess.getHost().deleteProcessByName(
							backupProcess.getName());
					monitoredProcess.getHost().addProcess(monitoredProcess);
					// if (monitoredProcess.isNeedRestart(backupProcess)) {
					// monitoredProcess.getHost().getHostMgr().stopProcess(monitoredProcess.getName());
					// monitoredProcess.getHost().getHostMgr().startProcess(monitoredProcess.getName());
					// }
				} else {
					monitoredProcess.getHost().deleteProcessByName(
							backupProcess.getName());
					monitoredProcess.getHost().addProcess(monitoredProcess);
				}
			} catch (DataAccessException e) {
				monitoredProcess.updateConfig(backupProcess);
				throwDBExceptionMessage();
			} catch (AgentRuntimeException e) {
				throwAgentExceptionMessage(monitoredProcess.getHost().getName());
			}

		} else {
			throwDuplicatedNameMessage(type);
		}

	}

	public void editEventHandlerConfig(MonitoredProcess monitoredProcess,
			EventHandlerConfig eventHandlerConfig, EventHandlerConfig dummy) {
		String hostName = null;
		EventHandlerConfig backup = eventHandlerConfig
				.getBackupEventHandlerConfig();
		eventHandlerConfig.updateConfig(dummy);

		try {
			DataAccess.processConfigDao.save(eventHandlerConfig
					.getProcessConfig());
			hostName = eventHandlerConfig.getProcessConfig().getHostConfig()
					.getName();
			String sysName = eventHandlerConfig.getProcessConfig()
					.getHostConfig().getSystemConfig().getName();
			MonitoredHost host = SmpClient.instance.getHost(sysName, hostName);
			if (host.isAgentConnected()) {
				host.getHostMgr().pushHostConfig();
			}
		} catch (DataAccessException e) {
			eventHandlerConfig.updateConfig(backup);
			throwDBExceptionMessage();
		} catch (AgentRuntimeException e) {
			throwAgentExceptionMessage(hostName);
		}
	}

	public void editActionCommand(EventHandlerConfig eventHandlerConfig,
			ActionCommand dummy, ActionCommand actionCommand) {
		String hostName = null;
		ActionCommand backup = actionCommand.getBackup();
		actionCommand.update(dummy);

		try {
			DataAccess.processConfigDao.save(actionCommand
					.getEventHandlerConfig().getProcessConfig());
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-action", actionCommand.getEventHandlerConfig()
							.getProcessConfig().getAuditString());
			hostName = actionCommand.getEventHandlerConfig().getProcessConfig()
					.getHostConfig().getName();
			String sysName = actionCommand.getEventHandlerConfig()
					.getProcessConfig().getHostConfig().getSystemConfig()
					.getName();
			MonitoredHost host = SmpClient.instance.getHost(sysName, hostName);
			if (host.isAgentConnected()) {
				host.getHostMgr().pushHostConfig();
			}
		} catch (DataAccessException e) {
			actionCommand.update(backup);
			throwDBExceptionMessage();
		} catch (AgentRuntimeException e) {
			throwAgentExceptionMessage(hostName);
		}
	}

	public ProcessConfig getProcess() {
		return process;
	}

	public void setProcess(ProcessConfig process) {
		this.process = process;
	}

	public EventHandlerConfig getEventHandlerConfig() {
		return eventHandlerConfig;
	}

	public void setEventHandlerConfig(EventHandlerConfig eventHandlerConfig) {
		this.eventHandlerConfig = eventHandlerConfig;
	}

	public SNMPConfig getSnmp() {
		return snmp;
	}

	public void setSnmp(SNMPConfig snmp) {
		this.snmp = snmp;
	}

	public ActionCommand getActionCommand() {
		return actionCommand;
	}

	public void setActionCommand(ActionCommand actionCommand) {
		this.actionCommand = actionCommand;
	}

	private void reset() {
		process = new ProcessConfig();
	}

}
