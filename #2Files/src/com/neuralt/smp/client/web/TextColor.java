package com.neuralt.smp.client.web;

public enum TextColor {
	RED("text-red"), ORANGE("text-orange"), YELLOW("text-yellow"), GREEN(
			"text-green"), AQUA("text-aqua"), BLUE("text-blue"), VIOLET(
			"text-violet"), ;

	private String cssClass;

	private TextColor(String cssClass) {
		this.cssClass = cssClass;
	}

	public String styleClass() {
		return cssClass;
	}
}
