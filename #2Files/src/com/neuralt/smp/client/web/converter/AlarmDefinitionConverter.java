package com.neuralt.smp.client.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.config.AlarmDefinition;
import com.neuralt.smp.client.service.AlarmService;

public class AlarmDefinitionConverter implements Converter {

	private static final Logger logger = LoggerFactory
			.getLogger(AlarmDefinitionConverter.class);

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String str) {
		// logger.debug("getAsObject called:"+str);
		for (AlarmDefinition def : AlarmService.instance.listAlarmDefinitions()) {
			if (def.toString().equals(str)) {
				return def;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object obj) {
		// logger.debug("getAsString called:"+obj.toString());
		AlarmDefinition def = (AlarmDefinition) obj;
		return def.toString();
	}
}
