package com.neuralt.smp.client.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.snmp4j.smi.OID;

import com.neuralt.smp.client.data.TrapContainer;
import com.neuralt.smp.snmp.util.MibFileLoader;

public class TrapContainerConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String str) {
		for (OID oid : MibFileLoader.getInstance().getSvrTrapNameMap().keySet()) {
			if (MibFileLoader.getInstance().getSvrTrapNameMap().get(oid)
					.equals(str)) {
				TrapContainer container = new TrapContainer();
				container.setName(str);
				container.setOid(oid);
				return container;
			}
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object obj) {
		return obj.toString();
	}

}
