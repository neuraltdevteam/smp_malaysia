package com.neuralt.smp.client.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.web.controller.SystemEditer;
import com.neuralt.smp.config.Site;

public class SiteConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String str) {
		Site site = null;
		if (!str.equals(SystemEditer.selectSiteName)) {
			site = DataAccess.siteDao.getSiteByName(str);
		}
		if (site == null) {
			site = new Site();
			site.setSiteName(SystemEditer.selectSiteName);
		}
		return site;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object obj) {
		Site site = (Site) obj;
		return site.getSiteName();
	}

}
