package com.neuralt.smp.client.web.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.ProductGroup;

public class ProductGroupConverter implements Converter {

	private static final Logger logger = LoggerFactory
			.getLogger(ProductGroupConverter.class);

	@Override
	public Object getAsObject(FacesContext context, UIComponent component,
			String str) {
		if (logger.isDebugEnabled())
			logger.debug("finding product group:" + str);
		ProductGroup gp = DataAccess.productGroupDao.getProductGroupByName(str);
		if (logger.isDebugEnabled())
			logger.debug("found product group:" + gp);
		return gp;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component,
			Object o) {
		ProductGroup dummy = (ProductGroup) o;
		return dummy.getOidAsTrapName();
	}

}
