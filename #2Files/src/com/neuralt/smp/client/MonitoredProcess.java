package com.neuralt.smp.client;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.agent.mbean.model.ProcessHealth;
import com.neuralt.smp.client.data.stats.ProcessStats;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.client.util.FixedLengthLinkedHashMap;
import com.neuralt.smp.client.util.FixedLengthLinkedList;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.config.ProcessConfig;
import com.neuralt.smp.config.ProcessStatus;
import com.neuralt.smp.util.ConstantUtil;
import com.neuralt.smp.util.StringUtil;

public class MonitoredProcess implements MonitoredEntity,
		Comparable<MonitoredProcess> {
	private static final long serialVersionUID = -7398542577618008166L;
	private static final Logger logger = LoggerFactory
			.getLogger(MonitoredProcess.class);

	public static final String ALL_PROCESSES = "ALL";

	private ProcessConfig config;

	private MonitoredHost host;
	private ProcessStatus status;
	private long started;
	private long stopped;

	private Map<Object, Number> cpuHealthMap;
	private Map<Object, Number> memHealthMap;

	private long lastChecked;
	private String duration;
	private Number currentCpuUsage;
	private Number currentMemoryUsage;

	private LinkedList<ProcessHealth> healthNotes;

	public MonitoredProcess(ProcessConfig config, MonitoredHost host) {
		this.config = config;
		this.host = host;
		setStatus(ProcessStatus.STOPPED);

		cpuHealthMap = new FixedLengthLinkedHashMap<Object, Number>(
				AppConfig.getCpuLiveChartPlotSize());
		memHealthMap = new FixedLengthLinkedHashMap<Object, Number>(
				AppConfig.getMemoryLiveChartPlotSize());
		healthNotes = new FixedLengthLinkedList<ProcessHealth>(1000);
	}

	public void receivedNotification(ProcessHealth health) {
		if (logger.isDebugEnabled()) {
			logger.debug("proc health received:" + health);
		}
		if (!status.equals(ProcessStatus.STARTED)) {
			if ((ProcessStatus.DIED.equals(status) || ProcessStatus.RESTARTED
					.equals(status))
					&& health.getStatus().equals(
							ProcessHealth.STATUS_PROCESS_RUNNING)) { // check if
																		// it
																		// died
																		// before
			// EventLog eventLog = new EventLog(Severity.WARNING,
			// SourceType.PROCESS, this, System.currentTimeMillis(),
			// ProcessStatus.RESTARTED.toString());

				CentralizedEventService.getInstance().onRemoteAppStartSuccess(
						host, getName(), "");
				setStatus(ProcessStatus.STARTED);
			}
		}
		// TODO
		// Does it include starting?
		if (!status.equals(ProcessStatus.STOPPING)
				&& !status.equals(ProcessStatus.STOPPED)
				&& !status.equals(ProcessStatus.STARTING)) {
			if (!health.getStatus()
					.equals(ProcessHealth.STATUS_PROCESS_RUNNING)) {
				CentralizedEventService.getInstance().onRemoteAppDown(
						getHost(), getName(),
						"ProcessHealth status: " + health.getStatus());
				setStatus(ProcessStatus.DIED);
			}
		}
		if (health.getStatus().equals(ProcessHealth.STATUS_PROCESS_RUNNING)) {
			setStatus(ProcessStatus.STARTED);
		}
		if (status.equals(ProcessStatus.STARTED)) {
			cpuHealthMap.put(health.getTimeStamp(), health.getCpu());
			memHealthMap.put(health.getTimeStamp(), health.getMem());
			duration = StringUtil.parseEtime(health.getDuration());
			lastChecked = System.currentTimeMillis();
			currentCpuUsage = health.getCpu();
			currentMemoryUsage = health.getMem();

			healthNotes.add(health);
		}
	}

	public ProcessStats computeAverageStats(long periodMs) {
		long now = System.currentTimeMillis();
		ProcessStats stats = new ProcessStats(host.getSys().getName(), host
				.getConfig().getName(), getName(), new Timestamp(now));

		synchronized (healthNotes) {
			int noteCount = 0;
			Iterator<ProcessHealth> backIter = healthNotes.descendingIterator();
			while (backIter.hasNext()) {
				ProcessHealth health = backIter.next();
				if (now - health.getTimeStamp() <= periodMs) {
					noteCount++;
					stats.add(health);
				} else {
					break;
				}
			}
			if (noteCount > 0) {
				stats.divideBy(noteCount);
			}
		}
		return stats;
	}

	public ProcessConfig getBackupProcessConfig() {
		ProcessConfig backupProcess = new ProcessConfig();
		backupProcess.setName(this.getConfig().getName());
		backupProcess.setDescription(this.getConfig().getDescription());
		backupProcess.setEnabled(this.getConfig().isEnabled());
		backupProcess.setPidPath(this.getConfig().getPidPath());
		backupProcess.setStartScriptPath(this.getConfig().getStartScriptPath());
		backupProcess.setStopScriptPath(this.getConfig().getStopScriptPath());
		backupProcess.setAdminLink(this.getConfig().getAdminLink());
		backupProcess.setHostConfig(this.getConfig().getHostConfig());
		return backupProcess;
	}

	public void updateConfig(ProcessConfig dummyProcess) {
		getConfig().setName(dummyProcess.getName());
		getConfig().setDescription(dummyProcess.getDescription());
		getConfig().setEnabled(dummyProcess.isEnabled());
		getConfig().setPidPath(dummyProcess.getPidPath());
		getConfig().setStartScriptPath(dummyProcess.getStartScriptPath());
		getConfig().setStopScriptPath(dummyProcess.getStopScriptPath());
		getConfig().setAdminLink(dummyProcess.getAdminLink());
	}

	public boolean isNeedRestart(ProcessConfig backupProcess) {
		if ((!getConfig().getStartScriptPath().equals(
				backupProcess.getStartScriptPath()))
				&& isRunning()) {
			return true;
		}
		return false;
	}

	public boolean isRunning() {
		return status == ProcessStatus.STARTED;
	}

	public boolean isStopped() {
		return status == ProcessStatus.STOPPED;
	}

	public ProcessStatus getStatus() {
		return status;
	}

	public synchronized void setStatus(ProcessStatus status) {
		this.status = status;
		if (logger.isDebugEnabled())
			logger.debug("set status called:" + status.name());
	}

	public long getStarted() {
		return started;
	}

	public void setStarted(long started) {
		this.started = started;
	}

	public long getStopped() {
		return stopped;
	}

	public void setStopped(long stopped) {
		this.stopped = stopped;
	}

	public ProcessConfig getConfig() {
		return config;
	}

	public MonitoredHost getHost() {
		return host;
	}

	public void setHost(MonitoredHost host) {
		this.host = host;
	}

	public String getName() {
		return config.getName();
	}

	@Override
	public String getDisplayableName() {
		return getName();
	}

	@Override
	public String getFullName() {
		return host.getFullName() + " - " + getName();
	}

	@Override
	public String getStatusText() {
		return status.name();
	}

	@Override
	public EntityType getEntityType() {
		return EntityType.PROCESS;
	}

	public String getDescription() {
		return config.getDescription();
	}

	public Map<Object, Number> getCpuHealthMap() {
		return cpuHealthMap;
	}

	public Map<Object, Number> getMemHealthMap() {
		return memHealthMap;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MonitoredProcess [config=").append(config)
				.append(", host=").append(host).append(", status=")
				.append(status).append("]");
		return builder.toString();
	}

	@Override
	public String getCurrentCpuUsage() {
		return (currentCpuUsage != null && System.currentTimeMillis()
				- lastChecked < AppConfig.getTimeConsideredDisconnected()
				* ConstantUtil.MIN_MS) ? StringUtil
				.formatNumberWithMaximumFractionDigits(currentCpuUsage) : "-";
	}

	@Override
	public String getCurrentMemoryUsage() {
		return (currentMemoryUsage != null && System.currentTimeMillis()
				- lastChecked < AppConfig.getTimeConsideredDisconnected()
				* ConstantUtil.MIN_MS) ? StringUtil
				.formatNumberWithMaximumFractionDigits(currentMemoryUsage)
				: "-";
	}

	@Override
	public String getDuration() {
		if (StringUtil.isNullOrEmpty(duration)
				|| System.currentTimeMillis() - lastChecked > AppConfig
						.getTimeConsideredDisconnected() * ConstantUtil.MIN_MS)
			return "-";
		else {
			return duration;
		}
	}

	@Override
	public String getLastChecked() {
		SimpleDateFormat sf = new SimpleDateFormat(AppConfig.getWebDateFormat());
		return lastChecked != 0 ? sf.format(new Date(lastChecked)) : "-";
	}

	@Override
	public int compareTo(MonitoredProcess o) {
		return config.compareTo(o.getConfig());
	}

}
