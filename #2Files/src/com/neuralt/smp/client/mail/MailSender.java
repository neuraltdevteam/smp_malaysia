package com.neuralt.smp.client.mail;

import java.text.MessageFormat;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.ContactPerson;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.client.event.SmpEvent;
import com.neuralt.smp.client.web.AppConfig;
import com.neuralt.smp.config.model.AbstractContact;
import com.neuralt.smp.util.StringUtil;

public class MailSender {
	private static Properties props;
	private static Session session;
	public static final MailSender instance = new MailSender();
	private static String username;
	private static String password;
	private static String senderAddress;
	private static Address fromAddress;
	private static String subjectFormat;
	private static String outstandingTrapContentFormat;
	private static String unattendedTrapContentFormat;
	private static String outstandingEventContentFormat;
	private static String unattendedEventContentFormat;
	private static final Logger logger = LoggerFactory
			.getLogger(MailSender.class);

	private MailSender() {
		reload();
	}

	public static void main(String[] args) {

		// final String username = "g704668@192.168.80.128";
		// final String password = "abc123";
		//
		// Properties props = new Properties();
		// props.put("mail.smtp.auth", "true");
		// props.put("mail.smtp.host", "192.168.80.128");
		// props.put("mail.smtp.port", "25");
		// props.put("mail.smtp.timeout", "60000");
		//
		// Session session = Session.getInstance(props,
		// new javax.mail.Authenticator() {
		// protected PasswordAuthentication getPasswordAuthentication() {
		// return new PasswordAuthentication(username, password);
		// }
		// });
		//
		// try {
		//
		// Message message = new MimeMessage(session);
		// message.setFrom(fromAddress);
		// message.setRecipients(Message.RecipientType.TO,
		// InternetAddress.parse("g704667@localhost.localdomain"));
		// message.setSubject("Testing Subject");
		// message.setText("mail test");
		// Transport.send(message);
		// if (logger.isDebugEnabled())
		// logger.debug("mail sent");
		//
		// } catch (MessagingException e) {
		// throw new RuntimeException(e);
		// }
	}

	// public void sendOutstandingAlarm(SnmpTrap alarm, ContactPerson contact) {
	// try {
	// Message message = new MimeMessage(session);
	// message.setFrom(fromAddress);
	// message.addRecipient(Message.RecipientType.TO,
	// contactToAddress(contact));
	//
	// message.setSubject("Outstanding Alarm");
	// message.setText(alarm.toString());
	//
	// message.setSubject(MessageFormat.format(subjectFormat,
	// alarm.getTrapAlarmLevel(),alarm.getTrapName()));
	// message.setText(MessageFormat.format(outstandingContentFormat,
	// alarm.getTrapName(), alarm.getTime(), alarm.getDisplayableSource(),
	// alarm.getAppName(), alarm.getLevel(), alarm.getTrapAlarmId(),
	// alarm.getAlarmDescription(), AppConfig.getMyName()));
	// Transport.send(message);
	// if (logger.isDebugEnabled())
	// logger.debug("mail sent");
	//
	// } catch (MessagingException e) {
	// }
	//
	// }

	// public void sendUnattendedAlarm(SnmpTrap alarm, ContactPerson contact) {
	// try {
	// Message message = new MimeMessage(session);
	// message.setFrom(fromAddress);
	// message.addRecipient(Message.RecipientType.TO,
	// contactToAddress(contact));
	// message.setSubject("Unattended Alarm");
	// message.setText(alarm.toString());
	//
	// Transport.send(message);
	// if (logger.isDebugEnabled())
	// logger.debug("mail sent");
	//
	// } catch (MessagingException e) {
	// }
	//
	// }

	// public void sendUnattendedAlarm(SnmpTrap alarm, List<ContactPerson>
	// contacts) {
	// try {
	// Message message = new MimeMessage(session);
	// message.setFrom(fromAddress);
	// StringBuilder sb = new StringBuilder();
	// ContactPerson[] dummy = (ContactPerson[]) contacts.toArray();
	// for (int i=0; i<dummy.length; i++) {
	// sb.append(dummy[i].getEmail());
	// if (i<dummy.length-1) {
	// sb.append(",");
	// }
	// }
	// message.setRecipients(Message.RecipientType.TO,
	// InternetAddress.parse(sb.toString()));
	// message.setSubject("Unattended Alarm");
	// message.setText(alarm.toString());
	//
	// Transport.send(message);
	// if (logger.isDebugEnabled())
	// logger.debug("mail sent");
	//
	// } catch (MessagingException e) {
	// }
	//
	// }

	// public void sendOutstandingAlarm(SnmpTrap trap, Alarm alarmEntry,
	// ContactPerson contact) {
	// try {
	// Message message = new MimeMessage(session);
	// message.setFrom(fromAddress);
	// message.setRecipients(Message.RecipientType.TO,
	// InternetAddress.parse(contact.getEmail()));
	//
	// message.setSubject(MessageFormat.format(subjectFormat,
	// trap.getTrapAlarmLevel(),trap.getTrapName()));
	// message.setText(MessageFormat.format(outstandingContentFormat,
	// trap.getTrapName(),
	// trap.getTime(), trap.getDisplayableSource(), trap.getAppName(),
	// trap.getLevel(),
	// trap.getTrapAlarmId(), trap.getAlarmDescription(), AppConfig.getMyName(),
	// StringUtil.convertTimeFormat(alarmEntry.getFirstOccurred()),
	// StringUtil.convertTimeFormat(alarmEntry.getLastOccurred()),
	// alarmEntry.getOccurrence()));
	// Transport.send(message);
	// if (logger.isDebugEnabled())
	// logger.debug("mail sent");
	//
	// } catch (MessagingException e) {
	// logger.warn("Send email failed");
	// // TODO
	// // add log
	// }
	// }

	// public void sendUnattendedAlarm(Alarm alarm, ContactPerson contact) {
	// try {
	// Message message = new MimeMessage(session);
	// message.setFrom(fromAddress);
	// message.setRecipients(Message.RecipientType.TO,
	// InternetAddress.parse(contact.getEmail()));
	// message.setSubject(MessageFormat.format(subjectFormat,
	// alarm.getLevel().name(),alarm.getName()));
	// message.setText(MessageFormat.format(unattendedContentFormat,
	// alarm.getName(), StringUtil.convertTimeFormat(alarm.getFirstOccurred()),
	// alarm.getSource(), alarm.getLevel().name(),
	// StringUtil.convertTimeFormat(alarm.getFirstOccurred()),
	// StringUtil.convertTimeFormat(alarm.getLastOccurred()),
	// alarm.getOccurrence(), alarm.getOccurrenceCount(),
	// alarm.getDescription(), AppConfig.getMyName()));
	//
	// Transport.send(message);
	// if (logger.isDebugEnabled())
	// logger.debug("mail sent");
	//
	// } catch (MessagingException e) {
	// logger.warn("Send email failed");
	// // TODO
	// // add log
	// }
	// }

	public synchronized void reload() {
		Properties mailProperties = AppConfig.getMailConfig();
		props = new Properties();

		//

		for (Object key : mailProperties.keySet()) {
			String strKey = (String) key;
			if (strKey.startsWith("mail.smtp")) {
				props.put(strKey, (String) mailProperties.get(strKey));
			}
		}
		username = mailProperties.getProperty("mail.username");
		password = mailProperties.getProperty("mail.password");
		senderAddress = mailProperties.getProperty("mail.sender.address");
		try {
			fromAddress = new InternetAddress(senderAddress);
		} catch (AddressException e) {
			logger.warn("sender address error:" + senderAddress);
		}
		subjectFormat = mailProperties.getProperty("mail.subject.format");
		outstandingTrapContentFormat = mailProperties
				.getProperty("mail.outstanding.trap.content.format");
		unattendedTrapContentFormat = mailProperties
				.getProperty("mail.unattended.trap.content.format");
		outstandingEventContentFormat = mailProperties
				.getProperty("mail.outstanding.event.content.format");
		unattendedEventContentFormat = mailProperties
				.getProperty("mail.unattended.event.content.format");
		// testing dummy
		/*
		 * props.put("mail.smtp.auth", "true"); props.put("mail.smtp.host",
		 * AppConfig.getMailHost()); props.put("mail.smtp.port",
		 * AppConfig.getMailPort()); props.put("mail.smtp.timeout",
		 * AppConfig.getMailTimeout()); username = AppConfig.getMailUsername();
		 * password = AppConfig.getMailPassword();
		 */
		session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});
	}

	private Address contactToAddress(AbstractContact contact) {
		StringBuilder sb = new StringBuilder();
		sb.append(contact.getName()).append(" <").append(contact.getEmail())
				.append(">");
		Address address = null;
		try {
			address = new InternetAddress(sb.toString());
		} catch (AddressException e) {

		}
		return address;
	}

	public void sendOutstandingAlarm(SnmpTrap trap, Alarm alarmEntry,
			NotificationGroup group) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(fromAddress);
			addContactToMessage(group, message);
			message.setSubject(MessageFormat.format(subjectFormat,
					trap.getTrapAlarmLevel(), trap.getTrapName()));
			message.setText(MessageFormat.format(
					outstandingTrapContentFormat,
					trap.getTrapName(),
					trap.getTime(),
					trap.getDisplayableSource(),
					trap.getAppName(),
					trap.getLevel(),
					trap.getTrapAlarmId(),
					trap.getAlarmDescription(),
					AppConfig.getMyName(),
					StringUtil.convertTimeFormat(alarmEntry.getFirstOccurred()),
					StringUtil.convertTimeFormat(alarmEntry.getLastOccurred()),
					alarmEntry.getOccurrence()));
			Transport.send(message);
			if (logger.isDebugEnabled())
				logger.debug("mail sent");

		} catch (MessagingException e) {
			logger.warn("Send email failed");
			CentralizedEventService.getInstance().onSysGeneralError(
					"Send email failed: Group=" + group.getName() + ", Reason="
							+ e.getMessage());
		}

	}

	private void addContactToMessage(NotificationGroup group, Message message)
			throws MessagingException {
		for (ContactPerson contact : group.getContacts()) {
			if (!StringUtil.isNullOrEmpty(contact.getEmail())) {
				message.addRecipient(Message.RecipientType.TO,
						contactToAddress(contact));
			}
		}
	}

	public void sendUnattendedAlarm(Alarm alarm, NotificationGroup group) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(fromAddress);
			addContactToMessage(group, message);
			message.setSubject(MessageFormat.format(subjectFormat, alarm
					.getLevel().name(), alarm.getName()));
			message.setText(MessageFormat.format(unattendedTrapContentFormat,
					alarm.getName(),
					StringUtil.convertTimeFormat(alarm.getFirstOccurred()),
					alarm.getSource(), alarm.getLevel().name(),
					StringUtil.convertTimeFormat(alarm.getFirstOccurred()),
					StringUtil.convertTimeFormat(alarm.getLastOccurred()),
					alarm.getOccurrence(), alarm.getOccurrenceCount(),
					alarm.getDescription(), AppConfig.getMyName()));

			Transport.send(message);
			if (logger.isDebugEnabled())
				logger.debug("mail sent");

		} catch (MessagingException e) {
			logger.warn("Send email failed");
			CentralizedEventService.getInstance().onSysGeneralError(
					"Send email failed: Group=" + group.getName() + ", Reason="
							+ e.getMessage());
		}
	}

	public void sendOutstandingAlarm(SmpEvent event, Alarm alarmEntry,
			NotificationGroup group) {
		try {
			Message message = new MimeMessage(session);
			message.setFrom(fromAddress);
			addContactToMessage(group, message);
			message.setSubject(MessageFormat.format(subjectFormat, event
					.getLevel().name(), event.getName()));
			message.setText(MessageFormat.format(
					outstandingEventContentFormat,
					event.getName(),
					event.getEventTime(),
					event.getSource(),
					event.getEventTypeStr(),
					event.getLevel().name(),
					event.getDetails(),
					AppConfig.getMyName(),
					StringUtil.convertTimeFormat(alarmEntry.getFirstOccurred()),
					StringUtil.convertTimeFormat(alarmEntry.getLastOccurred()),
					alarmEntry.getOccurrence()));
			Transport.send(message);
			if (logger.isDebugEnabled())
				logger.debug("mail sent");

		} catch (MessagingException e) {
			logger.warn("Send email failed");
			CentralizedEventService.getInstance().onSysGeneralError(
					"Send email failed: Group=" + group.getName() + ", Reason="
							+ e.getMessage());
		}
	}
}
