package com.neuralt.smp.client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.management.InstanceNotFoundException;
import javax.management.ListenerNotFoundException;
import javax.management.MalformedObjectNameException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.AgentRuntimeException;
import com.neuralt.smp.client.config.HostGroupConfigManager;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.config.HostConfig;
import com.neuralt.smp.config.HostConfig.MonitorType;
import com.neuralt.smp.config.SystemConfig;
import com.neuralt.smp.rrd.RrdService;

/**
 * Top level class for SMP client module
 */
public class SmpClient {

	private static final Logger logger = LoggerFactory
			.getLogger(SmpClient.class);

	public static final SmpClient instance = new SmpClient();

	private LinkedHashMap<String, MonitoredSystem> sysMap;
	private ExecutorService workersPool;
	private LinkedHashMap<String, MonitoredHost> ipHostMap;

	private SmpClient() {
		sysMap = new LinkedHashMap<String, MonitoredSystem>();
		workersPool = Executors.newCachedThreadPool();
		ipHostMap = new LinkedHashMap<String, MonitoredHost>();
	}

	public synchronized void start() throws Exception {
		// String configXmlPath = AppConfig.getHostProcXMLConfigPath();
		List<SystemConfig> systems = HostGroupConfigManager.loadSystemConfig();
		for (SystemConfig sysConfig : systems) {
			logger.info("            Loaded system configs: {}", sysConfig);
		}

		if (systems != null && !systems.isEmpty()) {
			for (SystemConfig sysConfig : systems) {
				addSystem(sysConfig);
			}
		} else {
			logger.debug("init - No system is configured");
		}
		RrdService.getInstance().startAllDataCollection();
	}

	public synchronized void stop() {
		logger.info("Stop requested");

		if (sysMap != null) {
			for (MonitoredSystem sys : sysMap.values()) {
				for (MonitoredHost host : sys.getHosts()) {
					HostManager mgr = host.getHostMgr();
					try {
						mgr.stop();
						mgr.disconnectJMXAgent();
					} catch (Exception e) {
						logger.error("unable to stop host: "
								+ mgr.getHost().getName(), e);
					}
				}
			}
		}
		if (workersPool != null) {
			workersPool.shutdownNow();
		}
	}

	public List<MonitoredSystem> getSystems() {
		List<MonitoredSystem> list = new ArrayList<MonitoredSystem>();
		list.addAll(sysMap.values());
		Collections.sort(list);
		return list;
	}

	public synchronized String setClock(String sysName, String hostName,
			String time) {
		HostManager hostMgr = getHostMgr(sysName, hostName);
		return hostMgr.setClock(time);
	}

	public synchronized void startProcess(String sysName, String hostName,
			String procName) {
		HostManager hostMgr = getHostMgr(sysName, hostName);
		hostMgr.startProcess(procName);
	}

	public synchronized void stopProcess(String sysName, String hostName,
			String procName) {
		HostManager hostMgr = getHostMgr(sysName, hostName);
		hostMgr.stopProcess(procName);
	}

	public synchronized void startProcesses(String sysName, String hostName) {
		HostManager hostMgr = getHostMgr(sysName, hostName);
		hostMgr.startProcesses();
	}

	public synchronized void stopProcesses(String sysName, String hostName) {
		HostManager hostMgr = getHostMgr(sysName, hostName);
		hostMgr.stopProcesses();
	}

	public synchronized void startProcesses(String sysName) {
		MonitoredSystem sys = sysMap.get(sysName);
		if (sys == null)
			throw new IllegalArgumentException("System [" + sysName
					+ "] not found");
		for (MonitoredHost host : sys.getHosts()) {
			if (host.getConfig().isEnabled())
				host.getHostMgr().startProcesses();
		}
	}

	public synchronized void stopProcesses(String sysName) {
		MonitoredSystem sys = sysMap.get(sysName);
		if (sys == null)
			throw new IllegalArgumentException("System [" + sysName
					+ "] not found");
		for (MonitoredHost host : sys.getHosts()) {
			if (host.getConfig().isEnabled())
				host.getHostMgr().stopProcesses();
		}
	}

	public synchronized void startAllProcesses() {
		for (MonitoredSystem sys : sysMap.values()) {
			for (MonitoredHost host : sys.getHosts()) {
				if (host.getConfig().isEnabled())
					host.getHostMgr().startProcesses();
			}
		}
	}

	public synchronized void stopAllProcesses() {
		for (MonitoredSystem sys : sysMap.values()) {
			for (MonitoredHost host : sys.getHosts()) {
				if (host.getConfig().isEnabled())
					host.getHostMgr().stopProcesses();
			}
		}
	}

	public void addSystem(SystemConfig sysConfig) {
		MonitoredSystem sys = new MonitoredSystem(sysConfig);
		sysMap.put(sysConfig.getName(), sys);

		for (MonitoredHost host : sys.getHosts()) {
			addHost(host);
		}
	}

	public void addHost(MonitoredHost host) {
		logger.debug("adding host=" + host.getName());
		ipHostMap.put(host.getIpAddr(), host);
		if ((host.isEnabled()) && (host.getSys().isEnabled())) {
			HostManager mgr = host.getHostMgr();

			try {
				if (host.getConfig().getType().equals(MonitorType.BOTH)
						|| host.getConfig().getType()
								.equals(MonitorType.SCRIPT)) {
					mgr.connectJMXAgent();
					// mgr.startMonProcess(); // as process should be always
					// monitored with script
				}
			} catch (Exception e) {
				logger.error("unable to connect host: " + host.getName(), e);
			}

			mgr.start();
		}
	}

	public void deleteSystem(MonitoredSystem system) throws Exception {
		sysMap.remove(system.getName());
		for (MonitoredHost host : system.getHosts()) {
			deleteHost(host);
		}
		DataAccess.systemConfigDao.delete(system.getConfig());
	}

	public void deleteHost(MonitoredHost host) throws Exception {
		ipHostMap.remove(host.getIpAddr());
		logger.debug("host removed from ip map");
		try {
			host.getHostMgr().stop();
			logger.debug("host stopped");
			host.getHostMgr().disconnectJMXAgent();
			logger.debug("host disconnected");
		} catch (AgentRuntimeException e) {
			logger.debug("AgentRunTimeException:" + e);
			// TODO
		} catch (MalformedObjectNameException e) {

		} catch (InstanceNotFoundException e) {

		} catch (ListenerNotFoundException e) {

		} catch (IOException e) {

		}
		host.getSys().deleteHost(host);

		try {
			RrdService.getInstance().deleteHost(host.getConfig());
			DataAccess.snmpTargetDao.deleteSnmpTargetByHost(host.getConfig());
			DataAccess.scriptConfigDao.deleteScriptConfigByHost(host
					.getConfig());
			DataAccess.systemConfigDao.save(host.getSys().getConfig());
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-host", host.getConfig().toString());
			logger.info("host {} removed from system {}", host.getConfig()
					.getName(), host.getSys().getConfig().getName());
		} catch (DataAccessException e) {
			throw e;
		} catch (Exception e) {
			throw e;
		}
	}

	public void reloadSystem(SystemConfig backupSystem,
			MonitoredSystem monitoredSystem) {
		sysMap.remove(backupSystem.getName());
		sysMap.put(monitoredSystem.getName(), monitoredSystem);
		if (monitoredSystem.getConfig().isEnabled()) {
			for (MonitoredHost host : monitoredSystem.getHosts()) {
				if (!host.getHostMgr().isAgentConnected()
						&& host.getConfig().isEnabled()) {
					host.getHostMgr().start();
				} else if (host.getHostMgr().isAgentConnected()
						&& host.getConfig().isEnabled()) {
					host.getHostMgr().stop();
				}
			}
		} else {
			for (MonitoredHost host : monitoredSystem.getHosts()) {
				host.getHostMgr().stop();
			}
		}
	}

	public void reloadHost(HostConfig backupHost, MonitoredHost monitoredHost)
			throws Exception {
		logger.debug("reloading host...");
		try {
			DataAccess.hostConfigDao.save(monitoredHost.getConfig());
			logger.debug("host saved");
			monitoredHost.getSys().editHostMap(backupHost.getName(),
					monitoredHost);
		} catch (DataAccessException e) {
			throw e;
		}

		// stop and disconnect first, then addHost
		try {
			monitoredHost.getHostMgr().stop();
			logger.debug("host stopped");
			if (monitoredHost.getHostMgr().isAgentConnected()) {
				monitoredHost.getHostMgr().disconnectJMXAgent();
				logger.debug("host disconnected");
			}
		} finally {
			monitoredHost.getHostMgr().stopSnmpGetUsage();
		}

		ipHostMap.remove(backupHost.getIpAddr());
		addHost(monitoredHost);
	}

	/**
	 * SmpClient also houses a thread pool for handling short-lived async tasks
	 * 
	 * @param task
	 * @return
	 */
	public Future<?> submitTask(Runnable task) {
		return workersPool.submit(task);
	}

	private HostManager getHostMgr(String sysName, String hostName)
			throws IllegalArgumentException {
		MonitoredHost host = getHost(sysName, hostName);
		return host.getHostMgr();
	}

	public MonitoredHost getHost(String sysName, String hostName) {
		MonitoredSystem sys = getSystem(sysName);
		MonitoredHost host = sys.getHost(hostName);
		if (host == null)
			throw new IllegalArgumentException("Host [" + hostName
					+ "] not found");
		return host;
	}

	public MonitoredSystem getSystem(String sysName) {
		MonitoredSystem sys = sysMap.get(sysName);
		if (sys == null)
			throw new IllegalArgumentException("System [" + sysName
					+ "] not found");
		return sys;
	}
	
	public MonitoredSystem getSystemByID(long sysID) {
		for(MonitoredSystem sys : sysMap.values()){
			if(sysID == sys.getConfig().getId()){
				return sys;
			}
		}
		return null;
	}

	public LinkedHashMap<String, MonitoredHost> getIpHostMap() {
		return ipHostMap;
	}
}
