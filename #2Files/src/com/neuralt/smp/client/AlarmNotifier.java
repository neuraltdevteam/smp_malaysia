package com.neuralt.smp.client;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.snmp4j.smi.OID;

import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.EventLog;
import com.neuralt.smp.client.data.NotificationGroup;
import com.neuralt.smp.client.data.NotificationGroup.EventType;
import com.neuralt.smp.client.data.SnmpTrap;
import com.neuralt.smp.client.data.config.AlarmDefinition;
import com.neuralt.smp.client.data.config.AlarmMap;
import com.neuralt.smp.client.data.config.AlarmMap.AlarmMapType;
import com.neuralt.smp.client.event.SmpEvent;
import com.neuralt.smp.client.mail.MailSender;
import com.neuralt.smp.client.snmp.util.TrapRelayer;
import com.neuralt.smp.snmp.model.TrapRawObject;

public class AlarmNotifier {

	private static final Logger logger = LoggerFactory
			.getLogger(AlarmNotifier.class);

	public static final AlarmNotifier instance = new AlarmNotifier();

	private AlarmNotifier() {
		reload();
	}

	/**
	 * This map is used for storing the triggers of NotificationGroup for
	 * performance. TODO change the key to AlarmMapKey
	 */
	private Map<OID, List<NotificationGroup>> outstandingMap = new ConcurrentHashMap<OID, List<NotificationGroup>>();
	private Map<OID, List<NotificationGroup>> unattendedMap = new ConcurrentHashMap<OID, List<NotificationGroup>>();
	private List<NotificationGroup> wildcardGroups = new ArrayList<NotificationGroup>();
	private List<NotificationGroup> nonwildcardGroups = new ArrayList<NotificationGroup>();

	/**
	 * This method is used to maintain the consistency between
	 * AlarmDefinition/NotificationGroup setting and the maps held by this
	 * class. It should be called whenever changes are made to
	 * AlarmDefinition/NotificationGroup.
	 */
	public synchronized void reload() {
		logger.info("reloading notification groups....");
		outstandingMap.clear();
		unattendedMap.clear();
		wildcardGroups.clear();
		nonwildcardGroups.clear();
		List<NotificationGroup> list = DataAccess.notificationGroupDao
				.getAllGroups();
		for (NotificationGroup group : list) {
			if (group.isWildcard()) {
				// should construct a list for the all-relay group
				// normally this list should contain at most two groups, so the
				// search on the list will not be a performance issue
				wildcardGroups.add(group);
			} else if (group.getType() == EventType.NEW) {
				nonwildcardGroups.add(group);

				for (AlarmDefinition def : group.getAlarms()) {
					for (AlarmMap map : def.getAlarmMaps()) {
						if (map.getMapType().equals(AlarmMapType.TRAP)) {
							OID oid = null;
							boolean ok = true;
							try {
								oid = new OID(map.getCauseId());
							} catch (Exception e) {
								ok = false;
							}
							if (ok) {
								if (!outstandingMap.containsKey(oid)) {
									outstandingMap.put(oid,
											new ArrayList<NotificationGroup>());
								}
								outstandingMap.get(oid).add(group);
							}
						}
					}
				}
			} else if (group.getType() == EventType.UNATTENDED) {
				for (AlarmDefinition def : group.getAlarms()) {
					for (AlarmMap map : def.getAlarmMaps()) {
						if (map.getMapType().equals(AlarmMapType.TRAP)) {
							OID oid = null;
							boolean ok = true;
							try {
								oid = new OID(map.getCauseId());
							} catch (Exception e) {
								ok = false;
							}
							if (ok) {
								if (!unattendedMap.containsKey(oid)) {
									unattendedMap.put(oid,
											new ArrayList<NotificationGroup>());
								}
								unattendedMap.get(oid).add(group);
							}
						}
					}
				}
			}
		}
		logger.debug("Groups : NEW={}, UNATTENDED={}, WILDCARD={}",
				new Object[] { outstandingMap.size(), unattendedMap.size(),
						wildcardGroups.size() });
	}

	public Map<OID, List<NotificationGroup>> getUnattendedMap() {
		return unattendedMap;
	}

	public void handleOutstandingAlarm(final SnmpTrap trap,
			final Alarm alarmEntry) {
		OID oid = new OID(trap.getOid());
		// System.out.println(oid);
		if (outstandingMap.containsKey(oid)) {
			List<NotificationGroup> list = outstandingMap.get(oid);
			for (NotificationGroup group : list) {
				for (AlarmDefinition def : group.getAlarms()) {
					if (def.getName().equals(alarmEntry.getName())
							&& def.isEnabled()) {
						for (AlarmMap map : def.getAlarmMaps()) {
							if (map.getMapType().equals(AlarmMapType.TRAP)
									&& map.getCauseId().equals(trap.getOid())
									&& map.getCauseLevel().equals(
											alarmEntry.getLevel())) {
								if (group.isEmail()) {
									MailSender.instance.sendOutstandingAlarm(
											trap, alarmEntry, group);
								}
								if (group.isSms()) {

								}
							}
						}
					}
				}
			}
		}
		for (NotificationGroup group : wildcardGroups) {
			if (EventType.NEW.equals(group.getType())) {
				if (group.isEmail()) {
					MailSender.instance.sendOutstandingAlarm(trap, alarmEntry,
							group);
				}
				if (group.isSms()) {

				}
			}
		}
	}

	public void relayTrap(final TrapRawObject raw, final Alarm alarmEntry) {
		OID oid = raw.getOid();
		// System.out.println(oid);
		if (outstandingMap.containsKey(oid)) {
			List<NotificationGroup> list = outstandingMap.get(oid);
			for (NotificationGroup group : list) {
				if (group.isTrap()) {
					// checks if the group contains this alarm
					for (AlarmDefinition def : group.getAlarms()) {
						if (def.getName().equals(alarmEntry.getName())
								&& def.isEnabled()) {
							for (AlarmMap map : def.getAlarmMaps()) {
								if (map.getMapType().equals(AlarmMapType.TRAP)
										&& map.getCauseId().equals(
												raw.getOid().toString())
										&& map.getCauseLevel().equals(
												alarmEntry.getLevel())) {
									TrapRelayer.getInstance().sendTrap(group,
											raw);
								}
							}
						}
					}
				}
			}
		}
		for (NotificationGroup group : wildcardGroups) {
			if (group.isTrap() && EventType.NEW.equals(group.getType())) {
				TrapRelayer.getInstance().sendTrap(group, raw);
			}
		}
	}

	public void handleOutstandingAlarm(SmpEvent event, Alarm alarmEntry,
			SnmpTrap trap) {
		for (NotificationGroup group : nonwildcardGroups) {
			for (AlarmDefinition def : group.getAlarms()) {
				if (def.getName().equals(alarmEntry.getName())
						&& def.isEnabled()) {
					for (AlarmMap map : def.getAlarmMaps()) {
						if (map.getMapType().equals(AlarmMapType.TRAP)
								&& map.getCauseId().equals(
										event.getEventTypeStr())
								&& map.getCauseLevel().equals(
										alarmEntry.getLevel())) {
							if (group.isEmail()) {
								MailSender.instance.sendOutstandingAlarm(event,
										alarmEntry, group);
							}
							if (group.isSms()) {

							}

							if (group.isTrap()) {
								TrapRelayer.getInstance().sendTrap(group,
										trap.getOriginal());
							}
						}
					}
				}
			}
		}
		for (NotificationGroup group : wildcardGroups) {
			if (EventType.NEW.equals(group.getType())) {
				if (group.isEmail()) {
					MailSender.instance.sendOutstandingAlarm(event, alarmEntry,
							group);
				}

				if (group.isSms()) {

				}

				if (group.isTrap()) {
					TrapRelayer.getInstance().sendTrap(group,
							trap.getOriginal());
				}
			}
		}
	}

	public void handleOutstandingAlarm(EventLog event, Alarm alarmEntry) {

	}

	public void handleUnattendedAlarm(Alarm alarm, NotificationGroup group) {
		if (group.isEmail()) {
			MailSender.instance.sendUnattendedAlarm(alarm, group);
			// for (ContactPerson contact : group.getContacts()) {
			// MailSender.instance.sendUnattendedAlarm(alarm, contact);
			// }
		}
		if (group.isSms()) {
		}
		if (group.isTrap()) {
		}
	}

}
