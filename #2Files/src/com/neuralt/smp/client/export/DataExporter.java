package com.neuralt.smp.client.export;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import jxl.Workbook;
import jxl.write.DateFormat;
import jxl.write.DateTime;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.MonitoredHost;
import com.neuralt.smp.client.data.Alarm;
import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.data.DataAccessException;
import com.neuralt.smp.client.data.stats.CpuStats;
import com.neuralt.smp.client.data.stats.DiskStats;
import com.neuralt.smp.client.data.stats.HostStats;
import com.neuralt.smp.client.data.stats.InetStats;
import com.neuralt.smp.client.data.stats.MemStats;
import com.neuralt.smp.client.web.AppConfig;

public class DataExporter {

	private static final Logger logger = LoggerFactory
			.getLogger(DataExporter.class);

	public static DataExporter getInstance() {
		if (instance == null) {
			synchronized (DataExporter.class) {
				if (instance == null) {
					instance = new DataExporter();
				}
			}
		}
		return instance;
	}

	private static DataExporter instance;
	private static WritableCellFormat dateFormat = new WritableCellFormat(
			new DateFormat(AppConfig.getWebDateFormat()));

	private DataExporter() {
	}

	/*
	 * TODO Only temporary, need to be replaced by a more general method.
	 */
	public void exportStats(String fileName, Timestamp from, Timestamp to,
			Set<MonitoredHost> set) {
		try {
			// PropertiesUtil.parsePropertiesValue
			int cpuRow = 0;
			int memRow = 0;
			int diskRow = 0;
			int inetRow = 0;
			logger.debug("from:" + from.toString());
			logger.debug("to:" + to.toString());
			logger.debug("creating workbook");
			WritableWorkbook workbook = Workbook.createWorkbook(new File(
					fileName));
			WritableSheet cpuSheet = workbook.createSheet("CPU", 0);
			WritableSheet memorySheet = workbook.createSheet("Memory", 1);
			WritableSheet diskSheet = workbook.createSheet("Disk", 2);
			WritableSheet inetSheet = workbook.createSheet("Interface", 3);
			cpuSheet.addCell(new Label(0, 0, "Source"));
			cpuSheet.addCell(new Label(1, 0, "Time"));
			cpuSheet.addCell(new Label(2, 0, "Figure"));
			memorySheet.addCell(new Label(0, 0, "Source"));
			memorySheet.addCell(new Label(1, 0, "Time"));
			memorySheet.addCell(new Label(2, 0, "Figure"));
			diskSheet.addCell(new Label(0, 0, "Source"));
			diskSheet.addCell(new Label(1, 0, "Time"));
			diskSheet.addCell(new Label(2, 0, "Figure"));
			inetSheet.addCell(new Label(0, 0, "Source"));
			inetSheet.addCell(new Label(1, 0, "Time"));
			inetSheet.addCell(new Label(2, 0, "Figure"));
			try {
				for (MonitoredHost host : set) {
					List<HostStats> stats = DataAccess.hostStatsDao
							.getHostStats(host.getSys().getConfig().getName(),
									host.getConfig().getName(), from, to, null,
									null);
					Collections.sort(stats);
					logger.debug("number of hostStat=" + stats.size());
					for (HostStats stat : stats) {
						// logger.debug("number of cpuStat="+stat.getCpuStats().size());
						// logger.debug("number of memStat="+stat.getMemStats().size());
						// logger.debug("number of diskStat="+stat.getDiskStats().size());
						// logger.debug("number of inetStat="+stat.getInetStats().size());
						for (CpuStats cpuStat : stat.getCpuStats()) {
							++cpuRow;
							// logger.debug("cpuRow="+cpuRow);
							cpuSheet.addCell(new Label(0, cpuRow, host
									.getConfig().getName()));
							cpuSheet.addCell(new DateTime(1, cpuRow, new Date(
									stat.getTs().getTime()), dateFormat));
							cpuSheet.addCell(new Number(2, cpuRow, cpuStat
									.getUsr()));
						}
						for (MemStats memStat : stat.getMemStats()) {
							++memRow;
							// logger.debug("memRow="+memRow);
							memorySheet.addCell(new Label(0, memRow, host
									.getConfig().getName()));
							memorySheet.addCell(new DateTime(1, memRow,
									new Date(stat.getTs().getTime()),
									dateFormat));
							memorySheet.addCell(new Number(2, memRow, memStat
									.getUsedPct()));
						}
						for (DiskStats diskStat : stat.getDiskStats()) {
							++diskRow;
							// logger.debug("diskRow="+diskRow);
							diskSheet.addCell(new Label(0, diskRow, host
									.getConfig().getName()
									+ ": "
									+ diskStat.getFs()));
							diskSheet.addCell(new DateTime(1, diskRow,
									new Date(stat.getTs().getTime()),
									dateFormat));
							diskSheet.addCell(new Number(2, diskRow, diskStat
									.getUsedPercent()));
						}
						for (InetStats inetStat : stat.getInetStats()) {
							++inetRow;
							inetSheet.addCell(new Label(0, inetRow, host
									.getConfig().getName()
									+ ": "
									+ inetStat.getKey().getDescr()
									+ " (InOctet)"));
							inetSheet.addCell(new DateTime(1, inetRow,
									new Date(stat.getTs().getTime()),
									dateFormat));
							inetSheet.addCell(new Number(2, inetRow, inetStat
									.getInOctet()));
							++inetRow;
							inetSheet.addCell(new Label(0, inetRow, host
									.getConfig().getName()
									+ ": "
									+ inetStat.getKey().getDescr()
									+ " (OutOctet)"));
							inetSheet.addCell(new DateTime(1, inetRow,
									new Date(stat.getTs().getTime()),
									dateFormat));
							inetSheet.addCell(new Number(2, inetRow, inetStat
									.getOutOctet()));
						}
					}
				}
			} catch (DataAccessException e) {
				logger.warn("failed in accessing DB");
			}
			logger.debug("writing workbook");
			workbook.write();
			workbook.close();
			logger.debug("closed workbook");
		} catch (IOException e) {
			logger.debug("failed in writing workbook", e);
		} catch (WriteException e) {
			logger.debug("failed in writing workbook", e);
		}
	}

	public void exportAlarms(String filename, List<Alarm> list) {
		try {
			WritableWorkbook workbook = Workbook.createWorkbook(new File(
					filename));
			WritableSheet sheet = workbook.createSheet("Sheet 1", 0);
			sheet.addCell(new Label(0, 0, "Level"));
			sheet.addCell(new Label(1, 0, "Name"));
			sheet.addCell(new Label(2, 0, "Cause"));
			sheet.addCell(new Label(3, 0, "Source"));
			sheet.addCell(new Label(4, 0, "IP"));
			sheet.addCell(new Label(5, 0, "Status"));
			sheet.addCell(new Label(6, 0, "First Occurred"));
			sheet.addCell(new Label(7, 0, "Last Occurred"));
			sheet.addCell(new Label(8, 0, "Occurrence"));
			sheet.addCell(new Label(9, 0, "Owner"));
			sheet.addCell(new Label(10, 0, "Resolver"));
			sheet.addCell(new Label(11, 0, "Resolved Time"));
			sheet.addCell(new Label(12, 0, "Description"));
			int row = 0;
			for (Alarm alarm : list) {
				++row;
				sheet.addCell(new Label(0, row, alarm.getLevelString()));
				sheet.addCell(new Label(1, row, alarm.getName()));
				sheet.addCell(new Label(2, row, alarm.getCause()));
				sheet.addCell(new Label(3, row, alarm.getSource()));
				sheet.addCell(new Label(4, row, alarm.getSourceIp()));
				sheet.addCell(new Label(5, row, alarm.getStatus()));
				sheet.addCell(new DateTime(6, row, alarm.getFirstOccurred()));
				sheet.addCell(new DateTime(7, row, alarm.getLastOccurred()));
				sheet.addCell(new Number(8, row, alarm.getOccurrence()));
				sheet.addCell(new Label(9, row, alarm.getOwnerId()));
				sheet.addCell(new Label(10, row, alarm.getResolverId()));
				if (alarm.getResolved() != null)
					sheet.addCell(new DateTime(11, row, alarm.getResolved()));
				sheet.addCell(new Label(12, row, alarm.getDescription()));
			}
			logger.debug("writing workbook");
			workbook.write();
			workbook.close();
			logger.debug("closed workbook");
		} catch (IOException e) {
			logger.error("IOException when writing file:" + filename
					+ " exception:" + e);
		} catch (WriteException e) {
			logger.error("WriteException when writing file:" + filename
					+ " exception:" + e);
		}
	}

	public void exportHistory() {

	}

}
