package com.neuralt.smp.client.export;

public class DataSheet {
	private String[][] cells;

	public String[][] getCells() {
		return cells;
	}

	public void setCells(String[][] cells) {
		this.cells = cells;
	}

}
