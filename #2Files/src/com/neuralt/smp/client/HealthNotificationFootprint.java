package com.neuralt.smp.client;

import com.neuralt.smp.notification.WarningLevel;

public class HealthNotificationFootprint {
	private WarningLevel level;
	private String notiClassname;

	public HealthNotificationFootprint(WarningLevel level, String notiClassname) {
		this.level = level;
		this.notiClassname = notiClassname;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result
				+ ((notiClassname == null) ? 0 : notiClassname.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HealthNotificationFootprint other = (HealthNotificationFootprint) obj;
		if (level != other.level)
			return false;
		if (notiClassname == null) {
			if (other.notiClassname != null)
				return false;
		} else if (!notiClassname.equals(other.notiClassname))
			return false;
		return true;
	}

}
