package com.neuralt.smp.client.license;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.license.License;
import com.neuralt.smp.util.Base64Coder;
import com.neuralt.smp.util.ExpiryDateUtil;
import com.neuralt.smp.util.NetworkUtil;

public class LicenseOfficer {
	private static final Logger logger = LoggerFactory
			.getLogger(LicenseOfficer.class);

	public String getContents(String aFile) {
		// ...checks on aFile are elided
		StringBuilder contents = new StringBuilder();

		try {
			BufferedReader input = new BufferedReader(new FileReader(aFile));
			try {
				String line = null; // not declared within while loop

				// read the first line only
				if ((line = input.readLine()) != null) {
					contents.append(line);
				}
			} finally {
				input.close();
			}
		} catch (IOException ex) {
			logger.debug("failed in reading license", ex);
		}

		return contents.toString();
	}

	// private String getHostId() throws Exception{
	// InetAddress addr = InetAddress.getLocalHost();
	// byte[] ipaddr = addr.getAddress();
	// if (ipaddr.length == 4) {
	// int hostid = 0 | ipaddr[1] << 24 | ipaddr[0] << 16 | ipaddr[3] << 8 |
	// ipaddr[2];
	// StringBuilder sb = new StringBuilder();
	// Formatter formatter = new Formatter(sb, Locale.US);
	// formatter.format("%08x", hostid);
	//
	// return sb.toString();
	// } else {
	// throw new Exception("hostid for IPv6 addresses not implemented yet");
	// }
	// }

	/** Read the object from Base64 string. */
	private static Object fromString(String s) throws IOException,
			ClassNotFoundException {
		byte[] data = Base64Coder.decode(s);
		ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(
				data));
		Object o = ois.readObject();
		ois.close();
		return o;
	}

	public void checkServerLicense() throws Exception {
		License aLic = null;
		try {
			String userHome = System.getProperty("user.home");
			String rawLicenseString = null;
			try {
				rawLicenseString = getContents(userHome
						+ "/smpserver/smpserver.lic");
			} catch (NullPointerException e) {
				logger.error(
						"Failed to locate server license file. Please ensure there is a valid license file in the classpath",
						e.getCause());
				throw new Exception(e);
			}
			aLic = (License) fromString(rawLicenseString);

			logger.info("Server License: " + aLic);

			String macAddress = NetworkUtil.getMac(aLic.getInetName());
			Long now = System.currentTimeMillis();

			if (macAddress != null && !macAddress.equals(aLic.getMacAddress())) {
				String msg = "This server is holding an invalid SMP server license. Please contact PCTi for more information.";
				logger.error(msg);
				throw new Exception(msg);
			}

			if (now > ExpiryDateUtil.parseAsLong(aLic.getValidUntil())) {
				String msg = "The SMP server license has expired. Please contact PCTi to renew the license.";
				logger.error(msg);
				throw new Exception(msg);
			}

		} catch (IOException e) {
			logger.error("Failed to load server license file", e.getCause());
			throw new Exception("Failed to check license file");
		} catch (ClassNotFoundException e) {
			logger.error(
					"Failed to load server license file. the license file does not seem to be in a valid format",
					e.getCause());
			throw new Exception("Failed to check license file");
		} catch (Exception e) {
			throw new Exception("Failed to check license file");
		}
	}

	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		LicenseOfficer test = new LicenseOfficer();
		test.checkServerLicense();
	}

}
