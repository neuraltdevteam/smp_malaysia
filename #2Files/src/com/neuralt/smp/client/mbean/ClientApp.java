package com.neuralt.smp.client.mbean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.event.CentralizedEventService;
import com.neuralt.smp.client.web.AppConfig;

public class ClientApp implements ClientAppMBean {
	private static final Logger logger = LoggerFactory
			.getLogger(AppConfig.class);

	@Override
	public void reloadAppConfig() {
		CentralizedEventService.getInstance().onSysConfigReload(
				"Reloading app config from: "
						+ AppConfig.getAppConfigFilePath());

		logger.info("reloadAppConfig() invoked");
		try {
			AppConfig.loadAppConfig();
		} catch (Exception e) {
			logger.error("Failed to reload app config.", e);
			CentralizedEventService.getInstance().onSysConfigReloadError(
					"Failed to reload app config from: "
							+ AppConfig.getAppConfigFilePath());
		}
	}

	@Override
	public void reloadLoggerConfig() {
		CentralizedEventService.getInstance().onSysConfigReload(
				"Reloading app config from: " + AppConfig.getLog4jDir());

		logger.info("reloadLoggerConfig() invoked");
		try {
			AppConfig.loadLogConfig();
		} catch (Exception e) {
			logger.error("Failed to reload logger config.", e);
			CentralizedEventService.getInstance().onSysConfigReloadError(
					"Failed to reload logger config from: "
							+ AppConfig.getLog4jDir());
		}
	}

}
