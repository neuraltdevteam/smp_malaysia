package com.neuralt.smp.client.mbean;

public interface ClientAppMBean {
	public abstract void reloadAppConfig();

	public abstract void reloadLoggerConfig();
}
