package com.neuralt.smp.script;

/**
 * This is the common interface for all kinds of script runners. Implementing
 * this interface is necessary as the scripts are OS dependent.
 *
 */
public interface ScriptRunnerInterface {
	/**
	 * It is expected that the output stream of the script only produces a
	 * single number. If any other value comes to the output, it is likely to be
	 * regarded as an unknown value.
	 */
	public Double query(String path);
}
