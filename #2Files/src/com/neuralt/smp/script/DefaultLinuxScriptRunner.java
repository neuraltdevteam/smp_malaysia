package com.neuralt.smp.script;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.web.AppConfig;

/**
 * This class serves as the default script runner for SMP servers which are
 * established in Linux. It will use <code>sudo</code> command to run scripts,
 * and that is unlikely to be the same as the user who starts up Tomcat
 * 
 * 
 */
public class DefaultLinuxScriptRunner implements ScriptRunnerInterface {

	private static final int PREFIX_ARGUMENT = 3;
	private static final Logger logger = LoggerFactory
			.getLogger(DefaultLinuxScriptRunner.class);
	private String user;

	/**
	 * This constructor will use the default script runner user name specified
	 * in smp_server.properties for execution of scripts
	 */
	public DefaultLinuxScriptRunner() {
		this.user = AppConfig.getScriptRunnerUser();
	}

	/**
	 * This constructor will use the user-provided name as the script runner
	 * 
	 * @param user
	 *            the name of user to run the script
	 */
	public DefaultLinuxScriptRunner(String user) {
		this.user = user;
	}

	private Process exec(String command) throws IOException {
		return exec(new String[] { command });
	}

	private Process exec(String[] command) throws IOException {
		String dir = getDir(command);
		command[0] = makeRunnableCommand(command[0]);

		String[] sudoComm = new String[command.length + PREFIX_ARGUMENT];
		sudoComm[0] = "sudo";
		sudoComm[1] = "-u";
		sudoComm[2] = user;
		for (int i = PREFIX_ARGUMENT; i < command.length + PREFIX_ARGUMENT; i++) {
			sudoComm[i] = command[i - PREFIX_ARGUMENT];
		}
		ProcessBuilder builder = new ProcessBuilder(sudoComm);
		logger.debug("changing working directory to [{}]", dir);
		builder.directory(new File(dir));
		// builder.redirectErrorStream(true);
		logger.debug("executing command: [{}]", command);
		Process process = builder.start();
		builder = null;

		return process;
	}

	private String getDir(String[] command) {
		String dir = ".";
		if (command[0].lastIndexOf('/') > 0)
			dir = command[0].substring(0, command[0].lastIndexOf('/'));
		return dir;
	}

	private String makeRunnableCommand(String command) {
		if (command.lastIndexOf('/') > 0) {
			command = "./" + command.substring(command.lastIndexOf('/') + 1);
		} else if (command != null && !command.startsWith("./"))
			command = "./" + command;

		return command;
	}

	public Double query(String path) {
		Process proc = null;
		try {
			proc = exec(path);
			proc.waitFor();
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(proc.getInputStream()));
			String result = bufferedReader.readLine();
			bufferedReader.close();
			Double retn = Double.valueOf(result);
			return retn;
		} catch (IOException e) {
			logger.debug("failed in query", e);
		} catch (InterruptedException e) {

		} catch (Exception e) {
			// TODO
			// do not let exception ruin the query - just treat it as an unknown
			// value
			// may need further handling
			e.printStackTrace();
		}
		return Double.NaN;
	}

}
