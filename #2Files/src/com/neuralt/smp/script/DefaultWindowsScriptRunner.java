package com.neuralt.smp.script;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultWindowsScriptRunner implements ScriptRunnerInterface {

	private static final Logger logger = LoggerFactory
			.getLogger(DefaultWindowsScriptRunner.class);

	@Override
	public Double query(String path) {
		Process proc = null;
		try {
			ProcessBuilder builder = new ProcessBuilder(path);
			proc = builder.start();
			proc.waitFor();
			BufferedReader bufferedReader = new BufferedReader(
					new InputStreamReader(proc.getInputStream()));
			String result = bufferedReader.readLine();
			bufferedReader.close();
			Double retn = Double.valueOf(result);
			builder = null;
			return retn;
		} catch (IOException e) {
			logger.debug("failed in query", e);
		} catch (InterruptedException e) {

		} catch (Exception e) {
			// TODO
			// do not let exception ruin the query - just treat it as an unknown
			// value
			// may need further handling
		}
		return Double.NaN;
	}

}
