package com.neuralt.blc.monitor.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.blc.monitor.model.MonStat;
import com.neuralt.blc.trace.model.Session;
import com.neuralt.smp.client.data.dao.GenericDAO;

@ManagedBean
@ViewScoped

public class MonStatController implements Serializable {
		
	private static final long serialVersionUID = 1021929391426748800L;

	private static final Logger logger = LoggerFactory.getLogger(MonStatController.class);
	
	public boolean resultsVisible = true;
        
	private LazyDataModel<MonStat> monstats;
	
	public MonStatController() {
		monitor();
	}
	
	public void monitor() {
		System.out.println("in monitor");
		monstats = new MonStatModel();
		//int rowcount = (int) new RawDAO(false).count(buildCriteria());
		int rowcount = this.getDataRowcount();
		System.out.println("monStatTransactions rowcount=" + rowcount);
		monstats.setRowCount(rowcount);		
				
		resultsVisible = true;		
	}
	
	public LazyDataModel<MonStat> getMonstats() {
		return monstats;
	}

	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
            
	public int getDataRowcount() {

		//System.out.println("getDataRowcount");

		GenericDAO<MonStat, Integer> dao = new GenericDAO<MonStat, Integer> (MonStat.class, false);
		
		String strSQLWhere = "";
		String strSQLGroupBy = "server_ip, mon_item_name";
		String strSQLOrderBy = "server_ip, mon_item_name";

		//System.out.println("strSQLWhere1=" + strSQLWhere);	
		
		/*
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = " where " + strSQLWhere;
		*/
		
		//String strSQL = "select * from view_umb_session " + strSQLWhere + strSQLGroupBy + strSQLOrderBy;
		//System.out.println("getDataRowcount strSQL=" + strSQL);
		//List<Session> results = dao.findByCriteriaBySQL(strSQL,null,null);
		
		String strSQL = "call sp_mon_stat(?,?,?,?,?)";
		List<MonStat> results = dao.findByCriteriaBySQLSP("MON_STAT a", strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, null, null);
		
		System.out.println("getDataRowcount results.size()=" + results.size());
		return results.size();
	
						
	}
	
	private class MonStatModel extends LazyDataModel<MonStat> {

		private static final long serialVersionUID = 1L;
		private GenericDAO<MonStat, Integer> dao = new GenericDAO<MonStat, Integer>(MonStat.class, false);

	
		@Override
		public List<MonStat> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {				
			
			String strSQLWhere = "";
			String strSQLGroupBy = "server_ip, mon_item_name";
			String strSQLOrderBy = "server_ip, mon_item_name";

			/*
			if (strSQLWhere.compareTo("") != 0)
				strSQLWhere = " where " + strSQLWhere;
			*/			
			
			if (sortField != null) {

				//strSQLOrderBy = " Order by " + sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";
					
			}			
					
			/*
			String strSQL = "select * from view_umb_session " + strSQLWhere + strSQLGroupBy + strSQLOrderBy;			
			System.out.println("strSQL=" + strSQL);					
			List<Session> results = dao.findByCriteriaBySQL(strSQL, null, null);				
			*/
			
			//System.out.println("strSQLWhere=" + strSQLWhere);	
			String strSQL = "call sp_mon_stat(?,?,?,?,?)";
			List<MonStat> results = dao.findByCriteriaBySQLSP("MON_STAT a", strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, null, null);
					
		    Iterator<MonStat> itr=results.iterator();
		    
		    int i = 1;
		    while(itr.hasNext()){		    	
		        MonStat m =itr.next();
		        System.out.println("Mon Time=" + m.getMon_time());		        
		        System.out.println("Mon item name=" + m.getMon_item_name());
		        System.out.println("Status=" + m.getStatus());
		        System.out.println("");
		    }
		    
			return results;
		}

	}

	
}
					