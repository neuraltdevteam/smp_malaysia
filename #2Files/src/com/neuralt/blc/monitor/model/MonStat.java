package com.neuralt.blc.monitor.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "MON_STAT")
public class MonStat implements Serializable {

	private static final long serialVersionUID = 431707898385908826L;

	@Id
	@GeneratedValue	
	private Long id;
	
	private String mon_time;
	private String server_ip;
	private String mon_item_name;
	private String status;
	
	public MonStat() {
	}

	
	public MonStat(String status, String mon_time, String server_ip, String mon_item_name) //, List<Menu> menus)
	{
		this.status = status;
		this.mon_time = mon_time;	
		this.server_ip = server_ip;	
		this.mon_item_name = mon_item_name;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getMon_time() {
		return mon_time;
	}

	public void setMon_time(String mon_time) {
		this.mon_time = mon_time;
	}

	public String getServer_ip() {
		return server_ip;
	}

	public void setServer_ip(String server_ip) {
		this.server_ip = server_ip;
	}	
		
	public String getMon_item_name() {
		return mon_item_name;
	}

	public void setMon_item_name(String mon_item_name) {
		this.mon_item_name = mon_item_name;
	}	
	
}
