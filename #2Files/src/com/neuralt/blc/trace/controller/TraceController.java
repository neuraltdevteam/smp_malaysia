package com.neuralt.blc.trace.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.blc.report.model.TransactionSummary1;
import com.neuralt.blc.trace.model.BLCInterface_Ext;
import com.neuralt.blc.trace.model.SMSIN;
import com.neuralt.blc.trace.model.SMSIN_Ext;
import com.neuralt.blc.trace.model.SMSOUT;
import com.neuralt.blc.trace.model.Session;
import com.neuralt.blc.trace.model.Session_Ext;
import com.neuralt.blc.trace.model.Menu;
import com.neuralt.blc.trace.model.Menu_Ext;
import com.neuralt.blc.trace.model.UMBInterface;
import com.neuralt.blc.trace.model.BLCInterface;
import com.neuralt.blc.trace.model.UMBInterface_Ext;
import com.neuralt.smp.client.data.dao.GenericDAO;

@ManagedBean
@ViewScoped

public class TraceController implements Serializable {
		
	private static final long serialVersionUID = 4810689518997391810L;	
	private static final Logger logger = LoggerFactory.getLogger(TraceController.class);
	
	private Date searchFrom;
	private Date searchTo;
	private String MSISDN;
	public boolean resultsVisible = false;

    /*
    private List<BLCInterface> BLCInterface1 = new ArrayList<BLCInterface>();
    private List<BLCInterface> BLCInterface2 = new ArrayList<BLCInterface>();
    private List<BLCInterface> BLCInterface3 = new ArrayList<BLCInterface>();
    
    private List<UMBInterface> UMBInterface1 = new ArrayList<UMBInterface>();
    private List<UMBInterface> UMBInterface2 = new ArrayList<UMBInterface>();
    private List<UMBInterface> UMBInterface3 = new ArrayList<UMBInterface>();
    
    private List<Menu> menu = new ArrayList<Menu>();
    private List<Menu> menu1 = new ArrayList<Menu>();
    private List<Menu> menu2 = new ArrayList<Menu>();
    private List<Menu> menu3 = new ArrayList<Menu>();
	*/
    
    //private List<Session> sessions = new ArrayList<Session>();		
        
	private LazyDataModel<Session_Ext> sessions;
	private LazyDataModel<SMSIN_Ext> smsins;
	private LazyDataModel<UMBInterface_Ext> arInterfaces;
	private LazyDataModel<UMBInterface_Ext> chInterfaces;
	
	public TraceController() {

		/*
		BLCInterface1.add(new BLCInterface("010315052810083680000bf9d01", "check Balance", "chk_balance", "2015-05-28 10:08:36", "2015-05-28 10:08:37", 60000, "Success", "0", "01", "USSD"));
		BLCInterface1.add(new BLCInterface("010315052810083680000bf9d01", "Topup", "topup", "2015-05-28 10:08:38", "2015-05-28 10:08:39", 60000, "Success", "0", "01", "USSD"));		
		BLCInterface2.add(new BLCInterface("010315052810083680000bf9d01", "Check Status", "chk_status", "2015-05-28 10:08:39", "2015-05-28 10:08:40", 60000, "Success", "0", "01", "USSD"));
		BLCInterface3.add(new BLCInterface("", "", "", "", "", 0, "", "", "", ""));
				
		UMBInterface1.add(new UMBInterface("010315052810083680000bf9d01", "BLC", "2015-05-28 10:08:36", "2015-05-28 10:08:37", 60000, "Success", "0", BLCInterface1));
		UMBInterface1.add(new UMBInterface("010315052810083680000bf9d01", "BLC", "2015-05-28 10:08:38", "2015-05-28 10:08:39", 60000, "Success", "0", BLCInterface2));		
		UMBInterface2.add(new UMBInterface("010315052810083680000bf9d02", "BLc", "2015-05-28 10:08:44", "2015-05-28 10:08:45", 60000, "Success", "0",BLCInterface3));
		UMBInterface3.add(new UMBInterface("", "", "", "", 0, "", "", BLCInterface3));
				
		menu1.add(new Menu("010315052810083680000bf9d01", "*118#1", "Account", "2015-05-28 10:08:36", "2015-05-28 10:08:38", 120000, "Success", "0", UMBInterface1));
		menu1.add(new Menu("010315052810083680000bf9d02", "*118#1#1","Check Balance", "2015-05-28 10:08:38", "2015-05-28 10:08:40", 120000, "Success", "0", UMBInterface2));		
		menu2.add(new Menu("0103150528100844300009a5f01", "*118#1", "Account", "2015-05-28 10:08:44", "2015-05-28 10:08:45", 60000, "Success", "0", UMBInterface3));
		menu3.add(new Menu("01031505281008175000095a501", "*118#1", "Account", "2015-05-28 10:08:17", "2015-05-28 10:08:18", 60000, "Success", "0", null));
				
		sessions.add(new Session("010315052810083680000bf9d", "01", "*118", "2015-05-28 10:08:36", "2015-05-28 10:08:40", 240000, "PRE", menu1));
		sessions.add(new Session("0103150528100844300009a5f", "01", "*118", "2015-05-28 10:08:44", "2015-05-28 10:08:45", 60000, "PRE", menu2));
		sessions.add(new Session("01031505281008175000095a5", "01", "*118", "2015-05-28 10:08:17", "2015-05-28 10:08:20", 180000, "PRE", menu3));
		*/
		
	}
	
	public void traceTransactions() {
		sessions = new TraceModel();
		//int rowcount = (int) new RawDAO(false).count(buildCriteria());
		int rowcount = this.getDataRowcount();
		System.out.println("traceTransactions ussd rowcount=" + rowcount);
		sessions.setRowCount(rowcount);		
		
		smsins = new TraceSMSModel();
		int rowcountSMS = this.getDataRowcountSMS();
		System.out.println("traceTransactions sms rowcount=" + rowcountSMS);
		smsins.setRowCount(rowcountSMS);		
		
		arInterfaces = new TraceARModel();
		int rowcountar = this.getDataRowcountAR();
		System.out.println("traceTransactions ar rowcount=" + rowcountar);
		arInterfaces.setRowCount(rowcountar);		

		chInterfaces = new TraceThirdPartyChanModel();
		int rowcountch = this.getDataRowcountThirdPartyChan();
		System.out.println("traceTransactions ch rowcount=" + rowcountch);
		chInterfaces.setRowCount(rowcountch);		

		resultsVisible = true;		
	}
	
	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date searchFrom) {
		this.searchFrom = searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date searchTo) {
		this.searchTo = searchTo;
	}

	public String getMSISDN() {
		return MSISDN;
	}

	public void setMSISDN(String MSISDN) {
		this.MSISDN = MSISDN;
	}

	public LazyDataModel<Session_Ext> getSessions() {
		return sessions;
	}

	public LazyDataModel<SMSIN_Ext> getSmsins() {
		return smsins;
	}

	public LazyDataModel<UMBInterface_Ext> getArInterfaces() {
		return arInterfaces;
	}

	public LazyDataModel<UMBInterface_Ext> getChInterfaces() {
		return chInterfaces;
	}
	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
    

    public void onRowToggle(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected Session", "SessionID:" + ((Session) event.getData()).getSession_id());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    
    /*
	private DetachedCriteria buildCriteria() {
		
		//System.out.println("in buildCriteria");		
						
		DetachedCriteria crit = DetachedCriteria.forClass(Session.class);
		
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";

		if (searchFrom != null && searchTo != null)
		{
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}
		
		if (searchFrom != null)
		{
			//System.out.println("startdate >=" + strsearchFrom );	
			crit.add(Property.forName("startdate").ge(strsearchFrom + " 00:00:00"));
		}
		
		if (searchTo != null)
		{
			//System.out.println("startdate <=" + strsearchTo );
			crit.add(Property.forName("startdate").le(strsearchTo + " 23:59:59"));
		}
		
		if (MSISDN != null)
		{
			//System.out.println("msisdn =" + MSISDN );
			crit.add(Property.forName("msisdn").eq(MSISDN));		
		}
		
		return crit;
	}
	*/
    
    
	public int getDataRowcount() {

		//System.out.println("getDataRowcount");

		GenericDAO<Session, Integer> dao = new GenericDAO<Session, Integer>(
				Session.class, false);
		
		String strSQLWhere = "";
		//String strSQLGroupBy = " group by startdate ";
		String strSQLGroupBy = "start_date, session_id";
		String strSQLOrderBy = "start_date, session_id";

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";

		if (searchFrom != null && searchTo != null)
		{
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}
		
		if (searchFrom != null && searchTo != null)
			strSQLWhere = "start_date >= '" + strsearchFrom + " 00:00:00' and start_date <= '" + strsearchTo + " 23:59:59'"; 			
		
		if (MSISDN != null)
		{
			if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
			{
				if (strSQLWhere.compareTo("") != 0)
					strSQLWhere = strSQLWhere + " AND ";

				strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
			}
		}
		//System.out.println("strSQLWhere1=" + strSQLWhere);	
		
		/*
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = " where " + strSQLWhere;
		*/
		
		//String strSQL = "select * from view_umb_session " + strSQLWhere + strSQLGroupBy + strSQLOrderBy;
		//System.out.println("getDataRowcount strSQL=" + strSQL);
		//List<Session> results = dao.findByCriteriaBySQL(strSQL,null,null);
		
		String strSQL = "call sp_trans_trace(?,?,?,?,?)";
		List<Session> results = dao.findByCriteriaBySQLSP("UMB_SESS", strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, 0, 50);
		
		System.out.println("getDataRowcount results.size()=" + results.size());
		return results.size();
	
						
	}
	
	private class TraceModel extends LazyDataModel<Session_Ext> {

		private static final long serialVersionUID = 1L;
		private GenericDAO<Session, Integer> dao = new GenericDAO<Session, Integer>(Session.class, false);
		private GenericDAO<Menu, Integer> daomenu = new GenericDAO<Menu, Integer>(Menu.class, false);
		private GenericDAO<UMBInterface, Integer> daoumbInterface = new GenericDAO<UMBInterface, Integer>(UMBInterface.class, false);
		private GenericDAO<BLCInterface, Integer> daoblcInterface = new GenericDAO<BLCInterface, Integer>(BLCInterface.class, false);
		private GenericDAO<SMSOUT, Integer> daosmsout = new GenericDAO<SMSOUT, Integer>(SMSOUT.class, false);
		
		private GenericDAO<UMBInterface, Integer> daoUMBInterfaceFailed = new GenericDAO<UMBInterface, Integer>(UMBInterface.class, false);
		private GenericDAO<BLCInterface, Integer> daoblcInterfaceFailed = new GenericDAO<BLCInterface, Integer>(BLCInterface.class, false);

	
		@Override
		public List<Session_Ext> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			
			/*
			System.out.println("in TraceModel");			
			System.out.println("first="+first);
			System.out.println("pageSize="+pageSize);					
			System.out.println("sortField="+sortField);
			System.out.println("sortOrder="+sortOrder);
			System.out.println("filters="+filters);
			*/
			
			/*
			DetachedCriteria criteria = buildCriteria();
			
			if (sortField != null) {
				criteria.addOrder(sortOrder == SortOrder.ASCENDING ? Order
						.asc(sortField) : Order.desc(sortField));
								
			}
			if (filters != null && !filters.isEmpty()) {
				for (Map.Entry<String, String> entry : filters.entrySet()) {
					criteria.add(Property.forName(entry.getKey())
							.like(entry.getValue(), MatchMode.ANYWHERE)
							.ignoreCase());
					System.out.println(entry.getKey() + " like "
							+ entry.getValue());
				}
			}
			
			List<Session> results = dao.findByCriteria(criteria, null, null);
			System.out.println("results.size = " + results.size());
			*/
			
			
			String strSQLWhere = "";
			//String strSQLGroupBy = " group by startdate ";
			String strSQLGroupBy = "start_date, session_id";
			String strSQLOrderBy = "start_date";

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";

			if (searchFrom != null && searchTo != null)
			{
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
			}
			
			if (searchFrom != null && searchTo != null)
				strSQLWhere = "start_date >= '" + strsearchFrom + " 00:00:00' and start_date <= '" + strsearchTo + " 23:59:59'"; 			
			
			if (MSISDN != null)
			{
				if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
				{
					if (strSQLWhere.compareTo("") != 0)
						strSQLWhere = strSQLWhere + " AND ";

					strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
				}
			}

			/*
			if (strSQLWhere.compareTo("") != 0)
				strSQLWhere = " where " + strSQLWhere;
			*/			
			
			if (sortField != null) {

				//strSQLOrderBy = " Order by " + sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";
					
			}			
					
			/*
			String strSQL = "select * from view_umb_session " + strSQLWhere + strSQLGroupBy + strSQLOrderBy;			
			System.out.println("strSQL=" + strSQL);					
			List<Session> results = dao.findByCriteriaBySQL(strSQL, null, null);				
			*/
			
			//System.out.println("strSQLWhere=" + strSQLWhere);	
			String strSQL = "call sp_trans_trace(?,?,?,?,?)";
			List<Session> results = dao.findByCriteriaBySQLSP("UMB_SESS", strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, 0, 50);
			
			/*
			strSQLGroupBy = " group by startdate ";

			if (searchFrom != null && searchTo != null)
				strSQLWhere = "startdate >= '" + strsearchFrom + " 00:00:00' and startdate <= '" + strsearchTo + " 23:59:59'"; 			
				
			if (MSISDN != null)
			{
				if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
				{
					if (strSQLWhere.compareTo("") != 0)
						strSQLWhere = strSQLWhere + " AND ";

					strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
				}
			}
			
			if (strSQLWhere.compareTo("") != 0)
				strSQLWhere = " where " + strSQLWhere;
			

			if (sortField != null)
				strSQLOrderBy = " Order by " + strSQLOrderBy;
			*/
						
			List<Session_Ext> results_ext = new ArrayList<Session_Ext>();
			
		    Iterator<Session> itr=results.iterator();
		    int i = 1;
		    while(itr.hasNext()){		    	
		        Session s=itr.next();
		        System.out.println("sessionID=" + s.getSession_id());		        
		        System.out.println("");
		        
				//String strSQLWhereMenu = strSQLWhere + " and substr(transaction_id, 1, 25) = substr('" + s.getSession_id() + "', 1, 25) "; //YM 20151019
		        String strSQLWhereMenu = strSQLWhere + " and session_id = '" + s.getSession_id() + "' ";
		        String strSQLGroupByMenu = "start_date, session_id, transaction_id, menu_name";
				//String strSQLGroupByMenu = strSQLGroupBy; //YM 20151019
				String strSQLOrderByMenu = strSQLOrderBy;
				
				/*
				String strSQLmenu = "select * from view_umb_menu_req " + strSQLWhereMenu + strSQLGroupByMenu + strSQLOrderByMenu;
				System.out.println("strSQL=" + strSQLmenu);		
				List<Menu> resultsmenu = daomenu.findByCriteriaBySQL(strSQLmenu, null, null);	
				*/
				
				String strSQLmenu = "call sp_trans_trace(?,?,?,?,?)";
				List<Menu> resultsmenu = daomenu.findByCriteriaBySQLSP("UMB_MENU_HIT", strSQLmenu, strSQLWhereMenu, strSQLGroupByMenu, strSQLOrderByMenu, null, null);
				
				List<Menu_Ext> resultsmenu_ext = new ArrayList<Menu_Ext>();
				
			    Iterator<Menu> itrmenu=resultsmenu.iterator();
			    int imenu = 1;
			    while(itrmenu.hasNext())
			    {		    	
			        Menu m=itrmenu.next();
			        System.out.println("menu transID=" + m.getTransaction_id());
			        System.out.println("");
			        
					//String strSQLWhereUMBInterface = strSQLWhere + " and transaction_id = '" + m.getTransaction_id() + "' and (reason_code = '00' || reason_code = '0')";
			        String strSQLWhereUMBInterface = strSQLWhere + " and transaction_id = '" + m.getTransaction_id() + "' ";
			        
					String strSQLGroupByUMBInterface = "start_date, session_id, transaction_id";
					//String strSQLGroupByUMBInterface = strSQLGroupBy; //YM 20151019
					String strSQLOrderByUMBInterface = strSQLOrderBy;
					
					/*
					String strSQLumbInterface = "select * from view_umb_interface " + strSQLWhereUMBInterface + strSQLGroupByUMBInterface + strSQLOrderByUMBInterface;
					System.out.println("strSQL=" + strSQLumbInterface);		
					List<UMBInterface> resultsumbInterface = daoumbInterface.findByCriteriaBySQL(strSQLumbInterface, null, null);	
					*/
					String strSQLumbInterface = "call sp_trans_trace(?,?,?,?,?)";
					List<UMBInterface> resultsumbInterface = daoumbInterface.findByCriteriaBySQLSP("BLC_TXN_TYPE", strSQLumbInterface, strSQLWhereUMBInterface, strSQLGroupByUMBInterface, strSQLOrderByUMBInterface, null, null);
										
					List<UMBInterface_Ext> resultsumbInterface_ext = new ArrayList<UMBInterface_Ext>();
					
				    Iterator<UMBInterface> itrumbInterface=resultsumbInterface.iterator();
				    int iumbInterface = 1;
				    while(itrumbInterface.hasNext()){		    	
				        UMBInterface umbInterface =itrumbInterface.next();
				        System.out.println("umb interface transID=" + umbInterface.getTransaction_id());
				        System.out.println("");
				        				       
				        String strSQLWhereBLCInterface = strSQLWhere + " and transaction_id = '" + m.getTransaction_id() + "' ";
				        String strSQLGroupByBLCInterface = "start_date, session_id, transaction_id, api_name";				        
				        //String strSQLGroupByBLCInterface = strSQLGroupBy; //20151019
						String strSQLOrderByBLCInterface = strSQLOrderBy;

						/*
						String strSQLblcInterface = "select * from view_blc_trans " + strSQLWhereBLCInterface + strSQLGroupByBLCInterface + strSQLOrderByBLCInterface;
						System.out.println("strSQL=" + strSQLblcInterface);		
						List<BLCInterface> resultsblcInterface = daoblcInterface.findByCriteriaBySQL(strSQLblcInterface, null, null);	
						*/
						String strSQLblcInterface = "call sp_trans_trace(?,?,?,?,?)";
						List<BLCInterface> resultsblcInterface = daoblcInterface.findByCriteriaBySQLSP("BLC_EXT_INTERFACE", strSQLblcInterface, strSQLWhereBLCInterface, strSQLGroupByBLCInterface, strSQLOrderByBLCInterface, null, null);						
						
						
						String strSQLWhereSMSOut = strSQLWhere + " and transaction_id = '" + m.getTransaction_id() + "' ";
						String strSQLGroupBySMSOut = "start_date";
						String strSQLOrderBySMSOut = strSQLOrderBy;
						
						strSQLWhereSMSOut = strSQLWhereSMSOut.replaceAll("start_date", "request_date");
						strSQLGroupBySMSOut = strSQLGroupBySMSOut.replaceAll("start_date", "request_date");
						strSQLOrderBySMSOut = strSQLOrderBySMSOut.replaceAll("start_date", "request_date");
						
						System.out.println("strSQLWhereSMSOut=" + strSQLWhereSMSOut);	
						
						String strSQLSMSOut = "call sp_trans_trace(?,?,?,?,?)";
						List<SMSOUT> resultsSMSOut = daosmsout.findByCriteriaBySQLSP("SMS_OUT", strSQLSMSOut, strSQLWhereSMSOut, strSQLGroupBySMSOut, strSQLOrderBySMSOut, null, null);
						
						resultsumbInterface_ext.add(new UMBInterface_Ext(umbInterface.getSession_id(), umbInterface.getHost_id(), umbInterface.getShort_code(), umbInterface.getChannel(), umbInterface.getTransaction_id(), umbInterface.getTrans_type(), umbInterface.getIn_api_name(), umbInterface.getOut_api_name(), umbInterface.getStart_date(), umbInterface.getEnd_date(), umbInterface.getDuration(), umbInterface.getTrans_status(), umbInterface.getReason_code(), umbInterface.getOut_host_id(), umbInterface.getOut_channel(), umbInterface.getMsisdn(),  resultsblcInterface, resultsSMSOut));
						
				        ++iumbInterface;
				        
				    }//end while for umb interface
				    
				    resultsmenu_ext.add(new Menu_Ext(m.getSession_id(), m.getHost_id(), m.getShort_code(),m.getTransaction_id(), m.getMenu_id(), m.getMenu_name(), m.getStart_date(), m.getEnd_date(), m.getDuration(), m.getTrans_status(), m.getReason_code(), m.getMsisdn(), m.getMax_end_date(), m.getMax_duration(), resultsumbInterface_ext));
				    
				    
			    }//end while for menu
		        
			    List<UMBInterface_Ext> resultsUMBInterfaceFailed_ext =  null;

			    /*
			    System.out.println("");
			    System.out.println("************* failed umb interface ***********");
			    System.out.println("");
			    
				//String strSQLWhereUMBInterfaceFailed = strSQLWhere + " and substr(transaction_id, 1, 25) = substr('" + s.getSession_id() + "', 1, 25) and reason_code != '0'";
			    String strSQLWhereUMBInterfaceFailed = strSQLWhere + " and session_id = '" + s.getSession_id() + "' and reason_code != '0' and reason_code != '00'";
			    String strSQLGroupByUMBInterfaceFailed = "start_date, session_id, transaction_id";
				//String strSQLGroupByUMBInterfaceFailed = strSQLGroupBy; //YM 20151019
				String strSQLOrderByUMBInterfaceFailed = strSQLOrderBy;
				
				String strSQLUMBInterfaceFailed = "call sp_trans_trace(?,?,?,?,?)";
				List<UMBInterface> resultsUMBInterfaceFailed = daoUMBInterfaceFailed.findByCriteriaBySQLSP("BLC_TXN_TYPE", strSQLUMBInterfaceFailed, strSQLWhereUMBInterfaceFailed, strSQLGroupByUMBInterfaceFailed, strSQLOrderByUMBInterfaceFailed, null, null);
				
				List<UMBInterface_Ext> resultsUMBInterfaceFailed_ext = new ArrayList<UMBInterface_Ext>();
				
			    Iterator<UMBInterface> itrUMBInterfaceFailed=resultsUMBInterfaceFailed.iterator();
			    int iUMBInterfaceFailed = 1;
			    while(itrUMBInterfaceFailed.hasNext()){		    	
			        UMBInterface UMBInterfaceFailed =itrUMBInterfaceFailed.next();
			        System.out.println("transID=" + UMBInterfaceFailed.getTransaction_id());
			        System.out.println("");			        
			        
			        String strSQLWhereBLCInterfaceFailed = strSQLWhere + " and transaction_id = '" + UMBInterfaceFailed.getTransaction_id() + "' ";
			        String strSQLGroupByBLCInterfaceFailed = "start_date, session_id, transaction_id, api_name";
					//String strSQLGroupByBLCInterfaceFailed = strSQLGroupBy;
					String strSQLOrderByBLCInterfaceFailed = strSQLOrderBy;
					
					String strSQLblcInterfaceFailed = "call sp_trans_trace(?,?,?,?,?)";
					List<BLCInterface> resultsblcInterfaceFailed = daoblcInterfaceFailed.findByCriteriaBySQLSP("BLC_EXT_INTERFACE", strSQLblcInterfaceFailed, strSQLWhereBLCInterfaceFailed, strSQLGroupByBLCInterfaceFailed, strSQLOrderByBLCInterfaceFailed, null, null);
					
					resultsUMBInterfaceFailed_ext.add(new UMBInterface_Ext(UMBInterfaceFailed.getSession_id(), UMBInterfaceFailed.getHost_id(), UMBInterfaceFailed.getShort_code(), UMBInterfaceFailed.getChannel(), UMBInterfaceFailed.getTransaction_id(), UMBInterfaceFailed.getTrans_type(), UMBInterfaceFailed.getIn_api_name(), UMBInterfaceFailed.getOut_api_name(), UMBInterfaceFailed.getStart_date(), UMBInterfaceFailed.getEnd_date(), UMBInterfaceFailed.getDuration(), UMBInterfaceFailed.getTrans_status(), UMBInterfaceFailed.getReason_code(), UMBInterfaceFailed.getOut_host_id(), UMBInterfaceFailed.getOut_channel(), UMBInterfaceFailed.getMsisdn(), resultsblcInterfaceFailed));
					
			        //BLCInterface1.add(new BLCInterface("010315052810083680000bf9d01", "check Balance", "chk_balance", "2015-05-28 10:08:36", "2015-05-28 10:08:37", 60000, "Success", "0", "01", "USSD", "6285710020124"));
			        //resultsUMBInterfaceFailed_ext.add(new UMBInterface_Ext(UMBInterfaceFailed.getTransaction_id(), UMBInterfaceFailed.getInterfaceName(),  UMBInterfaceFailed.getStart_date(), UMBInterfaceFailed.getEnd_date(), UMBInterfaceFailed.getDuration(), UMBInterfaceFailed.getTrans_status(), UMBInterfaceFailed.getReason_code(), UMBInterfaceFailed.getMsisdn(),  BLCInterface1));
			        
			        ++iUMBInterfaceFailed;
			        
			    }//end while for UMB Interface Failed
			    */
			    
			    results_ext.add(new Session_Ext(s.getSession_id(), s.getHost_id(), s.getShort_code(), s.getStart_date(), s.getEnd_date(), s.getDuration(), s.getSub_grp_id(), s.getMsisdn(), resultsmenu_ext, resultsUMBInterfaceFailed_ext));
		        
		        ++i;
		        
		    } //end while for session
		    
			
			return results_ext;
		}

	}

	
	public int getDataRowcountSMS() {

		//System.out.println("getDataRowcount");

		GenericDAO<SMSIN, Integer> dao = new GenericDAO<SMSIN, Integer>(
				SMSIN.class, false);
		
		String strSQLWhere = "";
		//String strSQLGroupBy = " group by startdate ";
		String strSQLGroupBy = "";
		String strSQLOrderBy = "start_date";

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";

		if (searchFrom != null && searchTo != null)
		{
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}
		
		if (searchFrom != null && searchTo != null)
			strSQLWhere = "start_date >= '" + strsearchFrom + " 00:00:00' and start_date <= '" + strsearchTo + " 23:59:59'"; 			
		
		if (MSISDN != null)
		{
			if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
			{
				if (strSQLWhere.compareTo("") != 0)
					strSQLWhere = strSQLWhere + " AND ";

				strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
			}
		}
		//System.out.println("strSQLWhere1=" + strSQLWhere);	
		
		/*
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = " where " + strSQLWhere;
		*/
		
		//String strSQL = "select * from view_umb_session " + strSQLWhere + strSQLGroupBy + strSQLOrderBy;
		//System.out.println("getDataRowcount strSQL=" + strSQL);
		//List<Session> results = dao.findByCriteriaBySQL(strSQL,null,null);
		
		String strSQL = "call sp_trans_trace(?,?,?,?,?)";
		List<SMSIN> results = dao.findByCriteriaBySQLSP("SMS_IN", strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, 0, 50);
		
		System.out.println("getDataRowcountSMS results.size()=" + results.size());
		return results.size();
						
	}
	
	private class TraceSMSModel extends LazyDataModel<SMSIN_Ext> {

		private static final long serialVersionUID = 1L;
		private GenericDAO<SMSIN, Integer> dao = new GenericDAO<SMSIN, Integer>(SMSIN.class, false);
		private GenericDAO<UMBInterface, Integer> daoumbInterface = new GenericDAO<UMBInterface, Integer>(UMBInterface.class, false);
		private GenericDAO<BLCInterface, Integer> daoblcInterface = new GenericDAO<BLCInterface, Integer>(BLCInterface.class, false);
		private GenericDAO<SMSOUT, Integer> daosmsout = new GenericDAO<SMSOUT, Integer>(SMSOUT.class, false);
	
		private List<SMSOUT> smsouttest = new ArrayList<SMSOUT>();
		private List<BLCInterface_Ext> BLCInterfacetest = new ArrayList<BLCInterface_Ext>();
		
		@Override
		public List<SMSIN_Ext> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			
		
			//smsouttest.add(new SMSOUT("010315052810091220000ee32", "1", "s", "0", "010315052810091220000ee32", "2015-05-28 00:00:00", "test", "0", "12345",  "2015-05-28 00:00:00",  "2015-05-28 00:00:00", "1", "0", "S","0", "6285710020124", "S", "0"));
			//BLCInterfacetest.add(new BLCInterface_Ext("010315052810091220000ee32", "1", "", "SMS", "010315052810091220000ee32", "chk bal", "chk-bal", "soa-chk-bal", "2015-05-28 00:00:00",  "2015-05-28 00:00:00", 1000, "S", "0", "2","SOA", "6285710020124", smsouttest));
						/*
			System.out.println("in TraceSMSModel");			
			System.out.println("first="+first);
			System.out.println("pageSize="+pageSize);					
			System.out.println("sortField="+sortField);
			System.out.println("sortOrder="+sortOrder);
			System.out.println("filters="+filters);
			*/
			
			/*
			DetachedCriteria criteria = buildCriteria();
			
			if (sortField != null) {
				criteria.addOrder(sortOrder == SortOrder.ASCENDING ? Order
						.asc(sortField) : Order.desc(sortField));
								
			}
			if (filters != null && !filters.isEmpty()) {
				for (Map.Entry<String, String> entry : filters.entrySet()) {
					criteria.add(Property.forName(entry.getKey())
							.like(entry.getValue(), MatchMode.ANYWHERE)
							.ignoreCase());
					System.out.println(entry.getKey() + " like "
							+ entry.getValue());
				}
			}
			
			List<Session> results = dao.findByCriteria(criteria, null, null);
			System.out.println("results.size = " + results.size());
			*/
			
			
			String strSQLWhere = "";
			//String strSQLGroupBy = " group by startdate ";
			String strSQLGroupBy = "";
			String strSQLOrderBy = "start_date";

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";

			if (searchFrom != null && searchTo != null)
			{
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
			}
			
			if (searchFrom != null && searchTo != null)
				strSQLWhere = "start_date >= '" + strsearchFrom + " 00:00:00' and start_date <= '" + strsearchTo + " 23:59:59'"; 			
			
			if (MSISDN != null)
			{
				if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
				{
					if (strSQLWhere.compareTo("") != 0)
						strSQLWhere = strSQLWhere + " AND ";

					strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
				}
			}
			
			if (sortField != null) {

				//strSQLOrderBy = " Order by " + sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";
					
			}			
					
			/*
			String strSQL = "select * from view_umb_session " + strSQLWhere + strSQLGroupBy + strSQLOrderBy;			
			System.out.println("strSQL=" + strSQL);					
			List<Session> results = dao.findByCriteriaBySQL(strSQL, null, null);				
			*/
			
			System.out.println("strSQLWhere=" + strSQLWhere);	
			String strSQL = "call sp_trans_trace(?,?,?,?,?)";
			List<SMSIN> results = dao.findByCriteriaBySQLSP("SMS_IN", strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, 0, 50);
			List<SMSIN_Ext> results_ext = new ArrayList<SMSIN_Ext>();										
			
		    Iterator<SMSIN> itr=results.iterator();

		    while(itr.hasNext()){		 
		    	
		        SMSIN sin=itr.next();
		        System.out.println("sms in sessionid=" + sin.getSession_id());		        
		        System.out.println("");
			
				String strSQLWhereUMBInterface = strSQLWhere + " and session_id = '" + sin.getSession_id() + "' ";
				String strSQLGroupByUMBInterface = "start_date, session_id, transaction_id";
				String strSQLOrderByUMBInterface = strSQLOrderBy;
						
				String strSQLumbInterface = "call sp_trans_trace(?,?,?,?,?)";
				List<UMBInterface> resultsumbInterface = daoumbInterface.findByCriteriaBySQLSP("BLC_TXN_TYPE", strSQLumbInterface, strSQLWhereUMBInterface, strSQLGroupByUMBInterface, strSQLOrderByUMBInterface, null, null);
									
				List<UMBInterface_Ext> resultsumbInterface_ext = new ArrayList<UMBInterface_Ext>();
				
			    Iterator<UMBInterface> itrumbInterface=resultsumbInterface.iterator();
			    int iumbInterface = 1;
			    while(itrumbInterface.hasNext()){		    	
			        UMBInterface umbInterface =itrumbInterface.next();
			        System.out.println("umb interface transID=" + umbInterface.getTransaction_id());
			        System.out.println("");
			        				       
			        String strSQLWhereBLCInterface = strSQLWhere + " and session_id = '" + sin.getSession_id() + "' ";
			        String strSQLGroupByBLCInterface = "start_date, session_id, transaction_id, api_name";				        
					String strSQLOrderByBLCInterface = strSQLOrderBy;

					String strSQLblcInterface = "call sp_trans_trace(?,?,?,?,?)";
					List<BLCInterface> resultsblcInterface = daoblcInterface.findByCriteriaBySQLSP("BLC_EXT_INTERFACE", strSQLblcInterface, strSQLWhereBLCInterface, strSQLGroupByBLCInterface, strSQLOrderByBLCInterface, null, null);						
					

			        
					String strSQLWhereSMSOut = strSQLWhere + " and session_id = '" + sin.getSession_id() + "' ";
					String strSQLGroupBySMSOut = strSQLGroupBy;
					String strSQLOrderBySMSOut = strSQLOrderBy;
					
					strSQLWhereSMSOut = strSQLWhereSMSOut.replaceAll("start_date", "request_date");
					strSQLGroupBySMSOut = strSQLGroupBySMSOut.replaceAll("start_date", "request_date");
					strSQLOrderBySMSOut = strSQLOrderBySMSOut.replaceAll("start_date", "request_date");
					
					System.out.println("strSQLWhereSMSOut=" + strSQLWhereSMSOut);	
					
					String strSQLSMSOut = "call sp_trans_trace(?,?,?,?,?)";
					List<SMSOUT> resultsSMSOut = daosmsout.findByCriteriaBySQLSP("SMS_OUT", strSQLSMSOut, strSQLWhereSMSOut, strSQLGroupBySMSOut, strSQLOrderBySMSOut, null, null);

										
					resultsumbInterface_ext.add(new UMBInterface_Ext(umbInterface.getSession_id(), umbInterface.getHost_id(), umbInterface.getShort_code(), umbInterface.getChannel(), umbInterface.getTransaction_id(), umbInterface.getTrans_type(), umbInterface.getIn_api_name(), umbInterface.getOut_api_name(), umbInterface.getStart_date(), umbInterface.getEnd_date(), umbInterface.getDuration(), umbInterface.getTrans_status(), umbInterface.getReason_code(), umbInterface.getOut_host_id(), umbInterface.getOut_channel(), umbInterface.getMsisdn(),  resultsblcInterface, resultsSMSOut));
					
			        ++iumbInterface;
			        
			    }//end while for umb interface
			    
			    				
				/* YM 20151029
		        String strSQLWhereBLCInterface = strSQLWhere + " and session_id = '" + sin.getSession_id() + "' ";
				String strSQLGroupByBLCInterface = strSQLGroupBy;
				String strSQLOrderByBLCInterface = strSQLOrderBy;
				System.out.println("strSQLWhereBLCInterface=" + strSQLWhereBLCInterface);	


				String strSQLblcInterface = "call sp_trans_trace(?,?,?,?,?)";
				List<BLCInterface> resultsblcInterface = daoblcInterface.findByCriteriaBySQLSP("BLC_EXT_INTERFACE", strSQLblcInterface, strSQLWhereBLCInterface, strSQLGroupByBLCInterface, strSQLOrderByBLCInterface, null, null);
				*/
				
				
				/* YM 20151021
				List<BLCInterface_Ext> resultsblcInterface_ext = new ArrayList<BLCInterface_Ext>();
							
			    Iterator<BLCInterface> itrblcInterface=resultsblcInterface.iterator();
			    while(itrblcInterface.hasNext()){		    	
			        BLCInterface bi =itrblcInterface.next();
			        System.out.println("blc int sessionid=" + bi.getSession_id());		        
			        System.out.println("");
					
			        
			        String strSQLWhereSMSOut = strSQLWhere + " and session_id = '" + bi.getSession_id() + "' ";
					String strSQLGroupBySMSOut = strSQLGroupBy;
					String strSQLOrderBySMSOut = strSQLOrderBy;
					
					strSQLWhereSMSOut = strSQLWhereSMSOut.replaceAll("start_date", "request_date");
					strSQLGroupBySMSOut = strSQLGroupBySMSOut.replaceAll("start_date", "request_date");
					strSQLOrderBySMSOut = strSQLOrderBySMSOut.replaceAll("start_date", "request_date");
					
					System.out.println("strSQLWhereSMSOut=" + strSQLWhereSMSOut);	
					
					String strSQLSMSOut = "call sp_trans_trace(?,?,?,?,?)";
					List<SMSOUT> resultsSMSOut = daosmsout.findByCriteriaBySQLSP("SMS_OUT", strSQLSMSOut, strSQLWhereSMSOut, strSQLGroupBySMSOut, strSQLOrderBySMSOut, null, null);
					
			        
					resultsblcInterface_ext.add(new BLCInterface_Ext(bi.getSession_id(), bi.getHost_id(), bi.getShort_code(), bi.getChannel(), bi.getTransaction_id(), bi.getApi_name(), bi.getStart_date(), bi.getEnd_date(), bi.getDuration(), bi.getTrans_status(), bi.getReason_code(), bi.getOut_host_id(),bi.getOut_channel(), bi.getMsisdn(), resultsSMSOut));
			        //resultsblcInterface_ext.add(new BLCInterface_Ext(bi.getSession_id(), bi.getHost_id(), bi.getShort_code(), bi.getChannel(), bi.getTransaction_id(), bi.getTrans_type(), bi.getIn_api_name(), bi.getOut_api_name(), bi.getStart_date(), bi.getEnd_date(), bi.getDuration(), bi.getTrans_status(), bi.getReason_code(), bi.getOut_host_id(),bi.getOut_channel(), bi.getMsisdn(), smsouttest));			        			        
			    }
			    */

			    results_ext.add(new SMSIN_Ext(sin.getSession_id(), sin.getHost_id(), sin.getDest_addr(), sin.getIn_sms_channel(), sin.getTransaction_id(), sin.getReceived_content(), sin.getStart_date(), sin.getEnd_date(), sin.getDuration(), sin.getTrans_status(), sin.getReason_code(), sin.getMsisdn(), resultsumbInterface_ext, null));
		        //results_ext.add(new SMSIN_Ext(sin.getSession_id(), sin.getHost_id(), sin.getDest_addr(), sin.getIn_sms_channel(), sin.getTransaction_id(), sin.getReceived_content(), sin.getStart_date(), sin.getEnd_date(), sin.getDuration(), sin.getTrans_status(), sin.getReason_code(), sin.getMsisdn(), BLCInterfacetest));
			    
		    }			
		    
			return results_ext;
		}

	}


	public int getDataRowcountAR() {

		//System.out.println("getDataRowcount");

		GenericDAO<UMBInterface, Integer> dao = new GenericDAO<UMBInterface, Integer>(
				UMBInterface.class, false);
		
		String strSQLWhere = "";
		//String strSQLGroupBy = " group by startdate ";
		String strSQLGroupBy = "transaction_id";
		String strSQLOrderBy = "start_date";

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";

		if (searchFrom != null && searchTo != null)
		{
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}
		
		if (searchFrom != null && searchTo != null)
			strSQLWhere = "start_date >= '" + strsearchFrom + " 00:00:00' and start_date <= '" + strsearchTo + " 23:59:59'"; 			
		
		if (MSISDN != null)
		{
			if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
			{
				if (strSQLWhere.compareTo("") != 0)
					strSQLWhere = strSQLWhere + " AND ";

				strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
			}
		}
		//System.out.println("strSQLWhere1=" + strSQLWhere);	
		
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = strSQLWhere + " AND channel = 'AUTORENEWAL' ";
		/*
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = " where " + strSQLWhere;
		*/
		
		//String strSQL = "select * from view_umb_session " + strSQLWhere + strSQLGroupBy + strSQLOrderBy;
		//System.out.println("getDataRowcount strSQL=" + strSQL);
		//List<Session> results = dao.findByCriteriaBySQL(strSQL,null,null);
		
		String strSQL = "call sp_trans_trace(?,?,?,?,?)";
		List<UMBInterface> results = dao.findByCriteriaBySQLSP("BLC_TXN_TYPE", strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, 0, 50);
		
		System.out.println("getDataRowcountBLCInterface results.size()=" + results.size());
		return results.size();
						
	}
	
	private class TraceARModel extends LazyDataModel<UMBInterface_Ext> {

		private static final long serialVersionUID = 1L;
		private GenericDAO<UMBInterface, Integer> daoumbInterface = new GenericDAO<UMBInterface, Integer>(UMBInterface.class, false);
		private GenericDAO<BLCInterface, Integer> daoblcInterface = new GenericDAO<BLCInterface, Integer>(BLCInterface.class, false);
		private GenericDAO<SMSOUT, Integer> daosmsout = new GenericDAO<SMSOUT, Integer>(SMSOUT.class, false);
			
		@Override
		public List<UMBInterface_Ext> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
		
			/*
			System.out.println("in TraceSMSModel");			
			System.out.println("first="+first);
			System.out.println("pageSize="+pageSize);					
			System.out.println("sortField="+sortField);
			System.out.println("sortOrder="+sortOrder);
			System.out.println("filters="+filters);
			*/				
			
			String strSQLWhere = "";
			//String strSQLGroupBy = " group by startdate ";
			String strSQLGroupBy = "";
			String strSQLOrderBy = "start_date";

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";

			if (searchFrom != null && searchTo != null)
			{
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
			}
			
			if (searchFrom != null && searchTo != null)
				strSQLWhere = "start_date >= '" + strsearchFrom + " 00:00:00' and start_date <= '" + strsearchTo + " 23:59:59'"; 			
			
			if (MSISDN != null)
			{
				if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
				{
					if (strSQLWhere.compareTo("") != 0)
						strSQLWhere = strSQLWhere + " AND ";

					strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
				}
			}
			
			
			if (sortField != null) {

				//strSQLOrderBy = " Order by " + sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";
					
			}			
								        				       
	        String strSQLWhereUMBInterface = strSQLWhere + " AND channel = 'AUTORENEWAL' ";
	        String strSQLGroupByUMBInterface = "transaction_id";				        
			String strSQLOrderByUMBInterface = strSQLOrderBy;

			String strSQLblcInterface = "call sp_trans_trace(?,?,?,?,?)";
			List<UMBInterface> resultsumbInterface = daoumbInterface.findByCriteriaBySQLSP("BLC_TXN_TYPE", strSQLblcInterface, strSQLWhereUMBInterface, strSQLGroupByUMBInterface, strSQLOrderByUMBInterface, 0, 50);											
			List<UMBInterface_Ext> resultsumbInterface_ext = new ArrayList<UMBInterface_Ext>();
					 		    
		    Iterator<UMBInterface> itrumbInterface=resultsumbInterface.iterator();
		    while(itrumbInterface.hasNext()){		    	
		        UMBInterface ui =itrumbInterface.next();

		        String strSQLWhereBLCInterface = strSQLWhere + " AND channel = 'AUTORENEWAL'  AND transaction_id = '" + ui.getTransaction_id() + "' ";
		        String strSQLGroupByBLCInterface = "start_date, session_id, transaction_id, api_name";				        
				String strSQLOrderByBLCInterface = strSQLOrderBy;				   
				
				List<BLCInterface> resultsblcInterface = daoblcInterface.findByCriteriaBySQLSP("BLC_EXT_INTERFACE", strSQLblcInterface, strSQLWhereBLCInterface, strSQLGroupByBLCInterface, strSQLOrderByBLCInterface, null, null);											

				String strSQLWhereSMSOut = strSQLWhere + " AND transaction_id = '" + ui.getTransaction_id() + "' ";
				String strSQLGroupBySMSOut = strSQLGroupBy;
				String strSQLOrderBySMSOut = strSQLOrderBy;
				strSQLWhereSMSOut = strSQLWhereSMSOut.replaceAll("start_date", "request_date");
				strSQLGroupBySMSOut = strSQLGroupBySMSOut.replaceAll("start_date", "request_date");
				strSQLOrderBySMSOut = strSQLOrderBySMSOut.replaceAll("start_date", "request_date");
				
				System.out.println("strSQLWhereSMSOut=" + strSQLWhereSMSOut);	
				
				String strSQLSMSOut = "call sp_trans_trace(?,?,?,?,?)";
				List<SMSOUT> resultsSMSOut = daosmsout.findByCriteriaBySQLSP("SMS_OUT", strSQLSMSOut, strSQLWhereSMSOut, strSQLGroupBySMSOut, strSQLOrderBySMSOut, null, null);
				
				//resultsumbInterface_ext.add(new UMBInterface_Ext(ui.getSession_id(), ui.getHost_id(), ui.getShort_code(), "", ui.getTransaction_id(), "", "", "", ui.getStart_date(), ui.getEnd_date(), ui.getDuration(), ui.getTrans_status(), ui.getReason_code(), "", "", ui.getMsisdn(),  resultsblcInterface, resultsSMSOut));
				resultsumbInterface_ext.add(new UMBInterface_Ext(ui.getSession_id(), ui.getHost_id(), ui.getShort_code(), ui.getChannel(), ui.getTransaction_id(), ui.getTrans_type(), ui.getIn_api_name(), ui.getOut_api_name(), ui.getStart_date(), ui.getEnd_date(), ui.getDuration(), ui.getTrans_status(), ui.getReason_code(), ui.getOut_host_id(), ui.getOut_channel(), ui.getMsisdn(),  resultsblcInterface, resultsSMSOut));
				
		       		        
		    }

		  
			return resultsumbInterface_ext;
		}

	}


	
	
	public int getDataRowcountThirdPartyChan() {

		//System.out.println("getDataRowcount");

		GenericDAO<UMBInterface, Integer> dao = new GenericDAO<UMBInterface, Integer>(
				UMBInterface.class, false);
		
		String strSQLWhere = "";
		//String strSQLGroupBy = " group by startdate ";
		String strSQLGroupBy = "transaction_id";
		String strSQLOrderBy = "start_date";

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";

		if (searchFrom != null && searchTo != null)
		{
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}
		
		if (searchFrom != null && searchTo != null)
			strSQLWhere = "start_date >= '" + strsearchFrom + " 00:00:00' and start_date <= '" + strsearchTo + " 23:59:59'"; 			
		
		if (MSISDN != null)
		{
			if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
			{
				if (strSQLWhere.compareTo("") != 0)
					strSQLWhere = strSQLWhere + " AND ";

				strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
			}
		}
			
		
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = strSQLWhere + " AND channel <> 'USSD' AND channel <> 'SMS' AND channel <> 'AUTORENEWAL' ";
		/*
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = " where " + strSQLWhere;
		*/
		
		System.out.println("strSQLWhere1=" + strSQLWhere);
		
		//String strSQL = "select * from view_umb_session " + strSQLWhere + strSQLGroupBy + strSQLOrderBy;
		//System.out.println("getDataRowcount strSQL=" + strSQL);
		//List<Session> results = dao.findByCriteriaBySQL(strSQL,null,null);
		
		String strSQL = "call sp_trans_trace(?,?,?,?,?)";
		List<UMBInterface> results = dao.findByCriteriaBySQLSP("BLC_TXN_TYPE", strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, 0, 50);
		
		System.out.println("getDataRowcountBLCInterface results.size()=" + results.size());
		return results.size();
						
	}
	
	private class TraceThirdPartyChanModel extends LazyDataModel<UMBInterface_Ext> {

		private static final long serialVersionUID = 1L;
		private GenericDAO<UMBInterface, Integer> daoumbInterface = new GenericDAO<UMBInterface, Integer>(UMBInterface.class, false);
		private GenericDAO<BLCInterface, Integer> daoblcInterface = new GenericDAO<BLCInterface, Integer>(BLCInterface.class, false);
		private GenericDAO<SMSOUT, Integer> daosmsout = new GenericDAO<SMSOUT, Integer>(SMSOUT.class, false);
			
		@Override
		public List<UMBInterface_Ext> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
		
			/*
			System.out.println("in TraceSMSModel");			
			System.out.println("first="+first);
			System.out.println("pageSize="+pageSize);					
			System.out.println("sortField="+sortField);
			System.out.println("sortOrder="+sortOrder);
			System.out.println("filters="+filters);
			*/				
			
			String strSQLWhere = "";
			//String strSQLGroupBy = " group by startdate ";
			String strSQLGroupBy = "";
			String strSQLOrderBy = "start_date";

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";

			if (searchFrom != null && searchTo != null)
			{
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
			}
			
			if (searchFrom != null && searchTo != null)
				strSQLWhere = "start_date >= '" + strsearchFrom + " 00:00:00' and start_date <= '" + strsearchTo + " 23:59:59'"; 			
			
			if (MSISDN != null)
			{
				if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
				{
					if (strSQLWhere.compareTo("") != 0)
						strSQLWhere = strSQLWhere + " AND ";

					strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
				}
			}
			
			
			if (sortField != null) {

				//strSQLOrderBy = " Order by " + sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";
					
			}			
								        				       
	        String strSQLWhereUMBInterface = strSQLWhere + " AND channel <> 'USSD' AND channel <> 'SMS' AND channel <> 'AUTORENEWAL' ";
	        String strSQLGroupByUMBInterface = "transaction_id";				        
			String strSQLOrderByUMBInterface = strSQLOrderBy;

			String strSQLblcInterface = "call sp_trans_trace(?,?,?,?,?)";
			List<UMBInterface> resultsumbInterface = daoumbInterface.findByCriteriaBySQLSP("BLC_TXN_TYPE", strSQLblcInterface, strSQLWhereUMBInterface, strSQLGroupByUMBInterface, strSQLOrderByUMBInterface, 0, 50);											
			List<UMBInterface_Ext> resultsumbInterface_ext = new ArrayList<UMBInterface_Ext>();
					 		    
		    Iterator<UMBInterface> itrumbInterface=resultsumbInterface.iterator();
		    while(itrumbInterface.hasNext()){		    	
		        UMBInterface ui =itrumbInterface.next();

		        String strSQLWhereBLCInterface = strSQLWhere + " AND channel <> 'USSD' AND channel <> 'SMS' AND channel <> 'AUTORENEWAL'  AND transaction_id = '" + ui.getTransaction_id() + "' ";
		        String strSQLGroupByBLCInterface = "start_date, session_id, transaction_id, api_name";				        
				String strSQLOrderByBLCInterface = strSQLOrderBy;				   
				
				List<BLCInterface> resultsblcInterface = daoblcInterface.findByCriteriaBySQLSP("BLC_EXT_INTERFACE", strSQLblcInterface, strSQLWhereBLCInterface, strSQLGroupByBLCInterface, strSQLOrderByBLCInterface, null, null);											

				String strSQLWhereSMSOut = strSQLWhere + " AND transaction_id = '" + ui.getTransaction_id() + "' ";
				String strSQLGroupBySMSOut = strSQLGroupBy;
				String strSQLOrderBySMSOut = strSQLOrderBy;
				strSQLWhereSMSOut = strSQLWhereSMSOut.replaceAll("start_date", "request_date");
				strSQLGroupBySMSOut = strSQLGroupBySMSOut.replaceAll("start_date", "request_date");
				strSQLOrderBySMSOut = strSQLOrderBySMSOut.replaceAll("start_date", "request_date");
				
				System.out.println("strSQLWhereSMSOut=" + strSQLWhereSMSOut);	
				
				String strSQLSMSOut = "call sp_trans_trace(?,?,?,?,?)";
				List<SMSOUT> resultsSMSOut = daosmsout.findByCriteriaBySQLSP("SMS_OUT", strSQLSMSOut, strSQLWhereSMSOut, strSQLGroupBySMSOut, strSQLOrderBySMSOut, null, null);
				
				resultsumbInterface_ext.add(new UMBInterface_Ext(ui.getSession_id(), ui.getHost_id(), ui.getShort_code(), ui.getChannel(), ui.getTransaction_id(), ui.getTrans_type(), ui.getIn_api_name(), ui.getOut_api_name(), ui.getStart_date(), ui.getEnd_date(), ui.getDuration(), ui.getTrans_status(), ui.getReason_code(), ui.getOut_host_id(), ui.getOut_channel(), ui.getMsisdn(),  resultsblcInterface, resultsSMSOut));				
		       		        
		    }

		  
			return resultsumbInterface_ext;
		}

	}
	
	
	
	
	
}
					