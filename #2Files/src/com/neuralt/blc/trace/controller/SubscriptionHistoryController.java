package com.neuralt.blc.trace.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.primefaces.event.ToggleEvent;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.blc.report.model.TransactionSummary1;
import com.neuralt.blc.trace.model.AutoRenewEvent;
import com.neuralt.blc.trace.model.AutoRenewDeletions;
import com.neuralt.smp.client.data.dao.GenericDAOBLC;

@ManagedBean
@ViewScoped

public class SubscriptionHistoryController implements Serializable {
		
	private static final long serialVersionUID = 4810689518997391888L;	
	
	private static final Logger logger = LoggerFactory.getLogger(SubscriptionHistoryController.class);
	
	private Date searchFrom;
	private Date searchTo;
	private String MSISDN;
	public boolean resultsVisible = false;
    		      
	private LazyDataModel<AutoRenewDeletions> autorenewdeletions;
	
	public SubscriptionHistoryController() {
		
	}
	
	public void traceTransactions() {
		autorenewdeletions = new TraceModel();
		int rowcount = this.getDataRowcount();
		System.out.println("traceTransactions ussd rowcount=" + rowcount);
		autorenewdeletions.setRowCount(rowcount);		
		
		resultsVisible = true;		
	}
	
	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date searchFrom) {
		this.searchFrom = searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date searchTo) {
		this.searchTo = searchTo;
	}

	public String getMSISDN() {
		return MSISDN;
	}

	public void setMSISDN(String MSISDN) {
		this.MSISDN = MSISDN;
	}

	public LazyDataModel<AutoRenewDeletions> getAutorenewdeletions() {
		return autorenewdeletions;
	}
	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
    

    public void onRowToggle(ToggleEvent event) {
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Selected id", "DeletionID:" + ((AutoRenewDeletions) event.getData()).getDelete_id());
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }
    	
	
	public int getDataRowcount() {

		//System.out.println("getDataRowcount");

		GenericDAOBLC<AutoRenewEvent, Integer> daoautorenewevent = new GenericDAOBLC<AutoRenewEvent, Integer>(
				AutoRenewEvent.class, false);
		
		GenericDAOBLC<AutoRenewDeletions, Integer> daoautorenewdeletions = new GenericDAOBLC<AutoRenewDeletions, Integer>(
				AutoRenewDeletions.class, false);
		
		String strSQLWhere = "";
		String strSQLWhere1 = "";
		//String strSQLGroupBy = " group by startdate ";
		String strSQLGroupBy = "";
		String strSQLOrderBy = "";

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";

	    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
	    Date now = new Date();	        
	    Date dateBefore = new Date(now.getTime() - 720 * 24 * 3600 * 1000  ); //Subtract n days  	   	  
	    String strNow = sdfDate.format(now);
	    String strdateBefore = sdfDate.format(dateBefore);
	    
		strSQLWhere1 = "subscription_date >= '" + strdateBefore + "' and subscription_date <= '" + strNow + "'";
		
		if (searchFrom != null && searchTo != null)
		{
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);

			System.out.println("strsearchFrom=" + strsearchFrom);
			System.out.println("strsearchTo=" + strsearchTo);
		}
		else
		{
			System.out.println("strsearchFrom=null");
			System.out.println("strsearchTo==null");
		}
		
		if (searchFrom != null && searchTo != null)
			strSQLWhere = "delete_date >= '" + strsearchFrom + " 00:00:00' and delete_date <= '" + strsearchTo + " 23:59:59'"; 			
		
		if (MSISDN != null)
		{
			if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
			{
				if (strSQLWhere.compareTo("") != 0)
				{
					strSQLWhere = strSQLWhere + " AND ";
					strSQLWhere1 = strSQLWhere1 + " AND ";
				}
				strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
				strSQLWhere1 = strSQLWhere1 + " msisdn = '" + MSISDN + "' ";
			}
		}
			
		
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = strSQLWhere + "";
		/*
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = " where " + strSQLWhere;
		*/
		
		System.out.println("strSQLWhere1=" + strSQLWhere1);
		System.out.println("strSQLWhere=" + strSQLWhere);
		
		//String strSQL = "select * from view_umb_session " + strSQLWhere + strSQLGroupBy + strSQLOrderBy;
		//System.out.println("getDataRowcount strSQL=" + strSQL);
		//List<Session> results = dao.findByCriteriaBySQL(strSQL,null,null);
		
		String strSQL = "call sp_trans_trace(?,?,?,?,?)";
		List<AutoRenewEvent> results = daoautorenewevent.findByCriteriaBySQLSP("AUTO_RENEW_EVENT", strSQL, strSQLWhere1, strSQLGroupBy, strSQLOrderBy, 0, 50);		
		System.out.println("getDataRowcountAutoRenewDeletion results.size()=" + results.size());

		String strSQL2 = "call sp_trans_trace(?,?,?,?,?)";
		List<AutoRenewDeletions> results2 = daoautorenewdeletions.findByCriteriaBySQLSP("AUTO_RENEW_DELETIONS", strSQL2, strSQLWhere, strSQLGroupBy, strSQLOrderBy, 0, 50);		
		System.out.println("getDataRowcountAutoRenewDeletion results.size()=" + results2.size());
		
		
		return results.size() + results2.size();
						
	}
	
	private class TraceModel extends LazyDataModel<AutoRenewDeletions> {

		private static final long serialVersionUID = 1L;
		private GenericDAOBLC<AutoRenewDeletions, Integer> daoautorenewdeletions = new GenericDAOBLC<AutoRenewDeletions, Integer>(AutoRenewDeletions.class, false);
		private GenericDAOBLC<AutoRenewDeletions, Integer> daoautorenewdeletions2 = new GenericDAOBLC<AutoRenewDeletions, Integer>(AutoRenewDeletions.class, false);
		private GenericDAOBLC<AutoRenewEvent, Integer> daoautorenewevent = new GenericDAOBLC<AutoRenewEvent, Integer>(AutoRenewEvent.class, false);		
			
		@Override
		public List<AutoRenewDeletions> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
		
			/*
			System.out.println("in TraceSMSModel");			
			System.out.println("first="+first);
			System.out.println("pageSize="+pageSize);					
			System.out.println("sortField="+sortField);
			System.out.println("sortOrder="+sortOrder);
			System.out.println("filters="+filters);
			*/				
			
			String strSQLWhere = "";
			String strSQLWhere1 = "";
			//String strSQLGroupBy = " group by startdate ";
			String strSQLGroupBy = "";
			String strSQLOrderBy = "delete_date";

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";

		    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
		    Date now = new Date();	        
		    Date dateBefore = new Date(now.getTime() - 720 * 24 * 3600 * 1000  ); //Subtract n days  	   	  
		    String strNow = sdfDate.format(now);
		    String strdateBefore = sdfDate.format(dateBefore);
		    
			strSQLWhere1 = "subscription_date >= '" + strdateBefore + "' and subscription_date <= '" + strNow + "'";
					
			if (searchFrom != null && searchTo != null)
			{
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
			}
			
			if (searchFrom != null && searchTo != null)
				strSQLWhere = "delete_date >= '" + strsearchFrom + " 00:00:00' and delete_date <= '" + strsearchTo + " 23:59:59'"; 			
			
			if (MSISDN != null)
			{
				if (MSISDN.compareTo("") != 0 && MSISDN.compareToIgnoreCase("all") != 0)
				{
					if (strSQLWhere.compareTo("") != 0)
					{
						strSQLWhere = strSQLWhere + " AND ";
						strSQLWhere1 = strSQLWhere1 + " AND ";
					}
					strSQLWhere = strSQLWhere + " msisdn = '" + MSISDN + "' ";
					strSQLWhere1 = strSQLWhere1 +" msisdn = '" + MSISDN + "' ";
				}
			}
			
			
			if (sortField != null) {

				//strSQLOrderBy = " Order by " + sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";
				else
					strSQLOrderBy = strSQLOrderBy + " desc";	
			}							
			else
				strSQLOrderBy = strSQLOrderBy + " desc";
			
			System.out.println("strSQLWhere1(TraceModel)=" + strSQLWhere1);
			System.out.println("strSQLWhere(TraceModel)=" + strSQLWhere);

			String strSQLWhereAutoRenewEvent = strSQLWhere1 + "";
	        String strSQLGroupByAutoRenewEvent = "";				        
			String strSQLOrderByAutoRenewEvent = "subscription_date desc";

			String strSQLautorenewevent = "call sp_trans_trace(?,?,?,?,?)";			
			List<AutoRenewEvent> resultsautorenewevent = daoautorenewevent.findByCriteriaBySQLSP("AUTO_RENEW_EVENT", strSQLautorenewevent, strSQLWhereAutoRenewEvent, strSQLGroupByAutoRenewEvent, strSQLOrderByAutoRenewEvent, 0, 50);																
			List<AutoRenewDeletions> resultsautorenewdeletions = new ArrayList<AutoRenewDeletions>();			
		    Iterator<AutoRenewEvent> itrautorenewevent=resultsautorenewevent.iterator();
		    while(itrautorenewevent.hasNext()){		    	
		    	AutoRenewEvent ui =itrautorenewevent.next();

		        String strSQLWhereAutoRenewDeletions = strSQLWhere + "";
		        String strSQLGroupByAutoRenewDeletions = "";				        
				String strSQLOrderByAutoRenewDeletions = strSQLOrderBy;				   
												
				resultsautorenewdeletions.add(new AutoRenewDeletions("", "", ui.getId()+"", ui.getBundle_sncode(), ui.getBundle_name(), ui.getPurchase_date(), ui.getExpiry_date(), ui.getNext_action_date(), ui.getAuto_renew(), ui.getRenewal_state(), ui.getProcessing_state(), ui.getProcessing_owner(), ui.getStatus_code(), ui.getFree_count(), ui.getMsisdn(), ui.getActive_flag(), ui.getBundle_type(), ui.getSubscription_date(), ui.getTemp_sncode_in_use(), ui.getFreebies_idx(), ui.getLast_update_date(), ui.getSource(), ui.getLast_action(), "Y"));
				
				String strSQLautorenewdeletions = "call sp_trans_trace(?,?,?,?,?)";	
				List<AutoRenewDeletions> resultsautorenewdeletion2 = daoautorenewdeletions2.findByCriteriaBySQLSP("AUTO_RENEW_DELETIONS", strSQLautorenewdeletions, strSQLWhereAutoRenewDeletions, strSQLGroupByAutoRenewDeletions, strSQLOrderByAutoRenewDeletions, 0, 50);														    
				Iterator<AutoRenewDeletions> itrautorenewdelete=resultsautorenewdeletion2.iterator();
			    while(itrautorenewdelete.hasNext()){		    	
			    	AutoRenewDeletions ui2 = itrautorenewdelete.next();
			    	resultsautorenewdeletions.add(new AutoRenewDeletions(ui2.getDelete_date(), ui2.getDelete_cause(), ui2.getId(), ui2.getBundle_sncode(), ui2.getBundle_name(), ui2.getPurchase_date(), ui2.getExpiry_date(), ui2.getNext_action_date(), ui2.getAuto_renew(), ui2.getRenewal_state(), ui2.getProcessing_state(), ui2.getProcessing_owner(), ui2.getStatus_code(), ui2.getFree_count(), ui2.getMsisdn(), ui2.getActive_flag(), ui2.getBundle_type(), ui2.getSubscription_date(), ui2.getTemp_sncode_in_use(), ui2.getFreebies_idx(), ui2.getLast_update_date(), ui2.getSource(), ui2.getLast_action(), "N"));
			    }
			    
								
		       		        
		    }

		  
			return resultsautorenewdeletions;
		}

	}
	
	
	
	
	
}
					