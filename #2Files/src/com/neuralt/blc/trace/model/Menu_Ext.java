package com.neuralt.blc.trace.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "Menu_Ext")
public class Menu_Ext implements Serializable {

	private static final long serialVersionUID = -2754495580875353327L;

	@Id
	@GeneratedValue	
	private Long id;

	private String host_id;
	private String short_code;
	private String session_id;
	private String transaction_id;
	private String msisdn;
	private String menu_id;	
	private String menu_name;	
	private String start_date;
	private String end_date;
	private long duration;	
	private String trans_status;
	private String reason_code;
	private String max_end_date;
	private long max_duration;	
	public List<UMBInterface_Ext> UMBInterfaces = new ArrayList<UMBInterface_Ext>();
	
	public Menu_Ext() {
	}
	
	public Menu_Ext(String session_id, String host_id, String short_code, String transaction_id, String menu_id, String menu_name, String start_date, String end_date, long duration, String trans_status, String reason_code, String msisdn, String max_end_date, long max_duration, List<UMBInterface_Ext> UMBInterfaces)
	{
		this.session_id = session_id;
		this.host_id = host_id;	
		this.short_code = short_code;			
		this.transaction_id = transaction_id;
		this.menu_id = menu_id;	
		this.menu_name = menu_name;	
		this.start_date = start_date;
		this.end_date = end_date;			
		this.duration = duration;
		this.trans_status = trans_status;		
		this.reason_code = reason_code;
		this.UMBInterfaces = UMBInterfaces;
		this.msisdn = msisdn;
		this.max_end_date = max_end_date;			
		this.max_duration = max_duration;		
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}
	
	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}
	
	public String getShort_code() {
		return short_code;
	}

	public void setShort_code(String short_code) {
		this.short_code = short_code;
	}
	
	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}
	
	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	
	public String getMenu_name() {
		return menu_name;
	}

	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getReason_code() {
		return reason_code;
	}

	public void setReason_code(String reason_code) {
		this.reason_code = reason_code;
	}
	
	public String getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String trans_status) {
		this.trans_status = trans_status;
	}	

    public List<UMBInterface_Ext> getUMBInterfaces() {
        return UMBInterfaces;
    }

    public void setUMBInterfaces(List<UMBInterface_Ext> UMBInterfaces) {
        this.UMBInterfaces = UMBInterfaces;
    }
    
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}	    

	public String getMax_end_date() {
		return max_end_date;
	}

	public void setMax_end_date(String max_end_date) {
		this.max_end_date = max_end_date;
	}
	
	public long getMax_duration() {
		return max_duration;
	}

	public void setMax_duration(long max_duration) {
		this.max_duration = max_duration;
	}
}
