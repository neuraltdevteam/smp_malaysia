package com.neuralt.blc.trace.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "VIEW_SESSION_EXT")
public class Session_Ext implements Serializable {

	private static final long serialVersionUID = -7568094849996635658L;

	@Id
	@GeneratedValue	
	private Long id;
	
	private String host_id;
	private String session_id;
	private String msisdn;
	private String short_code;
	private String start_date;
	private String end_date;
	private long duration;	
	private String sub_grp_id;	
	public List<Menu_Ext> menus = new ArrayList<Menu_Ext>();
	public List<UMBInterface_Ext> UMBInterfaces = new ArrayList<UMBInterface_Ext>();
	
	public Session_Ext() {
	}

	
	public Session_Ext(String session_id, String host_id, String short_code, String start_date, String end_date, long duration, String sub_grp_id, String msisdn, List<Menu_Ext> menus, List<UMBInterface_Ext> UMBInterfaces)
	{
		this.session_id = session_id;
		this.host_id = host_id;	
		this.short_code = short_code;	
		this.start_date = start_date;
		this.end_date = end_date;			
		this.duration = duration;
		this.sub_grp_id = sub_grp_id;
		this.msisdn = msisdn;
		this.menus = menus;
		this.UMBInterfaces = UMBInterfaces;		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	
	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getSub_grp_id() {
		return sub_grp_id;
	}

	public void setSub_grp_id(String sub_grp_id) {
		this.sub_grp_id = sub_grp_id;
	}
	
	public String getShort_code() {
		return short_code;
	}

	public void setShort_code(String short_code) {
		this.short_code = short_code;
	}	
	
    public List<Menu_Ext> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu_Ext> menus) {
        this.menus = menus;
    }
 
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}	

    public List<UMBInterface_Ext> getUMBInterfaces() {
        return UMBInterfaces;
    }

    public void setUMBInterfaces(List<UMBInterface_Ext> UMBInterfaces) {
        this.UMBInterfaces = UMBInterfaces;
    }
}
