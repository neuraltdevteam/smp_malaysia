package com.neuralt.blc.trace.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BLC_EXT_INTERFACE_EXT")
public class BLCInterface_Ext implements Serializable {

	private static final long serialVersionUID = 7944227603875826064L;

	@Id
	@GeneratedValue
	private Long id;
	
	private String host_id;
	private String short_code;
	private String channel;
	private String msisdn;
	private String out_host_id;
	private String out_channel;
	private String session_id;	
	private String transaction_id;
	private String start_date;
	private String end_date;
	private long duration;	
	private String api_name;	
	private String trans_status;
	private String reason_code;	
	public List<SMSOUT> SMSOUT = new ArrayList<SMSOUT>();
	
	public BLCInterface_Ext() {
	}
	
	public BLCInterface_Ext(String session_id, String host_id, String short_code, String channel, String transaction_id, String api_name, String start_date, String end_date, long duration, String trans_status, String reason_code, String out_host_id,String out_channel, String msisdn, List<SMSOUT> SMSOUT)
	{
		this.session_id = session_id;
		this.host_id = host_id;	
		this.short_code = short_code;	
		this.transaction_id = transaction_id;
		this.api_name = api_name;		
		this.start_date = start_date;
		this.end_date = end_date;			
		this.duration = duration;
		this.trans_status = trans_status;		
		this.reason_code = reason_code;	
		this.out_host_id = out_host_id;	
		this.out_channel = out_channel;
		this.msisdn = msisdn;
		this.channel = channel;
		this.SMSOUT = SMSOUT;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}
	
	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}
	
	public String getShort_code() {
		return short_code;
	}

	public void setShort_code(String short_code) {
		this.short_code = short_code;
	}
		
	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	
	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	
	public String getApi_name() {
		return api_name;
	}

	public void setApi_name(String api_name) {
		this.api_name = api_name;
	}
		
	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getReason_code() {
		return reason_code;
	}

	public void setReason_code(String reason_code) {
		this.reason_code = reason_code;
	}
	
	public String getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String trans_status) {
		this.trans_status = trans_status;
	}	

	public String getOut_host_id() {
		return out_host_id;
	}

	public void setOut_host_id(String out_host_id) {
		this.out_host_id = out_host_id;
	}
	
	public String getOut_channel() {
		return out_channel;
	}

	public void setOut_channel(String out_channel) {
		this.out_channel = out_channel;
	}	

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	
    public List<SMSOUT> getSMSOUT() {
        return SMSOUT;
    }

    public void setSMSOUT(List<SMSOUT> SMSOUT) {
        this.SMSOUT = SMSOUT;
    }	
}
