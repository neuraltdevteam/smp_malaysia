package com.neuralt.blc.trace.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SMS_IN_Ext")
public class SMSIN_Ext implements Serializable {

	private static final long serialVersionUID = 3432926265357010085L;

	@Id
	@GeneratedValue
	private Long id;
	
	private String host_id;
	private String msisdn;	
	private String session_id;		
	private String transaction_id;
	private String start_date;
	private String end_date;
	private long duration;	
	private String dest_addr;
	private String received_content;		
	private String trans_status;
	private String reason_code;
	private String in_sms_channel;
	public List<UMBInterface_Ext> UMBInterfaces = new ArrayList<UMBInterface_Ext>();
	public List<SMSOUT> SMSOUT = new ArrayList<SMSOUT>();
	
	public SMSIN_Ext() {
	}
	
	public SMSIN_Ext(String session_id, String host_id, String dest_addr,String in_sms_channel, String transaction_id, String received_content,  String start_date, String end_date, long duration, String trans_status, String reason_code, String msisdn, List<UMBInterface_Ext> UMBInterfaces, List<SMSOUT> SMSOUT)
	{
		this.session_id = session_id;
		this.host_id = host_id;	
		this.dest_addr = dest_addr;			
		this.transaction_id = transaction_id;
		this.received_content = received_content;	
		this.start_date = start_date;
		this.end_date = end_date;			
		this.duration = duration;
		this.trans_status = trans_status;		
		this.reason_code = reason_code;
		this.UMBInterfaces = UMBInterfaces;
		this.msisdn = msisdn;
		this.in_sms_channel = in_sms_channel;
		this.SMSOUT = SMSOUT;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}
	
	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}
	
	public String getDest_addr() {
		return dest_addr;
	}

	public void setDest_addr(String dest_addr) {
		this.dest_addr = dest_addr;
	}
	
	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	
	public String getReceived_content() {
		return received_content;
	}

	public void setReceived_content(String received_content) {
		this.received_content = received_content;
	}

	public long getDuration() {
		return duration;
	}

	
	public void setDuration(String duration) {
		
		if (duration != null)
			this.duration = Long.parseLong(duration);
		else
			this.duration = 0;
	}

	/*
	public void setDuration(long duration) {
		this.duration = duration;
	}
	*/
	
	public String getReason_code() {
		return reason_code;
	}

	public void setReason_code(String reason_code) {
		this.reason_code = reason_code;
	}
	
	public String getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String trans_status) {
		this.trans_status = trans_status;
	}	
	
    public List<UMBInterface_Ext> getUMBInterfaces() {
        return UMBInterfaces;
    }

    public void setUMBInterfaces(List<UMBInterface_Ext> UMBInterfaces) {
        this.UMBInterfaces = UMBInterfaces;
    }

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}	
	
	public String getIn_sms_channel() {
		return in_sms_channel;
	}

	public void setIn_sms_channel(String in_sms_channel) {
		this.in_sms_channel = in_sms_channel;
	}

    public List<SMSOUT> getSMSOUT() {
        return SMSOUT;
    }

    public void setSMSOUT(List<SMSOUT> SMSOUT) {
        this.SMSOUT = SMSOUT;
    }	
}
