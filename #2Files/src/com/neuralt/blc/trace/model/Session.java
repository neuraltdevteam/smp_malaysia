package com.neuralt.blc.trace.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UMB_SESS")
public class Session implements Serializable {

	private static final long serialVersionUID = -999016420452462063L;
	
	@Id
	@GeneratedValue	
	private Long id;
	
	private String host_id;
	private String short_code;
	private String msisdn;
	private String session_id;
	private String start_date;
	private String end_date;
	private long duration;	
	private String sub_grp_id;	
	//public List<Menu> menus = new ArrayList<Menu>();
	
	public Session() {
	}

	
	public Session(String session_id, String host_id, String short_code, String start_date, String end_date, long duration, String sub_grp_id, String msisdn) //, List<Menu> menus)
	{
		this.session_id = session_id;
		this.host_id = host_id;	
		this.short_code = short_code;	
		this.start_date = start_date;
		this.end_date = end_date;			
		this.duration = duration;
		this.sub_grp_id = sub_grp_id;
		this.msisdn = msisdn;
		//this.menus = menus;	
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	
	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getSub_grp_id() {
		return sub_grp_id;
	}

	public void setSub_grp_id(String sub_grp_id) {
		this.sub_grp_id = sub_grp_id;
	}
	
	public String getShort_code() {
		return short_code;
	}

	public void setShort_code(String short_code) {
		this.short_code = short_code;
	}	
	
    /*
    public List<Menu> getMenus() {    
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }
 	*/
	
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}	
	
}
