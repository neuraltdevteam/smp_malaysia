package com.neuralt.blc.trace.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "AUTO_RENEW_DELETIONS")
public class AutoRenewDeletions implements Serializable {

	private static final long serialVersionUID = 2365406854237055726L;
	
	@Id
	@GeneratedValue	
	private Long delete_id;

	private String delete_date;
	private String delete_cause;
	private String id;
	private String msisdn;
	private String bundle_sncode;
	private String bundle_name;
	private String purchase_date;
	private String expiry_date;
	private String next_action_date;
	private String auto_renew;	
	private String renewal_state;
	private String processing_state;		
	private String processing_owner;	
	private String status_code;
	private String free_count;
	private String active_flag;
	private String bundle_type;	
	private String subscription_date;	
	private String temp_sncode_in_use;
	private String freebies_idx;	
	private String last_update_date;
	private String source;	
	private String last_action;
	private String active_bundle;
	
	public AutoRenewDeletions() {
	}
	
	public AutoRenewDeletions(String delete_date, String delete_cause, String id, String bundle_sncode, String bundle_name, String purchase_date, String expiry_date, String next_action_date, String auto_renew, String renewal_state, String processing_state, String processing_owner, String status_code, String free_count, String msisdn, String active_flag, String bundle_type, String subscription_date, String temp_sncode_in_use, String freebies_idx, String last_update_date, String source, String last_action, String active_bundle)
	{
		this.delete_date = delete_date;
		this.delete_cause = delete_cause;	
		this.id = id;
		
		this.bundle_sncode = bundle_sncode;
		this.bundle_name = bundle_name;	
		this.purchase_date = purchase_date;			
		this.expiry_date = expiry_date;
		this.next_action_date = next_action_date;	
		this.auto_renew = auto_renew;	
		this.renewal_state = renewal_state;
		this.processing_state = processing_state;			
		this.processing_owner = processing_owner;
		this.status_code = status_code;		
		this.free_count = free_count;
		this.msisdn = msisdn;
		this.active_flag = active_flag;
		this.bundle_type = bundle_type;	
		this.subscription_date = subscription_date;			
		this.temp_sncode_in_use = temp_sncode_in_use;
		this.freebies_idx = freebies_idx;	
		this.last_update_date = last_update_date;	
		this.source = source;
		this.last_action = last_action;
		this.active_bundle = active_bundle;
	}
	
	public Long getDelete_id() {
		return delete_id;
	}

	public void setDelete_id(Long delete_id) {
		this.delete_id = delete_id;
	}
		
	
	public String getDelete_date() {
		return delete_date;
	}

	public void setDelete_date(String delete_date) {
		this.delete_date = delete_date;
	}
	
	public String getDelete_cause() {
		return delete_cause;
	}

	public void setDelete_cause(String delete_cause) {
		this.delete_cause = delete_cause;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	

	public String getBundle_sncode() {
		return bundle_sncode;
	}

	public void setBundle_sncode(String bundle_sncode) {
		this.bundle_sncode = bundle_sncode;
	}
	
	public String getBundle_name() {
		return bundle_name;
	}

	public void setBundle_name(String bundle_name) {
		this.bundle_name = bundle_name;
	}
	
	public String getPurchase_date() {
		return purchase_date;
	}

	public void setPurchase_date(String purchase_date) {
		this.purchase_date = purchase_date;
	}
	
	public String getExpiry_date() {
		return expiry_date;
	}

	public void setExpiry_date(String expiry_date) {
		this.expiry_date = expiry_date;
	}

	public String getNext_action_date() {
		return next_action_date;
	}

	public void setNext_action_date(String next_action_date) {
		this.next_action_date = next_action_date;
	}
	
	public String getRenewal_state() {
		return renewal_state;
	}

	public void setRenewal_state(String renewal_state) {
		this.renewal_state = renewal_state;
	}

	public String getProcessing_state() {
		return processing_state;
	}

	public void setProcessing_state(String processing_state) {
		this.processing_state = processing_state;
	}
	
	public String getAuto_renew() {
		return auto_renew;
	}

	public void setAuto_renew(String auto_renew) {
		this.auto_renew = auto_renew;
	}

	public String getProcessing_owner() {
		return processing_owner;
	}

	public void setProcessing_owner(String processing_owner) {
		this.processing_owner = processing_owner;
	}

	public String getFree_count() {
		return free_count;
	}

	public void setFree_count(String free_count) {
		this.free_count = free_count;
	}
	
	public String getStatus_code() {
		return status_code;
	}

	public void setStatus_code(String status_code) {
		this.status_code = status_code;
	}	
    
	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}	

	public String getActive_flag() {
		return active_flag;
	}

	public void setActive_flag(String active_flag) {
		this.active_flag = active_flag;
	}
	
	public String getBundle_type() {
		return bundle_type;
	}

	public void setBundle_type(String bundle_type) {
		this.bundle_type = bundle_type;
	}
	
	public String getSubscription_date() {
		return subscription_date;
	}

	public void setSubscription_date(String subscription_date) {
		this.subscription_date = subscription_date;
	}
	
	public String getTemp_sncode_in_use() {
		return temp_sncode_in_use;
	}

	public void setTemp_sncode_in_use(String temp_sncode_in_use) {
		this.temp_sncode_in_use = temp_sncode_in_use;
	}

	public String getFreebies_idx() {
		return freebies_idx;
	}

	public void setFreebies_idx(String freebies_idx) {
		this.freebies_idx = freebies_idx;
	}
	
	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getLast_action() {
		return last_action;
	}

	public void setLast_action(String last_action) {
		this.last_action = last_action;
	}
	
	public String getLast_update_date() {
		return last_update_date;
	}

	public void setLast_update_date(String last_update_date) {
		this.last_update_date = last_update_date;
	}

	public void setActive_bundle(String active_bundle) {
		this.active_bundle = active_bundle;
	}
	
	public String getActive_bundle() {
		return active_bundle;
	}
}
