package com.neuralt.blc.trace.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SMS_OUT")
public class SMSOUT implements Serializable {

	private static final long serialVersionUID = -4110667696104991531L;

	@Id
	@GeneratedValue
	private Long id;
	
	private String host_id;
	private String msisdn;
	private String session_id;	
	private String transaction_id;
	private String message_id;
	private String request_date;
	private String submission_date;	
	private String delivery_date;	
	private String sms_content;
	private String data_coding_scheme;
	private String dr_enable_flag;
	private String priority_flag;	
	private String request_status;
	private String req_reason_code;
	private String submission_status;
	private String submission_reason_code;
	private String delivery_status;
	private String delivery_reason_code;	
	
	public SMSOUT() {
	}
	
	public SMSOUT(String session_id, String host_id, String submission_status, String submission_reason_code, String transaction_id, String delivery_date, String sms_content, String data_coding_scheme, String message_id, String request_date, String submission_date, String dr_enable_flag, String priority_flag, String request_status,String req_reason_code, String msisdn, String delivery_status, String delivery_reason_code)
	{
		this.session_id = session_id;
		this.host_id = host_id;	
		this.submission_status = submission_status;	
		this.transaction_id = transaction_id;
		this.delivery_date = delivery_date;	
		this.sms_content = sms_content;	
		this.data_coding_scheme = data_coding_scheme;	
		this.message_id = message_id;
		this.request_date = request_date;			
		this.submission_date = submission_date;
		this.dr_enable_flag = dr_enable_flag;		
		this.priority_flag = priority_flag;	
		this.request_status = request_status;	
		this.req_reason_code = req_reason_code;
		this.msisdn = msisdn;
		this.submission_reason_code = submission_reason_code;
		this.delivery_status = delivery_status;		
		this.delivery_reason_code = delivery_reason_code;			
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getSession_id() {
		return session_id;
	}

	public void setSession_id(String session_id) {
		this.session_id = session_id;
	}
	
	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}
	
	public String getSubmission_status() {
		return submission_status;
	}

	public void setSubmission_status(String submission_status) {
		this.submission_status = submission_status;
	}
		
	public String getTransaction_id() {
		return transaction_id;
	}

	public void setTransaction_id(String transaction_id) {
		this.transaction_id = transaction_id;
	}

	public String getDelivery_date() {
		return delivery_date;
	}

	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}
	
	public String getMessage_id() {
		return message_id;
	}

	public void setMessage_id(String message_id) {
		this.message_id = message_id;
	}

	public String getRequest_date() {
		return request_date;
	}

	public void setRequest_date(String request_date) {
		this.request_date = request_date;
	}
	
	public String getSms_content() {
		return sms_content;
	}

	public void setSms_content(String sms_content) {
		this.sms_content = sms_content;
	}

	public String getData_coding_scheme() {
		return data_coding_scheme;
	}

	public void setData_coding_scheme(String data_coding_scheme) {
		this.data_coding_scheme = data_coding_scheme;
	}
	
	public String getSubmission_date() {
		return submission_date;
	}

	public void setSubmission_date(String submission_date) {
		this.submission_date = submission_date;
	}

	public String getPriority_flag() {
		return priority_flag;
	}

	public void setPriority_flag(String priority_flag) {
		this.priority_flag = priority_flag;
	}
	
	public String getDr_enable_flag() {
		return dr_enable_flag;
	}

	public void setDr_enable_flag(String dr_enable_flag) {
		this.dr_enable_flag = dr_enable_flag;
	}	

	public String getRequest_status() {
		return request_status;
	}

	public void setRequest_status(String request_status) {
		this.request_status = request_status;
	}
	
	public String getReq_reason_code() {
		return req_reason_code;
	}

	public void setReq_reason_code(String req_reason_code) {
		this.req_reason_code = req_reason_code;
	}	

	public String getMsisdn() {
		return msisdn;
	}

	public void setMsisdn(String msisdn) {
		this.msisdn = msisdn;
	}

	public String getSubmission_reason_code() {
		return submission_reason_code;
	}

	public void setSubmission_reason_code(String submission_reason_code) {
		this.submission_reason_code = submission_reason_code;
	}

	public String getDelivery_reason_code() {
		return delivery_reason_code;
	}

	public void setDelivery_reason_code(String delivery_reason_code) {
		this.delivery_reason_code = delivery_reason_code;
	}
	
	public String getDelivery_status() {
		return delivery_status;
	}

	public void setDelivery_status(String delivery_status) {
		this.delivery_status = delivery_status;
	}
	
}
