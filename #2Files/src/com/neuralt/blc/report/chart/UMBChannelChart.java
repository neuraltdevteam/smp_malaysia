package com.neuralt.blc.report.chart;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.UMBChannelController;

@ManagedBean(name = "UMBChannelChart")
@SessionScoped

public class UMBChannelChart {
	
	private CartesianChartModel model1, model2, model3;
	public boolean resultsVisible = false;
	
	public UMBChannelChart() {
		
	}

	public CartesianChartModel UpdateTotalTranChartData() {

		//System.out.println("in UpdateTotalTranChartData");	
		
		model1 = null;
		model1 = new CartesianChartModel();
				
		String[] chartdates1 = UMBChannelController.getChartdates();
		int[] totaltransdata = UMBChannelController.getTotaltransdata();	
		
		ChartSeries TotalTransChart = new ChartSeries();		
		TotalTransChart.setLabel("Total transaction");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateTotalTranChartData, chartdates1[" + i + "]" + chartdates1[i]);
				TotalTransChart.set(chartdates1[i],totaltransdata[i]);
			}				
		}
		else
			TotalTransChart.set("0",0 );
		
		model1.addSeries(TotalTransChart);
				
		resultsVisible = true;
		
		return model1;
	}
	
	
	
	public CartesianChartModel getModel1() {return model1; }
	
	public CartesianChartModel getModel2() { return model2; }
	
	public CartesianChartModel getModel3() { return model3; }

	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
	
}