package com.neuralt.blc.report.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.BLCExtInterfaceByReasonCodeController;

@ManagedBean(name = "BLCExtInterfaceByReasonCodeChart")
@SessionScoped
public class BLCExtInterfaceByReasonCodeChart {

	private CartesianChartModel model1, model2;
	public boolean resultsVisible = false;

	public BLCExtInterfaceByReasonCodeChart() {

	}

	public CartesianChartModel UpdateTotalTranChartData() {

		//System.out.println("in UpdateTotalTranChartData");	
		
		model1 = null;
		model1 = new CartesianChartModel();
		
		String[] reason_code = BLCExtInterfaceByReasonCodeController.getReason_code();
		String[] chartdates1 = BLCExtInterfaceByReasonCodeController.getChartdates();
		int[] totaltransdata = BLCExtInterfaceByReasonCodeController.getTotaltransdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (reason_code != null)
		{
			String prevReasonCode = "";
			
			int i = 0;
			int j = 0;
			int reasonCodeCount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < reason_code.length)
			//for (int i = 0; i < reason_code.length; i++)
			{
				String currentReasonCode = reason_code[i];
				if (currentReasonCode.compareTo(prevReasonCode) != 0)
				{
					if (reasonCodeCount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							//System.out.println("series=" + reasonCodeCount + ",chartdate=" +s + ",totaltransdata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model1.addSeries(TotalTransChart.get(reasonCodeCount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++reasonCodeCount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(reason_code[i]);			
					//System.out.println("series=" + reasonCodeCount + ",label=" +reason_code[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						//System.out.println("series=" + reasonCodeCount + ",chartdate=" +s + ",totaltransdata=0");
						s = itr.next();				    
					}

					TempTransChart.set(chartdates1[i],totaltransdata[i]);
					//System.out.println("series=" + reasonCodeCount + ",chartdate=" +chartdates1[i] + ",totaltransdata=" + totaltransdata[i]);
					
				}
				prevReasonCode = currentReasonCode;
				++i;
			}			

			if (reason_code.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						//System.out.println("series=" + reasonCodeCount + ",chartdate=" +s + ",totaltransdata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model1.addSeries(TotalTransChart.get(reasonCodeCount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model1.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model1;
	}
	


	public CartesianChartModel UpdateTranPctChartData() {

		// System.out.println("in UpdateTranPctChartData");

		model2 = null;
		model2 = new CartesianChartModel();

		String[] reason_code = BLCExtInterfaceByReasonCodeController
				.getReason_code();
		String[] chartdates1 = BLCExtInterfaceByReasonCodeController
				.getChartdates();
		float[] totalPct = BLCExtInterfaceByReasonCodeController.getPctdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr = uniqChartdates.iterator();

		if (reason_code != null) {
			String prevReason_code = "";

			int i = 0;
			int j = 0;
			int reasoncodeCount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;

			while (i < reason_code.length)
			// for (int i = 0; i < api.length; i++)
			{
				String currentReason_code = reason_code[i];
				if (currentReason_code.compareTo(prevReason_code) != 0) {
					if (reasoncodeCount > -1) {
						while (itr.hasNext()) {
							String s = itr.next();
							TempTransChart.set(s, 0);
							System.out.println("series=" + reasoncodeCount
									+ ",chartdate=" + s + ",totaltransdata=0");
						}

						TotalTransChart.add(TempTransChart);
						model2.addSeries(TotalTransChart.get(reasoncodeCount));
						TempTransChart = null;

						itr = null;
						itr = uniqChartdates.iterator();
					}

					++reasoncodeCount;
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(reason_code[i]);
					// System.out.println("series=" + apicount + ",label="
					// +api[i]);
					j = 0;
				}

				if (chartdates1 != null) {

					String s = "";
					if (itr.hasNext())
						s = itr.next();

					while (itr.hasNext() && s.compareTo(chartdates1[i]) != 0) {
						TempTransChart.set(s, 0);
						System.out.println("series=" + reasoncodeCount
								+ ",chartdate=" + s + ",totaltransdata=0");
						s = itr.next();
					}

					TempTransChart.set(chartdates1[i], totalPct[i]);
					System.out.println("series=" + reasoncodeCount
							+ ",chartdate=" + chartdates1[i]
							+ ",totaltransdata=" + totalPct[i]);

				}
				prevReason_code = currentReason_code;
				++i;
			}

			if (reason_code.length > 0) {
				if (TempTransChart != null) {
					while (itr.hasNext()) {
						String s = itr.next();
						TempTransChart.set(s, 0);
						System.out.println("series=" + reasoncodeCount
								+ ",chartdate=" + s + ",totaltransdata=0");
					}

					TotalTransChart.add(TempTransChart);
					model2.addSeries(TotalTransChart.get(reasoncodeCount));
					TempTransChart = null;
				}
			}

		} else {

			ChartSeries TotalTransChart = new ChartSeries();
			TotalTransChart.set("0", 0);
			model2.addSeries(TotalTransChart);
		}

		resultsVisible = true;
		return model2;
	}

	public CartesianChartModel getModel1() {
		return model1;
	}

	public CartesianChartModel getModel2() {
		return model2;
	}

	public boolean isResultsVisible() {
		return resultsVisible;
	}

}
