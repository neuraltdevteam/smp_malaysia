package com.neuralt.blc.report.chart;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.TransactionStatController;

@ManagedBean(name = "TransactionChart")
@SessionScoped

public class TransactionChart {
	
	private CartesianChartModel model1, model2, model3, model4, model5, model6;
	public boolean resultsVisible = false;
	
	public TransactionChart() {
		
	}

	public CartesianChartModel UpdateTotalTranChartData() {

		//System.out.println("in UpdateTotalTranChartData");	
		
		model1 = null;
		model1 = new CartesianChartModel();
				
		String[] chartdates1 = TransactionStatController.getChartdates();
		int[] totaltransdata = TransactionStatController.getTotaltransdata();
		int[] totalsuccessdata = TransactionStatController.getTotalsuccessdata();
		int[] totalfaildata = TransactionStatController.getTotalfaildata();		
		
		ChartSeries TotalTransChart = new ChartSeries();		
		TotalTransChart.setLabel("Total transaction");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateTotalTranChartData, chartdates1[" + i + "]" + chartdates1[i]);
				TotalTransChart.set(chartdates1[i],totaltransdata[i]);
			}				
		}
		else
			TotalTransChart.set("0",0 );
		
		model1.addSeries(TotalTransChart);
		
		ChartSeries TotalSuccessChart = new ChartSeries();		
		TotalSuccessChart.setLabel("Total Success transaction");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateTotalTranChartData, chartdates1[" + i + "]" + chartdates1[i]);
				TotalSuccessChart.set(chartdates1[i],totalsuccessdata[i]);
			}				
		}
		else
			TotalSuccessChart.set("0",0 );
		
		model1.addSeries(TotalSuccessChart);

		ChartSeries TotalFailChart = new ChartSeries();		
		TotalFailChart.setLabel("Total Fail transaction");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateTotalTranChartData, chartdates1[" + i + "]" + chartdates1[i]);
				TotalFailChart.set(chartdates1[i],totalfaildata[i]);
			}				
		}
		else
			TotalFailChart.set("0",0 );
		
		model1.addSeries(TotalFailChart);
		
		
		/*
		if (chartdates != null)
		{
			System.out.println("in SetTotalTranData5");
			for (int i = 0; i < chartdates.length; i++)
			{
				System.out.println("in SetTotalTranData6, chartdates[" + i + "]" + chartdates[i]);
				TotalTransChart.set(chartdates[i],totaltransdata[i] );
			}				
		}
		else
			TotalTransChart.set("0",0 );

		TotalTransChart.set("2015-05-01",10 );
		TotalTransChart.set("2015-05-02",35 );

		model1.addSeries(TotalTransChart);
		*/		

		
		resultsVisible = true;
		
		return model1;
	}
	
	
	public CartesianChartModel UpdateTranPctChartData() {

		//System.out.println("in UpdateTranPctChartData");	
		
		model2 = null;
		model2 = new CartesianChartModel();
				
		String[] chartdates1 = TransactionStatController.getChartdates();		
		float[] successpctdata = TransactionStatController.getSuccesspctdata();
		float[] failpctdata = TransactionStatController.getFailpctdata();		
				
		ChartSeries SuccessPctChart = new ChartSeries();		
		SuccessPctChart.setLabel("Success %");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateTranPctChartData, chartdates1[" + i + "]" + chartdates1[i]);
				SuccessPctChart.set(chartdates1[i],successpctdata[i]);
			}				
		}
		else
			SuccessPctChart.set("0",0 );
		
		model2.addSeries(SuccessPctChart);

		ChartSeries FailPctChart = new ChartSeries();		
		FailPctChart.setLabel("Fail %");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateTranPctChartData, chartdates1[" + i + "]" + chartdates1[i]);
				FailPctChart.set(chartdates1[i],failpctdata[i]);
			}				
		}
		else
			FailPctChart.set("0",0 );
		
		model2.addSeries(FailPctChart);
		
				
		resultsVisible = true;
		
		return model2;
	}
	
	
	public CartesianChartModel UpdateDurationChartData() {

		//System.out.println("in UpdateDurationChartData");	
		
		model3 = null;
		model3 = new CartesianChartModel();
				
		String[] chartdates1 = TransactionStatController.getChartdates();		
		int[] mindurdata = TransactionStatController.getMindurdata();
		int[] maxdurdata = TransactionStatController.getMaxdurdata();		
		float[] avgdurdata = TransactionStatController.getAvgdurdata();	
		
		ChartSeries MinDurChart = new ChartSeries();		
		MinDurChart.setLabel("Min duration");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateDurationChartData, chartdates1[" + i + "]" + chartdates1[i]);
				MinDurChart.set(chartdates1[i],mindurdata[i]);
			}				
		}
		else
			MinDurChart.set("0",0 );
		
		model3.addSeries(MinDurChart);
		
		ChartSeries MaxDurChart = new ChartSeries();		
		MaxDurChart.setLabel("Max duration");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateDurationChartData, chartdates1[" + i + "]" + chartdates1[i]);
				MaxDurChart.set(chartdates1[i],maxdurdata[i]);
			}				
		}
		else
			MaxDurChart.set("0",0 );
		
		model3.addSeries(MaxDurChart);

		ChartSeries AvgDurChart = new ChartSeries();		
		AvgDurChart.setLabel("Min duration");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateDurationChartData, chartdates1[" + i + "]" + chartdates1[i]);
				AvgDurChart.set(chartdates1[i],avgdurdata[i]);
			}				
		}
		else
			AvgDurChart.set("0",0 );
		
		model3.addSeries(AvgDurChart);
		
		
		resultsVisible = true;
		
		return model3;
	}
	
	public CartesianChartModel UpdateIQRDurationChartData() {

		//System.out.println("in UpdateDurationChartData");	
		
		model4 = null;
		model4 = new CartesianChartModel();
				
		String[] chartdates1 = TransactionStatController.getChartdates();		
		float[] iqrdurdata = TransactionStatController.getIqrdurdata();
		
		ChartSeries IQRDurChart = new ChartSeries();		
		IQRDurChart.setLabel("IQR duration");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateDurationChartData, chartdates1[" + i + "]" + chartdates1[i]);
				IQRDurChart.set(chartdates1[i],iqrdurdata[i]);
			}				
		}
		else
			IQRDurChart.set("0",0 );
		
		model4.addSeries(IQRDurChart);
			
		resultsVisible = true;
		
		return model4;
	}
	
	public CartesianChartModel getModel1() {
			return model1;		
	}
	
	public CartesianChartModel getModel2() { return model2; }
	
	public CartesianChartModel getModel3() { return model3; }

	public CartesianChartModel getModel4() { return model4; }
	
	public CartesianChartModel getModel5() { return model5; }
	
	public CartesianChartModel getModel6() { return model6; }
	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
	
}