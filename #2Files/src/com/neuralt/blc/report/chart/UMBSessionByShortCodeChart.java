package com.neuralt.blc.report.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.UMBSessionByShortCodeController;

@ManagedBean(name = "UMBSessionByShortCodeChart")
@SessionScoped

public class UMBSessionByShortCodeChart {
	
	private CartesianChartModel model1, model2, model3, model4, model5, model6;
	public boolean resultsVisible = false;
	
	public UMBSessionByShortCodeChart() {
		
	}

	public CartesianChartModel UpdateTotalTranChartData() {

		//System.out.println("in UpdateTotalTranChartData");	
		
		model1 = null;
		model1 = new CartesianChartModel();
		
		String[] shortcode = UMBSessionByShortCodeController.getShortcode();
		String[] chartdates1 = UMBSessionByShortCodeController.getChartdates();
		int[] totaltransdata = UMBSessionByShortCodeController.getTotaltransdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (shortcode != null)
		{
			String prevShortCode = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int shortcodecount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < shortcode.length)
			//for (int i = 0; i < shortcode.length; i++)
			{
				String currentShortCode = shortcode[i];
				if (currentShortCode.compareTo(prevShortCode) != 0)
				{
					if (shortcodecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",totaltransdata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model1.addSeries(TotalTransChart.get(shortcodecount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++shortcodecount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(shortcode[i]);			
					//System.out.println("series=" + shortcodecount + ",label=" +shortcode[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",totaltransdata=0");
						s = itr.next();				    
					}

					TempTransChart.set(chartdates1[i],totaltransdata[i]);
					//System.out.println("series=" + shortcodecount + ",chartdate=" +chartdates1[i] + ",totaltransdata=" + totaltransdata[i]);
					
				}
				prevShortCode = currentShortCode;
				++i;
			}			

			if (shortcode.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",totaltransdata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model1.addSeries(TotalTransChart.get(shortcodecount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model1.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model1;
	}
	
	
			
	
	public CartesianChartModel UpdateDurationChartData() {

		//System.out.println("in UpdateDurationChartData");	
		
		model4 = null;
		model4 = new CartesianChartModel();
		
		String[] shortcode = UMBSessionByShortCodeController.getShortcode();
		String[] chartdates1 = UMBSessionByShortCodeController.getChartdates();
		float[] avgdurdata = UMBSessionByShortCodeController.getAvgdurdata();			

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (shortcode != null)
		{
			String prevShortCode = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int shortcodecount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;
            
			while (i < shortcode.length)
			//for (int i = 0; i < shortcode.length; i++)
			{
				String currentShortCode = shortcode[i];
				if (currentShortCode.compareTo(prevShortCode) != 0)
				{
					if (shortcodecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempDurChart.set(s,0);
							//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",avgdurdata=0");											    
						}
						
						DurChart.add(TempDurChart);						
						model4.addSeries(DurChart.get(shortcodecount));
						TempDurChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++shortcodecount;					
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(shortcode[i]);			
					//System.out.println("series=" + shortcodecount + ",label=" +shortcode[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempDurChart.set(s,0);
						//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",avgdurdata=0");
						s = itr.next();				    
					}

					TempDurChart.set(chartdates1[i],avgdurdata[i]);
					//System.out.println("series=" + shortcodecount + ",chartdate=" +chartdates1[i] + ",avgdurdata=" + avgdurdata[i]);
					
				}
				prevShortCode = currentShortCode;
				++i;
			}			

			if (shortcode.length > 0)
			{			
				if (TempDurChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempDurChart.set(s,0);
						//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",avgdurdata=0");											    
					}
					
					DurChart.add(TempDurChart);						
					model4.addSeries(DurChart.get(shortcodecount));
					TempDurChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries DurChart= new ChartSeries();
			DurChart.set("0",0 );
			model4.addSeries(DurChart);
		}

				
		resultsVisible = true;
		
		return model4;
	}
	
	
	public CartesianChartModel UpdateDurationChartDataMin() {

		//System.out.println("in UpdateDurationChartDataMin");	
		
		model5 = null;
		model5 = new CartesianChartModel();
		
		String[] shortcode = UMBSessionByShortCodeController.getShortcode();
		String[] chartdates1 = UMBSessionByShortCodeController.getChartdates();
		int[] mindurdata = UMBSessionByShortCodeController.getMindurdata();		

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (shortcode != null)
		{
			String prevShortCode = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int shortcodecount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;
            
			while (i < shortcode.length)
			//for (int i = 0; i < shortcode.length; i++)
			{
				String currentShortCode = shortcode[i];
				if (currentShortCode.compareTo(prevShortCode) != 0)
				{
					if (shortcodecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempDurChart.set(s,0);
							//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",mindurdata=0");											    
						}
						
						DurChart.add(TempDurChart);						
						model5.addSeries(DurChart.get(shortcodecount));
						TempDurChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++shortcodecount;					
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(shortcode[i]);			
					//System.out.println("series=" + shortcodecount + ",label=" +shortcode[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempDurChart.set(s,0);
						//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",mindurdata=0");
						s = itr.next();				    
					}

					TempDurChart.set(chartdates1[i],mindurdata[i]);
					//System.out.println("series=" + shortcodecount + ",chartdate=" +chartdates1[i] + ",mindurdata=" + mindurdata[i]);
					
				}
				prevShortCode = currentShortCode;
				++i;
			}			

			if (shortcode.length > 0)
			{			
				if (TempDurChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempDurChart.set(s,0);
						//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",mindurdata=0");											    
					}
					
					DurChart.add(TempDurChart);						
					model5.addSeries(DurChart.get(shortcodecount));
					TempDurChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries DurChart= new ChartSeries();
			DurChart.set("0",0 );
			model5.addSeries(DurChart);
		}

				
		resultsVisible = true;
		
		return model5;
	}
	
	public CartesianChartModel UpdateDurationChartDataMax() {

		//System.out.println("in UpdateDurationChartDataMax");	
		
		model6 = null;
		model6 = new CartesianChartModel();
		
		String[] shortcode = UMBSessionByShortCodeController.getShortcode();
		String[] chartdates1 = UMBSessionByShortCodeController.getChartdates();
		int[] maxdurdata = UMBSessionByShortCodeController.getMaxdurdata();			

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (shortcode != null)
		{
			String prevShortCode = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int shortcodecount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;
            
			while (i < shortcode.length)
			//for (int i = 0; i < shortcode.length; i++)
			{
				String currentShortCode = shortcode[i];
				if (currentShortCode.compareTo(prevShortCode) != 0)
				{
					if (shortcodecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempDurChart.set(s,0);
							//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",maxdurdata=0");											    
						}
						
						DurChart.add(TempDurChart);						
						model6.addSeries(DurChart.get(shortcodecount));
						TempDurChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++shortcodecount;					
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(shortcode[i]);			
					//System.out.println("series=" + shortcodecount + ",label=" +shortcode[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempDurChart.set(s,0);
						//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",maxdurdata=0");
						s = itr.next();				    
					}

					TempDurChart.set(chartdates1[i],maxdurdata[i]);
					//System.out.println("series=" + shortcodecount + ",chartdate=" +chartdates1[i] + ",maxdurdata=" + maxdurdata[i]);
					
				}
				prevShortCode = currentShortCode;
				++i;
			}			

			if (shortcode.length > 0)
			{			
				if (TempDurChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempDurChart.set(s,0);
						//System.out.println("series=" + shortcodecount + ",chartdate=" +s + ",maxdurdata=0");											    
					}
					
					DurChart.add(TempDurChart);						
					model6.addSeries(DurChart.get(shortcodecount));
					TempDurChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries DurChart= new ChartSeries();
			DurChart.set("0",0 );
			model6.addSeries(DurChart);
		}

				
		resultsVisible = true;
		
		return model6;
	}	
	
		
	public CartesianChartModel getModel1() { return model1;	}
	
	public CartesianChartModel getModel2() { return model2; }
	
	public CartesianChartModel getModel3() { return model3; }

	public CartesianChartModel getModel4() { return model4; }
	
	public CartesianChartModel getModel5() { return model5; }
	
	public CartesianChartModel getModel6() { return model6; }
	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
	
}