package com.neuralt.blc.report.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.SmsOutBySmppIdController;

@ManagedBean(name = "SmsOutBySmppIdChart")
@SessionScoped

public class SmsOutBySmppIdChart {
	
	private CartesianChartModel model1, model2, model3, model4, model5, model6;
	public boolean resultsVisible = false;
	
	public SmsOutBySmppIdChart() {
		
	}

	public CartesianChartModel UpdateTotalTranChartData() {

		//System.out.println("in UpdateTotalTranChartData");	
		
		model1 = null;
		model1 = new CartesianChartModel();
		
		String[] mdr_prefix = SmsOutBySmppIdController.getMdrprefix();
		String[] chartdates1 = SmsOutBySmppIdController.getChartdates();
		int[] totaltransdata = SmsOutBySmppIdController.getTotaltransdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (mdr_prefix != null)
		{
			String prevMdrPrefix = "";
			
			int i = 0;
			int j = 0;
			int mdr_prefixcount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < mdr_prefix.length)
			//for (int i = 0; i < mdr_prefix.length; i++)
			{
				String currentMdrPrefix = mdr_prefix[i];
				if (currentMdrPrefix.compareTo(prevMdrPrefix) != 0)
				{
					if (mdr_prefixcount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							//System.out.println("series=" + mdr_prefixcount + ",chartdate=" +s + ",totaltransdata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model1.addSeries(TotalTransChart.get(mdr_prefixcount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++mdr_prefixcount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(mdr_prefix[i]);			
					//System.out.println("series=" + mdr_prefixcount + ",label=" +mdr_prefix[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						//System.out.println("series=" + mdr_prefixcount + ",chartdate=" +s + ",totaltransdata=0");
						s = itr.next();				    
					}

					TempTransChart.set(chartdates1[i],totaltransdata[i]);
					//System.out.println("series=" + mdr_prefixcount + ",chartdate=" +chartdates1[i] + ",totaltransdata=" + totaltransdata[i]);
					
				}
				prevMdrPrefix = currentMdrPrefix;
				++i;
			}			

			if (mdr_prefix.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						//System.out.println("series=" + mdr_prefixcount + ",chartdate=" +s + ",totaltransdata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model1.addSeries(TotalTransChart.get(mdr_prefixcount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model1.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model1;
	}
	
		
		
	public CartesianChartModel getModel1() { return model1;	}
	
	public CartesianChartModel getModel2() { return model2; }
	
	public CartesianChartModel getModel3() { return model3; }

	public CartesianChartModel getModel4() { return model4; }
	
	public CartesianChartModel getModel5() { return model5; }
	
	public CartesianChartModel getModel6() { return model6; }
	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
	
}