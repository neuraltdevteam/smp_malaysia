package com.neuralt.blc.report.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.UMBChannelByChannelController;

@ManagedBean(name = "UMBChannelByChannelChart")
@SessionScoped

public class UMBChannelByChannelChart {
	
	private CartesianChartModel model1, model2, model3, model4, model5, model6;
	public boolean resultsVisible = false;
	
	public UMBChannelByChannelChart() {
		
	}

	public CartesianChartModel UpdateTotalTranChartData() {

		//System.out.println("in UpdateTotalTranChartData");	
		
		model1 = null;
		model1 = new CartesianChartModel();
		
		String[] channelname = UMBChannelByChannelController.getChannelname();
		String[] chartdates1 = UMBChannelByChannelController.getChartdates();
		int[] totaltransdata = UMBChannelByChannelController.getTotaltransdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (channelname != null)
		{
			String prevChannelName = "";
			
			int i = 0;
			int j = 0;
			int channelnamecount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < channelname.length)
			//for (int i = 0; i < channelname.length; i++)
			{
				String currentChannelName = channelname[i];
				if (currentChannelName.compareTo(prevChannelName) != 0)
				{
					if (channelnamecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							//System.out.println("series=" + channelnamecount + ",chartdate=" +s + ",totaltransdata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model1.addSeries(TotalTransChart.get(channelnamecount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++channelnamecount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(channelname[i]);			
					//System.out.println("series=" + channelnamecount + ",label=" +channelname[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						//System.out.println("series=" + channelnamecount + ",chartdate=" +s + ",totaltransdata=0");
						s = itr.next();				    
					}

					TempTransChart.set(chartdates1[i],totaltransdata[i]);
					//System.out.println("series=" + channelnamecount + ",chartdate=" +chartdates1[i] + ",totaltransdata=" + totaltransdata[i]);
					
				}
				prevChannelName = currentChannelName;
				++i;
			}			

			if (channelname.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						//System.out.println("series=" + channelnamecount + ",chartdate=" +s + ",totaltransdata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model1.addSeries(TotalTransChart.get(channelnamecount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model1.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model1;
	}
	
	
			
	
	public CartesianChartModel UpdatePercentageChartData() {

		//System.out.println("in UpdatePercentageChartData");	
		
		model4 = null;
		model4 = new CartesianChartModel();
		
		String[] channelname = UMBChannelByChannelController.getChannelname();
		String[] chartdates1 = UMBChannelByChannelController.getChartdates();
		float[] pctdata = UMBChannelByChannelController.getPctdata();			

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (channelname != null)
		{
			String prevChannelName = "";
			
			int i = 0;
			int j = 0;
			int channelnamecount = -1;
			List<ChartSeries> PctChart = new ArrayList<ChartSeries>();
			ChartSeries TempPctChart = null;
            
			while (i < channelname.length)
			//for (int i = 0; i < channelname.length; i++)
			{
				String currentChannelName = channelname[i];
				if (currentChannelName.compareTo(prevChannelName) != 0)
				{
					if (channelnamecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempPctChart.set(s,0);
							//System.out.println("series=" + channelnamecount + ",chartdate=" +s + ",pctdata=0");											    
						}
						
						PctChart.add(TempPctChart);						
						model4.addSeries(PctChart.get(channelnamecount));
						TempPctChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++channelnamecount;					
					TempPctChart = new ChartSeries();
					TempPctChart.setLabel(channelname[i]);			
					//System.out.println("series=" + channelnamecount + ",label=" +channelname[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempPctChart.set(s,0);
						//System.out.println("series=" + channelnamecount + ",chartdate=" +s + ",pctdata=0");
						s = itr.next();				    
					}

					TempPctChart.set(chartdates1[i],pctdata[i]);
					//System.out.println("series=" + channelnamecount + ",chartdate=" +chartdates1[i] + ",pctdata=" + pctdata[i]);
					
				}
				prevChannelName = currentChannelName;
				++i;
			}			

			if (channelname.length > 0)
			{			
				if (TempPctChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempPctChart.set(s,0);
						//System.out.println("series=" + channelnamecount + ",chartdate=" +s + ",pctdata=0");											    
					}
					
					PctChart.add(TempPctChart);						
					model4.addSeries(PctChart.get(channelnamecount));
					TempPctChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries PctChart= new ChartSeries();
			PctChart.set("0",0 );
			model4.addSeries(PctChart);
		}

				
		resultsVisible = true;
		
		return model4;
	}
		
		
	public CartesianChartModel getModel1() { return model1;	}
	
	public CartesianChartModel getModel2() { return model2; }
	
	public CartesianChartModel getModel3() { return model3; }

	public CartesianChartModel getModel4() { return model4; }
	
	public CartesianChartModel getModel5() { return model5; }
	
	public CartesianChartModel getModel6() { return model6; }
	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
	
}