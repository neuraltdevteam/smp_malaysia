package com.neuralt.blc.report.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.BLCExtInterfaceByApiNameController;

@ManagedBean(name = "BLCExtInterfaceAPIChart")
@SessionScoped
public class BLCExtInterfaceAPIChart {

	private CartesianChartModel model1, model2, model3, model4, model5, model6;
	public boolean resultsVisible = false;

	public BLCExtInterfaceAPIChart() {

	}

	public CartesianChartModel UpdateTotalTranChartData() {

		// System.out.println("in UpdateTotalTranChartData");

		model1 = null;
		model1 = new CartesianChartModel();

		String[] api = BLCExtInterfaceByApiNameController.getApi_name();
		String[] chartdates1 = BLCExtInterfaceByApiNameController
				.getChartdates();
		int[] totaltransdata = BLCExtInterfaceByApiNameController
				.getTotaltransdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr = uniqChartdates.iterator();

		if (api != null) {
			String prevAPI = "";

			int i = 0;
			int j = 0;
			int apicount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;

			while (i < api.length)
			// for (int i = 0; i < api.length; i++)
			{
				String currentAPI = api[i];
				if (currentAPI.compareTo(prevAPI) != 0) {
					if (apicount > -1) {
						while (itr.hasNext()) {
							String s = itr.next();
							TempTransChart.set(s, 0);
							System.out.println("series=" + apicount
									+ ",chartdate=" + s + ",totaltransdata=0");
						}

						TotalTransChart.add(TempTransChart);
						model1.addSeries(TotalTransChart.get(apicount));
						TempTransChart = null;

						itr = null;
						itr = uniqChartdates.iterator();
					}

					++apicount;
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(api[i]);
					// System.out.println("series=" + apicount + ",label="
					// +api[i]);
					j = 0;
				}

				if (chartdates1 != null) {

					String s = "";
					if (itr.hasNext())
						s = itr.next();

					while (itr.hasNext() && s.compareTo(chartdates1[i]) != 0) {
						TempTransChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",totaltransdata=0");
						s = itr.next();
					}

					TempTransChart.set(chartdates1[i], totaltransdata[i]);
					System.out.println("series=" + apicount + ",chartdate="
							+ chartdates1[i] + ",totaltransdata="
							+ totaltransdata[i]);

				}
				prevAPI = currentAPI;
				++i;
			}

			if (api.length > 0) {
				if (TempTransChart != null) {
					while (itr.hasNext()) {
						String s = itr.next();
						TempTransChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",totaltransdata=0");
					}

					TotalTransChart.add(TempTransChart);
					model1.addSeries(TotalTransChart.get(apicount));
					TempTransChart = null;
				}
			}

		} else {

			ChartSeries TotalTransChart = new ChartSeries();
			TotalTransChart.set("0", 0);
			model1.addSeries(TotalTransChart);
		}

		resultsVisible = true;

		return model1;
	}

	public CartesianChartModel UpdateTotalTranChartDataSuccess() {

		// System.out.println("in UpdateTotalTranChartDataSuccess");

		model2 = null;
		model2 = new CartesianChartModel();

		String[] api = BLCExtInterfaceByApiNameController.getApi_name();
		String[] chartdates1 = BLCExtInterfaceByApiNameController
				.getChartdates();
		int[] totalsuccessdata = BLCExtInterfaceByApiNameController
				.getTotalsuccessdata();
		int[] totalfaildata = BLCExtInterfaceByApiNameController
				.getTotalfaildata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr = uniqChartdates.iterator();

		if (api != null) {
			String prevAPI = "";

			int i = 0;
			int j = 0;
			int apicount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;

			while (i < api.length)
			// for (int i = 0; i < api.length; i++)
			{
				String currentAPI = api[i];
				if (currentAPI.compareTo(prevAPI) != 0) {
					if (apicount > -1) {
						while (itr.hasNext()) {
							String s = itr.next();
							TempTransChart.set(s, 0);
							System.out
									.println("series=" + apicount
											+ ",chartdate=" + s
											+ ",totalsuccessdata=0");
						}

						TotalTransChart.add(TempTransChart);
						model2.addSeries(TotalTransChart.get(apicount));
						TempTransChart = null;

						itr = null;
						itr = uniqChartdates.iterator();
					}

					++apicount;
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(api[i]);
					// System.out.println("series=" + apicount + ",label="
					// +api[i]);
					j = 0;
				}

				if (chartdates1 != null) {

					String s = "";
					if (itr.hasNext())
						s = itr.next();

					while (itr.hasNext() && s.compareTo(chartdates1[i]) != 0) {
						TempTransChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",totalsuccessdata=0");
						s = itr.next();
					}

					TempTransChart.set(chartdates1[i], totalsuccessdata[i]);
					System.out.println("series=" + apicount + ",chartdate="
							+ chartdates1[i] + ",totalsuccessdata="
							+ totalsuccessdata[i]);

				}
				prevAPI = currentAPI;
				++i;
			}

			if (api.length > 0) {
				if (TempTransChart != null) {
					while (itr.hasNext()) {
						String s = itr.next();
						TempTransChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",totalsuccessdata=0");
					}

					TotalTransChart.add(TempTransChart);
					model2.addSeries(TotalTransChart.get(apicount));
					TempTransChart = null;
				}
			}

		} else {

			ChartSeries TotalTransChart = new ChartSeries();
			TotalTransChart.set("0", 0);
			model2.addSeries(TotalTransChart);
		}

		resultsVisible = true;

		return model2;
	}

	public CartesianChartModel UpdateTotalTranChartDataFail() {

		// System.out.println("in UpdateTotalTranChartDataFail");

		model3 = null;
		model3 = new CartesianChartModel();

		String[] api = BLCExtInterfaceByApiNameController.getApi_name();
		String[] chartdates1 = BLCExtInterfaceByApiNameController
				.getChartdates();
		int[] totalfaildata = BLCExtInterfaceByApiNameController
				.getTotalfaildata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr = uniqChartdates.iterator();

		if (api != null) {
			String prevAPI = "";

			int i = 0;
			int j = 0;
			int apicount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;

			while (i < api.length)
			// for (int i = 0; i < api.length; i++)
			{
				String currentAPI = api[i];
				if (currentAPI.compareTo(prevAPI) != 0) {
					if (apicount > -1) {
						while (itr.hasNext()) {
							String s = itr.next();
							TempTransChart.set(s, 0);
							System.out.println("series=" + apicount
									+ ",chartdate=" + s + ",totalfaildata=0");
						}

						TotalTransChart.add(TempTransChart);
						model3.addSeries(TotalTransChart.get(apicount));
						TempTransChart = null;

						itr = null;
						itr = uniqChartdates.iterator();
					}

					++apicount;
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(api[i]);
					// System.out.println("series=" + apicount + ",label="
					// +api[i]);
					j = 0;
				}

				if (chartdates1 != null) {

					String s = "";
					if (itr.hasNext())
						s = itr.next();

					while (itr.hasNext() && s.compareTo(chartdates1[i]) != 0) {
						TempTransChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",totalfaildata=0");
						s = itr.next();
					}

					TempTransChart.set(chartdates1[i], totalfaildata[i]);
					System.out.println("series=" + apicount + ",chartdate="
							+ chartdates1[i] + ",totalfaildata="
							+ totalfaildata[i]);

				}
				prevAPI = currentAPI;
				++i;
			}

			if (api.length > 0) {
				if (TempTransChart != null) {
					while (itr.hasNext()) {
						String s = itr.next();
						TempTransChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",totalfaildata=0");
					}

					TotalTransChart.add(TempTransChart);
					model3.addSeries(TotalTransChart.get(apicount));
					TempTransChart = null;
				}
			}

		} else {

			ChartSeries TotalTransChart = new ChartSeries();
			TotalTransChart.set("0", 0);
			model3.addSeries(TotalTransChart);
		}

		resultsVisible = true;

		return model3;
	}

	public CartesianChartModel UpdateTranPctChartData() {

		// System.out.println("in UpdateTranPctChartData");

		model2 = null;
		model2 = new CartesianChartModel();

		String[] chartdates1 = BLCExtInterfaceByApiNameController
				.getChartdates();
		float[] successpctdata = BLCExtInterfaceByApiNameController
				.getSuccesspctdata();
		float[] failpctdata = BLCExtInterfaceByApiNameController
				.getFailpctdata();

		ChartSeries SuccessPctChart = new ChartSeries();
		SuccessPctChart.setLabel("Success %");

		if (chartdates1 != null) {
			for (int i = 0; i < chartdates1.length; i++) {
				// System.out.println("in UpdateTranPctChartData, chartdates1["
				// + i + "]" + chartdates1[i]);
				SuccessPctChart.set(chartdates1[i], successpctdata[i]);
			}
		} else
			SuccessPctChart.set("0", 0);

		model2.addSeries(SuccessPctChart);

		ChartSeries FailPctChart = new ChartSeries();
		FailPctChart.setLabel("Fail %");

		if (chartdates1 != null) {
			for (int i = 0; i < chartdates1.length; i++) {
				// System.out.println("in UpdateTranPctChartData, chartdates1["
				// + i + "]" + chartdates1[i]);
				FailPctChart.set(chartdates1[i], failpctdata[i]);
			}
		} else
			FailPctChart.set("0", 0);

		model2.addSeries(FailPctChart);

		resultsVisible = true;

		return model2;
	}

	public CartesianChartModel UpdateDurationChartData() {

		// System.out.println("in UpdateDurationChartData");

		model4 = null;
		model4 = new CartesianChartModel();

		String[] api = BLCExtInterfaceByApiNameController.getApi_name();
		String[] chartdates1 = BLCExtInterfaceByApiNameController
				.getChartdates();
		float[] avgdurdata = BLCExtInterfaceByApiNameController.getAvgdurdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr = uniqChartdates.iterator();

		if (api != null) {
			String prevAPI = "";

			int i = 0;
			int j = 0;
			int apicount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;

			while (i < api.length)
			// for (int i = 0; i < api.length; i++)
			{
				String currentAPI = api[i];
				if (currentAPI.compareTo(prevAPI) != 0) {
					if (apicount > -1) {
						while (itr.hasNext()) {
							String s = itr.next();
							TempDurChart.set(s, 0);
							System.out.println("series=" + apicount
									+ ",chartdate=" + s + ",avgdurdata=0");
						}

						DurChart.add(TempDurChart);
						model4.addSeries(DurChart.get(apicount));
						TempDurChart = null;

						itr = null;
						itr = uniqChartdates.iterator();
					}

					++apicount;
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(api[i]);
					// System.out.println("series=" + apicount + ",label="
					// +api[i]);
					j = 0;
				}

				if (chartdates1 != null) {

					String s = "";
					if (itr.hasNext())
						s = itr.next();

					while (itr.hasNext() && s.compareTo(chartdates1[i]) != 0) {
						TempDurChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",avgdurdata=0");
						s = itr.next();
					}

					TempDurChart.set(chartdates1[i], avgdurdata[i]);
					System.out.println("series=" + apicount + ",chartdate="
							+ chartdates1[i] + ",avgdurdata=" + avgdurdata[i]);

				}
				prevAPI = currentAPI;
				++i;
			}

			if (api.length > 0) {
				if (TempDurChart != null) {
					while (itr.hasNext()) {
						String s = itr.next();
						TempDurChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",avgdurdata=0");
					}

					DurChart.add(TempDurChart);
					model4.addSeries(DurChart.get(apicount));
					TempDurChart = null;
				}
			}

		} else {

			ChartSeries DurChart = new ChartSeries();
			DurChart.set("0", 0);
			model4.addSeries(DurChart);
		}

		resultsVisible = true;

		return model4;
	}

	public CartesianChartModel UpdateDurationChartDataMin() {

		// System.out.println("in UpdateDurationChartDataMin");

		model5 = null;
		model5 = new CartesianChartModel();

		String[] api = BLCExtInterfaceByApiNameController.getApi_name();
		String[] chartdates1 = BLCExtInterfaceByApiNameController
				.getChartdates();
		int[] mindurdata = BLCExtInterfaceByApiNameController.getMindurdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr = uniqChartdates.iterator();

		if (api != null) {
			String prevAPI = "";

			int i = 0;
			int j = 0;
			int apicount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;

			while (i < api.length)
			// for (int i = 0; i < api.length; i++)
			{
				String currentAPI = api[i];
				if (currentAPI.compareTo(prevAPI) != 0) {
					if (apicount > -1) {
						while (itr.hasNext()) {
							String s = itr.next();
							TempDurChart.set(s, 0);
							System.out.println("series=" + apicount
									+ ",chartdate=" + s + ",mindurdata=0");
						}

						DurChart.add(TempDurChart);
						model5.addSeries(DurChart.get(apicount));
						TempDurChart = null;

						itr = null;
						itr = uniqChartdates.iterator();
					}

					++apicount;
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(api[i]);
					// System.out.println("series=" + apicount + ",label="
					// +api[i]);
					j = 0;
				}

				if (chartdates1 != null) {

					String s = "";
					if (itr.hasNext())
						s = itr.next();

					while (itr.hasNext() && s.compareTo(chartdates1[i]) != 0) {
						TempDurChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",mindurdata=0");
						s = itr.next();
					}

					TempDurChart.set(chartdates1[i], mindurdata[i]);
					System.out.println("series=" + apicount + ",chartdate="
							+ chartdates1[i] + ",mindurdata=" + mindurdata[i]);

				}
				prevAPI = currentAPI;
				++i;
			}

			if (api.length > 0) {
				if (TempDurChart != null) {
					while (itr.hasNext()) {
						String s = itr.next();
						TempDurChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",mindurdata=0");
					}

					DurChart.add(TempDurChart);
					model5.addSeries(DurChart.get(apicount));
					TempDurChart = null;
				}
			}

		} else {

			ChartSeries DurChart = new ChartSeries();
			DurChart.set("0", 0);
			model5.addSeries(DurChart);
		}

		resultsVisible = true;

		return model5;
	}

	public CartesianChartModel UpdateDurationChartDataMax() {

		// System.out.println("in UpdateDurationChartDataMax");

		model6 = null;
		model6 = new CartesianChartModel();

		String[] api = BLCExtInterfaceByApiNameController.getApi_name();
		String[] chartdates1 = BLCExtInterfaceByApiNameController
				.getChartdates();
		int[] maxdurdata = BLCExtInterfaceByApiNameController.getMaxdurdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr = uniqChartdates.iterator();

		if (api != null) {
			String prevAPI = "";

			int i = 0;
			int j = 0;
			int apicount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;

			while (i < api.length)
			// for (int i = 0; i < api.length; i++)
			{
				String currentAPI = api[i];
				if (currentAPI.compareTo(prevAPI) != 0) {
					if (apicount > -1) {
						while (itr.hasNext()) {
							String s = itr.next();
							TempDurChart.set(s, 0);
							System.out.println("series=" + apicount
									+ ",chartdate=" + s + ",maxdurdata=0");
						}

						DurChart.add(TempDurChart);
						model6.addSeries(DurChart.get(apicount));
						TempDurChart = null;

						itr = null;
						itr = uniqChartdates.iterator();
					}

					++apicount;
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(api[i]);
					// System.out.println("series=" + apicount + ",label="
					// +api[i]);
					j = 0;
				}

				if (chartdates1 != null) {

					String s = "";
					if (itr.hasNext())
						s = itr.next();

					while (itr.hasNext() && s.compareTo(chartdates1[i]) != 0) {
						TempDurChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",maxdurdata=0");
						s = itr.next();
					}

					TempDurChart.set(chartdates1[i], maxdurdata[i]);
					System.out.println("series=" + apicount + ",chartdate="
							+ chartdates1[i] + ",maxdurdata=" + maxdurdata[i]);

				}
				prevAPI = currentAPI;
				++i;
			}

			if (api.length > 0) {
				if (TempDurChart != null) {
					while (itr.hasNext()) {
						String s = itr.next();
						TempDurChart.set(s, 0);
						System.out.println("series=" + apicount + ",chartdate="
								+ s + ",maxdurdata=0");
					}

					DurChart.add(TempDurChart);
					model6.addSeries(DurChart.get(apicount));
					TempDurChart = null;
				}
			}

		} else {

			ChartSeries DurChart = new ChartSeries();
			DurChart.set("0", 0);
			model6.addSeries(DurChart);
		}

		resultsVisible = true;

		return model6;
	}

	public CartesianChartModel UpdateDurationChartData1() {

		// System.out.println("in UpdateDurationChartData");

		model3 = null;
		model3 = new CartesianChartModel();

		String[] chartdates1 = BLCExtInterfaceByApiNameController
				.getChartdates();
		int[] mindurdata = BLCExtInterfaceByApiNameController.getMindurdata();
		int[] maxdurdata = BLCExtInterfaceByApiNameController.getMaxdurdata();
		float[] avgdurdata = BLCExtInterfaceByApiNameController.getAvgdurdata();

		ChartSeries MinDurChart = new ChartSeries();
		MinDurChart.setLabel("Min duration");

		if (chartdates1 != null) {
			for (int i = 0; i < chartdates1.length; i++) {
				// System.out.println("in UpdateDurationChartData, chartdates1["
				// + i + "]" + chartdates1[i]);
				MinDurChart.set(chartdates1[i], mindurdata[i]);
			}
		} else
			MinDurChart.set("0", 0);

		model3.addSeries(MinDurChart);

		ChartSeries MaxDurChart = new ChartSeries();
		MaxDurChart.setLabel("Max duration");

		if (chartdates1 != null) {
			for (int i = 0; i < chartdates1.length; i++) {
				// System.out.println("in UpdateDurationChartData, chartdates1["
				// + i + "]" + chartdates1[i]);
				MaxDurChart.set(chartdates1[i], maxdurdata[i]);
			}
		} else
			MaxDurChart.set("0", 0);

		model3.addSeries(MaxDurChart);

		ChartSeries AvgDurChart = new ChartSeries();
		AvgDurChart.setLabel("Min duration");

		if (chartdates1 != null) {
			for (int i = 0; i < chartdates1.length; i++) {
				// System.out.println("in UpdateDurationChartData, chartdates1["
				// + i + "]" + chartdates1[i]);
				AvgDurChart.set(chartdates1[i], avgdurdata[i]);
			}
		} else
			AvgDurChart.set("0", 0);

		model3.addSeries(AvgDurChart);

		resultsVisible = true;

		return model3;
	}

	public CartesianChartModel getModel1() {
		return model1;
	}

	public CartesianChartModel getModel2() {
		return model2;
	}

	public CartesianChartModel getModel3() {
		return model3;
	}

	public CartesianChartModel getModel4() {
		return model4;
	}

	public CartesianChartModel getModel5() {
		return model5;
	}

	public CartesianChartModel getModel6() {
		return model6;
	}

	public boolean isResultsVisible() {
		return resultsVisible;
	}

}