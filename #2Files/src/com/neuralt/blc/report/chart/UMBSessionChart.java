package com.neuralt.blc.report.chart;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.UMBSessionController;

@ManagedBean(name = "UMBSessionChart")
@SessionScoped

public class UMBSessionChart {
	
	private CartesianChartModel model1, model2, model3;
	public boolean resultsVisible = false;
	
	public UMBSessionChart() {
		
	}

	public CartesianChartModel UpdateTotalTranChartData() {

		//System.out.println("in UpdateTotalTranChartData");	
		
		model1 = null;
		model1 = new CartesianChartModel();
				
		String[] chartdates1 = UMBSessionController.getChartdates();
		int[] totaltransdata = UMBSessionController.getTotaltransdata();	
		
		ChartSeries TotalTransChart = new ChartSeries();		
		TotalTransChart.setLabel("Total transaction");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateTotalTranChartData, chartdates1[" + i + "]" + chartdates1[i]);
				TotalTransChart.set(chartdates1[i],totaltransdata[i]);
			}				
		}
		else
			TotalTransChart.set("0",0 );
		
		model1.addSeries(TotalTransChart);
				
		resultsVisible = true;
		
		return model1;
	}
	
	
	public CartesianChartModel UpdateDurationChartData() {

		//System.out.println("in UpdateDurationChartData");	
		
		model3 = null;
		model3 = new CartesianChartModel();
				
		String[] chartdates1 = UMBSessionController.getChartdates();		
		int[] mindurdata = UMBSessionController.getMindurdata();
		int[] maxdurdata = UMBSessionController.getMaxdurdata();		
		float[] avgdurdata = UMBSessionController.getAvgdurdata();	
		
		ChartSeries MinDurChart = new ChartSeries();		
		MinDurChart.setLabel("Min duration");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateDurationChartData, chartdates1[" + i + "]" + chartdates1[i]);
				MinDurChart.set(chartdates1[i],mindurdata[i]);
			}				
		}
		else
			MinDurChart.set("0",0 );
		
		model3.addSeries(MinDurChart);
		
		ChartSeries MaxDurChart = new ChartSeries();		
		MaxDurChart.setLabel("Max duration");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateDurationChartData, chartdates1[" + i + "]" + chartdates1[i]);
				MaxDurChart.set(chartdates1[i],maxdurdata[i]);
			}				
		}
		else
			MaxDurChart.set("0",0 );
		
		model3.addSeries(MaxDurChart);

		ChartSeries AvgDurChart = new ChartSeries();		
		AvgDurChart.setLabel("Average duration");
		
		if (chartdates1 != null)
		{
			for (int i = 0; i < chartdates1.length; i++)
			{
				//System.out.println("in UpdateDurationChartData, chartdates1[" + i + "]" + chartdates1[i]);
				AvgDurChart.set(chartdates1[i],avgdurdata[i]);
			}				
		}
		else
			AvgDurChart.set("0",0 );
		
		model3.addSeries(AvgDurChart);
		
		
		resultsVisible = true;
		
		return model3;
	}
	
	
	public CartesianChartModel getModel1() {return model1; }
	
	public CartesianChartModel getModel2() { return model2; }
	
	public CartesianChartModel getModel3() { return model3; }

	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
	
}