package com.neuralt.blc.report.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.UMBMenuHitByMenuController;

@ManagedBean(name = "UMBMenuHitByMenuChart")
@SessionScoped

public class UMBMenuHitByMenuChart {
	
	private CartesianChartModel model1, model2, model3, model4, model5, model6;
	public boolean resultsVisible = false;
	
	public UMBMenuHitByMenuChart() {
		
	}

	public CartesianChartModel UpdateTotalTranChartData() {

		//System.out.println("in UpdateTotalTranChartData");	
		
		model1 = null;
		model1 = new CartesianChartModel();
		
		String[] menu = UMBMenuHitByMenuController.getMenu();
		String[] chartdates1 = UMBMenuHitByMenuController.getChartdates();
		int[] totaltransdata = UMBMenuHitByMenuController.getTotaltransdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (menu != null)
		{
			String prevMenu = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int menucount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < menu.length)
			//for (int i = 0; i < menu.length; i++)
			{
				String currentMenu = menu[i];
				if (currentMenu.compareTo(prevMenu) != 0)
				{
					if (menucount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							//System.out.println("series=" + menucount + ",chartdate=" +s + ",totaltransdata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model1.addSeries(TotalTransChart.get(menucount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++menucount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(menu[i]);			
					System.out.println("series=" + menucount + ",label=" +menu[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						System.out.println("series=" + menucount + ",chartdate=" +s + ",totaltransdata=0");
						s = itr.next();				    
					}

					
					System.out.println("series=" + menucount + ",chartdate=" +chartdates1[i] + ",totaltransdata=" + totaltransdata[i]);
					TempTransChart.set(chartdates1[i],totaltransdata[i]);
				}
				prevMenu = currentMenu;
				++i;
			}			

			if (menu.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",totaltransdata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model1.addSeries(TotalTransChart.get(menucount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model1.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model1;
	}
	
	
	public CartesianChartModel UpdateTotalTranChartDataSuccess() {

		//System.out.println("in UpdateTotalTranChartDataSuccess");	
		
		model2 = null;
		model2 = new CartesianChartModel();
		
		String[] menu = UMBMenuHitByMenuController.getMenu();
		String[] chartdates1 = UMBMenuHitByMenuController.getChartdates();
		int[] totalsuccessdata = UMBMenuHitByMenuController.getTotalsuccessdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (menu != null)
		{
			String prevMenu = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int menucount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < menu.length)
			//for (int i = 0; i < menu.length; i++)
			{
				String currentMenu = menu[i];
				if (currentMenu.compareTo(prevMenu) != 0)
				{
					if (menucount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							//System.out.println("series=" + menucount + ",chartdate=" +s + ",totalsuccessdata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model2.addSeries(TotalTransChart.get(menucount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++menucount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(menu[i]);			
					//System.out.println("series=" + menucount + ",label=" +menu[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",totalsuccessdata=0");
						s = itr.next();				    
					}

					TempTransChart.set(chartdates1[i],totalsuccessdata[i]);
					//System.out.println("series=" + menucount + ",chartdate=" +chartdates1[i] + ",totalsuccessdata=" + totalsuccessdata[i]);
					
				}
				prevMenu = currentMenu;
				++i;
			}			

			if (menu.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",totalsuccessdata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model2.addSeries(TotalTransChart.get(menucount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model2.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model2;
	}
	
	public CartesianChartModel UpdateTotalTranChartDataFail() {

		//System.out.println("in UpdateTotalTranChartDataFail");	
		
		model3 = null;
		model3 = new CartesianChartModel();
		
		String[] menu = UMBMenuHitByMenuController.getMenu();
		String[] chartdates1 = UMBMenuHitByMenuController.getChartdates();
		int[] totalfaildata = UMBMenuHitByMenuController.getTotalfaildata();				

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (menu != null)
		{
			String prevMenu = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int menucount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < menu.length)
			//for (int i = 0; i < menu.length; i++)
			{
				String currentMenu = menu[i];
				if (currentMenu.compareTo(prevMenu) != 0)
				{
					if (menucount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							//System.out.println("series=" + menucount + ",chartdate=" +s + ",totalfaildata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model3.addSeries(TotalTransChart.get(menucount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++menucount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(menu[i]);			
					//System.out.println("series=" + menucount + ",label=" +menu[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",totalfaildata=0");
						s = itr.next();				    
					}

					TempTransChart.set(chartdates1[i],totalfaildata[i]);
					//System.out.println("series=" + menucount + ",chartdate=" +chartdates1[i] + ",totalfaildata=" + totalfaildata[i]);
					
				}
				prevMenu = currentMenu;
				++i;
			}			

			if (menu.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",totalfaildata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model3.addSeries(TotalTransChart.get(menucount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model3.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model3;
	}
		
	
	public CartesianChartModel UpdateDurationChartData() {

		//System.out.println("in UpdateDurationChartData");	
		
		model4 = null;
		model4 = new CartesianChartModel();
		
		String[] menu = UMBMenuHitByMenuController.getMenu();
		String[] chartdates1 = UMBMenuHitByMenuController.getChartdates();
		float[] avgdurdata = UMBMenuHitByMenuController.getAvgdurdata();			

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (menu != null)
		{
			String prevMenu = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int menucount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;
            
			while (i < menu.length)
			//for (int i = 0; i < menu.length; i++)
			{
				String currentMenu = menu[i];
				if (currentMenu.compareTo(prevMenu) != 0)
				{
					if (menucount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempDurChart.set(s,0);
							//System.out.println("series=" + menucount + ",chartdate=" +s + ",avgdurdata=0");											    
						}
						
						DurChart.add(TempDurChart);						
						model4.addSeries(DurChart.get(menucount));
						TempDurChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++menucount;					
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(menu[i]);			
					//System.out.println("series=" + menucount + ",label=" +menu[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempDurChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",avgdurdata=0");
						s = itr.next();				    
					}

					TempDurChart.set(chartdates1[i],avgdurdata[i]);
					//System.out.println("series=" + menucount + ",chartdate=" +chartdates1[i] + ",avgdurdata=" + avgdurdata[i]);
					
				}
				prevMenu = currentMenu;
				++i;
			}			

			if (menu.length > 0)
			{			
				if (TempDurChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempDurChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",avgdurdata=0");											    
					}
					
					DurChart.add(TempDurChart);						
					model4.addSeries(DurChart.get(menucount));
					TempDurChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries DurChart= new ChartSeries();
			DurChart.set("0",0 );
			model4.addSeries(DurChart);
		}

				
		resultsVisible = true;
		
		return model4;
	}
	
	
	public CartesianChartModel UpdateDurationChartDataMin() {

		//System.out.println("in UpdateDurationChartDataMin");	
		
		model5 = null;
		model5 = new CartesianChartModel();
		
		String[] menu = UMBMenuHitByMenuController.getMenu();
		String[] chartdates1 = UMBMenuHitByMenuController.getChartdates();
		int[] mindurdata = UMBMenuHitByMenuController.getMindurdata();		

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (menu != null)
		{
			String prevMenu = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int menucount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;
            
			while (i < menu.length)
			//for (int i = 0; i < menu.length; i++)
			{
				String currentMenu = menu[i];
				if (currentMenu.compareTo(prevMenu) != 0)
				{
					if (menucount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempDurChart.set(s,0);
							//System.out.println("series=" + menucount + ",chartdate=" +s + ",mindurdata=0");											    
						}
						
						DurChart.add(TempDurChart);						
						model5.addSeries(DurChart.get(menucount));
						TempDurChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++menucount;					
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(menu[i]);			
					//System.out.println("series=" + menucount + ",label=" +menu[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempDurChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",mindurdata=0");
						s = itr.next();				    
					}

					TempDurChart.set(chartdates1[i],mindurdata[i]);
					//System.out.println("series=" + menucount + ",chartdate=" +chartdates1[i] + ",mindurdata=" + mindurdata[i]);
					
				}
				prevMenu = currentMenu;
				++i;
			}			

			if (menu.length > 0)
			{			
				if (TempDurChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempDurChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",mindurdata=0");											    
					}
					
					DurChart.add(TempDurChart);						
					model5.addSeries(DurChart.get(menucount));
					TempDurChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries DurChart= new ChartSeries();
			DurChart.set("0",0 );
			model5.addSeries(DurChart);
		}

				
		resultsVisible = true;
		
		return model5;
	}
	
	public CartesianChartModel UpdateDurationChartDataMax() {

		//System.out.println("in UpdateDurationChartDataMax");	
		
		model6 = null;
		model6 = new CartesianChartModel();
		
		String[] menu = UMBMenuHitByMenuController.getMenu();
		String[] chartdates1 = UMBMenuHitByMenuController.getChartdates();
		int[] maxdurdata = UMBMenuHitByMenuController.getMaxdurdata();			

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (menu != null)
		{
			String prevMenu = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int menucount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;
            
			while (i < menu.length)
			//for (int i = 0; i < menu.length; i++)
			{
				String currentMenu = menu[i];
				if (currentMenu.compareTo(prevMenu) != 0)
				{
					if (menucount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempDurChart.set(s,0);
							//System.out.println("series=" + menucount + ",chartdate=" +s + ",maxdurdata=0");											    
						}
						
						DurChart.add(TempDurChart);						
						model6.addSeries(DurChart.get(menucount));
						TempDurChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++menucount;					
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(menu[i]);			
					//System.out.println("series=" + menucount + ",label=" +menu[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempDurChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",maxdurdata=0");
						s = itr.next();				    
					}

					TempDurChart.set(chartdates1[i],maxdurdata[i]);
					//System.out.println("series=" + menucount + ",chartdate=" +chartdates1[i] + ",maxdurdata=" + maxdurdata[i]);
					
				}
				prevMenu = currentMenu;
				++i;
			}			

			if (menu.length > 0)
			{			
				if (TempDurChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempDurChart.set(s,0);
						//System.out.println("series=" + menucount + ",chartdate=" +s + ",maxdurdata=0");											    
					}
					
					DurChart.add(TempDurChart);						
					model6.addSeries(DurChart.get(menucount));
					TempDurChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries DurChart= new ChartSeries();
			DurChart.set("0",0 );
			model6.addSeries(DurChart);
		}

				
		resultsVisible = true;
		
		return model6;
	}	
	
		
	public CartesianChartModel getModel1() { return model1;	}
	
	public CartesianChartModel getModel2() { return model2; }
	
	public CartesianChartModel getModel3() { return model3; }

	public CartesianChartModel getModel4() { return model4; }
	
	public CartesianChartModel getModel5() { return model5; }
	
	public CartesianChartModel getModel6() { return model6; }
	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
	
}