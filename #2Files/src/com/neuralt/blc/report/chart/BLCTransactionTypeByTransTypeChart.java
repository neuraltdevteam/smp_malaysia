package com.neuralt.blc.report.chart;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.primefaces.model.chart.CartesianChartModel;
import org.primefaces.model.chart.ChartSeries;

import com.neuralt.blc.report.controller.BLCTransactionTypeByTransTypeController;

@ManagedBean(name = "BLCTransactionTypeByTransTypeChart")
@SessionScoped

public class BLCTransactionTypeByTransTypeChart {
	
	private CartesianChartModel model1, model2, model3, model4, model5, model6;
	public boolean resultsVisible = false;
	
	public BLCTransactionTypeByTransTypeChart() {
		
	}

	public CartesianChartModel UpdateTotalTranChartData() {

		//System.out.println("in UpdateTotalTranChartData");	
		
		model1 = null;
		model1 = new CartesianChartModel();
		
		String[] transtype = BLCTransactionTypeByTransTypeController.getTranstype();
		String[] chartdates1 = BLCTransactionTypeByTransTypeController.getChartdates();
		int[] totaltransdata = BLCTransactionTypeByTransTypeController.getTotaltransdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (transtype != null)
		{
			String prevTransType = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int transtypecount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < transtype.length)
			//for (int i = 0; i < transtype.length; i++)
			{
				String currentTransType = transtype[i];
				if (currentTransType.compareTo(prevTransType) != 0)
				{
					if (transtypecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							System.out.println("series=" + transtypecount + ",chartdate=" +s + ",totaltransdata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model1.addSeries(TotalTransChart.get(transtypecount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++transtypecount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(transtype[i]);			
					//System.out.println("series=" + transtypecount + ",label=" +transtype[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",totaltransdata=0");
						s = itr.next();				    
					}

					TempTransChart.set(chartdates1[i],totaltransdata[i]);
					System.out.println("series=" + transtypecount + ",chartdate=" +chartdates1[i] + ",totaltransdata=" + totaltransdata[i]);
					
				}
				prevTransType = currentTransType;
				++i;
			}			

			if (transtype.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",totaltransdata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model1.addSeries(TotalTransChart.get(transtypecount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model1.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model1;
	}
	
	
	public CartesianChartModel UpdateTotalTranChartDataSuccess() {

		//System.out.println("in UpdateTotalTranChartDataSuccess");	
		
		model2 = null;
		model2 = new CartesianChartModel();
		
		String[] transtype = BLCTransactionTypeByTransTypeController.getTranstype();
		String[] chartdates1 = BLCTransactionTypeByTransTypeController.getChartdates();
		int[] totalsuccessdata = BLCTransactionTypeByTransTypeController.getTotalsuccessdata();

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (transtype != null)
		{
			String prevTransType = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int transtypecount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < transtype.length)
			//for (int i = 0; i < transtype.length; i++)
			{
				String currentTransType = transtype[i];
				if (currentTransType.compareTo(prevTransType) != 0)
				{
					if (transtypecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							System.out.println("series=" + transtypecount + ",chartdate=" +s + ",totalsuccessdata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model2.addSeries(TotalTransChart.get(transtypecount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++transtypecount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(transtype[i]);			
					//System.out.println("series=" + transtypecount + ",label=" +transtype[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",totalsuccessdata=0");
						s = itr.next();				    
					}

					TempTransChart.set(chartdates1[i],totalsuccessdata[i]);
					System.out.println("series=" + transtypecount + ",chartdate=" +chartdates1[i] + ",totalsuccessdata=" + totalsuccessdata[i]);
					
				}
				prevTransType = currentTransType;
				++i;
			}			

			if (transtype.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",totalsuccessdata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model2.addSeries(TotalTransChart.get(transtypecount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model2.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model2;
	}
	
	public CartesianChartModel UpdateTotalTranChartDataFail() {

		//System.out.println("in UpdateTotalTranChartDataFail");	
		
		model3 = null;
		model3 = new CartesianChartModel();
		
		String[] transtype = BLCTransactionTypeByTransTypeController.getTranstype();
		String[] chartdates1 = BLCTransactionTypeByTransTypeController.getChartdates();
		int[] totalfaildata = BLCTransactionTypeByTransTypeController.getTotalfaildata();				

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (transtype != null)
		{
			String prevTransType = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int transtypecount = -1;
			List<ChartSeries> TotalTransChart = new ArrayList<ChartSeries>();
			ChartSeries TempTransChart = null;
            
			while (i < transtype.length)
			//for (int i = 0; i < transtype.length; i++)
			{
				String currentTransType = transtype[i];
				if (currentTransType.compareTo(prevTransType) != 0)
				{
					if (transtypecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempTransChart.set(s,0);
							System.out.println("series=" + transtypecount + ",chartdate=" +s + ",totalfaildata=0");											    
						}
						
						TotalTransChart.add(TempTransChart);						
						model3.addSeries(TotalTransChart.get(transtypecount));
						TempTransChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++transtypecount;					
					TempTransChart = new ChartSeries();
					TempTransChart.setLabel(transtype[i]);			
					//System.out.println("series=" + transtypecount + ",label=" +transtype[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempTransChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",totalfaildata=0");
						s = itr.next();				    
					}

					TempTransChart.set(chartdates1[i],totalfaildata[i]);
					System.out.println("series=" + transtypecount + ",chartdate=" +chartdates1[i] + ",totalfaildata=" + totalfaildata[i]);
					
				}
				prevTransType = currentTransType;
				++i;
			}			

			if (transtype.length > 0)
			{			
				if (TempTransChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempTransChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",totalfaildata=0");											    
					}
					
					TotalTransChart.add(TempTransChart);						
					model3.addSeries(TotalTransChart.get(transtypecount));
					TempTransChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries TotalTransChart= new ChartSeries();
			TotalTransChart.set("0",0 );
			model3.addSeries(TotalTransChart);
		}

				
		resultsVisible = true;
		
		return model3;
	}
		
	
	public CartesianChartModel UpdateDurationChartData() {

		//System.out.println("in UpdateDurationChartData");	
		
		model4 = null;
		model4 = new CartesianChartModel();
		
		String[] transtype = BLCTransactionTypeByTransTypeController.getTranstype();
		String[] chartdates1 = BLCTransactionTypeByTransTypeController.getChartdates();
		float[] avgdurdata = BLCTransactionTypeByTransTypeController.getAvgdurdata();			

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (transtype != null)
		{
			String prevTransType = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int transtypecount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;
            
			while (i < transtype.length)
			//for (int i = 0; i < transtype.length; i++)
			{
				String currentTransType = transtype[i];
				if (currentTransType.compareTo(prevTransType) != 0)
				{
					if (transtypecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempDurChart.set(s,0);
							System.out.println("series=" + transtypecount + ",chartdate=" +s + ",avgdurdata=0");											    
						}
						
						DurChart.add(TempDurChart);						
						model4.addSeries(DurChart.get(transtypecount));
						TempDurChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++transtypecount;					
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(transtype[i]);			
					//System.out.println("series=" + transtypecount + ",label=" +transtype[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempDurChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",avgdurdata=0");
						s = itr.next();				    
					}

					TempDurChart.set(chartdates1[i],avgdurdata[i]);
					System.out.println("series=" + transtypecount + ",chartdate=" +chartdates1[i] + ",avgdurdata=" + avgdurdata[i]);
					
				}
				prevTransType = currentTransType;
				++i;
			}			

			if (transtype.length > 0)
			{			
				if (TempDurChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempDurChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",avgdurdata=0");											    
					}
					
					DurChart.add(TempDurChart);						
					model4.addSeries(DurChart.get(transtypecount));
					TempDurChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries DurChart= new ChartSeries();
			DurChart.set("0",0 );
			model4.addSeries(DurChart);
		}

				
		resultsVisible = true;
		
		return model4;
	}
	
	
	public CartesianChartModel UpdateDurationChartDataMin() {

		//System.out.println("in UpdateDurationChartDataMin");	
		
		model5 = null;
		model5 = new CartesianChartModel();
		
		String[] transtype = BLCTransactionTypeByTransTypeController.getTranstype();
		String[] chartdates1 = BLCTransactionTypeByTransTypeController.getChartdates();
		int[] mindurdata = BLCTransactionTypeByTransTypeController.getMindurdata();		

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (transtype != null)
		{
			String prevTransType = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int transtypecount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;
            
			while (i < transtype.length)
			//for (int i = 0; i < transtype.length; i++)
			{
				String currentTransType = transtype[i];
				if (currentTransType.compareTo(prevTransType) != 0)
				{
					if (transtypecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempDurChart.set(s,0);
							System.out.println("series=" + transtypecount + ",chartdate=" +s + ",mindurdata=0");											    
						}
						
						DurChart.add(TempDurChart);						
						model5.addSeries(DurChart.get(transtypecount));
						TempDurChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++transtypecount;					
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(transtype[i]);			
					//System.out.println("series=" + transtypecount + ",label=" +transtype[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempDurChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",mindurdata=0");
						s = itr.next();				    
					}

					TempDurChart.set(chartdates1[i],mindurdata[i]);
					System.out.println("series=" + transtypecount + ",chartdate=" +chartdates1[i] + ",mindurdata=" + mindurdata[i]);
					
				}
				prevTransType = currentTransType;
				++i;
			}			

			if (transtype.length > 0)
			{			
				if (TempDurChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempDurChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",mindurdata=0");											    
					}
					
					DurChart.add(TempDurChart);						
					model5.addSeries(DurChart.get(transtypecount));
					TempDurChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries DurChart= new ChartSeries();
			DurChart.set("0",0 );
			model5.addSeries(DurChart);
		}

				
		resultsVisible = true;
		
		return model5;
	}
	
	public CartesianChartModel UpdateDurationChartDataMax() {

		//System.out.println("in UpdateDurationChartDataMax");	
		
		model6 = null;
		model6 = new CartesianChartModel();
		
		String[] transtype = BLCTransactionTypeByTransTypeController.getTranstype();
		String[] chartdates1 = BLCTransactionTypeByTransTypeController.getChartdates();
		int[] maxdurdata = BLCTransactionTypeByTransTypeController.getMaxdurdata();			

		Set<String> uniqChartdates = new TreeSet<String>();
		uniqChartdates.addAll(Arrays.asList(chartdates1));
		Iterator<String> itr=uniqChartdates.iterator();		
	
		if (transtype != null)
		{
			String prevTransType = "<Dummy]";
			
			int i = 0;
			int j = 0;
			int transtypecount = -1;
			List<ChartSeries> DurChart = new ArrayList<ChartSeries>();
			ChartSeries TempDurChart = null;
            
			while (i < transtype.length)
			//for (int i = 0; i < transtype.length; i++)
			{
				String currentTransType = transtype[i];
				if (currentTransType.compareTo(prevTransType) != 0)
				{
					if (transtypecount > -1)
					{
						while(itr.hasNext()){					
							String s = itr.next();
							TempDurChart.set(s,0);
							System.out.println("series=" + transtypecount + ",chartdate=" +s + ",maxdurdata=0");											    
						}
						
						DurChart.add(TempDurChart);						
						model6.addSeries(DurChart.get(transtypecount));
						TempDurChart = null;
						
						itr = null;
						itr=uniqChartdates.iterator();
					}
					
					++transtypecount;					
					TempDurChart = new ChartSeries();
					TempDurChart.setLabel(transtype[i]);			
					//System.out.println("series=" + transtypecount + ",label=" +transtype[i]);
					j = 0;		
				}
				
				
				if (chartdates1 != null)
				{					

					String s= "";
					if (itr.hasNext())	
							s = itr.next();
					
					while(itr.hasNext() && s.compareTo(chartdates1[i]) != 0){					
						TempDurChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",maxdurdata=0");
						s = itr.next();				    
					}

					TempDurChart.set(chartdates1[i],maxdurdata[i]);
					System.out.println("series=" + transtypecount + ",chartdate=" +chartdates1[i] + ",maxdurdata=" + maxdurdata[i]);
					
				}
				prevTransType = currentTransType;
				++i;
			}			

			if (transtype.length > 0)
			{			
				if (TempDurChart != null)
				{
					while(itr.hasNext()){					
						String s = itr.next();
						TempDurChart.set(s,0);
						System.out.println("series=" + transtypecount + ",chartdate=" +s + ",maxdurdata=0");											    
					}
					
					DurChart.add(TempDurChart);						
					model6.addSeries(DurChart.get(transtypecount));
					TempDurChart = null;
				}
			}
			
		}
		else
		{			
			
			ChartSeries DurChart= new ChartSeries();
			DurChart.set("0",0 );
			model6.addSeries(DurChart);
		}

				
		resultsVisible = true;
		
		return model6;
	}	
	
		
	public CartesianChartModel getModel1() { return model1;	}
	
	public CartesianChartModel getModel2() { return model2; }
	
	public CartesianChartModel getModel3() { return model3; }

	public CartesianChartModel getModel4() { return model4; }
	
	public CartesianChartModel getModel5() { return model5; }
	
	public CartesianChartModel getModel6() { return model6; }
	
	public boolean isResultsVisible() {
		return resultsVisible;
	}
	
}