package com.neuralt.blc.report.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SMS_OUT")
public class SmsOut implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1720902415006839114L;
	@Id
	@GeneratedValue
	private Long id;
	private String start_date;
	private String host_id;
	private String trans_status;
	private String reason_code;
	private int total_traffic;
	private int total_success;
	private int total_fail;
	private float success_pct;
	private float fail_pct;

	public SmsOut() {
	}

	public SmsOut(Long id, String start_date, String host_id,
			String trans_status, String reason_code, int total_traffic,
			int total_success, int total_fail, float success_pct, float fail_pct) {
		this.id = id;
		this.start_date = start_date;
		this.host_id = host_id;
		this.trans_status = trans_status;
		this.reason_code = reason_code;
		this.total_traffic = total_traffic;
		this.total_success = total_success;
		this.total_fail = total_fail;
		this.success_pct = success_pct;
		this.fail_pct = fail_pct;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}

	public String getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String trans_status) {
		this.trans_status = trans_status;
	}

	public String getReason_code() {
		return reason_code;
	}

	public void setReason_code(String reason_code) {
		this.reason_code = reason_code;
	}

	public int getTotal_traffic() {
		return total_traffic;
	}

	public void setTotal_traffic(int total_traffic) {
		this.total_traffic = total_traffic;
	}

	public int getTotal_success() {
		return total_success;
	}

	public void setTotal_success(int total_success) {
		this.total_success = total_success;
	}

	public int getTotal_fail() {
		return total_fail;
	}

	public void setTotal_fail(int total_fail) {
		this.total_fail = total_fail;
	}

	public float getSuccess_pct() {
		return success_pct;
	}

	public void setSuccess_pct(float success_pct) {
		this.success_pct = success_pct;
	}

	public float getFail_pct() {
		return fail_pct;
	}

	public void setFail_pct(float fail_pct) {
		this.fail_pct = fail_pct;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SmsOut [id=").append(id).append(", start_date=")
				.append(start_date).append(", host_id=").append(host_id)
				.append(", trans_status=").append(trans_status)
				.append(", reason_code=").append(reason_code)
				.append(", total_traffic=").append(total_traffic)
				.append(", total_success=").append(total_success)
				.append(", total_fail=").append(total_fail)
				.append(", success_pct=").append(success_pct)
				.append(", fail_pct=").append(fail_pct).append("]");
		return builder.toString();
	}

}
