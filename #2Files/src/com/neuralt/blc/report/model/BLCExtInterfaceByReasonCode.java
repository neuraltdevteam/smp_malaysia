package com.neuralt.blc.report.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BLC_EXT_INTERFACE_REASON_CODE")
public class BLCExtInterfaceByReasonCode implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2950127733189963072L;
	@Id
	@GeneratedValue
	private Long id;
	private String start_date;
	private String api_name;
	private String trans_status;
	private String reason_code;
	private int total_count;
	private float percentage;

	public BLCExtInterfaceByReasonCode() {
	}
	

	public BLCExtInterfaceByReasonCode(Long id, String start_date, String api_name,
			String trans_status, String reason_code, int total_count, float percentage) {
		this.id = id;
		this.start_date = start_date;
		this.api_name = api_name;
		this.trans_status = trans_status;
		this.reason_code = reason_code;
		this.total_count = total_count;
		this.percentage = percentage;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getApi_name() {
		return api_name;
	}

	public void setApi_name(String api_name) {
		this.api_name = api_name;
	}

	public String getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String trans_status) {
		this.trans_status = trans_status;
	}

	public String getReason_code() {
		return reason_code;
	}

	public void setReason_code(String reason_code) {
		this.reason_code = reason_code;
	}

	public int getTotal_count() {
		return total_count;
	}

	public void setTotal_count(int total_count) {
		this.total_count = total_count;
	}

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BLCExtInterface [id=").append(id)
				.append(", start_date=").append(start_date)
				.append(", api_name=").append(api_name)
				.append(", trans_status=").append(trans_status)
				.append(", reason_code=").append(reason_code)
				.append(", total_count=").append(total_count).append(", percentage=")
				.append(percentage).append("]");
		return builder.toString();
	}


}
