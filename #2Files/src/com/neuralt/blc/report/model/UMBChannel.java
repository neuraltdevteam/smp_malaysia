package com.neuralt.blc.report.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UMB_Channel")
public class UMBChannel implements Serializable {

	private static final long serialVersionUID = 5496646706532629875L;
	@Id
	@GeneratedValue
	private Long id;
	private String start_date;	
	private String channel_name;
	private String in_sms_channel;
	private int total_trans;
	private float percentage;
	
	public UMBChannel() {
	}

	public UMBChannel(String start_date, String channel_name, String in_sms_channel, int total_trans,
						float percentage) {
		this.start_date = start_date;
		this.channel_name = channel_name;
		this.in_sms_channel = in_sms_channel;
		this.total_trans = total_trans;		
		this.percentage = percentage;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getChannel_name() {
		return channel_name;
	}

	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}
	
	public String getIn_sms_channel() {
		return in_sms_channel;
	}

	public void setIn_sms_channel(String in_sms_channel) {
		this.in_sms_channel = in_sms_channel;
	}

	public int getTotal_trans() {
		return total_trans;
	}

	public void setTotal_trans(int total_trans) {
		this.total_trans = total_trans;
	}

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Transaction [id=").append(id)
				.append(", start_date=").append(start_date)
				.append(", channel_name=").append(channel_name)
				.append(", in_sms_channel=").append(in_sms_channel)
				.append(", total_trans=").append(total_trans)
				.append(", percentage=").append(percentage)
				.append("]");
		return builder.toString();
	}
}
