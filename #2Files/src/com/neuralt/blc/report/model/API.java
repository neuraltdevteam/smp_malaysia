package com.neuralt.blc.report.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "API")
public class API implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2690388606447339481L;
	@Id
	@Column(name = "api_name", unique = true, nullable = false)
	private String api_name;

	public String getApi_name() {
		return api_name;
	}

	public void setApi_name(String api_name) {
		this.api_name = api_name;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("API [api_name=").append(api_name).append("]");
		return builder.toString();
	}

}
