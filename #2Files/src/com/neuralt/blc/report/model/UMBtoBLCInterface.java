package com.neuralt.blc.report.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UMB_BLC_INTERFACE")
public class UMBtoBLCInterface implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 714566843009256627L;
	@Id
	@GeneratedValue
	private Long id;
	private String start_date;
	private String host_id;
	private String interface_name;
	private String trans_status;
	private String reason_code;
	private int total_traffic;
	private int total_success;
	private int total_fail;
	private float success_pct;
	private float fail_pct;
	private float avg_dur;
	private int min_dur;
	private int max_dur;

	public UMBtoBLCInterface() {
	}

	public UMBtoBLCInterface(Long id, String start_date, String host_id,
			String interface_name, String trans_status, String reason_code,
			int total_traffic, int total_success, int total_fail,
			float success_pct, float fail_pct, float avg_dur, int min_dur,
			int max_dur) {
		this.id = id;
		this.start_date = start_date;
		this.host_id = host_id;
		this.interface_name = interface_name;
		this.trans_status = trans_status;
		this.reason_code = reason_code;
		this.total_traffic = total_traffic;
		this.total_success = total_success;
		this.total_fail = total_fail;
		this.success_pct = success_pct;
		this.fail_pct = fail_pct;
		this.avg_dur = avg_dur;
		this.min_dur = min_dur;
		this.max_dur = max_dur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}

	public String getInterface_name() {
		return interface_name;
	}

	public void setInterface_name(String interface_name) {
		this.interface_name = interface_name;
	}

	public String getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String trans_status) {
		this.trans_status = trans_status;
	}

	public String getReason_code() {
		return reason_code;
	}

	public void setReason_code(String reason_code) {
		this.reason_code = reason_code;
	}

	public int getTotal_traffic() {
		return total_traffic;
	}

	public void setTotal_traffic(int total_traffic) {
		this.total_traffic = total_traffic;
	}

	public int getTotal_success() {
		return total_success;
	}

	public void setTotal_success(int total_success) {
		this.total_success = total_success;
	}

	public int getTotal_fail() {
		return total_fail;
	}

	public void setTotal_fail(int total_fail) {
		this.total_fail = total_fail;
	}

	public float getSuccess_pct() {
		return success_pct;
	}

	public void setSuccess_pct(float success_pct) {
		this.success_pct = success_pct;
	}

	public float getFail_pct() {
		return fail_pct;
	}

	public void setFail_pct(float fail_pct) {
		this.fail_pct = fail_pct;
	}

	public float getAvg_dur() {
		return avg_dur;
	}

	public void setAvg_dur(float avg_dur) {
		this.avg_dur = avg_dur;
	}

	public int getMin_dur() {
		return min_dur;
	}

	public void setMin_dur(int min_dur) {
		this.min_dur = min_dur;
	}

	public int getMax_dur() {
		return max_dur;
	}

	public void setMax_dur(int max_dur) {
		this.max_dur = max_dur;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UMBtoBLCInterface [id=").append(id)
				.append(", start_date=").append(start_date)
				.append(", host_id=").append(host_id)
				.append(", interface_name=").append(interface_name)
				.append(", trans_status=").append(trans_status)
				.append(", reason_code=").append(reason_code)
				.append(", total_traffic=").append(total_traffic)
				.append(", total_success=").append(total_success)
				.append(", total_fail=").append(total_fail)
				.append(", success_pct=").append(success_pct)
				.append(", fail_pct=").append(fail_pct).append(", avg_dur=")
				.append(avg_dur).append(", min_dur=").append(min_dur)
				.append(", max_dur=").append(max_dur).append("]");
		return builder.toString();
	}

	

}
