package com.neuralt.blc.report.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRANS")
public class Transaction implements Serializable {

	private static final long serialVersionUID = -6769693610111010249L;
	//private static final long serialVersionUID = 3589103961469466832L;
	//private static final long serialVersionUID = -3256072599320969706L;

	@Id
	@GeneratedValue
	private Long id;
	private Timestamp startdate;
	private Timestamp enddate;
	private Timestamp duration;
	private String API;
	private String status;
	private String reasoncode;
	
	
	public Transaction() {
	}

	public Transaction(Timestamp startdate, Timestamp enddate, String API,
			String status, String reasoncode) {
		this.startdate = startdate;
		this.enddate = enddate;
		this.API = API;		
		this.status = status;
		this.reasoncode = reasoncode;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getStartdate() {
		return startdate;
	}

	public void setStartDate(Timestamp startdate) {
		this.startdate = startdate;
	}

	public Timestamp getEnddate() {
		return enddate;
	}

	public void setEnddate(Timestamp enddate) {
		this.enddate = enddate;
	}
	
	public String getAPI() {
		return API;
	}

	public void setAPI(String API) {
		this.API = API;
	}

	public Timestamp getDuration() {
		return duration;
	}

	public void setDuration(Timestamp duration) {
		this.duration = duration;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getReasoncode() {
		return reasoncode;
	}

	public void setReasoncode(String reasoncode) {
		this.reasoncode = reasoncode;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Transaction [id=").append(id)
				.append(", startDate=").append(startdate)
				.append(", enddate=").append(enddate)
				.append(", API=").append(API)
				.append(", duration=").append(duration)
				.append(", status=").append(status)
				.append(", reasoncode=").append(reasoncode).append("]");
		return builder.toString();
	}
}
