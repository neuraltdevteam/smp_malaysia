package com.neuralt.blc.report.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UMB_SESS")
public class UMBSession implements Serializable {
	
	private static final long serialVersionUID = -3462499435509218875L;
	@Id
	@GeneratedValue
	private Long id;
	@Column(name="start_date")
	private String start_date;	
	private String host_id;
	private String short_code;
	private int total_trans;
	private float avg_dur;
	private int min_dur;
	private int max_dur;
	
	public UMBSession() {
	}

	public UMBSession(String start_date, String host_id, String short_code, int total_trans,
						float avg_dur,int min_dur,int max_dur) {
		this.start_date = start_date;
		this.host_id = host_id;
		this.short_code = short_code;
		this.total_trans = total_trans;		
		this.avg_dur = avg_dur;
		this.min_dur = min_dur;
		this.max_dur = max_dur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}
	
	public String getShort_code() {
		return short_code;
	}

	public void setShort_code(String short_code) {
		this.short_code = short_code;
	}

	public int getTotal_trans() {
		return total_trans;
	}

	public void setTotal_trans(int total_trans) {
		this.total_trans = total_trans;
	}

		public float getAvg_dur() {
		return avg_dur;
	}

	public void setAvg_dur(float avg_dur) {
		this.avg_dur = avg_dur;
	}
	
	public int getMin_dur() {
		return min_dur;
	}

	public void setMin_dur(int min_dur) {
		this.min_dur = min_dur;
	}

	public int getMax_dur() {
		return max_dur;
	}

	public void setMax_dur(int max_dur) {
		this.max_dur = max_dur;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Transaction [id=").append(id)
				.append(", start_date=").append(start_date)
				.append(", host_id=").append(host_id)
				.append(", short_code=").append(short_code)
				.append(", total_trans=").append(total_trans)
				.append(", avg_dur=").append(avg_dur)
				.append(", min_dur=").append(min_dur)
				.append(", max_dur=").append(max_dur)
				.append("]");
		return builder.toString();
	}
}
