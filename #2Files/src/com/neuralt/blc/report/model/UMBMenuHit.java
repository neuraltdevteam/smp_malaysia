package com.neuralt.blc.report.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "UMB_Menu_Hit")
public class UMBMenuHit implements Serializable {
	
	private static final long serialVersionUID = -4455063888349168044L;
	@Id
	@GeneratedValue
	private Long id;
	private String start_date;	
	private String host_id;
	private String short_code;	
	private String menu_id;
	private String menu_name;		
	private String trans_status;
	private String reason_code;	
	private int total_hit;
	private int total_success;
	private int total_fail;
	private float success_pct;
	private float fail_pct;
	private float avg_dur;
	private int min_dur;
	private int max_dur;
	
	public UMBMenuHit() {
	}
	
	public UMBMenuHit(String start_date, String host_id, String short_code, String menu_id, String menu_name, String reason_code, String trans_status, int total_hit,
			int total_success, int total_fail,
			float success_pct,float fail_pct,
			float avg_dur,int min_dur,int max_dur) {
		this.start_date = start_date;		
		this.host_id = host_id;
		this.short_code = short_code;		
		this.menu_id = menu_id;
		this.menu_name = menu_name;
		this.reason_code = reason_code;
		this.trans_status = trans_status;
		this.total_hit = total_hit;		
		this.total_success = total_success;
		this.total_fail = total_fail;
		this.success_pct = success_pct;
		this.fail_pct = fail_pct;
		this.avg_dur = avg_dur;
		this.min_dur = min_dur;
		this.max_dur = max_dur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}

	public String getShort_code() {
		return short_code;
	}

	public void setShort_code(String short_code) {
		this.short_code = short_code;
	}

	public String getMenu_id() {
		return menu_id;
	}

	public void setMenu_id(String menu_id) {
		this.menu_id = menu_id;
	}

	public String getMenu_name() {
		return menu_name;
	}

	public void setMenu_name(String menu_name) {
		this.menu_name = menu_name;
	}
	
	public String getReason_code() {
		return reason_code;
	}

	public void setReason_code(String reason_code) {
		this.reason_code = reason_code;
	}
	
	public String getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String trans_status) {
		this.trans_status = trans_status;
	}

	public int getTotal_hit() {
		return total_hit;
	}

	public void setTotal_hit(int total_hit) {
		this.total_hit = total_hit;
	}
	
	public int getTotal_success() {
		return total_success;
	}

	public void setTotal_success(int total_success) {
		this.total_success = total_success;
	}
	
	public int getTotal_fail() {
		return total_fail;
	}

	public void setTotal_fail(int total_fail) {
		this.total_fail = total_fail;
	}

	public float getSuccess_pct() {
		return success_pct;
	}

	public void setSuccess_pct(float success_pct) {
		this.success_pct = success_pct;
	}

	public float getFail_pct() {
		return fail_pct;
	}

	public void setFail_pct(float fail_pct) {
		this.fail_pct = fail_pct;
	}
	
	public float getAvg_dur() {
		return avg_dur;
	}

	public void setAvg_dur(float avg_dur) {
		this.avg_dur = avg_dur;
	}
	
	public int getMin_dur() {
		return min_dur;
	}

	public void setMin_dur(int min_dur) {
		this.min_dur = min_dur;
	}

	public int getMax_dur() {
		return max_dur;
	}

	public void setMax_dur(int max_dur) {
		this.max_dur = max_dur;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Transaction [id=").append(id)
				.append(", start_date=").append(start_date)
				.append(", host_id=").append(host_id)
				.append(", short_code=").append(short_code)
				.append(", menu_id=").append(menu_id)
				.append(", menu_name=").append(menu_name)				
				.append(", reason_code=").append(reason_code)
				.append(", trans_status=").append(trans_status)
				.append(", total_hit=").append(total_hit)
				.append(", total_success=").append(total_success)
				.append(", total_fail=").append(total_fail)
				.append(", success_pct=").append(success_pct)
				.append(", fail_pct=").append(fail_pct)
				.append(", avg_dur=").append(avg_dur)
				.append(", min_dur=").append(min_dur)
				.append(", max_dur=").append(max_dur)
				.append("]");
		return builder.toString();
	}
}
