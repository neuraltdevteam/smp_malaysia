package com.neuralt.blc.report.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SMS_OUT_BY_SMPPID")
public class SmsOutBySmppId implements Serializable {

	private static final long serialVersionUID = 803526171999253395L;
	@Id
	@GeneratedValue
	private Long id;
	private String start_date;
	private String host_id;
	private String mdr_prefix;
	private int total_tx;

	public SmsOutBySmppId() {
	}

	public SmsOutBySmppId(Long id, String start_date, String host_id,
			String mdr_prefix, int total_tx) {
		this.id = id;
		this.start_date = start_date;
		this.host_id = host_id;
		this.mdr_prefix = mdr_prefix;
		this.total_tx = total_tx;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getHost_id() {
		return host_id;
	}

	public void setHost_id(String host_id) {
		this.host_id = host_id;
	}

	public String getMdr_prefix() {
		return mdr_prefix;
	}

	public void setMdr_prefix(String mdr_prefix) {
		this.mdr_prefix = mdr_prefix;
	}

	public int getTotal_tx() {
		return total_tx;
	}

	public void setTotal_tx(int total_tx) {
		this.total_tx = total_tx;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("SmsOut [id=").append(id).append(", start_date=")
				.append(start_date).append(", host_id=").append(host_id)
				.append(", mdr_prefix=").append(mdr_prefix)
				.append(", total_tx=").append(total_tx).append("]");
		return builder.toString();
	}

}
