package com.neuralt.blc.report.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "TRANS_SUMMARY")
public class TransactionSummary implements Serializable {

	private static final long serialVersionUID = 3589103961469466832L;
	//private static final long serialVersionUID = -3256072599320969706L;

	@Id
	@GeneratedValue
	private Long id;
	private Timestamp txdate;
	private String API;
	private int total_trans;
	private int total_success;
	private int total_fail;
	private float success_pct;
	private float fail_pct;	
	private Timestamp avg_duration;
	private Timestamp max_duration;
	private Timestamp min_duration;
	private Timestamp IQR;
	
	public TransactionSummary() {
	}

	public TransactionSummary(Timestamp txdate, String API,
			int total_trans,
			Timestamp avg_duration, Timestamp max_duration, Timestamp min_duration) {
		this.txdate = txdate;
		this.API = API;
		this.total_trans = total_trans;
		this.avg_duration = avg_duration;
		this.max_duration = max_duration;
		this.min_duration = min_duration;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Timestamp getTxdate() {
		return txdate;
	}

	public void setTxdate(Timestamp txdate) {
		this.txdate = txdate;
	}

	public String getAPI() {
		return API;
	}

	public void setAPI(String API) {
		this.API = API;
	}

	public int getTotal_trans() {
		return total_trans;
	}
	
	public void setTotal_trans(int total_trans) {
		this.total_trans = total_trans;
	}

	public int getTotal_success() {
		return total_success;
	}

	public void setTotal_success(int total_success) {
		this.total_success = total_success;
	}
	
	public int getTotal_fail() {
		return total_fail;
	}

	public void setTotal_fail(int total_fail) {
		this.total_fail = total_fail;
	}
	
	public float getSuccess_pct() {
		return success_pct;
	}

	public void setSuccess_pct(float success_pct) {
		this.success_pct = success_pct;
	}
	
	public float getFail_pct() {
		return fail_pct;
	}

	public void setFail_pct(float fail_pct) {
		this.fail_pct = fail_pct;
	}
	
	public Timestamp getAvg_duration() {
		return avg_duration;
	}

	public void setAvg_duration(Timestamp avg_duration) {
		this.avg_duration = avg_duration;
	}

	public Timestamp getMax_duration() {
		return max_duration;
	}

	public void setMax_duration(Timestamp max_duration) {
		this.max_duration = max_duration;
	}

	public Timestamp getMin_duration() {
		return min_duration;
	}

	public void setMin_duration(Timestamp min_duration) {
		this.min_duration = min_duration;
	}

	public Timestamp getIQR() {
		return IQR;
	}

	public void setIQR(Timestamp IQR) {
		this.IQR = IQR;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TransactionLog [id=").append(id)
				.append(", txdate=").append(txdate)
				.append(", API=").append(API)
				.append(", total_trans=").append(total_trans)				
				.append(", avg_duration=").append(avg_duration)
				.append(", max_duration=").append(max_duration)
				.append(", min_duration=").append(min_duration).append("]");
		return builder.toString();
	}
}
