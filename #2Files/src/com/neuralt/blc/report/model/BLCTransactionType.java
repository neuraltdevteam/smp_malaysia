package com.neuralt.blc.report.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BLC_TXN_TYPE")
public class BLCTransactionType implements Serializable {
	
	private static final long serialVersionUID = -9129277363334021948L;
	@Id
	@GeneratedValue
	private Long id;
	private String start_date;	
	private String trans_type;
	private String trans_status;
	private int total_trans;
	private float percentage;
	private int total_success;
	private int total_fail;
	private float success_pct;
	private float fail_pct;
	private float avg_dur;
	private int min_dur;
	private int max_dur;
	
	public BLCTransactionType() {
	}

	public BLCTransactionType(String start_date, String trans_type, String trans_status, int total_trans,
			float percentage, int total_success, int total_fail,
			float success_pct,float fail_pct,
			float avg_dur,int min_dur,int max_dur) {
		this.start_date = start_date;
		this.trans_type = trans_type;
		this.trans_status = trans_status;
		this.total_trans = total_trans;
		this.percentage = percentage;
		this.total_success = total_success;
		this.total_fail = total_fail;
		this.success_pct = success_pct;
		this.fail_pct = fail_pct;
		this.avg_dur = avg_dur;
		this.min_dur = min_dur;
		this.max_dur = max_dur;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getTrans_type() {
		return trans_type;
	}

	public void setTrans_type(String trans_type) {
		this.trans_type = trans_type;
	}
	
	public String getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String trans_status) {
		this.trans_status = trans_status;
	}

	public int getTotal_trans() {
		return total_trans;
	}

	public void setTotal_trans(int total_trans) {
		this.total_trans = total_trans;
	}

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	
	public int getTotal_success() {
		return total_success;
	}

	public void setTotal_success(int total_success) {
		this.total_success = total_success;
	}
	
	public int getTotal_fail() {
		return total_fail;
	}

	public void setTotal_fail(int total_fail) {
		this.total_fail = total_fail;
	}

	public float getSuccess_pct() {
		return success_pct;
	}

	public void setSuccess_pct(float success_pct) {
		this.success_pct = success_pct;
	}

	public float getFail_pct() {
		return fail_pct;
	}

	public void setFail_pct(float fail_pct) {
		this.fail_pct = fail_pct;
	}
	
	public float getAvg_dur() {
		return avg_dur;
	}

	public void setAvg_dur(float avg_dur) {
		this.avg_dur = avg_dur;
	}
	
	public int getMin_dur() {
		return min_dur;
	}

	public void setMin_dur(int min_dur) {
		this.min_dur = min_dur;
	}

	public int getMax_dur() {
		return max_dur;
	}

	public void setMax_dur(int max_dur) {
		this.max_dur = max_dur;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Transaction [id=").append(id)
				.append(", start_date=").append(start_date)
				.append(", trans_type=").append(trans_type)
				.append(", trans_status=").append(trans_status)
				.append(", total_trans=").append(total_trans)
				.append(", percentage=").append(percentage)
				.append(", total_success=").append(total_success)
				.append(", total_fail=").append(total_fail)
				.append(", success_pct=").append(success_pct)
				.append(", fail_pct=").append(fail_pct)
				.append(", avg_dur=").append(avg_dur)
				.append(", min_dur=").append(min_dur)
				.append(", max_dur=").append(max_dur)
				.append("]");
		return builder.toString();
	}
}
