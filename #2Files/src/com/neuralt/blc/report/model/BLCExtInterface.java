package com.neuralt.blc.report.model;

import java.io.Serializable;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "BLC_EXT_INTERFACE")
public class BLCExtInterface implements Serializable {

	private static final long serialVersionUID = 3770795657839444956L;
	@Id
	@GeneratedValue
	private Long id;
	private String start_date;
	private String api_name;
	private String trans_status;
	private String reason_code;
	private String channel;
	private int total_trans;
	private int total_success;
	private int total_fail;
	private float success_pct;
	private float fail_pct;
	private float avg_dur;
	private int min_dur;
	private int max_dur;

	public BLCExtInterface() {
	}

	public BLCExtInterface(Long id, String start_date, String api_name,
			String trans_status, String reason_code, String channel, int total_trans,
			int total_success, int total_fail, float success_pct,
			float fail_pct, float avg_dur, int min_dur, int max_dur) {
		this.id = id;
		this.start_date = start_date;
		this.api_name = api_name;
		this.trans_status = trans_status;
		this.reason_code = reason_code;
		this.channel = channel;
		this.total_trans = total_trans;
		this.total_success = total_success;
		this.total_fail = total_fail;
		this.success_pct = success_pct;
		this.fail_pct = fail_pct;
		this.avg_dur = avg_dur;
		this.min_dur = min_dur;
		this.max_dur = max_dur;
	}

	public Long getId() {
		return id;
	}

	public String getStart_date() {
		return start_date;
	}

	public String getApi_name() {
		return api_name;
	}

	public String getTrans_status() {
		return trans_status;
	}

	public String getReason_code() {
		return reason_code;
	}

	public String getChannel() {
		return channel;
	}
	
	public int getTotal_trans() {
		return total_trans;
	}

	public int getTotal_success() {
		return total_success;
	}

	public int getTotal_fail() {
		return total_fail;
	}

	public float getSuccess_pct() {
		return success_pct;
	}

	public float getFail_pct() {
		return fail_pct;
	}

	public float getAvg_dur() {
		return avg_dur;
	}

	public int getMin_dur() {
		return min_dur;
	}

	public int getMax_dur() {
		return max_dur;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public void setApi_name(String api_name) {
		this.api_name = api_name;
	}

	public void setTrans_status(String trans_status) {
		this.trans_status = trans_status;
	}

	public void setReason_code(String reason_code) {
		this.reason_code = reason_code;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}
	
	public void setTotal_trans(int total_trans) {
		this.total_trans = total_trans;
	}

	public void setTotal_success(int total_success) {
		this.total_success = total_success;
	}

	public void setTotal_fail(int total_fail) {
		this.total_fail = total_fail;
	}

	public void setSuccess_pct(float success_pct) {
		this.success_pct = success_pct;
	}

	public void setFail_pct(float fail_pct) {
		this.fail_pct = fail_pct;
	}

	public void setAvg_dur(float avg_dur) {
		this.avg_dur = avg_dur;
	}

	public void setMin_dur(int min_dur) {
		this.min_dur = min_dur;
	}

	public void setMax_dur(int max_dur) {
		this.max_dur = max_dur;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("BLCExtInterface [id=").append(id)
				.append(", start_date=").append(start_date)
				.append(", api_name=").append(api_name)
				.append(", trans_status=").append(trans_status)
				.append(", reason_code=").append(reason_code)
				.append(", channel=").append(channel)
				.append(", total_trans=").append(total_trans)
				.append(", total_success=").append(total_success)
				.append(", total_fail=").append(total_fail)
				.append(", success_pct=").append(success_pct)
				.append(", fail_pct=").append(fail_pct).append(", avg_dur=")
				.append(avg_dur).append(", min_dur=").append(min_dur)
				.append(", max_dur=").append(max_dur).append("]");
		return builder.toString();
	}

}
