package com.neuralt.blc.report.data.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.neuralt.blc.report.model.API;
import com.neuralt.smp.client.data.dao.GenericDAO;

public class ApiDAO {

	public static List<API> getAPINameByDate(String searchFrom, String searchTo) {

		GenericDAO<API, Integer> dao = new GenericDAO<API, Integer>(API.class,
				false);

		String strSQL = "select Distinct api_name from BLC_EXT_INTERFACE_STAT";
		if (searchFrom != "" && searchTo != "") {
			strSQL = strSQL + " WHERE tx_hour between '" + searchFrom
					+ " 00:00:00' AND '" + searchTo + " 23:59:59' ";
		}
		List<API> results = dao.findByCriteriaBySQL(strSQL, null, null);

		return results;
	}
}
