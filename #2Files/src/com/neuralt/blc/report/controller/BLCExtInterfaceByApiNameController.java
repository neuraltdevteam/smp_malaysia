package com.neuralt.blc.report.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.neuralt.smp.client.data.dao.GenericDAO;
import com.neuralt.blc.report.model.API;
import com.neuralt.blc.report.model.BLCExtInterface;

import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class BLCExtInterfaceByApiNameController implements Serializable {

	private static final long serialVersionUID = 7479762120242911789L;
	private Date searchFrom;
	private Date searchTo;
	private String reportmode = "";
	private int rowcount = 0;

	private static String[] chartdates;
	private static String[] api_name;
	private static String[] channel;
	private static String[] trans_status;
	private static String[] reason_code;
	private static int[] totaltransdata;
	private static int[] totalsuccessdata;
	private static int[] totalfaildata;
	private static float[] successpctdata;
	private static float[] failpctdata;
	private static int[] mindurdata;
	private static int[] maxdurdata;
	private static float[] avgdurdata;

	private LazyDataModel<BLCExtInterface> BLCExtInterface;
	private static boolean resultsVisible = false;

	private String[] columnTitles = { "trans.API", "channel.channel", "trans.Date",
			"trans.totalTrans", "trans.totalSuccess", "trans.totalFail",
			"trans.successPct", "trans.failPct", "trans.avgDuration",
			"trans.maxDuration", "trans.minDuration" };
	private String cvsFilePrefix = "";
	private String reportStr = "", reportHeaderTitle="";
	public List<API> API = new ArrayList<API>();

	public BLCExtInterfaceByApiNameController() {
		resultsVisible = false;
	}

	public void getreportSetting() {
		String reportTitle = "";
		String reportHeader = "";

		FacesContext cs = FacesContext.getCurrentInstance();
		ResourceBundle bundle = cs.getApplication()
				.getResourceBundle(cs, "msg");

		cvsFilePrefix = bundle
				.getString("blc_ext_int_by_api_name.csv.FilePrefix");
		reportTitle = bundle
				.getString("blc_ext_int_by_api_name.csv.ReportTitle");

		for (int i = 0; i < columnTitles.length; ++i) {
			reportHeader = reportHeader + bundle.getString(columnTitles[i])
					+ ",";
		}

		reportHeaderTitle = reportTitle + "\r\n" + "\r\n" + reportHeader + "\r\n";
	}

	public String getCvsFilePrefix() {
		return cvsFilePrefix;
	}

	public String getReportStr() {
		return reportStr;
	}

	public void searchTransactions() {
		getreportSetting();
		BLCExtInterfaceController.setResultsVisible(false);
		;

		// System.out.println("in searchTransactions");
		BLCExtInterface = new TransactionModel();

		int rowcount = this.getDataRowcount();
		System.out.println("in searchTransactions; rowcount=" + rowcount);
		BLCExtInterface.setRowCount(rowcount);

		resultsVisible = true;
	}

	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date in_searchFrom) {
		searchFrom = in_searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date in_searchTo) {
		searchTo = in_searchTo;
	}

	public String getReportmode() {
		return reportmode;
	}

	public void setReportmode(String reportmode) {
		this.reportmode = reportmode;
	}

	public int getRowcount() {
		return rowcount;
	}

	public void setRowcount(int rowcount) {
		this.rowcount = rowcount;
	}

	public static String[] getChartdates() {
		return chartdates;
	}

	public void setChartdates(String[] chartdates) {
		this.chartdates = chartdates;
	}

	public static String[] getApi_name() {
		return api_name;
	}

	public void setApi_name(String[] api_name) {
		this.api_name = api_name;
	}

	public static String[] getChannel() {
		return channel;
	}

	public void setChannel(String[] channel) {
		this.channel = channel;
	}
	
	public static String[] getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String[] trans_status) {
		this.trans_status = trans_status;
	}

	public static String[] getReason_code() {
		return reason_code;
	}

	public void setReason_code(String[] reason_code) {
		this.reason_code = reason_code;
	}

	public static int[] getTotaltransdata() {
		return totaltransdata;
	}

	public void setTotaltransdata(int[] totaltransdata) {
		this.totaltransdata = totaltransdata;
	}

	public static int[] getTotalsuccessdata() {
		return totalsuccessdata;
	}

	public static int[] getTotalfaildata() {
		return totalfaildata;
	}

	public static float[] getSuccesspctdata() {
		return successpctdata;
	}

	public static float[] getFailpctdata() {
		return failpctdata;
	}

	public static int[] getMindurdata() {
		return mindurdata;
	}

	public static int[] getMaxdurdata() {
		return maxdurdata;
	}

	public static float[] getAvgdurdata() {
		return avgdurdata;
	}

	public LazyDataModel<BLCExtInterface> getBLCExtInterface() {
		return BLCExtInterface;
	}

	public boolean isResultsVisible() {
		return resultsVisible;
	}

	public static boolean getResultsVisible() {
		return resultsVisible;
	}

	public static void setResultsVisible(boolean in_resultsVisible) {
		resultsVisible = in_resultsVisible;
	}

	public int getDataRowcount() {

		// System.out.println("getDataRowcount");

		GenericDAO<BLCExtInterface, Integer> dao = new GenericDAO<BLCExtInterface, Integer>(
				BLCExtInterface.class, false);

		searchFrom = getSearchFromParam();
		searchTo = getSearchToParam();
		reportmode = getReportModeParam();

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";
		String strSQL = "call sp_report_blc_ext_interface(?,?,?,?,?,?,?)";
		String strSQLWhere = " ";
		String strSQLGroupBy = "start_date, api_name";
		String strSQLOrderBy = " ";
		String additional_condition = "";

		if (searchFrom != null && searchTo != null) {
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}

		/*
		 * if (searchFrom != null && searchTo != null) strSQLWhere =
		 * "date(start_date) >= date('" + strsearchFrom +
		 * "') and date(start_date) <= date('" + strsearchTo + "')";
		 */

		/*
		 * if (trans_type != null) { if (trans_type.compareTo("") != 0 &&
		 * trans_type.compareToIgnoreCase("all") != 0) { if
		 * (strSQLWhere.compareTo("") != 0) strSQLWhere = strSQLWhere + " AND ";
		 * 
		 * strSQLWhere = strSQLWhere + " trans_type = '" + trans_type + "' "; }
		 * }
		 */

		/*
		 * if (filters != null && !filters.isEmpty()) {
		 * 
		 * for (Map.Entry<String, String> entry : filters.entrySet()) {
		 * 
		 * if (strSQLWhere.compareTo("") != 0) strSQLWhere = strSQLWhere +
		 * " AND ";
		 * 
		 * strSQLWhere = entry.getKey() + " like '%" + entry.getValue() + "%'";
		 * //System.out.println(entry.getKey() + " like " + entry.getValue()); }
		 * }
		 */

		System.out.println("strSQLWhere=" + strSQLWhere);
		System.out.println("strSQLGroupBy=" + strSQLGroupBy);
		System.out.println("strSQLOrderBy=" + strSQLOrderBy);

		List<BLCExtInterface> results = dao.findReportByCriteriaBySQLSP(
				reportmode, strSQL, strsearchFrom, strsearchTo, strSQLGroupBy,
				strSQLOrderBy, null, null, additional_condition);
		System.out.println("results.size()=" + results.size());

		return results.size();

	}

	//
	//
	private class TransactionModel extends LazyDataModel<BLCExtInterface> {
		//
		private static final long serialVersionUID = 1L;
		private GenericDAO<BLCExtInterface, Integer> dao = new GenericDAO<BLCExtInterface, Integer>(
				BLCExtInterface.class, false);

		@Override
		public List<BLCExtInterface> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {

			searchFrom = getSearchFromParam();
			searchTo = getSearchToParam();
			reportmode = getReportModeParam();

			System.out
					.println("in BLCExtInterfaceByApiNameController.TransactionModel");
			System.out.println("searchFrom=" + searchFrom);
			System.out.println("searchTo=" + searchTo);
			System.out.println("reportmode=" + reportmode);

			/*
			 * System.out.println("in TransactionModel");
			 * System.out.println("first="+first);
			 * System.out.println("pageSize="+pageSize);
			 * System.out.println("sortField="+sortField);
			 * System.out.println("sortOrder="+sortOrder);
			 */

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";
			String strSQL = "call sp_report_blc_ext_interface(?,?,?,?,?,?,?)";
			String strSQLWhere = " ";
			String strSQLGroupBy = "start_date, api_name";
			String strSQLOrderBy = " ";
			String additional_condition = "";

			if (searchFrom != null && searchTo != null) {
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
			}

			/*
			 * if (searchFrom != null && searchTo != null) strSQLWhere =
			 * "date(start_date) >= date('" + strsearchFrom +
			 * "') and date(start_date) <= date('" + strsearchTo + "')";
			 */

			/*
			 * if (trans_type != null) { if (trans_type.compareTo("") != 0 &&
			 * trans_type.compareToIgnoreCase("all") != 0) { if
			 * (strSQLWhere.compareTo("") != 0) strSQLWhere = strSQLWhere +
			 * " AND ";
			 * 
			 * strSQLWhere = strSQLWhere + " trans_type = '" + trans_type +
			 * "' "; } }
			 */

			/*
			 * if (filters != null && !filters.isEmpty()) {
			 * 
			 * for (Map.Entry<String, String> entry : filters.entrySet()) {
			 * 
			 * if (strSQLWhere.compareTo("") != 0) strSQLWhere = strSQLWhere +
			 * " AND ";
			 * 
			 * strSQLWhere = entry.getKey() + " like '%" + entry.getValue() +
			 * "%'"; //System.out.println(entry.getKey() + " like " +
			 * entry.getValue()); } }
			 */

			if (sortField != null) {

				strSQLOrderBy = sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";

			}

			String strSQLGroupBy1 = "api_name, start_date";
			//String strSQLOrderBy1 = "start_date, api_name"; //YM - 20151019
			String strSQLOrderBy1 = "api_name, start_date";
			System.out.println("strSQLGroupBy1=" + strSQLGroupBy1);
			System.out.println("strSQLOrderBy1=" + strSQLOrderBy1);

			List<BLCExtInterface> results = dao.findReportByCriteriaBySQLSP(
					reportmode, strSQL, strsearchFrom, strsearchTo,
					strSQLGroupBy1, strSQLOrderBy1, null, null,
					additional_condition);
			rowcount = results.size();
			System.out.println("rowcount=" + rowcount);

			int i = 0;
			chartdates = null;
			totaltransdata = null;
			api_name = new String[results.size()];
			channel = new String[results.size()];
			trans_status = new String[results.size()];
			reason_code = new String[results.size()];
			chartdates = new String[results.size()];
			totaltransdata = new int[results.size()];
			totalsuccessdata = new int[results.size()];
			totalfaildata = new int[results.size()];
			successpctdata = new float[results.size()];
			failpctdata = new float[results.size()];
			mindurdata = new int[results.size()];
			maxdurdata = new int[results.size()];
			avgdurdata = new float[results.size()];

			reportStr = reportHeaderTitle;
			
			for (BLCExtInterface entity : results) {
				api_name[i] = entity.getApi_name() + "";
				channel[i] = entity.getChannel() + "";
				trans_status[i] = entity.getTrans_status() + "";
				reason_code[i] = entity.getReason_code() + "";
				chartdates[i] = entity.getStart_date() + "";
				totaltransdata[i] = entity.getTotal_trans();
				totalsuccessdata[i] = entity.getTotal_success();
				totalfaildata[i] = entity.getTotal_fail();
				successpctdata[i] = entity.getSuccess_pct();
				failpctdata[i] = entity.getFail_pct();
				mindurdata[i] = entity.getMin_dur();
				maxdurdata[i] = entity.getMax_dur();
				avgdurdata[i] = entity.getAvg_dur();

				reportStr = reportStr + entity.getApi_name() + ",";
				reportStr = reportStr + entity.getChannel() + ",";
				reportStr = reportStr + entity.getStart_date() + ",";
				reportStr = reportStr + entity.getTotal_trans() + ",";
				reportStr = reportStr + entity.getTotal_success() + ",";
				reportStr = reportStr + entity.getTotal_fail() + ",";
				reportStr = reportStr + entity.getSuccess_pct() + ",";
				reportStr = reportStr + entity.getFail_pct() + ",";
				reportStr = reportStr + entity.getAvg_dur() + ",";
				reportStr = reportStr + entity.getMax_dur() + ",";
				reportStr = reportStr + entity.getMin_dur() + "," + "\r\n";
				++i;
			}

			System.out.println("strSQLGroupBy=" + strSQLGroupBy);
			System.out.println("strSQLOrderBy=" + strSQLOrderBy);
			
			List<BLCExtInterface> results1 = dao.findReportByCriteriaBySQLSP(
					reportmode, strSQL, strsearchFrom, strsearchTo,
					strSQLGroupBy, strSQLOrderBy, first, pageSize,
					additional_condition);
			
			return results1;

		}

	}

	public Date getSearchFromParam() {
		Date retDate = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String strDate = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:searchFrom_input");

		System.out.println("getSearchFromParam strDate=" + strDate);

		DateFormat format = new SimpleDateFormat("M/d/yy");
		try {
			retDate = format.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strDate1 = sm.format(retDate);
		System.out.println("getSearchFromParam strDate1=" + strDate1);

		return retDate;
	}

	public Date getSearchToParam() {
		Date retDate = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String strDate = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:searchTo_input");

		System.out.println("getSearchToParam strDate=" + strDate);

		DateFormat format = new SimpleDateFormat("M/d/yy");
		try {
			retDate = format.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strDate1 = sm.format(retDate);
		System.out.println("getSearchToParam strDate1=" + strDate1);

		return retDate;
	}

	public String getReportModeParam() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String resultStr = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:reportMode_input");

		System.out.println("resultStr=" + resultStr);
		return resultStr;
	}

}
