package com.neuralt.blc.report.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.neuralt.blc.report.data.dao.ApiDAO;
import com.neuralt.blc.report.model.BLCExtInterfaceByReasonCode;
import com.neuralt.blc.report.model.API;
import com.neuralt.smp.client.data.dao.GenericDAO;

import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

@ManagedBean
@ViewScoped
public class BLCExtInterfaceByReasonCodeController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3200250903302406860L;
	private Date searchFrom;
	private Date searchTo;
	private String reportmode = "";
	private String reportApi = "";
	private int rowcount = 0;
	private API api;
	private List<API> ApiList = new ArrayList<API>();
	private List<SelectItem> apiSelectItems = new ArrayList<SelectItem>();
	public API selectedApi = new API();
	private String selectItem = "";

	private static String[] chartdates;
	private static String[] api_name;
	private static String[] trans_status;
	private static String[] reason_code;
	private static int[] totaltransdata;
	private static float[] pctdata;

	private LazyDataModel<BLCExtInterfaceByReasonCode> BLCExtInterfaceByReasonCode;
	private static boolean resultsVisible = false;

	private String[] columnTitles = { "trans.API","trans.Date", 
			"trans.ReasonCode", "trans.totalTrans", "trans.Pct" };
	private String cvsFilePrefix = "";
	private String reportStr = "", reportHeaderTitle="";
	public List<API> API = new ArrayList<API>();

	public BLCExtInterfaceByReasonCodeController() {
		getAllApi();
		resultsVisible = false;
	}

	public void getreportSetting() {
		String reportTitle = "";
		String reportHeader = "";

		FacesContext cs = FacesContext.getCurrentInstance();
		ResourceBundle bundle = cs.getApplication()
				.getResourceBundle(cs, "msg");

		cvsFilePrefix = bundle
				.getString("blc_ext_int_by_reason_code.csv.FilePrefix");
		reportTitle = bundle
				.getString("blc_ext_int_by_reason_code.csv.ReportTitle");

		for (int i = 0; i < columnTitles.length; ++i) {
			reportHeader = reportHeader + bundle.getString(columnTitles[i])
					+ ",";
		}

		reportHeaderTitle = reportTitle + "\r\n" + "\r\n" + reportHeader + "\r\n";
	}

	public String getCvsFilePrefix() {
		return cvsFilePrefix;
	}

	public String getReportStr() {
		return reportStr;
	}

	

	public String getSelectItem() {
		return selectItem;
	}

	public void setSelectItem(String selectItem) {
		this.selectItem = selectItem;
	}

	public void getAllApi() {
		api = new API();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String strFromDate = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:searchFrom_input");
		String strToDate = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:searchTo_input");
		Date searchFrom = null;
		Date searchTo = null;
		String strsearchFrom="";
		String strsearchTo ="";
		if ((strFromDate != null && strFromDate != "") && (strToDate != null && strToDate != "")) {
			searchFrom = getSearchFromParam();
			searchTo = getSearchToParam();
			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}
		
		ApiList = ApiDAO.getAPINameByDate(strsearchFrom, strsearchTo);
		apiSelectItems = new ArrayList<SelectItem>();
		for (API api : ApiList) {
			apiSelectItems.add(new SelectItem(api.getApi_name(), api
					.getApi_name()));
		}
	}

	public API getSelectedApi() {
		return selectedApi;
	}

	public void setSelectedApi(API selectedApi) {
		this.selectedApi = selectedApi;
	}

	public List<SelectItem> getApiSelectItems() {
		return apiSelectItems;
	}

	public void setApiSelectItems(List<SelectItem> apiSelectItems) {
		this.apiSelectItems = apiSelectItems;
	}

	public void searchTransactions() {
		getAllApi();
		getreportSetting();
		BLCExtInterfaceController.setResultsVisible(false);

		// System.out.println("in searchTransactions");
		BLCExtInterfaceByReasonCode = new TransactionModel();
		int rowcount = this.getDataRowcount();
		System.out.println("in searchTransactions; rowcount=" + rowcount);
		BLCExtInterfaceByReasonCode.setRowCount(rowcount);

		resultsVisible = true;
	}

	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date in_searchFrom) {
		searchFrom = in_searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date in_searchTo) {
		searchTo = in_searchTo;
	}

	public String getReportmode() {
		return reportmode;
	}

	public void setReportmode(String reportmode) {
		this.reportmode = reportmode;
	}

	public String getReportApi() {
		return reportApi;
	}

	public void setReportApi(String reportApi) {
		this.reportApi = reportApi;
	}

	public int getRowcount() {
		return rowcount;
	}

	public void setRowcount(int rowcount) {
		this.rowcount = rowcount;
	}

	public static String[] getChartdates() {
		return chartdates;
	}

	public void setChartdates(String[] chartdates) {
		this.chartdates = chartdates;
	}

	public static String[] getApi_name() {
		return api_name;
	}

	public void setApi_name(String[] api_name) {
		this.api_name = api_name;
	}

	public static String[] getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String[] trans_status) {
		this.trans_status = trans_status;
	}

	public static String[] getReason_code() {
		return reason_code;
	}

	public void setReason_code(String[] reason_code) {
		this.reason_code = reason_code;
	}

	public static int[] getTotaltransdata() {
		return totaltransdata;
	}

	public void setTotaltransdata(int[] totaltransdata) {
		this.totaltransdata = totaltransdata;
	}

	public static float[] getPctdata() {
		return pctdata;
	}

	public LazyDataModel<BLCExtInterfaceByReasonCode> getBLCExtInterface() {
		return BLCExtInterfaceByReasonCode;
	}

	public boolean isResultsVisible() {
		return resultsVisible;
	}

	public static boolean getResultsVisible() {
		return resultsVisible;
	}

	public static void setResultsVisible(boolean in_resultsVisible) {
		resultsVisible = in_resultsVisible;
	}

	public int getDataRowcount() {

		// System.out.println("getDataRowcount");

		GenericDAO<BLCExtInterfaceByReasonCode, Integer> dao = new GenericDAO<BLCExtInterfaceByReasonCode, Integer>(
				BLCExtInterfaceByReasonCode.class, false);

		searchFrom = getSearchFromParam();
		searchTo = getSearchToParam();
		reportmode = getReportModeParam();
		reportApi = getApiParam();
		System.out.println("reportApi=" + reportApi);
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";
		String strSQL = "call sp_report_blc_ext_interface_By_ReasonCode(?,?,?,?,?,?,?)";
		String strSQLWhere = " ";
		String strSQLGroupBy = "start_date,api_name,reason_code";
		String strSQLOrderBy = "";
		String additional_condition = "'" + reportApi + "'###";

		if (searchFrom != null && searchTo != null) {
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}

		System.out.println("additional_condition=" + additional_condition);
		System.out.println("strSQLWhere=" + strSQLWhere);
		System.out.println("strSQLGroupBy=" + strSQLGroupBy);
		System.out.println("strSQLOrderBy=" + strSQLOrderBy);

		List<BLCExtInterfaceByReasonCode> results = dao
				.findReportByCriteriaBySQLSP(reportmode, strSQL, strsearchFrom,
						strsearchTo, strSQLGroupBy, strSQLOrderBy, null, null,
						additional_condition);
		System.out.println("results.size()=" + results.size());

		return results.size();

	}

	private class TransactionModel extends
			LazyDataModel<BLCExtInterfaceByReasonCode> {
		//
		private static final long serialVersionUID = 1L;
		private GenericDAO<BLCExtInterfaceByReasonCode, Integer> dao = new GenericDAO<BLCExtInterfaceByReasonCode, Integer>(
				BLCExtInterfaceByReasonCode.class, false);

		@Override
		public List<BLCExtInterfaceByReasonCode> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {

			searchFrom = getSearchFromParam();
			searchTo = getSearchToParam();
			reportmode = getReportModeParam();
			reportApi = getApiParam();

			System.out
					.println("in BLCExtInterfaceByReasonCodeController.TransactionModel");
			System.out.println("searchFrom=" + searchFrom);
			System.out.println("searchTo=" + searchTo);
			System.out.println("reportmode=" + reportmode);
			System.out.println("reportApi=" + reportApi);

			/*
			 * System.out.println("in TransactionModel");
			 * System.out.println("first="+first);
			 * System.out.println("pageSize="+pageSize);
			 * System.out.println("sortField="+sortField);
			 * System.out.println("sortOrder="+sortOrder);
			 */

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";
			String strSQL = "call sp_report_blc_ext_interface_By_ReasonCode(?,?,?,?,?,?,?)";
			String strSQLWhere = " ";
			String strSQLGroupBy = "start_date, api_name,reason_code";
			String strSQLOrderBy = "";
			String additional_condition = "'" + reportApi + "'###";

			if (searchFrom != null && searchTo != null) {
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
			}

			if (sortField != null) {

				strSQLOrderBy = sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";

			}

			String strSQLGroupBy1 = "reason_code,api_name,start_date";
			String strSQLOrderBy1 = "reason_code,api_name,start_date";
			System.out.println("strSQLGroupBy1=" + strSQLGroupBy1);
			System.out.println("strSQLOrderBy1=" + strSQLOrderBy1);

			List<BLCExtInterfaceByReasonCode> results = dao
					.findReportByCriteriaBySQLSP(reportmode, strSQL,
							strsearchFrom, strsearchTo, strSQLGroupBy1,
							strSQLOrderBy1, null, null, additional_condition);
			rowcount = results.size();
			System.out.println("rowcount=" + rowcount);

			int i = 0;
			chartdates = null;
			totaltransdata = null;
			api_name = new String[results.size()];
			trans_status = new String[results.size()];
			reason_code = new String[results.size()];
			chartdates = new String[results.size()];
			totaltransdata = new int[results.size()];
			pctdata = new float[results.size()];

			reportStr = reportHeaderTitle;
			
			for (BLCExtInterfaceByReasonCode entity : results) {
				api_name[i] = entity.getApi_name() + "";
				trans_status[i] = entity.getTrans_status() + "";
				reason_code[i] = entity.getReason_code() + "";
				chartdates[i] = entity.getStart_date() + "";
				totaltransdata[i] = entity.getTotal_count();
				pctdata[i] = entity.getPercentage();

				reportStr = reportStr + entity.getApi_name() + ",";
				reportStr = reportStr + entity.getStart_date() + ",";
				reportStr = reportStr + entity.getReason_code() + ",";
				reportStr = reportStr + entity.getTotal_count() + ",";
				reportStr = reportStr + entity.getPercentage() + "," + "\r\n";
				++i;
			}

			System.out.println("strSQLGroupBy=" + strSQLGroupBy);
			System.out.println("strSQLOrderBy=" + strSQLOrderBy);

			List<BLCExtInterfaceByReasonCode> results1 = dao
					.findReportByCriteriaBySQLSP(reportmode, strSQL,
							strsearchFrom, strsearchTo, strSQLGroupBy,
							strSQLOrderBy, first, pageSize,
							additional_condition);
			System.out.println(results1);
			return results1;

		}

	}

	public Date getSearchFromParam() {
		Date retDate = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String strDate = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:searchFrom_input");

		System.out.println("getSearchFromParam strDate=" + strDate);

		DateFormat format = new SimpleDateFormat("M/d/yy");
		try {
			retDate = format.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strDate1 = sm.format(retDate);
		System.out.println("getSearchFromParam strDate1=" + strDate1);

		return retDate;
	}

	public Date getSearchToParam() {
		Date retDate = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String strDate = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:searchTo_input");

		System.out.println("getSearchToParam strDate=" + strDate);

		DateFormat format = new SimpleDateFormat("M/d/yy");
		try {
			retDate = format.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strDate1 = sm.format(retDate);
		System.out.println("getSearchToParam strDate1=" + strDate1);

		return retDate;
	}

	public String getReportModeParam() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String resultStr = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:reportMode_input");

		System.out.println("resultStr=" + resultStr);
		return resultStr;
	}

	public String getApiParam() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String retApi = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:reportApi_input");
		// String retApi = "SOA";
		System.out.println("ReportApi=" + retApi);
		return retApi;
	}

}
