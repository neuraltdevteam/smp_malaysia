package com.neuralt.blc.report.controller;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.neuralt.smp.client.data.dao.GenericDAO;
import com.neuralt.smp.client.data.dao.RawDAO;
import com.neuralt.blc.report.model.TransactionSummary;


@ManagedBean
@ViewScoped
public class TransactionController implements Serializable {

	private static final long serialVersionUID = -1557319022101948113L;
	//private static final long serialVersionUID = 8604301842618866191L;

	private Date searchFrom;
	private Date searchTo;

	private LazyDataModel<TransactionSummary> transactionSummaries;
	private boolean resultsVisible = false;

	public void searchTransactions() {
		//System.out.println("in searchTransactions");	
		transactionSummaries = new TransactionModel();
		int rowcount = (int) new RawDAO(false).count(buildCriteria());
		System.out.println("in searchTransactions 3 rowcount=" + rowcount);
		transactionSummaries.setRowCount(rowcount);
		//transactionLogs.setRowCount(3);
		resultsVisible = true;
	}

	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date searchFrom) {
		this.searchFrom = searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date searchTo) {
		this.searchTo = searchTo;
	}

	public LazyDataModel<TransactionSummary> getTransactionSummaries() {
		return transactionSummaries;
	}

	public boolean isResultsVisible() {
		return resultsVisible;
	}

	private DetachedCriteria buildCriteria() {
		
		//System.out.println("in buildCriteria");		
						
		DetachedCriteria crit = DetachedCriteria.forClass(TransactionSummary.class);
		if (searchFrom != null)
			crit.add(Property.forName("txdate").ge(searchFrom));
		if (searchTo != null)
			crit.add(Property.forName("txdate").le(searchTo));
		
		
		return crit;
	}

	private class TransactionModel extends LazyDataModel<TransactionSummary> {

		private static final long serialVersionUID = 1L;
		private GenericDAO<TransactionSummary, Integer> dao = new GenericDAO<TransactionSummary, Integer>(
				TransactionSummary.class, false);

		@Override
		public List<TransactionSummary> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {
			
			/*
			System.out.println("in TransactionModel");			
			System.out.println("first="+first);
			System.out.println("pageSize="+pageSize);
			System.out.println("sortField="+sortField);
			System.out.println("sortOrder="+sortOrder);			
			*/
			
			DetachedCriteria criteria = buildCriteria();
			if (sortField != null) {
				criteria.addOrder(sortOrder == SortOrder.ASCENDING ? Order
						.asc(sortField) : Order.desc(sortField));
			}
			if (filters != null && !filters.isEmpty()) {
				for (Map.Entry<String, String> entry : filters.entrySet()) {
					criteria.add(Property.forName(entry.getKey())
							.like(entry.getValue(), MatchMode.ANYWHERE)
							.ignoreCase());
					System.out.println(entry.getKey() + " like "
							+ entry.getValue());
				}
			}
			
			List<TransactionSummary> results = dao.findByCriteria(criteria, first,
					pageSize);
						
			return results;
		}

	}
}
