package com.neuralt.blc.report.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Property;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.neuralt.smp.client.data.dao.GenericDAOBLC;
import com.neuralt.smp.client.data.dao.RawDAOBLC;
import com.neuralt.blc.report.model.UMBSession;

import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class UMBSessionTestController implements Serializable {

	private static final long serialVersionUID = 7147794635114896869L;
	private Date searchFrom;
	private Date searchTo;
	private String reportmode = "";
	private int rowcount = 0;

	private static String[] chartdates;
	private static int[] totaltransdata;
	private static int[] mindurdata;
	private static int[] maxdurdata;
	private static float[] avgdurdata;
	
	private LazyDataModel<UMBSession> UMBSessions;
	private static boolean resultsVisible = false;

	private String[] columnTitles = {"trans.Date", "trans.totalTrans", "trans.avgDuration", "trans.maxDuration", "trans.minDuration"};
	private String cvsFilePrefix = "";
	private String reportStr = "";
	
	public UMBSessionTestController() {		
		resultsVisible = false;
	}

	public void getreportSetting()
	{
		String reportTitle = "";
		String reportHeader = "";
		
		FacesContext cs = FacesContext.getCurrentInstance();
		ResourceBundle bundle = cs.getApplication().getResourceBundle(cs, "msg");
		
		cvsFilePrefix = bundle.getString("umb_sess_overall_stat.csv.FilePrefix");
		reportTitle = bundle.getString("umb_sess_overall_stat.csv.ReportTitle");
		
		for (int i=0; i< columnTitles.length; ++i)
		{
			 reportHeader = reportHeader + bundle.getString(columnTitles[i]) + ",";
		}		        		
		
		reportStr = reportTitle + "\r\n" + "\r\n" + reportHeader + "\r\n";	
	}
	
	public String getCvsFilePrefix() {
		return cvsFilePrefix;
	}
	
	public String getReportStr() {
		return reportStr;
	}
	
	public void searchTransactions() {
		getreportSetting();
		UMBSessionByShortCodeController.setResultsVisible(false);;
		System.out.println("in searchTransactions; cvsFilePrefix=" + cvsFilePrefix);
		System.out.println("in searchTransactions; reportStr=" + reportStr);
		
		//UMBSessionByShortCodeController.setResultsVisible(false);;
		
		//System.out.println("in searchTransactions");	
		UMBSessions = new TransactionModel();
		
		int rowcount = this.getDataRowcount();			
		System.out.println("in searchTransactions; rowcount=" + rowcount);
		UMBSessions.setRowCount(rowcount);
		
		resultsVisible = true;
		
	}

	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date in_searchFrom) {
		searchFrom = in_searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date in_searchTo) {
		searchTo = in_searchTo;
	}
	
	public String getReportmode() {
		return reportmode;
	}

	public void setReportmode(String reportmode) {
		this.reportmode = reportmode;
	}

	public int getRowcount() {
		return rowcount;
	}

	public void setRowcount(int rowcount) {
		this.rowcount = rowcount;
	}
		
	public static String[] getChartdates() {
		return chartdates;
	}

	public  void setChartdates(String[] chartdates) {
		this.chartdates = chartdates;
	}

	public static int[] getTotaltransdata() {
		return totaltransdata;
	}

	public void setTotaltransdata(int[] totaltransdata) {
		this.totaltransdata = totaltransdata;
	}
	
	public static int[] getMindurdata() {
		return mindurdata;
	}

	public static int[] getMaxdurdata() {
		return maxdurdata;
	}

	public static float[] getAvgdurdata() {
		return avgdurdata;
	}
	
	public LazyDataModel<UMBSession> getUMBSessions() {
		return UMBSessions;
	}

	public boolean isResultsVisible() {
		return resultsVisible;
	}

	public static boolean getResultsVisible() {
		return resultsVisible;
	}

	public static void setResultsVisible(boolean in_resultsVisible) {
		resultsVisible = in_resultsVisible;
	}	
	
	public int getDataRowcount() {

		//System.out.println("getDataRowcount");

		GenericDAOBLC<UMBSession, Integer> dao = new GenericDAOBLC<UMBSession, Integer>(
				UMBSession.class, false);
		
		searchFrom = getSearchFromParam();
		searchTo = getSearchToParam();
		reportmode = getReportModeParam();
		
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";
		String strSQL = "call sp_report_umb_sess(?,?,?,?,?,?,?)";
		String strSQLWhere = " ";
		String strSQLGroupBy = "start_date";
		String strSQLOrderBy = " ";
		String additional_condition = "";
		
		if (searchFrom != null && searchTo != null)
		{
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
			//System.out.println("strsearchFrom="  + strsearchFrom);
			//System.out.println("strsearchTo="  + strsearchTo);			
		}
		
		/*
		if (searchFrom != null && searchTo != null)
			strSQLWhere = "date(start_date) >= date('" + strsearchFrom + "') and date(start_date) <= date('" + strsearchTo + "')";	
		*/
		
		/*
		if (trans_type != null)
		{
			if (trans_type.compareTo("") != 0 && trans_type.compareToIgnoreCase("all") != 0)
			{
				if (strSQLWhere.compareTo("") != 0)
					strSQLWhere = strSQLWhere + " AND ";

				strSQLWhere = strSQLWhere + " trans_type = '" + trans_type + "' ";
			}
		}
		*/
		
		/*
		if (filters != null && !filters.isEmpty()) {
							
			for (Map.Entry<String, String> entry : filters.entrySet()) {
				
				if (strSQLWhere.compareTo("") != 0)
					strSQLWhere = strSQLWhere + " AND ";
				
				strSQLWhere = entry.getKey() + " like '%" + entry.getValue() + "%'";					
				//System.out.println(entry.getKey() + " like " + entry.getValue());
			}
		}
		*/
		
		System.out.println("strSQLWhere=" + strSQLWhere);			
		System.out.println("strSQLGroupBy=" + strSQLGroupBy);
		System.out.println("strSQLOrderBy=" + strSQLOrderBy);
							
		List<UMBSession> results = dao.findReportByCriteriaBySQLSP(reportmode, strSQL, strsearchFrom, strsearchTo, strSQLGroupBy, strSQLOrderBy, null, null, additional_condition);
		System.out.println("results.size()="+results.size());	
				
		return results.size();
				
	
	}
	

	private class TransactionModel extends LazyDataModel<UMBSession> {

		private static final long serialVersionUID = 1L;
		private GenericDAOBLC<UMBSession, Integer> dao = new GenericDAOBLC<UMBSession, Integer>(
				UMBSession.class, false);

		@Override
		public List<UMBSession> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {

			searchFrom = getSearchFromParam();
			searchTo = getSearchToParam();
			reportmode = getReportModeParam();
			
			/*
			System.out.println("in TransactionModel");			
			System.out.println("first="+first);
			System.out.println("pageSize="+pageSize);
			System.out.println("sortField="+sortField);
			System.out.println("sortOrder="+sortOrder);			
			*/
			
			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";
			String strSQL = "call sp_report_umb_sess(?,?,?,?,?,?,?)";
			String strSQLWhere = " ";			
			String strSQLGroupBy = "start_date";
			String strSQLOrderBy = " ";
			String additional_condition = "";
			
			if (searchFrom != null && searchTo != null)
			{
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
				System.out.println("strsearchFrom="  + strsearchFrom);
				System.out.println("strsearchTo="  + strsearchTo);
			
			}		
						
			/*
			if (searchFrom != null && searchTo != null)
				strSQLWhere = "date(start_date) >= date('" + strsearchFrom + "') and date(start_date) <= date('" + strsearchTo + "')"; 
			*/
			
			/*
			if (trans_type != null)
			{
				if (trans_type.compareTo("") != 0 && trans_type.compareToIgnoreCase("all") != 0)
				{
					if (strSQLWhere.compareTo("") != 0)
						strSQLWhere = strSQLWhere + " AND ";

					strSQLWhere = strSQLWhere + " trans_type = '" + trans_type + "' ";
				}
			}
			*/
			
			/*
			if (filters != null && !filters.isEmpty()) {
								
				for (Map.Entry<String, String> entry : filters.entrySet()) {
					
					if (strSQLWhere.compareTo("") != 0)
						strSQLWhere = strSQLWhere + " AND ";
					
					strSQLWhere = entry.getKey() + " like '%" + entry.getValue() + "%'";					
					//System.out.println(entry.getKey() + " like " + entry.getValue());
				}
			}
			*/
			
			if (sortField != null) {
				
				strSQLOrderBy = sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";
				
			}
			
			System.out.println("strSQLWhere=" + strSQLWhere);			
			System.out.println("strSQLGroupBy=" + strSQLGroupBy);
			System.out.println("strSQLOrderBy=" + strSQLOrderBy);
						
			
			List<UMBSession> results = dao.findReportByCriteriaBySQLSP(reportmode, strSQL, strsearchFrom, strsearchTo, strSQLGroupBy, strSQLOrderBy, null, null, additional_condition);
			rowcount = results.size();
			System.out.println("rowcount="+rowcount);	
					        
			int i = 0;
			chartdates = null;
			totaltransdata = null;
			chartdates = new String[results.size()];
			totaltransdata = new int[results.size()];
			mindurdata = new int[results.size()];
			maxdurdata = new int[results.size()];
			avgdurdata = new float[results.size()];
			
			for (UMBSession entity : results) 
			{
				chartdates[i] = entity.getStart_date() + "" ;								
				totaltransdata[i] = entity.getTotal_trans() ;												
				mindurdata[i] =  entity.getMin_dur();
				maxdurdata[i] =  entity.getMax_dur();
				avgdurdata[i] =  entity.getAvg_dur();	
				
				reportStr = reportStr + entity.getStart_date() + "," ;
				reportStr = reportStr + entity.getTotal_trans() + "," ;
				reportStr = reportStr + entity.getAvg_dur() + "," ;
				reportStr = reportStr + entity.getMax_dur() + "," ;
				reportStr = reportStr + entity.getMin_dur() + "," + "\r\n";
				
				++i;
			}
			
			
			List<UMBSession> results1 = dao.findReportByCriteriaBySQLSP(reportmode, strSQL, strsearchFrom, strsearchTo, strSQLGroupBy, strSQLOrderBy, first, pageSize, additional_condition);			
			return results1;
			
		}
	}
	
	public Date getSearchFromParam() {
		Date retDate = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String strDate = (String) facesContext.getExternalContext().getRequestParameterMap().get("transform:transFrom_input");
		
		System.out.println("getSearchFromParam strDate="+strDate);
		
		DateFormat format = new SimpleDateFormat("M/d/yy");
		try {
			retDate = format.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strDate1 = sm.format(retDate);
		System.out.println("getSearchFromParam strDate1="+strDate1);
		
		return retDate;
	}
		 

	public Date getSearchToParam() {
		Date retDate = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String strDate = (String) facesContext.getExternalContext().getRequestParameterMap().get("transform:transTo_input");
		
		System.out.println("getSearchToParam strDate="+strDate);
				
		DateFormat format = new SimpleDateFormat("M/d/yy");
		try {
			retDate = format.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strDate1 = sm.format(retDate);
		System.out.println("getSearchToParam strDate1="+strDate1);
		
		return retDate;	
	}
	
	public String getReportModeParam() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String resultStr = (String) facesContext.getExternalContext().getRequestParameterMap().get("transform:reportMode_input");
		
		System.out.println("resultStr="+resultStr);
		return resultStr;
	}
	
	
}
