package com.neuralt.blc.report.controller;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.neuralt.blc.report.model.UMBtoBLCInterface;
import com.neuralt.smp.client.data.dao.GenericDAO;

import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class UMBtoBLCInterfaceController implements Serializable {

	private static final long serialVersionUID = 7479762120242911789L;
	private Date searchFrom;
	private Date searchTo;
	private String reportmode = "";
	private int rowcount = 0;

	private static String[] chartdates;
	private static String[] host_id;
	private static String[] interface_name;
	private static String[] trans_status;
	private static String[] reason_code;
	private static int[] totaltraffic;
	private static int[] totalsuccessdata;
	private static int[] totalfaildata;
	private static float[] successpctdata;
	private static float[] failpctdata;
	private static int[] mindurdata;
	private static int[] maxdurdata;
	private static float[] avgdurdata;

	private LazyDataModel<UMBtoBLCInterface> UMBtoBLCInterface;
	private static boolean resultsVisible = false;

	private String[] columnTitles = { "trans.Date",
			"trans.totalTraffic", "trans.totalSuccess", "trans.totalFail",
			"trans.successPct", "trans.failPct", "trans.avgDuration",
			"trans.maxDuration", "trans.minDuration" };
	private String cvsFilePrefix = "";
	private String reportStr = "", reportHeaderTitle="";

	public UMBtoBLCInterfaceController() {
		resultsVisible = false;
	}

	public void getreportSetting() {
		String reportTitle = "";
		String reportHeader = "";

		FacesContext cs = FacesContext.getCurrentInstance();
		ResourceBundle bundle = cs.getApplication()
				.getResourceBundle(cs, "msg");

		cvsFilePrefix = bundle.getString("umb_blc_interface.csv.FilePrefix");
		reportTitle = bundle.getString("umb_blc_interface.csv.ReportTitle");

		for (int i = 0; i < columnTitles.length; ++i) {
			reportHeader = reportHeader + bundle.getString(columnTitles[i])
					+ ",";
		}

		reportHeaderTitle = reportTitle + "\r\n" + "\r\n" + reportHeader + "\r\n";
	}

	public String getCvsFilePrefix() {
		return cvsFilePrefix;
	}

	public String getReportStr() {
		return reportStr;
	}

	public void searchTransactions() {
		getreportSetting();
		// BLCExtInterfaceByApiNameController.setResultsVisible(false);;

		// System.out.println("in searchTransactions");
		UMBtoBLCInterface = new TransactionModel();

		int rowcount = this.getDataRowcount();
		System.out.println("in searchTransactions; rowcount=" + rowcount);
		UMBtoBLCInterface.setRowCount(rowcount);

		resultsVisible = true;
	}

	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date in_searchFrom) {
		searchFrom = in_searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date in_searchTo) {
		searchTo = in_searchTo;
	}

	public String getReportmode() {
		return reportmode;
	}

	public void setReportmode(String reportmode) {
		this.reportmode = reportmode;
	}

	public int getRowcount() {
		return rowcount;
	}

	public void setRowcount(int rowcount) {
		this.rowcount = rowcount;
	}

	public static String[] getChartdates() {
		return chartdates;
	}

	public void setChartdates(String[] chartdates) {
		this.chartdates = chartdates;
	}

	public static String[] getHost_id() {
		return host_id;
	}

	public void setHost_id(String[] host_id) {
		this.host_id = host_id;
	}

	public static String[] getInterface_name() {
		return interface_name;
	}

	public void setInterface_name(String[] interface_name) {
		this.interface_name = interface_name;
	}

	public static String[] getTrans_status() {
		return trans_status;
	}

	public void setTrans_status(String[] trans_status) {
		this.trans_status = trans_status;
	}

	public static String[] getReason_code() {
		return reason_code;
	}

	public void setReason_code(String[] reason_code) {
		this.reason_code = reason_code;
	}

	public static int[] getTotaltransdata() {
		return totaltraffic;
	}

	public void setTotaltransdata(int[] totaltraffic) {
		this.totaltraffic = totaltraffic;
	}

	public static int[] getTotalsuccessdata() {
		return totalsuccessdata;
	}

	public static int[] getTotalfaildata() {
		return totalfaildata;
	}

	public static float[] getSuccesspctdata() {
		return successpctdata;
	}

	public static float[] getFailpctdata() {
		return failpctdata;
	}

	public static int[] getMindurdata() {
		return mindurdata;
	}

	public static int[] getMaxdurdata() {
		return maxdurdata;
	}

	public static float[] getAvgdurdata() {
		return avgdurdata;
	}

	public LazyDataModel<UMBtoBLCInterface> getUMBtoBLCInterface() {
		return UMBtoBLCInterface;
	}

	public boolean isResultsVisible() {
		return resultsVisible;
	}

	public static boolean getResultsVisible() {
		return resultsVisible;
	}

	public static void setResultsVisible(boolean in_resultsVisible) {
		resultsVisible = in_resultsVisible;
	}

	public int getDataRowcount() {

		// System.out.println("getDataRowcount");

		GenericDAO<UMBtoBLCInterface, Integer> dao = new GenericDAO<UMBtoBLCInterface, Integer>(
				UMBtoBLCInterface.class, false);

		searchFrom = getSearchFromParam();
		searchTo = getSearchToParam();
		reportmode = getReportModeParam();

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";
		String strSQL = "call sp_report_umb_blc_interface(?,?,?,?,?,?,?)";
		String strSQLWhere = " ";
		String strSQLGroupBy = "start_date";
		String strSQLOrderBy = " ";
		String additional_condition = "";

		if (searchFrom != null && searchTo != null) {
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}

		System.out.println("strSQLWhere=" + strSQLWhere);
		System.out.println("strSQLGroupBy=" + strSQLGroupBy);
		System.out.println("strSQLOrderBy=" + strSQLOrderBy);

		List<UMBtoBLCInterface> results = dao.findReportByCriteriaBySQLSP(
				reportmode, strSQL, strsearchFrom, strsearchTo, strSQLGroupBy,
				strSQLOrderBy, null, null, additional_condition);
		System.out.println("results.size()=" + results.size());

		return results.size();

	}

	private class TransactionModel extends LazyDataModel<UMBtoBLCInterface> {

		private static final long serialVersionUID = 1L;
		private GenericDAO<UMBtoBLCInterface, Integer> dao = new GenericDAO<UMBtoBLCInterface, Integer>(
				UMBtoBLCInterface.class, false);

		@Override
		public List<UMBtoBLCInterface> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {

			searchFrom = getSearchFromParam();
			searchTo = getSearchToParam();
			reportmode = getReportModeParam();

			System.out
					.println("in UMBtoBLCInterfaceController.TransactionModel");
			System.out.println("searchFrom=" + searchFrom);
			System.out.println("searchTo=" + searchTo);
			System.out.println("reportmode=" + reportmode);

			/*
			 * System.out.println("in TransactionModel");
			 * System.out.println("first="+first);
			 * System.out.println("pageSize="+pageSize);
			 * System.out.println("sortField="+sortField);
			 * System.out.println("sortOrder="+sortOrder);
			 */

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";
			String strSQL = "call sp_report_umb_blc_interface(?,?,?,?,?,?,?)";
			String strSQLWhere = " ";
			String strSQLGroupBy = "start_date";
			String strSQLOrderBy = " ";
			String additional_condition = "";

			if (searchFrom != null && searchTo != null) {
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
			}

			if (sortField != null) {

				strSQLOrderBy = sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";

			}

			String strSQLGroupBy1 = "start_date";
			String strSQLOrderBy1 = "start_date";
			System.out.println("strSQLGroupBy1=" + strSQLGroupBy1);
			System.out.println("strSQLOrderBy1=" + strSQLOrderBy1);

			List<UMBtoBLCInterface> results = dao.findReportByCriteriaBySQLSP(
					reportmode, strSQL, strsearchFrom, strsearchTo,
					strSQLGroupBy1, strSQLOrderBy1, null, null,
					additional_condition);
			rowcount = results.size();
			System.out.println("rowcount=" + rowcount);

			int i = 0;
			chartdates = null;
			totaltraffic = null;
			chartdates = new String[results.size()];
			host_id = new String[results.size()];
			interface_name = new String[results.size()];
			trans_status = new String[results.size()];
			reason_code = new String[results.size()];
			totaltraffic = new int[results.size()];
			totalsuccessdata = new int[results.size()];
			totalfaildata = new int[results.size()];
			successpctdata = new float[results.size()];
			failpctdata = new float[results.size()];
			mindurdata = new int[results.size()];
			maxdurdata = new int[results.size()];
			avgdurdata = new float[results.size()];

			reportStr = reportHeaderTitle;
			
			for (UMBtoBLCInterface entity : results) {
				chartdates[i] = entity.getStart_date() + "";
				host_id[i] = entity.getHost_id();
				interface_name[i] = entity.getInterface_name();
				trans_status[i] = entity.getTrans_status();
				reason_code[i] = entity.getReason_code();
				totaltraffic[i] = entity.getTotal_traffic();
				totalsuccessdata[i] = entity.getTotal_success();
				totalfaildata[i] = entity.getTotal_fail();
				successpctdata[i] = entity.getSuccess_pct();
				failpctdata[i] = entity.getFail_pct();
				mindurdata[i] = entity.getMin_dur();
				maxdurdata[i] = entity.getMax_dur();
				avgdurdata[i] = entity.getAvg_dur();

				reportStr = reportStr + entity.getStart_date() + ",";
				reportStr = reportStr + entity.getTotal_traffic() + ",";
				reportStr = reportStr + entity.getTotal_success() + ",";
				reportStr = reportStr + entity.getTotal_fail() + ",";
				reportStr = reportStr + entity.getSuccess_pct() + ",";
				reportStr = reportStr + entity.getFail_pct() + ",";
				reportStr = reportStr + entity.getAvg_dur() + ",";
				reportStr = reportStr + entity.getMax_dur() + ",";
				reportStr = reportStr + entity.getMin_dur() + "," + "\r\n";
				++i;
			}

			System.out.println("strSQLGroupBy=" + strSQLGroupBy);
			System.out.println("strSQLOrderBy=" + strSQLOrderBy);

			List<UMBtoBLCInterface> results1 = dao.findReportByCriteriaBySQLSP(
					reportmode, strSQL, strsearchFrom, strsearchTo,
					strSQLGroupBy, strSQLOrderBy, first, pageSize,
					additional_condition);
			return results1;

		}

	}

	public Date getSearchFromParam() {
		Date retDate = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String strDate = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:searchFrom_input");

		System.out.println("getSearchFromParam strDate=" + strDate);

		DateFormat format = new SimpleDateFormat("M/d/yy");
		try {
			retDate = format.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strDate1 = sm.format(retDate);
		System.out.println("getSearchFromParam strDate1=" + strDate1);

		return retDate;
	}

	public Date getSearchToParam() {
		Date retDate = null;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String strDate = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:searchTo_input");

		System.out.println("getSearchToParam strDate=" + strDate);

		DateFormat format = new SimpleDateFormat("M/d/yy");
		try {
			retDate = format.parse(strDate);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strDate1 = sm.format(retDate);
		System.out.println("getSearchToParam strDate1=" + strDate1);

		return retDate;
	}

	public String getReportModeParam() {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		String resultStr = (String) facesContext.getExternalContext()
				.getRequestParameterMap().get("transform:reportMode_input");

		System.out.println("resultStr=" + resultStr);
		return resultStr;
	}

}
