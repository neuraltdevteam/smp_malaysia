package com.neuralt.blc.report.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.neuralt.smp.client.data.dao.GenericDAO;
import com.neuralt.blc.report.model.TransactionSummary1;

@ManagedBean
@ViewScoped
public class TransactionStatController implements Serializable {

	private static final long serialVersionUID = -4433737600244420663L;
	//private static final long serialVersionUID = -1557319022101948113L;
	//private static final long serialVersionUID = 8604301842618866191L;

	private Date searchFrom;
	private Date searchTo;
	private static String[] chartdates;
	private static int[] totaltransdata;
	private static int[] totalsuccessdata;	
	private static int[] totalfaildata;
	private static float[] successpctdata;	
	private static float[] failpctdata;
	private static int[] mindurdata;
	private static int[] maxdurdata;
	private static float[] avgdurdata;
	private static float[] q1durdata;
	private static float[] q3durdata;
	private static float[] iqrdurdata;	
	private int rowcount = 0;
	private String reportmode = "";
	private String api = "";
	
	private LazyDataModel<TransactionSummary1> transactionSummaries1;
	public boolean resultsVisible = false;

	public void searchTransactions() {
		//System.out.println("in searchTransactions");	
		transactionSummaries1 = new TransactionModel();
		//transactions.setRowCount((int) new RawDAO(false).count(buildCriteria()));	
		rowcount = this.getDataRowcount();
		transactionSummaries1.setRowCount(rowcount);
		resultsVisible = true;		
	}

	public Date getSearchFrom() {
		return searchFrom;
	}

	public void setSearchFrom(Date searchFrom) {
		this.searchFrom = searchFrom;
	}

	public Date getSearchTo() {
		return searchTo;
	}

	public void setSearchTo(Date searchTo) {
		this.searchTo = searchTo;
	}

	public static String[] getChartdates() {
		return chartdates;
	}

	public  void setChartdates(String[] chartdates) {
		this.chartdates = chartdates;
	}

	public static int[] getTotaltransdata() {
		return totaltransdata;
	}

	public void setTotaltransdata(int[] totaltransdata) {
		this.totaltransdata = totaltransdata;
	}

	public static int[] getTotalsuccessdata() {
		return totalsuccessdata;
	}

	public static int[] getTotalfaildata() {
		return totalfaildata;
	}
	
	public static float[] getSuccesspctdata() {
		return successpctdata;
	}

	public static float[] getFailpctdata() {
		return failpctdata;
	}
	
	public static int[] getMindurdata() {
		return mindurdata;
	}

	public static int[] getMaxdurdata() {
		return maxdurdata;
	}

	public static float[] getAvgdurdata() {
		return avgdurdata;
	}

	public static float[] getQ1durdata() {
		return q1durdata;
	}
	
	public static float[] getQ3durdata() {
		return q3durdata;
	}
	
	public static float[] getIqrdurdata() {
		return iqrdurdata;
	}
	
	
	public LazyDataModel<TransactionSummary1> getTransactionSummaries1() {
		return transactionSummaries1;
	}

	public boolean isResultsVisible() {
		return resultsVisible;
	}
	
	public int getRowcount() {
		return rowcount;
	}

	public void setRowcount(int rowcount) {
		this.rowcount = rowcount;
	}
	
	public String getReportmode() {
		return reportmode;
	}

	public void setReportmode(String reportmode) {
		this.reportmode = reportmode;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}
	
	public int getDataRowcount() {

		//System.out.println("getDataRowcount");

		GenericDAO<TransactionSummary1, Integer> dao = new GenericDAO<TransactionSummary1, Integer>(
				TransactionSummary1.class, false);
		
		SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
		String strsearchFrom = "";
		String strsearchTo = "";

		if (searchFrom != null && searchTo != null)
		{
			strsearchFrom = sm.format(searchFrom);
			strsearchTo = sm.format(searchTo);
		}
		
		String strSQL = "Select max(id) as id, DATE_FORMAT(date(startdate), '%Y-%m-%d') as txdate, count(*) as total_trans, sum(duration)/count(*) as avg_duration, min(duration) as min_duration, max(duration) as max_duration from trans ";
		String strSQLWhere = " ";
		String strSQLGroupBy = " group by txdate";
		String strSQLOrderBy = " ";

		if (searchFrom != null && searchTo != null)
			strSQLWhere = " date(startdate) >= date('" + strsearchFrom + "') and date(startdate) <= date('" + strsearchTo + "') "; 		
	
		if (api != null)
		{
			if (api.compareTo("") != 0 && api.compareToIgnoreCase("all") != 0)
			{
				if (strSQLWhere.compareTo("") != 0)
					strSQLWhere = strSQLWhere + " AND ";

				strSQLWhere = strSQLWhere + " api = '" + api + "' ";
			}
		}
		
		/*
		if (filters != null && !filters.isEmpty()) {
							
			for (Map.Entry<String, String> entry : filters.entrySet()) {
				
				if (strSQLWhere.compareTo("") != 0)
					strSQLWhere = strSQLWhere + " AND ";
				
				strSQLWhere = entry.getKey() + " like '%" + entry.getValue() + "%'";					
				//System.out.println(entry.getKey() + " like " + entry.getValue());
			}
		}
		*/
		
		if (strSQLWhere.compareTo("") != 0)
			strSQLWhere = " where " + strSQLWhere;
		
		strSQL = strSQL + strSQLWhere + strSQLGroupBy + strSQLOrderBy;
		//System.out.println("getDataRowcount strSQL=" + strSQL);
		
		
		//int resultcount = (int) new RawDAO(false).countBySQL(strSQL);
		//System.out.println("getDataRowcount resultcount=" + resultcount);
			
		////List<TransactionSummary1> results = dao.findByCriteriaBySQL(strSQL,null,null);		
		////System.out.println("getDataRowcount results.size()=" + results.size());
		////return results.size();
		
		strSQL = "call sp_report_trans(?,?,?,?,?)";
		List<TransactionSummary1> results = dao.findByCriteriaBySQLSP(reportmode, strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, null, null);
		return results.size();
				
		//return resultcount;
	}

	
	private class TransactionModel extends LazyDataModel<TransactionSummary1> {

		private static final long serialVersionUID = 1L;
		private GenericDAO<TransactionSummary1, Integer> dao = new GenericDAO<TransactionSummary1, Integer>(
				TransactionSummary1.class, false);

		@Override
		public List<TransactionSummary1> load(int first, int pageSize,
				String sortField, SortOrder sortOrder,
				Map<String, String> filters) {

			SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");
			String strsearchFrom = "";
			String strsearchTo = "";

			if (searchFrom != null && searchTo != null)
			{
				strsearchFrom = sm.format(searchFrom);
				strsearchTo = sm.format(searchTo);
			}
			
			System.out.println("in TransactionModel");			
			System.out.println("first="+first);
			System.out.println("pageSize="+pageSize);
			System.out.println("reportmode="+reportmode);	
			
			/*
			System.out.println("sortField="+sortField);			
			System.out.println("sortOrder="+sortOrder);			
			System.out.println("searchFrom="+strsearchFrom);
			System.out.println("searchTo="+strsearchTo);
			 */
			
			//String strSQL = "Select max(id) as id, DATE_FORMAT(date(startdate), '%Y-%m-%d') as txdate, count(*) as total_trans, sum(duration)/count(*) as avg_duration, min(duration) as min_duration, max(duration) as max_duration from trans ";
			String strSQL = "";
			String strSQLWhere = " ";
			//String strSQLGroupBy = " group by date(startdate)";
			String strSQLGroupBy = " group by txdate ";
			String strSQLOrderBy = " ";
	
			if (searchFrom != null && searchTo != null)
				strSQLWhere = " date(startdate) >= date('" + strsearchFrom + "') and date(startdate) <= date('" + strsearchTo + "')"; 
			
			if (api != null)
			{
				if (api.compareTo("") != 0 && api.compareToIgnoreCase("all") != 0)
				{
					if (strSQLWhere.compareTo("") != 0)
						strSQLWhere = strSQLWhere + " AND ";

					strSQLWhere = strSQLWhere + " api = '" + api + "' ";
				}
			}
			
			if (sortField != null) {
				
				strSQLOrderBy = " Order by " + sortField;
				if (sortOrder == SortOrder.DESCENDING)
					strSQLOrderBy = strSQLOrderBy + " desc";
				
			}
			if (filters != null && !filters.isEmpty()) {
								
				for (Map.Entry<String, String> entry : filters.entrySet()) {
					
					if (strSQLWhere.compareTo("") != 0)
						strSQLWhere = strSQLWhere + " AND ";
					
					strSQLWhere = entry.getKey() + " like '%" + entry.getValue() + "%'";					
					//System.out.println(entry.getKey() + " like " + entry.getValue());
				}
			}
			
			if (strSQLWhere.compareTo("") != 0)
				strSQLWhere = " where " + strSQLWhere;
			
			//strSQL = strSQL + strSQLWhere + strSQLGroupBy + strSQLOrderBy;
			//System.out.println("strSQL=" + strSQL);			
			//List<TransactionSummary1> results = dao.findByCriteriaBySQL(strSQL, first, pageSize);
			
			
			System.out.println("strSQLWhere=" + strSQLWhere);			
			System.out.println("strSQLGroupBy=" + strSQLGroupBy);
			System.out.println("strSQLOrderBy=" + strSQLOrderBy);
			
			
			strSQL = "call sp_report_trans(?,?,?,?,?)";
			List<TransactionSummary1> results = dao.findByCriteriaBySQLSP(reportmode, strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, null, null);
			rowcount = results.size();
			System.out.println("rowcount="+rowcount);	
			int i = 0;
			chartdates = null;
			totaltransdata = null;
			chartdates = new String[results.size()];
			totaltransdata = new int[results.size()];
			totalsuccessdata = new int[results.size()];
			totalfaildata = new int[results.size()];
			successpctdata = new float[results.size()];
			failpctdata = new float[results.size()];
			mindurdata = new int[results.size()];
			maxdurdata = new int[results.size()];
			avgdurdata = new float[results.size()];
			q1durdata = new float[results.size()];
			q3durdata = new float[results.size()];
			iqrdurdata = new float[results.size()];
			
			for (TransactionSummary1 entity : results) 
			{
				//delete(entity);
				
				//chartdates[i] = new String();
				//totaltransdata[i] = new int();
				//mindurdata[i] = new String();
				
				chartdates[i] = entity.getTxdate() + "" ;								
				totaltransdata[i] = entity.getTotal_trans() ;												
				totalsuccessdata[i] = entity.getTotal_success() ;
				totalfaildata[i] = entity.getTotal_fail() ;
				successpctdata[i] = entity.getSuccess_pct() ;
				failpctdata[i] = entity.getFail_pct() ;
				mindurdata[i] =  entity.getMin_duration();
				maxdurdata[i] =  entity.getMax_duration();
				avgdurdata[i] =  entity.getAvg_duration();				
				q1durdata[i] =  entity.getQ1_dur();
				q3durdata[i] =  entity.getQ3_dur();
				iqrdurdata[i] =  entity.getIqr_dur();
				
				/*
				if (entity.getMin_duration() != null )
				{
					mindurdata[i] =  (entity.getMin_duration().getHours() * 3600 + 
					entity.getMin_duration().getMinutes() * 60 +
					entity.getMin_duration().getSeconds() ) ;
					
				}
				else
					mindurdata[i] =0;

				if (entity.getMax_duration() != null )
				{
					maxdurdata[i] =  (entity.getMax_duration().getHours() * 3600 + 
					entity.getMax_duration().getMinutes() * 60 +
					entity.getMax_duration().getSeconds() ) ;
					
				}
				else
					maxdurdata[i] =0;
				*/
				
				++i;
			}
			

			strSQL = "call sp_report_trans(?,?,?,?,?)";
			List<TransactionSummary1> results1 = dao.findByCriteriaBySQLSP(reportmode, strSQL, strSQLWhere, strSQLGroupBy, strSQLOrderBy, first, pageSize);

			return results1;
		}

	}
	
	
}
