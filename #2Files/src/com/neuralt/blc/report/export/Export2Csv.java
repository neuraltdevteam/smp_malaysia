package com.neuralt.blc.report.export;


import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.ResourceBundle;
import java.util.regex.Pattern;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

@ManagedBean
@SessionScoped
public class Export2Csv {
    
    private String csvBuffer;
    
    public Export2Csv() {
        
    }
    
    public void exportHtmlTableToExcel(String filePrefix, String inCsvBuffer) throws IOException{        
        
        //Set the filename    	
		DateFormat ymd_format = new SimpleDateFormat("yyyyMMddHHmmss");
		Calendar cal = Calendar.getInstance();           
		String current_day_ymd = ymd_format.format(cal.getTime());
		String filename = current_day_ymd + ".csv";
                   
        //Setup the output
        String contentType = "text/csv";
        //String contentType = "application/vnd.ms-excel";
        
        FacesContext fc = FacesContext.getCurrentInstance();
        //filename = fc.getExternalContext().getUserPrincipal().toString() + "-"+ filename;       
        filename = filePrefix + "_"+ filename;
        
        HttpServletResponse response = (HttpServletResponse)fc.getExternalContext().getResponse();
        response.setHeader("Content-disposition", "attachment; filename=" + filename);
        response.setContentType(contentType);
  
        
        //csvBuffer = "Col1,Col2,Col3" + "\r\n" + "a,b,c"  + "\r\n" + "1,2,3";
        csvBuffer = inCsvBuffer;
        
        //Write the table back out
        PrintWriter out = response.getWriter();
        out.print(csvBuffer);
        out.close();
        fc.responseComplete();
    }    
    
    public String getCvsBuffer() {
        return csvBuffer;
    }
    
    public void setCvsBuffer(String csvBuffer) {
        this.csvBuffer = csvBuffer;
    }
    
}