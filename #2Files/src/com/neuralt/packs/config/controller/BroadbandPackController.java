package com.neuralt.packs.config.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.neuralt.packs.config.data.PackBroadbandConfig;
import com.neuralt.packs.config.model.BroadbandPack;

@ManagedBean
@ViewScoped
public class BroadbandPackController extends BasePackController<PackBroadbandConfig, BroadbandPack>{

	public BroadbandPackController() {
		super(BroadbandPack.class);
	}

}
