package com.neuralt.packs.config.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.neuralt.packs.config.data.PackIplanConfig;
import com.neuralt.packs.config.model.IplanPack;

@ManagedBean
@ViewScoped
public class IplanPackController extends BasePackController<PackIplanConfig, IplanPack>{

	public IplanPackController() {
		super(IplanPack.class);
	}

}
