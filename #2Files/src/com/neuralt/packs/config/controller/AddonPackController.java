package com.neuralt.packs.config.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.neuralt.packs.config.data.PackAddonConfig;
import com.neuralt.packs.config.model.AddonPack;

@ManagedBean
@ViewScoped
public class AddonPackController extends BasePackController<PackAddonConfig, AddonPack>{

	public AddonPackController() {
		super(AddonPack.class);
	}

}
