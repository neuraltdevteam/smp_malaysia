package com.neuralt.packs.config.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.neuralt.packs.config.data.PackVpConfig;
import com.neuralt.packs.config.model.VpPack;

@ManagedBean
@ViewScoped
public class VpPackController extends BasePackController<PackVpConfig, VpPack>{

	public VpPackController() {
		super(VpPack.class);
	}

}
