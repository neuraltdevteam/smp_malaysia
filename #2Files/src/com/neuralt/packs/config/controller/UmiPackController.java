package com.neuralt.packs.config.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.neuralt.packs.config.data.PackUmiConfig;
import com.neuralt.packs.config.model.UmiPack;

@ManagedBean
@ViewScoped
public class UmiPackController extends BasePackController<PackUmiConfig, UmiPack>{

	public UmiPackController() {
		super(UmiPack.class);
	}

}
