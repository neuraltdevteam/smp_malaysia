package com.neuralt.packs.config.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.packs.config.PackMessageKey;
import com.neuralt.packs.config.data.PackConfig;
import com.neuralt.packs.config.model.BasePack;
import com.neuralt.packs.config.model.PackageException;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.web.util.jsf.WebUtil;

abstract public class BasePackController<C extends PackConfig, M extends BasePack<C>> {

	protected final Logger log;
	
	protected final Class<M> modelType;
	
	protected final ModelDataTable modelDataTable;
	
	protected M model;
	
	protected List<M> listModel;
	
	protected boolean readOnly;
	
	protected BasePackController(Class<M> modelType){
		this.log = initLog();
		this.modelType = modelType;
		this.model = newModel();
		this.modelDataTable = initModelDataTable();
	}
	
	protected Logger initLog(){
		return LoggerFactory.getLogger(this.getClass());
	}
	public Logger getLog() {
		return log;
	}

	public Class<M> getModelType() {
		return modelType;
	}
	
	public ModelDataTable initModelDataTable(){
		return new ModelDataTable("name like :name", "name", "%");
	}
	public ModelDataTable getModelDataTable() {
		return modelDataTable;
	}

	protected M newModel(){
		try {
			return modelType.newInstance();
		}catch (Exception e) {
			log.error("failed create new instance of type ["+modelType.getName()+"]", e);
			return null;
		}
	}
	public M getModel() {
		return model;
	}
	public void setModel(M model) {
		this.model = (model==null?newModel():model);
	}
	
	public List<M> getListModel() {
		return listModel;
	}
	public void setListModel(List<M> listModel) {
		this.listModel = listModel;
	}
	
	public boolean isReadOnly() {
		return readOnly;
	}
	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	@PostConstruct
	public void reload(){
		listModel = modelDataTable.list();
	}
	
	public void save(){
		try{
			boolean isNew = (model.getId() == null);
			model.save();
			if(isNew){
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
						"add-pack", model.toString());
			}else{
				AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
						"edit-pack", model.toString());
			}
		}catch(PackageException e){
			switch(e.getReason()){
			case ALREADY_EXIST:
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						PackMessageKey.PACK_ALREADY_EXIST);
			default:
			}
		}
		reload();
	}
	
	public void delete(){
		try{
			model.delete();
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-pack", model.toString());
		}catch(PackageException e){
			switch(e.getReason()){
			case NOT_EXIST:
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						PackMessageKey.PACK_NOT_EXIST);
			case STILL_ACTIVE:
				WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
						MessageKey.GENERAL_ERROR,
						PackMessageKey.PACK_STILL_ACTIVE);
			default:
			}
		}
		reload();
	}

	public class ModelDataTable{
		
		protected final String hql;
		protected final Map<String,Object> param;
		
		protected String orderBy = "id asc";
		
		public ModelDataTable(String baseCriteria, Object ... baseParam){
			this.hql = "from "+model.getConfigType().getSimpleName()+" where "+baseCriteria;
			this.param = new HashMap<String,Object>();
			String key = null;
			for(Object p : baseParam){
				if(key == null){
					key = (String)p;
				}else{
					this.param.put(key, p);
					key = null;
				}
			}
		}
		
		public String getHql() {
			return hql;
		}

		public Map<String, Object> getParam() {
			return param;
		}

		public String getOrderBy() {
			return orderBy;
		}
		public void setOrderBy(String orderBy) {
			this.orderBy = orderBy;
		}

		public List<M> list(Map<String,Object> overrideParam, String extraHql){
			Map<String,Object> param = new HashMap<String,Object>();
			param.putAll(this.param);
			if(overrideParam != null){
				param.putAll(overrideParam);
			}
			String hql = this.hql;
			if(extraHql != null){
				hql+=" "+extraHql;
			}
			if(orderBy != null){
				hql+=" order by "+orderBy;
			}
			List<C> listC = model.getConfigDAO().find(hql, param);
			List<M> listM = new ArrayList<M>(listC.size());
			for(C config : listC){
				M m = newModel();
				m.setConfig(config);
				listM.add(m);
			}
			return listM;
		}
		
		public List<M> list(Map<String,Object> overrideParam){
			return list(overrideParam, null);
		}
		
		public List<M> list(String extraHql){
			return list(null, extraHql);
		}
		
		public List<M> list(){
			return list(null, null);
		}
	}
}
