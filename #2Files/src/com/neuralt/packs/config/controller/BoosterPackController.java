package com.neuralt.packs.config.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.neuralt.packs.config.data.PackBoosterConfig;
import com.neuralt.packs.config.model.BoosterPack;

@ManagedBean
@ViewScoped
public class BoosterPackController extends BasePackController<PackBoosterConfig, BoosterPack>{

	public BoosterPackController() {
		super(BoosterPack.class);
	}

}
