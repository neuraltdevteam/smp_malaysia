package com.neuralt.packs.config.controller;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.neuralt.packs.config.data.PackBlackberryConfig;
import com.neuralt.packs.config.model.BlackberryPack;

@ManagedBean
@ViewScoped
public class BlackberryPackController extends BasePackController<PackBlackberryConfig, BlackberryPack>{

	public BlackberryPackController() {
		super(BlackberryPack.class);
	}

}
