package com.neuralt.packs.config;

public interface PackMessageKey {

	public static final String PACK_NOT_EXIST = "packs.not.exist";
	public static final String PACK_ALREADY_EXIST = "packs.already.exist";
	public static final String PACK_STILL_ACTIVE = "packs.still.active";
	
}
