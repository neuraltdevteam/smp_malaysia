package com.neuralt.packs.config.data;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "PACK_VP_CONFIG", uniqueConstraints = @UniqueConstraint(columnNames = "NAME"))
public class PackVpConfig implements java.io.Serializable, PackConfig {

	private static final long serialVersionUID = 1L;
	
	// Fields

	private Long id;
	private String name;
	private Integer validity;
	private BigDecimal price;
	private Integer priceCode;
	private String chargingName;
	private Integer bundleSncode;
	private Integer bundleSpcode;
	private Boolean autoRenew;
	private String displayLabel;
	private Long packAmount;
	private Integer unit;
	private String entityCategory;
	private String entityTypeKeyOnnet;
	private String entityTypeKeyOffnet;
	private Boolean active;

	// Constructors

	/** default constructor */
	public PackVpConfig() {
	}

	/** minimal constructor */
	public PackVpConfig(String name, Boolean active) {
		this.name = name;
		this.active = active;
	}

	/** full constructor */
	public PackVpConfig(String name, Integer validity, BigDecimal price,
			Integer priceCode, String chargingName, Integer bundleSncode,
			Integer bundleSpcode, Boolean autoRenew, String displayLabel,
			Long packAmount, Integer unit, String entityCategory,
			String entityTypeKeyOnnet, String entityTypeKeyOffnet,
			Boolean active) {
		this.name = name;
		this.validity = validity;
		this.price = price;
		this.priceCode = priceCode;
		this.chargingName = chargingName;
		this.bundleSncode = bundleSncode;
		this.bundleSpcode = bundleSpcode;
		this.autoRenew = autoRenew;
		this.displayLabel = displayLabel;
		this.packAmount = packAmount;
		this.unit = unit;
		this.entityCategory = entityCategory;
		this.entityTypeKeyOnnet = entityTypeKeyOnnet;
		this.entityTypeKeyOffnet = entityTypeKeyOffnet;
		this.active = active;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME", unique = true, nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "VALIDITY")
	public Integer getValidity() {
		return this.validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	@Column(name = "PRICE", precision = 10)
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Column(name = "PRICE_CODE")
	public Integer getPriceCode() {
		return this.priceCode;
	}

	public void setPriceCode(Integer priceCode) {
		this.priceCode = priceCode;
	}

	@Column(name = "CHARGING_NAME", length = 50)
	public String getChargingName() {
		return this.chargingName;
	}

	public void setChargingName(String chargingName) {
		this.chargingName = chargingName;
	}

	@Column(name = "BUNDLE_SNCODE")
	public Integer getBundleSncode() {
		return this.bundleSncode;
	}

	public void setBundleSncode(Integer bundleSncode) {
		this.bundleSncode = bundleSncode;
	}

	@Column(name = "BUNDLE_SPCODE")
	public Integer getBundleSpcode() {
		return this.bundleSpcode;
	}

	public void setBundleSpcode(Integer bundleSpcode) {
		this.bundleSpcode = bundleSpcode;
	}

	@Column(name = "AUTO_RENEW")
	public Boolean getAutoRenew() {
		return this.autoRenew;
	}

	public void setAutoRenew(Boolean autoRenew) {
		this.autoRenew = autoRenew;
	}

	@Column(name = "DISPLAY_LABEL", length = 50)
	public String getDisplayLabel() {
		return this.displayLabel;
	}

	public void setDisplayLabel(String displayLabel) {
		this.displayLabel = displayLabel;
	}

	@Column(name = "PACK_AMOUNT")
	public Long getPackAmount() {
		return this.packAmount;
	}

	public void setPackAmount(Long packAmount) {
		this.packAmount = packAmount;
	}

	@Column(name = "UNIT")
	public Integer getUnit() {
		return this.unit;
	}

	public void setUnit(Integer unit) {
		this.unit = unit;
	}

	@Column(name = "ENTITY_CATEGORY", length = 50)
	public String getEntityCategory() {
		return this.entityCategory;
	}

	public void setEntityCategory(String entityCategory) {
		this.entityCategory = entityCategory;
	}

	@Column(name = "ENTITY_TYPE_KEY_ONNET", length = 50)
	public String getEntityTypeKeyOnnet() {
		return this.entityTypeKeyOnnet;
	}

	public void setEntityTypeKeyOnnet(String entityTypeKeyOnnet) {
		this.entityTypeKeyOnnet = entityTypeKeyOnnet;
	}

	@Column(name = "ENTITY_TYPE_KEY_OFFNET", length = 50)
	public String getEntityTypeKeyOffnet() {
		return this.entityTypeKeyOffnet;
	}

	public void setEntityTypeKeyOffnet(String entityTypeKeyOffnet) {
		this.entityTypeKeyOffnet = entityTypeKeyOffnet;
	}

	@Column(name = "ACTIVE", nullable = false)
	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}