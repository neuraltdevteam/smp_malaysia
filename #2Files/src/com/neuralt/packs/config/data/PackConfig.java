package com.neuralt.packs.config.data;

import java.io.Serializable;

public interface PackConfig extends Serializable{

	public static final String NAME = "name";
	
	public Long getId();
	public void setId(Long id);

	public String getName();
	public void setName(String name);
	
	public Boolean getActive();
	public void setActive(Boolean active);
}
