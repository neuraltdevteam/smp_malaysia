package com.neuralt.packs.config.data;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "PACK_BROADBAND_CONFIG", uniqueConstraints = @UniqueConstraint(columnNames = "NAME"))
public class PackBroadbandConfig implements java.io.Serializable, PackConfig  {

	private static final long serialVersionUID = 1L;
	
	// Fields

	private Long id;
	private String name;
	private Integer validity;
	private BigDecimal price;
	private Integer priceCode;
	private Long packSize;
	private Long bonusPackSize;
	private String chargingName;
	private Integer bundleSncode;
	private Integer bundleSpcode;
	private String subscriberType;
	private Integer dataPackSncode;
	private Integer dataPackSpcode;
	private Integer bonusBundleSncode;
	private Integer bonusDataPackSncode;
	private Integer bonusDataPackSpcode;
	private Integer freeAutoRenewCount;
	private String displayLabel;
	private String retryRules;
	private Boolean charging;
	private Boolean immediateStop;
	private Boolean promoPack;
	private Boolean sendSms;
	private Boolean autoRenew;
	private String basePlan;
	private Integer freebiesIdx;
	private Integer freeUsageOfferid;
	private String entityTypeKey;
	private String entityCategory;
	private Integer freeDataPackSncode;
	private Integer supervisionExtendPeriod;
	private Integer serviceFeeExtendPeriod;
	private Integer offPeakDataSncode;
	private Boolean active;

	// Constructors

	/** default constructor */
	public PackBroadbandConfig() {
	}

	/** minimal constructor */
	public PackBroadbandConfig(String name, Boolean active) {
		this.name = name;
		this.active = active;
	}

	/** full constructor */
	public PackBroadbandConfig(String name, Integer validity, BigDecimal price,
			Integer priceCode, Long packSize, Long bonusPackSize,
			String chargingName, Integer bundleSncode, Integer bundleSpcode,
			String subscriberType, Integer dataPackSncode,
			Integer dataPackSpcode, Integer bonusBundleSncode,
			Integer bonusDataPackSncode, Integer bonusDataPackSpcode,
			Integer freeAutoRenewCount, String displayLabel, String retryRules,
			Boolean charging, Boolean immediateStop, Boolean promoPack,
			Boolean sendSms, Boolean autoRenew, String basePlan,
			Integer freebiesIdx, Integer freeUsageOfferid,
			String entityTypeKey, String entityCategory,
			Integer freeDataPackSncode, Integer supervisionExtendPeriod,
			Integer serviceFeeExtendPeriod, Integer offPeakDataSncode,
			Boolean active) {
		this.name = name;
		this.validity = validity;
		this.price = price;
		this.priceCode = priceCode;
		this.packSize = packSize;
		this.bonusPackSize = bonusPackSize;
		this.chargingName = chargingName;
		this.bundleSncode = bundleSncode;
		this.bundleSpcode = bundleSpcode;
		this.subscriberType = subscriberType;
		this.dataPackSncode = dataPackSncode;
		this.dataPackSpcode = dataPackSpcode;
		this.bonusBundleSncode = bonusBundleSncode;
		this.bonusDataPackSncode = bonusDataPackSncode;
		this.bonusDataPackSpcode = bonusDataPackSpcode;
		this.freeAutoRenewCount = freeAutoRenewCount;
		this.displayLabel = displayLabel;
		this.retryRules = retryRules;
		this.charging = charging;
		this.immediateStop = immediateStop;
		this.promoPack = promoPack;
		this.sendSms = sendSms;
		this.autoRenew = autoRenew;
		this.basePlan = basePlan;
		this.freebiesIdx = freebiesIdx;
		this.freeUsageOfferid = freeUsageOfferid;
		this.entityTypeKey = entityTypeKey;
		this.entityCategory = entityCategory;
		this.freeDataPackSncode = freeDataPackSncode;
		this.supervisionExtendPeriod = supervisionExtendPeriod;
		this.serviceFeeExtendPeriod = serviceFeeExtendPeriod;
		this.offPeakDataSncode = offPeakDataSncode;
		this.active = active;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME", unique = true, nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "VALIDITY")
	public Integer getValidity() {
		return this.validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	@Column(name = "PRICE", precision = 10)
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Column(name = "PRICE_CODE")
	public Integer getPriceCode() {
		return this.priceCode;
	}

	public void setPriceCode(Integer priceCode) {
		this.priceCode = priceCode;
	}

	@Column(name = "PACK_SIZE")
	public Long getPackSize() {
		return this.packSize;
	}

	public void setPackSize(Long packSize) {
		this.packSize = packSize;
	}

	@Column(name = "BONUS_PACK_SIZE")
	public Long getBonusPackSize() {
		return this.bonusPackSize;
	}

	public void setBonusPackSize(Long bonusPackSize) {
		this.bonusPackSize = bonusPackSize;
	}

	@Column(name = "CHARGING_NAME", length = 50)
	public String getChargingName() {
		return this.chargingName;
	}

	public void setChargingName(String chargingName) {
		this.chargingName = chargingName;
	}

	@Column(name = "BUNDLE_SNCODE")
	public Integer getBundleSncode() {
		return this.bundleSncode;
	}

	public void setBundleSncode(Integer bundleSncode) {
		this.bundleSncode = bundleSncode;
	}

	@Column(name = "BUNDLE_SPCODE")
	public Integer getBundleSpcode() {
		return this.bundleSpcode;
	}

	public void setBundleSpcode(Integer bundleSpcode) {
		this.bundleSpcode = bundleSpcode;
	}

	@Column(name = "SUBSCRIBER_TYPE", length = 12)
	public String getSubscriberType() {
		return this.subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	@Column(name = "DATA_PACK_SNCODE")
	public Integer getDataPackSncode() {
		return this.dataPackSncode;
	}

	public void setDataPackSncode(Integer dataPackSncode) {
		this.dataPackSncode = dataPackSncode;
	}

	@Column(name = "DATA_PACK_SPCODE")
	public Integer getDataPackSpcode() {
		return this.dataPackSpcode;
	}

	public void setDataPackSpcode(Integer dataPackSpcode) {
		this.dataPackSpcode = dataPackSpcode;
	}

	@Column(name = "BONUS_BUNDLE_SNCODE")
	public Integer getBonusBundleSncode() {
		return this.bonusBundleSncode;
	}

	public void setBonusBundleSncode(Integer bonusBundleSncode) {
		this.bonusBundleSncode = bonusBundleSncode;
	}

	@Column(name = "BONUS_DATA_PACK_SNCODE")
	public Integer getBonusDataPackSncode() {
		return this.bonusDataPackSncode;
	}

	public void setBonusDataPackSncode(Integer bonusDataPackSncode) {
		this.bonusDataPackSncode = bonusDataPackSncode;
	}

	@Column(name = "BONUS_DATA_PACK_SPCODE")
	public Integer getBonusDataPackSpcode() {
		return this.bonusDataPackSpcode;
	}

	public void setBonusDataPackSpcode(Integer bonusDataPackSpcode) {
		this.bonusDataPackSpcode = bonusDataPackSpcode;
	}

	@Column(name = "FREE_AUTO_RENEW_COUNT")
	public Integer getFreeAutoRenewCount() {
		return this.freeAutoRenewCount;
	}

	public void setFreeAutoRenewCount(Integer freeAutoRenewCount) {
		this.freeAutoRenewCount = freeAutoRenewCount;
	}

	@Column(name = "DISPLAY_LABEL", length = 50)
	public String getDisplayLabel() {
		return this.displayLabel;
	}

	public void setDisplayLabel(String displayLabel) {
		this.displayLabel = displayLabel;
	}

	@Column(name = "RETRY_RULES", length = 128)
	public String getRetryRules() {
		return this.retryRules;
	}

	public void setRetryRules(String retryRules) {
		this.retryRules = retryRules;
	}

	@Column(name = "CHARGING")
	public Boolean getCharging() {
		return this.charging;
	}

	public void setCharging(Boolean charging) {
		this.charging = charging;
	}

	@Column(name = "IMMEDIATE_STOP")
	public Boolean getImmediateStop() {
		return this.immediateStop;
	}

	public void setImmediateStop(Boolean immediateStop) {
		this.immediateStop = immediateStop;
	}

	@Column(name = "PROMO_PACK")
	public Boolean getPromoPack() {
		return this.promoPack;
	}

	public void setPromoPack(Boolean promoPack) {
		this.promoPack = promoPack;
	}

	@Column(name = "SEND_SMS")
	public Boolean getSendSms() {
		return this.sendSms;
	}

	public void setSendSms(Boolean sendSms) {
		this.sendSms = sendSms;
	}

	@Column(name = "AUTO_RENEW")
	public Boolean getAutoRenew() {
		return this.autoRenew;
	}

	public void setAutoRenew(Boolean autoRenew) {
		this.autoRenew = autoRenew;
	}

	@Column(name = "BASE_PLAN", length = 50)
	public String getBasePlan() {
		return this.basePlan;
	}

	public void setBasePlan(String basePlan) {
		this.basePlan = basePlan;
	}

	@Column(name = "FREEBIES_IDX")
	public Integer getFreebiesIdx() {
		return this.freebiesIdx;
	}

	public void setFreebiesIdx(Integer freebiesIdx) {
		this.freebiesIdx = freebiesIdx;
	}

	@Column(name = "FREE_USAGE_OFFERID")
	public Integer getFreeUsageOfferid() {
		return this.freeUsageOfferid;
	}

	public void setFreeUsageOfferid(Integer freeUsageOfferid) {
		this.freeUsageOfferid = freeUsageOfferid;
	}

	@Column(name = "ENTITY_TYPE_KEY", length = 50)
	public String getEntityTypeKey() {
		return this.entityTypeKey;
	}

	public void setEntityTypeKey(String entityTypeKey) {
		this.entityTypeKey = entityTypeKey;
	}

	@Column(name = "ENTITY_CATEGORY", length = 50)
	public String getEntityCategory() {
		return this.entityCategory;
	}

	public void setEntityCategory(String entityCategory) {
		this.entityCategory = entityCategory;
	}

	@Column(name = "FREE_DATA_PACK_SNCODE")
	public Integer getFreeDataPackSncode() {
		return this.freeDataPackSncode;
	}

	public void setFreeDataPackSncode(Integer freeDataPackSncode) {
		this.freeDataPackSncode = freeDataPackSncode;
	}

	@Column(name = "SUPERVISION_EXTEND_PERIOD")
	public Integer getSupervisionExtendPeriod() {
		return this.supervisionExtendPeriod;
	}

	public void setSupervisionExtendPeriod(Integer supervisionExtendPeriod) {
		this.supervisionExtendPeriod = supervisionExtendPeriod;
	}

	@Column(name = "SERVICE_FEE_EXTEND_PERIOD")
	public Integer getServiceFeeExtendPeriod() {
		return this.serviceFeeExtendPeriod;
	}

	public void setServiceFeeExtendPeriod(Integer serviceFeeExtendPeriod) {
		this.serviceFeeExtendPeriod = serviceFeeExtendPeriod;
	}

	@Column(name = "OFF_PEAK_DATA_SNCODE")
	public Integer getOffPeakDataSncode() {
		return this.offPeakDataSncode;
	}

	public void setOffPeakDataSncode(Integer offPeakDataSncode) {
		this.offPeakDataSncode = offPeakDataSncode;
	}

	@Column(name = "ACTIVE", nullable = false)
	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}