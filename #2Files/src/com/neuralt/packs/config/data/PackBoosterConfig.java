package com.neuralt.packs.config.data;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "PACK_BOOSTER_CONFIG", uniqueConstraints = @UniqueConstraint(columnNames = "NAME"))
public class PackBoosterConfig implements java.io.Serializable, PackConfig {

	private static final long serialVersionUID = 1L;
	
	// Fields

	private Long id;
	private String name;
	private Integer bundleSncode;
	private Integer dataPackSncode;
	private Integer dataPackSpcode;
	private BigDecimal price;
	private Integer priceCode;
	private String chargingName;
	private String displayLabel;
	private String planName;
	private String subscriberType;
	private String size;
	private Integer validity;
	private Integer accumulatorId;
	private Boolean active;

	// Constructors

	/** default constructor */
	public PackBoosterConfig() {
	}

	/** minimal constructor */
	public PackBoosterConfig(String name, Boolean active) {
		this.name = name;
		this.active = active;
	}

	/** full constructor */
	public PackBoosterConfig(String name, Integer bundleSncode,
			Integer dataPackSncode, Integer dataPackSpcode, BigDecimal price,
			Integer priceCode, String chargingName, String displayLabel,
			String planName, String subscriberType, String size,
			Integer validity, Integer accumulatorId, Boolean active) {
		this.name = name;
		this.bundleSncode = bundleSncode;
		this.dataPackSncode = dataPackSncode;
		this.dataPackSpcode = dataPackSpcode;
		this.price = price;
		this.priceCode = priceCode;
		this.chargingName = chargingName;
		this.displayLabel = displayLabel;
		this.planName = planName;
		this.subscriberType = subscriberType;
		this.size = size;
		this.validity = validity;
		this.accumulatorId = accumulatorId;
		this.active = active;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME", unique = true, nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "BUNDLE_SNCODE")
	public Integer getBundleSncode() {
		return this.bundleSncode;
	}

	public void setBundleSncode(Integer bundleSncode) {
		this.bundleSncode = bundleSncode;
	}

	@Column(name = "DATA_PACK_SNCODE")
	public Integer getDataPackSncode() {
		return this.dataPackSncode;
	}

	public void setDataPackSncode(Integer dataPackSncode) {
		this.dataPackSncode = dataPackSncode;
	}

	@Column(name = "DATA_PACK_SPCODE")
	public Integer getDataPackSpcode() {
		return this.dataPackSpcode;
	}

	public void setDataPackSpcode(Integer dataPackSpcode) {
		this.dataPackSpcode = dataPackSpcode;
	}

	@Column(name = "PRICE", precision = 10)
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Column(name = "PRICE_CODE")
	public Integer getPriceCode() {
		return this.priceCode;
	}

	public void setPriceCode(Integer priceCode) {
		this.priceCode = priceCode;
	}

	@Column(name = "CHARGING_NAME", length = 50)
	public String getChargingName() {
		return this.chargingName;
	}

	public void setChargingName(String chargingName) {
		this.chargingName = chargingName;
	}

	@Column(name = "DISPLAY_LABEL", length = 50)
	public String getDisplayLabel() {
		return this.displayLabel;
	}

	public void setDisplayLabel(String displayLabel) {
		this.displayLabel = displayLabel;
	}

	@Column(name = "PLAN_NAME", length = 50)
	public String getPlanName() {
		return this.planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	@Column(name = "SUBSCRIBER_TYPE", length = 12)
	public String getSubscriberType() {
		return this.subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	@Column(name = "SIZE", length = 10)
	public String getSize() {
		return this.size;
	}

	public void setSize(String size) {
		this.size = size;
	}

	@Column(name = "VALIDITY")
	public Integer getValidity() {
		return this.validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	@Column(name = "ACCUMULATOR_ID")
	public Integer getAccumulatorId() {
		return this.accumulatorId;
	}

	public void setAccumulatorId(Integer accumulatorId) {
		this.accumulatorId = accumulatorId;
	}

	@Column(name = "ACTIVE", nullable = false)
	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}