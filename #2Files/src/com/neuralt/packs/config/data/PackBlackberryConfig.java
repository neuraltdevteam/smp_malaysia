package com.neuralt.packs.config.data;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "PACK_BLACKBERRY_CONFIG", uniqueConstraints = @UniqueConstraint(columnNames = "NAME"))
public class PackBlackberryConfig implements java.io.Serializable, PackConfig {

	private static final long serialVersionUID = 1L;
	
	// Fields

	private Long id;
	private String name;
	private Boolean purchaseable;
	private BigDecimal price;
	private Integer priceCode;
	private String displayLabel;
	private String subscriberType;
	private Integer bundleSncode;
	private Boolean autoRenew;
	private Integer autoRenewRetry;
	private Integer rimDataSncode;
	private Integer rimDataSpcode;
	private Integer freeDataSncode;
	private Integer freeDataSpcode;
	private Long freeDataUsageLimit;
	private Boolean additionAutorenewDataPackSncode;
	private Long additionAutorenewDataPackUsageLimit;
	private Integer validity;
	private Boolean dailyAutoRenew;
	private Integer dayBundlePackSncode;
	private Integer dayPackFreeDataSncode;
	private String entityTypeKey;
	private String chargingName;
	private Boolean active;

	// Constructors

	/** default constructor */
	public PackBlackberryConfig() {
	}

	/** minimal constructor */
	public PackBlackberryConfig(String name, Boolean active) {
		this.name = name;
		this.active = active;
	}

	/** full constructor */
	public PackBlackberryConfig(String name, Boolean purchaseable,
			BigDecimal price, Integer priceCode, String displayLabel,
			String subscriberType, Integer bundleSncode, Boolean autoRenew,
			Integer autoRenewRetry, Integer rimDataSncode,
			Integer rimDataSpcode, Integer freeDataSncode,
			Integer freeDataSpcode, Long freeDataUsageLimit,
			Boolean additionAutorenewDataPackSncode,
			Long additionAutorenewDataPackUsageLimit, Integer validity,
			Boolean dailyAutoRenew, Integer dayBundlePackSncode,
			Integer dayPackFreeDataSncode, String entityTypeKey,
			String chargingName, Boolean active) {
		this.name = name;
		this.purchaseable = purchaseable;
		this.price = price;
		this.priceCode = priceCode;
		this.displayLabel = displayLabel;
		this.subscriberType = subscriberType;
		this.bundleSncode = bundleSncode;
		this.autoRenew = autoRenew;
		this.autoRenewRetry = autoRenewRetry;
		this.rimDataSncode = rimDataSncode;
		this.rimDataSpcode = rimDataSpcode;
		this.freeDataSncode = freeDataSncode;
		this.freeDataSpcode = freeDataSpcode;
		this.freeDataUsageLimit = freeDataUsageLimit;
		this.additionAutorenewDataPackSncode = additionAutorenewDataPackSncode;
		this.additionAutorenewDataPackUsageLimit = additionAutorenewDataPackUsageLimit;
		this.validity = validity;
		this.dailyAutoRenew = dailyAutoRenew;
		this.dayBundlePackSncode = dayBundlePackSncode;
		this.dayPackFreeDataSncode = dayPackFreeDataSncode;
		this.entityTypeKey = entityTypeKey;
		this.chargingName = chargingName;
		this.active = active;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME", unique = true, nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "PURCHASEABLE")
	public Boolean getPurchaseable() {
		return this.purchaseable;
	}

	public void setPurchaseable(Boolean purchaseable) {
		this.purchaseable = purchaseable;
	}

	@Column(name = "PRICE", precision = 10)
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Column(name = "PRICE_CODE")
	public Integer getPriceCode() {
		return this.priceCode;
	}

	public void setPriceCode(Integer priceCode) {
		this.priceCode = priceCode;
	}

	@Column(name = "DISPLAY_LABEL", length = 50)
	public String getDisplayLabel() {
		return this.displayLabel;
	}

	public void setDisplayLabel(String displayLabel) {
		this.displayLabel = displayLabel;
	}

	@Column(name = "SUBSCRIBER_TYPE", length = 12)
	public String getSubscriberType() {
		return this.subscriberType;
	}

	public void setSubscriberType(String subscriberType) {
		this.subscriberType = subscriberType;
	}

	@Column(name = "BUNDLE_SNCODE")
	public Integer getBundleSncode() {
		return this.bundleSncode;
	}

	public void setBundleSncode(Integer bundleSncode) {
		this.bundleSncode = bundleSncode;
	}

	@Column(name = "AUTO_RENEW")
	public Boolean getAutoRenew() {
		return this.autoRenew;
	}

	public void setAutoRenew(Boolean autoRenew) {
		this.autoRenew = autoRenew;
	}

	@Column(name = "AUTO_RENEW_RETRY")
	public Integer getAutoRenewRetry() {
		return this.autoRenewRetry;
	}

	public void setAutoRenewRetry(Integer autoRenewRetry) {
		this.autoRenewRetry = autoRenewRetry;
	}

	@Column(name = "RIM_DATA_SNCODE")
	public Integer getRimDataSncode() {
		return this.rimDataSncode;
	}

	public void setRimDataSncode(Integer rimDataSncode) {
		this.rimDataSncode = rimDataSncode;
	}

	@Column(name = "RIM_DATA_SPCODE")
	public Integer getRimDataSpcode() {
		return this.rimDataSpcode;
	}

	public void setRimDataSpcode(Integer rimDataSpcode) {
		this.rimDataSpcode = rimDataSpcode;
	}

	@Column(name = "FREE_DATA_SNCODE")
	public Integer getFreeDataSncode() {
		return this.freeDataSncode;
	}

	public void setFreeDataSncode(Integer freeDataSncode) {
		this.freeDataSncode = freeDataSncode;
	}

	@Column(name = "FREE_DATA_SPCODE")
	public Integer getFreeDataSpcode() {
		return this.freeDataSpcode;
	}

	public void setFreeDataSpcode(Integer freeDataSpcode) {
		this.freeDataSpcode = freeDataSpcode;
	}

	@Column(name = "FREE_DATA_USAGE_LIMIT")
	public Long getFreeDataUsageLimit() {
		return this.freeDataUsageLimit;
	}

	public void setFreeDataUsageLimit(Long freeDataUsageLimit) {
		this.freeDataUsageLimit = freeDataUsageLimit;
	}

	@Column(name = "ADDITION_AUTORENEW_DATA_PACK_SNCODE")
	public Boolean getAdditionAutorenewDataPackSncode() {
		return this.additionAutorenewDataPackSncode;
	}

	public void setAdditionAutorenewDataPackSncode(
			Boolean additionAutorenewDataPackSncode) {
		this.additionAutorenewDataPackSncode = additionAutorenewDataPackSncode;
	}

	@Column(name = "ADDITION_AUTORENEW_DATA_PACK_USAGE_LIMIT")
	public Long getAdditionAutorenewDataPackUsageLimit() {
		return this.additionAutorenewDataPackUsageLimit;
	}

	public void setAdditionAutorenewDataPackUsageLimit(
			Long additionAutorenewDataPackUsageLimit) {
		this.additionAutorenewDataPackUsageLimit = additionAutorenewDataPackUsageLimit;
	}

	@Column(name = "VALIDITY")
	public Integer getValidity() {
		return this.validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	@Column(name = "DAILY_AUTO_RENEW")
	public Boolean getDailyAutoRenew() {
		return this.dailyAutoRenew;
	}

	public void setDailyAutoRenew(Boolean dailyAutoRenew) {
		this.dailyAutoRenew = dailyAutoRenew;
	}

	@Column(name = "DAY_BUNDLE_PACK_SNCODE")
	public Integer getDayBundlePackSncode() {
		return this.dayBundlePackSncode;
	}

	public void setDayBundlePackSncode(Integer dayBundlePackSncode) {
		this.dayBundlePackSncode = dayBundlePackSncode;
	}

	@Column(name = "DAY_PACK_FREE_DATA_SNCODE")
	public Integer getDayPackFreeDataSncode() {
		return this.dayPackFreeDataSncode;
	}

	public void setDayPackFreeDataSncode(Integer dayPackFreeDataSncode) {
		this.dayPackFreeDataSncode = dayPackFreeDataSncode;
	}

	@Column(name = "ENTITY_TYPE_KEY", length = 50)
	public String getEntityTypeKey() {
		return this.entityTypeKey;
	}

	public void setEntityTypeKey(String entityTypeKey) {
		this.entityTypeKey = entityTypeKey;
	}

	@Column(name = "CHARGING_NAME", length = 50)
	public String getChargingName() {
		return this.chargingName;
	}

	public void setChargingName(String chargingName) {
		this.chargingName = chargingName;
	}

	@Column(name = "ACTIVE", nullable = false)
	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}