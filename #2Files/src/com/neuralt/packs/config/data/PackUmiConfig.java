package com.neuralt.packs.config.data;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "PACK_UMI_CONFIG", uniqueConstraints = @UniqueConstraint(columnNames = "NAME"))
public class PackUmiConfig implements java.io.Serializable, PackConfig {

	private static final long serialVersionUID = 1L;
	
	// Fields

	private Long id;
	private String name;
	private Integer validity;
	private BigDecimal price;
	private Integer priceCode;
	private String chargingName;
	private Integer bundleSncode;
	private Integer bundleSpcode;
	private String entityTypeKeySms;
	private String entityTypeKeyVoice;
	private String entityTypeKeyData;
	private Boolean autoRenew;
	private Integer dataPackSncode;
	private Integer dataPackSpcode;
	private String displayLabel;
	private String retryRules;
	private Boolean charging;
	private Boolean immediateStop;
	private Integer smsQuota;
	private Integer voiceQuota;
	private Integer smsQuotaLimit;
	private Integer voiceQuotaLimit;
	private Integer dataQuotaGb;
	private Integer freePromoDataSncode;
	private Date freePromoStartDate;
	private Date freePromoEndDate;
	private Integer freePromoDataQuotaGb;
	private Integer renewPromoDataSncode;
	private Date renewPromoStartDate;
	private Date renewPromoEndDate;
	private Integer renewPromoDataQuotaMb;
	private Integer renewPromoDataValidity;
	private Integer freebiesIdx;
	private Integer dataBonusSncode;
	private Integer dataBonusQuotaMb;
	private String entityCategory;
	private Boolean active;

	// Constructors

	/** default constructor */
	public PackUmiConfig() {
	}

	/** minimal constructor */
	public PackUmiConfig(String name, Boolean active) {
		this.name = name;
		this.active = active;
	}

	/** full constructor */
	public PackUmiConfig(String name, Integer validity, BigDecimal price,
			Integer priceCode, String chargingName, Integer bundleSncode,
			Integer bundleSpcode, String entityTypeKeySms,
			String entityTypeKeyVoice, String entityTypeKeyData,
			Boolean autoRenew, Integer dataPackSncode, Integer dataPackSpcode,
			String displayLabel, String retryRules, Boolean charging,
			Boolean immediateStop, Integer smsQuota, Integer voiceQuota,
			Integer smsQuotaLimit, Integer voiceQuotaLimit,
			Integer dataQuotaGb, Integer freePromoDataSncode,
			Date freePromoStartDate, Date freePromoEndDate,
			Integer freePromoDataQuotaGb, Integer renewPromoDataSncode,
			Date renewPromoStartDate, Date renewPromoEndDate,
			Integer renewPromoDataQuotaMb, Integer renewPromoDataValidity,
			Integer freebiesIdx, Integer dataBonusSncode,
			Integer dataBonusQuotaMb, String entityCategory, Boolean active) {
		this.name = name;
		this.validity = validity;
		this.price = price;
		this.priceCode = priceCode;
		this.chargingName = chargingName;
		this.bundleSncode = bundleSncode;
		this.bundleSpcode = bundleSpcode;
		this.entityTypeKeySms = entityTypeKeySms;
		this.entityTypeKeyVoice = entityTypeKeyVoice;
		this.entityTypeKeyData = entityTypeKeyData;
		this.autoRenew = autoRenew;
		this.dataPackSncode = dataPackSncode;
		this.dataPackSpcode = dataPackSpcode;
		this.displayLabel = displayLabel;
		this.retryRules = retryRules;
		this.charging = charging;
		this.immediateStop = immediateStop;
		this.smsQuota = smsQuota;
		this.voiceQuota = voiceQuota;
		this.smsQuotaLimit = smsQuotaLimit;
		this.voiceQuotaLimit = voiceQuotaLimit;
		this.dataQuotaGb = dataQuotaGb;
		this.freePromoDataSncode = freePromoDataSncode;
		this.freePromoStartDate = freePromoStartDate;
		this.freePromoEndDate = freePromoEndDate;
		this.freePromoDataQuotaGb = freePromoDataQuotaGb;
		this.renewPromoDataSncode = renewPromoDataSncode;
		this.renewPromoStartDate = renewPromoStartDate;
		this.renewPromoEndDate = renewPromoEndDate;
		this.renewPromoDataQuotaMb = renewPromoDataQuotaMb;
		this.renewPromoDataValidity = renewPromoDataValidity;
		this.freebiesIdx = freebiesIdx;
		this.dataBonusSncode = dataBonusSncode;
		this.dataBonusQuotaMb = dataBonusQuotaMb;
		this.entityCategory = entityCategory;
		this.active = active;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Long getId() {
		return this.id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "NAME", unique = true, nullable = false, length = 50)
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name = "VALIDITY")
	public Integer getValidity() {
		return this.validity;
	}

	public void setValidity(Integer validity) {
		this.validity = validity;
	}

	@Column(name = "PRICE", precision = 10)
	public BigDecimal getPrice() {
		return this.price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	@Column(name = "PRICE_CODE")
	public Integer getPriceCode() {
		return this.priceCode;
	}

	public void setPriceCode(Integer priceCode) {
		this.priceCode = priceCode;
	}

	@Column(name = "CHARGING_NAME", length = 50)
	public String getChargingName() {
		return this.chargingName;
	}

	public void setChargingName(String chargingName) {
		this.chargingName = chargingName;
	}

	@Column(name = "BUNDLE_SNCODE")
	public Integer getBundleSncode() {
		return this.bundleSncode;
	}

	public void setBundleSncode(Integer bundleSncode) {
		this.bundleSncode = bundleSncode;
	}

	@Column(name = "BUNDLE_SPCODE")
	public Integer getBundleSpcode() {
		return this.bundleSpcode;
	}

	public void setBundleSpcode(Integer bundleSpcode) {
		this.bundleSpcode = bundleSpcode;
	}

	@Column(name = "ENTITY_TYPE_KEY_SMS", length = 50)
	public String getEntityTypeKeySms() {
		return this.entityTypeKeySms;
	}

	public void setEntityTypeKeySms(String entityTypeKeySms) {
		this.entityTypeKeySms = entityTypeKeySms;
	}

	@Column(name = "ENTITY_TYPE_KEY_VOICE", length = 50)
	public String getEntityTypeKeyVoice() {
		return this.entityTypeKeyVoice;
	}

	public void setEntityTypeKeyVoice(String entityTypeKeyVoice) {
		this.entityTypeKeyVoice = entityTypeKeyVoice;
	}

	@Column(name = "ENTITY_TYPE_KEY_DATA", length = 50)
	public String getEntityTypeKeyData() {
		return this.entityTypeKeyData;
	}

	public void setEntityTypeKeyData(String entityTypeKeyData) {
		this.entityTypeKeyData = entityTypeKeyData;
	}

	@Column(name = "AUTO_RENEW")
	public Boolean getAutoRenew() {
		return this.autoRenew;
	}

	public void setAutoRenew(Boolean autoRenew) {
		this.autoRenew = autoRenew;
	}

	@Column(name = "DATA_PACK_SNCODE")
	public Integer getDataPackSncode() {
		return this.dataPackSncode;
	}

	public void setDataPackSncode(Integer dataPackSncode) {
		this.dataPackSncode = dataPackSncode;
	}

	@Column(name = "DATA_PACK_SPCODE")
	public Integer getDataPackSpcode() {
		return this.dataPackSpcode;
	}

	public void setDataPackSpcode(Integer dataPackSpcode) {
		this.dataPackSpcode = dataPackSpcode;
	}

	@Column(name = "DISPLAY_LABEL", length = 50)
	public String getDisplayLabel() {
		return this.displayLabel;
	}

	public void setDisplayLabel(String displayLabel) {
		this.displayLabel = displayLabel;
	}

	@Column(name = "RETRY_RULES", length = 128)
	public String getRetryRules() {
		return this.retryRules;
	}

	public void setRetryRules(String retryRules) {
		this.retryRules = retryRules;
	}

	@Column(name = "CHARGING")
	public Boolean getCharging() {
		return this.charging;
	}

	public void setCharging(Boolean charging) {
		this.charging = charging;
	}

	@Column(name = "IMMEDIATE_STOP")
	public Boolean getImmediateStop() {
		return this.immediateStop;
	}

	public void setImmediateStop(Boolean immediateStop) {
		this.immediateStop = immediateStop;
	}

	@Column(name = "SMS_QUOTA")
	public Integer getSmsQuota() {
		return this.smsQuota;
	}

	public void setSmsQuota(Integer smsQuota) {
		this.smsQuota = smsQuota;
	}

	@Column(name = "VOICE_QUOTA")
	public Integer getVoiceQuota() {
		return this.voiceQuota;
	}

	public void setVoiceQuota(Integer voiceQuota) {
		this.voiceQuota = voiceQuota;
	}

	@Column(name = "SMS_QUOTA_LIMIT")
	public Integer getSmsQuotaLimit() {
		return this.smsQuotaLimit;
	}

	public void setSmsQuotaLimit(Integer smsQuotaLimit) {
		this.smsQuotaLimit = smsQuotaLimit;
	}

	@Column(name = "VOICE_QUOTA_LIMIT")
	public Integer getVoiceQuotaLimit() {
		return this.voiceQuotaLimit;
	}

	public void setVoiceQuotaLimit(Integer voiceQuotaLimit) {
		this.voiceQuotaLimit = voiceQuotaLimit;
	}

	@Column(name = "DATA_QUOTA_GB")
	public Integer getDataQuotaGb() {
		return this.dataQuotaGb;
	}

	public void setDataQuotaGb(Integer dataQuotaGb) {
		this.dataQuotaGb = dataQuotaGb;
	}

	@Column(name = "FREE_PROMO_DATA_SNCODE")
	public Integer getFreePromoDataSncode() {
		return this.freePromoDataSncode;
	}

	public void setFreePromoDataSncode(Integer freePromoDataSncode) {
		this.freePromoDataSncode = freePromoDataSncode;
	}

	@Column(name = "FREE_PROMO_START_DATE")
	public Date getFreePromoStartDate() {
		return this.freePromoStartDate;
	}

	public void setFreePromoStartDate(Date freePromoStartDate) {
		this.freePromoStartDate = freePromoStartDate;
	}

	@Column(name = "FREE_PROMO_END_DATE")
	public Date getFreePromoEndDate() {
		return this.freePromoEndDate;
	}

	public void setFreePromoEndDate(Date freePromoEndDate) {
		this.freePromoEndDate = freePromoEndDate;
	}

	@Column(name = "FREE_PROMO_DATA_QUOTA_GB")
	public Integer getFreePromoDataQuotaGb() {
		return this.freePromoDataQuotaGb;
	}

	public void setFreePromoDataQuotaGb(Integer freePromoDataQuotaGb) {
		this.freePromoDataQuotaGb = freePromoDataQuotaGb;
	}

	@Column(name = "RENEW_PROMO_DATA_SNCODE")
	public Integer getRenewPromoDataSncode() {
		return this.renewPromoDataSncode;
	}

	public void setRenewPromoDataSncode(Integer renewPromoDataSncode) {
		this.renewPromoDataSncode = renewPromoDataSncode;
	}

	@Column(name = "RENEW_PROMO_START_DATE")
	public Date getRenewPromoStartDate() {
		return this.renewPromoStartDate;
	}

	public void setRenewPromoStartDate(Date renewPromoStartDate) {
		this.renewPromoStartDate = renewPromoStartDate;
	}

	@Column(name = "RENEW_PROMO_END_DATE")
	public Date getRenewPromoEndDate() {
		return this.renewPromoEndDate;
	}

	public void setRenewPromoEndDate(Date renewPromoEndDate) {
		this.renewPromoEndDate = renewPromoEndDate;
	}

	@Column(name = "RENEW_PROMO_DATA_QUOTA_MB")
	public Integer getRenewPromoDataQuotaMb() {
		return this.renewPromoDataQuotaMb;
	}

	public void setRenewPromoDataQuotaMb(Integer renewPromoDataQuotaMb) {
		this.renewPromoDataQuotaMb = renewPromoDataQuotaMb;
	}

	@Column(name = "RENEW_PROMO_DATA_VALIDITY")
	public Integer getRenewPromoDataValidity() {
		return this.renewPromoDataValidity;
	}

	public void setRenewPromoDataValidity(Integer renewPromoDataValidity) {
		this.renewPromoDataValidity = renewPromoDataValidity;
	}

	@Column(name = "FREEBIES_IDX")
	public Integer getFreebiesIdx() {
		return this.freebiesIdx;
	}

	public void setFreebiesIdx(Integer freebiesIdx) {
		this.freebiesIdx = freebiesIdx;
	}

	@Column(name = "DATA_BONUS_SNCODE")
	public Integer getDataBonusSncode() {
		return this.dataBonusSncode;
	}

	public void setDataBonusSncode(Integer dataBonusSncode) {
		this.dataBonusSncode = dataBonusSncode;
	}

	@Column(name = "DATA_BONUS_QUOTA_MB")
	public Integer getDataBonusQuotaMb() {
		return this.dataBonusQuotaMb;
	}

	public void setDataBonusQuotaMb(Integer dataBonusQuotaMb) {
		this.dataBonusQuotaMb = dataBonusQuotaMb;
	}

	@Column(name = "ENTITY_CATEGORY", length = 50)
	public String getEntityCategory() {
		return this.entityCategory;
	}

	public void setEntityCategory(String entityCategory) {
		this.entityCategory = entityCategory;
	}

	@Column(name = "ACTIVE", nullable = false)
	public Boolean getActive() {
		return this.active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

}