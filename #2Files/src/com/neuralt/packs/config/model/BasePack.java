package com.neuralt.packs.config.model;

import java.util.List;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.packs.config.data.PackConfig;
import com.neuralt.packs.config.model.PackageException.Reason;
import com.neuralt.smp.client.data.dao.BaseDAO;

abstract public class BasePack<C extends PackConfig> implements Package{

	protected final Logger log;
	
	protected final Class<C> configType;
	
	protected C config;
	
	protected BasePack(Class<C> configType) {
		this.log = initLog();
		this.configType = configType;
		try {
			this.config = configType.newInstance();
		}catch (Exception e) {
			log.error("failed create new instance of type ["+configType.getName()+"]", e);
		}
	}
	
	protected Logger initLog(){
		return LoggerFactory.getLogger(this.getClass());
	}
	public Logger getLog() {
		return log;
	}

	public Class<C> getConfigType() {
		return configType;
	}
	
	abstract public BaseDAO<C> getConfigDAO();

	public C getConfig() {
		return config;
	}
	public void setConfig(C config) {
		this.config = config;
	}

	@Override
	public Long getId() {
		return config.getId();
	}

	@Override
	public void setId(Long id) {
		config.setId(id);
	}

	@Override
	public String getName() {
		return config.getName();
	}

	@Override
	public void setName(String name) {
		config.setName(name);
	}

	@Override
	public boolean isActive() {
		return config.getActive() != null && config.getActive();
	}

	@Override
	public void setActive(boolean active) {
		config.setActive(active);
	}
	
	protected C findByName(String name){
		List<C> list = getConfigDAO().findByProperty(PackConfig.NAME, name);
		if(list.isEmpty()){
			return null;
		}
		return list.get(0);
	}

	@Override
	public void load() throws PackageException {
		C found = null;
		if(config.getId() != null){
			found = getConfigDAO().findById(config.getId());
		}else if(config.getName() != null){
			found = findByName(config.getName());
		}else{
			log.error("package ID/Name not set");
			throw new PackageException(Reason.NOT_EXIST);
		}
		if(found == null){
			log.error("not found package ["+(config.getId()==null?config.getName():config.getId())+"]");
			throw new PackageException(Reason.NOT_EXIST);
		}else{
			config = found;
		}
	}

	@Override
	public void save() throws PackageException {
		C exist = findByName(config.getName());
		if(exist != null && !exist.getId().equals(config.getId())){
			log.error("package ["+config.getName()+"] already exist");
			throw new PackageException(Reason.ALREADY_EXIST);
		}
		if(config.getId() == null){
			//create
			getConfigDAO().save(config);
		}else{
			//update
			getConfigDAO().merge(config);
		}
	}

	@Override
	public void delete() throws PackageException {
		//reload and check status
		load();
		if(config.getActive()){
			log.error("package ["+config.getName()+"] still active; reject deletion");
			throw new PackageException(Reason.STILL_ACTIVE);
		}
		//delete
		getConfigDAO().delete(config);
	}
	
	@Override
	public String toString(){
		return this.getClass().getSimpleName()+" - "
				+ToStringBuilder.reflectionToString(config,ToStringStyle.JSON_STYLE);
	}

}
