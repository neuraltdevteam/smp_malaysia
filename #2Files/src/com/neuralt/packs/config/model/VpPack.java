package com.neuralt.packs.config.model;

import java.math.BigDecimal;

import com.neuralt.packs.config.dao.PacksDAO;
import com.neuralt.packs.config.data.PackVpConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public class VpPack extends BasePack<PackVpConfig>{

	public VpPack() {
		super(PackVpConfig.class);
	}

	@Override
	public BaseDAO<PackVpConfig> getConfigDAO(){
		return PacksDAO.VP;
	}
	
	public Integer getValidity() {
		return config.getValidity();
	}

	public void setValidity(Integer validity) {
		config.setValidity(validity);
	}

	public BigDecimal getPrice() {
		return config.getPrice();
	}

	public void setPrice(BigDecimal price) {
		config.setPrice(price);
	}

	public Integer getPriceCode() {
		return config.getPriceCode();
	}

	public void setPriceCode(Integer priceCode) {
		config.setPriceCode(priceCode);
	}

	public String getChargingName() {
		return config.getChargingName();
	}

	public void setChargingName(String chargingName) {
		config.setChargingName(chargingName);
	}

	public Integer getBundleSncode() {
		return config.getBundleSncode();
	}

	public void setBundleSncode(Integer sncode) {
		config.setBundleSncode(sncode);
	}

	public Integer getBundleSpcode() {
		return config.getBundleSpcode();
	}

	public void setBundleSpcode(Integer spcode) {
		config.setBundleSpcode(spcode);
	}

	public Boolean getAutoRenew() {
		return config.getAutoRenew();
	}

	public void setAutoRenew(Boolean autoRenew) {
		config.setAutoRenew(autoRenew);
	}

	public String getDisplayLabel() {
		return config.getDisplayLabel();
	}

	public void setDisplayLabel(String displayLabel) {
		config.setDisplayLabel(displayLabel);
	}

	public Long getPackAmount() {
		return config.getPackAmount();
	}

	public void setPackAmount(Long packAmount) {
		config.setPackAmount(packAmount);
	}

	public Integer getUnit() {
		return config.getUnit();
	}

	public void setUnit(Integer unit) {
		config.setUnit(unit);
	}
	
	public String getEntityCategory() {
		return config.getEntityCategory();
	}

	public void setEntityCategory(String entityCategory) {
		config.setEntityCategory(entityCategory);
	}

	public String getEntityTypeKeyOnnet() {
		return config.getEntityTypeKeyOnnet();
	}

	public void setEntityTypeKeyOnnet(String entityTypeKeyOnnet) {
		config.setEntityTypeKeyOnnet(entityTypeKeyOnnet);
	}

	public String getEntityTypeKeyOffnet() {
		return config.getEntityTypeKeyOffnet();
	}

	public void setEntityTypeKeyOffnet(String entityTypeKeyOffnet) {
		config.setEntityTypeKeyOffnet(entityTypeKeyOffnet);
	}
	
}
