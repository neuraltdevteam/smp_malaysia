package com.neuralt.packs.config.model;

import java.math.BigDecimal;

import com.neuralt.packs.config.dao.PacksDAO;
import com.neuralt.packs.config.data.PackBoosterConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public class BoosterPack extends BasePack<PackBoosterConfig>{

	public BoosterPack() {
		super(PackBoosterConfig.class);
	}
	
	@Override
	public BaseDAO<PackBoosterConfig> getConfigDAO(){
		return PacksDAO.BOOSTER;
	}

	public Integer getBundleSncode() {
		return config.getBundleSncode();
	}

	public void setBundleSncode(Integer bundlePackSncode) {
		config.setBundleSncode(bundlePackSncode);
	}

	public Integer getDataPackSncode() {
		return config.getDataPackSncode();
	}

	public void setDataPackSncode(Integer dataPackSncode) {
		config.setDataPackSncode(dataPackSncode);
	}

	public Integer getDataPackSpcode() {
		return config.getDataPackSpcode();
	}

	public void setDataPackSpcode(Integer dataPackSpcode) {
		config.setDataPackSpcode(dataPackSpcode);
	}

	public BigDecimal getPrice() {
		return config.getPrice();
	}

	public void setPrice(BigDecimal price) {
		config.setPrice(price);
	}

	public Integer getPriceCode() {
		return config.getPriceCode();
	}

	public void setPriceCode(Integer priceCode) {
		config.setPriceCode(priceCode);
	}

	public String getChargingName() {
		return config.getChargingName();
	}

	public void setChargingName(String chargingName) {
		config.setChargingName(chargingName);
	}

	public String getDisplayLabel() {
		return config.getDisplayLabel();
	}

	public void setDisplayLabel(String displayLabel) {
		config.setDisplayLabel(displayLabel);
	}

	public String getPlanName() {
		return config.getPlanName();
	}

	public void setPlanName(String planName) {
		config.setPlanName(planName);
	}

	public String getSubscriberType() {
		return config.getSubscriberType();
	}

	public void setSubscriberType(String subscriberType) {
		config.setSubscriberType(subscriberType);
	}

	public String getSize() {
		return config.getSize();
	}

	public void setSize(String size) {
		config.setSize(size);
	}

	public Integer getValidity() {
		return config.getValidity();
	}

	public void setValidity(Integer validity) {
		config.setValidity(validity);
	}

	public Integer getAccumulatorId() {
		return config.getAccumulatorId();
	}

	public void setAccumulatorId(Integer accumulatorId) {
		config.setAccumulatorId(accumulatorId);
	}
	
}
