package com.neuralt.packs.config.model;

public class PackageException extends Exception{

	private static final long serialVersionUID = 1L;
	
	public static enum Reason{
		NONE,
		ALREADY_EXIST, NOT_EXIST,
		STILL_ACTIVE, NOT_ACTIVE
	}

	protected Reason reason = Reason.NONE;

	public Reason getReason() {
		return reason;
	}
	public void setReason(Reason reason) {
		this.reason = reason;
	}
	
	public PackageException(Reason reason) {
		super();
		this.reason = reason;
	}
	
	public PackageException(Reason reason, String message, Throwable cause) {
		super(message, cause);
		this.reason = reason;
	}
	
	public PackageException(Reason reason, String message) {
		super(message);
		this.reason = reason;
	}
	
	public PackageException(Reason reason, Throwable cause) {
		super(cause);
		this.reason = reason;
	}
	
}
