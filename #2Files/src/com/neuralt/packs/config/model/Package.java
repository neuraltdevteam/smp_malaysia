package com.neuralt.packs.config.model;

public interface Package {
	
	public Long getId();
	public void setId(Long id);

	public String getName();
	public void setName(String name);
	
	public boolean isActive();
	public void setActive(boolean active);
	
	public void load() throws PackageException;
	public void save() throws PackageException;
	public void delete() throws PackageException;
	
}
