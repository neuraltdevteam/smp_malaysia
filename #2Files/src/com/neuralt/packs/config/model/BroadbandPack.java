package com.neuralt.packs.config.model;

import java.math.BigDecimal;

import com.neuralt.packs.config.dao.PacksDAO;
import com.neuralt.packs.config.data.PackBroadbandConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public class BroadbandPack extends BasePack<PackBroadbandConfig>{

	public BroadbandPack() {
		super(PackBroadbandConfig.class);
	}
	
	@Override
	public BaseDAO<PackBroadbandConfig> getConfigDAO(){
		return PacksDAO.BROADBAND;
	}

	public Integer getValidity() {
		return config.getValidity();
	}

	public void setValidity(Integer validity) {
		config.setValidity(validity);
	}

	public BigDecimal getPrice() {
		return config.getPrice();
	}

	public void setPrice(BigDecimal price) {
		config.setPrice(price);
	}

	public Integer getPriceCode() {
		return config.getPriceCode();
	}

	public void setPriceCode(Integer priceCode) {
		config.setPriceCode(priceCode);
	}

	public Long getPackSize() {
		return config.getPackSize();
	}

	public void setPackSize(Long packSize) {
		config.setPackSize(packSize);
	}

	public Long getBonusPackSize() {
		return config.getBonusPackSize();
	}

	public void setBonusPackSize(Long bonusPackSize) {
		config.setBonusPackSize(bonusPackSize);
	}

	public String getChargingName() {
		return config.getChargingName();
	}

	public void setChargingName(String chargingName) {
		config.setChargingName(chargingName);
	}

	public Integer getBundleSncode() {
		return config.getBundleSncode();
	}

	public void setBundleSncode(Integer bundleSncode) {
		config.setBundleSncode(bundleSncode);
	}

	public Integer getBundleSpcode() {
		return config.getBundleSpcode();
	}

	public void setBundleSpcode(Integer bundleSpcode) {
		config.setBundleSpcode(bundleSpcode);
	}

	public String getSubscriberType() {
		return config.getSubscriberType();
	}

	public void setSubscriberType(String subscriberType) {
		config.setSubscriberType(subscriberType);
	}

	public Integer getDataPackSncode() {
		return config.getDataPackSncode();
	}

	public void setDataPackSncode(Integer dataPackSncode) {
		config.setDataPackSncode(dataPackSncode);
	}

	public Integer getDataPackSpcode() {
		return config.getDataPackSpcode();
	}

	public void setDataPackSpcode(Integer dataPackSpcode) {
		config.setDataPackSpcode(dataPackSpcode);
	}

	public Integer getBonusBundleSncode() {
		return config.getBonusBundleSncode();
	}

	public void setBonusBundleSncode(Integer bonusBundleSncode) {
		config.setBonusBundleSncode(bonusBundleSncode);
	}

	public Integer getBonusDataPackSncode() {
		return config.getBonusDataPackSncode();
	}

	public void setBonusDataPackSncode(Integer bonusBundlePackSncode) {
		config.setBonusDataPackSncode(bonusBundlePackSncode);
	}

	public Integer getBonusDataPackSpcode() {
		return config.getBonusDataPackSpcode();
	}

	public void setBonusDataPackSpcode(Integer bonusBundlePackSpcode) {
		config.setBonusDataPackSpcode(bonusBundlePackSpcode);
	}

	public Integer getFreeAutoRenewCount() {
		return config.getFreeAutoRenewCount();
	}

	public void setFreeAutoRenewCount(Integer freeAutoRenewCount) {
		config.setFreeAutoRenewCount(freeAutoRenewCount);
	}

	public String getDisplayLabel() {
		return config.getDisplayLabel();
	}

	public void setDisplayLabel(String displayLabel) {
		config.setDisplayLabel(displayLabel);
	}

	public String getRetryRules() {
		return config.getRetryRules();
	}

	public void setRetryRules(String retryRules) {
		config.setRetryRules(retryRules);
	}

	public Boolean getCharging() {
		return config.getCharging();
	}

	public void setCharging(Boolean charging) {
		config.setCharging(charging);
	}

	public Boolean getImmediateStop() {
		return config.getImmediateStop();
	}

	public void setImmediateStop(Boolean immediateStop) {
		config.setImmediateStop(immediateStop);
	}

	public Boolean getPromoPack() {
		return config.getPromoPack();
	}

	public void setPromoPack(Boolean promoPack) {
		config.setPromoPack(promoPack);
	}

	public Boolean getSendSms() {
		return config.getSendSms();
	}

	public void setSendSms(Boolean sendSms) {
		config.setSendSms(sendSms);
	}

	public Boolean getAutoRenew() {
		return config.getAutoRenew();
	}

	public void setAutoRenew(Boolean autoRenew) {
		config.setAutoRenew(autoRenew);
	}

	public String getBasePlan() {
		return config.getBasePlan();
	}

	public void setBasePlan(String basePlan) {
		config.setBasePlan(basePlan);
	}

	public Integer getFreebiesIdx() {
		return config.getFreebiesIdx();
	}

	public void setFreebiesIdx(Integer freebiesIdx) {
		config.setFreebiesIdx(freebiesIdx);
	}

	public Integer getFreeUsageOfferid() {
		return config.getFreeUsageOfferid();
	}

	public void setFreeUsageOfferid(Integer freeUsageOfferid) {
		config.setFreeUsageOfferid(freeUsageOfferid);
	}

	public String getEntityTypeKey() {
		return config.getEntityTypeKey();
	}

	public void setEntityTypeKey(String entityTypeKey) {
		config.setEntityTypeKey(entityTypeKey);
	}

	public String getEntityCategory() {
		return config.getEntityCategory();
	}

	public void setEntityCategory(String entityCategory) {
		config.setEntityCategory(entityCategory);
	}

	public Integer getFreeDataPackSncode() {
		return config.getFreeDataPackSncode();
	}

	public void setFreeDataPackSncode(Integer freeDataPackSncode) {
		config.setFreeDataPackSncode(freeDataPackSncode);
	}

	public Integer getSupervisionExtendPeriod() {
		return config.getSupervisionExtendPeriod();
	}

	public void setSupervisionExtendPeriod(Integer supervisionExtendPeriod) {
		config.setSupervisionExtendPeriod(supervisionExtendPeriod);
	}

	public Integer getServiceFeeExtendPeriod() {
		return config.getServiceFeeExtendPeriod();
	}

	public void setServiceFeeExtendPeriod(Integer serviceFeeExtendPeriod) {
		config.setServiceFeeExtendPeriod(serviceFeeExtendPeriod);
	}
	
}
