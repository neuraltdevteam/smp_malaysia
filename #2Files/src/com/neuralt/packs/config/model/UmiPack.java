package com.neuralt.packs.config.model;

import java.math.BigDecimal;
import java.util.Date;

import com.neuralt.packs.config.dao.PacksDAO;
import com.neuralt.packs.config.data.PackUmiConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public class UmiPack extends BasePack<PackUmiConfig>{

	public UmiPack() {
		super(PackUmiConfig.class);
	}
	
	@Override
	public BaseDAO<PackUmiConfig> getConfigDAO(){
		return PacksDAO.UMI;
	}

	public Integer getValidity() {
		return config.getValidity();
	}

	public void setValidity(Integer validity) {
		config.setValidity(validity);
	}

	public BigDecimal getPrice() {
		return config.getPrice();
	}

	public void setPrice(BigDecimal price) {
		config.setPrice(price);
	}

	public Integer getPriceCode() {
		return config.getPriceCode();
	}

	public void setPriceCode(Integer priceCode) {
		config.setPriceCode(priceCode);
	}

	public String getChargingName() {
		return config.getChargingName();
	}

	public void setChargingName(String chargingName) {
		config.setChargingName(chargingName);
	}

	public Integer getBundleSncode() {
		return config.getBundleSncode();
	}

	public void setBundleSncode(Integer bundleSncode) {
		config.setBundleSncode(bundleSncode);
	}

	public Integer getBundleSpcode() {
		return config.getBundleSpcode();
	}

	public void setBundleSpcode(Integer bundleSpcode) {
		config.setBundleSpcode(bundleSpcode);
	}

	public String getEntityTypeKeySms() {
		return config.getEntityTypeKeySms();
	}

	public void setEntityTypeKeySms(String entityTypeKeySms) {
		config.setEntityTypeKeySms(entityTypeKeySms);
	}

	public String getEntityTypeKeyVoice() {
		return config.getEntityTypeKeyVoice();
	}

	public void setEntityTypeKeyVoice(String entityTypeKeyVoice) {
		config.setEntityTypeKeyVoice(entityTypeKeyVoice);
	}

	public String getEntityTypeKeyData() {
		return config.getEntityTypeKeyData();
	}

	public void setEntityTypeKeyData(String entityTypeKeyData) {
		config.setEntityTypeKeyData(entityTypeKeyData);
	}

	public Boolean getAutoRenew() {
		return config.getAutoRenew();
	}

	public void setAutoRenew(Boolean autoRenew) {
		config.setAutoRenew(autoRenew);
	}

	public Integer getDataPackSncode() {
		return config.getDataPackSncode();
	}

	public void setDataPackSncode(Integer dataPackSncode) {
		config.setDataPackSncode(dataPackSncode);
	}

	public Integer getDataPackSpcode() {
		return config.getDataPackSpcode();
	}

	public void setDataPackSpcode(Integer dataPackSpcode) {
		config.setDataPackSpcode(dataPackSpcode);
	}

	public String getDisplayLabel() {
		return config.getDisplayLabel();
	}

	public void setDisplayLabel(String displayLabel) {
		config.setDisplayLabel(displayLabel);
	}

	public String getRetryRules() {
		return config.getRetryRules();
	}

	public void setRetryRules(String retryRules) {
		config.setRetryRules(retryRules);
	}

	public Boolean getCharging() {
		return config.getCharging();
	}

	public void setCharging(Boolean charging) {
		config.setCharging(charging);
	}

	public Boolean getImmediateStop() {
		return config.getImmediateStop();
	}

	public void setImmediateStop(Boolean immediateStop) {
		config.setImmediateStop(immediateStop);
	}

	public Integer getSmsQuota() {
		return config.getSmsQuota();
	}

	public void setSmsQuota(Integer smsQuota) {
		config.setSmsQuota(smsQuota);
	}

	public Integer getVoiceQuota() {
		return config.getVoiceQuota();
	}

	public void setVoiceQuota(Integer voiceQuota) {
		config.setVoiceQuota(voiceQuota);
	}

	public Integer getSmsQuotaLimit() {
		return config.getSmsQuotaLimit();
	}

	public void setSmsQuotaLimit(Integer smsQuotaLimit) {
		config.setSmsQuotaLimit(smsQuotaLimit);
	}

	public Integer getVoiceQuotaLimit() {
		return config.getVoiceQuotaLimit();
	}

	public void setVoiceQuotaLimit(Integer voiceQuotaLimit) {
		config.setVoiceQuotaLimit(voiceQuotaLimit);
	}

	public Integer getDataQuotaGb() {
		return config.getDataQuotaGb();
	}

	public void setDataQuotaGb(Integer dataQuotaGb) {
		config.setDataQuotaGb(dataQuotaGb);
	}

	public Integer getFreePromoDataSncode() {
		return config.getFreePromoDataSncode();
	}

	public void setFreePromoDataSncode(Integer freePromoDataSncode) {
		config.setFreePromoDataSncode(freePromoDataSncode);
	}

	public Date getFreePromoStartDate() {
		return config.getFreePromoStartDate();
	}

	public void setFreePromoStartDate(Date freePromoStartDate) {
		config.setFreePromoStartDate(freePromoStartDate);
	}

	public Date getFreePromoEndDate() {
		return config.getFreePromoEndDate();
	}

	public void setFreePromoEndDate(Date freePromoEndDate) {
		config.setFreePromoEndDate(freePromoEndDate);
	}

	public Integer getFreePromoDataQuotaGb() {
		return config.getFreePromoDataQuotaGb();
	}

	public void setFreePromoDataQuotaGb(Integer freePromoDataQuotaGb) {
		config.setFreePromoDataQuotaGb(freePromoDataQuotaGb);
	}

	public Integer getRenewPromoDataSncode() {
		return config.getRenewPromoDataSncode();
	}

	public void setRenewPromoDataSncode(Integer renewPromoDataSncode) {
		config.setRenewPromoDataSncode(renewPromoDataSncode);
	}

	public Date getRenewPromoStartDate() {
		return config.getRenewPromoStartDate();
	}

	public void setRenewPromoStartDate(Date renewPromoStartDate) {
		config.setRenewPromoStartDate(renewPromoStartDate);
	}

	public Date getRenewPromoEndDate() {
		return config.getRenewPromoEndDate();
	}

	public void setRenewPromoEndDate(Date renewPromoEndDate) {
		config.setRenewPromoEndDate(renewPromoEndDate);
	}

	public Integer getRenewPromoDataQuotaMb() {
		return config.getRenewPromoDataQuotaMb();
	}

	public void setRenewPromoDataQuotaMb(Integer renewPromoDataQuotaMb) {
		config.setRenewPromoDataQuotaMb(renewPromoDataQuotaMb);
	}

	public Integer getRenewPromoDataValidity() {
		return config.getRenewPromoDataValidity();
	}

	public void setRenewPromoDataValidity(Integer renewPromoDataValidity) {
		config.setRenewPromoDataValidity(renewPromoDataValidity);
	}

	public Integer getFreebiesIdx() {
		return config.getFreebiesIdx();
	}

	public void setFreebiesIdx(Integer freebiesIdx) {
		config.setFreebiesIdx(freebiesIdx);
	}

	public Integer getDataBonusSncode() {
		return config.getDataBonusSncode();
	}

	public void setDataBonusSncode(Integer dataBonusSncode) {
		config.setDataBonusSncode(dataBonusSncode);
	}

	public Integer getDataBonusQuotaMb() {
		return config.getDataBonusQuotaMb();
	}

	public void setDataBonusQuotaMb(Integer dataBonusQuotaMb) {
		config.setDataBonusQuotaMb(dataBonusQuotaMb);
	}
	
	public String getEntityCategory() {
		return config.getEntityCategory();
	}

	public void setEntityCategory(String entityCategory) {
		config.setEntityCategory(entityCategory);
	}
}
