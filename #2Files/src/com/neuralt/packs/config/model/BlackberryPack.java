package com.neuralt.packs.config.model;

import java.math.BigDecimal;

import com.neuralt.packs.config.dao.PacksDAO;
import com.neuralt.packs.config.data.PackBlackberryConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public class BlackberryPack extends BasePack<PackBlackberryConfig>{

	public BlackberryPack() {
		super(PackBlackberryConfig.class);
	}
	
	@Override
	public BaseDAO<PackBlackberryConfig> getConfigDAO() {
		return PacksDAO.BLACKBERRY;
	}

	public Boolean getPurchaseable() {
		return config.getPurchaseable();
	}

	public void setPurchaseable(Boolean purchaseable) {
		config.setPurchaseable(purchaseable);
	}

	public BigDecimal getPrice() {
		return config.getPrice();
	}

	public void setPrice(BigDecimal price) {
		config.setPrice(price);
	}

	public Integer getPriceCode() {
		return config.getPriceCode();
	}

	public void setPriceCode(Integer priceCode) {
		config.setPriceCode(priceCode);
	}

	public String getDisplayLabel() {
		return config.getDisplayLabel();
	}

	public void setDisplayLabel(String displayLabel) {
		config.setDisplayLabel(displayLabel);
	}

	public String getSubscriberType() {
		return config.getSubscriberType();
	}

	public void setSubscriberType(String subscriberType) {
		config.setSubscriberType(subscriberType);
	}

	public Integer getBundleSncode() {
		return config.getBundleSncode();
	}

	public void setBundleSncode(Integer bundlePackSncode) {
		config.setBundleSncode(bundlePackSncode);
	}

	public Boolean getAutoRenew() {
		return config.getAutoRenew();
	}

	public void setAutoRenew(Boolean autoRenew) {
		config.setAutoRenew(autoRenew);
	}

	public Integer getAutoRenewRetry() {
		return config.getAutoRenewRetry();
	}

	public void setAutoRenewRetry(Integer autoRenewRetry) {
		config.setAutoRenewRetry(autoRenewRetry);
	}

	public Integer getRimDataSncode() {
		return config.getRimDataSncode();
	}

	public void setRimDataSncode(Integer rimDataSncode) {
		config.setRimDataSncode(rimDataSncode);
	}

	public Integer getRimDataSpcode() {
		return config.getRimDataSpcode();
	}

	public void setRimDataSpcode(Integer rimDataSpcode) {
		config.setRimDataSpcode(rimDataSpcode);
	}

	public Integer getFreeDataSncode() {
		return config.getFreeDataSncode();
	}

	public void setFreeDataSncode(Integer freeDataSncode) {
		config.setFreeDataSncode(freeDataSncode);
	}

	public Integer getFreeDataSpcode() {
		return config.getFreeDataSpcode();
	}

	public void setFreeDataSpcode(Integer freeDataSpcode) {
		config.setFreeDataSpcode(freeDataSpcode);
	}

	public Long getFreeDataUsageLimit() {
		return config.getFreeDataUsageLimit();
	}

	public void setFreeDataUsageLimit(Long freeDataUsageLimit) {
		config.setFreeDataUsageLimit(freeDataUsageLimit);
	}

	public Boolean getAdditionAutorenewDataPackSncode() {
		return config.getAdditionAutorenewDataPackSncode();
	}

	public void setAdditionAutorenewDataPackSncode(
			Boolean additionAutorenewDataPackSncode) {
		config.setAdditionAutorenewDataPackSncode(additionAutorenewDataPackSncode);
	}

	public Long getAdditionAutorenewDataPackUsageLimit() {
		return config.getAdditionAutorenewDataPackUsageLimit();
	}

	public void setAdditionAutorenewDataPackUsageLimit(
			Long additionAutorenewDataPackUsageLimit) {
		config.setAdditionAutorenewDataPackUsageLimit(additionAutorenewDataPackUsageLimit);
	}

	public Integer getValidity() {
		return config.getValidity();
	}

	public void setValidity(Integer validity) {
		config.setValidity(validity);
	}

	public Boolean getDailyAutoRenew() {
		return config.getDailyAutoRenew();
	}

	public void setDailyAutoRenew(Boolean dailyAutoRenew) {
		config.setDailyAutoRenew(dailyAutoRenew);
	}

	public Integer getDayBundlePackSncode() {
		return config.getDayBundlePackSncode();
	}

	public void setDayBundlePackSncode(Integer dayBundlePackSncode) {
		config.setDayBundlePackSncode(dayBundlePackSncode);
	}

	public Integer getDayPackFreeDataSncode() {
		return config.getDayPackFreeDataSncode();
	}

	public void setDayPackFreeDataSncode(Integer dayPackFreeDataSncode) {
		config.setDayPackFreeDataSncode(dayPackFreeDataSncode);
	}

	public String getEntityTypeKey() {
		return config.getEntityTypeKey();
	}

	public void setEntityTypeKey(String entityTypeKey) {
		config.setEntityTypeKey(entityTypeKey);
	}

	public String getChargingName() {
		return config.getChargingName();
	}

	public void setChargingName(String chargingName) {
		config.setChargingName(chargingName);
	}
	
}
