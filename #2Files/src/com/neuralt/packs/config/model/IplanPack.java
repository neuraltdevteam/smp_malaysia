package com.neuralt.packs.config.model;

import java.math.BigDecimal;

import com.neuralt.packs.config.dao.PacksDAO;
import com.neuralt.packs.config.data.PackIplanConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public class IplanPack extends BasePack<PackIplanConfig>{

	public IplanPack() {
		super(PackIplanConfig.class);
	}
	
	@Override
	public BaseDAO<PackIplanConfig> getConfigDAO(){
		return PacksDAO.IPLAN;
	}

	public Integer getValidity() {
		return config.getValidity();
	}

	public void setValidity(Integer validity) {
		config.setValidity(validity);
	}

	public BigDecimal getPrice() {
		return config.getPrice();
	}

	public void setPrice(BigDecimal price) {
		config.setPrice(price);
	}

	public Integer getPriceCode() {
		return config.getPriceCode();
	}

	public void setPriceCode(Integer priceCode) {
		config.setPriceCode(priceCode);
	}

	public String getChargingName() {
		return config.getChargingName();
	}

	public void setChargingName(String chargingName) {
		config.setChargingName(chargingName);
	}

	public Integer getBundleSncode() {
		return config.getBundleSncode();
	}

	public void setBundleSncode(Integer bundleSncode) {
		config.setBundleSncode(bundleSncode);
	}

	public Integer getBundleSpcode() {
		return config.getBundleSpcode();
	}

	public void setBundleSpcode(Integer bundleSpcode) {
		config.setBundleSpcode(bundleSpcode);
	}

	public String getEntityTypeKeySms() {
		return config.getEntityTypeKeySms();
	}

	public void setEntityTypeKeySms(String entityTypeKeySms) {
		config.setEntityTypeKeySms(entityTypeKeySms);
	}

	public String getEntityTypeKeyVoice() {
		return config.getEntityTypeKeyVoice();
	}

	public void setEntityTypeKeyVoice(String entityTypeKeyVoice) {
		config.setEntityTypeKeyVoice(entityTypeKeyVoice);
	}

	public String getEntityTypeKeyData() {
		return config.getEntityTypeKeyData();
	}

	public void setEntityTypeKeyData(String entityTypeKeyData) {
		config.setEntityTypeKeyData(entityTypeKeyData);
	}

	public Boolean getAutoRenew() {
		return config.getAutoRenew();
	}

	public void setAutoRenew(Boolean autoRenew) {
		config.setAutoRenew(autoRenew);
	}

	public Integer getDataPackSncode() {
		return config.getDataPackSncode();
	}

	public void setDataPackSncode(Integer dataPackSncode) {
		config.setDataPackSncode(dataPackSncode);
	}

	public Integer getDataPackSpcode() {
		return config.getDataPackSpcode();
	}

	public void setDataPackSpcode(Integer dataPackSpcode) {
		config.setDataPackSpcode(dataPackSpcode);
	}

	public String getDisplayLabel() {
		return config.getDisplayLabel();
	}

	public void setDisplayLabel(String displayLabel) {
		config.setDisplayLabel(displayLabel);
	}

	public String getRetryRules() {
		return config.getRetryRules();
	}

	public void setRetryRules(String retryRules) {
		config.setRetryRules(retryRules);
	}

	public String getRetryCount() {
		return config.getRetryCount();
	}

	public void setRetryCount(String retryCount) {
		config.setRetryCount(retryCount);
	}

	public Boolean getCharging() {
		return config.getCharging();
	}

	public void setCharging(Boolean charging) {
		config.setCharging(charging);
	}

	public Boolean getImmediateStop() {
		return config.getImmediateStop();
	}

	public void setImmediateStop(Boolean immediateStop) {
		config.setImmediateStop(immediateStop);
	}

	public Integer getSmsQuota() {
		return config.getSmsQuota();
	}

	public void setSmsQuota(Integer smsQuota) {
		config.setSmsQuota(smsQuota);
	}

	public Integer getVoiceQuota() {
		return config.getVoiceQuota();
	}

	public void setVoiceQuota(Integer voiceQuota) {
		config.setVoiceQuota(voiceQuota);
	}

	public Integer getDataQuotaGb() {
		return config.getDataQuotaGb();
	}

	public void setDataQuotaGb(Integer dataQuotaGb) {
		config.setDataQuotaGb(dataQuotaGb);
	}

	public Integer getAccumulatorId() {
		return config.getAccumulatorId();
	}

	public void setAccumulatorId(Integer accumulatorId) {
		config.setAccumulatorId(accumulatorId);
	}

	public Integer getAccumulatorValueAbs() {
		return config.getAccumulatorValueAbs();
	}

	public void setAccumulatorValueAbs(Integer accumulatorValueAbs) {
		config.setAccumulatorValueAbs(accumulatorValueAbs);
	}
	
	public String getEntityCategory() {
		return config.getEntityCategory();
	}

	public void setEntityCategory(String entityCategory) {
		config.setEntityCategory(entityCategory);
	}
	
}
