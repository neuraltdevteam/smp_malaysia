package com.neuralt.packs.config.model;

import java.math.BigDecimal;

import com.neuralt.packs.config.dao.PacksDAO;
import com.neuralt.packs.config.data.PackAddonConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public class AddonPack extends BasePack<PackAddonConfig>{

	public AddonPack() {
		super(PackAddonConfig.class);
	}
	
	@Override
	public BaseDAO<PackAddonConfig> getConfigDAO() {
		return PacksDAO.ADDON;
	}

	public Integer getValidity() {
		return config.getValidity();
	}

	public void setValidity(Integer validity) {
		config.setValidity(validity);
	}

	public BigDecimal getPrice() {
		return config.getPrice();
	}

	public void setPrice(BigDecimal price) {
		config.setPrice(price);
	}

	public Integer getPriceCode() {
		return config.getPriceCode();
	}

	public void setPriceCode(Integer priceCode) {
		config.setPriceCode(priceCode);
	}

	public String getChargingName() {
		return config.getChargingName();
	}

	public void setChargingName(String chargingName) {
		config.setChargingName(chargingName);
	}

	public Integer getBundleSncode() {
		return config.getBundleSncode();
	}

	public void setBundleSncode(Integer sncode) {
		config.setBundleSncode(sncode);
	}

	public Integer getBundleSpcode() {
		return config.getBundleSpcode();
	}

	public void setBundleSpcode(Integer spcode) {
		config.setBundleSpcode(spcode);
	}

	public Boolean getAutoRenew() {
		return config.getAutoRenew();
	}

	public void setAutoRenew(Boolean autoRenew) {
		config.setAutoRenew(autoRenew);
	}

	public String getDisplayLabel() {
		return config.getDisplayLabel();
	}

	public void setDisplayLabel(String displayLabel) {
		config.setDisplayLabel(displayLabel);
	}

	public Long getPackAmount() {
		return config.getPackAmount();
	}

	public void setPackAmount(Long packAmount) {
		config.setPackAmount(packAmount);
	}

	public Integer getUnit() {
		return config.getUnit();
	}

	public void setUnit(Integer unit) {
		config.setUnit(unit);
	}

	public Integer getNumberOfSubscription() {
		return config.getNumberOfSubscription();
	}

	public void setNumberOfSubscription(Integer numberOfSubscription) {
		config.setNumberOfSubscription(numberOfSubscription);
	}

	public Integer getFamilySharePlanCounter() {
		return config.getFamilySharePlanCounter();
	}

	public void setFamilySharePlanCounter(Integer familySharePlanCounter) {
		config.setFamilySharePlanCounter(familySharePlanCounter);
	}

	public String getEntityTypeKey() {
		return config.getEntityTypeKey();
	}

	public void setEntityTypeKey(String entityTypeKey) {
		config.setEntityTypeKey(entityTypeKey);
	}

	public String getEntityCategory() {
		return config.getEntityCategory();
	}

	public void setEntityCategory(String entityCategory) {
		config.setEntityCategory(entityCategory);
	}

	public Integer getAccumulatorId() {
		return config.getAccumulatorId();
	}

	public void setAccumulatorId(Integer accumulatorId) {
		config.setAccumulatorId(accumulatorId);
	}
	
}
