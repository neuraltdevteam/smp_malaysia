package com.neuralt.packs.config.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.packs.config.data.PackBoosterConfig;
import com.neuralt.smp.client.data.dao.BaseHibernateDAO;

public class PackBoosterConfigDaoImpl extends BaseHibernateDAO<PackBoosterConfig> implements PackBoosterConfigDAO{
	
	private static final Logger log = LoggerFactory.getLogger(PackBoosterConfigDAO.class);
	
	public PackBoosterConfigDaoImpl() {
		super(PackBoosterConfig.class);
	}
	
	@Override
	protected Logger getLog() {
		return log;
	}
	
	public List<PackBoosterConfig> findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List<PackBoosterConfig> findByBundlePackSncode(
			Object bundlePackSncode) {
		return findByProperty(BUNDLE_PACK_SNCODE, bundlePackSncode);
	}

	public List<PackBoosterConfig> findByDataPackSncode(Object dataPackSncode) {
		return findByProperty(DATA_PACK_SNCODE, dataPackSncode);
	}

	public List<PackBoosterConfig> findByDataPackSpcode(Object dataPackSpcode) {
		return findByProperty(DATA_PACK_SPCODE, dataPackSpcode);
	}

	public List<PackBoosterConfig> findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List<PackBoosterConfig> findByPriceCode(Object priceCode) {
		return findByProperty(PRICE_CODE, priceCode);
	}

	public List<PackBoosterConfig> findByChargingName(Object chargingName) {
		return findByProperty(CHARGING_NAME, chargingName);
	}

	public List<PackBoosterConfig> findByDisplayLabel(Object displayLabel) {
		return findByProperty(DISPLAY_LABEL, displayLabel);
	}

	public List<PackBoosterConfig> findByPlanName(Object planName) {
		return findByProperty(PLAN_NAME, planName);
	}

	public List<PackBoosterConfig> findBySubscriberType(Object subscriberType) {
		return findByProperty(SUBSCRIBER_TYPE, subscriberType);
	}

	public List<PackBoosterConfig> findBySize(Object size) {
		return findByProperty(SIZE, size);
	}

	public List<PackBoosterConfig> findByValidity(Object validity) {
		return findByProperty(VALIDITY, validity);
	}

	public List<PackBoosterConfig> findByAccumulatorId(Object accumulatorId) {
		return findByProperty(ACCUMULATOR_ID, accumulatorId);
	}

	public List<PackBoosterConfig> findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}

}