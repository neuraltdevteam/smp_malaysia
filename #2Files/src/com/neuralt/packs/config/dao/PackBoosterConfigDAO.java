package com.neuralt.packs.config.dao;

import java.util.List;

import com.neuralt.packs.config.data.PackBoosterConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public interface PackBoosterConfigDAO extends BaseDAO<PackBoosterConfig>{
	
	// property constants
	public static final String NAME = "name";
	public static final String BUNDLE_PACK_SNCODE = "bundlePackSncode";
	public static final String DATA_PACK_SNCODE = "dataPackSncode";
	public static final String DATA_PACK_SPCODE = "dataPackSpcode";
	public static final String PRICE = "price";
	public static final String PRICE_CODE = "priceCode";
	public static final String CHARGING_NAME = "chargingName";
	public static final String DISPLAY_LABEL = "displayLabel";
	public static final String PLAN_NAME = "planName";
	public static final String SUBSCRIBER_TYPE = "subscriberType";
	public static final String SIZE = "size";
	public static final String VALIDITY = "validity";
	public static final String ACCUMULATOR_ID = "accumulatorId";
	public static final String ACTIVE = "active";

	public List<PackBoosterConfig> findByName(Object name);
	public List<PackBoosterConfig> findByBundlePackSncode(Object bundlePackSncode);
	public List<PackBoosterConfig> findByDataPackSncode(Object dataPackSncode);
	public List<PackBoosterConfig> findByDataPackSpcode(Object dataPackSpcode);
	public List<PackBoosterConfig> findByPrice(Object price);
	public List<PackBoosterConfig> findByPriceCode(Object priceCode);
	public List<PackBoosterConfig> findByChargingName(Object chargingName);
	public List<PackBoosterConfig> findByDisplayLabel(Object displayLabel);
	public List<PackBoosterConfig> findByPlanName(Object planName);
	public List<PackBoosterConfig> findBySubscriberType(Object subscriberType);
	public List<PackBoosterConfig> findBySize(Object size);
	public List<PackBoosterConfig> findByValidity(Object validity);
	public List<PackBoosterConfig> findByAccumulatorId(Object accumulatorId);
	public List<PackBoosterConfig> findByActive(Object active);
	
}