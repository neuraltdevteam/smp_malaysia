package com.neuralt.packs.config.dao;

import java.util.List;

import com.neuralt.packs.config.data.PackAddonConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public interface PackAddonConfigDAO extends BaseDAO<PackAddonConfig> {
	
	// property constants
	public static final String NAME = "name";
	public static final String VALIDITY = "validity";
	public static final String PRICE = "price";
	public static final String PRICE_CODE = "priceCode";
	public static final String CHARGING_NAME = "chargingName";
	public static final String SNCODE = "sncode";
	public static final String SPCODE = "spcode";
	public static final String AUTO_RENEW = "autoRenew";
	public static final String DISPLAY_LABEL = "displayLabel";
	public static final String PACK_AMOUNT = "packAmount";
	public static final String UNIT = "unit";
	public static final String NUMBER_OF_SUBSCRIPTION = "numberOfSubscription";
	public static final String FAMILY_SHARE_PLAN_COUNTER = "familySharePlanCounter";
	public static final String ENTITY_TYPE_KEY = "entityTypeKey";
	public static final String ENTITY_CATEGORY = "entityCategory";
	public static final String ACCUMULATOR_ID = "accumulatorId";
	public static final String ACTIVE = "active";

	public List<PackAddonConfig> findByName(Object name);
	public List<PackAddonConfig> findByValidity(Object validity);
	public List<PackAddonConfig> findByPrice(Object price);
	public List<PackAddonConfig> findByPriceCode(Object priceCode);
	public List<PackAddonConfig> findByChargingName(Object chargingName);
	public List<PackAddonConfig> findBySncode(Object sncode);
	public List<PackAddonConfig> findBySpcode(Object spcode);
	public List<PackAddonConfig> findByAutoRenew(Object autoRenew);
	public List<PackAddonConfig> findByDisplayLabel(Object displayLabel);
	public List<PackAddonConfig> findByPackAmount(Object packAmount);
	public List<PackAddonConfig> findByUnit(Object unit);
	public List<PackAddonConfig> findByNumberOfSubscription(Object numberOfSubscription);
	public List<PackAddonConfig> findByFamilySharePlanCounter(Object familySharePlanCounter);
	public List<PackAddonConfig> findByEntityTypeKey(Object entityTypeKey);
	public List<PackAddonConfig> findByEntityCategory(Object entityCategory);
	public List<PackAddonConfig> findByAccumulatorId(Object accumulatorId);
	public List<PackAddonConfig> findByActive(Object active);
	
}