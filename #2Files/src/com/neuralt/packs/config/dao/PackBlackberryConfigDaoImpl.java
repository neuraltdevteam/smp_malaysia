package com.neuralt.packs.config.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.packs.config.data.PackBlackberryConfig;
import com.neuralt.smp.client.data.dao.BaseHibernateDAO;

public class PackBlackberryConfigDaoImpl extends BaseHibernateDAO<PackBlackberryConfig> implements PackBlackberryConfigDAO{
	
	private static final Logger log = LoggerFactory.getLogger(PackBlackberryConfigDAO.class);
	
	public PackBlackberryConfigDaoImpl() {
		super(PackBlackberryConfig.class);
	}
	
	@Override
	protected Logger getLog() {
		return log;
	}
	
	public List<PackBlackberryConfig> findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List<PackBlackberryConfig> findByPurchaseable(Object purchaseable) {
		return findByProperty(PURCHASEABLE, purchaseable);
	}

	public List<PackBlackberryConfig> findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List<PackBlackberryConfig> findByPriceCode(Object priceCode) {
		return findByProperty(PRICE_CODE, priceCode);
	}

	public List<PackBlackberryConfig> findByDisplayLabel(Object displayLabel) {
		return findByProperty(DISPLAY_LABEL, displayLabel);
	}

	public List<PackBlackberryConfig> findBySubscriberType(Object subscriberType) {
		return findByProperty(SUBSCRIBER_TYPE, subscriberType);
	}

	public List<PackBlackberryConfig> findByBundlePackSncode(
			Object bundlePackSncode) {
		return findByProperty(BUNDLE_PACK_SNCODE, bundlePackSncode);
	}

	public List<PackBlackberryConfig> findByAutoRenew(Object autoRenew) {
		return findByProperty(AUTO_RENEW, autoRenew);
	}

	public List<PackBlackberryConfig> findByAutoRenewRetry(Object autoRenewRetry) {
		return findByProperty(AUTO_RENEW_RETRY, autoRenewRetry);
	}

	public List<PackBlackberryConfig> findByRimDataSncode(Object rimDataSncode) {
		return findByProperty(RIM_DATA_SNCODE, rimDataSncode);
	}

	public List<PackBlackberryConfig> findByRimDataSpcode(Object rimDataSpcode) {
		return findByProperty(RIM_DATA_SPCODE, rimDataSpcode);
	}

	public List<PackBlackberryConfig> findByFreeDataSncode(Object freeDataSncode) {
		return findByProperty(FREE_DATA_SNCODE, freeDataSncode);
	}

	public List<PackBlackberryConfig> findByFreeDataSpcode(Object freeDataSpcode) {
		return findByProperty(FREE_DATA_SPCODE, freeDataSpcode);
	}

	public List<PackBlackberryConfig> findByFreeDataUsageLimit(
			Object freeDataUsageLimit) {
		return findByProperty(FREE_DATA_USAGE_LIMIT, freeDataUsageLimit);
	}

	public List<PackBlackberryConfig> findByAdditionAutorenewDataPackSncode(
			Object additionAutorenewDataPackSncode) {
		return findByProperty(ADDITION_AUTORENEW_DATA_PACK_SNCODE,
				additionAutorenewDataPackSncode);
	}

	public List<PackBlackberryConfig> findByAdditionAutorenewDataPackUsageLimit(
			Object additionAutorenewDataPackUsageLimit) {
		return findByProperty(ADDITION_AUTORENEW_DATA_PACK_USAGE_LIMIT,
				additionAutorenewDataPackUsageLimit);
	}

	public List<PackBlackberryConfig> findByValidity(Object validity) {
		return findByProperty(VALIDITY, validity);
	}

	public List<PackBlackberryConfig> findByDailyAutoRenew(Object dailyAutoRenew) {
		return findByProperty(DAILY_AUTO_RENEW, dailyAutoRenew);
	}

	public List<PackBlackberryConfig> findByDayBundlePackSncode(
			Object dayBundlePackSncode) {
		return findByProperty(DAY_BUNDLE_PACK_SNCODE, dayBundlePackSncode);
	}

	public List<PackBlackberryConfig> findByDayPackFreeDataSncode(
			Object dayPackFreeDataSncode) {
		return findByProperty(DAY_PACK_FREE_DATA_SNCODE, dayPackFreeDataSncode);
	}

	public List<PackBlackberryConfig> findByEntityTypeKey(Object entityTypeKey) {
		return findByProperty(ENTITY_TYPE_KEY, entityTypeKey);
	}

	public List<PackBlackberryConfig> findByChargingName(Object chargingName) {
		return findByProperty(CHARGING_NAME, chargingName);
	}

	public List<PackBlackberryConfig> findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}

}