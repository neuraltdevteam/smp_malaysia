package com.neuralt.packs.config.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.packs.config.data.PackBroadbandConfig;
import com.neuralt.smp.client.data.dao.BaseHibernateDAO;

public class PackBroadbandConfigDaoImpl extends BaseHibernateDAO<PackBroadbandConfig> implements PackBroadbandConfigDAO{
	
	private static final Logger log = LoggerFactory.getLogger(PackBroadbandConfigDAO.class);
	
	public PackBroadbandConfigDaoImpl() {
		super(PackBroadbandConfig.class);
	}
	
	@Override
	protected Logger getLog() {
		return log;
	}
	
	public List<PackBroadbandConfig> findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List<PackBroadbandConfig> findByValidity(Object validity) {
		return findByProperty(VALIDITY, validity);
	}

	public List<PackBroadbandConfig> findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List<PackBroadbandConfig> findByPriceCode(Object priceCode) {
		return findByProperty(PRICE_CODE, priceCode);
	}

	public List<PackBroadbandConfig> findByPackSize(Object packSize) {
		return findByProperty(PACK_SIZE, packSize);
	}

	public List<PackBroadbandConfig> findByBonusPackSize(Object bonusPackSize) {
		return findByProperty(BONUS_PACK_SIZE, bonusPackSize);
	}

	public List<PackBroadbandConfig> findByChargingName(Object chargingName) {
		return findByProperty(CHARGING_NAME, chargingName);
	}

	public List<PackBroadbandConfig> findByBundleSncode(Object bundleSncode) {
		return findByProperty(BUNDLE_SNCODE, bundleSncode);
	}

	public List<PackBroadbandConfig> findByBundleSpcode(Object bundleSpcode) {
		return findByProperty(BUNDLE_SPCODE, bundleSpcode);
	}

	public List<PackBroadbandConfig> findBySubscriberType(Object subscriberType) {
		return findByProperty(SUBSCRIBER_TYPE, subscriberType);
	}

	public List<PackBroadbandConfig> findByDataPackSncode(Object dataPackSncode) {
		return findByProperty(DATA_PACK_SNCODE, dataPackSncode);
	}

	public List<PackBroadbandConfig> findByDataPackSpcode(Object dataPackSpcode) {
		return findByProperty(DATA_PACK_SPCODE, dataPackSpcode);
	}

	public List<PackBroadbandConfig> findByBundlePackSncode(
			Object bundlePackSncode) {
		return findByProperty(BUNDLE_PACK_SNCODE, bundlePackSncode);
	}

	public List<PackBroadbandConfig> findByBonusBundlePackSncode(
			Object bonusBundlePackSncode) {
		return findByProperty(BONUS_BUNDLE_PACK_SNCODE, bonusBundlePackSncode);
	}

	public List<PackBroadbandConfig> findByBonusBundlePackSpcode(
			Object bonusBundlePackSpcode) {
		return findByProperty(BONUS_BUNDLE_PACK_SPCODE, bonusBundlePackSpcode);
	}

	public List<PackBroadbandConfig> findByFreeAutoRenewCount(
			Object freeAutoRenewCount) {
		return findByProperty(FREE_AUTO_RENEW_COUNT, freeAutoRenewCount);
	}

	public List<PackBroadbandConfig> findByDisplayLabel(Object displayLabel) {
		return findByProperty(DISPLAY_LABEL, displayLabel);
	}

	public List<PackBroadbandConfig> findByRetryRules(Object retryRules) {
		return findByProperty(RETRY_RULES, retryRules);
	}

	public List<PackBroadbandConfig> findByCharging(Object charging) {
		return findByProperty(CHARGING, charging);
	}

	public List<PackBroadbandConfig> findByImmediateStop(Object immediateStop) {
		return findByProperty(IMMEDIATE_STOP, immediateStop);
	}

	public List<PackBroadbandConfig> findByPromoPack(Object promoPack) {
		return findByProperty(PROMO_PACK, promoPack);
	}

	public List<PackBroadbandConfig> findBySendSms(Object sendSms) {
		return findByProperty(SEND_SMS, sendSms);
	}

	public List<PackBroadbandConfig> findByAutoRenew(Object autoRenew) {
		return findByProperty(AUTO_RENEW, autoRenew);
	}

	public List<PackBroadbandConfig> findByBasePlan(Object basePlan) {
		return findByProperty(BASE_PLAN, basePlan);
	}

	public List<PackBroadbandConfig> findByFreebiesIdx(Object freebiesIdx) {
		return findByProperty(FREEBIES_IDX, freebiesIdx);
	}

	public List<PackBroadbandConfig> findByFreeUsageOfferid(
			Object freeUsageOfferid) {
		return findByProperty(FREE_USAGE_OFFERID, freeUsageOfferid);
	}

	public List<PackBroadbandConfig> findByEntityTypeKey(Object entityTypeKey) {
		return findByProperty(ENTITY_TYPE_KEY, entityTypeKey);
	}

	public List<PackBroadbandConfig> findByEntityCategory(Object entityCategory) {
		return findByProperty(ENTITY_CATEGORY, entityCategory);
	}

	public List<PackBroadbandConfig> findByFreeDataPackSncode(
			Object freeDataPackSncode) {
		return findByProperty(FREE_DATA_PACK_SNCODE, freeDataPackSncode);
	}

	public List<PackBroadbandConfig> findBySupervisionExtendPeriod(
			Object supervisionExtendPeriod) {
		return findByProperty(SUPERVISION_EXTEND_PERIOD,
				supervisionExtendPeriod);
	}

	public List<PackBroadbandConfig> findByServiceFeeExtendPeriod(
			Object serviceFeeExtendPeriod) {
		return findByProperty(SERVICE_FEE_EXTEND_PERIOD, serviceFeeExtendPeriod);
	}

	public List<PackBroadbandConfig> findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}

}