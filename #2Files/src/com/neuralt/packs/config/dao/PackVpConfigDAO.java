package com.neuralt.packs.config.dao;

import java.util.List;

import com.neuralt.packs.config.data.PackVpConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public interface PackVpConfigDAO extends BaseDAO<PackVpConfig> {
	
	// property constants
	public static final String NAME = "name";
	public static final String VALIDITY = "validity";
	public static final String PRICE = "price";
	public static final String PRICE_CODE = "priceCode";
	public static final String CHARGING_NAME = "chargingName";
	public static final String SNCODE = "sncode";
	public static final String SPCODE = "spcode";
	public static final String AUTO_RENEW = "autoRenew";
	public static final String DISPLAY_LABEL = "displayLabel";
	public static final String PACK_AMOUNT = "packAmount";
	public static final String UNIT = "unit";
	public static final String ACTIVE = "active";

	public List<PackVpConfig> findByName(Object name);
	public List<PackVpConfig> findByValidity(Object validity);
	public List<PackVpConfig> findByPrice(Object price);
	public List<PackVpConfig> findByPriceCode(Object priceCode);
	public List<PackVpConfig> findByChargingName(Object chargingName);
	public List<PackVpConfig> findBySncode(Object sncode);
	public List<PackVpConfig> findBySpcode(Object spcode);
	public List<PackVpConfig> findByAutoRenew(Object autoRenew);
	public List<PackVpConfig> findByDisplayLabel(Object displayLabel);
	public List<PackVpConfig> findByPackAmount(Object packAmount);
	public List<PackVpConfig> findByUnit(Object unit);
	public List<PackVpConfig> findByActive(Object active);
	
}