package com.neuralt.packs.config.dao;

import java.util.List;

import com.neuralt.packs.config.data.PackUmiConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public interface PackUmiConfigDAO extends BaseDAO<PackUmiConfig> {
	
	// property constants
	public static final String NAME = "name";
	public static final String VALIDITY = "validity";
	public static final String PRICE = "price";
	public static final String PRICE_CODE = "priceCode";
	public static final String CHARGING_NAME = "chargingName";
	public static final String BUNDLE_SNCODE = "bundleSncode";
	public static final String BUNDLE_SPCODE = "bundleSpcode";
	public static final String ENTITY_TYPE_KEY_SMS = "entityTypeKeySms";
	public static final String ENTITY_TYPE_KEY_VOICE = "entityTypeKeyVoice";
	public static final String ENTITY_TYPE_KEY_DATA = "entityTypeKeyData";
	public static final String AUTO_RENEW = "autoRenew";
	public static final String DATA_PACK_SNCODE = "dataPackSncode";
	public static final String DATA_PACK_SPCODE = "dataPackSpcode";
	public static final String DISPLAY_LABEL = "displayLabel";
	public static final String RETRY_RULES = "retryRules";
	public static final String CHARGING = "charging";
	public static final String IMMEDIATE_STOP = "immediateStop";
	public static final String SMS_QUOTA = "smsQuota";
	public static final String VOICE_QUOTA = "voiceQuota";
	public static final String SMS_QUOTA_LIMIT = "smsQuotaLimit";
	public static final String VOICE_QUOTA_LIMIT = "voiceQuotaLimit";
	public static final String DATA_QUOTA_GB = "dataQuotaGb";
	public static final String FREE_PROMO_DATA_SNCODE = "freePromoDataSncode";
	public static final String FREE_PROMO_DATA_QUOTA_GB = "freePromoDataQuotaGb";
	public static final String RENEW_PROMO_DATA_SNCODE = "renewPromoDataSncode";
	public static final String RENEW_PROMO_DATA_QUOTA_MB = "renewPromoDataQuotaMb";
	public static final String RENEW_PROMO_DATA_VALIDITY = "renewPromoDataValidity";
	public static final String FREEBIES_IDX = "freebiesIdx";
	public static final String DATA_BONUS_SNCODE = "dataBonusSncode";
	public static final String DATA_BONUS_QUOTA_MB = "dataBonusQuotaMb";
	public static final String ACTIVE = "active";

	public List<PackUmiConfig> findByName(Object name);
	public List<PackUmiConfig> findByValidity(Object validity);
	public List<PackUmiConfig> findByPrice(Object price);
	public List<PackUmiConfig> findByPriceCode(Object priceCode);
	public List<PackUmiConfig> findByChargingName(Object chargingName);
	public List<PackUmiConfig> findByBundleSncode(Object bundleSncode);
	public List<PackUmiConfig> findByBundleSpcode(Object bundleSpcode);
	public List<PackUmiConfig> findByEntityTypeKeySms(Object entityTypeKeySms);
	public List<PackUmiConfig> findByEntityTypeKeyVoice(Object entityTypeKeyVoice);
	public List<PackUmiConfig> findByEntityTypeKeyData(Object entityTypeKeyData);
	public List<PackUmiConfig> findByAutoRenew(Object autoRenew);
	public List<PackUmiConfig> findByDataPackSncode(Object dataPackSncode);
	public List<PackUmiConfig> findByDataPackSpcode(Object dataPackSpcode);
	public List<PackUmiConfig> findByDisplayLabel(Object displayLabel);
	public List<PackUmiConfig> findByRetryRules(Object retryRules);
	public List<PackUmiConfig> findByCharging(Object charging);
	public List<PackUmiConfig> findByImmediateStop(Object immediateStop);
	public List<PackUmiConfig> findBySmsQuota(Object smsQuota);
	public List<PackUmiConfig> findByVoiceQuota(Object voiceQuota);
	public List<PackUmiConfig> findBySmsQuotaLimit(Object smsQuotaLimit);
	public List<PackUmiConfig> findByVoiceQuotaLimit(Object voiceQuotaLimit);
	public List<PackUmiConfig> findByDataQuotaGb(Object dataQuotaGb);
	public List<PackUmiConfig> findByFreePromoDataSncode(Object freePromoDataSncode);
	public List<PackUmiConfig> findByFreePromoDataQuotaGb(Object freePromoDataQuotaGb);
	public List<PackUmiConfig> findByRenewPromoDataSncode(Object renewPromoDataSncode);
	public List<PackUmiConfig> findByRenewPromoDataQuotaMb(Object renewPromoDataQuotaMb);
	public List<PackUmiConfig> findByRenewPromoDataValidity(Object renewPromoDataValidity);
	public List<PackUmiConfig> findByFreebiesIdx(Object freebiesIdx);
	public List<PackUmiConfig> findByDataBonusSncode(Object dataBonusSncode);
	public List<PackUmiConfig> findByDataBonusQuotaMb(Object dataBonusQuotaMb);
	public List<PackUmiConfig> findByActive(Object active);

}