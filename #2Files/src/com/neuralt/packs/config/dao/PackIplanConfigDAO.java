package com.neuralt.packs.config.dao;

import java.util.List;

import com.neuralt.packs.config.data.PackIplanConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public interface PackIplanConfigDAO extends BaseDAO<PackIplanConfig> {
	
	// property constants
	public static final String NAME = "name";
	public static final String VALIDITY = "validity";
	public static final String PRICE = "price";
	public static final String PRICE_CODE = "priceCode";
	public static final String CHARGING_NAME = "chargingName";
	public static final String BUNDLE_SNCODE = "bundleSncode";
	public static final String BUNDLE_SPCODE = "bundleSpcode";
	public static final String ENTITY_TYPE_KEY_SMS = "entityTypeKeySms";
	public static final String ENTITY_TYPE_KEY_VOICE = "entityTypeKeyVoice";
	public static final String ENTITY_TYPE_KEY_DATA = "entityTypeKeyData";
	public static final String AUTO_RENEW = "autoRenew";
	public static final String DATA_PACK_SNCODE = "dataPackSncode";
	public static final String DATA_PACK_SPCODE = "dataPackSpcode";
	public static final String DISPLAY_LABEL = "displayLabel";
	public static final String RETRY_RULES = "retryRules";
	public static final String RETRY_COUNT = "retryCount";
	public static final String CHARGING = "charging";
	public static final String IMMEDIATE_STOP = "immediateStop";
	public static final String SMS_QUOTA = "smsQuota";
	public static final String VOICE_QUOTA = "voiceQuota";
	public static final String DATA_QUOTA_GB = "dataQuotaGb";
	public static final String ACCUMULATOR_ID = "accumulatorId";
	public static final String ACCUMULATOR_VALUE_ABS = "accumulatorValueAbs";
	public static final String ACTIVE = "active";

	public List<PackIplanConfig> findByName(Object name);
	public List<PackIplanConfig> findByValidity(Object validity);
	public List<PackIplanConfig> findByPrice(Object price);
	public List<PackIplanConfig> findByPriceCode(Object priceCode);
	public List<PackIplanConfig> findByChargingName(Object chargingName);
	public List<PackIplanConfig> findByBundleSncode(Object bundleSncode);
	public List<PackIplanConfig> findByBundleSpcode(Object bundleSpcode);
	public List<PackIplanConfig> findByEntityTypeKeySms(Object entityTypeKeySms);
	public List<PackIplanConfig> findByEntityTypeKeyVoice(Object entityTypeKeyVoice);
	public List<PackIplanConfig> findByEntityTypeKeyData(Object entityTypeKeyData);
	public List<PackIplanConfig> findByAutoRenew(Object autoRenew);
	public List<PackIplanConfig> findByDataPackSncode(Object dataPackSncode);
	public List<PackIplanConfig> findByDataPackSpcode(Object dataPackSpcode);
	public List<PackIplanConfig> findByDisplayLabel(Object displayLabel);
	public List<PackIplanConfig> findByRetryRules(Object retryRules);
	public List<PackIplanConfig> findByRetryCount(Object retryCount);
	public List<PackIplanConfig> findByCharging(Object charging);
	public List<PackIplanConfig> findByImmediateStop(Object immediateStop);
	public List<PackIplanConfig> findBySmsQuota(Object smsQuota);
	public List<PackIplanConfig> findByVoiceQuota(Object voiceQuota);
	public List<PackIplanConfig> findByDataQuotaGb(Object dataQuotaGb);
	public List<PackIplanConfig> findByAccumulatorId(Object accumulatorId);
	public List<PackIplanConfig> findByAccumulatorValueAbs(Object accumulatorValueAbs);
	public List<PackIplanConfig> findByActive(Object active);
	
}