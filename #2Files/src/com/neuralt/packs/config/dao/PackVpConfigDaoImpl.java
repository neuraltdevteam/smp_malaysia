package com.neuralt.packs.config.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.packs.config.data.PackVpConfig;
import com.neuralt.smp.client.data.dao.BaseHibernateDAO;

public class PackVpConfigDaoImpl extends BaseHibernateDAO<PackVpConfig> implements PackVpConfigDAO{
	
	private static final Logger log = LoggerFactory.getLogger(PackVpConfigDAO.class);
	
	public PackVpConfigDaoImpl() {
		super(PackVpConfig.class);
	}
	
	@Override
	protected Logger getLog() {
		return log;
	}
	
	public List<PackVpConfig> findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List<PackVpConfig> findByValidity(Object validity) {
		return findByProperty(VALIDITY, validity);
	}

	public List<PackVpConfig> findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List<PackVpConfig> findByPriceCode(Object priceCode) {
		return findByProperty(PRICE_CODE, priceCode);
	}

	public List<PackVpConfig> findByChargingName(Object chargingName) {
		return findByProperty(CHARGING_NAME, chargingName);
	}

	public List<PackVpConfig> findBySncode(Object sncode) {
		return findByProperty(SNCODE, sncode);
	}

	public List<PackVpConfig> findBySpcode(Object spcode) {
		return findByProperty(SPCODE, spcode);
	}

	public List<PackVpConfig> findByAutoRenew(Object autoRenew) {
		return findByProperty(AUTO_RENEW, autoRenew);
	}

	public List<PackVpConfig> findByDisplayLabel(Object displayLabel) {
		return findByProperty(DISPLAY_LABEL, displayLabel);
	}

	public List<PackVpConfig> findByPackAmount(Object packAmount) {
		return findByProperty(PACK_AMOUNT, packAmount);
	}

	public List<PackVpConfig> findByUnit(Object unit) {
		return findByProperty(UNIT, unit);
	}

	public List<PackVpConfig> findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}

}