package com.neuralt.packs.config.dao;

import java.util.List;

import com.neuralt.packs.config.data.PackBlackberryConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public interface PackBlackberryConfigDAO extends BaseDAO<PackBlackberryConfig> {
	
	// property constants
	public static final String NAME = "name";
	public static final String PURCHASEABLE = "purchaseable";
	public static final String PRICE = "price";
	public static final String PRICE_CODE = "priceCode";
	public static final String DISPLAY_LABEL = "displayLabel";
	public static final String SUBSCRIBER_TYPE = "subscriberType";
	public static final String BUNDLE_PACK_SNCODE = "bundlePackSncode";
	public static final String AUTO_RENEW = "autoRenew";
	public static final String AUTO_RENEW_RETRY = "autoRenewRetry";
	public static final String RIM_DATA_SNCODE = "rimDataSncode";
	public static final String RIM_DATA_SPCODE = "rimDataSpcode";
	public static final String FREE_DATA_SNCODE = "freeDataSncode";
	public static final String FREE_DATA_SPCODE = "freeDataSpcode";
	public static final String FREE_DATA_USAGE_LIMIT = "freeDataUsageLimit";
	public static final String ADDITION_AUTORENEW_DATA_PACK_SNCODE = "additionAutorenewDataPackSncode";
	public static final String ADDITION_AUTORENEW_DATA_PACK_USAGE_LIMIT = "additionAutorenewDataPackUsageLimit";
	public static final String VALIDITY = "validity";
	public static final String DAILY_AUTO_RENEW = "dailyAutoRenew";
	public static final String DAY_BUNDLE_PACK_SNCODE = "dayBundlePackSncode";
	public static final String DAY_PACK_FREE_DATA_SNCODE = "dayPackFreeDataSncode";
	public static final String ENTITY_TYPE_KEY = "entityTypeKey";
	public static final String CHARGING_NAME = "chargingName";
	public static final String ACTIVE = "active";

	public List<PackBlackberryConfig> findByName(Object name);
	public List<PackBlackberryConfig> findByPurchaseable(Object purchaseable);
	public List<PackBlackberryConfig> findByPrice(Object price);
	public List<PackBlackberryConfig> findByPriceCode(Object priceCode);
	public List<PackBlackberryConfig> findByDisplayLabel(Object displayLabel);
	public List<PackBlackberryConfig> findBySubscriberType(Object subscriberType);
	public List<PackBlackberryConfig> findByBundlePackSncode(Object bundlePackSncode);
	public List<PackBlackberryConfig> findByAutoRenew(Object autoRenew);
	public List<PackBlackberryConfig> findByAutoRenewRetry(Object autoRenewRetry);
	public List<PackBlackberryConfig> findByRimDataSncode(Object rimDataSncode);
	public List<PackBlackberryConfig> findByRimDataSpcode(Object rimDataSpcode);
	public List<PackBlackberryConfig> findByFreeDataSncode(Object freeDataSncode);
	public List<PackBlackberryConfig> findByFreeDataSpcode(Object freeDataSpcode);
	public List<PackBlackberryConfig> findByFreeDataUsageLimit(Object freeDataUsageLimit);
	public List<PackBlackberryConfig> findByAdditionAutorenewDataPackSncode(Object additionAutorenewDataPackSncode);
	public List<PackBlackberryConfig> findByAdditionAutorenewDataPackUsageLimit(Object additionAutorenewDataPackUsageLimit);
	public List<PackBlackberryConfig> findByValidity(Object validity);
	public List<PackBlackberryConfig> findByDailyAutoRenew(Object dailyAutoRenew);
	public List<PackBlackberryConfig> findByDayBundlePackSncode(Object dayBundlePackSncode);
	public List<PackBlackberryConfig> findByDayPackFreeDataSncode(Object dayPackFreeDataSncode);
	public List<PackBlackberryConfig> findByEntityTypeKey(Object entityTypeKey);
	public List<PackBlackberryConfig> findByChargingName(Object chargingName);
	public List<PackBlackberryConfig> findByActive(Object active);

}