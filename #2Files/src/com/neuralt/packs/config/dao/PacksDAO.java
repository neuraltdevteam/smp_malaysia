package com.neuralt.packs.config.dao;

public class PacksDAO {

	public static final PackAddonConfigDAO ADDON = new PackAddonConfigDaoImpl();
	
	public static final PackBlackberryConfigDAO BLACKBERRY = new PackBlackberryConfigDaoImpl();
	
	public static final PackBoosterConfigDAO BOOSTER = new PackBoosterConfigDaoImpl();
	
	public static final PackBroadbandConfigDAO BROADBAND = new PackBroadbandConfigDaoImpl();
	
	public static final PackIplanConfigDAO IPLAN = new PackIplanConfigDaoImpl();
	
	public static final PackUmiConfigDAO UMI = new PackUmiConfigDaoImpl();
	
	public static final PackVpConfigDAO VP = new PackVpConfigDaoImpl();
}
