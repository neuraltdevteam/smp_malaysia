package com.neuralt.packs.config.dao;

import java.util.List;

import com.neuralt.packs.config.data.PackBroadbandConfig;
import com.neuralt.smp.client.data.dao.BaseDAO;

public interface PackBroadbandConfigDAO extends BaseDAO<PackBroadbandConfig> {
	
	// property constants
	public static final String NAME = "name";
	public static final String VALIDITY = "validity";
	public static final String PRICE = "price";
	public static final String PRICE_CODE = "priceCode";
	public static final String PACK_SIZE = "packSize";
	public static final String BONUS_PACK_SIZE = "bonusPackSize";
	public static final String CHARGING_NAME = "chargingName";
	public static final String BUNDLE_SNCODE = "bundleSncode";
	public static final String BUNDLE_SPCODE = "bundleSpcode";
	public static final String SUBSCRIBER_TYPE = "subscriberType";
	public static final String DATA_PACK_SNCODE = "dataPackSncode";
	public static final String DATA_PACK_SPCODE = "dataPackSpcode";
	public static final String BUNDLE_PACK_SNCODE = "bundlePackSncode";
	public static final String BONUS_BUNDLE_PACK_SNCODE = "bonusBundlePackSncode";
	public static final String BONUS_BUNDLE_PACK_SPCODE = "bonusBundlePackSpcode";
	public static final String FREE_AUTO_RENEW_COUNT = "freeAutoRenewCount";
	public static final String DISPLAY_LABEL = "displayLabel";
	public static final String RETRY_RULES = "retryRules";
	public static final String CHARGING = "charging";
	public static final String IMMEDIATE_STOP = "immediateStop";
	public static final String PROMO_PACK = "promoPack";
	public static final String SEND_SMS = "sendSms";
	public static final String AUTO_RENEW = "autoRenew";
	public static final String BASE_PLAN = "basePlan";
	public static final String FREEBIES_IDX = "freebiesIdx";
	public static final String FREE_USAGE_OFFERID = "freeUsageOfferid";
	public static final String ENTITY_TYPE_KEY = "entityTypeKey";
	public static final String ENTITY_CATEGORY = "entityCategory";
	public static final String FREE_DATA_PACK_SNCODE = "freeDataPackSncode";
	public static final String SUPERVISION_EXTEND_PERIOD = "supervisionExtendPeriod";
	public static final String SERVICE_FEE_EXTEND_PERIOD = "serviceFeeExtendPeriod";
	public static final String ACTIVE = "active";

	public List<PackBroadbandConfig> findByName(Object name);
	public List<PackBroadbandConfig> findByValidity(Object validity);
	public List<PackBroadbandConfig> findByPrice(Object price);
	public List<PackBroadbandConfig> findByPriceCode(Object priceCode);
	public List<PackBroadbandConfig> findByPackSize(Object packSize);
	public List<PackBroadbandConfig> findByBonusPackSize(Object bonusPackSize);
	public List<PackBroadbandConfig> findByChargingName(Object chargingName);
	public List<PackBroadbandConfig> findByBundleSncode(Object bundleSncode);
	public List<PackBroadbandConfig> findByBundleSpcode(Object bundleSpcode);
	public List<PackBroadbandConfig> findBySubscriberType(Object subscriberType);
	public List<PackBroadbandConfig> findByDataPackSncode(Object dataPackSncode);
	public List<PackBroadbandConfig> findByDataPackSpcode(Object dataPackSpcode);
	public List<PackBroadbandConfig> findByBundlePackSncode(Object bundlePackSncode);
	public List<PackBroadbandConfig> findByBonusBundlePackSncode(Object bonusBundlePackSncode);
	public List<PackBroadbandConfig> findByBonusBundlePackSpcode(Object bonusBundlePackSpcode);
	public List<PackBroadbandConfig> findByFreeAutoRenewCount(Object freeAutoRenewCount);
	public List<PackBroadbandConfig> findByDisplayLabel(Object displayLabel);
	public List<PackBroadbandConfig> findByRetryRules(Object retryRules);
	public List<PackBroadbandConfig> findByCharging(Object charging);
	public List<PackBroadbandConfig> findByImmediateStop(Object immediateStop);
	public List<PackBroadbandConfig> findByPromoPack(Object promoPack);
	public List<PackBroadbandConfig> findBySendSms(Object sendSms);
	public List<PackBroadbandConfig> findByAutoRenew(Object autoRenew);
	public List<PackBroadbandConfig> findByBasePlan(Object basePlan);
	public List<PackBroadbandConfig> findByFreebiesIdx(Object freebiesIdx);
	public List<PackBroadbandConfig> findByFreeUsageOfferid(Object freeUsageOfferid);
	public List<PackBroadbandConfig> findByEntityTypeKey(Object entityTypeKey);
	public List<PackBroadbandConfig> findByEntityCategory(Object entityCategory);
	public List<PackBroadbandConfig> findByFreeDataPackSncode(Object freeDataPackSncode);
	public List<PackBroadbandConfig> findBySupervisionExtendPeriod(Object supervisionExtendPeriod);
	public List<PackBroadbandConfig> findByServiceFeeExtendPeriod(Object serviceFeeExtendPeriod);
	public List<PackBroadbandConfig> findByActive(Object active);
	
}