package com.neuralt.packs.config.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.packs.config.data.PackIplanConfig;
import com.neuralt.smp.client.data.dao.BaseHibernateDAO;

public class PackIplanConfigDaoImpl extends BaseHibernateDAO<PackIplanConfig> implements PackIplanConfigDAO{
	
	private static final Logger log = LoggerFactory.getLogger(PackIplanConfigDAO.class);
	
	public PackIplanConfigDaoImpl() {
		super(PackIplanConfig.class);
	}
	
	@Override
	protected Logger getLog() {
		return log;
	}
	
	public List<PackIplanConfig> findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List<PackIplanConfig> findByValidity(Object validity) {
		return findByProperty(VALIDITY, validity);
	}

	public List<PackIplanConfig> findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List<PackIplanConfig> findByPriceCode(Object priceCode) {
		return findByProperty(PRICE_CODE, priceCode);
	}

	public List<PackIplanConfig> findByChargingName(Object chargingName) {
		return findByProperty(CHARGING_NAME, chargingName);
	}

	public List<PackIplanConfig> findByBundleSncode(Object bundleSncode) {
		return findByProperty(BUNDLE_SNCODE, bundleSncode);
	}

	public List<PackIplanConfig> findByBundleSpcode(Object bundleSpcode) {
		return findByProperty(BUNDLE_SPCODE, bundleSpcode);
	}

	public List<PackIplanConfig> findByEntityTypeKeySms(Object entityTypeKeySms) {
		return findByProperty(ENTITY_TYPE_KEY_SMS, entityTypeKeySms);
	}

	public List<PackIplanConfig> findByEntityTypeKeyVoice(
			Object entityTypeKeyVoice) {
		return findByProperty(ENTITY_TYPE_KEY_VOICE, entityTypeKeyVoice);
	}

	public List<PackIplanConfig> findByEntityTypeKeyData(
			Object entityTypeKeyData) {
		return findByProperty(ENTITY_TYPE_KEY_DATA, entityTypeKeyData);
	}

	public List<PackIplanConfig> findByAutoRenew(Object autoRenew) {
		return findByProperty(AUTO_RENEW, autoRenew);
	}

	public List<PackIplanConfig> findByDataPackSncode(Object dataPackSncode) {
		return findByProperty(DATA_PACK_SNCODE, dataPackSncode);
	}

	public List<PackIplanConfig> findByDataPackSpcode(Object dataPackSpcode) {
		return findByProperty(DATA_PACK_SPCODE, dataPackSpcode);
	}

	public List<PackIplanConfig> findByDisplayLabel(Object displayLabel) {
		return findByProperty(DISPLAY_LABEL, displayLabel);
	}

	public List<PackIplanConfig> findByRetryRules(Object retryRules) {
		return findByProperty(RETRY_RULES, retryRules);
	}

	public List<PackIplanConfig> findByRetryCount(Object retryCount) {
		return findByProperty(RETRY_COUNT, retryCount);
	}

	public List<PackIplanConfig> findByCharging(Object charging) {
		return findByProperty(CHARGING, charging);
	}

	public List<PackIplanConfig> findByImmediateStop(Object immediateStop) {
		return findByProperty(IMMEDIATE_STOP, immediateStop);
	}

	public List<PackIplanConfig> findBySmsQuota(Object smsQuota) {
		return findByProperty(SMS_QUOTA, smsQuota);
	}

	public List<PackIplanConfig> findByVoiceQuota(Object voiceQuota) {
		return findByProperty(VOICE_QUOTA, voiceQuota);
	}

	public List<PackIplanConfig> findByDataQuotaGb(Object dataQuotaGb) {
		return findByProperty(DATA_QUOTA_GB, dataQuotaGb);
	}

	public List<PackIplanConfig> findByAccumulatorId(Object accumulatorId) {
		return findByProperty(ACCUMULATOR_ID, accumulatorId);
	}

	public List<PackIplanConfig> findByAccumulatorValueAbs(
			Object accumulatorValueAbs) {
		return findByProperty(ACCUMULATOR_VALUE_ABS, accumulatorValueAbs);
	}

	public List<PackIplanConfig> findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}
}