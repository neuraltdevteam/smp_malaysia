package com.neuralt.packs.config.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.packs.config.data.PackUmiConfig;
import com.neuralt.smp.client.data.dao.BaseHibernateDAO;

public class PackUmiConfigDaoImpl extends BaseHibernateDAO<PackUmiConfig> implements PackUmiConfigDAO{
	
	private static final Logger log = LoggerFactory.getLogger(PackUmiConfigDAO.class);
	
	public PackUmiConfigDaoImpl() {
		super(PackUmiConfig.class);
	}
	
	@Override
	protected Logger getLog() {
		return log;
	}
	
	public List<PackUmiConfig> findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List<PackUmiConfig> findByValidity(Object validity) {
		return findByProperty(VALIDITY, validity);
	}

	public List<PackUmiConfig> findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List<PackUmiConfig> findByPriceCode(Object priceCode) {
		return findByProperty(PRICE_CODE, priceCode);
	}

	public List<PackUmiConfig> findByChargingName(Object chargingName) {
		return findByProperty(CHARGING_NAME, chargingName);
	}

	public List<PackUmiConfig> findByBundleSncode(Object bundleSncode) {
		return findByProperty(BUNDLE_SNCODE, bundleSncode);
	}

	public List<PackUmiConfig> findByBundleSpcode(Object bundleSpcode) {
		return findByProperty(BUNDLE_SPCODE, bundleSpcode);
	}

	public List<PackUmiConfig> findByEntityTypeKeySms(Object entityTypeKeySms) {
		return findByProperty(ENTITY_TYPE_KEY_SMS, entityTypeKeySms);
	}

	public List<PackUmiConfig> findByEntityTypeKeyVoice(
			Object entityTypeKeyVoice) {
		return findByProperty(ENTITY_TYPE_KEY_VOICE, entityTypeKeyVoice);
	}

	public List<PackUmiConfig> findByEntityTypeKeyData(Object entityTypeKeyData) {
		return findByProperty(ENTITY_TYPE_KEY_DATA, entityTypeKeyData);
	}

	public List<PackUmiConfig> findByAutoRenew(Object autoRenew) {
		return findByProperty(AUTO_RENEW, autoRenew);
	}

	public List<PackUmiConfig> findByDataPackSncode(Object dataPackSncode) {
		return findByProperty(DATA_PACK_SNCODE, dataPackSncode);
	}

	public List<PackUmiConfig> findByDataPackSpcode(Object dataPackSpcode) {
		return findByProperty(DATA_PACK_SPCODE, dataPackSpcode);
	}

	public List<PackUmiConfig> findByDisplayLabel(Object displayLabel) {
		return findByProperty(DISPLAY_LABEL, displayLabel);
	}

	public List<PackUmiConfig> findByRetryRules(Object retryRules) {
		return findByProperty(RETRY_RULES, retryRules);
	}

	public List<PackUmiConfig> findByCharging(Object charging) {
		return findByProperty(CHARGING, charging);
	}

	public List<PackUmiConfig> findByImmediateStop(Object immediateStop) {
		return findByProperty(IMMEDIATE_STOP, immediateStop);
	}

	public List<PackUmiConfig> findBySmsQuota(Object smsQuota) {
		return findByProperty(SMS_QUOTA, smsQuota);
	}

	public List<PackUmiConfig> findByVoiceQuota(Object voiceQuota) {
		return findByProperty(VOICE_QUOTA, voiceQuota);
	}

	public List<PackUmiConfig> findBySmsQuotaLimit(Object smsQuotaLimit) {
		return findByProperty(SMS_QUOTA_LIMIT, smsQuotaLimit);
	}

	public List<PackUmiConfig> findByVoiceQuotaLimit(Object voiceQuotaLimit) {
		return findByProperty(VOICE_QUOTA_LIMIT, voiceQuotaLimit);
	}

	public List<PackUmiConfig> findByDataQuotaGb(Object dataQuotaGb) {
		return findByProperty(DATA_QUOTA_GB, dataQuotaGb);
	}

	public List<PackUmiConfig> findByFreePromoDataSncode(
			Object freePromoDataSncode) {
		return findByProperty(FREE_PROMO_DATA_SNCODE, freePromoDataSncode);
	}

	public List<PackUmiConfig> findByFreePromoDataQuotaGb(
			Object freePromoDataQuotaGb) {
		return findByProperty(FREE_PROMO_DATA_QUOTA_GB, freePromoDataQuotaGb);
	}

	public List<PackUmiConfig> findByRenewPromoDataSncode(
			Object renewPromoDataSncode) {
		return findByProperty(RENEW_PROMO_DATA_SNCODE, renewPromoDataSncode);
	}

	public List<PackUmiConfig> findByRenewPromoDataQuotaMb(
			Object renewPromoDataQuotaMb) {
		return findByProperty(RENEW_PROMO_DATA_QUOTA_MB, renewPromoDataQuotaMb);
	}

	public List<PackUmiConfig> findByRenewPromoDataValidity(
			Object renewPromoDataValidity) {
		return findByProperty(RENEW_PROMO_DATA_VALIDITY, renewPromoDataValidity);
	}

	public List<PackUmiConfig> findByFreebiesIdx(Object freebiesIdx) {
		return findByProperty(FREEBIES_IDX, freebiesIdx);
	}

	public List<PackUmiConfig> findByDataBonusSncode(Object dataBonusSncode) {
		return findByProperty(DATA_BONUS_SNCODE, dataBonusSncode);
	}

	public List<PackUmiConfig> findByDataBonusQuotaMb(Object dataBonusQuotaMb) {
		return findByProperty(DATA_BONUS_QUOTA_MB, dataBonusQuotaMb);
	}

	public List<PackUmiConfig> findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}

}