package com.neuralt.packs.config.dao;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.packs.config.data.PackAddonConfig;
import com.neuralt.smp.client.data.dao.BaseHibernateDAO;

public class PackAddonConfigDaoImpl extends BaseHibernateDAO<PackAddonConfig> implements PackAddonConfigDAO{
	
	private static final Logger log = LoggerFactory.getLogger(PackAddonConfigDAO.class);
	
	public PackAddonConfigDaoImpl() {
		super(PackAddonConfig.class);
	}
	
	@Override
	protected Logger getLog() {
		return log;
	}
	
	public List<PackAddonConfig> findByName(Object name) {
		return findByProperty(NAME, name);
	}

	public List<PackAddonConfig> findByValidity(Object validity) {
		return findByProperty(VALIDITY, validity);
	}

	public List<PackAddonConfig> findByPrice(Object price) {
		return findByProperty(PRICE, price);
	}

	public List<PackAddonConfig> findByPriceCode(Object priceCode) {
		return findByProperty(PRICE_CODE, priceCode);
	}

	public List<PackAddonConfig> findByChargingName(Object chargingName) {
		return findByProperty(CHARGING_NAME, chargingName);
	}

	public List<PackAddonConfig> findBySncode(Object sncode) {
		return findByProperty(SNCODE, sncode);
	}

	public List<PackAddonConfig> findBySpcode(Object spcode) {
		return findByProperty(SPCODE, spcode);
	}

	public List<PackAddonConfig> findByAutoRenew(Object autoRenew) {
		return findByProperty(AUTO_RENEW, autoRenew);
	}

	public List<PackAddonConfig> findByDisplayLabel(Object displayLabel) {
		return findByProperty(DISPLAY_LABEL, displayLabel);
	}

	public List<PackAddonConfig> findByPackAmount(Object packAmount) {
		return findByProperty(PACK_AMOUNT, packAmount);
	}

	public List<PackAddonConfig> findByUnit(Object unit) {
		return findByProperty(UNIT, unit);
	}

	public List<PackAddonConfig> findByNumberOfSubscription(
			Object numberOfSubscription) {
		return findByProperty(NUMBER_OF_SUBSCRIPTION, numberOfSubscription);
	}

	public List<PackAddonConfig> findByFamilySharePlanCounter(
			Object familySharePlanCounter) {
		return findByProperty(FAMILY_SHARE_PLAN_COUNTER, familySharePlanCounter);
	}

	public List<PackAddonConfig> findByEntityTypeKey(Object entityTypeKey) {
		return findByProperty(ENTITY_TYPE_KEY, entityTypeKey);
	}

	public List<PackAddonConfig> findByEntityCategory(Object entityCategory) {
		return findByProperty(ENTITY_CATEGORY, entityCategory);
	}

	public List<PackAddonConfig> findByAccumulatorId(Object accumulatorId) {
		return findByProperty(ACCUMULATOR_ID, accumulatorId);
	}

	public List<PackAddonConfig> findByActive(Object active) {
		return findByProperty(ACTIVE, active);
	}

}