-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: smp
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ALARM`
--

DROP TABLE IF EXISTS `ALARM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ALARM` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` int(11) DEFAULT NULL,
  `cause` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `firstOccurred` datetime DEFAULT NULL,
  `lastNotified` datetime DEFAULT NULL,
  `lastOccurred` datetime DEFAULT NULL,
  `warningLevel` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `occurrence` int(11) NOT NULL,
  `occurrenceCount` int(11) NOT NULL,
  `ownerId` varchar(255) DEFAULT NULL,
  `resolved` datetime DEFAULT NULL,
  `resolverId` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `sourceIp` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ALARM`
--

LOCK TABLES `ALARM` WRITE;
/*!40000 ALTER TABLE `ALARM` DISABLE KEYS */;
INSERT INTO `ALARM` VALUES (44,2,'SYS_STARTUP_FAILED','Failed to initialise system context','2015-02-06 16:10:34','1970-01-01 08:00:00','2015-02-08 16:18:33','FATAL','LocalSystemEvent',3,3,NULL,NULL,NULL,'SMP Web Console','0.0.0.0','unhandled');
/*!40000 ALTER TABLE `ALARM` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ALARM_DEFINITION`
--

DROP TABLE IF EXISTS `ALARM_DEFINITION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ALARM_DEFINITION` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(1) NOT NULL,
  `warningLevel` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ALARM_DEFINITION`
--

LOCK TABLES `ALARM_DEFINITION` WRITE;
/*!40000 ALTER TABLE `ALARM_DEFINITION` DISABLE KEYS */;
/*!40000 ALTER TABLE `ALARM_DEFINITION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ALARM_MAP`
--

DROP TABLE IF EXISTS `ALARM_MAP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ALARM_MAP` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `causeId` varchar(255) DEFAULT NULL,
  `causeLevel` varchar(255) DEFAULT NULL,
  `mapType` varchar(255) DEFAULT NULL,
  `mappedAlarm_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ALARM_MAP` (`mappedAlarm_id`),
  KEY `IDX_ALARM_MAP_NAME` (`mappedAlarm_id`)
) ENGINE=MyISAM AUTO_INCREMENT=108 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ALARM_MAP`
--

LOCK TABLES `ALARM_MAP` WRITE;
/*!40000 ALTER TABLE `ALARM_MAP` DISABLE KEYS */;
/*!40000 ALTER TABLE `ALARM_MAP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ALARM_STATUS`
--

DROP TABLE IF EXISTS `ALARM_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ALARM_STATUS` (
  `name` varchar(255) NOT NULL,
  `category` varchar(255) DEFAULT NULL,
  `textColor` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ALARM_STATUS`
--

LOCK TABLES `ALARM_STATUS` WRITE;
/*!40000 ALTER TABLE `ALARM_STATUS` DISABLE KEYS */;
/*!40000 ALTER TABLE `ALARM_STATUS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `AUDIT_TRAIL`
--

DROP TABLE IF EXISTS `AUDIT_TRAIL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUDIT_TRAIL` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `actionTime` datetime DEFAULT NULL,
  `actionType` varchar(255) DEFAULT NULL,
  `actorIp` varchar(255) DEFAULT NULL,
  `actorName` varchar(255) DEFAULT NULL,
  `details` longtext,
  `screenId` varchar(255) DEFAULT NULL,
  `screenType` varchar(255) DEFAULT NULL,
  `summary` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=820 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `AUDIT_TRAIL`
--

LOCK TABLES `AUDIT_TRAIL` WRITE;
/*!40000 ALTER TABLE `AUDIT_TRAIL` DISABLE KEYS */;
INSERT INTO `AUDIT_TRAIL` VALUES (583,'2015-02-06 16:35:41','LOGIN','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','login success'),(584,'2015-02-06 16:38:46','LOGOUT','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/monitor/alarmMgmt.xhtml','FREE','logout'),(585,'2015-02-06 17:32:42','LOGIN','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','login success'),(586,'2015-02-06 17:58:34','LOGIN','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','login success'),(587,'2015-02-09 09:27:51','LOGIN','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','login success'),(588,'2015-02-09 09:28:47','LOGOUT','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','logout'),(589,'2015-02-09 09:38:41','LOGIN','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','login success'),(590,'2015-02-09 09:39:11','LOGOUT','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','logout'),(591,'2015-02-09 09:41:23','LOGIN','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','login success'),(592,'2015-02-09 10:10:26','LOGIN','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/monitor/alarmMgmt.xhtml','FREE','login success'),(593,'2015-02-09 10:16:40','LOGIN','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','login success'),(594,'2015-02-09 10:17:00','LOGOUT','127.0.0.1','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','logout'),(595,'2015-02-09 12:06:02','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(596,'2015-02-09 12:06:13','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(597,'2015-02-09 12:06:23','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(598,'2015-02-09 12:07:10','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(599,'2015-02-09 12:07:20','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(600,'2015-02-09 12:15:12','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(601,'2015-02-09 12:15:23','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(602,'2015-02-09 12:15:31','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(603,'2015-02-09 12:15:41','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(604,'2015-02-09 12:16:11','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(605,'2015-02-09 12:16:22','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(606,'2015-02-09 12:17:23','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(607,'2015-02-09 12:17:34','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(608,'2015-02-09 12:18:03','LOGIN','127.0.0.1',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(609,'2015-02-09 13:04:08','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(610,'2015-02-09 13:04:19','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(611,'2015-02-09 13:04:46','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(612,'2015-02-09 13:04:56','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(613,'2015-02-09 13:05:06','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(614,'2015-02-09 13:05:16','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(615,'2015-02-09 13:05:26','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(616,'2015-02-09 13:05:36','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(617,'2015-02-09 13:05:46','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(618,'2015-02-09 13:05:49','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(619,'2015-02-09 13:05:56','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(620,'2015-02-09 13:06:06','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(621,'2015-02-09 13:06:16','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(622,'2015-02-09 13:06:26','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(623,'2015-02-09 13:06:36','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(624,'2015-02-09 13:06:46','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(625,'2015-02-09 13:06:56','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(626,'2015-02-09 13:07:06','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(627,'2015-02-09 13:07:16','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(628,'2015-02-09 13:07:26','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(629,'2015-02-09 13:07:36','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(630,'2015-02-09 13:07:46','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(631,'2015-02-09 13:07:56','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(632,'2015-02-09 13:08:06','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(633,'2015-02-09 13:08:16','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(634,'2015-02-09 13:08:26','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(635,'2015-02-09 13:08:36','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(636,'2015-02-09 13:08:46','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(637,'2015-02-09 13:08:56','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(638,'2015-02-09 13:09:06','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(639,'2015-02-09 13:09:16','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(640,'2015-02-09 13:09:26','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(641,'2015-02-09 13:09:36','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(642,'2015-02-09 13:09:46','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(643,'2015-02-09 13:09:56','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(644,'2015-02-09 13:10:06','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(645,'2015-02-09 13:10:16','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(646,'2015-02-09 13:10:26','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(647,'2015-02-09 13:10:36','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(648,'2015-02-09 13:10:46','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(649,'2015-02-09 13:10:56','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(650,'2015-02-09 13:11:06','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(651,'2015-02-09 13:11:16','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(652,'2015-02-09 13:11:26','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(653,'2015-02-09 13:11:36','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(654,'2015-02-09 13:11:46','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(655,'2015-02-09 13:11:56','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(656,'2015-02-09 13:12:06','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(657,'2015-02-09 13:12:16','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(658,'2015-02-09 13:12:26','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(659,'2015-02-09 13:12:36','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(660,'2015-02-09 13:12:46','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(661,'2015-02-09 13:12:56','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(662,'2015-02-09 13:13:06','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(663,'2015-02-09 13:13:19','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(664,'2015-02-09 13:13:33','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(665,'2015-02-09 13:13:43','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(666,'2015-02-09 13:13:53','LOGIN','192.168.0.102',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(667,'2015-02-09 13:14:30','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(668,'2015-02-09 13:14:41','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(669,'2015-02-09 13:20:17','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(670,'2015-02-09 13:20:28','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(671,'2015-02-09 15:15:18','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(672,'2015-02-09 15:15:28','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(673,'2015-02-09 15:15:38','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(674,'2015-02-09 15:17:07','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(675,'2015-02-09 15:17:18','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(676,'2015-02-09 15:17:28','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(677,'2015-02-09 15:17:32','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(678,'2015-02-09 15:17:42','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(679,'2015-02-09 15:17:52','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(680,'2015-02-09 15:18:02','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(681,'2015-02-09 15:18:12','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(682,'2015-02-09 15:18:22','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(683,'2015-02-09 15:18:32','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(684,'2015-02-09 15:18:42','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(685,'2015-02-09 15:18:52','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(686,'2015-02-09 15:19:02','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(687,'2015-02-09 15:19:12','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(688,'2015-02-09 15:19:22','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(689,'2015-02-09 15:19:32','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(690,'2015-02-09 15:19:42','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(691,'2015-02-09 15:19:52','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(692,'2015-02-09 15:20:02','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(693,'2015-02-09 15:20:12','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(694,'2015-02-09 15:20:22','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(695,'2015-02-09 15:20:32','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(696,'2015-02-09 15:20:42','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(697,'2015-02-09 15:20:52','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(698,'2015-02-09 15:21:02','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(699,'2015-02-09 15:21:12','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(700,'2015-02-09 15:21:22','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(701,'2015-02-09 15:21:32','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(702,'2015-02-09 15:21:42','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(703,'2015-02-09 15:21:52','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(704,'2015-02-09 15:22:02','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(705,'2015-02-09 15:22:12','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(706,'2015-02-09 15:22:22','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(707,'2015-02-09 15:22:32','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(708,'2015-02-09 15:22:42','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(709,'2015-02-09 15:22:52','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(710,'2015-02-09 15:23:02','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(711,'2015-02-09 15:23:12','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(712,'2015-02-09 15:27:16','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(713,'2015-02-09 15:27:26','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(714,'2015-02-09 15:27:36','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(715,'2015-02-09 15:27:46','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(716,'2015-02-09 15:27:56','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(717,'2015-02-09 15:28:06','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(718,'2015-02-09 15:28:16','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(719,'2015-02-09 15:28:26','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(720,'2015-02-09 15:31:01','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(721,'2015-02-09 15:31:10','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(722,'2015-02-09 15:31:21','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(723,'2015-02-09 15:31:31','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(724,'2015-02-09 15:31:41','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(725,'2015-02-09 15:32:14','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(726,'2015-02-09 15:32:25','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(727,'2015-02-09 15:38:38','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(728,'2015-02-09 15:38:49','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(729,'2015-02-09 15:43:55','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(730,'2015-02-09 15:44:06','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(731,'2015-02-09 15:49:03','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(732,'2015-02-09 15:49:13','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(733,'2015-02-09 15:51:32','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(734,'2015-02-09 15:51:43','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(735,'2015-02-09 15:52:47','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(736,'2015-02-09 15:52:57','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(737,'2015-02-09 15:54:34','LOGIN','192.168.0.105','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','login success'),(738,'2015-02-09 16:05:42','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(739,'2015-02-09 16:05:53','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(740,'2015-02-09 16:10:57','LOGIN','192.168.0.105','SYS_DEFAULT','SYS_DEFAULT','/pages/home.xhtml','FREE','login success'),(741,'2015-02-09 16:12:10','INSERT','192.168.0.105','SYS_DEFAULT','SystemConfig [name=LEAD, description=LEAD RA, enabled=true, hosts=[]]','/pages/config/systemconfig.xhtml','CONFIG','add-system'),(742,'2015-02-09 16:13:28','INSERT','192.168.0.105','SYS_DEFAULT','HostConfig [id=0, name=AWS, description=AWS for LEAD, system=LEAD]','/pages/config/systemconfig.xhtml','CONFIG','add-host'),(743,'2015-02-09 16:14:06','INSERT','192.168.0.105','SYS_DEFAULT','Site : [siteName=LEAD, location=USA, contactPerson=, phone=, email=]','/pages/config/site.xhtml','CONFIG','add-site'),(744,'2015-02-09 16:20:27','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(745,'2015-02-09 16:20:38','LOGIN','192.168.0.105',NULL,NULL,'/pages/home.xhtml','FREE','login failed'),(746,'2015-02-09 16:22:15','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(747,'2015-02-09 16:27:26','INSERT','192.168.0.105','SYS','IF-MIB.txt','/pages/config/mibMgmt.xhtml','CONFIG','add-mib'),(748,'2015-02-09 16:50:27','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(749,'2015-02-09 17:06:32','INSERT','192.168.0.105','SYS','HostConfig [id=0, name=AWS_SERVER_1, description=LEAD on AWS 1, system=LEAD]','/pages/config/systemconfig.xhtml','CONFIG','add-host'),(750,'2015-02-09 17:14:11','LOGIN','192.168.0.102','SYS','SYS','/pages/home.xhtml','FREE','login success'),(751,'2015-02-09 17:20:39','INSERT','192.168.0.102','SYS','ProcessConfig [name=web, description=tomcat instance, enabled=false, host=AWS_SERVER_1]','/pages/config/systemconfig.xhtml','CONFIG','add-process'),(752,'2015-02-09 17:24:14','LOGOUT','192.168.0.102','SYS','SYS','/pages/config/noticonfig.xhtml','FREE','logout'),(753,'2015-02-09 17:35:11','LOGIN','192.168.0.105','SYS','SYS','/pages/config/systemconfig.xhtml','FREE','login success'),(754,'2015-02-09 17:46:30','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(755,'2015-02-09 18:07:06','LOGIN','192.168.0.102','SYS','SYS','/pages/home.xhtml','FREE','login success'),(756,'2015-02-09 18:08:38','LOGOUT','192.168.0.102','SYS','SYS','/pages/home.xhtml','FREE','logout'),(757,'2015-02-09 18:20:38','LOGIN','192.168.0.102','SYS','SYS','/pages/commands/snmpquery.xhtml','FREE','login success'),(758,'2015-02-09 18:21:38','LOGOUT','192.168.0.102','SYS','SYS','/pages/home.xhtml','FREE','logout'),(759,'2015-02-09 19:02:23','INSERT','192.168.0.105','SYS','def=RrdDefConfig :[name=Test1, path=null, step=10, locked=false, enabled=false, ds count=0, archive count=0, procedure count=0]','/pages/config/datadefinition.xhtml','CONFIG','add-data definition'),(760,'2015-02-09 19:12:01','INSERT','192.168.0.105','SYS','ScriptConfig :[name=Script1, path=/home/erl/smpserver/script/AWS_SERVER_1_Script1_2015-02-09_07-12-01_PM.sh, host=AWS_SERVER_1]','/pages/config/scriptconfig.xhtml','CONFIG','add-script'),(761,'2015-02-10 09:49:05','LOGIN','127.0.0.1','SYS','SYS','/pages/home.xhtml','FREE','login success'),(762,'2015-02-10 10:27:56','LOGIN','127.0.0.1','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','login success'),(763,'2015-02-10 11:08:41','LOGIN','127.0.0.1','SYS','SYS','/pages/config/datadefinition.xhtml','FREE','login success'),(764,'2015-02-10 18:49:50','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(765,'2015-02-11 16:20:51','UPDATE','192.168.0.105','SYS','ProcessConfig [name=Web, description=tomcat instance, enabled=false, host=AWS_SERVER_1]','/pages/config/systemconfig.xhtml','CONFIG','edit-process'),(766,'2015-02-11 18:23:11','LOGIN','192.168.0.102','SYS','SYS','/pages/config/datadefinition.xhtml','FREE','login success'),(767,'2015-02-11 19:22:03','LOGIN','192.168.0.105','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','login success'),(768,'2015-02-11 19:41:50','INSERT','192.168.0.105','SYS','Site : [siteName=HLR, location=Hong Kong, contactPerson=, phone=, email=]','/pages/config/site.xhtml','CONFIG','add-site'),(769,'2015-02-12 11:26:52','LOGIN','192.168.0.102','SYS','SYS','/pages/config/datadefinition.xhtml','FREE','login success'),(770,'2015-02-12 11:28:35','LOGOUT','192.168.0.102','SYS','SYS','/pages/home.xhtml','FREE','logout'),(771,'2015-02-12 11:48:34','LOGOUT','192.168.0.105','SYS','SYS','/pages/user/user.xhtml','FREE','logout'),(772,'2015-02-12 11:50:41','LOGIN','192.168.0.105','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','login success'),(773,'2015-02-12 11:56:11','LOGIN','192.168.0.105','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','login success'),(774,'2015-02-12 12:02:33','LOGIN','192.168.0.102','SYS','SYS','/pages/config/datadefinition.xhtml','FREE','login success'),(775,'2015-02-12 12:04:25','LOGOUT','192.168.0.102','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','logout'),(776,'2015-02-12 12:19:52','INSERT','192.168.0.105','SYS','SystemConfig [name=HLR, description=Hong Kong HLR, enabled=false, hosts=[]]','/pages/config/systemconfig.xhtml','CONFIG','add-system'),(777,'2015-02-12 12:21:20','INSERT','192.168.0.105','SYS','HostConfig [id=0, name=AWS_SERVER_3, description=Server 3 on AWS, system=HLR]','/pages/config/systemconfig.xhtml','CONFIG','add-host'),(778,'2015-02-12 15:21:16','UPDATE','192.168.0.105','SYS','HLR','/pages/config/systemconfig.xhtml','CONFIG','edit-system'),(779,'2015-02-12 15:21:36','UPDATE','192.168.0.105','SYS','HostConfig [id=5, name=AWS_SERVER_3, description=Server 3 on AWS, system=HLR]','/pages/config/systemconfig.xhtml','CONFIG','edit-host'),(780,'2015-02-12 15:24:00','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(781,'2015-02-12 15:31:21','INSERT','192.168.0.105','SYS','HostConfig [id=5, name=AWS_SERVER_3, description=Server 3 on AWS, system=HLR]','/pages/config/systemconfig.xhtml','CONFIG','add-SNMP config'),(782,'2015-02-12 15:32:02','INSERT','192.168.0.105','SYS','SystemConfig [name=LDAP, description=LDAP Server, enabled=false, hosts=[]]','/pages/config/systemconfig.xhtml','CONFIG','add-system'),(783,'2015-02-12 15:32:15','DELETE','192.168.0.105','SYS','LDAP','/pages/config/systemconfig.xhtml','CONFIG','delete-system'),(784,'2015-02-12 15:43:55','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(785,'2015-02-12 16:05:34','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(786,'2015-02-12 16:15:51','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(787,'2015-02-12 16:20:30','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(788,'2015-02-12 16:35:50','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(789,'2015-02-12 17:28:14','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(790,'2015-02-12 17:29:43','LOGOUT','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','logout'),(791,'2015-02-12 19:25:00','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(792,'2015-02-13 09:11:04','LOGIN','192.168.0.101','SYS','SYS','/pages/config/datadefinition.xhtml','FREE','login success'),(793,'2015-02-13 09:17:06','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(794,'2015-02-13 09:28:29','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(795,'2015-02-13 09:34:40','LOGIN','192.168.0.101','SYS','SYS','/pages/home.xhtml','FREE','login success'),(796,'2015-02-13 09:35:31','LOGOUT','192.168.0.101','SYS','SYS','/pages/home.xhtml','FREE','logout'),(797,'2015-02-13 09:35:37','LOGIN','192.168.0.101','SYS','SYS','/pages/home.xhtml','FREE','login success'),(798,'2015-02-13 09:56:55','LOGIN','192.168.0.105','SYS','SYS','/pages/config/systemconfig.xhtml','FREE','login success'),(799,'2015-02-13 09:57:23','LOGIN','192.168.0.101','SYS','SYS','/pages/home.xhtml','FREE','login success'),(800,'2015-02-13 10:05:33','LOGIN','192.168.0.105','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','login success'),(801,'2015-02-13 10:06:22','LOGIN','192.168.0.105','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','login success'),(802,'2015-02-13 10:08:52','LOGIN','192.168.0.105','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','login success'),(803,'2015-02-13 10:09:24','LOGIN','192.168.0.101','SYS','SYS','/pages/home.xhtml','FREE','login success'),(804,'2015-02-13 10:11:26','LOGOUT','192.168.0.105','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','logout'),(805,'2015-02-13 10:19:22','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(806,'2015-02-13 10:19:30','LOGIN','192.168.0.101','SYS','SYS','/pages/config/datadefinition.xhtml','FREE','login success'),(807,'2015-02-13 10:21:57','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(808,'2015-02-13 10:22:19','LOGIN','192.168.0.101','SYS','SYS','/pages/home.xhtml','FREE','login success'),(809,'2015-02-13 10:22:53','LOGOUT','192.168.0.101','SYS','SYS','/pages/home.xhtml','FREE','logout'),(810,'2015-02-13 10:23:01','LOGIN','192.168.0.101','SYS','SYS','/pages/home.xhtml','FREE','login success'),(811,'2015-02-13 10:49:21','LOGOUT','192.168.0.101','SYS','SYS','/pages/config/systemconfig.xhtml','FREE','logout'),(812,'2015-02-13 11:14:55','LOGOUT','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','logout'),(813,'2015-02-13 11:15:36','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(814,'2015-02-13 11:15:41','LOGOUT','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','logout'),(815,'2015-02-13 11:34:15','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(816,'2015-02-13 12:03:10','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(817,'2015-02-13 12:58:32','LOGIN','192.168.0.105','SYS','SYS','/pages/home.xhtml','FREE','login success'),(818,'2015-02-13 13:12:22','LOGIN','192.168.0.105','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','login success'),(819,'2015-02-13 14:42:18','LOGIN','192.168.0.105','SYS','SYS','/pages/monitor/alarmMgmt.xhtml','FREE','login success');
/*!40000 ALTER TABLE `AUDIT_TRAIL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CHART_TPL`
--

DROP TABLE IF EXISTS `CHART_TPL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CHART_TPL` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `priority` int(11) NOT NULL,
  `timeRange` int(11) NOT NULL,
  `timeRangeType` varchar(255) DEFAULT NULL,
  `verticalLabel` varchar(255) DEFAULT NULL,
  `user_USERID` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKE4A0E3DBFBF78C53` (`user_USERID`),
  KEY `FK46C454FFBF78C53` (`user_USERID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CHART_TPL`
--

LOCK TABLES `CHART_TPL` WRITE;
/*!40000 ALTER TABLE `CHART_TPL` DISABLE KEYS */;
/*!40000 ALTER TABLE `CHART_TPL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CHART_TPL_RRD_ARCH_CFG`
--

DROP TABLE IF EXISTS `CHART_TPL_RRD_ARCH_CFG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CHART_TPL_RRD_ARCH_CFG` (
  `CHART_TPL_id` bigint(20) NOT NULL DEFAULT '0',
  `archiveConfigs_id` bigint(20) NOT NULL,
  PRIMARY KEY (`CHART_TPL_id`,`archiveConfigs_id`),
  KEY `FKD56CBBFEC3ACDCA4` (`archiveConfigs_id`),
  KEY `FKD56CBBFEE5F8A05E` (`CHART_TPL_id`),
  KEY `FK61EED5A6C3ACDCA4` (`archiveConfigs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CHART_TPL_RRD_ARCH_CFG`
--

LOCK TABLES `CHART_TPL_RRD_ARCH_CFG` WRITE;
/*!40000 ALTER TABLE `CHART_TPL_RRD_ARCH_CFG` DISABLE KEYS */;
/*!40000 ALTER TABLE `CHART_TPL_RRD_ARCH_CFG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CHART_TPL_RRD_DS_CONFIG`
--

DROP TABLE IF EXISTS `CHART_TPL_RRD_DS_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CHART_TPL_RRD_DS_CONFIG` (
  `CHART_TPL_id` bigint(20) NOT NULL DEFAULT '0',
  `dsConfigs_id` bigint(20) NOT NULL,
  PRIMARY KEY (`CHART_TPL_id`,`dsConfigs_id`),
  KEY `FKC52A5B13E5F8A05E` (`CHART_TPL_id`),
  KEY `FKC52A5B1324E126F4` (`dsConfigs_id`),
  KEY `FKC064218724E126F4` (`dsConfigs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CHART_TPL_RRD_DS_CONFIG`
--

LOCK TABLES `CHART_TPL_RRD_DS_CONFIG` WRITE;
/*!40000 ALTER TABLE `CHART_TPL_RRD_DS_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `CHART_TPL_RRD_DS_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CMD_CFG`
--

DROP TABLE IF EXISTS `CMD_CFG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CMD_CFG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `script` varchar(255) DEFAULT NULL,
  `HOST_CONFIG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_CMD_HOST` (`HOST_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CMD_CFG`
--

LOCK TABLES `CMD_CFG` WRITE;
/*!40000 ALTER TABLE `CMD_CFG` DISABLE KEYS */;
INSERT INTO `CMD_CFG` VALUES (4,'SET_CLOCK','scripts/set_clock.sh',4),(5,'SET_CLOCK','scripts/set_clock.sh',5);
/*!40000 ALTER TABLE `CMD_CFG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `CONTACT_PERSON`
--

DROP TABLE IF EXISTS `CONTACT_PERSON`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CONTACT_PERSON` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `CONTACT_PERSON`
--

LOCK TABLES `CONTACT_PERSON` WRITE;
/*!40000 ALTER TABLE `CONTACT_PERSON` DISABLE KEYS */;
/*!40000 ALTER TABLE `CONTACT_PERSON` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ConsoleUser`
--

DROP TABLE IF EXISTS `ConsoleUser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConsoleUser` (
  `USERID` varchar(30) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `lastLogin` datetime DEFAULT NULL,
  `lastUpdatedBy` varchar(30) DEFAULT NULL,
  `lastUpdatedOn` datetime NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `loginFailure` int(11) DEFAULT NULL,
  `name` varchar(60) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `status` varchar(10) NOT NULL,
  `GROUPID` varchar(30) NOT NULL,
  PRIMARY KEY (`USERID`),
  KEY `FK_CONSOLEUSER_USERGROUP` (`GROUPID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ConsoleUser`
--

LOCK TABLES `ConsoleUser` WRITE;
/*!40000 ALTER TABLE `ConsoleUser` DISABLE KEYS */;
INSERT INTO `ConsoleUser` VALUES ('SYS','2015-02-06 16:10:17',NULL,'2015-02-13 14:42:18',NULL,'2015-02-13 14:42:18',NULL,NULL,'Default Superuser','LHoy9QipwP4=blMC0B6R2tI=','ACTIVE','superGroup');
/*!40000 ALTER TABLE `ConsoleUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `DATA_COLLECTION_PROCEDURE`
--

DROP TABLE IF EXISTS `DATA_COLLECTION_PROCEDURE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DATA_COLLECTION_PROCEDURE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `RRD_DS_CONFIG_ID` bigint(20) DEFAULT NULL,
  `RRD_DEF_CONFIG_ID` bigint(20) DEFAULT NULL,
  `SCRIPT_CONFIG_ID` bigint(20) DEFAULT NULL,
  `SNMP_TARGET_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKE5567BC754539E0A` (`SCRIPT_CONFIG_ID`),
  KEY `FKE5567BC7951609AB` (`SNMP_TARGET_ID`),
  KEY `FKE5567BC79E7DB03F` (`RRD_DS_CONFIG_ID`),
  KEY `FK_CFG_PROCEDURE_RRD_DEF` (`RRD_DEF_CONFIG_ID`),
  KEY `FKE5567BC77F1674D0` (`SNMP_TARGET_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `DATA_COLLECTION_PROCEDURE`
--

LOCK TABLES `DATA_COLLECTION_PROCEDURE` WRITE;
/*!40000 ALTER TABLE `DATA_COLLECTION_PROCEDURE` DISABLE KEYS */;
/*!40000 ALTER TABLE `DATA_COLLECTION_PROCEDURE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `EVENTLOG`
--

DROP TABLE IF EXISTS `EVENTLOG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EVENTLOG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `details` varchar(255) DEFAULT NULL,
  `eventName` varchar(255) DEFAULT NULL,
  `eventTime` datetime DEFAULT NULL,
  `hostName` varchar(255) DEFAULT NULL,
  `warningLevel` varchar(255) DEFAULT NULL,
  `procName` varchar(255) DEFAULT NULL,
  `sourceType` varchar(255) DEFAULT NULL,
  `sysName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_TIME` (`eventTime`),
  KEY `IDX_EVENTLOG_TIME` (`eventTime`)
) ENGINE=MyISAM AUTO_INCREMENT=56458 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `EVENTLOG`
--

LOCK TABLES `EVENTLOG` WRITE;
/*!40000 ALTER TABLE `EVENTLOG` DISABLE KEYS */;
INSERT INTO `EVENTLOG` VALUES (56360,'Failed to initialise system context','SYS_STARTUP_FAILED','2015-02-06 16:10:34',NULL,'FATAL',NULL,'SMP',NULL),(56361,'Failed to initialise system context','SYS_STARTUP_FAILED','2015-02-06 16:12:04',NULL,'FATAL',NULL,'SMP',NULL),(56362,NULL,'SYS_STARTUP_SUCCESS','2015-02-06 16:12:47',NULL,'INFO',NULL,'SMP',NULL),(56363,NULL,'SYS_STARTUP_SUCCESS','2015-02-06 16:34:54',NULL,'INFO',NULL,'SMP',NULL),(56364,NULL,'SYS_SHUTDOWN','2015-02-06 17:25:04',NULL,'INFO',NULL,'SMP',NULL),(56365,NULL,'SYS_STARTUP_SUCCESS','2015-02-06 17:25:15',NULL,'INFO',NULL,'SMP',NULL),(56366,NULL,'SYS_STARTUP_SUCCESS','2015-02-06 17:28:26',NULL,'INFO',NULL,'SMP',NULL),(56367,NULL,'SYS_SHUTDOWN','2015-02-06 17:42:35',NULL,'INFO',NULL,'SMP',NULL),(56368,NULL,'SYS_STARTUP_SUCCESS','2015-02-06 17:42:40',NULL,'INFO',NULL,'SMP',NULL),(56369,NULL,'SYS_STARTUP_SUCCESS','2015-02-06 17:57:09',NULL,'INFO',NULL,'SMP',NULL),(56370,'Failed to initialise system context','SYS_STARTUP_FAILED','2015-02-08 16:18:32',NULL,'FATAL',NULL,'SMP',NULL),(56371,NULL,'SYS_STARTUP_SUCCESS','2015-02-08 16:19:30',NULL,'INFO',NULL,'SMP',NULL),(56372,NULL,'SYS_STARTUP_SUCCESS','2015-02-08 16:20:30',NULL,'INFO',NULL,'SMP',NULL),(56373,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 09:24:06',NULL,'INFO',NULL,'SMP',NULL),(56374,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 09:27:09',NULL,'INFO',NULL,'SMP',NULL),(56375,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 09:37:43',NULL,'INFO',NULL,'SMP',NULL),(56376,NULL,'SYS_SHUTDOWN','2015-02-09 10:08:38',NULL,'INFO',NULL,'SMP',NULL),(56377,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 10:08:44',NULL,'INFO',NULL,'SMP',NULL),(56378,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 10:15:06',NULL,'INFO',NULL,'SMP',NULL),(56379,NULL,'SYS_SHUTDOWN','2015-02-09 10:44:10',NULL,'INFO',NULL,'SMP',NULL),(56380,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 10:45:09',NULL,'INFO',NULL,'SMP',NULL),(56381,NULL,'SYS_SHUTDOWN','2015-02-09 11:17:41',NULL,'INFO',NULL,'SMP',NULL),(56382,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 11:17:48',NULL,'INFO',NULL,'SMP',NULL),(56383,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 12:03:41',NULL,'INFO',NULL,'SMP',NULL),(56384,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 12:15:55',NULL,'INFO',NULL,'SMP',NULL),(56385,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 12:17:05',NULL,'INFO',NULL,'SMP',NULL),(56386,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 12:45:22',NULL,'INFO',NULL,'SMP',NULL),(56387,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 12:59:09',NULL,'INFO',NULL,'SMP',NULL),(56388,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 13:03:42',NULL,'INFO',NULL,'SMP',NULL),(56389,NULL,'SYS_SHUTDOWN','2015-02-09 13:14:01',NULL,'INFO',NULL,'SMP',NULL),(56390,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 13:14:11',NULL,'INFO',NULL,'SMP',NULL),(56391,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 13:19:51',NULL,'INFO',NULL,'SMP',NULL),(56392,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 15:16:29',NULL,'INFO',NULL,'SMP',NULL),(56393,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 15:26:45',NULL,'INFO',NULL,'SMP',NULL),(56394,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 15:30:42',NULL,'INFO',NULL,'SMP',NULL),(56395,NULL,'SYS_SHUTDOWN','2015-02-09 15:35:52',NULL,'INFO',NULL,'SMP',NULL),(56396,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 15:38:21',NULL,'INFO',NULL,'SMP',NULL),(56397,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 15:43:38',NULL,'INFO',NULL,'SMP',NULL),(56398,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 15:48:44',NULL,'INFO',NULL,'SMP',NULL),(56399,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 15:51:10',NULL,'INFO',NULL,'SMP',NULL),(56400,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 15:52:24',NULL,'INFO',NULL,'SMP',NULL),(56401,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 15:53:58',NULL,'INFO',NULL,'SMP',NULL),(56402,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 16:04:51',NULL,'INFO',NULL,'SMP',NULL),(56403,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 16:10:38',NULL,'INFO',NULL,'SMP',NULL),(56404,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 16:20:07',NULL,'INFO',NULL,'SMP',NULL),(56405,NULL,'SYS_SHUTDOWN','2015-02-09 16:50:00',NULL,'INFO',NULL,'SMP',NULL),(56406,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 16:50:07',NULL,'INFO',NULL,'SMP',NULL),(56407,NULL,'SYS_SHUTDOWN','2015-02-09 17:27:03',NULL,'INFO',NULL,'SMP',NULL),(56408,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 17:27:09',NULL,'INFO',NULL,'SMP',NULL),(56409,NULL,'SYS_SHUTDOWN','2015-02-09 17:44:32',NULL,'INFO',NULL,'SMP',NULL),(56410,NULL,'SYS_STARTUP_SUCCESS','2015-02-09 17:44:42',NULL,'INFO',NULL,'SMP',NULL),(56411,NULL,'SYS_SHUTDOWN','2015-02-10 10:26:34',NULL,'INFO',NULL,'SMP',NULL),(56412,NULL,'SYS_STARTUP_SUCCESS','2015-02-10 10:26:43',NULL,'INFO',NULL,'SMP',NULL),(56413,NULL,'SYS_SHUTDOWN','2015-02-10 10:56:38',NULL,'INFO',NULL,'SMP',NULL),(56414,NULL,'SYS_STARTUP_SUCCESS','2015-02-10 10:56:49',NULL,'INFO',NULL,'SMP',NULL),(56415,NULL,'SYS_STARTUP_SUCCESS','2015-02-10 18:49:27',NULL,'INFO',NULL,'SMP',NULL),(56416,NULL,'SYS_SHUTDOWN','2015-02-11 19:15:52',NULL,'INFO',NULL,'SMP',NULL),(56417,NULL,'SYS_STARTUP_SUCCESS','2015-02-11 19:16:00',NULL,'INFO',NULL,'SMP',NULL),(56418,NULL,'SYS_SHUTDOWN','2015-02-12 11:55:46',NULL,'INFO',NULL,'SMP',NULL),(56419,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 11:55:52',NULL,'INFO',NULL,'SMP',NULL),(56420,NULL,'SYS_SHUTDOWN','2015-02-12 15:23:21',NULL,'INFO',NULL,'SMP',NULL),(56421,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 15:23:27',NULL,'INFO',NULL,'SMP',NULL),(56422,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 15:43:33',NULL,'INFO',NULL,'SMP',NULL),(56423,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 15:46:18',NULL,'INFO',NULL,'SMP',NULL),(56424,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 16:01:05',NULL,'INFO',NULL,'SMP',NULL),(56425,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 16:05:08',NULL,'INFO',NULL,'SMP',NULL),(56426,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 16:15:34',NULL,'INFO',NULL,'SMP',NULL),(56427,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 16:19:51',NULL,'INFO',NULL,'SMP',NULL),(56428,NULL,'SYS_SHUTDOWN','2015-02-12 16:34:27',NULL,'INFO',NULL,'SMP',NULL),(56429,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 16:34:34',NULL,'INFO',NULL,'SMP',NULL),(56430,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 19:12:22',NULL,'INFO',NULL,'SMP',NULL),(56431,NULL,'SYS_STARTUP_SUCCESS','2015-02-12 19:13:24',NULL,'INFO',NULL,'SMP',NULL),(56432,NULL,'SYS_SHUTDOWN','2015-02-13 09:21:02',NULL,'INFO',NULL,'SMP',NULL),(56433,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 09:21:09',NULL,'INFO',NULL,'SMP',NULL),(56434,NULL,'SYS_SHUTDOWN','2015-02-13 09:56:03',NULL,'INFO',NULL,'SMP',NULL),(56435,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 09:56:09',NULL,'INFO',NULL,'SMP',NULL),(56436,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 09:56:38',NULL,'INFO',NULL,'SMP',NULL),(56437,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 10:05:21',NULL,'INFO',NULL,'SMP',NULL),(56438,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 10:06:13',NULL,'INFO',NULL,'SMP',NULL),(56439,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 10:07:53',NULL,'INFO',NULL,'SMP',NULL),(56440,NULL,'SYS_SHUTDOWN','2015-02-13 10:16:07',NULL,'INFO',NULL,'SMP',NULL),(56441,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 10:19:01',NULL,'INFO',NULL,'SMP',NULL),(56442,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 10:21:39',NULL,'INFO',NULL,'SMP',NULL),(56443,NULL,'SYS_SHUTDOWN','2015-02-13 11:20:13',NULL,'INFO',NULL,'SMP',NULL),(56444,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 11:20:20',NULL,'INFO',NULL,'SMP',NULL),(56445,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 11:34:02',NULL,'INFO',NULL,'SMP',NULL),(56446,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 11:59:38',NULL,'INFO',NULL,'SMP',NULL),(56447,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 12:02:51',NULL,'INFO',NULL,'SMP',NULL),(56448,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 12:04:03',NULL,'INFO',NULL,'SMP',NULL),(56449,'Failed to initialise system context','SYS_STARTUP_FAILED','2015-02-13 12:55:04',NULL,'FATAL',NULL,'SMP',NULL),(56450,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 12:56:35',NULL,'INFO',NULL,'SMP',NULL),(56451,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 12:57:47',NULL,'INFO',NULL,'SMP',NULL),(56452,NULL,'SYS_SHUTDOWN','2015-02-13 13:11:20',NULL,'INFO',NULL,'SMP',NULL),(56453,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 13:11:31',NULL,'INFO',NULL,'SMP',NULL),(56454,NULL,'SYS_SHUTDOWN','2015-02-13 13:16:51',NULL,'INFO',NULL,'SMP',NULL),(56455,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 13:16:57',NULL,'INFO',NULL,'SMP',NULL),(56456,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 14:41:55',NULL,'INFO',NULL,'SMP',NULL),(56457,NULL,'SYS_STARTUP_SUCCESS','2015-02-13 15:01:52',NULL,'INFO',NULL,'SMP',NULL);
/*!40000 ALTER TABLE `EVENTLOG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GROUP_SNMP_CONFIG`
--

DROP TABLE IF EXISTS `GROUP_SNMP_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GROUP_SNMP_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auth` varchar(255) DEFAULT NULL,
  `authPassword` varchar(255) DEFAULT NULL,
  `communityGet` varchar(255) DEFAULT NULL,
  `communitySet` varchar(255) DEFAULT NULL,
  `communityTrap` varchar(255) DEFAULT NULL,
  `engineId` varchar(255) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `priv` varchar(255) DEFAULT NULL,
  `privPassword` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `original` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GROUP_SNMP_CONFIG`
--

LOCK TABLES `GROUP_SNMP_CONFIG` WRITE;
/*!40000 ALTER TABLE `GROUP_SNMP_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `GROUP_SNMP_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `GroupRole`
--

DROP TABLE IF EXISTS `GroupRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GroupRole` (
  `GROUPID` varchar(30) NOT NULL,
  `ROLEID` varchar(30) NOT NULL,
  PRIMARY KEY (`GROUPID`,`ROLEID`),
  KEY `FK_GROUPROLE_USERROLE` (`ROLEID`),
  KEY `FK_GROUPROLE_USERGROUP` (`GROUPID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `GroupRole`
--

LOCK TABLES `GroupRole` WRITE;
/*!40000 ALTER TABLE `GroupRole` DISABLE KEYS */;
INSERT INTO `GroupRole` VALUES ('superGroup','superRole');
/*!40000 ALTER TABLE `GroupRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_CFG`
--

DROP TABLE IF EXISTS `HOST_CFG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_CFG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `agentDeployed` tinyint(1) NOT NULL,
  `agentJmxPort` int(11) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `ipAddr` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `role` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `SITE_ID` bigint(20) DEFAULT NULL,
  `SYSTEM_CONFIG_ID` bigint(20) DEFAULT NULL,
  `rrdFilePath` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_HOST_SYS` (`SYSTEM_CONFIG_ID`),
  KEY `FK_CFG_HOST_SITE` (`SITE_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_CFG`
--

LOCK TABLES `HOST_CFG` WRITE;
/*!40000 ALTER TABLE `HOST_CFG` DISABLE KEYS */;
INSERT INTO `HOST_CFG` VALUES (4,1,9999,'LEAD on AWS 1',1,'192.168.0.105','AWS_SERVER_1','ACTIVE','SCRIPT',1,3,NULL),(5,0,9999,'Server 3 on AWS',1,'192.168.0.106','AWS_SERVER_3','ACTIVE','SNMP',2,4,NULL);
/*!40000 ALTER TABLE `HOST_CFG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_CFG_CMD_CFG`
--

DROP TABLE IF EXISTS `HOST_CFG_CMD_CFG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_CFG_CMD_CFG` (
  `HOST_CFG_id` bigint(20) NOT NULL DEFAULT '0',
  `commandConfigs_id` bigint(20) NOT NULL,
  PRIMARY KEY (`HOST_CFG_id`,`commandConfigs_id`),
  UNIQUE KEY `commandConfigs_id` (`commandConfigs_id`),
  KEY `FKA6547EC78D3574DF` (`HOST_CFG_id`),
  KEY `FKA6547EC72AD8DAD` (`commandConfigs_id`),
  KEY `FK539A528D2AD8DAD` (`commandConfigs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_CFG_CMD_CFG`
--

LOCK TABLES `HOST_CFG_CMD_CFG` WRITE;
/*!40000 ALTER TABLE `HOST_CFG_CMD_CFG` DISABLE KEYS */;
INSERT INTO `HOST_CFG_CMD_CFG` VALUES (4,4),(5,5);
/*!40000 ALTER TABLE `HOST_CFG_CMD_CFG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_CFG_HOST_MON_CFG`
--

DROP TABLE IF EXISTS `HOST_CFG_HOST_MON_CFG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_CFG_HOST_MON_CFG` (
  `HOST_CFG_id` bigint(20) NOT NULL DEFAULT '0',
  `monitorConfig_id` bigint(20) NOT NULL,
  PRIMARY KEY (`HOST_CFG_id`,`monitorConfig_id`),
  UNIQUE KEY `monitorConfig_id` (`monitorConfig_id`),
  KEY `FK912581D83DEF73CC` (`monitorConfig_id`),
  KEY `FK912581D88D3574DF` (`HOST_CFG_id`),
  KEY `FKDD4D62AC3DEF73CC` (`monitorConfig_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_CFG_HOST_MON_CFG`
--

LOCK TABLES `HOST_CFG_HOST_MON_CFG` WRITE;
/*!40000 ALTER TABLE `HOST_CFG_HOST_MON_CFG` DISABLE KEYS */;
INSERT INTO `HOST_CFG_HOST_MON_CFG` VALUES (4,19),(4,20),(4,21),(4,22),(4,23),(4,24),(5,25),(5,26),(5,27),(5,28),(5,29),(5,30);
/*!40000 ALTER TABLE `HOST_CFG_HOST_MON_CFG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_MON_CFG`
--

DROP TABLE IF EXISTS `HOST_MON_CFG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_MON_CFG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `enabled` tinyint(1) NOT NULL,
  `monitorInterval` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `script` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_MON_CFG`
--

LOCK TABLES `HOST_MON_CFG` WRITE;
/*!40000 ALTER TABLE `HOST_MON_CFG` DISABLE KEYS */;
INSERT INTO `HOST_MON_CFG` VALUES (19,1,10,'CPU_MONITOR','scripts/check_cpu.sh'),(20,1,10,'UPTIME_MONITOR','scripts/check_uptime.sh'),(21,1,10,'DISK_MONITOR','scripts/check_disk.sh'),(22,1,10,'INET_MONITOR','scripts/check_net.sh'),(23,1,10,'MEMORY_MONITOR','scripts/check_mem.sh'),(24,1,10,'PS_MONITOR','scripts/check_ps.sh'),(25,1,10,'MEMORY_MONITOR','scripts/check_mem.sh'),(26,1,10,'DISK_MONITOR','scripts/check_disk.sh'),(27,1,10,'INET_MONITOR','scripts/check_net.sh'),(28,1,10,'CPU_MONITOR','scripts/check_cpu.sh'),(29,1,10,'UPTIME_MONITOR','scripts/check_uptime.sh'),(30,1,10,'PS_MONITOR','scripts/check_ps.sh');
/*!40000 ALTER TABLE `HOST_MON_CFG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_SNMP_CONFIG`
--

DROP TABLE IF EXISTS `HOST_SNMP_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_SNMP_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `auth` varchar(255) DEFAULT NULL,
  `authPassword` varchar(255) DEFAULT NULL,
  `communityGet` varchar(255) DEFAULT NULL,
  `communitySet` varchar(255) DEFAULT NULL,
  `communityTrap` varchar(255) DEFAULT NULL,
  `engineId` varchar(255) DEFAULT NULL,
  `port` int(11) DEFAULT NULL,
  `priv` varchar(255) DEFAULT NULL,
  `privPassword` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `version` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `targetType` varchar(255) DEFAULT NULL,
  `HOST_CONFIG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_HOSTSNMP_HOST` (`HOST_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_SNMP_CONFIG`
--

LOCK TABLES `HOST_SNMP_CONFIG` WRITE;
/*!40000 ALTER TABLE `HOST_SNMP_CONFIG` DISABLE KEYS */;
INSERT INTO `HOST_SNMP_CONFIG` VALUES (4,NULL,NULL,'',NULL,NULL,NULL,161,NULL,NULL,NULL,'SNMPV1','LDAP','APP',5);
/*!40000 ALTER TABLE `HOST_SNMP_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_STATS`
--

DROP TABLE IF EXISTS `HOST_STATS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_STATS` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostName` varchar(255) DEFAULT NULL,
  `sysName` varchar(255) DEFAULT NULL,
  `ts` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_STATS_TS` (`ts`)
) ENGINE=MyISAM AUTO_INCREMENT=6847 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_STATS`
--

LOCK TABLES `HOST_STATS` WRITE;
/*!40000 ALTER TABLE `HOST_STATS` DISABLE KEYS */;
/*!40000 ALTER TABLE `HOST_STATS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_STATS_CPU`
--

DROP TABLE IF EXISTS `HOST_STATS_CPU`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_STATS_CPU` (
  `processor` varchar(255) NOT NULL,
  `idle` double DEFAULT NULL,
  `iowait` double DEFAULT NULL,
  `sys` double DEFAULT NULL,
  `usr` double DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`owner_id`,`processor`),
  KEY `FK_STATS_CPU_HOST` (`owner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_STATS_CPU`
--

LOCK TABLES `HOST_STATS_CPU` WRITE;
/*!40000 ALTER TABLE `HOST_STATS_CPU` DISABLE KEYS */;
/*!40000 ALTER TABLE `HOST_STATS_CPU` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_STATS_DISK`
--

DROP TABLE IF EXISTS `HOST_STATS_DISK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_STATS_DISK` (
  `fs` varchar(255) NOT NULL,
  `avail` bigint(20) DEFAULT NULL,
  `mount` varchar(255) DEFAULT NULL,
  `disk_size` bigint(20) DEFAULT NULL,
  `used` bigint(20) DEFAULT NULL,
  `usedPercent` bigint(20) DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`fs`,`owner_id`),
  KEY `FK_CFG_DISK_HOST` (`owner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_STATS_DISK`
--

LOCK TABLES `HOST_STATS_DISK` WRITE;
/*!40000 ALTER TABLE `HOST_STATS_DISK` DISABLE KEYS */;
/*!40000 ALTER TABLE `HOST_STATS_DISK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_STATS_INET`
--

DROP TABLE IF EXISTS `HOST_STATS_INET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_STATS_INET` (
  `descr` varchar(255) NOT NULL,
  `inOctet` float DEFAULT NULL,
  `outOctet` float DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `statusText` varchar(255) DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`descr`,`owner_id`),
  KEY `FK_CFG_INET_HOST` (`owner_id`),
  KEY `FK_INET_HOST_STATS` (`owner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_STATS_INET`
--

LOCK TABLES `HOST_STATS_INET` WRITE;
/*!40000 ALTER TABLE `HOST_STATS_INET` DISABLE KEYS */;
/*!40000 ALTER TABLE `HOST_STATS_INET` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `HOST_STATS_MEM`
--

DROP TABLE IF EXISTS `HOST_STATS_MEM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HOST_STATS_MEM` (
  `statName` varchar(255) NOT NULL,
  `free` bigint(20) DEFAULT NULL,
  `total` bigint(20) DEFAULT NULL,
  `used` bigint(20) DEFAULT NULL,
  `usedPct` float DEFAULT NULL,
  `owner_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`owner_id`,`statName`),
  KEY `FK_STATS_MEM_HOST` (`owner_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `HOST_STATS_MEM`
--

LOCK TABLES `HOST_STATS_MEM` WRITE;
/*!40000 ALTER TABLE `HOST_STATS_MEM` DISABLE KEYS */;
/*!40000 ALTER TABLE `HOST_STATS_MEM` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `INET_CATEGORY`
--

DROP TABLE IF EXISTS `INET_CATEGORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `INET_CATEGORY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `HOST_CONFIG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_INET_HOST` (`HOST_CONFIG_ID`),
  KEY `FK_CAT_INET_HOST` (`HOST_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `INET_CATEGORY`
--

LOCK TABLES `INET_CATEGORY` WRITE;
/*!40000 ALTER TABLE `INET_CATEGORY` DISABLE KEYS */;
INSERT INTO `INET_CATEGORY` VALUES (1,'eth0',4);
/*!40000 ALTER TABLE `INET_CATEGORY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `INET_LABEL`
--

DROP TABLE IF EXISTS `INET_LABEL`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `INET_LABEL` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `defaultName` varchar(255) DEFAULT NULL,
  `label` varchar(255) DEFAULT NULL,
  `INET_CATEGORY_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_INET_CATEGORY` (`INET_CATEGORY_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `INET_LABEL`
--

LOCK TABLES `INET_LABEL` WRITE;
/*!40000 ALTER TABLE `INET_LABEL` DISABLE KEYS */;
INSERT INTO `INET_LABEL` VALUES (1,'eth0','eth 0',1);
/*!40000 ALTER TABLE `INET_LABEL` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MIB_FILE_WRAPPER`
--

DROP TABLE IF EXISTS `MIB_FILE_WRAPPER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MIB_FILE_WRAPPER` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mibName` varchar(255) DEFAULT NULL,
  `HOST_SNMP_CONFIG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_MIBFILEWRAPPER_HOSTSNMPCONFIG` (`HOST_SNMP_CONFIG_ID`),
  KEY `FK_CFG_MIBFW_HOSTSNMPCFG` (`HOST_SNMP_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MIB_FILE_WRAPPER`
--

LOCK TABLES `MIB_FILE_WRAPPER` WRITE;
/*!40000 ALTER TABLE `MIB_FILE_WRAPPER` DISABLE KEYS */;
/*!40000 ALTER TABLE `MIB_FILE_WRAPPER` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MIB_FLAG`
--

DROP TABLE IF EXISTS `MIB_FLAG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MIB_FLAG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `loaded` tinyint(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `filepath` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=116 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MIB_FLAG`
--

LOCK TABLES `MIB_FLAG` WRITE;
/*!40000 ALTER TABLE `MIB_FLAG` DISABLE KEYS */;
INSERT INTO `MIB_FLAG` VALUES (110,1,'IF-MIB','/home/erl/smpserver/all-mibs/IF-MIB.txt'),(111,1,'UCD-SNMP-MIB','/home/erl/smpserver/all-mibs/UCD-SNMP-MIB.txt'),(112,1,'HOST-RESOURCES-MIB','/home/erl/smpserver/all-mibs/HOST-RESOURCES-MIB.txt');
/*!40000 ALTER TABLE `MIB_FLAG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MISC_CONFIG`
--

DROP TABLE IF EXISTS `MISC_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MISC_CONFIG` (
  `configKey` varchar(255) NOT NULL,
  `configValue` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`configKey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MISC_CONFIG`
--

LOCK TABLES `MISC_CONFIG` WRITE;
/*!40000 ALTER TABLE `MISC_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `MISC_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `MONITOR_THRESHOLD_CONFIG`
--

DROP TABLE IF EXISTS `MONITOR_THRESHOLD_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MONITOR_THRESHOLD_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `warningLevel` varchar(255) DEFAULT NULL,
  `operator` varchar(255) DEFAULT NULL,
  `target` varchar(255) DEFAULT NULL,
  `watermark` float DEFAULT NULL,
  `HOST_MONITOR_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_HOSTMON_THRESHOLD` (`HOST_MONITOR_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `MONITOR_THRESHOLD_CONFIG`
--

LOCK TABLES `MONITOR_THRESHOLD_CONFIG` WRITE;
/*!40000 ALTER TABLE `MONITOR_THRESHOLD_CONFIG` DISABLE KEYS */;
INSERT INTO `MONITOR_THRESHOLD_CONFIG` VALUES (59,'WARNING','gt','all',80,19),(60,'MINOR','gt','all',85,19),(61,'MAJOR','gt','all',90,19),(62,'WARNING','gt','uptime',100,20),(63,'MINOR','gt','uptime',200,20),(64,'MAJOR','gt','uptime',300,20),(65,'WARNING','gt','/dev/sda1',80,21),(66,'MINOR','gt','/dev/sda1',85,21),(67,'MAJOR','gt','/dev/sda1',90,21),(68,'WARNING','eq','eth0',1,22),(69,'WARNING','gt','mem',80,23),(70,'MINOR','gt','mem',85,23),(71,'MAJOR','gt','mem',90,23),(72,'WARNING','gt','cpu',70,24),(73,'MINOR','gt','cpu',80,24),(74,'MAJOR','gt','cpu',90,24),(75,'WARNING','gt','mem',70,24),(76,'MINOR','gt','mem',80,24),(77,'MAJOR','gt','mem',90,24),(78,'WARNING','gt','mem',80,25),(79,'MINOR','gt','mem',85,25),(80,'MAJOR','gt','mem',90,25),(81,'WARNING','gt','/dev/sda1',80,26),(82,'MINOR','gt','/dev/sda1',85,26),(83,'MAJOR','gt','/dev/sda1',90,26),(84,'WARNING','eq','eth0',1,27),(85,'WARNING','gt','all',80,28),(86,'MINOR','gt','all',85,28),(87,'MAJOR','gt','all',90,28),(88,'WARNING','gt','uptime',100,29),(89,'MINOR','gt','uptime',200,29),(90,'MAJOR','gt','uptime',300,29),(91,'WARNING','gt','cpu',70,30),(92,'MINOR','gt','cpu',80,30),(93,'MAJOR','gt','cpu',90,30),(94,'WARNING','gt','mem',70,30),(95,'MINOR','gt','mem',80,30),(96,'MAJOR','gt','mem',90,30);
/*!40000 ALTER TABLE `MONITOR_THRESHOLD_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NOTI_GROUP`
--

DROP TABLE IF EXISTS `NOTI_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NOTI_GROUP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` tinyint(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `occurence` int(11) DEFAULT NULL,
  `sms` tinyint(1) NOT NULL,
  `time` bigint(20) DEFAULT NULL,
  `trap` tinyint(1) NOT NULL,
  `triggerer` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `wildcard` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NOTI_GROUP`
--

LOCK TABLES `NOTI_GROUP` WRITE;
/*!40000 ALTER TABLE `NOTI_GROUP` DISABLE KEYS */;
/*!40000 ALTER TABLE `NOTI_GROUP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NOTI_GROUP_ALARM_DEFINITION`
--

DROP TABLE IF EXISTS `NOTI_GROUP_ALARM_DEFINITION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NOTI_GROUP_ALARM_DEFINITION` (
  `NOTI_GROUP_id` bigint(20) NOT NULL DEFAULT '0',
  `alarms_id` int(11) NOT NULL,
  PRIMARY KEY (`NOTI_GROUP_id`,`alarms_id`),
  KEY `FK400CFFD57CC2764A` (`NOTI_GROUP_id`),
  KEY `FK400CFFD582E293E9` (`alarms_id`),
  KEY `FK6AE50C6A82E293E9` (`alarms_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NOTI_GROUP_ALARM_DEFINITION`
--

LOCK TABLES `NOTI_GROUP_ALARM_DEFINITION` WRITE;
/*!40000 ALTER TABLE `NOTI_GROUP_ALARM_DEFINITION` DISABLE KEYS */;
/*!40000 ALTER TABLE `NOTI_GROUP_ALARM_DEFINITION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NOTI_GROUP_CONTACT_PERSON`
--

DROP TABLE IF EXISTS `NOTI_GROUP_CONTACT_PERSON`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NOTI_GROUP_CONTACT_PERSON` (
  `NOTI_GROUP_id` bigint(20) NOT NULL DEFAULT '0',
  `contacts_id` bigint(20) NOT NULL,
  PRIMARY KEY (`NOTI_GROUP_id`,`contacts_id`),
  KEY `FK1012DD287CC2764A` (`NOTI_GROUP_id`),
  KEY `FK1012DD28C56841E3` (`contacts_id`),
  KEY `FKD53F0AFDC56841E3` (`contacts_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NOTI_GROUP_CONTACT_PERSON`
--

LOCK TABLES `NOTI_GROUP_CONTACT_PERSON` WRITE;
/*!40000 ALTER TABLE `NOTI_GROUP_CONTACT_PERSON` DISABLE KEYS */;
/*!40000 ALTER TABLE `NOTI_GROUP_CONTACT_PERSON` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `NOTI_GROUP_GROUP_SNMP_CONFIG`
--

DROP TABLE IF EXISTS `NOTI_GROUP_GROUP_SNMP_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NOTI_GROUP_GROUP_SNMP_CONFIG` (
  `NOTI_GROUP_id` bigint(20) NOT NULL DEFAULT '0',
  `snmpConfigs_id` bigint(20) NOT NULL,
  PRIMARY KEY (`NOTI_GROUP_id`,`snmpConfigs_id`),
  KEY `FKF7E6E24F7CC2764A` (`NOTI_GROUP_id`),
  KEY `FKF7E6E24FF9CACC6D` (`snmpConfigs_id`),
  KEY `FK2810685AF9CACC6D` (`snmpConfigs_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NOTI_GROUP_GROUP_SNMP_CONFIG`
--

LOCK TABLES `NOTI_GROUP_GROUP_SNMP_CONFIG` WRITE;
/*!40000 ALTER TABLE `NOTI_GROUP_GROUP_SNMP_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `NOTI_GROUP_GROUP_SNMP_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OBJECT_NAME`
--

DROP TABLE IF EXISTS `OBJECT_NAME`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OBJECT_NAME` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mappedOid` tinyblob,
  `oid` tinyblob,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_OBJECT_NAME_OID` (`PARENT_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5188 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OBJECT_NAME`
--

LOCK TABLES `OBJECT_NAME` WRITE;
/*!40000 ALTER TABLE `OBJECT_NAME` DISABLE KEYS */;
INSERT INTO `OBJECT_NAME` VALUES (4848,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1058),(4849,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1058),(4850,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1058),(4851,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1058),(4852,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1059),(4853,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1059),(4854,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1059),(4855,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1059),(4856,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1060),(4857,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1060),(4858,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1060),(4859,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1060),(4860,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1061),(4861,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1061),(4862,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1061),(4863,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1061),(4864,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1062),(4865,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1062),(4866,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1062),(4867,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1062),(4868,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1063),(4869,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1063),(4870,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1063),(4871,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1063),(4872,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1064),(4873,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1064),(4874,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1064),(4875,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1064),(4876,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1065),(4877,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1065),(4878,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1065),(4879,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1065),(4880,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1066),(4881,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1066),(4882,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1066),(4883,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1066),(4884,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1067),(4885,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1067),(4886,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1067),(4887,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1067),(4888,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1068),(4889,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1068),(4890,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1068),(4891,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1068),(4892,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1069),(4893,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1069),(4894,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1069),(4895,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1069),(4896,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1070),(4897,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1070),(4898,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1070),(4899,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1070),(4900,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1071),(4901,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1071),(4902,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1071),(4903,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1071),(4904,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1072),(4905,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1072),(4906,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1072),(4907,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1072),(4908,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1073),(4909,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1073),(4910,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1073),(4911,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1073),(4912,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1074),(4913,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1074),(4914,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1074),(4915,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1074),(4916,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1075),(4917,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1075),(4918,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1075),(4919,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1075),(4920,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1076),(4921,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1076),(4922,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1076),(4923,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1076),(4924,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1077),(4925,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1077),(4926,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1077),(4927,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1077),(4928,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1078),(4929,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1078),(4930,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1078),(4931,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1078),(4932,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1079),(4933,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1079),(4934,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1079),(4935,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1079),(4936,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1080),(4937,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1080),(4938,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1080),(4939,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1080),(4940,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1081),(4941,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1081),(4942,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1081),(4943,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1081),(4944,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1082),(4945,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1082),(4946,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1082),(4947,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1082),(4948,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1083),(4949,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1083),(4950,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1083),(4951,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1083),(4952,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1084),(4953,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1084),(4954,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1084),(4955,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1084),(4956,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1085),(4957,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1085),(4958,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1085),(4959,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1085),(4960,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1086),(4961,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1086),(4962,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1086),(4963,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1086),(4964,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1087),(4965,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1087),(4966,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1087),(4967,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\0\0\0\0',1087),(4968,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4969,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4970,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4971,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4972,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4973,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4974,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	',1088),(4975,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4976,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4977,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4978,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1088),(4979,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',1089),(4980,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1089),(4981,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1089),(4982,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1089),(4983,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1089),(4984,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',1090),(4985,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1090),(4986,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1090),(4987,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1090),(4988,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1090),(4989,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1090),(4990,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',1090),(4991,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(4992,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',1091),(4993,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(4994,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r',1091),(4995,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(4996,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(4997,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(4998,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(4999,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(5000,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(5001,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(5002,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(5003,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(5004,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(5005,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1091),(5006,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5007,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',1092),(5008,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5009,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r',1092),(5010,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5011,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5012,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5013,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5014,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5015,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5016,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5017,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5018,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5019,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5020,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5021,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',1092),(5022,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1092),(5023,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5024,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',1093),(5025,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5026,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r',1093),(5027,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5028,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5029,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5030,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5031,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5032,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5033,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5034,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5035,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5036,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5037,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5038,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5039,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5040,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	',1093),(5041,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',1093),(5042,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5043,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5044,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r',1093),(5045,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1093),(5046,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1094),(5047,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1094),(5048,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1095),(5049,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1095),(5050,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1095),(5051,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1095),(5052,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1095),(5053,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1095),(5054,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1096),(5055,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5056,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5057,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5058,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5059,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5060,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5061,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5062,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5063,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	',1097),(5064,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5065,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5066,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5067,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5068,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5069,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1097),(5070,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1098),(5071,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1098),(5072,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1099),(5073,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1099),(5074,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1099),(5075,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1099),(5076,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1100),(5077,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1101),(5078,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1101),(5079,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1101),(5080,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1101),(5081,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1101),(5082,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1101),(5083,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1101),(5084,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1102),(5085,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1102),(5086,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1102),(5087,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1102),(5088,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1102),(5089,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1102),(5090,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1102),(5091,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1102),(5092,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5093,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5094,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5095,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5096,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5097,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5098,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5099,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5100,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5101,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5102,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5103,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5104,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5105,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5106,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5107,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5108,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5109,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5110,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5111,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5112,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5113,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5114,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5115,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5116,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5117,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5118,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5119,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1103),(5120,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	',1103),(5121,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1104),(5122,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1104),(5123,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1104),(5124,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1104),(5125,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1104),(5126,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1104),(5127,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1104),(5128,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1104),(5129,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1105),(5130,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1105),(5131,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1106),(5132,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1106),(5133,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1106),(5134,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1106),(5135,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1106),(5136,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1106),(5137,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1106),(5138,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1109),(5139,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1109),(5140,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1109),(5141,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1110),(5142,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1110),(5143,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1110),(5144,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1112),(5145,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1113),(5146,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1113),(5147,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1113),(5148,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1113),(5149,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1113),(5150,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1113),(5151,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1113),(5152,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1113),(5153,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0',1113),(5154,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0',1113),(5155,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	\0\0\0\0\0\0',1113),(5156,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1114),(5157,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1114),(5158,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1114),(5159,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1114),(5160,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1114),(5161,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 ','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0 ',1114),(5162,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1115),(5163,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1115),(5164,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5165,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5166,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	',1116),(5167,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',1116),(5168,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5169,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5170,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r',1116),(5171,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5172,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5173,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5174,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5175,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5176,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5177,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5178,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5179,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5180,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5181,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5182,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\Z',1116),(5183,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5184,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5185,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1116),(5186,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1117),(5187,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',1117);
/*!40000 ALTER TABLE `OBJECT_NAME` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OBJECT_NAME_MAP`
--

DROP TABLE IF EXISTS `OBJECT_NAME_MAP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OBJECT_NAME_MAP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mappedValue` varchar(255) DEFAULT NULL,
  `oid` tinyblob,
  `value` varchar(255) DEFAULT NULL,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_OBJECT_NAME_MAP_OID` (`PARENT_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OBJECT_NAME_MAP`
--

LOCK TABLES `OBJECT_NAME_MAP` WRITE;
/*!40000 ALTER TABLE `OBJECT_NAME_MAP` DISABLE KEYS */;
/*!40000 ALTER TABLE `OBJECT_NAME_MAP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `OID_MAP`
--

DROP TABLE IF EXISTS `OID_MAP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OID_MAP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mappedOid` tinyblob,
  `name` varchar(255) DEFAULT NULL,
  `oid` tinyblob,
  `PARENT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_OID_MAP_PRODUCT` (`PARENT_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=1120 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `OID_MAP`
--

LOCK TABLES `OID_MAP` WRITE;
/*!40000 ALTER TABLE `OID_MAP` DISABLE KEYS */;
INSERT INTO `OID_MAP` VALUES (1058,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0\0\0\0',150),(1059,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0\0\0\0',150),(1060,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0\0\0\0\r',150),(1061,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0\0\0\0',150),(1062,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0\0\0\0',150),(1063,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0\0\0\0',150),(1064,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0\0\0\0',150),(1065,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0\0\0\0',150),(1066,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0\0\0\0',150),(1067,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1068,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1069,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1070,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1071,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1072,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1073,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1074,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1075,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0	',151),(1076,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1077,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1078,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1079,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1080,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1081,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0\Z',151),(1082,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1083,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0',151),(1084,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0)',151),(1085,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\0*',151),(1086,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\03',151),(1087,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\r\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0\0\0\04',151),(1088,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1089,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1090,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1091,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1092,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1093,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1094,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1095,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1096,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	',152),(1097,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',152),(1098,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1099,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',152),(1100,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\r',152),(1101,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',153),(1102,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',153),(1103,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',153),(1104,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',153),(1105,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',153),(1106,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',153),(1107,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',148),(1108,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',148),(1109,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',148),(1110,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',148),(1111,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',148),(1112,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',154),(1113,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',154),(1114,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',154),(1115,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0	',154),(1116,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\n',154),(1117,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\n\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',154),(1118,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\E5\0\0\0\FB\0\0\0',149),(1119,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0',NULL,'\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\E5\0\0\0\FB\0\0\0',149);
/*!40000 ALTER TABLE `OID_MAP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROCESS_ACTION_COMMAND`
--

DROP TABLE IF EXISTS `PROCESS_ACTION_COMMAND`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROCESS_ACTION_COMMAND` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outputLimit` int(11) NOT NULL,
  `script` varchar(255) DEFAULT NULL,
  `timeout` int(11) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `priority` int(11) NOT NULL,
  `EVENT_HANDLER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_ACTION_EVENTHANDLER` (`EVENT_HANDLER_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROCESS_ACTION_COMMAND`
--

LOCK TABLES `PROCESS_ACTION_COMMAND` WRITE;
/*!40000 ALTER TABLE `PROCESS_ACTION_COMMAND` DISABLE KEYS */;
/*!40000 ALTER TABLE `PROCESS_ACTION_COMMAND` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROCESS_CONFIG`
--

DROP TABLE IF EXISTS `PROCESS_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROCESS_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `adminLink` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pidPath` varchar(255) DEFAULT NULL,
  `startScriptPath` varchar(255) DEFAULT NULL,
  `stopScriptPath` varchar(255) DEFAULT NULL,
  `HOST_CONFIG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_PROCESS_HOST` (`HOST_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROCESS_CONFIG`
--

LOCK TABLES `PROCESS_CONFIG` WRITE;
/*!40000 ALTER TABLE `PROCESS_CONFIG` DISABLE KEYS */;
INSERT INTO `PROCESS_CONFIG` VALUES (3,'','tomcat instance',0,'Web','/usr/bin/tomcat/tomcat',NULL,NULL,4);
/*!40000 ALTER TABLE `PROCESS_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROCESS_EVENT_HANDLER_CONFIG`
--

DROP TABLE IF EXISTS `PROCESS_EVENT_HANDLER_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROCESS_EVENT_HANDLER_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `eventType` varchar(255) DEFAULT NULL,
  `PROCESS_CONFIG_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_EVENT_HANDLER_PROCESS` (`PROCESS_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROCESS_EVENT_HANDLER_CONFIG`
--

LOCK TABLES `PROCESS_EVENT_HANDLER_CONFIG` WRITE;
/*!40000 ALTER TABLE `PROCESS_EVENT_HANDLER_CONFIG` DISABLE KEYS */;
INSERT INTO `PROCESS_EVENT_HANDLER_CONFIG` VALUES (7,'STARTUP',3),(8,'SHUTDOWN',3),(9,'DIED',3);
/*!40000 ALTER TABLE `PROCESS_EVENT_HANDLER_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROCESS_SNMP_CONFIG`
--

DROP TABLE IF EXISTS `PROCESS_SNMP_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROCESS_SNMP_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `message` varchar(255) DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  `trap` varchar(255) DEFAULT NULL,
  `EVENT_HANDLER_CONFIG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_SNMP_EVENT_HANDLER` (`EVENT_HANDLER_CONFIG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROCESS_SNMP_CONFIG`
--

LOCK TABLES `PROCESS_SNMP_CONFIG` WRITE;
/*!40000 ALTER TABLE `PROCESS_SNMP_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `PROCESS_SNMP_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PROCESS_STATS`
--

DROP TABLE IF EXISTS `PROCESS_STATS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PROCESS_STATS` (
  `hostName` varchar(100) NOT NULL,
  `processName` varchar(100) NOT NULL,
  `sysName` varchar(100) NOT NULL,
  `ts` datetime NOT NULL,
  `cpu` float DEFAULT NULL,
  `mem` float DEFAULT NULL,
  PRIMARY KEY (`hostName`,`processName`,`sysName`,`ts`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PROCESS_STATS`
--

LOCK TABLES `PROCESS_STATS` WRITE;
/*!40000 ALTER TABLE `PROCESS_STATS` DISABLE KEYS */;
/*!40000 ALTER TABLE `PROCESS_STATS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `PRODUCT_GROUP`
--

DROP TABLE IF EXISTS `PRODUCT_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PRODUCT_GROUP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `oid` tinyblob,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=155 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `PRODUCT_GROUP`
--

LOCK TABLES `PRODUCT_GROUP` WRITE;
/*!40000 ALTER TABLE `PRODUCT_GROUP` DISABLE KEYS */;
INSERT INTO `PRODUCT_GROUP` VALUES (148,'snmpTraps','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(149,'ucdTraps','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\E5\0\0\0\FB'),(150,'svrGenCataID','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\D0\0\0\0\0\0\0\0\0\0'),(151,'svrSmpCataID','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0I	\0\0\0\0\0\0\E7\0\0\0\0\0\0\0\0\0'),(152,'ifGroups','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(153,'hrMIBGroups','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0'),(154,'snmpMIBGroups','\AC\ED\0sr\0org.snmp4j.smi.OIDhbT\80\B0S:t\0[\0valuet\0[Ixr\0org.snmp4j.smi.AbstractVariable_\C4\F02\AE\88\0\0xpur\0[IM\BA`&v겥\0\0xp\0\0\0	\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0');
/*!40000 ALTER TABLE `PRODUCT_GROUP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RRD_ARCH_CFG`
--

DROP TABLE IF EXISTS `RRD_ARCH_CFG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RRD_ARCH_CFG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `frequency` int(11) NOT NULL,
  `frequencyType` varchar(255) DEFAULT NULL,
  `length` int(11) NOT NULL,
  `lengthType` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `rrd_row` int(11) DEFAULT NULL,
  `step` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `xff` float NOT NULL,
  `RRD_DEF_CONFIG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_RRD_ARCHIVE_RRD_DEF` (`RRD_DEF_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RRD_ARCH_CFG`
--

LOCK TABLES `RRD_ARCH_CFG` WRITE;
/*!40000 ALTER TABLE `RRD_ARCH_CFG` DISABLE KEYS */;
INSERT INTO `RRD_ARCH_CFG` VALUES (39,10,'MINUTE',20,'MINUTE','Archive1',2,60,'AVERAGE','',0.5,10);
/*!40000 ALTER TABLE `RRD_ARCH_CFG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RRD_DEF_CONFIG`
--

DROP TABLE IF EXISTS `RRD_DEF_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RRD_DEF_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `createDate` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `lastUpdate` datetime DEFAULT NULL,
  `locked` tinyint(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `step` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RRD_DEF_CONFIG`
--

LOCK TABLES `RRD_DEF_CONFIG` WRITE;
/*!40000 ALTER TABLE `RRD_DEF_CONFIG` DISABLE KEYS */;
INSERT INTO `RRD_DEF_CONFIG` VALUES (10,NULL,0,NULL,0,'Test1',NULL,10);
/*!40000 ALTER TABLE `RRD_DEF_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `RRD_DS_CONFIG`
--

DROP TABLE IF EXISTS `RRD_DS_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `RRD_DS_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `heartbeat` bigint(20) NOT NULL,
  `maximum` float DEFAULT NULL,
  `minimum` float DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `RRD_DEF_CONFIG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_RRD_DS_RRD_DEF` (`RRD_DEF_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=68 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `RRD_DS_CONFIG`
--

LOCK TABLES `RRD_DS_CONFIG` WRITE;
/*!40000 ALTER TABLE `RRD_DS_CONFIG` DISABLE KEYS */;
/*!40000 ALTER TABLE `RRD_DS_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SCRIPT_CONFIG`
--

DROP TABLE IF EXISTS `SCRIPT_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SCRIPT_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `HOST_CONFIG_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_CFG_SCRIPT_HOST` (`HOST_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SCRIPT_CONFIG`
--

LOCK TABLES `SCRIPT_CONFIG` WRITE;
/*!40000 ALTER TABLE `SCRIPT_CONFIG` DISABLE KEYS */;
INSERT INTO `SCRIPT_CONFIG` VALUES (1,'Script1','/home/erl/smpserver/script/AWS_SERVER_1_Script1_2015-02-09_07-12-01_PM.sh',4);
/*!40000 ALTER TABLE `SCRIPT_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SITE`
--

DROP TABLE IF EXISTS `SITE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SITE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contactPerson` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `siteName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `siteName` (`siteName`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SITE`
--

LOCK TABLES `SITE` WRITE;
/*!40000 ALTER TABLE `SITE` DISABLE KEYS */;
INSERT INTO `SITE` VALUES (1,'','','USA','','LEAD'),(2,'','','Hong Kong','','HLR');
/*!40000 ALTER TABLE `SITE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SNMP_TARGET`
--

DROP TABLE IF EXISTS `SNMP_TARGET`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SNMP_TARGET` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `keyDir` varchar(255) DEFAULT NULL,
  `keyType` varchar(255) DEFAULT NULL,
  `methodType` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `suffix` int(11) NOT NULL,
  `targetString` varchar(255) DEFAULT NULL,
  `valueDir` varchar(255) DEFAULT NULL,
  `walkDir` varchar(255) DEFAULT NULL,
  `HOST_SNMP_CONFIG_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKF7FB37B250E9D94A` (`HOST_SNMP_CONFIG_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SNMP_TARGET`
--

LOCK TABLES `SNMP_TARGET` WRITE;
/*!40000 ALTER TABLE `SNMP_TARGET` DISABLE KEYS */;
/*!40000 ALTER TABLE `SNMP_TARGET` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SNMP_TRAP`
--

DROP TABLE IF EXISTS `SNMP_TRAP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SNMP_TRAP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `VERSION` int(11) DEFAULT NULL,
  `alarmDescription` varchar(255) DEFAULT NULL,
  `direction` int(11) DEFAULT NULL,
  `lastCheckTime` datetime DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `sourceIp` varchar(255) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `trapAlarmId` varchar(255) DEFAULT NULL,
  `trapAlarmLevel` varchar(255) DEFAULT NULL,
  `trapAppName` varchar(255) DEFAULT NULL,
  `trapDescription` varchar(255) DEFAULT NULL,
  `trapName` varchar(255) DEFAULT NULL,
  `trapVersion` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_TIME` (`time`),
  KEY `IDX_TRAP_TIME` (`time`)
) ENGINE=MyISAM AUTO_INCREMENT=59725 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SNMP_TRAP`
--

LOCK TABLES `SNMP_TRAP` WRITE;
/*!40000 ALTER TABLE `SNMP_TRAP` DISABLE KEYS */;
INSERT INTO `SNMP_TRAP` VALUES (59646,0,'Failed to initialise system context',1,'2015-02-06 16:10:34','1.3.6.1.4.1.18697.0.999.1.1.1.9','SMP Web Console','0.0.0.0','2015-02-06 16:10:34',NULL,'FATAL','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.9',0),(59647,0,'Failed to initialise system context',1,'2015-02-06 16:10:34','1.3.6.1.4.1.18697.0.999.1.1.1.9','SMP Web Console','0.0.0.0','2015-02-06 16:10:34',NULL,'WARNING','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.9',0),(59648,0,'Failed to initialise system context',1,'2015-02-06 16:12:04','1.3.6.1.4.1.18697.0.999.1.1.1.9','SMP Web Console','0.0.0.0','2015-02-06 16:12:04',NULL,'FATAL','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.9',0),(59649,0,'Failed to initialise system context',1,'2015-02-06 16:12:04','1.3.6.1.4.1.18697.0.999.1.1.1.9','SMP Web Console','0.0.0.0','2015-02-06 16:12:04',NULL,'WARNING','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.9',0),(59650,0,'System started successfully',1,'2015-02-06 16:12:47','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-06 16:12:47',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59651,0,'System started successfully',1,'2015-02-06 16:34:54','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-06 16:34:54',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59652,0,'System started successfully',1,'2015-02-06 17:25:15','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-06 17:25:15',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59653,0,'System started successfully',1,'2015-02-06 17:28:26','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-06 17:28:26',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59654,0,'System started successfully',1,'2015-02-06 17:42:40','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-06 17:42:40',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59655,0,'System started successfully',1,'2015-02-06 17:57:09','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-06 17:57:09',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59656,0,'Failed to initialise system context',1,'2015-02-08 16:18:32','1.3.6.1.4.1.18697.0.999.1.1.1.9','SMP Web Console','0.0.0.0','2015-02-08 16:18:32',NULL,'FATAL','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.9',0),(59657,0,'Failed to initialise system context',1,'2015-02-08 16:18:33','1.3.6.1.4.1.18697.0.999.1.1.1.9','SMP Web Console','0.0.0.0','2015-02-08 16:18:33',NULL,'WARNING','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.9',0),(59658,0,'System started successfully',1,'2015-02-08 16:19:30','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-08 16:19:30',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59659,0,'System started successfully',1,'2015-02-08 16:20:30','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-08 16:20:30',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59660,0,'System started successfully',1,'2015-02-09 09:24:06','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 09:24:06',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59661,0,'System started successfully',1,'2015-02-09 09:27:09','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 09:27:09',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59662,0,'System started successfully',1,'2015-02-09 09:37:43','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 09:37:43',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59663,0,'System started successfully',1,'2015-02-09 10:08:44','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 10:08:44',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59664,0,'System started successfully',1,'2015-02-09 10:15:07','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 10:15:07',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59665,0,'System started successfully',1,'2015-02-09 10:45:09','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 10:45:09',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59666,0,'System started successfully',1,'2015-02-09 11:17:48','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 11:17:48',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59667,0,'System started successfully',1,'2015-02-09 12:03:41','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 12:03:41',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59668,0,'System started successfully',1,'2015-02-09 12:15:55','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 12:15:55',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59669,0,'System started successfully',1,'2015-02-09 12:17:06','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 12:17:06',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59670,0,'System started successfully',1,'2015-02-09 12:45:23','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 12:45:23',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59671,0,'System started successfully',1,'2015-02-09 12:59:10','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 12:59:10',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59672,0,'System started successfully',1,'2015-02-09 13:03:42','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 13:03:42',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59673,0,'System started successfully',1,'2015-02-09 13:14:11','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 13:14:11',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59674,0,'System started successfully',1,'2015-02-09 13:19:51','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 13:19:51',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59675,0,'System started successfully',1,'2015-02-09 15:16:29','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 15:16:29',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59676,0,'System started successfully',1,'2015-02-09 15:26:45','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 15:26:45',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59677,0,'System started successfully',1,'2015-02-09 15:30:42','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 15:30:42',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59678,0,'System started successfully',1,'2015-02-09 15:38:21','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 15:38:21',NULL,'INFO','SMP Web Console','This notification is generated when the system is\nstarted up sucessfully.','sysStartUpSuccessNotification',0),(59679,0,'System started successfully',1,'2015-02-09 15:43:38','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 15:43:38',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59680,0,'System started successfully',1,'2015-02-09 15:48:44','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 15:48:44',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59681,0,'System started successfully',1,'2015-02-09 15:51:10','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 15:51:10',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59682,0,'System started successfully',1,'2015-02-09 15:52:24','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 15:52:24',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59683,0,'System started successfully',1,'2015-02-09 15:53:58','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 15:53:58',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59684,0,'System started successfully',1,'2015-02-09 16:04:51','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 16:04:51',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59685,0,'System started successfully',1,'2015-02-09 16:10:39','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 16:10:39',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59686,0,'System started successfully',1,'2015-02-09 16:20:07','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 16:20:07',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59687,0,'System started successfully',1,'2015-02-09 16:50:07','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 16:50:07',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59688,0,'System started successfully',1,'2015-02-09 17:27:09','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 17:27:09',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59689,0,'System started successfully',1,'2015-02-09 17:44:42','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-09 17:44:42',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59690,0,'System started successfully',1,'2015-02-10 10:26:43','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-10 10:26:43',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59691,0,'System started successfully',1,'2015-02-10 10:56:49','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-10 10:56:49',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59692,0,'System started successfully',1,'2015-02-10 18:49:27','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-10 18:49:27',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59693,0,'System started successfully',1,'2015-02-11 19:16:00','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-11 19:16:00',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59694,0,'System started successfully',1,'2015-02-12 11:55:52','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 11:55:52',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59695,0,'System started successfully',1,'2015-02-12 15:23:27','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 15:23:27',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59696,0,'System started successfully',1,'2015-02-12 15:43:33','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 15:43:33',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59697,0,'System started successfully',1,'2015-02-12 15:46:18','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 15:46:18',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59698,0,'System started successfully',1,'2015-02-12 16:01:05','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 16:01:05',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59699,0,'System started successfully',1,'2015-02-12 16:05:08','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 16:05:08',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59700,0,'System started successfully',1,'2015-02-12 16:15:34','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 16:15:34',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59701,0,'System started successfully',1,'2015-02-12 16:19:51','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 16:19:51',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59702,0,'System started successfully',1,'2015-02-12 16:34:34','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 16:34:34',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59703,0,'System started successfully',1,'2015-02-12 19:12:22','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 19:12:22',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59704,0,'System started successfully',1,'2015-02-12 19:13:24','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-12 19:13:24',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59705,0,'System started successfully',1,'2015-02-13 09:21:09','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 09:21:09',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59706,0,'System started successfully',1,'2015-02-13 09:56:09','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 09:56:09',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59707,0,'System started successfully',1,'2015-02-13 09:56:38','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 09:56:38',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59708,0,'System started successfully',1,'2015-02-13 10:05:21','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 10:05:21',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59709,0,'System started successfully',1,'2015-02-13 10:06:13','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 10:06:13',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59710,0,'System started successfully',1,'2015-02-13 10:07:53','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 10:07:53',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59711,0,'System started successfully',1,'2015-02-13 10:19:01','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 10:19:01',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59712,0,'System started successfully',1,'2015-02-13 10:21:39','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 10:21:39',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59713,0,'System started successfully',1,'2015-02-13 11:20:20','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 11:20:20',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59714,0,'System started successfully',1,'2015-02-13 11:34:02','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 11:34:02',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59715,0,'System started successfully',1,'2015-02-13 11:59:38','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 11:59:38',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59716,0,'System started successfully',1,'2015-02-13 12:02:51','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 12:02:51',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59717,0,'System started successfully',1,'2015-02-13 12:04:03','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 12:04:03',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59718,0,'Failed to initialise system context',1,'2015-02-13 12:55:04','1.3.6.1.4.1.18697.0.999.1.1.1.9','SMP Web Console','0.0.0.0','2015-02-13 12:55:04',NULL,'FATAL','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.9',0),(59719,0,'System started successfully',1,'2015-02-13 12:56:35','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 12:56:35',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59720,0,'System started successfully',1,'2015-02-13 12:57:47','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 12:57:47',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59721,0,'System started successfully',1,'2015-02-13 13:11:31','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 13:11:31',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59722,0,'System started successfully',1,'2015-02-13 13:16:57','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 13:16:57',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59723,0,'System started successfully',1,'2015-02-13 14:41:55','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 14:41:55',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0),(59724,0,'System started successfully',1,'2015-02-13 15:01:52','1.3.6.1.4.1.18697.0.999.1.1.1.8','SMP Web Console','0.0.0.0','2015-02-13 15:01:52',NULL,'INFO','SMP Web Console',NULL,'1.3.6.1.4.1.18697.0.999.1.1.1.8',0);
/*!40000 ALTER TABLE `SNMP_TRAP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `SYSTEM_CONFIG`
--

DROP TABLE IF EXISTS `SYSTEM_CONFIG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SYSTEM_CONFIG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `SYSTEM_CONFIG`
--

LOCK TABLES `SYSTEM_CONFIG` WRITE;
/*!40000 ALTER TABLE `SYSTEM_CONFIG` DISABLE KEYS */;
INSERT INTO `SYSTEM_CONFIG` VALUES (3,'LEAD RA',1,'LEAD'),(4,'Hong Kong HLR',1,'HLR');
/*!40000 ALTER TABLE `SYSTEM_CONFIG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserGroup`
--

DROP TABLE IF EXISTS `UserGroup`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserGroup` (
  `groupId` varchar(30) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `groupName` varchar(60) DEFAULT NULL,
  `lastUpdatedBy` varchar(30) DEFAULT NULL,
  `lastUpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`groupId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserGroup`
--

LOCK TABLES `UserGroup` WRITE;
/*!40000 ALTER TABLE `UserGroup` DISABLE KEYS */;
INSERT INTO `UserGroup` VALUES ('superGroup','2015-02-06 16:10:17','Superuser Group',NULL,'2015-02-06 16:10:17');
/*!40000 ALTER TABLE `UserGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserRight`
--

DROP TABLE IF EXISTS `UserRight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserRight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `accessLevel` varchar(10) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `lastUpdatedBy` varchar(30) DEFAULT NULL,
  `lastUpdatedOn` datetime DEFAULT NULL,
  `typeId` varchar(50) NOT NULL,
  `ROLEID` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ROLEID` (`ROLEID`,`typeId`),
  KEY `FK_USERRIGHT_USERROLE` (`ROLEID`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserRight`
--

LOCK TABLES `UserRight` WRITE;
/*!40000 ALTER TABLE `UserRight` DISABLE KEYS */;
INSERT INTO `UserRight` VALUES (1,'WRITE','2015-02-06 16:10:17',NULL,'2015-02-06 16:10:17','MANAGE_USER','superRole'),(2,'WRITE','2015-02-06 16:10:17',NULL,'2015-02-06 16:10:17','COMMAND','superRole'),(3,'WRITE','2015-02-06 16:10:17',NULL,'2015-02-06 16:10:17','CONFIG','superRole'),(4,'WRITE','2015-02-06 16:10:17',NULL,'2015-02-06 16:10:17','MONITOR','superRole');
/*!40000 ALTER TABLE `UserRight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `UserRole`
--

DROP TABLE IF EXISTS `UserRole`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UserRole` (
  `roleId` varchar(30) NOT NULL,
  `createDate` datetime DEFAULT NULL,
  `lastUpdatedBy` varchar(30) DEFAULT NULL,
  `lastUpdatedOn` datetime NOT NULL,
  `roleName` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `UserRole`
--

LOCK TABLES `UserRole` WRITE;
/*!40000 ALTER TABLE `UserRole` DISABLE KEYS */;
INSERT INTO `UserRole` VALUES ('superRole','2015-02-06 16:10:17',NULL,'2015-02-06 16:10:17','Superuser Role');
/*!40000 ALTER TABLE `UserRole` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `VARIABLE`
--

DROP TABLE IF EXISTS `VARIABLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VARIABLE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `mappedOid` varchar(255) DEFAULT NULL,
  `oid` varchar(255) DEFAULT NULL,
  `value` varchar(255) DEFAULT NULL,
  `ALARM_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_VARIABLE_TRAP` (`ALARM_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=10561 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `VARIABLE`
--

LOCK TABLES `VARIABLE` WRITE;
/*!40000 ALTER TABLE `VARIABLE` DISABLE KEYS */;
/*!40000 ALTER TABLE `VARIABLE` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-13 15:18:56
