/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuralt.smp.client.web.controller;

/**
 *
 * @author ag111
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.neuralt.smp.client.data.DataAccess;
import com.neuralt.smp.client.web.MessageKey;
import com.neuralt.smp.client.web.security.AuditLogger;
import com.neuralt.smp.client.web.security.AuditTrailLog.ActionType;
import com.neuralt.smp.client.web.security.ScreenType;
import com.neuralt.smp.client.data.CRSMS_CHANNEL_PROFILE;
import com.neuralt.smp.client.web.security.UserBean;
import com.neuralt.web.util.jsf.WebUtil;
import javax.faces.bean.ManagedProperty;

@ManagedBean
@ViewScoped
public class ManageCPController implements Serializable {
    
	private static final long serialVersionUID = -7016477944366559290L;
	private static final Logger logger = LoggerFactory
			.getLogger(SiteController.class);
        
        @ManagedProperty(value = "#{UserBean}")
	private UserBean userBean;
        
        @ManagedProperty(value = "#{CRSMS_CHANNEL_PROFILE}")
	//private UserBean userBean;
	private List<CRSMS_CHANNEL_PROFILE> managecps = new ArrayList<CRSMS_CHANNEL_PROFILE>();
	private CRSMS_CHANNEL_PROFILE managecp = new CRSMS_CHANNEL_PROFILE();
	private CRSMS_CHANNEL_PROFILE selectedmanagecp;
	private CRSMS_CHANNEL_PROFILE dummymanagecp= new CRSMS_CHANNEL_PROFILE();
	private List<SelectItem> cpSelectItems = new ArrayList<SelectItem>();	
	private String passedParameter;
        private String passwordCurrent;
	private String password;
	private String passwordReenter;
        
        private boolean createNew;
	
	public ManageCPController() {
		reload();
	}
        
        private void resetPassword() {
		password = null;
		passwordCurrent = null;
		passwordReenter = null;
	}
	
        public void reload() {
		managecp = new CRSMS_CHANNEL_PROFILE();
		dummymanagecp = new CRSMS_CHANNEL_PROFILE();		
		
		managecps = DataAccess.manageCPDao.getAllCPs();
		if (managecps != null) {
			logger.debug("cp size=" + managecps.size());
		}
	   	   		 
		cpSelectItems = null;
		cpSelectItems = new ArrayList<SelectItem>();	
		
		cpSelectItems.add(new SelectItem("All", "All"));
		
	    for(CRSMS_CHANNEL_PROFILE managecp: managecps){
	    	//cpSelectItems.add(new SelectItem(site.getSiteName(), site.getSiteName()));
                cpSelectItems.add(new SelectItem(managecp.getchannel_id(), managecp.getuserid()));
	    }
		    
		selectedmanagecp = null;
		
		String selectedCPID = getPassedParameter("dataTabView:siteForm:ManageCP"); 
		logger.info("selectedCPID=" + selectedCPID);
		if (selectedCPID != null)
		{
			
			if (selectedCPID != "" && selectedCPID != "All")
			{
				managecps = null;
				managecps = new ArrayList<CRSMS_CHANNEL_PROFILE>();		
                                managecps.add( DataAccess.manageCPDao.getCPByName(selectedCPID));
			}
		}
	}

	public void addCP() {
		boolean ok = true;

//		ManageCP dummy = DataAccess.siteDao.getSiteByName(site.getSiteName());
                CRSMS_CHANNEL_PROFILE dummy = DataAccess.manageCPDao.getCPByName(managecp.getchannel_id().toString());
		if (dummy != null) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.GENERAL_ADD_FAIL);
		}

		if (ok) {
			//DataAccess.siteDao.save(site);
                        DataAccess.manageCPDao.save(managecp);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.INSERT,
					"add-manageCP", managecp.toString());
			reload();
		}
	}

	public void deleteCP(CRSMS_CHANNEL_PROFILE managecp) {
		boolean ok = true;
		//if (managecp.getHostCount() != 0) {
                if (managecp.getchannel_id() != 0) {
			ok = false;
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR,
					MessageKey.GENERAL_DELETE_FAIL);
		}
		if (ok) {
			DataAccess.manageCPDao.delete(managecp);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.DELETE,
					"delete-CP", managecp.toString());
			reload();
			WebUtil.addMessage(FacesMessage.SEVERITY_INFO,
					MessageKey.GENERAL_SUCCESS, managecp.getchannel_id()+ " "
							+ "is deleted");
		}
	}

	public void editCP() {
		logger.debug("dummySite=" + dummymanagecp);
		logger.debug("CP=" + managecp);
		boolean ok = true;

		CRSMS_CHANNEL_PROFILE dummy = DataAccess.manageCPDao.getCPByName(managecp.getchannel_id().toString());

		if (dummy != null && !dummy.equals(managecp)) {
			ok = false;
			managecp = new CRSMS_CHANNEL_PROFILE();
			WebUtil.addMessage(FacesMessage.SEVERITY_ERROR,
					MessageKey.GENERAL_ERROR, MessageKey.GENERAL_EDIT_FAIL);
		}

		if (ok) {
			managecp.update(dummymanagecp);
			DataAccess.manageCPDao.save(managecp);
			AuditLogger.logAuditTrail(ScreenType.CONFIG, ActionType.UPDATE,
					"edit-cp", managecp.toString());
			reload();
		}
	}

	// getter/setter

	public List<CRSMS_CHANNEL_PROFILE> getManageCPs() {
		return managecps;
	}

	public void setManageCPs(List<CRSMS_CHANNEL_PROFILE> managecps) {
		this.managecps = managecps;
	}

	public CRSMS_CHANNEL_PROFILE getManageCP() {
		return managecp;
	}

	public void setManageCP(CRSMS_CHANNEL_PROFILE managecp) {
		this.managecp = managecp;
	}

	public CRSMS_CHANNEL_PROFILE getSelectedCP() {
		return selectedmanagecp;
	}

	public void setSelectedCP(CRSMS_CHANNEL_PROFILE selectedmanagecp) {
		this.selectedmanagecp = selectedmanagecp;
	}

	public CRSMS_CHANNEL_PROFILE getDummyCP() {
		return dummymanagecp;
	}

	public void setDummyCP(CRSMS_CHANNEL_PROFILE dummymanagecp) {
		this.dummymanagecp = dummymanagecp.getBackup();
		this.managecp = dummymanagecp;
	}

	public boolean isCPEmpty() {
		return managecps == null || managecps.isEmpty();
	}
	
	public List<SelectItem> getCPSelectItems() {
		return cpSelectItems;
	}

	public void setCPSelectItems(List<SelectItem> cpSelectItems) {
		this.cpSelectItems = cpSelectItems;
	}
	
	public String getPassedParameter(String paramName) {
		FacesContext facesContext = FacesContext.getCurrentInstance();
		this.passedParameter = (String) facesContext.getExternalContext().
		getRequestParameterMap().get(paramName);
		return this.passedParameter;
	}
		 

	public void setPassedParameter(String passedParameter) {
		this.passedParameter = passedParameter;
	}	
        
        public boolean isCreateNew() {
		return createNew;
	}

	public void setCreateNew(boolean createNew) {
		this.createNew = createNew;
	}
        
        public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}
}
