/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.neuralt.smp.client.data;

/**
 *
 * @author ag111
 */


import java.io.Serializable;
import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Version;

import com.neuralt.smp.config.model.Editable;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name="CRSMS_CHANNEL_PROFILE")
public class CRSMS_CHANNEL_PROFILE implements Serializable, Comparable<CRSMS_CHANNEL_PROFILE> {
	private static final long serialVersionUID = 1L;

	@Id
	//@Column(length = 60)
	private Timestamp createDate;
	//@Version
	//@Column(nullable = false)
	// inverse side
	//@OneToMany(mappedBy = "channel_id", orphanRemoval = true, fetch = FetchType.EAGER)
	//@Cascade(value = { CascadeType.SAVE_UPDATE, CascadeType.DELETE })
	//private Set<ManageCP> channel_ids;
        private Integer channel_id;
        private String channel;
        private String userid;
        private String pwd;
        private String priority;
        private String status;
        private String lastUpdatedBy;
        //@Version
	//@Column(nullable = false)
        private Timestamp lastUpdatedOn;
        

	public CRSMS_CHANNEL_PROFILE() {
		createDate = new Timestamp(System.currentTimeMillis());
	}

	public CRSMS_CHANNEL_PROFILE(Integer channel_id, String channel, String userid, String priority, String status, String lastUpdatedBy) {
		this.channel_id = channel_id;
		this.channel = channel;
                this.userid = userid;
                this.priority = priority;
                this.status = status;
		this.lastUpdatedBy = lastUpdatedBy;
		this.createDate = new Timestamp(System.currentTimeMillis());
	}
        public CRSMS_CHANNEL_PROFILE getBackup() {
		CRSMS_CHANNEL_PROFILE dummy = new CRSMS_CHANNEL_PROFILE();
		dummy.setchannel_id(channel_id);
		dummy.setchannel(channel);
		dummy.setuserid(userid);
		dummy.setpriority(priority);
		dummy.setstatus(status);
		dummy.setLastUpdatedBy(lastUpdatedBy);
                dummy.setLastUpdatedOn(lastUpdatedOn);
		dummy.setCreateDate(createDate);
		return dummy;
	}

	public void update(CRSMS_CHANNEL_PROFILE dummy) {
		dummy.setchannel_id(channel_id);
		dummy.setchannel(channel);
		dummy.setuserid(userid);
		dummy.setpriority(priority);
		dummy.setstatus(status);
		dummy.setLastUpdatedBy(lastUpdatedBy);
                dummy.setLastUpdatedOn(lastUpdatedOn);
		dummy.setCreateDate(createDate);
	}

	public Integer getchannel_id() {
		return channel_id;
	}

	public void setchannel_id(Integer channel_id) {
		this.channel_id = channel_id;
	}

	public String getchannel() {
		return channel;
	}

	public void setchannel(String channel) {
		this.channel = channel;
	}
        
        public String getuserid() {
		return userid;
	}

	public void setuserid(String userid) {
		this.userid = userid;
	}
        
        public String getpassword() {
		return pwd;
	}

	public void setpassword(String pwd) {
		this.pwd = pwd;
	}
        
        public String getpriority() {
		return priority;
	}

	public void setpriority(String priority) {
		this.priority = priority;
	}
        
        public String getstatus() {
		return status;
	}

	public void setstatus(String status) {
		this.status = status;
	}

	public Timestamp getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}

	public Timestamp getLastUpdatedOn() {
		return lastUpdatedOn;
	}

	public void setLastUpdatedOn(Timestamp lastUpdatedOn) {
		this.lastUpdatedOn = lastUpdatedOn;
	}

	public String getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(String lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append("CHANNEL_ID: " + channel_id + ";");
		buffer.append("CHANNEL: " + channel + ";");
                buffer.append("USERID: " + userid + ";");
                buffer.append("PWD: " + pwd + ";");
                buffer.append("PRIORITY: " + priority + ";");
                buffer.append("STATUS: " + status + ";");
		if (createDate != null) {
			buffer.append("CREATE_DATE: " + createDate.toString() + ";");
		}
		if (lastUpdatedOn != null) {
			buffer.append("LAST_UPDATE_DATE: " + lastUpdatedOn.toString() + ";");
		}
		if (lastUpdatedBy != null) {
			buffer.append("UPDATED_BY: " + lastUpdatedBy);
		}
		return buffer.toString();
	}
        
        @Override
	public int compareTo(CRSMS_CHANNEL_PROFILE o) {
		if (o != null) {
			if (o.getchannel() != null && channel != null) {
				return channel.compareTo(o.getchannel());
			}
		}
		return 0;
	}
}


