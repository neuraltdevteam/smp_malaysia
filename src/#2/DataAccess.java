package com.neuralt.smp.client.data;

import com.neuralt.smp.client.data.config.AlarmConfigDAO;
import com.neuralt.smp.client.data.config.MiscConfig;
import com.neuralt.smp.client.data.dao.AlarmDAO;
import com.neuralt.smp.client.data.dao.ChartTemplateDAO;
import com.neuralt.smp.client.data.dao.ContactPersonDAO;
import com.neuralt.smp.client.data.dao.DataCollectionProcedureDAO;
import com.neuralt.smp.client.data.dao.EventLogDAO;
import com.neuralt.smp.client.data.dao.GenericDAO;
import com.neuralt.smp.client.data.dao.GenericHibernateDAO;
import com.neuralt.smp.client.data.dao.GroupSnmpConfigDAO;
import com.neuralt.smp.client.data.dao.HostConfigDAO;
import com.neuralt.smp.client.data.dao.HostStatsDAO;
import com.neuralt.smp.client.data.dao.ManageCPDAO;
import com.neuralt.smp.client.data.dao.ManageSMSDAO;
import com.neuralt.smp.client.data.dao.MibFlagDAO;
import com.neuralt.smp.client.data.dao.NotificationGroupDAO;
import com.neuralt.smp.client.data.dao.OidMapDAO;
import com.neuralt.smp.client.data.dao.ProcessConfigDAO;
import com.neuralt.smp.client.data.dao.ProductGroupDAO;
import com.neuralt.smp.client.data.dao.RrdDefConfigDAO;
import com.neuralt.smp.client.data.dao.ScriptConfigDAO;
import com.neuralt.smp.client.data.dao.SiteDAO;
import com.neuralt.smp.client.data.dao.SnmpTargetDAO;
import com.neuralt.smp.client.data.dao.SnmpTrapDAO;
import com.neuralt.smp.client.data.dao.SystemConfigDAO;
import com.neuralt.smp.client.data.dao.WebDAO;

/**
 * static container for shared DAOs
 */
public class DataAccess {
	
	public static final GenericHibernateDAO genDao = new GenericHibernateDAO();

	public static final EventLogDAO eventLogDao = new EventLogDAO(false);
	public static final HostStatsDAO hostStatsDao = new HostStatsDAO(false);
	public static final SnmpTrapDAO snmpTrapDao = new SnmpTrapDAO(false);
	public static final WebDAO webDao = new WebDAO();
	public static final SystemConfigDAO systemConfigDao = new SystemConfigDAO(
			true);
	public static final HostConfigDAO hostConfigDao = new HostConfigDAO(false);
	public static final ProcessConfigDAO processConfigDao = new ProcessConfigDAO(
			false);
	public static final OidMapDAO oidMapDao = new OidMapDAO(false);
	public static final ProductGroupDAO productGroupDao = new ProductGroupDAO(
			false);
	public static final NotificationGroupDAO notificationGroupDao = new NotificationGroupDAO(
			false);
	public static final AlarmDAO alarmDao = new AlarmDAO(false);
	public static final AlarmConfigDAO alarmConfigDao = new AlarmConfigDAO();
	public static final GenericDAO<MiscConfig, String> miscConfigDao = new GenericDAO<MiscConfig, String>(
			MiscConfig.class, false);
	public static final MibFlagDAO mibFlagDao = new MibFlagDAO(false);
	public static final SiteDAO siteDao = new SiteDAO(false);
	public static final ContactPersonDAO contactPersonDao = new ContactPersonDAO(
			false);
	public static final GroupSnmpConfigDAO groupSnmpConfigDao = new GroupSnmpConfigDAO(
			false);
	public static final SnmpTargetDAO snmpTargetDao = new SnmpTargetDAO(false);
	public static final RrdDefConfigDAO rrdDefConfigDao = new RrdDefConfigDAO(
			false);
	public static final ScriptConfigDAO scriptConfigDao = new ScriptConfigDAO(
			false);
	public static final DataCollectionProcedureDAO dataCollectionProcedureDao = new DataCollectionProcedureDAO(
			false);
	public static final ChartTemplateDAO chartTemplateDao = new ChartTemplateDAO(
			false);
        public static final ManageCPDAO manageCPDao = new ManageCPDAO(
			false);
        public static final ManageSMSDAO manageSMSDao = new ManageSMSDAO(
			false);
}
