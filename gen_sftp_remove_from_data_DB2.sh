#!/bin/bash
module_folder="/home/umbadm/bin/step1_daily"
current_mth=$(date +"%Y%m")
current_day=$(date +"%Y%m%d")

echo "#!/usr/bin/expect -f" > $module_folder/sftp_remove_from_data_DB2.sh
echo "spawn sftp umbadm@otherhost" >> $module_folder/sftp_remove_from_data_DB2.sh
echo "expect \"password:\"" >> $module_folder/sftp_remove_from_data_DB2.sh
echo "send \"password\n\"" >> $module_folder/sftp_remove_from_data_DB2.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_remove_from_data_DB2.sh
echo "send \"rm $1 \n \"" >> $module_folder/sftp_remove_from_data_DB2.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_remove_from_data_DB2.sh
echo "send \"exit\n\"" >> $module_folder/sftp_remove_from_data_DB2.sh

