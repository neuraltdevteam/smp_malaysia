#!/bin/bash

echo "" 
echo "" >> $log_file
echo "***** sftp from BLC2 and move starts *****" 
echo "***** sftp from BLC2 and move starts *****" >> $log_file
NOW=$(date)
echo "start date: $NOW" >>  $log_file

	
	$module_folder/gen_sftp_from_BLC.sh "$BLC_TDR_folder/*_in_*.txt" /data/tdr $BLC_IP_2 $BLC_ID_2 $BLC_Pwd_2
	/usr/bin/expect -f $module_folder/sftp_from_BLC.sh
	echo "downloaded TDR from BLC Server 2" 
	echo "downloaded TDR from BLC Server 2" >> $log_file
		
	$module_folder/gen_sftp_from_BLC.sh "$BLC_TDR_folder/*_out_*.txt" /data/tdr $BLC_IP_2 $BLC_ID_2 $BLC_Pwd_2
	/usr/bin/expect -f $module_folder/sftp_from_BLC.sh
	echo "downloaded TDR from BLC Server 2" 
	echo "downloaded TDR from BLC Server 2" >> $log_file

        $module_folder/gen_sftp_from_BLC.sh "$BLC_TDR_folder/*TDR*.txt" /data/tdr $BLC_IP_2 $BLC_ID_2 $BLC_Pwd_2
        /usr/bin/expect -f $module_folder/sftp_from_BLC.sh
        echo "downloaded TDR from BLC Server 1"
        echo "downloaded TDR from BLC Server 1" >> $log_file

        $module_folder/gen_sftp_from_BLC.sh "$BLC_TDR_folder/*Log*.txt" /data/tdr $BLC_IP_2 $BLC_ID_2 $BLC_Pwd_2
        /usr/bin/expect -f $module_folder/sftp_from_BLC.sh
        echo "downloaded TDR from BLC Server 1"
        echo "downloaded TDR from BLC Server 1" >> $log_file
	
	FILES=/data/tdr/*.txt
	for f in $FILES
	do
	$module_folder/gen_sftp_move_in_BLC.sh "$BLC_TDR_folder"/${f##*/} "$BLC_TDR_folder"/processed/$current_mth/$current_day/${f##*/} "$BLC_TDR_folder"/processed/$current_mth "$BLC_TDR_folder"/processed/$current_mth/$current_day $BLC_IP_2 $BLC_ID_2 $BLC_Pwd_2
	/usr/bin/expect -f $module_folder/sftp_move_in_BLC.sh
	done
	echo "moved TDR to processed folder in BLC Server 2" 
	echo "moved TDR to processed folder in BLC Server 2" >> $log_file



NOW=$(date)
echo "end date: $NOW" >>  $log_file

echo "***** sftp from BLC2 and move end *****" 
echo "***** sftp from BLC2 and move end *****" >> $log_file
echo ""
echo "" >> $log_file

