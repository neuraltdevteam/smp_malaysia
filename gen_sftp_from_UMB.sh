#!/bin/bash
module_folder="/home/umbadm/bin/step1_daily"

echo "#!/usr/bin/expect -f" > $module_folder/sftp_from_UMB_$6.sh
echo "set timeout -1" > $module_folder/sftp_from_UMB_$6.sh
echo "spawn sftp $4@$3" >> $module_folder/sftp_from_UMB_$6.sh
#echo "expect \"password:\"" >> $module_folder/sftp_from_UMB_$6.sh
#echo "send \"$5\n\"" >> $module_folder/sftp_from_UMB_$6.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_from_UMB_$6.sh
echo "send \"mget $1 $2 \n\"" >> $module_folder/sftp_from_UMB_$6.sh
echo "expect \"sftp>\"" >> $module_folder/sftp_from_UMB_$6.sh
echo "send \"exit\n\"" >> $module_folder/sftp_from_UMB_$6.sh

