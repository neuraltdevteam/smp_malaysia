#set @today_date_yyyymm = date_format(now(), '%Y%m');
set @today_date_yyyymm = date_format(now() - INTERVAL 1 MONTH, '%Y%m');

set @sql1 = concat("CREATE TABLE IF NOT EXISTS `UMB_SESS_", @today_date_yyyymm , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `short_code` varchar(10) DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL, 
  `duration` int(10) DEFAULT NULL,
  `sub_grp_id` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;


set @sql1 = concat("CREATE TABLE IF NOT EXISTS `UMB_MENU_HIT_", @today_date_yyyymm , "` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `short_code` varchar(10) DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL, 
  `duration` int(10) DEFAULT NULL,
  `menu_id` varchar(10) DEFAULT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;


set @sql1 = concat("CREATE TABLE IF NOT EXISTS `UMB_BLC_INTERFACE_", @today_date_yyyymm , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` varchar(2) DEFAULT NULL,
  `short_code` varchar(10) DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL, 
  `duration` int(10) DEFAULT NULL,
  `interface_name` varchar(50) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  `result_str` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

set @sql1 = concat("CREATE TABLE IF NOT EXISTS `BLC_TXN_TYPE_", @today_date_yyyymm , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `short_code` varchar(10) DEFAULT NULL,
  `channel` varchar(10) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL, 
  `duration` int(10) DEFAULT NULL,
  `trans_type` varchar(20) DEFAULT NULL,
  `in_api_name` varchar(50) DEFAULT NULL,
  `out_api_name` varchar(50) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  `out_host_id` int(2) DEFAULT NULL,
  `out_channel` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

set @sql1 = concat("CREATE TABLE IF NOT EXISTS `BLC_EXT_INTERFACE_", @today_date_yyyymm , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `short_code` varchar(10) DEFAULT NULL,
  `channel` varchar(10) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `out_host_id` int(2) DEFAULT NULL,
  `out_channel` varchar(10) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `duration` int(10) DEFAULT NULL,
  `api_name` varchar(50) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;


set @sql1 = concat("CREATE TABLE IF NOT EXISTS `SMS_IN_", @today_date_yyyymm , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL, 
  `duration` int(10) DEFAULT NULL,
  `dest_addr` varchar(20) DEFAULT NULL,
  `received_content` varchar(320) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  `IN_SMS_CHANNEL` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

set @sql1 = concat("CREATE TABLE IF NOT EXISTS `BLC_SMS_OUT_", @today_date_yyyymm , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `sms_content` varchar(160) DEFAULT NULL,
  `messsage_id` varchar(15) DEFAULT NULL,  
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

set @sql1 = concat("CREATE TABLE IF NOT EXISTS `SMPP_MDR_", @today_date_yyyymm , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `timestamp` datetime(6) DEFAULT NULL,
  `message_id` varchar(15) DEFAULT NULL,
  `command_id` varchar(5) DEFAULT NULL,
  `command_status` varchar(5) DEFAULT NULL,
  `src_addr` varchar(20) DEFAULT NULL,
  `dest_addr` varchar(20) DEFAULT NULL,
  `dest_imsi` varchar(20) DEFAULT NULL, 
  `usr_data_length` int(3) DEFAULT NULL,
  `usr_data_header_flag` varchar(20) DEFAULT NULL,
  `user_data` varchar(320) DEFAULT NULL,
  `data_coding_scheme` int(1) DEFAULT NULL,
  `message_status` varchar(30) DEFAULT NULL,
  `result_code` varchar(5) DEFAULT NULL,
  `priority_flag` int(1) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `dr_enable` varchar(1) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `optional_param` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;

set @sql1 = concat("CREATE TABLE IF NOT EXISTS `SMS_OUT_", @today_date_yyyymm , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `message_id` varchar(30) DEFAULT NULL,
  `request_date` datetime(6) DEFAULT NULL,
  `submission_date` datetime(6) DEFAULT NULL,
  `delivery_date` datetime(6) DEFAULT NULL,
  `sms_content` varchar(320) DEFAULT NULL,
  `data_coding_scheme` int(1) DEFAULT NULL,
  `dr_enable_flag` int(1) DEFAULT NULL,
  `priority_flag` int(1) DEFAULT NULL,
  `request_status` varchar(1) DEFAULT NULL,
  `req_reason_code` varchar(10) DEFAULT NULL,
  `submission_status` varchar(1) DEFAULT NULL,
  `submission_reason_code` varchar(10) DEFAULT NULL,
  `delivery_status` varchar(1) DEFAULT NULL,
  `delivery_reason_code` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;


set @sql1 = concat("CREATE TABLE IF NOT EXISTS `CHANNEL_", @today_date_yyyymm , "` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `host_id` int DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  `channel_name` varchar(4) DEFAULT NULL,
  `in_sms_channel` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=myisam AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");

PREPARE stmt1 FROM @sql1;
EXECUTE stmt1;
DEALLOCATE PREPARE stmt1;
