#!/bin/bash

export module_folder="/home/umbadm/bin/step1_daily"
export ussd_sess_tdr_prefix="ussd"
export ussd_menu_req_tdr_prefix="menureq"
export blc_incoming_transaction="blc_in"
export blc_backend_transaction="blc_out"
export blc_sms_in_prefix="blc_sms_in"
export blc_sms_out="blc_sms_out"
export smpp_gateway_mdr="mdr_smpp3_host"

export UMB_TDR_folder="/data/umb/tdr"
export UMB_CP_REQ_TDR_folder="cp_req"
export UMB_MAP_TDR_folder="map"
export UMB_SMPP_MDR_TDR_folder="smpp_mdr"
export UMB_ISMPP_MDR_TDR_folder="ismpp_mdr"
export UMB_MENU_HIT_TDR_folder="umb_menu_hit"  
export UMB_SESSION_TDR_folder="umb_session"

export BLC_TDR_folder="/BLC/tdr"

export UMB_IP_1="L28UMBAGWRPT01-OAM"
export UMB_ID_1="blc"
export UMB_Pwd_1="blc@umb2015"

export UMB_IP_2="10.67.32.149"
export UMB_ID_2="umbadm"
export UMB_Pwd_2="password"

export BLC_IP_1="10.67.32.149"
export BLC_ID_1="umbadm"
export BLC_Pwd_1="password"

export BLC_IP_2="10.67.32.149"
export BLC_ID_2="umbadm"
export BLC_Pwd_2="password"

export current_mth=$(date +"%Y%m")
#echo $current_mth

export current_day=$(date +"%Y%m%d")
#echo $current_day

export bkp_folder="/data/tdr/processed"
mkdir -p $bkp_folder/$current_mth
mkdir -p $bkp_folder/$current_mth/$current_day
 
export archv_folder="/data1/tdr/processed"
mkdir -p $archv_folder/$current_mth
mkdir -p $archv_folder/$current_mth/$current_day

export log_folder="/log/report_module"
mkdir -p $log_folder
export log_file=$log_folder/step1_$current_day.log 

echo "" 
echo "" >> $log_file
echo "***** Step 1 starts *****" 
echo "***** Step 1 starts *****" >> $log_file
NOW=$(date)
echo "start date: $NOW" >>  $log_file

if [[ -f "$module_folder/processflag.txt" ]]
then
	echo "previous processing not completed!! Please check and delete $module_folder/processflag.txt from DB 01 and DB 02 to proceed." 
	echo "previous processing not completed!! Please check and delete $module_folder/processflag.txt from DB 01 and DB 02 to proceed." >> $log_file

else

	echo "processing step 1" > $module_folder/processflag.txt
	echo "create in_process file"
	echo "create in_process file" >> $log_file
	
	#$module_folder/gen_sftp_put_to_data_DB2.sh $module_folder/processflag.txt $module_folder/
	#/usr/bin/expect -f $module_folder/sftp_put_to_data_DB2.sh
	#echo "copied in_process file to $module_folder in DB 2" 
	#echo "copied in_process file to $module_folder in DB 2" >> $log_file
	
	$module_folder/chk_create_raw_tables.sh
	echo "completed checking and creating raw tables" 
	echo "completed checking and creating raw tables" >> $log_file
	
	$module_folder/delete_raw_tables.sh
	echo "deleting data from raw tables" 
	echo "deleting data from raw tables" >> $log_file
	

	#$module_folder/sftp_from_umb_only.sh
	$module_folder/sftp_from_umb_and_move.sh

	
        #$module_folder/gen_sftp_remove_from_data_DB2.sh $module_folder/processflag.txt
        #/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
        #echo "removed in_process file from $module_folder in DB 2"
        #echo "removed in_process file from $module_folder in DB 2"  >> $log_file


        rm $module_folder/processflag.txt
        echo "delete in_process file"
        echo "delete in_process file" >> $log_file
	
fi

NOW=$(date)
echo "end date: $NOW" >>  $log_file

echo "***** Step 1 end *****" 
echo "***** Step 1 end *****" >> $log_file
echo ""
echo "" >> $log_file

