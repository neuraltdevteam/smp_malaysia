begin

Declare tmr_ymd varchar(8);
set tmr_ymd = date_format((date_add(now(), interval 1 day)), '%Y%m%d');

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `BLC_EXT_INTERFACE_", tmr_ymd , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `short_code` varchar(50) DEFAULT NULL,
  `reserved1` varchar(50) DEFAULT NULL,
  `reserved2` varchar(50) DEFAULT NULL,
  `reserved3` varchar(50) DEFAULT NULL,
  `channel` varchar(20) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `out_host_id` varchar(30) DEFAULT NULL,
  `out_channel` varchar(10) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `duration` int(10) DEFAULT NULL,
  `api_name` varchar(100) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  `tdr_name` VARCHAR(70) NULL,
  PRIMARY KEY (`ID`),
  KEY `Transaction_id` (`transaction_id`,`start_date`),
  KEY `msisdn_date` (`msisdn`,`start_date`),
  KEY `session_id` (`session_id`,`start_date`),
  KEY `date_msisdn_session` (`start_date`,`msisdn`,`session_id`),
  KEY `tdr_name` (`tdr_name`),
  KEY `date_msisdn_transaction` (`start_date`,`msisdn`,`transaction_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `BLC_SMS_OUT_", tmr_ymd , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `sms_content` varchar(160) DEFAULT NULL,
  `messsage_id` varchar(15) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  `tdr_name` VARCHAR(70) NULL,
  PRIMARY KEY (`ID`),
  KEY `session_id` (`session_id`,`start_date`),
  KEY `transaction_id` (`transaction_id`,`start_date`),
  KEY `date_msisdn_session` (`start_date`,`msisdn`,`session_id`),
  KEY `date_msisdn_transaction` (`start_date`,`msisdn`,`transaction_id`),
  KEY `tdr_name` (`tdr_name`),
  KEY `msisdn_date` (`msisdn`,`start_date`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `BLC_TXN_TYPE_", tmr_ymd , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `short_code` varchar(50) DEFAULT NULL,
  `channel` varchar(20) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `duration` int(10) DEFAULT NULL,
  `trans_type` varchar(50) DEFAULT NULL,
  `in_api_name` varchar(100) DEFAULT NULL,
  `out_api_name` varchar(100) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  `out_host_id` varchar(30) DEFAULT NULL,
  `out_channel` varchar(10) DEFAULT NULL,
  `tdr_name` VARCHAR(70) NULL,
  PRIMARY KEY (`ID`),
  KEY `TransID_TransType_APIName` (`start_date`,`transaction_id`,`trans_type`,`in_api_name`,`trans_status`,`reason_code`),
  KEY `session_id` (`session_id`,`start_date`),
  KEY `transaction_id` (`transaction_id`,`start_date`),
  KEY `date_msisdn_session_transtype` (`start_date`,`msisdn`,`session_id`,`trans_type`),
  KEY `date_msisdn_transaction_transtype` (`start_date`,`msisdn`,`transaction_id`,`trans_type`),
  KEY `date_msisdn_session_apiname` (`start_date`,`msisdn`,`session_id`,`in_api_name`),
  KEY `msisdn_date` (`msisdn`,`start_date`),
  KEY `tdr_name` (`tdr_name`),
  KEY `date_msisdn_transaction_apiname` (`start_date`,`msisdn`,`transaction_id`,`in_api_name`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `CHANNEL_", tmr_ymd , "` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `host_id` int(11) DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `date` datetime(6) DEFAULT NULL,
  `channel_name` varchar(20) DEFAULT NULL,
  `in_sms_channel` varchar(10) DEFAULT NULL,
  `tdr_name` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `session_id` (`session_id`,`date`),
  KEY `transaction_id` (`transaction_id`,`date`),
  KEY `date_msisdn_session_channelname` (`date`,`msisdn`,`session_id`,`channel_name`),
  KEY `date_msisdn_transaction_channelname` (`date`,`msisdn`,`transaction_id`,`channel_name`),
  KEY `msisdn_date` (`msisdn`,`date`),
  KEY `tdr_name` (`tdr_name`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `ISMPP_MDR_", tmr_ymd , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `timestamp` datetime(6) DEFAULT NULL,
  `message_id` varchar(15) DEFAULT NULL,
  `command_id` varchar(5) DEFAULT NULL,
  `command_status` varchar(5) DEFAULT NULL,
  `src_addr` varchar(20) DEFAULT NULL,
  `dest_addr` varchar(20) DEFAULT NULL,
  `dest_imsi` varchar(20) DEFAULT NULL,
  `usr_data_length` int(3) DEFAULT NULL,
  `usr_data_header_flag` varchar(20) DEFAULT NULL,
  `user_data` varchar(320) DEFAULT NULL,
  `data_coding_scheme` int(1) DEFAULT NULL,
  `message_status` varchar(30) DEFAULT NULL,
  `result_code` varchar(5) DEFAULT NULL,
  `priority_flag` int(1) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `dr_enable` varchar(1) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `optional_param` varchar(200) DEFAULT NULL,
  `discharge_time` datetime(6) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `transaction_id_idx` (`transaction_id`,`timestamp`),
  KEY `message_id_idx` (`message_id`,`timestamp`),
  KEY `message_id` (`message_id`,`timestamp`),
  KEY `session_id` (`session_id`,`timestamp`),
  KEY `transaction_id` (`transaction_id`,`timestamp`),
  KEY `date_destaddr_session` (`timestamp`,`dest_addr`,`session_id`),
  KEY `date_destaddr_transaction` (`timestamp`,`dest_addr`,`transaction_id`),
  KEY `destaddr_date` (`dest_addr`,`timestamp`),
  KEY `host_date` (`host_id`,`timestamp`),
  KEY `filename` (`filename`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `SMPP_MDR_", tmr_ymd , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `timestamp` datetime(6) DEFAULT NULL,
  `message_id` varchar(15) DEFAULT NULL,
  `command_id` varchar(5) DEFAULT NULL,
  `command_status` varchar(5) DEFAULT NULL,
  `src_addr` varchar(20) DEFAULT NULL,
  `dest_addr` varchar(20) DEFAULT NULL,
  `dest_imsi` varchar(20) DEFAULT NULL,
  `usr_data_length` int(3) DEFAULT NULL,
  `usr_data_header_flag` varchar(20) DEFAULT NULL,
  `user_data` varchar(320) DEFAULT NULL,
  `data_coding_scheme` int(1) DEFAULT NULL,
  `message_status` varchar(30) DEFAULT NULL,
  `result_code` varchar(5) DEFAULT NULL,
  `priority_flag` int(1) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `dr_enable` varchar(1) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `optional_param` varchar(200) DEFAULT NULL,
  `filename` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `message_id` (`message_id`,`timestamp`),
  KEY `session_id` (`session_id`,`timestamp`),
  KEY `transaction_id` (`transaction_id`,`timestamp`),
  KEY `date_destaddr_session` (`timestamp`,`dest_addr`,`session_id`),
  KEY `date_destaddr_transaction` (`timestamp`,`dest_addr`,`transaction_id`),
  KEY `destaddr_date` (`dest_addr`,`timestamp`),
  KEY `host_date` (`host_id`,`timestamp`),
  KEY `filename` (`filename`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `SMS_IN_", tmr_ymd , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `duration` int(10) DEFAULT '0',
  `dest_addr` varchar(20) DEFAULT NULL,
  `received_content` varchar(320) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  `IN_SMS_CHANNEL` varchar(10) DEFAULT NULL,
  `tdr_name` VARCHAR(70) NULL,
  PRIMARY KEY (`ID`),
  KEY `transaction_id_idx` (`transaction_id`,`start_date`),
  KEY `session_id` (`session_id`,`start_date`),
  KEY `date_msisdn_session` (`start_date`,`msisdn`,`session_id`),
  KEY `date_msisdn_transaction` (`start_date`,`msisdn`,`transaction_id`),
  KEY `msisdn_date` (`msisdn`,`start_date`),
  KEY `duration` (`duration`),
  KEY `tdr_name` (`tdr_name`),
  KEY `msisdn_transaction` (`msisdn`,`transaction_id`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `SMS_OUT_", tmr_ymd , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `msisdn` varchar(30) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `message_id` varchar(30) DEFAULT NULL,
  `request_date` datetime(6) DEFAULT NULL,
  `submission_date` datetime(6) DEFAULT NULL,
  `delivery_date` datetime(6) DEFAULT NULL,
  `sms_content` varchar(320) DEFAULT NULL,
  `data_coding_scheme` int(1) DEFAULT NULL,
  `dr_enable_flag` int(1) DEFAULT NULL,
  `priority_flag` int(1) DEFAULT NULL,
  `request_status` varchar(1) DEFAULT NULL,
  `req_reason_code` varchar(10) DEFAULT NULL,
  `submission_status` varchar(1) DEFAULT NULL,
  `submission_reason_code` varchar(10) DEFAULT NULL,
  `delivery_status` varchar(1) DEFAULT NULL,
  `delivery_reason_code` varchar(10) DEFAULT NULL,
  `smsc_submission_date` datetime(6) DEFAULT NULL,
  `tdr_name` VARCHAR(70) NULL,
  PRIMARY KEY (`ID`),
  KEY `transaction_id_idx` (`transaction_id`,`request_date`),
  KEY `message_id_idx` (`message_id`,`request_date`),
  KEY `session_id` (`session_id`,`request_date`),
  KEY `date_msisdn_session` (`request_date`,`msisdn`,`session_id`),
  KEY `date_msisdn_transaction` (`request_date`,`msisdn`,`transaction_id`),
  KEY `msisdn_date` (`msisdn`,`request_date`),
  KEY `delivery` (`delivery_date`,`delivery_status`,`delivery_reason_code`),
  KEY `tdr_name` (`tdr_name`),
  KEY `submission` (`submission_date`,`submission_status`,`submission_reason_code`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `UMB_BLC_INTERFACE_", tmr_ymd , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` varchar(2) DEFAULT NULL,
  `short_code` varchar(50) DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `session_id` varchar(50) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `duration` int(10) DEFAULT NULL,
  `interface_name` varchar(100) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  `result_str` varchar(150) DEFAULT NULL,
  `tdr_name` VARCHAR(70) NULL,
  PRIMARY KEY (`ID`),
  KEY `TransID_APIName` (`start_date`,`transaction_id`,`trans_status`,`reason_code`),
  KEY `session_id` (`session_id`,`start_date`),
  KEY `transaction_id` (`transaction_id`,`start_date`),
  KEY `date_msisdn_session` (`start_date`,`msisdn`,`session_id`),
  KEY `date_msisdn_transaction` (`start_date`,`msisdn`,`transaction_id`),
  KEY `tdr_name` (`tdr_name`),
  KEY `msisdn_date` (`msisdn`,`start_date`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `UMB_MENU_HIT_", tmr_ymd , "` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `short_code` varchar(10) DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `transaction_id` varchar(30) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `duration` int(10) DEFAULT NULL,
  `menu_id` varchar(10) DEFAULT NULL,
  `menu_name` varchar(50) DEFAULT NULL,
  `trans_status` varchar(1) DEFAULT NULL,
  `reason_code` varchar(10) DEFAULT NULL,
  `tdr_name` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `session_id` (`session_id`,`start_date`),
  KEY `transaction_id` (`transaction_id`,`start_date`),
  KEY `date_msisdn_session` (`start_date`,`msisdn`,`session_id`),
  KEY `date_msisdn_transaction` (`start_date`,`msisdn`,`transaction_id`),
  KEY `msisdn_date` (`msisdn`,`start_date`),
  KEY `tdr_name` (`tdr_name`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

set @sqlfull = concat("CREATE TABLE IF NOT EXISTS `UMB_SESS_", tmr_ymd , "` (
  `ID` int(10) NOT NULL AUTO_INCREMENT,
  `host_id` int(2) DEFAULT NULL,
  `short_code` varchar(10) DEFAULT NULL,
  `msisdn` varchar(20) DEFAULT NULL,
  `session_id` varchar(30) DEFAULT NULL,
  `start_date` datetime(6) DEFAULT NULL,
  `end_date` datetime(6) DEFAULT NULL,
  `duration` int(10) DEFAULT NULL,
  `sub_grp_id` varchar(1) DEFAULT NULL,
  `tdr_name` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `sub_grp_id` (`sub_grp_id`,`start_date`),
  KEY `date_msisdn_session` (`start_date`,`msisdn`,`session_id`),
  KEY `msisdn_date` (`msisdn`,`start_date`),
  KEY `tdr_name` (`tdr_name`)
) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;");
prepare stmt from @sqlfull;
execute stmt;
deallocate prepare stmt;

end
