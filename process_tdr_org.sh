#!/bin/bash

	echo "Process tdr starts"
	echo "Process tdr starts" >> $log_file

	FILES=/data/tdr/*.txt
	for f in $FILES
	do
  echo "Processing $f file..." 
  echo "Processing $f file..." >> $log_file
  #echo ${f##*/}
  
  # take action on each file. $f store current file name
	
	if [[ $f == *$ussd_sess_tdr_prefix* ]]
	then	  
		$module_folder/loadfile/load_ussd_sess.sh $f $ussd_sess_tdr_prefix
		echo "completed loading $f file..."
		echo "completed loading $f file..." >> $log_file
	
		$module_folder/gen_sftp_to_DB2.sh $f $archv_folder/$current_mth/$current_day $archv_folder/$current_mth
		/usr/bin/expect -f $module_folder/sftp_to_DB2.sh
		echo "archived TDR to processed folder in DB 2" 
		echo "archived TDR to processed folder in DB 2"  >> $log_file
		
		#cp -f $f $bkp_folder/$current_mth/$current_day/
		#echo "copied $f to $bkp_folder/$current_mth/$current_day/" 
		#echo "copied $f to $bkp_folder/$current_mth/$current_day/" >> $log_file
		
		mv $f $archv_folder/$current_mth/$current_day/
		echo "moved $f to $archv_folder/$current_mth/$current_day/" 
		echo "moved $f to $archv_folder/$current_mth/$current_day/" >> $log_file
				
		$module_folder/gen_sftp_remove_from_data_DB2.sh $f
		/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
		echo "removed TDR from tdr folder in DB 2" 
		echo "removed TDR from tdr folder in DB 2"  >> $log_file
				
	elif [[ $f == *$ussd_menu_req_tdr_prefix* ]]
	then	
		$module_folder/loadfile/load_ussd_menu_req.sh $f $ussd_menu_req_tdr_prefix
		echo "completed loading $f file..." 
		echo "completed loading $f file..." >> $log_file
	
		$module_folder/gen_sftp_to_DB2.sh $f $archv_folder/$current_mth/$current_day  $archv_folder/$current_mth
		/usr/bin/expect -f $module_folder/sftp_to_DB2.sh		
		echo "archived TDR to processed folder in DB 2" 
		echo "archived TDR to processed folder in DB 2"  >> $log_file
			
		mv $f $archv_folder/$current_mth/$current_day/
		echo "moving $f to $archv_folder/$current_mth/$current_day/" 
		echo "moving $f to $archv_folder/$current_mth/$current_day/" >> $log_file
	
		$module_folder/gen_sftp_remove_from_data_DB2.sh $f
		/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
		echo "removed TDR from tdr folder in DB 2" 
		echo "removed TDR from tdr folder in DB 2"  >> $log_file
				
	elif [[ $f == *$blc_incoming_transaction* ]]
	then	
		$module_folder/loadfile/load_blc_incoming_transaction.sh $f	$blc_incoming_transaction
		echo "completed loading $f file..." >> $log_file
		echo "completed loading $f file..." 
			
		$module_folder/gen_sftp_to_DB2.sh $f $archv_folder/$current_mth/$current_day  $archv_folder/$current_mth
		/usr/bin/expect -f $module_folder/sftp_to_DB2.sh		
		echo "archived TDR to processed folder in DB 2" 
		echo "archived TDR to processed folder in DB 2"  >> $log_file
			
		mv $f $archv_folder/$current_mth/$current_day/
		echo "moving $f to $archv_folder/$current_mth/$current_day/" 
		echo "moving $f to $archv_folder/$current_mth/$current_day/" >> $log_file
	
		$module_folder/gen_sftp_remove_from_data_DB2.sh $f
		/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
		echo "removed TDR from tdr folder in DB 2" 
		echo "removed TDR from tdr folder in DB 2"  >> $log_file
					
	elif [[ $f == *$blc_backend_transaction* ]]
	then	
		$module_folder/loadfile/load_blc_backend_transaction.sh $f $blc_backend_transaction
		echo "completed loading $f file..." 
		echo "completed loading $f file..." >> $log_file
	
		$module_folder/gen_sftp_to_DB2.sh $f $archv_folder/$current_mth/$current_day  $archv_folder/$current_mth
		/usr/bin/expect -f $module_folder/sftp_to_DB2.sh		
		echo "archived TDR to processed folder in DB 2" 
		echo "archived TDR to processed folder in DB 2"  >> $log_file
						
		mv $f $archv_folder/$current_mth/$current_day/
		echo "moving $f to $archv_folder/$current_mth/$current_day/" 
		echo "moving $f to $archv_folder/$current_mth/$current_day/" >> $log_file
		
		$module_folder/gen_sftp_remove_from_data_DB2.sh $f
		/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
		echo "removed TDR from tdr folder in DB 2" 
		echo "removed TDR from tdr folder in DB 2"  >> $log_file
						
	elif [[ $f == *$blc_sms_in_prefix* ]]
	then	
		$module_folder/loadfile/load_blc_sms_in.sh $f	$blc_sms_in_prefix
		echo "completed loading $f file..." 
		echo "completed loading $f file..." >> $log_file
	
		$module_folder/gen_sftp_to_DB2.sh $f $archv_folder/$current_mth/$current_day  $archv_folder/$current_mth
		/usr/bin/expect -f $module_folder/sftp_to_DB2.sh		
		echo "archived TDR to processed folder in DB 2" 
		echo "archived TDR to processed folder in DB 2"  >> $log_file
						
		mv $f $archv_folder/$current_mth/$current_day/
		echo "moving $f to $archv_folder/$current_mth/$current_day/" 
		echo "moving $f to $archv_folder/$current_mth/$current_day/" >> $log_file
		
		$module_folder/gen_sftp_remove_from_data_DB2.sh $f
		/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
		echo "removed TDR from tdr folder in DB 2" 
		echo "removed TDR from tdr folder in DB 2"  >> $log_file
						
	elif [[ $f == *$blc_sms_out* ]]
	then	
		$module_folder/loadfile/load_blc_sms_out.sh $f $blc_sms_out
		echo "completed loading $f file..." 
		echo "completed loading $f file..." >> $log_file
	
		$module_folder/gen_sftp_to_DB2.sh $f $archv_folder/$current_mth/$current_day  $archv_folder/$current_mth
		/usr/bin/expect -f $module_folder/sftp_to_DB2.sh		
		echo "archived TDR to processed folder in DB 2" 
		echo "archived TDR to processed folder in DB 2"  >> $log_file
						
		mv $f $archv_folder/$current_mth/$current_day/
		echo "moving $f to $archv_folder/$current_mth/$current_day/" 
		echo "moving $f to $archv_folder/$current_mth/$current_day/" >> $log_file
		
		$module_folder/gen_sftp_remove_from_data_DB2.sh $f
		/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
		echo "removed TDR from tdr folder in DB 2" 
		echo "removed TDR from tdr folder in DB 2"  >> $log_file
						
	elif [[ $f == *$smpp_gateway_mdr* ]]
	then	
		$module_folder/loadfile/load_smpp_gateway_mdr.sh $f	$smpp_gateway_mdr
		echo "completed loading $f file..." 
		echo "completed loading $f file..." >> $log_file
	
		$module_folder/gen_sftp_to_sub_DB2.sh $f $archv_folder/$current_mth/$current_day  $archv_folder/$current_mth $archv_folder/$current_mth/$current_day/smpp_mdr
		/usr/bin/expect -f $module_folder/sftp_to_sub_DB2.sh		
		echo "archived TDR to processed folder in DB 2" 
		echo "archived TDR to processed folder in DB 2"  >> $log_file
					
		mkdir $archv_folder/$current_mth/$current_day/smpp_mdr	
		mv $f $archv_folder/$current_mth/$current_day/smpp_mdr
		echo "moving $f to $archv_folder/$current_mth/$current_day/smpp_mdr" 
		echo "moving $f to $archv_folder/$current_mth/$current_day/smpp_mdr" >> $log_file
		
		$module_folder/gen_sftp_remove_from_data_DB2.sh $f
		/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
		echo "removed TDR from tdr folder in DB 2" 
		echo "removed TDR from tdr folder in DB 2"  >> $log_file

        elif [[ $f == *$ismpp_gateway_mdr* ]]
        then
                #$module_folder/loadfile/load_ismpp_gateway_mdr.sh $f     $ismpp_gateway_mdr
                #echo "completed loading $f file..."
                #echo "completed loading $f file..." >> $log_file

                $module_folder/gen_sftp_to_sub_DB2.sh $f $archv_folder/$current_mth/$current_day  $archv_folder/$current_mth $archv_folder/$current_mth/$current_day/ismpp_mdr 
                /usr/bin/expect -f $module_folder/sftp_to_sub_DB2.sh
                echo "archived TDR to processed folder in DB 2"
                echo "archived TDR to processed folder in DB 2"  >> $log_file

                mkdir $archv_folder/$current_mth/$current_day/ismpp_mdr
                mv $f $archv_folder/$current_mth/$current_day/ismpp_mdr
                echo "moving $f to $archv_folder/$current_mth/$current_day/ismpp_mdr"
                echo "moving $f to $archv_folder/$current_mth/$current_day/ismpp_mdr" >> $log_file

                $module_folder/gen_sftp_remove_from_data_DB2.sh $f
                /usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh
                echo "removed TDR from tdr folder in DB 2"
                echo "removed TDR from tdr folder in DB 2"  >> $log_file
					
	else
		mysql --login-path=local -D smp -e "Insert into report_module_hist (step1_id, step1_desc, process_id, process_desc, processed_date, TDR_name, status) values ('1', 'Process TDR', '', '' , now(), '$f', 'Unknown file; file prefix not matched')"	
	
		$module_folder/gen_sftp_to_sub_DB2.sh $f $archv_folder/$current_mth/$current_day  $archv_folder/$current_mth $archv_folder/$current_mth/$current_day/misc
		/usr/bin/expect -f $module_folder/sftp_to_sub_DB2.sh		
		echo "archived TDR to processed folder in DB 2" 
		echo "archived TDR to processed folder in DB 2"  >> $log_file
		
		mkdir $archv_folder/$current_mth/$current_day/misc	
		mv $f $archv_folder/$current_mth/$current_day/misc
		echo "moving $f to $archv_folder/$current_mth/$current_day/misc" 
		echo "moving $f to $archv_folder/$current_mth/$current_day/mics" >> $log_file
		
		$module_folder/gen_sftp_remove_from_data_DB2.sh $f
		/usr/bin/expect -f $module_folder/sftp_remove_from_data_DB2.sh		
		echo "removed TDR from tdr folder in DB 2" 
		echo "removed TDR from tdr folder in DB 2"  >> $log_file
	
	fi
	
	done

	echo "Process tdr end"
	echo "Process tdr end" >> $log_file
